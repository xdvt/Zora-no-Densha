package zoranodensha.api.vehicles.rendering;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.boundingBox.MultiBoundingBox;
import zoranodensha.api.vehicles.event.train.TrainRenderEvent;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer.IVehiclePartSpecialRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.util.ConcatenatedPartsList;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * Entity renderer that delegates draw calls down to the {@link zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer renderers}
 * of a {@link zoranodensha.api.vehicles.Train train}'s vehicle parts.
 * 
 * @author Leshuwa Kaiheiwa
 */
@SideOnly(Side.CLIENT)
public class TrainRenderer extends Render
{
	/** Current partial tick time, used to interpolate positions. */
	public static float partialTick;
	/** Currently active render pass, as returned by {@link net.minecraftforge.client.MinecraftForgeClient#getRenderPass() getRenderPass()}. */
	public static int renderPass;



	@Override
	public void doRender(Entity entity, double x, double y, double z, float f, float partialTick)
	{
		if (entity instanceof Train)
		{
			TrainRenderer.partialTick = partialTick;
			TrainRenderer.renderPass = MinecraftForgeClient.getRenderPass();
			doRender((Train)entity);
		}
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return null;
	}

	/**
	 * Actual render method.
	 *
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} to render.
	 */
	protected static void doRender(Train train)
	{
		/*
		 * Only render if the event wasn't cancelled.
		 */
		if (!MinecraftForge.EVENT_BUS.post(new TrainRenderEvent.Pre(train, partialTick, renderPass)))
		{
			/*
			 * General matrix push
			 */
			GL11.glPushMatrix();
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

			/*
			 * Section rendering
			 */
			for (VehicleData vehicle : train)
			{
				for (VehicleSection section : vehicle)
				{
					GL11.glPushMatrix();
					{
						/* Brightness calculation */
						int brightness = (PartHelper.getBrightness(section.parts.get(0)) + PartHelper.getBrightness(section.parts.get(section.parts.size() - 1))) / 2;
						int u = brightness % 65536;
						int v = brightness / 65536;
						OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, u, v);

						/* Rotation calculation */
						float roll = interpolate(section.getRotationRoll(), section.getPrevRotationRoll());
						float yaw = interpolate(section.getRotationYaw(), section.getPrevRotationYaw());
						float pitch = interpolate(section.getRotationPitch(), section.getPrevRotationPitch());

						/* Offset section position */
						translate(section.getPrevPosX(), section.getPrevPosY(), section.getPrevPosZ(), section.getPosX(), section.getPosY(), section.getPosZ());

						GL11.glPushMatrix();
						{
							/* Rotate section parts */
							GL11.glRotatef(yaw, 0.0F, -1.0F, 0.0F);
							GL11.glRotatef(pitch, 0.0F, 0.0F, 1.0F);
							GL11.glRotatef(roll, -1.0F, 0.0F, 0.0F);
							GL11.glTranslatef(-section.centerX, -section.centerY, -section.centerZ);

							/* Render section parts */
							for (int i = section.parts.size() - 1; i >= 0; --i)
							{
								renderPart(section.parts.get(i), false);
							}
						}
						GL11.glPopMatrix();

						/* Render bogies */
						if (section.sectionF == null || section.bogieF != section.sectionF.bogieB)
						{
							renderPart_Bogie(section.bogieF.getParent(), section, roll, yaw, pitch);
						}
						if (section.bogieB != null)
						{
							renderPart_Bogie(section.bogieB.getParent(), section, roll, yaw, pitch);
						}
					}
					GL11.glPopMatrix();
				}
			}

			/*
			 * MultiBoundingBox rendering
			 */
			GL11.glTranslated(-RenderManager.renderPosX, -RenderManager.renderPosY, -RenderManager.renderPosZ);
			renderBoundingBox(train);

			/*
			 * General matrix pop
			 */
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glPopMatrix();
		}

		/*
		 * Post after-render event
		 */
		MinecraftForge.EVENT_BUS.post(new TrainRenderEvent.Post(train));
	}

	/**
	 * Helper method to determine whether the given scale inverts normals vectors and thus requires inverse cull mode.
	 */
	public static final boolean getInvertCullMode(float scaleX, float scaleY, float scaleZ)
	{
		int i = 0;

		if (scaleX < 0.0F)
		{
			++i;
		}
		if (scaleY < 0.0F)
		{
			++i;
		}
		if (scaleZ < 0.0F)
		{
			++i;
		}

		return (i % 2 != 0);
	}

	/**
	 * Helper method to interpolate the given values with the current partial tick time.
	 */
	public static float interpolate(float curr, float prev)
	{
		return prev + (curr - prev) * partialTick;
	}

	/**
	 * Renders (debug) bounding boxes for the given train.
	 * 
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} whose bounding boxes should be drawn.
	 */
	protected static void renderBoundingBox(Train train)
	{
		/* Only render if necessary. */
		ConcatenatedPartsList unfinishedParts = train.getVehicleParts(OrderedPartsList.SELECTOR_UNFINISHED);
		if (!RenderManager.debugBoundingBox && unfinishedParts.isEmpty())
		{
			return;
		}

		/*
		 * Draw bounding boxes.
		 */
		GL11.glDepthMask(false);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);
		{
			/* Render debug boxes while F3 + B is active. */
			if (RenderManager.debugBoundingBox)
			{
				for (VehicleData vehicle : train)
				{
					for (VehicleSection section : vehicle)
					{
						/* Each section box renders yellow. Children of a section render in shades of green. */
						renderBoundingBoxChildren(section.getBoundingBox(), new ColorRGBA(0xFFFF00));
					}

					/* Each vehicle box render orange. */
					AxisAlignedBB modifiedVehicleBB = AxisAlignedBB.getBoundingBox(0, 0, 0, 0, 0, 0);
					modifiedVehicleBB.setBB(vehicle.getBoundingBox());
					modifiedVehicleBB = modifiedVehicleBB.expand(0.01, 0.01, 0.01);

					RenderGlobal.drawOutlinedBoundingBox(modifiedVehicleBB, 0xFF7700);
				}
			}

			/* Train bounding box. */
			AxisAlignedBB modifiedTrainBB = AxisAlignedBB.getBoundingBox(0, 0, 0, 0, 0, 0);
			modifiedTrainBB.setBB(train.getBoundingBox());
			modifiedTrainBB = modifiedTrainBB.expand(0.02, 0.02, 0.02);

			/* Render unfinished parts and train in turquoise. */
			if (!unfinishedParts.isEmpty())
			{
				for (VehParBase part : unfinishedParts)
				{
					RenderGlobal.drawOutlinedBoundingBox(part.getBoundingBox(), 0x00FFFF);
				}

				RenderGlobal.drawOutlinedBoundingBox(modifiedTrainBB, 0x00FFFF);
			}

			/* Render train outline red if there is no unfinished part. */
			else
			{
				RenderGlobal.drawOutlinedBoundingBox(modifiedTrainBB, 0xFF0000);
			}

		}
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDepthMask(true);
	}

	/**
	 * Helper method to recursively render child boxes of a (multi-) bounding box.
	 * 
	 * @param aabb - {@link net.minecraft.util.AxisAlignedBB Bounding box} to render; may contain children if it's a {@link zoranodensha.api.vehicles.boundingBox.MultiBoundingBox Multi Bounding Box}.
	 * @param col - {@link zoranodensha.api.util.ColorRGBA Colour} to render the given box in.
	 */
	protected static void renderBoundingBoxChildren(AxisAlignedBB aabb, ColorRGBA col)
	{
		if (aabb instanceof MultiBoundingBox)
		{
			ColorRGBA childCol = new ColorRGBA(aabb.hashCode() & 0x00FF00).multiply(0.8F);
			for (AxisAlignedBB child : (MultiBoundingBox)aabb)
			{
				renderBoundingBoxChildren(child, childCol);
			}
		}

		RenderGlobal.drawOutlinedBoundingBox(aabb, col.toHEX());
	}

	/**
	 * Helper method to render a part of a vehicle.
	 */
	protected static void renderPart(VehParBase part, boolean isBogie)
	{
		/*
		 * Check preconditions; don't render if either unfinished or not in valid pass.
		 */
		if (!part.getIsFinished() && part.getTrain() != null && part.getTrain().addedToChunk)
		{
			return;
		}

		IVehiclePartRenderer render = part.getRenderer();
		if (!render.renderInPass(renderPass))
		{
			return;
		}

		/*
		 * Push matrix stack, backup cull mode, check whether render is a VPSR.
		 */
		GL11.glPushMatrix();
		GL11.glPushAttrib(GL11.GL_LIGHTING);
		final int prevCullMode = GL11.glGetInteger(GL11.GL_CULL_FACE_MODE);
		final IVehiclePartSpecialRenderer vpsr;

		if (render instanceof IVehiclePartSpecialRenderer)
		{
			vpsr = (IVehiclePartSpecialRenderer)render;
		}
		else
		{
			vpsr = null;
		}

		/* Disable lighting during transparency render pass. */
		if (renderPass == 1)
		{
			GL11.glDisable(GL11.GL_LIGHTING);
		}
		else
		{
			GL11.glEnable(GL11.GL_LIGHTING);
		}

		/* Prepare OpenGL engine */
		if (vpsr == null || !vpsr.renderPart_Pre(partialTick))
		{
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			float[] arr;

			PartTypeBogie bogie = part.getPartType(PartTypeBogie.class);
			if (isBogie && bogie != null)
			{
				GL11.glRotatef(interpolate(bogie.getRotationYaw(), bogie.getPrevRotationYaw()), 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(interpolate(bogie.getRotationPitch(), bogie.getPrevRotationPitch()), 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(interpolate(bogie.getRotationRoll(), bogie.getPrevRotationRoll()), 1.0F, 0.0F, 0.0F);
			}
			else
			{
				arr = part.getOffset();
				GL11.glTranslatef(arr[0], arr[1], arr[2]);
				arr = part.getRotation();
				GL11.glRotatef(arr[1], 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(arr[2], 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(arr[0], 1.0F, 0.0F, 0.0F);
			}

			arr = part.getScale();
			GL11.glScalef(arr[0], arr[1], arr[2]);

			if (getInvertCullMode(arr[0], arr[1], arr[2]))
			{
				GL11.glCullFace(GL11.GL_FRONT);
			}
		}

		/* Render */
		render.renderPart(partialTick);

		/* Undo possible changes */
		if (vpsr != null)
		{
			vpsr.renderPart_Post();
		}

		/* Pop matrix stack, reset cull mode. */
		GL11.glCullFace(prevCullMode);
		GL11.glPopAttrib();
		GL11.glPopMatrix();
	}

	/**
	 * Helper method to prepare bogie rendering.
	 */
	protected static void renderPart_Bogie(VehParBase bogie, VehicleSection section, float roll, float yaw, float pitch)
	{
		float[] off = bogie.getOffset();
		Vec3 vec = Vec3.createVectorHelper(off[0] - section.centerX, off[1] - section.centerY, off[2] - section.centerZ);
		vec.rotateAroundZ(-pitch * VehicleHelper.RAD_FACTOR);
		vec.rotateAroundY(-yaw * VehicleHelper.RAD_FACTOR);
		vec.rotateAroundX(-roll * VehicleHelper.RAD_FACTOR);

		GL11.glPushMatrix();
		GL11.glTranslated(vec.xCoord, vec.yCoord, vec.zCoord);
		renderPart(bogie, true);
		GL11.glPopMatrix();
	}

	/**
	 * Helper method to translate section and bogie positions.
	 */
	public static void translate(double prevX, double prevY, double prevZ, double x, double y, double z)
	{
		x = prevX + (x - prevX) * partialTick;
		y = prevY + (y - prevY) * partialTick;
		z = prevZ + (z - prevZ) * partialTick;
		GL11.glTranslated(x - RenderManager.renderPosX, y - RenderManager.renderPosY, z - RenderManager.renderPosZ);
	}
}