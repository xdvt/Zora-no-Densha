package zoranodensha.api.vehicles.rendering;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.PriorityQueue;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.launchwrapper.Launch;
import zoranodensha.api.util.APILogger;



/**
 * Helper class to create a {@link net.minecraft.client.shader.TesselatorVertexState TessellatorVertexState}
 * holding vertex state data for <i>triangles</i>, unlike Minecraft's default implementation using squares.<br>
 * Uses reflection to access {@link net.minecraft.client.renderer.Tessellator Tessellator} fields.
 */
@SideOnly(Side.CLIENT)
public class VertexStateCreator_Tris
{
	/** {@code double} */
	private static Field xOffset;
	private static Field yOffset;
	private static Field zOffset;

	/** {@code int} and {@code int[]} */
	private static Field rawBuffer;
	private static Field rawBufferIndex;
	private static Field vertexCount;

	/*
	 * Statically initialise all field references.
	 * Throw an exception if something goes wrong.
	 */
	static
	{
		final boolean isDeobf = Boolean.TRUE.equals(Launch.blackboard.get("fml.deobfuscatedEnvironment"));
		try
		{
			//@formatter:off
			xOffset 		= Tessellator.class.getDeclaredField(isDeobf ? "xOffset" : 			"field_78408_v");
			yOffset 		= Tessellator.class.getDeclaredField(isDeobf ? "yOffset" : 			"field_78407_w");
			zOffset 		= Tessellator.class.getDeclaredField(isDeobf ? "zOffset" : 			"field_78417_x");

			rawBuffer 		= Tessellator.class.getDeclaredField(isDeobf ? "rawBuffer" : 		"field_78405_h");
			rawBufferIndex 	= Tessellator.class.getDeclaredField(isDeobf ? "rawBufferIndex" : 	"field_147569_p");
			vertexCount 	= Tessellator.class.getDeclaredField(isDeobf ? "vertexCount" : 		"field_78406_i");
			//@formatter:on
		}
		catch (Exception e)
		{
			APILogger.catching(e);
		}
	}



	/**
	 * Retrieves a vertex state from the current tessellation.
	 */
	public static final TesselatorVertexState getVertexState()
	{
		try
		{
			final int vertexCount = getVertexCount();
			final int rawBufferIndex = getRawBufferIndex();
			final int i0 = (rawBufferIndex / vertexCount) * 3;
			final int[] rawBuffer = getRawBuffer();

			PriorityQueue<Integer> priorityqueue = new PriorityQueue<Integer>(rawBufferIndex, new TriangleComparator(rawBuffer, getOffsetX(), getOffsetY(), getOffsetZ()));
			int[] aint = new int[rawBufferIndex];
			int i;

			for (i = 0; i < rawBufferIndex; i += i0)
			{
				priorityqueue.add(i);
			}

			for (i = 0; !priorityqueue.isEmpty(); i += i0)
			{
				System.arraycopy(rawBuffer, priorityqueue.remove(), aint, i, i0);
			}

			System.arraycopy(aint, 0, rawBuffer, 0, aint.length);
			return new TesselatorVertexState(aint, rawBufferIndex, vertexCount, true, false, true, true);
		}
		catch (IllegalAccessException e)
		{
			APILogger.debug(VertexStateCreator_Tris.class, String.format("Unable to create vertex state: %s", e));
		}
		return null;
	}


	/*
	 * ==============================================
	 * ============== Accessor methods ==============
	 * ==============================================
	 */

	/*
	 * Double fields
	 */
	private static float getOffsetX() throws IllegalArgumentException, IllegalAccessException
	{
		xOffset.setAccessible(true);
		return (float)xOffset.getDouble(Tessellator.instance);
	}

	private static float getOffsetY() throws IllegalArgumentException, IllegalAccessException
	{
		yOffset.setAccessible(true);
		return (float)yOffset.getDouble(Tessellator.instance);
	}

	private static float getOffsetZ() throws IllegalArgumentException, IllegalAccessException
	{
		zOffset.setAccessible(true);
		return (float)zOffset.getDouble(Tessellator.instance);
	}

	/*
	 * Integer fields
	 */
	private static int[] getRawBuffer() throws IllegalArgumentException, IllegalAccessException
	{
		rawBuffer.setAccessible(true);
		return (int[])rawBuffer.get(Tessellator.instance);
	}

	private static int getRawBufferIndex() throws IllegalArgumentException, IllegalAccessException
	{
		rawBufferIndex.setAccessible(true);
		return rawBufferIndex.getInt(Tessellator.instance);
	}

	private static int getVertexCount() throws IllegalArgumentException, IllegalAccessException
	{
		vertexCount.setAccessible(true);
		return vertexCount.getInt(Tessellator.instance);
	}



	/**
	 * Comparator class, similar to Minecraft's {@link net.minecraft.client.util.QuadComparator QuadComparator}.
	 */
	@SideOnly(Side.CLIENT)
	public static class TriangleComparator implements Comparator<Integer>
	{
		private final float offX;
		private final float offY;
		private final float offZ;
		private final int[] buffer;
		private static final float div = 1.0F / 3.0F;



		public TriangleComparator(int[] buffer, float offX, float offY, float offZ)
		{
			this.buffer = buffer;
			this.offX = offX;
			this.offY = offY;
			this.offZ = offZ;
		}

		@Override
		public int compare(Integer a, Integer b)
		{
			float f0 = Float.intBitsToFloat(buffer[a]) - offX;
			float f1 = Float.intBitsToFloat(buffer[a + 1]) - offY;
			float f2 = Float.intBitsToFloat(buffer[a + 2]) - offZ;

			float f3 = Float.intBitsToFloat(buffer[a + 8]) - offX;
			float f4 = Float.intBitsToFloat(buffer[a + 9]) - offY;
			float f5 = Float.intBitsToFloat(buffer[a + 10]) - offZ;

			float f6 = Float.intBitsToFloat(buffer[a + 16]) - offX;
			float f7 = Float.intBitsToFloat(buffer[a + 17]) - offY;
			float f8 = Float.intBitsToFloat(buffer[a + 18]) - offZ;

			float f12 = Float.intBitsToFloat(buffer[b]) - offX;
			float f13 = Float.intBitsToFloat(buffer[b + 1]) - offY;
			float f14 = Float.intBitsToFloat(buffer[b + 2]) - offZ;

			float f15 = Float.intBitsToFloat(buffer[b + 8]) - offX;
			float f16 = Float.intBitsToFloat(buffer[b + 9]) - offY;
			float f17 = Float.intBitsToFloat(buffer[b + 10]) - offZ;

			float f18 = Float.intBitsToFloat(buffer[b + 16]) - offX;
			float f19 = Float.intBitsToFloat(buffer[b + 17]) - offY;
			float f20 = Float.intBitsToFloat(buffer[b + 18]) - offZ;

			float f24 = (f0 + f3 + f6) * div;
			float f25 = (f1 + f4 + f7) * div;
			float f26 = (f2 + f5 + f8) * div;

			float f27 = (f12 + f15 + f18) * div;
			float f28 = (f13 + f16 + f19) * div;
			float f29 = (f14 + f17 + f20) * div;

			float f30 = f24 * f24 + f25 * f25 + f26 * f26;
			float f31 = f27 * f27 + f28 * f28 + f29 * f29;

			return Float.compare(f31, f30);
		}
	}
}