package zoranodensha.api.vehicles.rendering;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.client.model.obj.Face;
import net.minecraftforge.client.model.obj.GroupObject;
import net.minecraftforge.client.model.obj.TextureCoordinate;
import net.minecraftforge.client.model.obj.Vertex;
import net.minecraftforge.client.model.obj.WavefrontObject;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * Custom wrapper type for wavefront object models, used for quicker rendering.<br>
 * <br>
 * Properties encapsulated within this class (such as {@link FaceSpeedy} and {@link GroupObjectSpeedy}) are fail-fast.
 * This means they crash in any case whenever unexpected behavior occurs.
 * However, such behavior allows for fewer checks during rendering, speeding up the overall process.
 * 
 * @author Leshuwa Kaiheiwa
 */
@SideOnly(Side.CLIENT)
public class ObjModelSpeedy
{
	public final WavefrontObject obj;



	/**
	 * Load a wavefront object model with given name as speedy model.
	 */
	public ObjModelSpeedy(String resourceLocation, String modelName)
	{
		this(new ResourceLocation(resourceLocation, modelName + ".obj"));
	}

	public ObjModelSpeedy(ResourceLocation modelLocation)
	{
		IModelCustom model = AdvancedModelLoader.loadModel(modelLocation);
		if (!(model instanceof WavefrontObject))
		{
			throw new IllegalArgumentException("Specified model was either null or not a valid wavefront object!");
		}
		obj = (WavefrontObject)model;
	}


	/**
	 * Searches for and compiles all object groups from the wrapped model with given names
	 * and returns them as new object list.
	 */
	public ObjectList makeGroup(String... names)
	{
		return new ObjectList(this, names);
	}



	/**
	 * Faster implementation of the {@link net.minecraftforge.client.model.obj.Face Face} class.
	 */
	@SideOnly(Side.CLIENT)
	public static class FaceSpeedy
	{
		public final Vertex[] vertices;
		public final Vertex faceNormal;
		public final TextureCoordinate[] textureCoordinates;
		public final boolean hasTextureCoords;



		/**
		 * Create a speedy face from the given standard wavefront face.
		 */
		public FaceSpeedy(Face face)
		{
			/* Assign vertices and pre-calculate face normal. */
			vertices = face.vertices;
			faceNormal = face.calculateFaceNormal();

			/* Assign texture coordinates. */
			textureCoordinates = face.textureCoordinates;
			hasTextureCoords = (textureCoordinates != null) && (textureCoordinates.length == vertices.length);

			/* If applicable, offset texture U and V accordingly. */
			if (hasTextureCoords)
			{
				float u = 0.0F;
				float v = 0.0F;

				for (TextureCoordinate textureCoordinate : textureCoordinates)
				{
					u += textureCoordinate.u;
					v += textureCoordinate.v;
				}

				float averageU = u / textureCoordinates.length;
				float averageV = v / textureCoordinates.length;

				for (TextureCoordinate textureCoordinate : textureCoordinates)
				{
					float offsetU = 0.0F;
					float offsetV = 0.0F;

					if (textureCoordinate.u > averageU)
					{
						offsetU = -offsetU;
					}
					if (textureCoordinate.v > averageV)
					{
						offsetV = -offsetV;
					}

					textureCoordinate.u += offsetU;
					textureCoordinate.v += offsetV;
				}
			}
		}

		/**
		 * Add this speedy face to render.
		 */
		public void addFaceForRender(Tessellator tessellator)
		{
			tessellator.setNormal(faceNormal.x, faceNormal.y, faceNormal.z);

			if (hasTextureCoords)
			{
				for (int i = 0; i < vertices.length; ++i)
				{
					tessellator.addVertexWithUV(vertices[i].x, vertices[i].y, vertices[i].z, textureCoordinates[i].u, textureCoordinates[i].v);
				}
			}
			else
			{
				for (Vertex vertex : vertices)
				{
					tessellator.addVertex(vertex.x, vertex.y, vertex.z);
				}
			}
		}

		/**
		 * Add this speedy face to render after rotating about
		 * the specified {@link Mat3x3 rotation matrix}.
		 */
		public void addFaceForRender(Tessellator tessellator, Mat3x3 matrix)
		{
			//@formatter:off
			tessellator.setNormal
			(
			 (float)matrix.getX(faceNormal.x, faceNormal.y, faceNormal.z),
			 (float)matrix.getY(faceNormal.x, faceNormal.y, faceNormal.z),
			 (float)matrix.getZ(faceNormal.x, faceNormal.y, faceNormal.z)
					);

			if (hasTextureCoords)
			{
				for (int i = 0; i < vertices.length; ++i)
				{
					tessellator.addVertexWithUV(
					                            (float)matrix.getX(vertices[i].x, vertices[i].y, vertices[i].z),
					                            (float)matrix.getY(vertices[i].x, vertices[i].y, vertices[i].z),
					                            (float)matrix.getZ(vertices[i].x, vertices[i].y, vertices[i].z),
					                            textureCoordinates[i].u,
					                            textureCoordinates[i].v);
				}
			}
			else
			{
				for (Vertex vertex : vertices)
				{
					tessellator.addVertex
					(
					 (float)matrix.getX(vertex.x, vertex.y, vertex.z),
					 (float)matrix.getY(vertex.x, vertex.y, vertex.z),
					 (float)matrix.getZ(vertex.x, vertex.y, vertex.z)
							);
				}
			}
			//@formatter:on
		}
	}


	/**
	 * Faster implementation of the {@link net.minecraftforge.client.model.obj.GroupObject GroupObject} class.
	 */
	@SideOnly(Side.CLIENT)
	public static class GroupObjectSpeedy
	{
		public final FaceSpeedy[] faces;



		/**
		 * Create a speedy group object from the given standard group object.
		 */
		public GroupObjectSpeedy(GroupObject groupObject)
		{
			faces = new FaceSpeedy[groupObject.faces.size()];
			for (int i = 0; i < faces.length; ++i)
			{
				faces[i] = new FaceSpeedy(groupObject.faces.get(i));
			}
		}

		/**
		 * Render this speedy group object.
		 */
		public void render(Tessellator tessellator)
		{
			for (FaceSpeedy face : faces)
			{
				face.addFaceForRender(tessellator);
			}
		}

		/**
		 * Render this speedy group object with given {@link Mat3x3 rotation matrix}.
		 */
		public void render(Tessellator tessellator, Mat3x3 matrix)
		{
			for (FaceSpeedy face : faces)
			{
				face.addFaceForRender(tessellator, matrix);
			}
		}
	}


	/**
	 * A collection of various model objects to render together.
	 */
	@SideOnly(Side.CLIENT)
	public static class ObjectList
	{
		public final GroupObjectSpeedy[] groupObjects;



		/**
		 * Create a new object list of specified speedy group objects.
		 */
		public ObjectList(ObjModelSpeedy model, String... names)
		{
			groupObjects = new GroupObjectSpeedy[names.length];
			for (int i = 0; i < groupObjects.length; ++i)
			{
				GroupObject groupObject = null;
				for (GroupObject obj : model.obj.groupObjects)
				{
					if (obj.name.equalsIgnoreCase(names[i]))
					{
						groupObject = obj;
						break;
					}
				}
				if (groupObject == null)
				{
					throw new IllegalArgumentException("Specified GroupObject wasn't found: " + names[i]);
				}
				groupObjects[i] = new GroupObjectSpeedy(groupObject);
			}
		}

		/**
		 * Render this object list.
		 */
		public void render()
		{
			Tessellator tessellator = Tessellator.instance;
			tessellator.startDrawing(GL11.GL_TRIANGLES);

			for (GroupObjectSpeedy groupObject : groupObjects)
			{
				groupObject.render(tessellator);
			}

			tessellator.draw();
		}

		/**
		 * Render this object list with given {@link net.minecraft.client.renderer.Tessellator Tessellator}.
		 */
		public void render(Tessellator tessellator)
		{
			for (GroupObjectSpeedy groupObject : groupObjects)
			{
				groupObject.render(tessellator);
			}
		}

		/**
		 * Render this object list with given {@link net.minecraft.client.renderer.Tessellator Tessellator}
		 * and {@link Mat3x3 rotation matrix}.
		 */
		public void render(Tessellator tessellator, Mat3x3 matrix)
		{
			for (GroupObjectSpeedy groupObject : groupObjects)
			{
				groupObject.render(tessellator, matrix);
			}
		}
	}


	/**
	 * Rotation matrix used during render calls in {@link ObjModelSpeedy} to apply rotation to tessellated vertices.<br>
	 * <br>
	 * Backed by {@link net.minecraft.util.Vec3 Vec3}.
	 */
	@SideOnly(Side.CLIENT)
	public static class Mat3x3
	{
		private final Vec3 vecX = Vec3.createVectorHelper(1, 0, 0);
		private final Vec3 vecY = Vec3.createVectorHelper(0, 1, 0);
		private final Vec3 vecZ = Vec3.createVectorHelper(0, 0, 1);



		/**
		 * Resets this matrix to original coordinates.
		 */
		public Mat3x3 reset()
		{
			vecX.xCoord = 1;
			vecX.yCoord = 0;
			vecX.zCoord = 0;

			vecY.xCoord = 0;
			vecY.yCoord = 1;
			vecY.zCoord = 0;

			vecZ.xCoord = 0;
			vecZ.yCoord = 0;
			vecZ.zCoord = 1;

			return this;
		}

		/**
		 * Rotate this matrix by the given angle (in degrees) about X-axis.
		 */
		public Mat3x3 rotateX(float angle)
		{
			angle = angle * VehicleHelper.RAD_FACTOR;
			vecX.rotateAroundX(angle);
			vecY.rotateAroundX(angle);
			vecZ.rotateAroundX(angle);
			return this;
		}

		/**
		 * Rotate this matrix by the given angle (in degrees) about Y-axis.
		 */
		public Mat3x3 rotateY(float angle)
		{
			angle = angle * VehicleHelper.RAD_FACTOR;
			vecX.rotateAroundY(angle);
			vecY.rotateAroundY(angle);
			vecZ.rotateAroundY(angle);
			return this;
		}

		/**
		 * Rotate this matrix by the given angle (in degrees) about Z-axis.
		 */
		public Mat3x3 rotateZ(float angle)
		{
			angle = angle * VehicleHelper.RAD_FACTOR;
			vecX.rotateAroundZ(angle);
			vecY.rotateAroundZ(angle);
			vecZ.rotateAroundZ(angle);
			return this;
		}

		/**
		 * Multiplies the given coordinate triplet against this matrix to calculate the X-position after rotation about this matrix.
		 */
		public double getX(double posX, double posY, double posZ)
		{
			return vecX.xCoord * posX + vecY.xCoord * posY + vecZ.xCoord * posZ;
		}

		/**
		 * Multiplies the given coordinate triplet against this matrix to calculate the Y-position after rotation about this matrix.
		 */
		public double getY(double posX, double posY, double posZ)
		{
			return vecX.yCoord * posX + vecY.yCoord * posY + vecZ.yCoord * posZ;
		}

		/**
		 * Multiplies the given coordinate triplet against this matrix to calculate the Z-position after rotation about this matrix.
		 */
		public double getZ(double posX, double posY, double posZ)
		{
			return vecX.zCoord * posX + vecY.zCoord * posY + vecZ.zCoord * posZ;
		}
	}
}