package zoranodensha.api.vehicles.boundingBox;

import net.minecraft.util.AxisAlignedBB;

/**
 * Interface implemented by members of the Zora no Densha train bounding box hierarchy.
 * 
 * @author Leshuwa Kaiheiwa
 */
public interface IBoundingBoxProvider
{
	<T extends AxisAlignedBB & IBoundingBoxUpdateReceiver> T getBoundingBox();
}