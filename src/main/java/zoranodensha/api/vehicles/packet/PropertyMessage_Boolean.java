package zoranodensha.api.vehicles.packet;

import io.netty.buffer.ByteBuf;



public class PropertyMessage_Boolean extends APropertyMessage<Boolean>
{
	public PropertyMessage_Boolean()
	{
	}


	public PropertyMessage_Boolean(int entityID, int vehicleID, int partID, byte key, Boolean newVal)
	{
		super(entityID, vehicleID, partID, key, newVal);
	}

	@Override
	public Boolean fromByteBuf(ByteBuf bbuf)
	{
		return bbuf.readBoolean();
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
		bbuf.writeBoolean(newVal);
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_Boolean>
	{
	}
}
