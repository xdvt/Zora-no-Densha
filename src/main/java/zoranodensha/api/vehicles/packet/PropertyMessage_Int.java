package zoranodensha.api.vehicles.packet;

import io.netty.buffer.ByteBuf;



public class PropertyMessage_Int extends APropertyMessage<Integer>
{
	public PropertyMessage_Int()
	{
	}


	public PropertyMessage_Int(int entityID, int vehicleID, int partID, byte key, Integer newVal)
	{
		super(entityID, vehicleID, partID, key, newVal);
	}

	@Override
	public Integer fromByteBuf(ByteBuf bbuf)
	{
		return bbuf.readInt();
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
		bbuf.writeInt(newVal);
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_Int>
	{
	}
}
