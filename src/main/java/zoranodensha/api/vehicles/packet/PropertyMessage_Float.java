package zoranodensha.api.vehicles.packet;

import io.netty.buffer.ByteBuf;



public class PropertyMessage_Float extends APropertyMessage<Float>
{
	public PropertyMessage_Float()
	{
	}


	public PropertyMessage_Float(int entityID, int vehicleID, int partID, byte key, Float newVal)
	{
		super(entityID, vehicleID, partID, key, newVal);
	}

	@Override
	public Float fromByteBuf(ByteBuf bbuf)
	{
		return bbuf.readFloat();
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
		bbuf.writeFloat(newVal);
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_Float>
	{
	}
}
