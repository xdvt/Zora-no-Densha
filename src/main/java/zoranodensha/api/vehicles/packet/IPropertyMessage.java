package zoranodensha.api.vehicles.packet;

import cpw.mods.fml.common.network.simpleimpl.IMessage;



/**
 * Interface implemented by all messages used to update fields of all vehicle
 * {@link zoranodensha.api.vehicles.part.VehParBase parts} and {@link zoranodensha.api.vehicles.Train trains}.
 *
 * @param <T> The type of value that is sent in this packet.
 * 
 * @author Leshuwa Kaiheiwa
 */
public interface IPropertyMessage<T> extends IMessage
{
	/**
	 * Return the ID of the entity whose value changed.
	 */
	int getEntityID();

	/**
	 * Return the key to the changed value.
	 */
	byte getKey();

	/**
	 * Return the new value.
	 */
	T getNewVal();

	/**
	 * Return the ID of the part whose value changed, or 0 if a value of the parent itself changed.
	 */
	int getPartID();

	/**
	 * Return the part's parent vehicle's identifier.
	 */
	int getVehicleID();
}
