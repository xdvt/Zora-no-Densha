package zoranodensha.api.vehicles.packet;

import java.util.ArrayList;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.util.VehicleHelper;


/**
 * <p>
 * Sent by the server to update a {@link Train train}'s vehicle list.
 * </p>
 * <p>
 * More precisely, updates a train's vehicle list data by moving already existing client-sided instances of
 * {@link zoranodensha.api.vehicles.VehicleData vehicles} to their appropriate (new) parent train.
 * </p>
 */
public class TrainMessage_UpdateVehicles implements IMessage
{
	ArrayList<Object[]> vehicleData;
	int trainID;



	public TrainMessage_UpdateVehicles()
	{
	}

	public TrainMessage_UpdateVehicles(Train train)
	{
		trainID = train.getEntityId();
		vehicleData = new ArrayList<Object[]>();

		for (VehicleData vehicle : train)
		{
			vehicleData.add(new Object[] { vehicle.getID(), vehicle.getIsInverse(), vehicle.getOffset() });
		}
	}

	@Override
	public void fromBytes(ByteBuf bbuf)
	{
		trainID = bbuf.readInt();
		vehicleData = new ArrayList<Object[]>();
		{
			int vehicleCount = bbuf.readInt();
			for (int i = 0; i < vehicleCount; ++i)
			{
				vehicleData.add(new Object[] { bbuf.readInt(), bbuf.readBoolean(), bbuf.readFloat() });
			}
		}
	}

	@Override
	public void toBytes(ByteBuf bbuf)
	{
		bbuf.writeInt(trainID);
		bbuf.writeInt(vehicleData.size());

		for (Object[] obj : vehicleData)
		{
			bbuf.writeInt((Integer)obj[0]);
			bbuf.writeBoolean((Boolean)obj[1]);
			bbuf.writeFloat((Float)obj[2]);
		}
	}



	public static class Handler implements IMessageHandler<TrainMessage_UpdateVehicles, IMessage>
	{
		@Override
		public IMessage onMessage(TrainMessage_UpdateVehicles message, MessageContext context)
		{
			Entity entity = VehicleHelper.getPlayerFromContext(context).worldObj.getEntityByID(message.trainID);
			if (entity instanceof Train)
			{
				Train train = (Train)entity;
				if (train.isEntityAlive())
				{
					/* List of vehicle IDs in the train. */
					ArrayList<Integer> vehicleIDs = new ArrayList<Integer>();

					/* Update the train's vehicle data. */
					for (Object[] obj : message.vehicleData)
					{
						int vehicleID = (Integer)obj[0];
						vehicleIDs.add(vehicleID);
						moveVehicleToTrain(vehicleID, (Boolean)obj[1], (Float)obj[2], train);
					}

					/* Remove any vehicle in the train that's not contained in the vehicle list. */
					ArrayList<VehicleData> vehiclesToRemove = new ArrayList<VehicleData>();
					{
						for (VehicleData vehicle : train)
						{
							if (!vehicleIDs.contains(vehicle.getID()))
							{
								vehiclesToRemove.add(vehicle);
							}
						}

						if (!vehiclesToRemove.isEmpty())
						{
							for (VehicleData vehicleToRemove : vehiclesToRemove)
							{
								train.removeVehicle_Client(vehicleToRemove);
							}
						}
					}
				}
			}

			return null;
		}

		/**
		 * Helper method to move the vehicle specified by the given vehicle ID to the given train.
		 */
		@SideOnly(Side.CLIENT)
		private void moveVehicleToTrain(int vehicleID, boolean isInverse, float offset, Train train)
		{
			if (VehicleData.globalVehicleMap != null)
			{
				VehicleData vehicle = VehicleData.globalVehicleMap.get(vehicleID);
				if (vehicle != null)
				{
					if (vehicle.getTrain() != null)
					{
						vehicle.getTrain().removeVehicle_Client(vehicle);
					}

					vehicle.popChanges();
					vehicle.pushChanges(isInverse, offset);
					train.addVehicle_Client(vehicle);
				}
			}
		}
	}
}
