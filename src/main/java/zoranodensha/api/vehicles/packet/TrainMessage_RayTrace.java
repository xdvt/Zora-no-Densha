package zoranodensha.api.vehicles.packet;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Activatable;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * Message sent by clients to notify the server about intended interaction.<br>
 * <br>
 * The server ray traces the given {@link zoranodensha.api.vehicles.part.util.OrderedPartsList parts list} again
 * and triggers the respective action on a suitable hit candidate.
 */
public class TrainMessage_RayTrace implements IMessage
{
	List<VehParBase> hitCandidates;
	int[][] hitCandidateIDs;
	boolean isRightClick;
	Vec3 pos;
	Vec3 ray;



	public TrainMessage_RayTrace()
	{
	}


	public TrainMessage_RayTrace(boolean isRightClick, List<VehParBase> hitCandidates, Vec3 pos, Vec3 ray)
	{
		this.isRightClick = isRightClick;
		this.hitCandidates = hitCandidates;
		this.pos = pos;
		this.ray = ray;
	}

	@Override
	public void fromBytes(ByteBuf bbuf)
	{
		/* Ray trace data */
		isRightClick = bbuf.readBoolean();
		pos = Vec3.createVectorHelper(bbuf.readDouble(), bbuf.readDouble(), bbuf.readDouble());
		ray = Vec3.createVectorHelper(bbuf.readDouble(), bbuf.readDouble(), bbuf.readDouble());

		/* Candidate data */
		hitCandidateIDs = new int[bbuf.readInt()][3];
		for (int i = 0; i < hitCandidateIDs.length; ++i)
		{
			hitCandidateIDs[i] = new int[] { bbuf.readInt(), bbuf.readInt(), bbuf.readInt() };
		}
	}

	@Override
	public void toBytes(ByteBuf bbuf)
	{
		/* Ray trace data */
		bbuf.writeBoolean(isRightClick);
		bbuf.writeDouble(pos.xCoord);
		bbuf.writeDouble(pos.yCoord);
		bbuf.writeDouble(pos.zCoord);
		bbuf.writeDouble(ray.xCoord);
		bbuf.writeDouble(ray.yCoord);
		bbuf.writeDouble(ray.zCoord);

		/* Candidate data */
		bbuf.writeInt(hitCandidates.size());
		for (VehParBase hitCandidate : hitCandidates)
		{
			bbuf.writeInt(hitCandidate.getTrain().getEntityId());
			bbuf.writeInt(hitCandidate.getVehicle().getID());
			bbuf.writeInt(hitCandidate.getID());
		}
	}



	public static class Handler implements IMessageHandler<TrainMessage_RayTrace, IMessage>
	{
		@Override
		public IMessage onMessage(TrainMessage_RayTrace message, MessageContext context)
		{
			/* Prepare ray trace vector. */
			EntityPlayer player = VehicleHelper.getPlayerFromContext(context);
			ArrayList<VehParBase> partsList = new ArrayList<VehParBase>();

			/*
			 * Read candidate IDs and add them to the list of hit candidates.
			 */
			for (int[] hitCandidateID : message.hitCandidateIDs)
			{
				Entity entity = player.worldObj.getEntityByID(hitCandidateID[0]);
				if (entity instanceof Train)
				{
					Train train = (Train)entity;
					if (train.isDead)
					{
						continue;
					}

					VehParBase part = train.getPartFromID(hitCandidateID[1], hitCandidateID[2]);
					if (part != null)
					{
						/* Skip this part if this is a right-click and the part isn't activatable. */
						if (message.isRightClick)
						{
							boolean isActivatable = false;

							for (APartType type : part.getPartTypes())
							{
								if (type instanceof IPartType_Activatable)
								{
									isActivatable = true;
									break;
								}
							}

							if (!isActivatable)
							{
								continue;
							}
						}

						partsList.add(part);
					}
				}
			}

			/*
			 * If there exist parts, run a ray trace through all parts to determine actual hit candidates.
			 */
			if (partsList.isEmpty())
			{
				return null;
			}

			/* Determine selected part and hit candidates. Abort if there is no selected part. */
			ArrayList<VehParBase> hitCandidates = new ArrayList<VehParBase>();
			VehParBase selectedPart = PartHelper.rayTraceParts(hitCandidates, message.pos, message.ray, null, partsList);
			if (selectedPart == null)
			{
				return null;
			}

			/*
			 * If the player right-clicked, call a part's onActivated() method.
			 */
			if (message.isRightClick)
			{
				/* Ask the selected part first. */
				if (selectedPart.getIsFinished())
				{
					for (APartType type : selectedPart.getPartTypes())
					{
						if (type instanceof IPartType_Activatable)
						{
							if (((IPartType_Activatable)type).onActivated(player))
							{
								return null;
							}
						}
					}
				}

				/* If the selected part hasn't indicated any change, consider hit candidates. */
				if (!hitCandidates.isEmpty())
				{
					for (VehParBase iteratedPart : hitCandidates)
					{
						if (!iteratedPart.getIsFinished())
						{
							continue;
						}

						for (APartType type : iteratedPart.getPartTypes())
						{
							if (type instanceof IPartType_Activatable)
							{
								if (((IPartType_Activatable)type).onActivated(player))
								{
									return null;
								}
							}
						}
					}
				}
			}

			/*
			 * Otherwise attack train.
			 */
			else
			{
				player.attackTargetEntityWithCurrentItem(selectedPart.getTrain());
			}

			return null;
		}
	}
}
