package zoranodensha.api.vehicles.part.xml;

/**
 * Names of all tags required to be specified in every XML part file.
 * 
 * @author Leshuwa Kaiheiwa
 */
public enum EDefaultTags
{
	/** Total height of the vehicle part, in meters. */
	HEIGHT("height"),
	/** Vehicle part mass, in tonnes (1t = 1000kg). */
	MASS("mass"),
	/** Local path (originating from file root) pointing to the part's model file. */
	MODEL_PATH("modelPath"),
	/** The part's unique name. */
	NAME("name"),
	/** Local path (originating from file root) pointing to the part texture file. */
	TEXTURE_PATH("texturePath"),
	/** Recipe matrix containing <b>exactly</b> nine {@code <item>} tags. One or more of these must have a valid value specified. */
	RECIPE("recipe"),
	/** Renderer node specifying part rendering. */
	RENDERER("renderer"),
	/** Total width of the vehicle part, in meters. */
	WIDTH("width");

	/** XML tag name. */
	public final String tagName;



	EDefaultTags(String tagName)
	{
		this.tagName = tagName;
	}

	/**
	 * Determines whether the given node is valid for this default tag type.
	 * 
	 * @param node - {@link zoranodensha.api.vehicles.part.xml.XMLTreeNode Node} to check for validity.
	 * @return {@code true} if the given node is valid for this tag type.
	 */
	public boolean getIsNodeValid(XMLTreeNode node)
	{
		if (node == null)
		{
			return false;
		}

		switch (this)
		{
			default:
				return !node.getValue().isEmpty();
				
			case RECIPE:
				return node.getChildren().size() == 9;

			case RENDERER:
				return !node.getChildren().isEmpty();
		}
	}
}