package zoranodensha.api.vehicles.part.xml;

import java.util.ArrayList;



/**
 * Structure holding an XML document's nodes.<br>
 * <br>
 * In this context, we evaluate<br>
 * {@code <tagName value="SomeString" />}<br>
 * and<br>
 * {@code <tagName> <value>SomeString</value> </tagName>}<br>
 * equally; both {@code <value>} tags are represented as nodes without children.<br>
 * <br>
 * Whenever a node field (either {@link #name} or {@link #value}) has been completed during parsing,
 * it will be <i>locked</i> using {@link #lockName()} or {@link #lockValue()}, respectively.<br>
 * The respective field then rejects any following additions to its content.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class XMLTreeNode
{
	private static final String INVALID_NAME_PREFIXES = "-.";
	private static final String INVALID_NAME_CHARACTERS = "!\"#$%&'()*+,/;<=>?@[\\]^`´{|}~";

	/** Name of this node. */
	private String name = "";
	/** Value of this node. */
	private String value = "";
	/** Once {@code true}, will reject any further name input. */
	private boolean lockName = false;
	/** Once {@code true}, will reject any further value input. */
	private boolean lockValue = false;
	/** This node's parent. May be {@code null}. */
	private XMLTreeNode parent;
	/** This node's children. */
	private final ArrayList<XMLTreeNode> children = new ArrayList<XMLTreeNode>();



	/**
	 * Add the given node as a child to this node.<br>
	 * Automatically assigns this node as the child's parent.
	 * 
	 * @param node - Child node to add.
	 */
	public void addChild(XMLTreeNode node)
	{
		node.parent = this;
		children.add(node);
	}

	/**
	 * Appends the given character code point to this node's name.<br>
	 * Runs checks whether the given code point is valid before appending.
	 * 
	 * @param codePoint - The code point of the character to append.
	 * @return {@code true} if the given code point was valid.
	 */
	public boolean addToName(int codePoint)
	{
		/* Reject input if locked. */
		if (lockName)
		{
			return false;
		}

		/* Suffix to append. */
		final String s = String.copyValueOf(Character.toChars(codePoint));

		/* If the name is empty, it may not start with dash, dot, or a number. */
		if (name.isEmpty())
		{
			if (INVALID_NAME_PREFIXES.contains(s) || Character.isDigit(s.codePointAt(0)))
			{
				return false;
			}
		}

		/* Ensure each character is generally valid. */
		if (INVALID_NAME_CHARACTERS.contains(s) || Character.isWhitespace(codePoint))
		{
			return false;
		}

		/* Append if valid. */
		name += s;
		return true;
	}

	/**
	 * Appends the given code point to this node's value.<br>
	 * Checks whether the given code point is valid, beforehand.
	 * 
	 * @param codePoint - The code point of the character to append.
	 * @return {@code true} if the given code point was valid.
	 */
	public boolean addToValue(int codePoint)
	{
		/* Reject input if locked. */
		if (lockValue)
		{
			return false;
		}

		/* Suffix to append. */
		final String s = String.copyValueOf(Character.toChars(codePoint));

		/* Append if valid. */
		value += s;
		return true;
	}

	/**
	 * Retrieves a child node for the given name.<br>
	 * More formally, returns the first node with given name that is a <i>direct</i> child of this node.
	 * 
	 * @return The child {@link XMLTreeNode node} for the specified name, or {@code null} if no such node exists.
	 */
	public XMLTreeNode getChildByName(String name)
	{
		if (name != null && !name.isEmpty())
		{
			for (XMLTreeNode child : getChildren())
			{
				if (name.equals(child.getName()))
				{
					return child;
				}
			}
		}
		return null;
	}

	/**
	 * Returns all children of this node.
	 */
	public ArrayList<XMLTreeNode> getChildren()
	{
		return children;
	}

	/**
	 * Returns the name of this node.
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Returns this node's parent node.
	 */
	public XMLTreeNode getParent()
	{
		return parent;
	}

	/**
	 * Return the value of this node.
	 */
	public String getValue()
	{
		return value;
	}

	/**
	 * Tries to return the {@link #value} of this tree node as a parsed double.
	 */
	public double getValueAsDouble()
	{
		return Double.parseDouble(getValue());
	}

	/**
	 * Returns whether the name is locked, i.e. whether calls to {@link #addToName(int)} automatically fail.
	 */
	public boolean isNameLocked()
	{
		return lockName;
	}

	/**
	 * Returns whether the value is locked, i.e. whether calls to {@link #addToValue(int)} automatically fail.
	 */
	public boolean isValueLocked()
	{
		return lockValue;
	}

	/**
	 * Locks name and value of this node as well as of all its child nodes.
	 */
	public void lockAll()
	{
		lockName();
		lockValue();

		for (XMLTreeNode child : children)
		{
			child.lockAll();
		}
	}

	/**
	 * Lock the {@link #name} field. It rejects any further input to {@link #addToName(int)} from now on.
	 */
	public void lockName()
	{
		lockName = true;
		name = name.trim();
	}

	/**
	 * Lock the {@link #value} field. It rejects any further input to {@link #addToValue(int)} from now on.
	 */
	public void lockValue()
	{
		lockValue = true;
		value = value.trim();
	}

	@Override
	public String toString()
	{
		if (getChildren().isEmpty())
		{
			return String.format("[n=%s, v=%s]", name, value);
		}

		String childString = "";
		for (XMLTreeNode child : getChildren())
		{
			if (!childString.isEmpty())
			{
				childString += ",\n";
			}
			childString += child.toString();
		}
		return String.format("[n=%s, v=%s]\n{\n%s\n}", name, value, childString);
	}
}