package zoranodensha.api.vehicles.part.xml;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.google.common.collect.ImmutableSet;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import javax.annotation.Nonnull;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.audio.SoundCategory;
import net.minecraft.client.resources.IResourcePack;
import net.minecraft.client.resources.data.IMetadataSection;
import net.minecraft.client.resources.data.IMetadataSerializer;
import net.minecraft.util.ResourceLocation;



/**
 * Resource pack holding XML part data, such as models and textures.
 * 
 * @author Leshuwa Kaiheiwa
 */
@SideOnly(Side.CLIENT)
public class XMLResourcePack implements IResourcePack
{
	/** Static instance of this resource pack. Used for access while parsing parts. */
	public static final XMLResourcePack INSTANCE = new XMLResourcePack();

	/** Cache containing previously loaded resource locations and their respective content. Used to avoid loading the same resource multiple times. */
	private static final HashMap<String, ResourceLocation> resourceLocationCache = new HashMap<String, ResourceLocation>();
	/** Mapping from language keys to a list of ZIP files' respective language file. */
	private static final HashMap<String, ArrayList<ResourceLocation>> langFileLocationMap = new HashMap<String, ArrayList<ResourceLocation>>();
	/** Set containing default resource domains. */
	private static final Set<?> defaultResourceDomains = ImmutableSet.of(INSTANCE.getPackName());
	/** Emulated JSON file containing all registered sounds. */
	private static final SoundsFile soundsFile = new SoundsFile();

	/** Splitter String used to divide a resource location's path into ZIP-file path and ZIP-entry path. */
	private static final String ZIP_LOC_SPLITTER = ".zip:";
	/** Language files' file extension. */
	private static final String LANG_FILE_POSTFIX = ".lang";
	/** Language file directory in ZIP-files for XML parts, starting at the ZIP-file's root. */
	private static final String LANG_FILE_PATH = "lang/";
	/** File name for the sounds file containing all vehicle part sounds as {@code json} file type. */
	private static final String SOUNDS_JSON = "sounds.json";



	/**
	 * Add the given language file to all known language files.
	 * 
	 * @param lang - The language of the added file, such as {@code en_US} or {@code de_DE}.
	 * @param langFileLocation - {@link net.minecraft.util.ResourceLocation Resource location} pointing to the language file inside of its ZIP file.
	 */
	public void addLanguageFile(String lang, ResourceLocation langFileLocation)
	{
		if (langFileLocationMap.containsKey(lang))
		{
			langFileLocationMap.get(lang).add(langFileLocation);
		}
		else
		{
			ArrayList<ResourceLocation> list = new ArrayList<ResourceLocation>();
			list.add(langFileLocation);
			langFileLocationMap.put(lang, list);
		}
	}

	/**
	 * Returns an input stream if this resource pack recognises the specified resource location.
	 * 
	 * @param resLoc - {@link net.minecraft.util.ResourceLocation Resource location} to retrieve an input stream for.
	 * @return {@link java.io.InputStream Input stream} for the specified resource, or {@code null} if the resource wasn't recognised.
	 * @throws IOException If either IO operation fails.
	 */
	@Override
    public InputStream getInputStream(ResourceLocation resLoc) throws IOException
	{
		/*
		 * The given resource location is pointing to the sounds file.
		 */
		if (SOUNDS_JSON.equals(resLoc.getResourcePath()))
		{
			return soundsFile.toInputStream();
		}

		ResourceLocation cleanSoundLocation = cleanSoundLocation(resLoc);
		if (soundsFile.isRegistered(cleanSoundLocation))
		{
			resLoc = cleanSoundLocation;
		}

		/*
		 * If the given resource location is pointing to a language file, try to get and load all corresponding language files.
		 */
		if (resLoc.getResourcePath().startsWith(LANG_FILE_PATH) && resLoc.getResourcePath().endsWith(LANG_FILE_POSTFIX))
		{
			String lang = resLoc.getResourcePath();
			lang = lang.substring(lang.lastIndexOf("/") + 1, lang.lastIndexOf(LANG_FILE_POSTFIX));

			ArrayList<ResourceLocation> langFiles = langFileLocationMap.get(lang);
			if (langFiles != null && !langFiles.isEmpty())
			{
				return new InputStreamSequence(langFiles);
			}
		}

		/*
		 * Otherwise try to return the referenced ZIP entry's InputStream.
		 */
		ZipFile zipFile = getZipFile(resLoc);
		if (zipFile != null)
		{
			ZipEntry zipEntry = zipFile.getEntry(resLoc.getResourcePath().split(ZIP_LOC_SPLITTER)[1]);
			if (zipEntry != null)
			{
				return zipFile.getInputStream(zipEntry);
			}
		}

		/* Return null if nothing succeeded. */
		return null;
	}

	@Override
	public BufferedImage getPackImage() {
		return null;
	}

	@Override
	public IMetadataSection getPackMetadata(IMetadataSerializer metadataSerialiser, String metadataSection) {
		return null;
	}

	/**
	 * Resource pack name is the name of this class.
	 */
	@Override
	public String getPackName()
	{
		return XMLResourcePack.class.getSimpleName().toLowerCase();
	}

	/**
	 * Return all resource domains recognised by this resource pack as a {@link java.util.Set Set}.
	 */
	@Override
	public Set<?> getResourceDomains()
	{
		return defaultResourceDomains;
	}

	/**
	 * Tries to find a ZIP file as specified by the given resource location.
	 * 
	 * @param resLoc - {@link net.minecraft.util.ResourceLocation Resource location} to retrieve the file from.
	 * @return The requested {@link java.util.zip.ZipFile ZIP file}, or {@code null} if the request couldn't be fulfilled.
	 */
	private ZipFile getZipFile(ResourceLocation resLoc)
	{
		String[] p = resLoc.getResourcePath().split(ZIP_LOC_SPLITTER);
		if (p.length == 2)
		{
			File file = new File(p[0] + ".zip");
			if (file.exists() && !file.isDirectory())
			{
				try
				{
					return new ZipFile(file);
				}
				catch (Exception e)
				{
					/* Silent catch. */
				}
			}
		}
		return null;
	}

	/**
	 * Loads a resource location from the given ZIP archive at the specified path, relative to the archive's root.<br>
	 * This operation does <b>not</b> close the supplied ZIP file after completion.
	 * 
	 * @param zipFile - {@link java.util.zip.ZipFile ZIP-file} to load the resource from.
	 * @param path - Resource path within the ZIP file, relative to the archive's root.
	 * @return {@link net.minecraft.util.ResourceLocation Resource location} pointing to the specified ZIP archive's resource.
	 * 
	 * @see #loadSound(ZipFile, String)
	 */
	public ResourceLocation loadResource(ZipFile zipFile, String path)
	{
		ZipEntry zipEntry = zipFile.getEntry(path);
		if (zipEntry != null && !zipEntry.isDirectory())
		{
			String key = zipFile.getName() + ":" + zipEntry.getName();
			ResourceLocation resLoc = resourceLocationCache.get(key);

			if (resLoc == null)
			{
				resLoc = new ResourceLocation(getPackName(), key);
				resourceLocationCache.put(key, resLoc);
			}

			return resLoc;
		}
		return null;
	}

	/**
	 * Loads a sound file from the given ZIP archive at the specified path, relative to the archive's root.<br>
	 * Also {@link SoundsFile#registerSound(ResourceLocation) registers} the loaded sound file to this resource pack's {@code sounds.json} dummy so it can be properly loaded by Minecraft.<br>
	 * This operation does <b>not</b> close the supplied ZIP file after completion.
	 * 
	 * @param zipFile - {@link java.util.zip.ZipFile ZIP-file} to load the sound file from.
	 * @param path - Sound file path within the ZIP file, relative to the archive's root.
	 * @return {@link net.minecraft.util.ResourceLocation Resource location} pointing to the specified ZIP archive's sound file.
	 * 
	 * @see #loadResource(ZipFile, String)
	 */
	public ResourceLocation loadSound(ZipFile zipFile, String path)
	{
		ResourceLocation resLoc = loadResource(zipFile, path);
		if (resLoc != null)
		{
			soundsFile.registerSound(resLoc);
		}
		return resLoc;
	}

	@Override
	public boolean resourceExists(ResourceLocation resLoc)
	{
		/*
		 * Check whether the given resource is a sound file.
		 */
		if (SOUNDS_JSON.equals(resLoc.getResourcePath()) || soundsFile.isRegistered(cleanSoundLocation(resLoc)))
		{
			return true;
		}

		/*
		 * If the given resource location is pointing to a language file, check whether the language map has a corresponding value stored.
		 */
		if (resLoc.getResourcePath().startsWith(LANG_FILE_PATH))
		{
			String lang = resLoc.getResourcePath();
			lang = lang.substring(lang.lastIndexOf("/") + 1, lang.lastIndexOf(LANG_FILE_POSTFIX));

			ArrayList<ResourceLocation> langFiles = langFileLocationMap.get(lang);
			return (langFiles != null && !langFiles.isEmpty());
		}

		/*
		 * Otherwise return whether the referenced ZIP entry can be parsed.
		 */
		ZipFile zipFile = getZipFile(resLoc);
		if (zipFile != null)
		{
			try
			{
				return zipFile.getEntry(resLoc.getResourcePath().split(ZIP_LOC_SPLITTER)[1]) != null;
			}
			finally
			{
				try
				{
					zipFile.close();
				}
				catch (IOException e)
				{
					;
				}
			}
		}
		return false;
	}

	/**
	 * Append the static instance of this resource pack to the given list.
	 */
	public static void addToList(List<IResourcePack> list)
	{
		list.add(INSTANCE);
	}

	/**
	 * Helper method to create a new resource location containing the given resource location's domain and its path,
	 * cleaned from Minecraft's {@code sounds/} prefix and potentially doubled {@code .ogg} suffix.
	 */
	public static ResourceLocation cleanSoundLocation(ResourceLocation resLoc)
	{
		String resPath = resLoc.getResourcePath();
		if (resPath.startsWith("sounds/"))
		{
			resPath = resPath.substring("sounds/".length());
		}

		int lastIndexOfSuffix = resPath.lastIndexOf(".ogg");
		if (lastIndexOfSuffix != resPath.indexOf(".ogg"))
		{
			resPath = resPath.substring(0, lastIndexOfSuffix);
		}

		return new ResourceLocation(resLoc.getResourceDomain(), resPath);
	}



	/**
	 * <p>
	 * Input Stream type which iterates over a list of {@link net.minecraft.util.ResourceLocation resource locations},
	 * creating and reading an input stream of the current element until there is none left.
	 * </p>
	 * 
	 * <p>
	 * Code heavily based off {@link java.io.SequenceInputStream}, thus no copyright claims are made on this class.
	 * </p>
	 * 
	 * @author Leshuwa Kaiheiwa
	 */
	private static class InputStreamSequence extends InputStream
	{
		/** List of resource locations which are read from. */
		private final ArrayList<ResourceLocation> resLocs;

		/** The current list index of {@link #resLocs}. */
		private int currentIndex;
		/** Current input stream that is read. May be {@code null} while initialising this instance. */
		private InputStream currentInputStream;



		/**
		 * Creates a new input stream sequence for the given list of resource locations.
		 * 
		 * @param resLocs - List of {@link net.minecraft.util.ResourceLocation resource locations} read by this input stream.
		 * @throws NullPointerException If {@code resLocs} is {@code null}.
		 * @throws IllegalArgumentException If {@code resLocs} is empty.
		 */
		public InputStreamSequence(ArrayList<ResourceLocation> resLocs)
		{
			if (resLocs == null)
			{
				throw new NullPointerException("Null resource location list!");
			}
			else if (resLocs.isEmpty())
			{
				throw new IllegalArgumentException("Empty resource location list!");
			}

			/* Create a shallow copy of the given list to avoid race conditions while iterating. */
			this.resLocs = new ArrayList<ResourceLocation>(resLocs);
			this.currentIndex = -1;
			try
			{
				nextStream();
			}
			catch (IOException e)
			{
				throw new RuntimeException("Failed to fetch next InputStream.");
			}
		}

		@Override
		public int available() throws IOException
		{
			return (currentInputStream == null) ? 0 : currentInputStream.available();
		}

		@Override
		public void close() throws IOException
		{
			do
			{
				nextStream();
			}
			while (currentInputStream != null);
		}

		/**
		 * Grabs the next available input stream from the list.
		 */
		private void nextStream() throws IOException
		{
			if (currentInputStream != null)
			{
				currentInputStream.close();
			}

			++currentIndex;
			if (currentIndex < resLocs.size())
			{
				currentInputStream = XMLResourcePack.INSTANCE.getInputStream(resLocs.get(currentIndex));
				if (currentInputStream == null)
				{
					throw new NullPointerException("Failed to retrieve InputStream from XMLResourcePack.");
				}
			}
			else
			{
				currentInputStream = null;
			}
		}

		@Override
		public int read() throws IOException
		{
			while (currentInputStream != null)
			{
				int c = currentInputStream.read();
				if (c != 1)
				{
					return c;
				}
				nextStream();
			}
			return -1;
		}

		@Override
		public int read(@Nonnull  byte[] b, int off, int len) throws IOException
		{
			if (currentInputStream == null)
			{
				return -1;
			}
			else if (b == null)
			{
				throw new NullPointerException("Specified byte array was null!");
			}
			else if (off < 0 || len < 0 || len > b.length - off)
			{
				throw new IndexOutOfBoundsException();
			}
			else if (len == 0)
			{
				return 0;
			}

			do
			{
				int n = currentInputStream.read(b, off, len);
				if (n > 0)
				{
					return n;
				}
				nextStream();
			}
			while (currentInputStream != null);

			return -1;
		}
	}

	/**
	 * <p>
	 * Helper class containing sound resources loaded by the {@link XMLResourcePack}.
	 * </p>
	 * 
	 * @author Leshuwa Kaiheiwa
	 */
	private static final class SoundsFile
	{
		/** Resource locations of each registered sound. */
		private static final ArrayList<ResourceLocation> soundLocations = new ArrayList<ResourceLocation>();
		/** The sound file's JSON representation. */
		private static final JsonObject soundJson = new JsonObject();



		/**
		 * Determines whether the given resource location is registered to this sounds file.
		 * 
		 * @param resLoc - {@link net.minecraft.util.ResourceLocation Resource location} to check for.
		 * @return {@code true} if the given resource location is registered.
		 */
		public boolean isRegistered(ResourceLocation resLoc)
		{
			return soundLocations.contains(resLoc);
		}

		/**
		 * Register the given resource location as a sound to this sounds file.
		 * 
		 * @param resLoc - {@link net.minecraft.util.ResourceLocation Resource location} to register.
		 */
		public void registerSound(ResourceLocation resLoc)
		{
			/* Create entry in the JSON representation. */
			JsonObject root = new JsonObject();
			{
				JsonPrimitive category = new JsonPrimitive(SoundCategory.AMBIENT.getCategoryName());
				JsonArray sounds = new JsonArray();
				{
					JsonObject sound = new JsonObject();
					{
						sound.add("name", new JsonPrimitive(resLoc.toString()));
						sound.add("stream", new JsonPrimitive(false));
					}

					sounds.add(sound);
				}

				root.add("category", category);
				root.add("sounds", sounds);
			}

			/* Finally, add sound JSON and -location. */
			soundJson.add(resLoc.getResourcePath(), root);
			soundLocations.add(resLoc);
		}

		/**
		 * Returns all registered sounds formatted as a JSON input stream.
		 * 
		 * @return Sound data as an {@link java.io.InputStream input stream}.
		 */
		public InputStream toInputStream()
		{
			return new ByteArrayInputStream(soundJson.toString().getBytes());
		}
	}
}