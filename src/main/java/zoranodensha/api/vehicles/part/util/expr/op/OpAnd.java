package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;

public class OpAnd extends AOperator
{
	public OpAnd(IExpressionComponent[] arguments)
	{
		super(arguments);
	}

	@Override
	protected double evaluate()
	{
		return getIsTrue() ? 1 : 0;
	}

	@Override
	public boolean getIsTrue()
	{
		return (arguments[0].getIsTrue() && arguments[1].getIsTrue());
	}

	@Override
	public String toString()
	{
		String arg0 = (arguments[0] instanceof AOperator) ? "(%s)" : "%s";
		String arg1 = (arguments[1] instanceof AOperator) ? "(%s)" : "%s";
		return String.format("%s AND %s", String.format(arg0, arguments[0]), String.format(arg1, arguments[1]));
	}
}