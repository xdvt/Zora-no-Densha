package zoranodensha.api.vehicles.part.util.expr.ref;

import zoranodensha.api.vehicles.part.property.PropLong;



/**
 * Reference to a {@link zoranodensha.api.vehicles.part.property.PropLong Long property}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class RefLong extends AReference
{
	/** The referenced {@link zoranodensha.api.vehicles.part.property.PropLong property}. */
	private final PropLong reference;



	public RefLong(PropLong reference)
	{
		this.reference = reference;
	}

	@Override
	public double getValue()
	{
		return reference.get();
	}
}