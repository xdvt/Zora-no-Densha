package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;

public class OpNot extends AOperator
{
	public OpNot(IExpressionComponent[] arguments)
	{
		super(arguments);
	}

	@Override
	protected double evaluate()
	{
		return getIsTrue() ? 1 : 0;
	}
	
	@Override
	public boolean getIsTrue()
	{
		return !arguments[0].getIsTrue();
	}

	@Override
	public String toString()
	{
		return String.format("NOT (%s)", arguments[0]);
	}
}