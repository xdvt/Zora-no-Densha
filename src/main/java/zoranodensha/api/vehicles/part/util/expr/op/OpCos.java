package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;

public class OpCos extends AOperator
{
	public OpCos(IExpressionComponent[] arguments)
	{
		super(arguments);
	}

	@Override
	protected double evaluate()
	{
		return Math.cos(arguments[0].getValue());
	}
	
	@Override
	public String toString()
	{
		return String.format("cos(%s)", arguments[0]);
	}
}