package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;

public class OpNegate extends AOperator
{
	public OpNegate(IExpressionComponent[] arguments)
	{
		super(arguments);
	}

	@Override
	protected double evaluate()
	{
		return -arguments[0].getValue();
	}
	
	@Override
	public String toString()
	{
		return String.format("-(%s)", arguments[0]);
	}
}