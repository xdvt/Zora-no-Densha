package zoranodensha.api.vehicles.part.util;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector;



/**
 * <p>
 * List implementation which concatenates lists of {@link zoranodensha.api.vehicles.part.util.OrderedPartsList vehicle parts}.<br>
 * Order of elements corresponds to the order in the parent train's {@link zoranodensha.api.vehicles.VehicleList vehicle list}.
 * </p>
 *
 * <p>
 * Note:<br>
 * This list creates a private copy of the vehicle list of the train it initially belonged to.
 * Modifications of the parent train's vehicle list are not reflected in this list's iteration behaviour.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class ConcatenatedPartsList extends AbstractList<VehParBase>
{
	/** List of {@link zoranodensha.api.vehicles.VehicleData vehicles} whose parts are referenced by this list. */
	private final ArrayList<VehicleData> vehicles;
	/** {@link zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector Selector} to choose which {@link zoranodensha.api.vehicles.part.VehParBase parts} are in this list. */
	private final IPartsSelector selector;
	/** Number of total elements in this parts list. We assume this list to be trashed if either child list is modified. */
	private final int totalSize;



	/**
	 * Create a concatenated parts list.
	 * 
	 * @param train - The {@link zoranodensha.api.vehicles.Train train} whose {@link #vehicles} are going to be iterated over.
	 * @param selector - The {@link #selector} that defines which parts are contained in this list.
	 */
	public ConcatenatedPartsList(Train train, IPartsSelector selector)
	{
		this.vehicles = train.getVehicles();
		this.selector = selector;

		int size = 0;
		for (VehicleData vehicle : vehicles)
		{
			size += vehicle.getParts(selector).size();
		}

		this.totalSize = size;
	}

	@Override
	public VehParBase get(int index)
	{
		if (index >= 0 && index < size())
		{
			for (VehicleData vehicle : vehicles)
			{
				OrderedPartsList partsListVehicle = vehicle.getParts(selector);
				if (index >= partsListVehicle.size())
				{
					index -= partsListVehicle.size();
				}
				else
				{
					return partsListVehicle.get(index);
				}
			}
		}

		throw new IndexOutOfBoundsException("Index: " + index + ", Size:" + size());
	}

	/**
	 * Searches this list for the part closest to the specified local X coordinate.
	 * 
	 * @param localX - Local X-coordinate used as pivot point.
	 * @return The closest part in this list, or {@code null} if this list is empty.
	 */
	public VehParBase getClosestPart(float localX)
	{
		Iterator<VehicleData> itera = vehicles.iterator();
		VehicleData vehicle;

		/* For each vehicle in the train.. */
		while (itera.hasNext())
		{
			vehicle = itera.next();

			/*
			 * ..consider its parts if the last part's local offset is greater than the given local offset,
			 * or if there are no more vehicles in this list.
			 */
			if (vehicle.getPartAt(vehicle.getPartCount() - 1).getOffset()[0] >= localX || !itera.hasNext())
			{
				return vehicle.getParts(selector).getClosestPart(localX);
			}
		}

		return null;
	}

	/**
	 * Determines the part whose in-world position is closest to the given coordinate triplet.
	 * 
	 * @return The {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} closest to the specified position, or {@code null} if this list is empty.
	 */
	public VehParBase getClosestPart(double posX, double posY, double posZ)
	{
		/* Return null if this list is empty. */
		if (isEmpty())
		{
			return null;
		}

		/* Prepare iteration. */
		VehParBase closest = null;
		double minDistSq = Double.MAX_VALUE;
		double curDistSq;

		/* Iterate over all parts in this list. */
		for (VehParBase part : this)
		{
			/* If the current part is closer to the position than the previous part, save its reference. */
			curDistSq = PartHelper.getPosition(part).squareDistanceTo(posX, posY, posZ);
			if (curDistSq < minDistSq)
			{
				closest = part;
				minDistSq = curDistSq;
			}
		}

		/* Return the closest part. */
		return closest;
	}

	@Override
	public Iterator<VehParBase> iterator()
	{
		return new Itera();
	}

	@Override
	public ListIterator<VehParBase> listIterator()
	{
		return new ListItera();
	}

	@Override
	public int size()
	{
		return totalSize;
	}



	private class Itera implements Iterator<VehParBase>
	{
		private Iterator<VehicleData> vehiclesItera = vehicles.iterator();
		private Iterator<VehParBase> partsListItera;



		public Itera()
		{
			nextPartsList();
		}


		@Override
		public boolean hasNext()
		{
			return (partsListItera != null) && partsListItera.hasNext();
		}

		@Override
		public VehParBase next()
		{
			/*
			 * Just call for the next parts list.
			 * If the current iterator has remaining elements, it won't be skipped.
			 */
			VehParBase elem = partsListItera.next();
			nextPartsList();
			return elem;
		}

		/**
		 * Prepare the next iterator over a vehicle parts list. Skip empty iterators.
		 */
		private void nextPartsList()
		{
			while (vehiclesItera.hasNext() && (partsListItera == null || !partsListItera.hasNext()))
			{
				partsListItera = vehiclesItera.next().getParts(selector).iterator();
			}
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}

	private class ListItera implements ListIterator<VehParBase>
	{
		private ListIterator<VehicleData> vehiclesItera = vehicles.listIterator();
		private ListIterator<VehParBase> partsListItera;
		private int currentTotalIndex = 0;



		@Override
		public void add(VehParBase e)
		{
			throw new UnsupportedOperationException("add");
		}

		@Override
		public boolean hasNext()
		{
			return (partsListItera != null) && partsListItera.hasNext();
		}

		@Override
		public boolean hasPrevious()
		{
			return (partsListItera != null) && partsListItera.hasPrevious();
		}

		@Override
		public VehParBase next()
		{
			/*
			 * Just call for the next parts list.
			 * If the current iterator has remaining elements, it won't be skipped.
			 */
			VehParBase elem = partsListItera.next();
			nextPartsList();
			++currentTotalIndex;
			return elem;
		}

		@Override
		public int nextIndex()
		{
			return currentTotalIndex;
		}

		/**
		 * Prepare the next iterator over a vehicle parts list. Skip empty iterators.
		 */
		private void nextPartsList()
		{
			while (vehiclesItera.hasNext() && (partsListItera == null || !partsListItera.hasNext()))
			{
				partsListItera = vehiclesItera.next().getParts(selector).listIterator();
			}
		}

		@Override
		public VehParBase previous()
		{
			/*
			 * Just call for the previous parts list.
			 * If the current iterator has remaining elements, it won't be skipped.
			 */
			VehParBase elem = partsListItera.previous();
			previousPartsList();
			--currentTotalIndex;
			return elem;
		}

		@Override
		public int previousIndex()
		{
			return currentTotalIndex - 1;
		}

		/**
		 * Prepare the next iterator over a vehicle parts list. Skip empty iterators.
		 */
		private void previousPartsList()
		{
			while (vehiclesItera.hasPrevious() && (partsListItera == null || !partsListItera.hasPrevious()))
			{
				partsListItera = vehiclesItera.previous().getParts(selector).listIterator();
			}
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}

		@Override
		public void set(VehParBase e)
		{
			throw new UnsupportedOperationException("set");
		}
	}
}