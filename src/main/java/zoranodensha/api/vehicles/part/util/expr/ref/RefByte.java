package zoranodensha.api.vehicles.part.util.expr.ref;

import zoranodensha.api.vehicles.part.property.PropByte;



/**
 * Reference to a {@link zoranodensha.api.vehicles.part.property.PropByte Byte property}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class RefByte extends AReference
{
	/** The referenced {@link zoranodensha.api.vehicles.part.property.PropByte property}. */
	private final PropByte reference;



	public RefByte(PropByte reference)
	{
		this.reference = reference;
	}

	@Override
	public double getValue()
	{
		return reference.get();
	}
}