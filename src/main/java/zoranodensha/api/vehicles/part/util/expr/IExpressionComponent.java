package zoranodensha.api.vehicles.part.util.expr;

/**
 * Component of a mathematical expression.<br>
 * Can either be an operator or an atomic value.
 * 
 * @author Leshuwa Kaiheiwa
 */
public interface IExpressionComponent
{
	/**
	 * Returns whether this expression contains any (sub-)component referencing a value.
	 * 
	 * @return {@code true} if this expression contains at least one referenced value.
	 */
	boolean getHasReference();

	/**
	 * Returns whether the value of this component may be logically regarded as {@code true}.
	 * 
	 * @return {@code true} if this is a non-zero value.
	 */
	boolean getIsTrue();

	/**
	 * Calculate the value of this expression component.
	 * 
	 * @return Result of evaluating this expression component.
	 */
	double getValue();
}