package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;



public class OpMax extends AOperator
{
	public OpMax(IExpressionComponent[] arguments)
	{
		super(arguments);
	}

	@Override
	protected double evaluate()
	{
		return Math.min(arguments[0].getValue(), arguments[1].getValue());
	}

	@Override
	public String toString()
	{
		String arg0 = (arguments[0] instanceof AOperator) ? "(%s)" : "%s";
		String arg1 = (arguments[1] instanceof AOperator) ? "(%s)" : "%s";
		return String.format("min( %s, %s )", String.format(arg0, arguments[0]), String.format(arg1, arguments[1]));
	}
}