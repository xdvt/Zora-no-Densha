package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;

/**
 * Abstract base class for operators in a mathematical expression.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class AOperator implements IExpressionComponent
{
	/** Array of expression {@link zoranodensha.api.vehicles.part.util.expr.IExpressionComponent components}, each of them representing an argument in this operator. */
	protected final IExpressionComponent[] arguments;
	/** {@code true} if either of this expression's arguments contains a property reference. */
	private final boolean hasReference;

	/** The previous value of this evaluated operator. */
	private double value;



	/**
	 * Create a new operator for given arguments.
	 * 
	 * @param arguments - Expression {@link zoranodensha.api.vehicles.part.util.expr.IExpressionComponent components} forming this expression's arguments.
	 */
	public AOperator(IExpressionComponent[] arguments)
	{
		/* Determine whether there are references anywhere. */
		boolean hasReference = false;
		{
			for (IExpressionComponent argument : arguments)
			{
				hasReference |= argument.getHasReference();
			}
		}

		/* Assign field values. */
		this.arguments = arguments;
		this.hasReference = hasReference;

		/* If there is no reference, evaluate the expression once. */
		if (!this.hasReference)
		{
			value = evaluate();
		}
	}


	/**
	 * Evaluate this operator using the known arguments.
	 * 
	 * @return The evaluated result.
	 */
	protected abstract double evaluate();

	@Override
	public boolean getHasReference()
	{
		return hasReference;
	}

	@Override
	public boolean getIsTrue()
	{
		return Math.abs(getValue()) > 0.00001;
	}

	@Override
	public double getValue()
	{
		/* Only re-evaluate if the operator has references which might have changed. */
		if (hasReference)
		{
			value = evaluate();
		}
		return value;
	}
}