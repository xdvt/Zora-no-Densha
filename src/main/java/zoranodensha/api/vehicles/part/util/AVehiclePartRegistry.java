package zoranodensha.api.vehicles.part.util;

import java.util.HashMap;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * A registry class for {@link zoranodensha.api.vehicles.part.VehParBase vehicle part}s with some helper methods for easier interaction.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class AVehiclePartRegistry
{
	/** Map containing all registered parts. */
	protected static final HashMap<String, VehParBase> instances = new HashMap<String, VehParBase>();
	/** {@link java.util.HashMap HashMap} to cache relations between {@link net.minecraft.item.ItemStack item stack} and {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts}. */
	private static final HashMap<ItemStack, VehParBase> partCache = new HashMap<ItemStack, VehParBase>();



	/**
	 * Creates a new copy of the {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} specified by the given name.
	 * 
	 * @param name - Unique instance name of the registered vehicle part.
	 * @return The specified part's copy, or {@code null} if no such part was registered.
	 * @throws RuntimeException - If the part's {@link zoranodensha.api.vehicles.part.VehParBase#copy() copy()} operation failed.
	 */
	public static final VehParBase getPart(String name)
	{
		/* Retrieve original part and ensure it existed. */
		VehParBase original = instances.get(name);
		if (original == null)
		{
			return null;
		}

		/* Create and return copy. */
		try
		{
			return original.copy();
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns a vehicle part from the given ItemStack, or {@code null} if not possible.<br>
	 * Bridge method to {@link #getPartFromItemStack(ItemStack, boolean) getPartFromItemStack()}.
	 */
	public static VehParBase getPartFromItemStack(ItemStack itemStack)
	{
		return getPartFromItemStack(itemStack, false);
	}

	/**
	 * Returns a vehicle part from the given ItemStack's NBT tag, if present.
	 *
	 * @param itemStack - The ItemStack to read the part from.
	 * @param noCache - True if the part shouldn't be read from the partCache.
	 */
	public static final VehParBase getPartFromItemStack(ItemStack itemStack, boolean noCache)
	{
		if (itemStack == null)
		{
			return null;
		}

		if (noCache)
		{
			NBTTagCompound nbt = itemStack.getTagCompound();
			if (nbt == null)
			{
				return null;
			}

			String s = nbt.getString(VehParBase.NBT_KEY);
			if (s != null && !s.isEmpty())
			{
				return getPart(s);
			}
		}
		else
		{
			VehParBase part = partCache.get(itemStack);
			if (part == null)
			{
				NBTTagCompound nbt = itemStack.getTagCompound();
				if (nbt == null)
				{
					return null;
				}

				String s = nbt.getString(VehParBase.NBT_KEY);
				if (s != null && !s.isEmpty())
				{
					if (partCache.size() > 100)
					{
						partCache.clear();
					}
					part = getPart(s);
					partCache.put(itemStack, part);
				}
			}
			return part;
		}
		return null;
	}

	/**
	 * Register the given vehicle part.
	 * 
	 * @param part - {@link zoranodensha.api.vehicles.part.VehParBase Vehicle part} to register.
	 * @throws IllegalArgumentException If {@link zoranodensha.api.vehicles.part.VehParBase#getName() getName()} returned {@code null} or was empty.
	 */
	public static boolean registerPart(VehParBase part)
	{
		if (part != null)
		{
			String name = part.getName();
			if (name == null || name.isEmpty())
			{
				throw new IllegalArgumentException("[VehiclePartRegistry] A mod tried to register a vehicle part with null or empty name.");
			}

			instances.put(name, part);
			return true;
		}
		return false;
	}
}