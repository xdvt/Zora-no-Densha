package zoranodensha.api.vehicles.part.util;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;



/**
 * <h1>BufferedTexture</h1>
 * <hr>
 * <p>
 * A class used to represent a 2D array of RGB pixels which can be applied by OpenGL as a texture.
 * </p>
 * 
 * @author Jaffa
 * @see java.awt.image.BufferedImage
 */
@SideOnly(Side.CLIENT)
public class BufferedTexture
{
	/** Image data. */
	private final BufferedImage image;
	/** The buffer data. */
	private final ByteBuffer buffer;
	/** The ID to use when applying this instance as a texture with OpenGL. */
	private int textureID = -1;



	/**
	 * Initialises a new instance of the
	 * {@link zoranodensha.api.vehicles.part.util.BufferedTexture} class.
	 * 
	 * @param width
	 *            - The width of the buffer, in pixels.
	 * @param height
	 *            - The height of the buffer, in pixels.
	 */
	public BufferedTexture(final int width, final int height)
	{
		int RGB_FORMAT = 4;
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		buffer = BufferUtils.createByteBuffer(image.getWidth() * image.getHeight() * RGB_FORMAT);
	}


	public Graphics2D getGraphics()
	{
		return image.createGraphics();
	}

	/**
	 * <p>
	 * Gets the texture ID to be used when binding this buffer's texture with OpenGL.
	 * </p>
	 * <hr>
	 * </p>
	 * <h1>Example:</h1>
	 * {@code GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID)}
	 * </p>
	 * 
	 * @return - An {@code int} representing the ID of the buffer.
	 */
	public int getTextureID()
	{
		return textureID;
	}

	/**
	 * Gets the width of the buffer, in pixels.
	 * 
	 * @return - An {@code int} representing the width of the buffer.
	 */
	public int getWidth()
	{
		return image.getWidth();
	}

	/**
	 * Gets the height of the buffer, in pixels.
	 * 
	 * @return - An {@code int} representing the height of the buffer.
	 */
	public int getHeight()
	{
		return image.getHeight();
	}

	/**
	 * <p>
	 * Called to attain the {@code textureID} of this buffer.
	 * The resulting ID can be used to apply this buffer as a texture with OpenGL.
	 * </p>
	 * <hr>
	 * </p>
	 * <h1>Example:</h1>
	 * {@code GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID)}
	 * </p>
	 * 
	 * @return - This buffer's {@code textureID}.
	 */
	public int loadTexture()
	{
		/*
		 * Create an array to store the RGB pixels.
		 */
		int[] pixels = new int[image.getWidth() * image.getHeight()];
		buffer.clear();

		/*
		 * Write the pixels from the buffer into the array.
		 */
		image.getRGB(	0, // X
						0, // Y
						image.getWidth(), // Width
						image.getHeight(), // Height
						pixels, // Write target
						0, // Offset
						image.getWidth()); // Scansize

		/*
		 * Go through all the pixels in the image and write it to the byte
		 * buffer.
		 */
		for (int y = 0; y < image.getHeight(); y++)
		{
			for (int x = 0; x < image.getWidth(); x++)
			{
				/*
				 * Get the pixel at the current location...
				 */
				int pixel = pixels[(y * image.getWidth()) + x];

				/*
				 * And write it into the byte buffer.
				 */
				buffer.put((byte)((pixel >> 16) & 0xFF)); // Red
				buffer.put((byte)((pixel >> 8) & 0xFF)); // Green
				buffer.put((byte)((pixel) & 0xFF)); // Blue
				buffer.put((byte)((pixel >> 24) & 0xFF)); // Alpha
			}
		}

		/*
		 * Flip the buffer around so it is read correctly by OpenGL.
		 */
		buffer.flip();

		/*
		 * Give the texture a new ID...
		 */
		if (this.textureID < 0)
		{
			this.textureID = glGenTextures();
		}

		/*
		 * And assign OpenGL properties so it is rendered correctly in the
		 * Minecraft world.
		 */
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL11.GL_RGBA, image.getWidth(), image.getHeight(), 0, GL11.GL_RGBA, GL_UNSIGNED_BYTE, buffer);

		return textureID;
	}
}