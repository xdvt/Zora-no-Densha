package zoranodensha.api.vehicles.part.type;

import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.SyncDir;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropTank;



/**
 * Basic fluid transport part type providing a single tank and some inventory slots for bucket insertion.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PartTypeTransportTank extends PartTypeTransportItem implements IFluidHandler
{
	/** This part type's fluid tank. */
	public final PropTank tank;

	/** Ticks to wait before draining/ filling the next slot stack. */
	@SyncDir(ESyncDir.NOSYNC) private int bucketTimer;



	public PartTypeTransportTank(VehParBase parent)
	{
		super(parent, 2, 2, 2, PartTypeTransportTank.class.getSimpleName());
		addProperty(tank = (PropTank)new PropTank(parent, 1, MathHelper.ceiling_float_int(64 * parent.getWeightScale()) * 1000, "tank").setSyncDir(ESyncDir.CLIENT));
	}


	@Override
	public boolean canDrain(ForgeDirection from, Fluid fluid)
	{
		FluidTank tank = getTank();
		switch (from)
		{
			case UP:
			case DOWN:
			case UNKNOWN:
				if (tank != null && tank.getFluid() != null)
				{
					return tank.getFluid().isFluidEqual(new FluidStack(fluid, 0));
				}
				break;
		}
		return false;
	}

	@Override
	public boolean canFill(ForgeDirection from, Fluid fluid)
	{
		FluidTank tank = getTank();
		switch (from)
		{
			case UP:
			case DOWN:
			case UNKNOWN:
				if (tank != null)
				{
					return (tank.getFluidAmount() == 0) || (tank.getFluid() != null && tank.getFluid().isFluidEqual(new FluidStack(fluid, 0)));
				}
				break;
		}
		return false;
	}

	@Override
	public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain)
	{
		switch (from)
		{
			case UP:
			case DOWN:
			case UNKNOWN:
				FluidTank tank = getTank();
				if (tank != null && tank.getFluid() != null && tank.getFluid().isFluidEqual(resource))
				{
					return tank.drain(resource.amount, doDrain);
				}
				break;
		}
		return null;
	}

	@Override
	public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain)
	{
		switch (from)
		{
			case UP:
			case DOWN:
			case UNKNOWN:
				FluidTank tank = getTank();
				if (tank != null)
				{
					return tank.drain(maxDrain, doDrain);
				}
				break;
		}
		return null;
	}

	@Override
	public int fill(ForgeDirection from, FluidStack resource, boolean doFill)
	{
		FluidTank tank = getTank();
		if (tank != null && canFill(from, resource.getFluid()))
		{
			return tank.fill(resource, doFill);
		}
		return 0;
	}

	@Override
	public float getFillPercentage()
	{
		FluidTank tank = getTank();
		if (tank != null && tank.getCapacity() > 0)
		{
			return (float)tank.getFluidAmount() / (float)tank.getCapacity();
		}
		return 0.0F;
	}

	@Override
	public float getMass(float mass)
	{
		return mass + (36.0F * getFillPercentage());
	}

	/**
	 * Helper method to access the actual fluid tank.
	 */
	public FluidTank getTank()
	{
		return (tank.get() != null) ? tank.get()[0] : null;
	}

	@Override
	public FluidTankInfo[] getTankInfo(ForgeDirection from)
	{
		FluidTank tank = getTank();
		return (tank != null) ? new FluidTankInfo[] { tank.getInfo() } : new FluidTankInfo[] { };
	}

	@Override
	public void onTick()
	{
		/* Call super method to handle item movement between this and other inventories. */
		super.onTick();

		/* Only update on server worlds. */
		Train train = parent.getTrain();
		if (train.worldObj.isRemote)
		{
			return;
		}

		/* Try to override the inventory size if required. This may be needed with inventories who read incorrect size data from NBT. */
		if (getSizeInventory() != 2)
		{
			inventory.setSize(2);
		}

		/* Abort if the inventory is empty or of invalid length. */
		ItemStack[] inventory = this.inventory.get();
		if (inventory == null || inventory.length != 2)
		{
			return;
		}

		/*
		 * If bucket timer cooldown is running, reduce and return.
		 */
		if (bucketTimer > 0)
		{
			--bucketTimer;
			return;
		}

		/* Safety check to ensure the tank exists. */
		FluidTank tank = getTank();
		if (tank == null)
		{
			return;
		}

		/*
		 * If the input contains an empty bucket, try to fill it with tank contents.
		 */
		if (FluidContainerRegistry.isEmptyContainer(inventory[0]))
		{
			ItemStack itemStack = FluidContainerRegistry.fillFluidContainer(tank.drain(FluidContainerRegistry.BUCKET_VOLUME, false), inventory[0]);
			if (itemStack != null)
			{
				if (inventory[1] == null || (itemStack.getItem().equals(inventory[1].getItem()) && itemStack.getItemDamage() == inventory[1].getItemDamage() && inventory[1].stackSize < inventory[1].getMaxStackSize()))
				{
					itemStack = FluidContainerRegistry.fillFluidContainer(tank.drain(FluidContainerRegistry.BUCKET_VOLUME, true), inventory[0]);
					if (itemStack != null)
					{
						decrStackSize(0, 1);

						if (inventory[1] == null)
						{
							setInventorySlotContents(1, itemStack);
						}
						else
						{
							++inventory[1].stackSize;
						}

						bucketTimer = 10;
					}
				}
			}
		}

		/*
		 * Otherwise check whether the input contains a filled bucket which we can drain.
		 */
		else if (FluidContainerRegistry.isFilledContainer(inventory[0]))
		{
			FluidStack fluidStack = FluidContainerRegistry.getFluidForFilledItem(inventory[0]);
			if (fluidStack != null && tank.fill(fluidStack, false) == FluidContainerRegistry.BUCKET_VOLUME)
			{
				ItemStack itemStack = FluidContainerRegistry.drainFluidContainer(inventory[0]);
				if (itemStack != null)
				{
					if (inventory[1] == null)
					{
						setInventorySlotContents(1, itemStack);
					}
					else if ((inventory[1].getItem().equals(itemStack.getItem()) && inventory[1].getItemDamage() == itemStack.getItemDamage()) && inventory[1].stackSize < inventory[1].getMaxStackSize())
					{
						++inventory[1].stackSize;
					}
					else
					{
						return;
					}

					tank.fill(fluidStack, true);
					decrStackSize(0, 1);

					bucketTimer = 10;
				}
			}
		}
	}
}