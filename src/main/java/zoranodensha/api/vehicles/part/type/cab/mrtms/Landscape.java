package zoranodensha.api.vehicles.part.type.cab.mrtms;

import java.util.ArrayList;

import net.minecraft.world.ChunkCoordIntPair;
import zoranodensha.api.vehicles.part.type.cab.MTMS;



/**
 * <p>
 * This class represents a package of data sent from signals to the MTCS module in trains.
 * </p>
 * <p>
 * During each MTCS tick in the train's active cab, it finds the next signal ahead of the train, and requests a {@link Landscape} instance from it. The data and values in this class tell the MTCS
 * system the condition of the track ahead. Information such as signal locations, buffer locations, speed limits at points, and other important factors are stored in this instance.
 * </p>
 * 
 * 
 * @author Jaffa
 */
public class Landscape
{
	/**
	 * How far away, in metres, a train should stop before a signal or obstruction.
	 * At this distance, the MTCS speed limit will reach {@code 0.0 km/h} causing a train to stop.
	 */
	private static final float BUFFER = 10.0F;

	/**
	 * A list of {@link zoranodensha.api.vehicles.part.type.cab.mrtms.Landmark} instances which effectively represent the condition of the track ahead. Signals, signs, speed limits, and buffers can
	 * all be represented as 'landmark' instances which are stored in this list.
	 */
	private ArrayList<Landmark> landmarks;

	/**
	 * A list of {@link net.minecraft.world.ChunkCoordIntPair} instances that represent all the unique chunks that this Landscape exists in. Used to chunk loading purposes.
	 */
	private ArrayList<ChunkCoordIntPair> chunks;

	/**
	 * The total length of this 'landscape' instance, starting from the signal. Measured in metres/blocks.
	 */
	private float totalLength;



	/**
	 * Initialises a new instance of the {@link zoranodensha.api.vehicles.part.type.cab.mrtms.Landscape} class. Creates a blank instance with a starting total length of 0 metres.
	 */
	public Landscape()
	{
		this(0.0F);
	}

	/**
	 * Initialises a new instance of the {@link zoranodensha.api.vehicles.part.type.cab.mrtms.Landscape} class.
	 * 
	 * @param totalLength - How long, in metres, this landscape instance should be. Measured from the relevant signal.
	 */
	public Landscape(float totalLength)
	{
		this.totalLength = totalLength;
		this.landmarks = new ArrayList<Landmark>();
		this.chunks = new ArrayList<ChunkCoordIntPair>();
	}

	/**
	 * Adds a new landmark to this landscape.
	 * 
	 * @param landmark - The landmark instance to add to the list of landmarks held within this landscape instance.
	 */
	public void addLandmark(Landmark landmark)
	{
		landmarks.add(landmark);
	}

	/**
	 * Gets a list of all the landmarks in this landscape instance.
	 * 
	 * @return - An {@link ArrayList} of {@link zoranodensha.api.vehicles.part.type.cab.mrtms.Landmark} instances.
	 */
	public ArrayList<Landmark> getAllLandmarks()
	{
		return landmarks;
	}

	public ArrayList<ChunkCoordIntPair> getChunkList()
	{
		return chunks;
	}

	/**
	 * <p>
	 * Gets the closest landmark to the beginning point of this landscape. Essentially, the landmark with the lowest 'distance' value.
	 * </p>
	 * <p>
	 * THIS MAY RETURN {@code null} IF THERE ARE NO LANDMARKS AHEAD. Ask me how I know.
	 * </p>
	 * 
	 * @return - A {@link zoranodensha.api.vehicles.part.type.cab.mrtms.Landmark} instance.
	 */
	public Landmark getClosestLandmark()
	{
		Landmark closest = null;

		for (Landmark landmark : landmarks)
		{
			if (closest == null)
			{
				closest = landmark;
			}
			else
			{
				if (landmark.getLocation() < closest.getLocation())
				{
					closest = landmark;
				}
			}
		}

		return closest;
	}

	/**
	 * Gets the maximum permitted speed of a train (based on the supplied deceleration value) if the train were to be at the very beginning of this landscape instance.
	 * 
	 * @param deceleration - The deceleration rate of the train, in {@code m/s/s}.
	 * @return - The maximum permitted speed of the train, if it were at the very begninning of the landscape instance (distance = 0), measured in {@code km/h}.
	 */
	public float getMaximumSpeed(float deceleration)
	{
		return getMaximumSpeed(deceleration, 0.0F);
	}

	/**
	 * Gets the maximum permitted speed of a train (based on the supplied deceleration value, and how far along the train is within this landscape distance).
	 * 
	 * @param deceleration - The deceleration rate of the train, in {@code m/s/s}.
	 * @param offset - How far along the train is along the landscape instance at this present moment, in {@code m}.
	 * @return - The maximum permitted speed of the train at this point in time based on the supplied values.
	 */
	public float getMaximumSpeed(float deceleration, float offset)
	{
		float maximumSpeed = MTMS.MAX_SPEED_LIMIT;
		float distance;
		float landmarkMaximum;

		for (Landmark landmark : landmarks)
		{
			distance = landmark.getLocation() - offset;

			if (landmark.getSpeedLimit() >= 0.0F)
			{
				landmarkMaximum = (float)(3.6F * Math.sqrt((2 * deceleration * Math.max(distance - BUFFER, 0.0F)) + Math.pow(landmark.getSpeedLimit() / 3.6F, 2.0D)));
			}
			else
			{
				landmarkMaximum = maximumSpeed;// (float)(3.6F * Math.sqrt(2 * deceleration * Math.max(distance - BUFFER, 0.0F)));
			}

			if (landmarkMaximum < maximumSpeed)
			{
				maximumSpeed = landmarkMaximum;
			}
		}

		/*
		 * End of the landscape
		 */
		{
			distance = this.getTotalLength() - offset;

			landmarkMaximum = (float)(3.6F * Math.sqrt(2 * deceleration * Math.max(distance - BUFFER, 0.0F)));

			if (landmarkMaximum < maximumSpeed)
			{
				maximumSpeed = landmarkMaximum;
			}
		}


		return maximumSpeed;
	}

	public float getTargetSpeed(float deceleration, float trackSpeed)
	{
		return getTargetSpeed(deceleration, trackSpeed, 0.0F);
	}

	public float getTargetSpeed(float deceleration, float trackSpeed, float offset)
	{
		float maximumSpeed = MTMS.MAX_SPEED_LIMIT;
		Landmark maximumLandmark = null;
		float distance;
		float landmarkMaximum;

		for (Landmark landmark : landmarks)
		{
			distance = landmark.getLocation() - offset;

			if (landmark.getSpeedLimit() >= 0.0F)
			{
				landmarkMaximum = (float)(3.6F * Math.sqrt((2 * deceleration * Math.max(distance - BUFFER, 0.0F)) + Math.pow(landmark.getSpeedLimit() / 3.6F, 2.0D)));
			}
			else
			{
				landmarkMaximum = maximumSpeed;// (float)(3.6F * Math.sqrt(2 * deceleration * Math.max(distance - BUFFER, 0.0F)));
			}

			if (landmarkMaximum < maximumSpeed)
			{
				maximumSpeed = landmarkMaximum;
				maximumLandmark = landmark;
			}
		}

		/*
		 * End of the landscape
		 */
		{
			distance = this.getTotalLength() - offset;

			landmarkMaximum = (float)(3.6F * Math.sqrt(2 * deceleration * Math.max(distance - BUFFER, 0.0F)));

			if (landmarkMaximum < maximumSpeed)
			{
				maximumSpeed = landmarkMaximum;
				maximumLandmark = null;
			}
		}

		if (maximumLandmark != null)
		{
			return maximumLandmark.getSpeedLimit();
		}
		else
		{
			return 0.0F;
		}
	}

	/**
	 * @return - The total length of this landscape instance, measured in {@code m}.
	 */
	public float getTotalLength()
	{
		return totalLength;
	}

	/**
	 * If needed, the total length of this landscape instance can be increased with this method.
	 * 
	 * @param length - How many metres to increase the total length of the landscape instance by.
	 */
	public void increaseTotalLength(float length)
	{
		this.totalLength += length;
	}

	public void setChunkList(ArrayList<ChunkCoordIntPair> chunks)
	{
		this.chunks = chunks;
	}

	/**
	 * Manually sets the length of this landscape instance.
	 * 
	 * @param totalLength - The total length of the landscape instance, in {@code m}.
	 */
	public void setTotalLength(float totalLength)
	{
		this.totalLength = totalLength;
	}
}
