package zoranodensha.api.vehicles.part.type;

import java.util.Random;

import org.apache.logging.log4j.Level;

import net.minecraft.util.MathHelper;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropBreaker;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.common.core.ModCenter;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.property.PropSound;



/**
 * Diesel engines, burning diesel fluid to create force.
 */
public class PartTypeEngineDiesel extends APartTypeEngine
{
	/** Random number generator, used to randomise RPM decay. */
	private static final Random ran = new Random();

	/** Current RPM of the engine. */
	public final PropFloat currentRPM;

	/** Engine efficiency. 75% is average for a railway diesel engine. */
	public final PropFloatBounded efficiency;
	/** This engine's responsiveness, which affects how quickly the RPM changes. */
	public final PropFloatBounded responsiveness;

	/**
	 * A {@code float} ranging from {@code 0.0F} to {@code 1.0F} which represents how much the electrical generator is generating electricity from the engine.
	 */
	public final PropFloatBounded generatorActivity;

	/**
	 * This engine's idling RPM.
	 */
	public final PropInteger idleRPM;
	/** Maximum service RPM of the engine. This is the RPM when the throttle is at the maximum value. */
	public final PropInteger maxRPM;
	/** Power output of the engine, in kiloWatts (kW). */
	public final PropInteger power;

	/** The breaker used to switch on/off this engine. */
	public final PropBreaker ignition;

	/** The sound of setting the ignition breaker switch. */
	public PropSound soundIgnitionSet;
	/** The sound of tripping the ignition breaker switch. */
	public PropSound soundIgnitionTrip;



	public PartTypeEngineDiesel(VehParBase parent)
	{
		super(parent, PartTypeEngineDiesel.class.getSimpleName());

		addProperty(currentRPM = (PropFloat)new PropFloat(parent, 0.0F, "rpm").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(efficiency = (PropFloatBounded)new PropFloatBounded(parent, 0.01F, 1.0F, "efficiency").setConfigurable());
		addProperty(responsiveness = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 1.0F, "responsiveness").setConfigurable());
		addProperty(generatorActivity = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 1.0F, "generatorActivity").setValidTagCalls(EValidTagCalls.PACKET).setSyncDir(ESyncDir.CLIENT));
		addProperty(idleRPM = new PropInteger(parent, "rpmIdle"));
		addProperty(maxRPM = new PropInteger(parent, "rpmMaximum"));
		addProperty(power = (PropInteger)new PropInteger(parent, "power").setConfigurable());
		addProperty(ignition = new PropBreaker(parent, false, "ignition")
		{
			@Override
			public void set()
			{
				if (!get())
				{
					if (soundIgnitionSet != null)
					{
						soundIgnitionSet.play();
					}
				}

				super.set();
			}

			@Override
			public void trip()
			{
				if (get())
				{
					if (soundIgnitionTrip != null)
					{
						soundIgnitionTrip.play();
					}
				}

				super.trip();
			}
		});
	}


	/**
	 * Getter to access the current RPM value.
	 */
	public float getCurrentRPM()
	{
		return currentRPM.get();
	}

	@Override
	public float getProducedForce()
	{
		/* The current horsepower being produced by the engine at this moment. */
		float horsepower;
		/* The resulting kN of output force. */
		float output;

		/* Calculate the current horsepower. */
		if (currentRPM.get() >= idleRPM.get())
		{
			horsepower = (currentRPM.get() - idleRPM.get()) / (maxRPM.get() - idleRPM.get());
		}
		else
		{
			horsepower = 0.0F;
		}
		/* The coefficient here converts the power value from kW to hp. */
		horsepower *= (power.get() * 1.341F);

		/* Decrease the engine's output power as speed increases. */
		float observedSpeed = (float)Math.abs(getTrain().getLastSpeed() * 20.0F * 3.6F);
		if (observedSpeed < 30.0F)
		{
			observedSpeed = 30.0F;
		}

		/* Calculate output kN. */
		output = 2.65F * ((efficiency.get() * horsepower) / observedSpeed);

		/* Clamp and invert if necessary. */
		if (getTrain() != null)
		{
			if (getTrain().getActiveCab() != null)
			{
				if (!getTrain().getActiveCab().getReverserState().isForwardMovement())
				{
					output = -output;
				}
			}
		}

		output *= generatorActivity.get();

		/* Return. */
		return output;
	}

	@Override
	public void onTick()
	{
		/* Only update on server worlds. */
		Train train = getTrain();
		if (train == null || train.worldObj.isRemote || !train.addedToChunk)
		{
			return;
		}

		/* Retrieve active cab and, if it exists, its throttle level. */
		float throttle = 0.0F;
		{
			PartTypeCabBasic activeCab = train.getActiveCab();
			if (activeCab != null)
			{
				if (activeCab.getReverserState().equals(EReverser.FORWARD) || activeCab.getReverserState().equals(EReverser.BACKWARD))
				{
					throttle = activeCab.getThrottleLogical();

					if (throttle < 0.0F)
					{
						throttle = 0.0F;
					}
				}
			}
		}

		/*
		 * Update the generator activity.
		 */
		generatorActivity.set(generatorActivity.get() + (((throttle != 0.0F ? 1.0F : 0.0F) - generatorActivity.get()) / 20.0F));

		/*
		 * Update current RPM each tick.
		 */
		{
			/* Get previous tick's RPM value. */
			float prevRPM = currentRPM.get();

			float governor;

			/* Determine the target RPM. */
			if (ignition.get())
			{
				governor = idleRPM.get() + (throttle * (maxRPM.get() - idleRPM.get()));
			}
			else
			{
				governor = 0.0F;
			}

			/* Get the maximum rate of RPM change, based on the responsiveness value. */
			float revvingSpeed = 3.5F * responsiveness.get();

			/* Get how much the RPM needs to move and then clamp it to the maximum revving speed. */
			float rpmMovement = (governor - currentRPM.get()) / 5.0F;
			rpmMovement -= (ran.nextFloat() * 1.2F);
			rpmMovement = MathHelper.clamp_float(rpmMovement, -revvingSpeed, revvingSpeed);

			/* Move the RPM. */
			prevRPM = MathHelper.clamp_float(prevRPM + rpmMovement, 0.0F, maxRPM.get());

			currentRPM.set(prevRPM);
		}
	}
}