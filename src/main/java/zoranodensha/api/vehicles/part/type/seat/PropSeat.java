package zoranodensha.api.vehicles.part.type.seat;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.type.PartTypeSeat;



/**
 * Custom property type to wrap the part type's seat entity reference.
 */
public class PropSeat extends APartProperty<EntitySeat>
{
	/** The parent of the part type this property belongs to. */
	private final PartTypeSeat parentType;

	/**
	 * The wrapped seat's entity identifier.<br>
	 * Assigned when reading this property from NBT while the {@link #parent} has no
	 * {@link zoranodensha.api.vehicles.Train train} reference which can be used to access
	 * the current {@link net.minecraft.world.World World} object.<br>
	 * {@code -1} while unassigned. Gets reset once a reference was found.
	 */
	private int nbtSeatEntityID = -1;



	public PropSeat(VehParBase parent, String name)
	{
		super(parent, EntitySeat.class, name);

		if ((parentType = parent.getPartType(PartTypeSeat.class)) == null)
		{
			throw new NullPointerException("Specified parent part doesn't hold any PartTypeSeat instance!");
		}

		setSyncDir(ESyncDir.CLIENT);
		setValidTagCalls(EValidTagCalls.PACKET);
	}


	@Override
	public EntitySeat get()
	{
		if (nbtSeatEntityID != -1)
		{
			Train train = getParent().getTrain();
			if (train != null)
			{
				/* Cache the entity ID and reset. */
				int nbtSeatEntityID = this.nbtSeatEntityID;
				this.nbtSeatEntityID = -1;

				/* Now call set() - this prevents infinitely looped method invocation. */
				set(train.worldObj.getEntityByID(nbtSeatEntityID));
			}
		}
		return super.get();
	}

	/**
	 * Getter for this property's {@link #parentType parent type}.
	 */
	public PartTypeSeat getParentType()
	{
		return parentType;
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			nbtSeatEntityID = nbt.getInteger(nbtKey);
			return true;
		}
		return false;
	}

	@Override
	public boolean set(Object property)
	{
		/*
		 * Handle property removal.
		 */
		if (property == null)
		{
			/* Only take action if there exists a seat. */
			EntitySeat seat = get();
			if (seat != null)
			{
				seat.setPartType(null);
				this.property = null;

				setHasChanged(true);
				getParent().onPropertyChanged(this);
				return true;
			}
		}

		/*
		 * Handle property override.
		 */
		else if (property instanceof EntitySeat)
		{
			/* Don't override if there exists a seat already. */
			if (get() != null && get() != property)
			{
				return false;
			}

			/* Assign seat and abort upon failure. */
			super.set(property);

			/* Update references and positions where required. */
			EntitySeat seat = get();
			if (seat != null)
			{
				if (seat.getPartType() != parentType)
				{
					seat.setPartType(parentType);
				}
			}
			return true;
		}

		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		EntitySeat seat = get();
		if (seat != null && seat.addedToChunk)
		{
			nbt.setInteger(nbtKey, seat.getEntityId());
		}
	}
}