package zoranodensha.api.vehicles.part.type;

import java.util.ArrayList;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.event.partType.PartTypeRegisterEvent;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.IPartProperty;



/**
 * <h1>Vehicle Part Type</h1>
 * <hr>
 * 
 * <p>
 * Types of {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts},
 * defining a part's abilities. A vehicle part may have multiple types.
 * </p>
 * 
 * <p>
 * In order for part types to be recognised by XML-based parts, they <b>must</b> provide a public
 * constructor taking <b>exactly one</b> {@link zoranodensha.api.vehicles.part.VehParBase part} as argument.
 * </p>
 * 
 * <p>
 * Note that part {@link #getProperties() properties} aren't required to be explicitly
 * declared in a part's XML-file.<br>
 * These are optional and only used if the implementing user wishes to override the
 * <b>default</b> value of a property.<br>
 * </p>
 * 
 * <p>
 * In XML-based vehicle parts, a part type is defined using a {@code <type>} tag:<br>
 * 
 * <pre>
 * {@code
 * <type name="PartTypeName">
 *   <property name="SomeProperty"> DefaultValue </property>
 *   <property name="AnotherProperty"> TRUE </property>
 *   ...
 * </type>
 * }
 * </pre>
 * </p>
 * 
 * @see PartTypeRegisterEvent PartTypeRegisterEvent
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class APartType
{
	/** All registered part types in a list. */
	private static final ArrayList<Class<? extends APartType>> partTypes = new ArrayList<Class<? extends APartType>>();

	/** This type's parent {@link zoranodensha.api.vehicles.part.VehParBase part}. */
	protected final VehParBase parent;
	/** Name of this part type. */
	protected final String name;
	/** List of part type {@link zoranodensha.api.vehicles.part.property.IPartProperty properties}. */
	protected final ArrayList<IPartProperty<?>> properties = new ArrayList<IPartProperty<?>>();


	/*
	 * Register all default part types.
	 * Then, broadcast event for custom part types to be registered.
	 */
	static
	{
		partTypes.add(PartTypeBogie.class);
		partTypes.add(PartTypeCab.class);
		partTypes.add(PartTypeCabBasic.class);
		partTypes.add(PartTypeCouplingChain.class);
		partTypes.add(PartTypeCouplingJanney.class);
		partTypes.add(PartTypeCouplingScharfenberg.class);
		partTypes.add(PartTypeDoor.class);
		partTypes.add(PartTypeEngineDiesel.class);
		partTypes.add(PartTypeEngineElectric.class);
		partTypes.add(PartTypeHorn.class);
		partTypes.add(PartTypeLamp.class);
		partTypes.add(PartTypeLoadable.class);
		partTypes.add(PartTypePantograph.class);
		partTypes.add(PartTypeSeat.class);
		partTypes.add(PartTypeTransportItem.class);
		partTypes.add(PartTypeTransportTank.class);

		MinecraftForge.EVENT_BUS.post(new PartTypeRegisterEvent(partTypes));
	}



	/**
	 * In order for part parsing to be able to parse a part type,
	 * each part type is required to contain a constructor that takes the given parent
	 * part (see below) as <b>only</b> argument.
	 * 
	 * @param parent - Parent {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} of this property.
	 * @param name - Unique name of this part type, case sensitive.
	 */
	public APartType(VehParBase parent, String name)
	{
		this.parent = parent;
		this.name = name;
		this.parent.addPartType(this);
	}

	/**
	 * Adds the given property to the {@link #parent} part's property list.
	 * 
	 * @param property - The {@link zoranodensha.api.vehicles.part.property.IPartProperty property} to place in the parent's property map.
	 * @see zoranodensha.api.vehicles.part.VehParBase#addProperty(IPartProperty) addProperty(IPartProperty)
	 */
	protected void addProperty(IPartProperty<?> property)
	{
		parent.addProperty(property);
		properties.add(property);
	}

	/**
	 * Getter method to access the {@link #parent}'s local X-{@link zoranodensha.api.vehicles.part.VehParBase#getOffset() offset}.
	 */
	public float getLocalX()
	{
		return getParent().getOffset()[0];
	}

	/**
	 * Return a unique <b>case sensitive</b> name of this part type.<br>
	 * The type name may be used for reference purposes and as tag name in XML-based parts.
	 * 
	 * @return This part type's name.
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Getter for the {@link #parent} of this part type.
	 */
	public VehParBase getParent()
	{
		return parent;
	}

	/**
	 * Return a list of all properties this part type uses.<br>
	 * The returned list may not be modified.
	 * 
	 * @return List of all {@link zoranodensha.api.vehicles.part.property.IPartProperty properties} used by this type.
	 */
	public ArrayList<IPartProperty<?>> getProperties()
	{
		return properties;
	}

	/**
	 * Returns a copy of the {@link #partTypes list} of registered part types.
	 */
	public static ArrayList<Class<? extends APartType>> getRegisteredPartTypes()
	{
		return new ArrayList<Class<? extends APartType>>(partTypes);
	}

	/**
	 * Helper method to get the {@link #parent}'s section.
	 * 
	 * @return The parent part's {@link zoranodensha.api.vehicles.VehicleSection section}. May be {@code null}.
	 */
	public VehicleSection getSection()
	{
		return getParent().getSection();
	}

	/**
	 * Getter for the {@link #parent}'s train.
	 * 
	 * @return The parent part's {@link zoranodensha.api.vehicles.Train train}. May be {@code null}.
	 */
	public Train getTrain()
	{
		return getParent().getTrain();
	}

	/**
	 * Getter for the {@link #parent}'s vehicle.
	 * 
	 * @return The parent part's {@link zoranodensha.api.vehicles.VehicleData vehicle}. May be {@code null}.
	 */
	public VehicleData getVehicle()
	{
		return getParent().getVehicle();
	}

	@Override
	public String toString()
	{
		return String.format("%s[part=%s]", getClass().getSimpleName().toString(), (getParent() != null ? getParent().toString() : "~NULL~"));
	}



	/**
	 * Interface for part types that may need to be notified when their parent part is being activated (i.e. when a player clicks on it).
	 */
	public interface IPartType_Activatable
	{
		/**
		 * Called by the parent part's {@link zoranodensha.api.vehicles.Train train} whenever the parent part
		 * was right-clicked.<br>
		 * <br>
		 * Called on <i>non-remote worlds</i> only.
		 * 
		 * @param player - {@link net.minecraft.entity.player.EntityPlayer Player} who clicked the parent part.
		 * @return {@code true} if something happens.
		 */
		boolean onActivated(EntityPlayer player);
	}

	/**
	 * This interface provides a hook for part types to change their parent's {@link zoranodensha.api.vehicles.part.VehParBase#getMass() mass}.
	 */
	public interface IPartType_MassListener
	{
		/**
		 * Called to ask for changes to part mass by this part type.
		 * 
		 * @param mass - The current value returned as mass, in Tonnes (1t = 1000kg).
		 * @return The part's modified mass.
		 */
		float getMass(float mass);
	}

	/**
	 * Part types implementing this interface receive a call to the interface's method each time a vehicle part's
	 * {@link zoranodensha.api.vehicles.part.VehParBase#setPosition(double, double, double) setPosition(double, double, double)}
	 * method is called.
	 */
	public interface IPartType_PositionListener
	{
		/**
		 * Called to notify about the parent part's position change.<br>
		 * This is called before effectively changing the parent part's bounding box.
		 */
		void setPosition(double x, double y, double z);
	}

	/**
	 * Interface implemented by part types which require an update every in-game tick.
	 */
	public interface IPartType_Tickable
	{
		/**
		 * Called every tick by the parent {@link zoranodensha.api.vehicles.Train train} on both server and client worlds.<br>
		 * This allows part types to run logic updates each ticks.
		 */
		void onTick();
	}
}