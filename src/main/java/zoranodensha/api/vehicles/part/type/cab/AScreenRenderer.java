package zoranodensha.api.vehicles.part.type.cab;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.part.util.BufferedTexture;



/**
 * Basic renderer class for
 * {@link zoranodensha.api.vehicles.part.type.cab.AScreen cab screens}.<br>
 * <br>
 * <i>Client-side only.</i>
 * 
 * @author Jaffa
 */
@SideOnly(Side.CLIENT)
public abstract class AScreenRenderer
{
	/*
	 * Colors
	 */
	protected static final Color COLOR_BACKGROUND = new Color(23, 54, 96);
	protected static final Color COLOR_INFO = new Color(0, 128, 255);
	protected static final Color COLOR_TEXT = new Color(255, 255, 255);
	protected static final Color COLOR_WARNING = new Color(255, 255, 0);

	/*
	 * Fonts
	 */
	protected static final Font FONT_BIG = new Font("Arial", Font.PLAIN, 50);
	protected static final Font FONT_DEFAULT = new Font("Arial", Font.PLAIN, 22);
	protected static final Font FONT_MICRO = new Font("Arial", Font.PLAIN, 11);
	protected static final Font FONT_MINI = new Font("Arial", Font.BOLD, 16);
	protected static final Font FONT_MINI_PLAIN = new Font("Arial", Font.PLAIN, 16);
	protected static final Font FONT_SPEEDOMETER = new Font("Arial", Font.BOLD, 25);

	/**
	 * The buffered texture object of this screen, which can be applied as a texture
	 * by OpenGL.
	 */
	public final BufferedTexture texture;



	/**
	 * Create a new cab screen renderer with specified width and height.
	 * 
	 * @param width - Screen width.
	 * @param height - Screen height.
	 */
	public AScreenRenderer(int width, int height)
	{
		texture = new BufferedTexture(width, height);
	}

	/**
	 * Create a new cab screen renderer with the specified width and height.
	 * 
	 * @param size - The size of the screen.
	 */
	public AScreenRenderer(Dimension size)
	{
		this(size.width, size.height);
	}

	/**
	 * Render the given String centered over the given X- and Y-coordinates.
	 */
	public static void drawCenteredString(Graphics2D g, String text, int x, int y)
	{
		/* Measure the size of the string so it can be displaced properly. */
		Rectangle2D textSize = g.getFontMetrics().getStringBounds(text, g);

		/* Determine the new origin of the string. */
		x -= textSize.getWidth() / 2;
		y += textSize.getHeight() / 4;

		/* Render the string. */
		g.drawString(text, x, y);
	}

	/**
	 * Returns the buffered texture containing rendered image data of the rendered
	 * control.
	 * 
	 * @return Buffered texture containing rendered image data.
	 */
	public BufferedTexture getTexture()
	{
		return texture;
	}

	/**
	 * Called when this screen should be re-drawn.
	 * 
	 * @see java.awt.Graphics2D Graphics2D
	 */
	public abstract void onRepaint();
}