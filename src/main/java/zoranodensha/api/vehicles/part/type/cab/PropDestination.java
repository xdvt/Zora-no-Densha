package zoranodensha.api.vehicles.part.type.cab;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.property.PropString;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic;



/**
 * Property holding a cab's declared destination data.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PropDestination extends PropString
{
	/** Line separator for the destination text. */
	public static final String NEW_LINE = Character.toString('\n');



	public PropDestination(PartTypeCabBasic cab, String name)
	{
		super(cab.getParent(), name);
		setValidTagCalls(EValidTagCalls.PACKET_SAVE);
		setSyncDir(ESyncDir.CLIENT);
	}

	/**
	 * Returns a new String array holding the destination data.
	 * 
	 * @return Destination data in a new array. May be {@code null}.
	 */
	public String[] getDestination()
	{
		if (get() != null)
		{
			return get().split(NEW_LINE);
		}
		return null;
	}

	/**
	 * Assign an array holding destination description as new destination data.<br>
	 * It may be displayed by applicable parts in the parent cab's train.
	 * 
	 * @param dest - An array of destinations, or {@code null} if there is none set.
	 */
	public void setDestination(String... dest)
	{
		String prop = "";
		{
			if (dest != null && dest.length > 0)
			{
				for (int i = 0; i < dest.length; ++i)
				{
					prop += dest[i];
					if (i + 1 < dest.length)
					{
						prop += NEW_LINE;
					}
				}
			}
		}
		set(prop);
	}
	
	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		/* Overridden to allow empty Strings to be synchronised. */
		if (get() != null)
		{
			nbt.setString(nbtKey, get());
		}
	}
}