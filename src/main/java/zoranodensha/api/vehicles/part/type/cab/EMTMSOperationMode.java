package zoranodensha.api.vehicles.part.type.cab;


/**
 * <p>
 * An enum which represents an 'Operation Mode' for the MTMS system. Each operation mode represents a different level of speed control and supervision.
 * </p>
 * 
 * @author Jaffa
 */
public enum EMTMSOperationMode
{
	/**
	 * <p>
	 * The speed of the train is under constant supervision and any overspeeding will activate alarms and automatic brake applications. The track ahead is continuously monitored by MTMS.
	 * </p>
	 */
	FULL_SUPERVISION,
	/**
	 * <p>
	 * MTMS does not supervise the speed of the train in any way and does not check the track ahead. The deadman system will still function. This mode should be selected by the train crew if MTMS is
	 * causing problems or is not reacting correctly to the track ahead.
	 * </p>
	 */
	ISOLATION,
	/**
	 * <p>
	 * MTMS will control the speed of the train up to a certain speed, but does not monitor the track ahead. Overspeed alarms and automatic brake applications will occur in this mode.
	 * </p>
	 */
	ON_SIGHT,
	/**
	 * <p>
	 * MTMS will allow the train to travel at a restricted speed in reverse. The track behind the train is NOT monitored, but overspeeding above the restricted speed will cause alarms and automatic
	 * brake applications.
	 * </p>
	 */
	REVERSING
}
