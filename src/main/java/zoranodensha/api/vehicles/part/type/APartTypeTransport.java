package zoranodensha.api.vehicles.part.type;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_MassListener;



/**
 * Part type for transport of various goods.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class APartTypeTransport extends APartType implements IPartType_MassListener
{
	public APartTypeTransport(VehParBase parent, String name)
	{
		super(parent, name);
	}

	/**
	 * Return how much of this part type's capacity is used.
	 *
	 * @return Percentage of usage, ranging from {@code 0.0F} (empty) to {@code 1.0F} (full).
	 */
	public abstract float getFillPercentage();
}