package zoranodensha.api.vehicles.part.type;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import zoranodensha.api.structures.signals.BalisePulse;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.type.PartTypeDoor.EDoorSide;
import zoranodensha.api.vehicles.part.type.PartTypeDoor.EDoorState;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode;
import zoranodensha.api.vehicles.part.type.cab.EAutodriveState;
import zoranodensha.api.vehicles.part.type.cab.MTMS;
import zoranodensha.api.vehicles.part.type.cab.PropButton;
import zoranodensha.api.vehicles.part.type.cab.PropLever;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.api.vehicles.part.type.cab.mrtms.Landscape;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.VehicleHelper;


/**
 * <p>
 * Modern cab containing routing systems such as MTCS and cruise control like
 * AFB.
 * </p>
 * <p>
 * <p>
 * As an extension of the default basic
 * {@link zoranodensha.api.vehicles.part.type.PartTypeCabBasic cab}, this cab
 * offers:
 * <ul>
 * <li>AFB (Automatic speed and brake regulation)</li>
 * <li>MTCS, MTIS, and MSMR functionality</li>
 * </ul>
 *
 * @author Jaffa
 * @author Leshuwa Kaiheiwa
 */
public abstract class PartTypeCab extends PartTypeCabBasic
{
	/*
	 * Levers
	 */
	public final PropLever afbController;
	
	/*
	 * Train control systems
	 */
	/**
	 * Minecraft Train Management System
	 */
	public final MTMS mtms;
	
	/**
	 * Currently hovered button, as determined in {@link #onUpdateHovering()}. May
	 * be {@code null}.<br>
	 * <i>Note: this property is not - and must not be - registered via
	 * {@link #addProperty(IPartProperty) addProperty()}.</i>
	 */
	@SideOnly(Side.CLIENT)
	public PropButton hoveredButton;
	
	/**
	 * {@code true} while the action button (usually left mouse button) is being
	 * pressed on the {@link #hoveredButton}.
	 */
	@SideOnly(Side.CLIENT)
	public boolean isHoverPressed;
	
	
	public PartTypeCab(VehParBase parent)
	{
		super(parent, PartTypeCab.class.getSimpleName());
		
		/*
		 * AFB Lever
		 */
		addProperty(afbController = new PropLever(this, "afbController", 32, 0.008F));
		
		mtms = new MTMS(this);
	}
	
	
	@Override
	public float getBrakeLevelLogical()
	{
		/* Retrieve default brake level. */
		float brake = getBrakeLevelPhysical();
		
		/*
		 * Make sure the current braking level meets the minimum requirements of MTMS at
		 * this time.
		 */
		brake = Math.max(brake, mtms.getMinimumBrake());
		
		return brake;
	}
	
	@Override
	public float getDynamicBrakeLevelLogical()
	{
		/* Retrieve default brake level. */
		float brake = getDynamicBrakeLevelPhysical();
		
		/*
		 * Make sure the current braking level meets the minimum requirements of MTMS at
		 * this time.
		 */
		brake = Math.max(brake, mtms.getMinimumBrake());
		
		return brake;
	}
	
	
	@Override
	public boolean getMayOverrideActive()
	{
		if (mtms.autodriveEnable.get())
		{
			if (mtms.autodriveState.get() == EAutodriveState.TERMINATING)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		return super.getMayOverrideActive();
	}
	
	@Override
	public float getThrottleLogical()
	{
		/* Retrieve default value. */
		float throttle = super.getThrottleLogical();
		
		/*
		 * If the autodrive system is enabled, force maximum power.
		 */
		if (mtms.autodriveState.get() != EAutodriveState.INACTIVE)
		{
			throttle = 1.0F;
		}
		
		/*
		 * Make sure that the current throttle does not exceed the maximum allowed
		 * throttle by MTMS.
		 */
		throttle = Math.min(throttle, mtms.getMaximumThrottle());
		
		return throttle;
	}
	
	@Override
	public void onTick()
	{
		super.onTick();
		
		if (getTrain() != null && !getTrain().worldObj.isRemote && getIsActive() && this.reverser.get().equals(EReverser.FORWARD))
		{
			if (mtms.isolateMTCS.get())
			{
				/* Safety systems are disabled. */
				
				/* Remove any information referring to the outside world so MTMS can't use it. */
				mtms.lastLandscape = null;
				
				/* If there is an alerter, constantly reset it so it stays disabled. */
				if (alerter != null)
				{
					alerter.reset();
				}
			}
			else
			{
				/*
				 * Attempt to get the front bogie of the train.
				 */
				ConcatenatedTypesList<PartTypeBogie> bogies = getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
				boolean cabInverse = VehicleHelper.getAngle(null, PartHelper.getViewVec(getParent(), true)) > 90.0;
				PartTypeBogie reference = cabInverse ? bogies.get(bogies.size() - 1) : bogies.get(0);
				
				/*
				 * Get the landscape.
				 */
				BalisePulse pulse = new BalisePulse(reference.prevPos.get(), reference.dir.get());
				Landscape landscape = pulse.getLandscape(getTrain().worldObj, mtms.trainLength.get(), mtms.trainSpeed.get(), mtms.autodriveEnable.get());
				
				mtms.lastLandscape = landscape;
			}
		}
		else
		{
			/*
			 * Normally here the mtms.lastLandscape variable is set to null, but it appears
			 * that sometimes this would cause MTCS to freak out when approaching signals at
			 * stop. So it's been removed from here.
			 */
		}
		
		mtms.onUpdate();
		
		/*
		 * On client side, update mouse hover.
		 */
		if (getTrain() != null && getTrain().worldObj.isRemote)
		{
			onUpdateHovering();
		}
		
		/*
		 * Automatically change the marker/head lights.
		 */
		{
			boolean shouldBeWhite = false;
			
			if (getIsActive())
			{
				switch (getReverserState())
				{
					case FORWARD:
					case NEUTRAL_FORWARD:
					{
						shouldBeWhite = true;
						break;
					}
					
					default:
					{
						break;
					}
				}
			}
			
			if (shouldBeWhite)
			{
				if (!lightMode.get().equals(ELightMode.FRONT))
				{
					lightMode.set(ELightMode.FRONT);
					onUpdateLamps();
				}
			}
			else
			{
				if (!lightMode.get().equals(ELightMode.REAR))
				{
					lightMode.set(ELightMode.REAR);
					onUpdateLamps();
				}
			}
		}
		
	}
	
	/**
	 * Updates cab interaction with the mouse, by hovering over respective
	 * controls.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 */
	@SideOnly(Side.CLIENT)
	protected void onUpdateHovering()
	{
		/*
		 * If the local player isn't passenger of this cab, abort.
		 */
		if (getPassenger() != Minecraft.getMinecraft().thePlayer)
		{
			return;
		}
		
		/* Cache and update player position. */
		EntityLivingBase viewEntity = Minecraft.getMinecraft().renderViewEntity;
		double posX = viewEntity.posX;
		double posY = viewEntity.posY;
		double posZ = viewEntity.posZ;
		getSeat().updateRiderPosition();
		
		/*
		 * Try to determine a selected button from all available properties.
		 */
		PropButton propButtonSelected = null;
		{
			/* Get the local client's view entity. */
			float[] scale = getParent().getScale();
			
			/*
			 * Retrieve view origin (origVector), view vector (lookVector) and get part
			 * orientation vector (partVector).
			 */
			Vec3 origVector = PartHelper.getPosition(getParent()).subtract(viewEntity.getPosition(1.0F));
			Vec3 partVector = PartHelper.getViewVec(getParent(), true);
			Vec3 lookVector = viewEntity.getLook(1.0F).normalize();
			
			/*
			 * Lower the look vector and add the part's local view vector onto the player's
			 * view vector to get the correct view.
			 */
			lookVector.yCoord += (getMountedYOffset() * 0.36 + 0.1625) * scale[1];
			lookVector.rotateAroundY((float) (VehicleHelper.getAngle(null, partVector) * VehicleHelper.RAD_FACTOR));
			
			/*
			 * Iterate through all properties and compare each valid button to the currently
			 * selected button.
			 */
			double minDist = Double.MAX_VALUE;
			for (IPartProperty<?> iPartProperty : getParent().getProperties())
			{
				if (iPartProperty instanceof PropButton)
				{
					/*
					 * Cast the button. Check whether it currently accepts input and if it specifies
					 * a hitbox.
					 */
					PropButton propButton = (PropButton) iPartProperty;
					if (!propButton.isInputAllowed() || propButton.getHitbox() == null)
					{
						continue;
					}
					
					/* Retrieve the button's hitbox and move according to part scale. */
					AxisAlignedBB hitBox = propButton.getHitbox().copy();
					{
						// @formatter:off
						Vec3 pos = VehicleHelper.getPosition(hitBox);
						hitBox.offset((pos.xCoord * scale[0]) - pos.xCoord, (pos.yCoord * scale[1]) - pos.yCoord, (pos.zCoord * scale[2]) - pos.zCoord);
						// @formatter:on
					}
					
					/* Ray trace the button hitbox. */
					MovingObjectPosition movObjPos = hitBox.calculateIntercept(origVector, lookVector);
					if (movObjPos != null)
					{
						double dist = origVector.distanceTo(movObjPos.hitVec);
						if (dist < minDist)
						{
							minDist = dist;
							propButtonSelected = propButton;
						}
					}
				}
			}
		}
		
		/* Reset player position. */
		viewEntity.posX = posX;
		viewEntity.posY = posY;
		viewEntity.posZ = posZ;
		
		/*
		 * Update the hovered button.
		 */
		if (hoveredButton != null)
		{
			/*
			 * If there is a button press ongoing and either the mouse was released or the
			 * hovered button changes, notify button of key unpress.
			 */
			if (isHoverPressed && (!Minecraft.getMinecraft().gameSettings.keyBindAttack.getIsKeyPressed() || hoveredButton != propButtonSelected))
			{
				/* Clear hover flag. */
				isHoverPressed = false;
				
				/* Notify of key change. */
				if (hoveredButton.cabKey != null)
				{
					onClientKeyChange(hoveredButton.cabKey, isHoverPressed);
				}
				else
				{
					hoveredButton.onClientKeyChange(isHoverPressed);
				}
			}
		}
		
		/* Assign new hovered button. */
		hoveredButton = propButtonSelected;
		
		/*
		 * If a button was found and there is no key press ongoing, check whether the
		 * player is pressing the button.
		 */
		if (hoveredButton != null && !isHoverPressed)
		{
			/* If the action key is pressed, notify button. */
			if (Minecraft.getMinecraft().gameSettings.keyBindAttack.getIsKeyPressed())
			{
				/* Set hover flag. */
				isHoverPressed = true;
				
				/* Notify of key change. */
				if (hoveredButton.cabKey != null)
				{
					onClientKeyChange(hoveredButton.cabKey, isHoverPressed);
				}
				else
				{
					hoveredButton.onClientKeyChange(isHoverPressed);
				}
			}
		}
	}
	
	@Override
	public void onUpdateTrain()
	{
		super.onUpdateTrain();
		
		/* Doors */
		if (mtms.autodriveState.get() == EAutodriveState.STATION_ARRIVAL && getTrain().getLastSpeed() == 0.0F)
		{
			ConcatenatedTypesList<PartTypeDoor> doors = getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_DOOR);
			if (!doors.isEmpty())
			{
				for (PartTypeDoor door : doors)
				{
					door.setDoorState(EDoorState.UNLOCK_ALL);
				}
			}
		}
		
		/*
		 * Automatically open unlocked doors if the option has been enabled within MTIS.
		 */
		if (mtms.automaticDoorOpen.get())
		{
			ConcatenatedTypesList<PartTypeDoor> doors = getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_DOOR);
			
			if (!doors.isEmpty())
			{
				for (PartTypeDoor door : doors)
				{
					switch (door.getDoorState())
					{
						case UNLOCK_ALL:
						case UNLOCK_LEFT:
						case UNLOCK_RIGHT:
						{
							door.setOpen(EDoorSide.LEFT, true);
							door.setOpen(EDoorSide.RIGHT, true);
							break;
						}
						default:
							break;
					}
				}
			}
		}
	}
	
	@Override
	public boolean trySetActive()
	{
		if (mtms.autodriveEnable.get() && mtms.autodriveState.get() != EAutodriveState.TERMINATING)
		{
			/*
			 * Get a list of all the cabs in the train
			 */
			ConcatenatedTypesList<PartTypeCabBasic> cabs = getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_CAB);
			
			/*
			 * Get the index of the current active cab (what the train is switching from) and this cab (what the train is trying to switch to) so we can compare them.
			 */
			PartTypeCabBasic frontCab = cabs.get(0);
			PartTypeCabBasic rearCab = cabs.get(cabs.size() - 1);
			
			boolean thisIsFrontCab = this.equals(frontCab);
			boolean thisIsRearCab = this.equals(rearCab);
			
			int activeCabIndex = cabs.indexOf(getTrain().getActiveCab());
			int thisCabIndex = cabs.indexOf(this);
			
			/*
			 * Allow the parent train to change to this cab only if we are at the end of the cabs list.
			 */
			if (thisIsRearCab || thisIsFrontCab)
			{
				return true;
			}
		}
		
		return super.trySetActive();
	}
}
