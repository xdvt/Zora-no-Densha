package zoranodensha.api.vehicles.part.type;

import net.minecraft.entity.player.EntityPlayer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Chain couplings.<br>
 * Couple and decouple manually.
 */
public class PartTypeCouplingChain extends APartTypeCoupling
{
	public PartTypeCouplingChain(VehParBase parent)
	{
		super(parent, PartTypeCouplingChain.class.getSimpleName());
	}

	@Override
	public boolean getIsCompatible(APartTypeCoupling coupling)
	{
		return (coupling instanceof PartTypeCouplingChain);
	}

	@Override
	public boolean onActivated(EntityPlayer player)
	{
		Train train = parent.getTrain();
		if (parent.getIsFinished() && train != null)
		{
			if (player.isSneaking() && !train.worldObj.isRemote)
			{
				return getIsCoupled() ? doTryDecouple() : doTryCouple();
			}
		}
		return false;
	}
}