package zoranodensha.api.vehicles.part.type.cab;

import org.apache.logging.log4j.Level;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.util.SyncDir;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic.ECabKey;
import zoranodensha.api.vehicles.part.type.cab.PropButton.PropButtonScreen;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.core.ModCenter;



/**
 * <h1>Driver's Display Unit</h1>
 * <p>
 * An instance of this class is used to represent a single train management
 * screen in a cab. There may be multiple screens in a cab, and each should be
 * represented with its own DDU instance.
 * </p>
 * <p>
 * This class does not collect any information from the train - instead simply
 * screen functions such as brightness, menus, and information display are
 * handled here.
 * </p>
 * 
 * @author Jaffa
 */
public class DDU
{
	protected final int BUTTON_BRIGHTNESS = 0;
	protected final int BUTTON_CONTRAST = 1;
	protected final int BUTTON_NEXT = 3;
	protected final int BUTTON_SWAP = 4;
	protected final int BUTTON_MENU = 5;
	protected final int BUTTON_UP = 6;
	protected final int BUTTON_LEFT = 7;
	protected final int BUTTON_RIGHT = 8;
	protected final int BUTTON_DOWN = 9;
	protected final int BUTTON_ENTER = 10;


	/**
	 * The parent MTMS instance associated with this DDU. Any information needed
	 * about the train is obtained through this class.
	 */
	public final MTMS mtms;
	/**
	 * The {@code String} name of this DDU that separates it from any other DDU's in the same cab.
	 */
	public final String dduSpecificName;

	/**
	 * The renderer for this screen.
	 */
	private AScreenRenderer renderer;

	/**
	 * An integer used to keep track of when this screen should be updated.
	 */
	@SyncDir(ESyncDir.NOSYNC) @SideOnly(Side.CLIENT) private int repaintPointer;

	/**
	 * This is a flag which, when set to {@code true}, will cause the screen
	 * renderer to redraw at the next available opportunity.
	 */
	private final PropBoolean rendererRepaint;

	/*
	 * Screen Functions
	 */
	public final PropIntegerBounded brightness;
	public final PropBoolean contrast;
	public final PropEnum<EDDUMenu> menu;

	/*
	 * Menu-Specific Items
	 */
	/** The number of carriages in which the information screen is 'scrolled' to the right. */
	public final PropIntegerBounded menuInformationCarriageScroll;
	/** The index of the selected settings screen item. */
	public final PropIntegerBounded menuSettingsSelection;

	/*
	 * Screen Physical Buttons
	 */
	public final PropButtonScreen screenButton_brightness;
	public final PropButtonScreen screenButton_contrast;
	public final PropButtonScreen screenButton_down;
	public final PropButtonScreen screenButton_enter;
	public final PropButtonScreen screenButton_left;
	public final PropButtonScreen screenButton_menu;
	public final PropButtonScreen screenButton_next;
	public final PropButtonScreen screenButton_right;
	public final PropButtonScreen screenButton_swap;
	public final PropButtonScreen screenButton_up;



	/**
	 * Initialises a new instance of the DDU class. An MTMS instance must be
	 * supplied. Ensure there is only one MTMS instance in the cab so all cab
	 * screens display the same information.
	 * 
	 * @param mtms - The MTMS instance of the PartTypeCab containing this screen.
	 * @param dduSpecificName - A unique {@code String} that is used to identify this DDU differently from any other DDU instances in a cab.
	 */
	public DDU(MTMS mtms, String dduSpecificName)
	{
		this(mtms, dduSpecificName, EDDUMenu.MENU_OPERATION);
	}

	/**
	 * Initialises a new instance of the DDU class. An MTMS instance must be
	 * supplied. Ensure there is only one MTMS instance in the cab so all cab
	 * screens display the same information.
	 * 
	 * @param mtms - The MTMS instance of the PartTypeCab containing this screen.
	 * @param dduSpecificName - A unique {@code String} that is used to identify this DDU differently from any other DDU instances in a cab.
	 * @param startingMenu - Sets the menu that will be displayed upon the initialisation of this screen.
	 */
	public DDU(MTMS mtms, String dduSpecificName, EDDUMenu startingMenu)
	{
		// Assign the parent MTMS information system.
		this.mtms = mtms;
		this.dduSpecificName = dduSpecificName;

		/*
		 * Rendering System
		 */
		addProperty(rendererRepaint = (PropBoolean)new PropBoolean(mtms.parentCab.getParent(), getPropertyNameFrom("rendererRepaint")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));

		/*
		 * Screen Functions
		 */
		addProperty(brightness = (PropIntegerBounded)new PropIntegerBounded(mtms.parentCab.getParent(), 0, 3, getPropertyNameFrom("brightness")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(contrast = (PropBoolean)new PropBoolean(mtms.parentCab.getParent(), getPropertyNameFrom("contrast")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(menu = (PropEnum)new PropEnum<EDDUMenu>(mtms.parentCab.getParent(), startingMenu, getPropertyNameFrom("menu")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));

		/*
		 * Menu-Specific Values
		 */
		addProperty(menuInformationCarriageScroll =
				(PropIntegerBounded)new PropIntegerBounded(mtms.parentCab.getParent(), 0, 0, Integer.MAX_VALUE, getPropertyNameFrom("menuInformationCarriageScroll")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(menuSettingsSelection = (PropIntegerBounded)new PropIntegerBounded(mtms.parentCab.getParent(), 0, 0, 4, getPropertyNameFrom("menuSettingsSelection")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));

		/*
		 * Screen Physical Buttons
		 */
		addProperty(screenButton_brightness = new PropButtonScreen(this, "brightness")
		{
			@Override
			protected void onPressed()
			{
				buttonPress(BUTTON_BRIGHTNESS);
			}
		});
		addProperty(screenButton_contrast = new PropButtonScreen(this, "contrast")
		{
			@Override
			protected void onPressed()
			{
				buttonPress(BUTTON_CONTRAST);
			}
		});
		addProperty(screenButton_down = new PropButtonScreen(this, "down")
		{
			protected void onPressed()
			{
				buttonPress(BUTTON_DOWN);
			};
		});
		addProperty(screenButton_enter = new PropButtonScreen(this, "enter")
		{
			@Override
			protected void onPressed()
			{
				buttonPress(BUTTON_ENTER);
			}
		});
		addProperty(screenButton_left = new PropButtonScreen(this, "left")
		{
			@Override
			protected void onPressed()
			{
				buttonPress(BUTTON_LEFT);
			}
		});
		addProperty(screenButton_menu = new PropButtonScreen(this, "menu")
		{
			@Override
			protected void onPressed()
			{
				buttonPress(BUTTON_MENU);
			}
		});
		addProperty(screenButton_next = new PropButtonScreen(this, "next")
		{
			@Override
			protected void onPressed()
			{
				buttonPress(BUTTON_NEXT);
			}
		});
		addProperty(screenButton_right = new PropButtonScreen(this, "right")
		{
			@Override
			protected void onPressed()
			{
				buttonPress(BUTTON_RIGHT);
			}
		});
		addProperty(screenButton_swap = new PropButtonScreen(this, "swap")
		{
			@Override
			protected void onPressed()
			{
				buttonPress(BUTTON_SWAP);
			}
		});
		addProperty(screenButton_up = new PropButtonScreen(this, "up")
		{
			@Override
			protected void onPressed()
			{
				buttonPress(BUTTON_UP);
			}
		});
	}

	/**
	 * Helper method to add a property to the parent cab vehicle part.
	 * 
	 * @param property - The property to add.
	 */
	private void addProperty(APartProperty property)
	{
		mtms.parentCab.getParent().addProperty(property);
	}


	/**
	 * Handles screen button events.
	 * 
	 * @param buttonCode - The {@code int} code of the screen button.
	 */
	private void buttonPress(int buttonCode)
	{
		repaint();

		/*
		 * A switch for the brightness/contrast buttons, which are always able to be pressed.
		 */
		switch (buttonCode)
		{
			/*
			 * Cycle the brightness.
			 */
			case BUTTON_BRIGHTNESS: {
				if (brightness.get() >= 2 || brightness.get() < 0)
				{
					brightness.set(0);
				}
				else
				{
					brightness.set(brightness.get() + 1);
				}
				return;
			}

			/*
			 * Toggle the screen contrast (day/night mode).
			 */
			case BUTTON_CONTRAST: {
				contrast.set(!contrast.get());
				return;
			}

			/*
			 * Switch to the settings screen.
			 */
			case BUTTON_MENU: {
				if (menu.get().equals(EDDUMenu.MENU_SETTINGS))
				{
					menu.set(EDDUMenu.MENU_OPERATION);
				}
				else
				{
					menu.set(EDDUMenu.MENU_SETTINGS);
				}
				return;
			}
		}

		/*
		 * At this point, the button was not the brightness or contrast button, so move on to the rest of the menu functionality.
		 */
		switch (menu.get())
		{
			case MENU_INFORMATION: {
				switch (buttonCode)
				{
					case BUTTON_ENTER: {
						menu.set(EDDUMenu.MENU_INFORMATION_DETAILED);
						break;
					}

					case BUTTON_LEFT: {
						menuInformationCarriageScroll.set(menuInformationCarriageScroll.get() - 1);
						break;
					}

					case BUTTON_RIGHT: {
						if (menuInformationCarriageScroll.get() < mtms.parentCab.getTrain().getVehicleCount() - 1)
						{
							menuInformationCarriageScroll.set(menuInformationCarriageScroll.get() + 1);
						}
						else
						{
							menuInformationCarriageScroll.set(mtms.parentCab.getTrain().getVehicleCount() - 1);
						}
						break;
					}

					case BUTTON_SWAP: {
						menu.set(EDDUMenu.MENU_OPERATION);
						break;
					}
				}
				break;
			}

			case MENU_INFORMATION_DETAILED: {
				switch (buttonCode)
				{
					case BUTTON_ENTER: {
						menu.set(EDDUMenu.MENU_INFORMATION);
						break;
					}

					case BUTTON_LEFT: {
						menuInformationCarriageScroll.set(menuInformationCarriageScroll.get() - 1);
						break;
					}

					case BUTTON_RIGHT: {
						if (menuInformationCarriageScroll.get() < mtms.parentCab.getTrain().getVehicleCount() - 1)
						{
							menuInformationCarriageScroll.set(menuInformationCarriageScroll.get() + 1);
						}
						else
						{
							menuInformationCarriageScroll.set(mtms.parentCab.getTrain().getVehicleCount() - 1);
						}
						break;
					}

					case BUTTON_SWAP: {
						menu.set(EDDUMenu.MENU_OPERATION);
						break;
					}
				}
				break;
			}

			case MENU_SETTINGS: {
				switch (buttonCode)
				{
					case BUTTON_SWAP: {
						menu.set(EDDUMenu.MENU_OPERATION);
						break;
					}

					case BUTTON_UP: {
						menuSettingsSelection.set(menuSettingsSelection.get() - 1);
						break;
					}

					case BUTTON_DOWN: {
						menuSettingsSelection.set(menuSettingsSelection.get() + 1);
						break;
					}

					case BUTTON_ENTER: {
						switch (menuSettingsSelection.get())
						{
							case 0: {
								mtms.automaticDoorBrake.set(!mtms.automaticDoorBrake.get());
								break;
							}

							case 1: {
								mtms.automaticDoorOpen.set(!mtms.automaticDoorOpen.get());
								break;
							}

							case 2: {
								mtms.imperial.set(!mtms.imperial.get());
								break;
							}

							case 3: {
								mtms.isolateMTCS.set(!mtms.isolateMTCS.get());
								break;
							}

							case 4: {
								boolean newAutodriveEnable = !mtms.autodriveEnable.get();

								ConcatenatedTypesList<PartTypeCabBasic> basicCabs = mtms.parentCab.getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_CAB);

								for (PartTypeCabBasic basicCab : basicCabs)
								{
									if (basicCab instanceof PartTypeCab)
									{
										PartTypeCab cab = (PartTypeCab)basicCab;

										cab.mtms.autodriveEnable.set(newAutodriveEnable);
									}
								}
								break;
							}
						}
						break;
					}
				}
				break;
			}

			case MENU_OPERATION: {
				switch (buttonCode)
				{
					case BUTTON_ENTER: {
						menu.set(EDDUMenu.MENU_OPERATION_DETAILED);
						break;
					}

					case BUTTON_SWAP: {
						menu.set(EDDUMenu.MENU_INFORMATION);
						break;
					}
				}
				break;
			}

			case MENU_OPERATION_DETAILED: {
				switch (buttonCode)
				{
					case BUTTON_ENTER: {
						menu.set(EDDUMenu.MENU_OPERATION);
						break;
					}

					case BUTTON_SWAP: {
						menu.set(EDDUMenu.MENU_INFORMATION);
						break;
					}
				}
				break;
			}

			/*
			 * Unknown/Unhandled Menu
			 */
			default: {
				break;
			}
		}
	}


	/**
	 * Helper method to get an appropriate name for a property based on the supplied
	 * word. For example,buttonCode
	 * 
	 * @param name - A word specific to the property you are attempting to get a
	 *            name for.
	 * @return - A generated string similar to {@code mtms.yourProperty}.
	 */
	private String getPropertyNameFrom(String name)
	{
		return getClass().getSimpleName().toLowerCase() + "." + dduSpecificName + "." + name;
	}

	/**
	 * Gets the renderer currently associated with this screen.
	 * 
	 * @return - An {@link zoranodensha.api.vehicles.part.type.cab.AScreenRenderer}
	 *         instance.
	 */
	public AScreenRenderer getScreenRenderer()
	{
		return renderer;
	}

	/**
	 * Returns whether this screen will accept input from screen buttons. This
	 * should return {@code false} when the screen is not ready to accept input,
	 * e.g. when loading.
	 * 
	 * @return - A {@code boolean}.
	 */
	public boolean isInputAllowed()
	{
		return true;
	}

	/**
	 * Called during client and server update ticks. If possible this method should
	 * be called after updating the parent MTMS instance.
	 */
	public void onUpdate()
	{
		/*
		 * Create a train instance and also perform a null check.
		 */
		Train train = mtms.parentCab.getTrain();
		if (train == null)
		{
			return;
		}

		/*
		 * Client Side
		 */
		if (train.worldObj.isRemote)
		{

			/*
			 * Check the repaint pointer.
			 */
			if (mtms.repaintPointer != this.repaintPointer)
			{
				repaint();
				this.repaintPointer = mtms.repaintPointer;
			}

			/*
			 * If it has been requested to do so, repaint the screen.
			 */
			if (rendererRepaint.get())
			{
				if (renderer != null)
				{
					if (Minecraft.getMinecraft().thePlayer.equals(mtms.parentCab.getPassenger()))
					{
						renderer.onRepaint();
					}
				}

				rendererRepaint.set(false);
			}
		}

		/*
		 * Server Side
		 */
		else
		{

		}
	}

	/**
	 * Call this method to trigger a redraw of the screen.
	 */
	public void repaint()
	{
		rendererRepaint.set(true);
	}

	/**
	 * Assigns a new screen renderer to this screen.
	 * 
	 * @param renderer - The new renderer to assign.
	 */
	public void setScreenRenderer(AScreenRenderer renderer)
	{
		this.renderer = renderer;
	}

}
