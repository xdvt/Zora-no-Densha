package zoranodensha.api.vehicles.part.type;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;



/**
 * Engines of any kind.<br>
 * These types take a source of power from within their parent {@link zoranodensha.api.vehicles.VehicleData vehicle}
 * (or, in some cases, {@link zoranodensha.api.vehicles.Train train}) and produce force from the source's power supply.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class APartTypeEngine extends APartType implements IPartType_Tickable
{
	public APartTypeEngine(VehParBase parent, String name)
	{
		super(parent, name);
	}


	/**
	 * <p>
	 * Return the amount of force (measured in kiloNewton, kN) produced at the moment.
	 * </p>
	 * 
	 * <p>
	 * <i>Called on server worlds only.</i>
	 * </p>
	 *
	 * @return The produced force in kiloNewton (kN). Negative values indicate backward movement.
	 */
	public abstract float getProducedForce();
}