package zoranodensha.api.vehicles.part.type.seat;

import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.type.PartTypeSeat;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * Special type of entity which acts a rideable ghost entity.
 */
public class EntitySeat extends Entity implements IEntityAdditionalSpawnData
{
	/** Bounding box to avoid collision detection, globally used by every seat entity. */
	private static final AxisAlignedBB noCollBox = new AxisAlignedBB(0, 0, 0, 0, 0, 0)
	{
		@Override
		public boolean intersectsWith(AxisAlignedBB aabb)
		{
			return false;
		}

		@Override
		public boolean isVecInside(Vec3 vec)
		{
			return false;
		}
	};

	/** The parent type of this seat entity. May temporarily be {@code null}. */
	private PartTypeSeat parentType;



	public EntitySeat(PartTypeSeat parentType)
	{
		this(parentType.getTrain().worldObj);
		setPartType(parentType);
	}

	public EntitySeat(World world)
	{
		super(world);
		noClip = true;
	}

	@Override
	public boolean canBeCollidedWith()
	{
		return false;
	}

	@Override
	public boolean canBePushed()
	{
		return false;
	}

	@Override
	protected void entityInit()
	{
		;
	}

	@Override
	public void fall(float distance)
	{
		/* Overridden to get public method access. */
		super.fall(distance);
	}

	@Override
	public AxisAlignedBB getBoundingBox()
	{
		return noCollBox;
	}

	@Override
	public double getMountedYOffset()
	{
		return (parentType != null ? -parentType.getMountedYOffset() : 0.0D);
	}

	/**
	 * @return The {@link PartTypeSeat part type} which is parent of this Entity.
	 */
	public PartTypeSeat getPartType()
	{
		return parentType;
	}

	@Override
	public boolean handleLavaMovement()
	{
		return false;
	}

	@Override
	public boolean handleWaterMovement()
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean isInRangeToRender3d(double x, double y, double z)
	{
		if (getPartType() != null)
		{
			Train parent = getPartType().getTrain();
			if (parent != null)
			{
				return parent.isInRangeToRender3d(x, y, z);
			}
		}
		return super.isInRangeToRender3d(x, y, z);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean isInRangeToRenderDist(double dist)
	{
		if (getPartType() != null)
		{
			Train parent = getPartType().getTrain();
			if (parent != null)
			{
				return parent.isInRangeToRenderDist(dist);
			}
		}
		return super.isInRangeToRenderDist(dist);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void onUpdate()
	{
		/* Kill this entity if it's either not needed anymore, or if connection to the parent train is lost. */
		if (!worldObj.isRemote)
		{
			if (riddenByEntity == null)
			{
				setDead();
			}
			else if (getPartType() == null || getPartType().getTrain() == null || getPartType().getTrain().isDead)
			{
				setDead();
			}
			else if (getPartType().getSeat() != this && !getPartType().setSeat(this))
			{
				setDead();
			}
		}

		/*
		 * This is a workaround to an issue where seats would update before the parent train, resulting in spazzy movement.
		 * Appending this seat entity to the back of the loaded entity list seems to resolve the issue.
		 */
		if (parentType != null)
		{
			Train parentTrain = parentType.getTrain();
			if (!parentTrain.isDead)
			{
				if (worldObj.loadedEntityList.indexOf(this) < worldObj.loadedEntityList.indexOf(parentTrain))
				{
					if (worldObj.loadedEntityList.remove(this))
					{
						worldObj.loadedEntityList.add(this);
					}
				}
			}
		}
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound nbt)
	{
		;
	}

	@Override
	public void readSpawnData(ByteBuf bbuf)
	{
		if (!bbuf.readBoolean())
		{
			return;
		}

		int entityID = bbuf.readInt();
		int vehicleID = bbuf.readInt();
		int partID = bbuf.readInt();
		int seatEntity_ID = bbuf.readInt();

		Entity entity = worldObj.getEntityByID(entityID);
		if (entity instanceof Train)
		{
			VehParBase part = ((Train)entity).getPartFromID(vehicleID, partID);
			if (part != null)
			{
				for (IPartProperty<?> prop : part.getProperties())
				{
					if (prop.getKey() == seatEntity_ID && prop instanceof PropSeat)
					{
						setPartType(((PropSeat)prop).getParentType());
					}
				}
			}
		}
	}

	/**
	 * Overridden to remove references to vehicle parts if this entity is set dead.
	 */
	@Override
	public void setDead()
	{
		super.setDead();

		if (parentType != null)
		{
			if (parentType.getSeat() == this)
			{
				parentType.setSeat(null);
			}
			parentType = null;
		}
	}

	/**
	 * Setter for the part field.
	 */
	public void setPartType(PartTypeSeat parentType)
	{
		if (parentType != null)
		{
			AxisAlignedBB aabb = parentType.getParent().getBoundingBox();
			Vec3 pos = VehicleHelper.getPosition(aabb);

			float width = (float)((aabb.maxX - aabb.minX) + (aabb.maxZ - aabb.minZ)) * 0.5F;
			float height = (float)(aabb.maxY - aabb.minY) * 0.5F;

			setSize(width, height);
			setPosition(pos.xCoord, pos.yCoord, pos.zCoord);

			if (parentType.getSeat() != this)
			{
				parentType.setSeat(this);
			}
		}
		this.parentType = parentType;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void setPositionAndRotation2(double posX, double posY, double posZ, float rotationYaw, float rotationPitch, int i)
	{
		setPosition(posX, posY, posZ);
		setRotation(rotationYaw, rotationPitch);
	}

	@Override
	public void updateRiderPosition()
	{
		if (riddenByEntity != null)
		{
			if (parentType != null)
			{
				riddenByEntity.setPosition(posX, posY + (getMountedYOffset() + riddenByEntity.getYOffset()) * parentType.getParent().getScale()[1], posZ);
			}
			else
			{
				riddenByEntity.setPosition(posX, posY + (getMountedYOffset() + riddenByEntity.getYOffset()), posZ);
			}
		}
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound nbt)
	{
		;
	}

	@Override
	public boolean writeMountToNBT(NBTTagCompound nbt)
	{
		/* Seats should not be written as mounts. */
		return false;
	}

	@Override
	public void writeSpawnData(ByteBuf bbuf)
	{
		if (parentType != null && parentType.getTrain() != null)
		{
			/* Write key of #seatEntity as a way to identify which part type this entity belongs to on the other end. */
			bbuf.writeBoolean(true);
			bbuf.writeInt(parentType.getTrain().getEntityId());
			bbuf.writeInt(parentType.getParent().getVehicle().getID());
			bbuf.writeInt(parentType.getParent().getID());
			bbuf.writeInt(parentType.seat.getKey());
		}
		else
		{
			bbuf.writeBoolean(false);
		}
	}

	@Override
	public boolean writeToNBTOptional(NBTTagCompound nbt)
	{
		return false;
	}
}