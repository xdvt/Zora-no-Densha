package zoranodensha.api.vehicles.part.type;

import java.util.List;

import org.apache.logging.log4j.Level;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.GuiPlayerInfo;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Vec3;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.handlers.MovementHandler;
import zoranodensha.api.vehicles.handlers.PneumaticsHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropBreaker;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.property.PropVec3d;
import zoranodensha.api.vehicles.part.property.PropVec3f;
import zoranodensha.api.vehicles.part.property.PropVessel;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_PositionListener;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.core.ModCenter;



/**
 * Vehicle parts of this type guide trains on a track - be it a bogie of one or more axles, or a magnet of a Maglev.<br>
 * <br>
 * This part type stores current movement and position data, which is used and updated by the {@link zoranodensha.api.vehicles.handlers.MovementHandler MovementHandler}.<br>
 * It also synchronises {@link #getPosX() position data} (on all axes X, Y, Z) and the {@link #getDirection() direction vector}, as well as various other data.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PartTypeBogie extends APartType implements IPartType_PositionListener, IPartType_Tickable
{
	/**
	 * Time (in milliseconds) per movement update step.<br>
	 * Initialised by Zora no Densha, can be configured in the mod's respective configuration category.<br>
	 * Lower values may result in smoother movement on clients due to more steps being taken, but may result in slower responses to movement.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 */
	@SideOnly(Side.CLIENT) protected static int MOVEMENT_TIME;


	/*
	 * Non-configurable properties (only via XML / constructor)
	 */
	/** Default local Y-offset of this bogie. Will be scaled against Y-scale and is <b>not</b> configurable. */
	public final PropFloatBounded localY;
	
	/*
	 * Configurable properties
	 */
	/** Maximum brake force, in kiloNewtons (kN). */
	public final PropIntegerBounded brakeForce;
	/** Maximum speed (in km/h). By default {@code 160}. */
	public final PropIntegerBounded maxSpeed;
	/** Maximum tractive effort, in kiloNewtons (kN). Only used if this bogie {@link #getHasMotor() has a motor}. */
	public final PropIntegerBounded tractiveEffort;
	/** {@code true} if the bogie has brakes. */
	public final PropBoolean hasBrakes;
	/** {@code true} if this bogie has an EP Brake unit, allowing for EP brakes on this bogie. If set to {@code false}, only pneumatic brakes will be available on this bogie. */
	public final PropBoolean hasEPUnit;
	/** {@code true} if the bogie has a motor. */
	public final PropBoolean hasMotor;
	/** {@code true} if this bogie is a dummy bogie and will not be used for path finding. */
	public final PropBoolean isDummy;

	/*
	 * Dynamic properties, synchronised.
	 */
	/**
	 * A reservoir which is charged when the auto brakes are released and is used to apply the auto brakes.
	 */
	public final PropVessel pneumaticsAuxiliaryReservoir;
	/**
	 * A chamber which, when pressurised, pushes against the brake shoes and applies the brakes.
	 */
	public final PropVessel pneumaticsBrakeCylinder;
	/**
	 * The auxiliary reservoir is charged from this pipe. When a reduction of air pressure in this pipe is detected by the triple valve, the brakes will be applied.<br>
	 * This pipe is continous along the length of the train (throughout all bogies).
	 */
	public final PropVessel pneumaticsBrakePipe;
	/**
	 * A small reservoir connected to the triple valve which is used to remember the fully-charged brake pipe pressure, in order to allow for a graduated release of the pneumatic brakes.
	 */
	public final PropVessel pneumaticsControlReservoir;
	/**
	 * A cylinder, not connected to the physical braking system, which is pressurised by the triple valve in order to apply the penumatic brakes. Refer to two-pipe pneumatic braking systems for
	 * further information.
	 */
	public final PropVessel pneumaticsDummyBrakeCylinder;
	/**
	 * The pipe which runs between the output of the EP brake unit and the double check valve just before the brake cylinder.
	 */
	public final PropVessel pneumaticsEPPipe;
	/**
	 * The brake cylinders receive their air pressure from this pipe during EP braking.<br>
	 * This pipe is continuous along the length of the train (throughout all bogies).
	 */
	public final PropVessel pneumaticsMainReservoirPipe;
	/**
	 * A chamber which, when pressurised, pushes against the spring parking brake mechanism and releases the parking brakes. If this chamber loses its air pressure, the parking brakes will apply.
	 */
	public final PropVessel pneumaticsParkBrakeCylinder;
	/**
	 * A reservoir which is charged by the main reservoir pipe. When pneumatic brakes are applied, air from this reservoir flows into the brake cylinder.
	 */
	public final PropVessel pneumaticsSupplementaryReservoir;
	/**
	 * The pipe which runs between the output of the triple valve and the double check valve just before the brake cylinder.
	 */
	public final PropVessel pneumaticsTripleValvePipe;

	/**
	 * When this breaker is set, main reservoir pipe air is allowed to flow into the park brake cylinder, thus releasing the spring parking brake. Trip this breaker to vent the park brake cylinders
	 * and apply the spring parking brake.
	 */
	public final PropBreaker breakerParkBrakeRelease;


	/** The bogie's {@link #getDirection() direction vector}. */
	public final PropVec3d dir;
	/** The bogie's {@link #getPosX() in-world position}. */
	public final PropVec3d serverPos;
	/** The bogie's {@link #getRotationYaw() in-world rotation}. */
	public final PropVec3f worldRotation;

	/*
	 * Dynamic properties, not synchronised.
	 */
	/**
	 * A bounded floating point number between {@code -1.0F} and {@code 1.0F} representing the current state of the electro-pneumatic brake valve. A value between {@code -1.0F and <0.0F} represents
	 * releasing the EP brakes. {@code >0.0F to 1.0F} represents applying the EP brakes. {@code 0.0F} represents lapped (unchanging) EP brakes.
	 */
	public final PropFloatBounded epValveActivity;
	/** This property is used to bypass the pneumatic braking system to allow for arcade-style braking without the use of the pneumatics system. */
	public final PropFloatBounded brakeArcade;
	/** This property is used to bypass the pneumatic braking system to allow for arcade-style braking without the use of the pneumatics system. */
	public final PropFloatBounded brakeArcadeTarget;
	/** Brake surface temperature, in degrees Celsius (°C). */
	public final PropFloat brakeTempSurface;
	/** Brake core temperature, in degrees Celsius (°C). */
	public final PropFloat brakeTempCore;
	/** The bogie's {@link #getPrevPosX() previous in-world position}. */
	public final PropVec3d prevPos;
	/** The bogie's {@link #getPrevRotationYaw() previous in-world rotation}. */
	public final PropVec3f prevWorldRotation;
	
	public final PropBoolean onSwitch;
	public final PropBoolean onCurve;

	/*
	 * Sounds
	 */
	/** Sound which is played when the park brake breaker is set on this bogie. */
	public PropSound sound_parkBrakeApply;
	/** Sound which is played when the park brake breaker is tripped on this bogie. */
	public PropSound sound_parkBrakeRelease;

	/*
	 * Client-only fields.
	 */
	/** Client-sided position of this bogie. <i>Client-side only.</i> */
	@SideOnly(Side.CLIENT) protected Vec3 clientPos;
	/** Client-sided offset applied to the {@link #clientPos client position} each tick, to eventually match the {@link #serverPos server position}. <i>Client-side only.</i> */
	@SideOnly(Side.CLIENT) protected Vec3 moveDistPerTick;
	/** Client-sided reference to the last tick's {@link #serverPos server position}. <i>Client-side only.</i> */
	@SideOnly(Side.CLIENT) protected Vec3 prevServerPos;
	/** Client-sided counter for calls to {@link #moveDistPerTick}. <i>Client-side only.</i> */
	@SideOnly(Side.CLIENT) protected int moveStepCounter;



	public PartTypeBogie(VehParBase parent)
	{
		super(parent, PartTypeBogie.class.getSimpleName());

		/*
		 * Pneumatics
		 */
		addProperty(pneumaticsAuxiliaryReservoir = (PropVessel)new PropVessel(parent, 10.0F, 1000.0F, 0.0F, "pneumatics_auxiliaryReservoir").setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(pneumaticsBrakeCylinder = (PropVessel)new PropVessel(parent, 10.0F, 325.0F, 0.0F, "pneumatics_brakeCylinder").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(pneumaticsBrakePipe = (PropVessel)new PropVessel(parent, 6.0F, 1000.0F, 0.0F, "pneumatics_brakePipe").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(pneumaticsControlReservoir = (PropVessel)new PropVessel(parent, 1.0F, 750.0F, 0.0F, "pneumatics_controlReservoir").setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(pneumaticsDummyBrakeCylinder = (PropVessel)new PropVessel(parent, 10.0F, 325.0F, 0.0F, "pneumatics_dummyBrakeCylinder").setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(pneumaticsEPPipe = (PropVessel)new PropVessel(parent, 2.0F, 325.0F, 0.0F, "pneumatics_epPipe").setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(pneumaticsMainReservoirPipe = (PropVessel)new PropVessel(parent, 6.0F, 1000.0F, 0.0F, "pneumatics_mainReservoirPipe").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(pneumaticsParkBrakeCylinder = (PropVessel)new PropVessel(parent, 3.0F, 325.0F, 0.0F, "pneumatics_parkBrakeCylinder").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(pneumaticsSupplementaryReservoir = (PropVessel)new PropVessel(parent, 20.0F, 1000.0F, 0.0F, "pneumatics_supplementaryReservoir").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(pneumaticsTripleValvePipe = (PropVessel)new PropVessel(parent, 2.0F, 325.0F, 0.0F, "pneumatics_tripleValvePipe").setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(epValveActivity = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, -1.0F, 1.0F, "pneumatics_epValveActivity").setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET_SAVE));

		/*
		 * Breaker switch to release the park brake.
		 */
		addProperty(breakerParkBrakeRelease = (PropBreaker)new PropBreaker(parent, true, "breaker_parkBrakeRelease")
		{
			@Override
			public void set()
			{
				if (!get())
				{
					if (sound_parkBrakeRelease != null && pneumaticsParkBrakeCylinder.getPressure() < 60.0F)
					{
						sound_parkBrakeRelease.play();
					}
				}

				super.set();
			};

			@Override
			public void trip()
			{
				if (get())
				{
					if (sound_parkBrakeApply != null && pneumaticsMainReservoirPipe.getPressure() > 100.0F)
					{
						sound_parkBrakeApply.play();
					}
				}

				super.trip();
			};
		}.setValidTagCalls(EValidTagCalls.PACKET_SAVE));

		/*
		 * Configurable brake data.
		 * Only visible if the value wrapped by #prop_hasBrakes is true.
		 */
		addProperty(brakeForce = (PropIntegerBounded)new PropIntegerBounded(parent, 1, 10000, "brakeForce")
		{
			@Override
			public boolean getIsConfigurable()
			{
				/*
				 * We can dynamically determine whether a property may be configured,
				 * e.g. if it is dependent on another property.
				 * 
				 * NOTE:
				 * The property (and its value, obviously) still exist, even if they can't be configured.
				 */
				return hasBrakes.get();
			}
		}.setConfigurable());

		/*
		 * Configurable motor properties.
		 * Only visible if the value wrapped by #hasBrakes is true.
		 */
		addProperty(hasMotor = (PropBoolean)new PropBoolean(parent, "hasMotor")
		{
			@Override
			public Boolean get()
			{
				return super.get() && getIsConfigurable();
			}

			@Override
			public boolean getIsConfigurable()
			{
				return hasBrakes.get();
			}
		}.setConfigurable());

		addProperty(tractiveEffort = (PropIntegerBounded)new PropIntegerBounded(parent, 0, 10000, "tractiveEffort")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return hasMotor.get();
			}
		}.setConfigurable());

		/*
		 * Configurable general properties.
		 * This bogie type may only have brakes if it isn't a dummy.
		 */
		addProperty(isDummy = (PropBoolean)new PropBoolean(parent, "isDummy").setConfigurable());
		addProperty(hasBrakes = (PropBoolean)new PropBoolean(parent, true, "hasBrakes")
		{
			@Override
			public Boolean get()
			{
				return super.get() && getIsConfigurable();
			}

			@Override
			public boolean getIsConfigurable()
			{
				return !isDummy.get();
			}
		}.setConfigurable());

		addProperty(hasEPUnit = (PropBoolean)new PropBoolean(parent, true, "hasEP")
		{
			@Override
			public Boolean get()
			{
				return super.get() && getIsConfigurable();
			}

			public boolean getIsConfigurable()
			{
				return hasBrakes.get();
			}
		}.setConfigurable());

		addProperty(maxSpeed = (PropIntegerBounded)new PropIntegerBounded(parent, 160, 10, 1000, "maxSpeed")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return !isDummy.get();
			}

			@Override
			public boolean set(Object property)
			{
				if (super.set(property))
				{
					Train train = getParent().getTrain();
					if (train != null)
					{
						train.refreshProperties();
					}
					return true;
				}
				return false;
			}
		}.setConfigurable());

		/*
		 * Non-configurable properties
		 */
		addProperty(brakeArcade = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 1.0F, "brakeArcade").setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(brakeArcadeTarget = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 1.0F, "brakeArcadeTarget").setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(dir = (PropVec3d)new PropVec3d(parent, "dir").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(localY = new PropFloatBounded(parent, 0.0F, -4.0F, 4.0F, "localY")
		{
			@Override
			protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
			{
				if (nbt.hasKey(nbtKey))
				{
					set(nbt.getFloat(nbtKey));
					return true;
				}
				return false;
			}
		});
		addProperty(prevPos = (PropVec3d)new PropVec3d(parent, "prevPos").setValidTagCalls(EValidTagCalls.SAVE));
		addProperty(prevWorldRotation = (PropVec3f)new PropVec3f(parent, "prevWorldRotation").setValidTagCalls(EValidTagCalls.SAVE));
		
		// XXX
		addProperty(onSwitch = (PropBoolean) new PropBoolean(parent, "bogieOnSwitch").setSyncDir(ESyncDir.CLIENT));
		addProperty(onCurve = (PropBoolean) new PropBoolean(parent, "bogieOnCurve").setSyncDir(ESyncDir.CLIENT));

		addProperty(serverPos = (PropVec3d)new PropVec3d(parent, "serverPos")
		{
			@Override
			public boolean set(Object property)
			{
				if (super.set(property))
				{
					/* On client, assign previous position to current position. */
					if (isClient() && clientPos == null)
					{
						setClientPos(get().xCoord, get().yCoord, get().zCoord);
					}

					/* Update bounding box on either side. */
					getParent().getBoundingBox().setBB(getParent().initBoundingBox().offset(get().xCoord, get().yCoord, get().zCoord));
					return true;
				}
				return false;
			}
		}.setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));

		addProperty(worldRotation = (PropVec3f)new PropVec3f(parent, "worldRotation")
		{
			@Override
			public boolean set(Object property)
			{
				float[] prev = get();
				if (super.set(property))
				{
					prevWorldRotation.set(prev != null ? prev : property);
					return true;
				}
				return false;
			}
		}.setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));

		addProperty(brakeTempSurface = (PropFloat)new PropFloat(parent, 22.0F, "brakeTempSurface").setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(brakeTempCore = (PropFloat)new PropFloat(parent, 22.0F, "brakeTempCore").setValidTagCalls(EValidTagCalls.PACKET_SAVE));
	}


	/**
	 * Called to handle acceleration during update routines in {@link MovementHandler#calculateSpeed(zoranodensha.api.vehicles.Train) calculateSpeed()}.<br>
	 * <br>
	 * Handle possible effects (such as wheelslip) here.<br>
	 * <br>
	 * Only called if {@link #getHasMotor()} returns {@code true}, indicating this bogie has a motor to convert generated force into acceleration force.
	 *
	 * @param force - The generated force (in kiloNewton - kN), may be negative to indicate backward movement.
	 * @return The remaining force usable for acceleration, in kiloNewton (kN), negative to indicate backward movement.
	 */
	public float getAccelerationForce(float force)
	{
		/* Don't do anything if this bogie has no motor. */
		if (!getHasMotor())
		{
			return 0.0F;
		}

		return force;
	}

	/**
	 * Gets the braking performance of this bogie, in percent.<br>
	 * The returned value is affected by the braking material's temperature (to simulate brake fade).
	 * 
	 * @return Percentage of brake effectiveness, ranging from {@code 0.1 = 10%} to {@code 1.0 = 100%}.
	 */
	protected float getBrakeEffectiveness()
	{
		/* Temperature of minimum efficiency decrease. */
		final float START = 190.0F;
		/* Temperature of maximum efficiency decrease. */
		final float END = 280.0F;

		if (brakeTempSurface.get() > START)
		{
			float effectiveness = 1.0F;
			effectiveness -= ((brakeTempSurface.get() - START) / (END - START));
			return (effectiveness < 0.1F) ? 0.1F : (effectiveness > 1.0F) ? 1.0F : effectiveness;
		}
		return 1.0F;
	}

	/**
	 * Return the deceleration force of this bogie.
	 *
	 * @return The brake force of this bogie, in kiloNewton (kN)
	 */
	public float getDecelerationForce()
	{
		if (!getHasBrakes())
		{
			return 0.0F;
		}

		/* If the arcade brake is being used, use its value as a priority over the pneumatic braking system. */
		if (brakeArcade.get() > 0.0F)
		{
			return brakeArcade.get() * getBrakeEffectiveness() * (float)brakeForce.get(); // [kN]
		}

		/* Get the force exerted by both the service brake cylinders and the park brake cylinders. */
		float brakeCylinderForce = (pneumaticsBrakeCylinder.getPressure() / pneumaticsBrakeCylinder.getMaximumPressure()) * (float)brakeForce.get() * getBrakeEffectiveness();
		float parkBrakeCylinderForce = ((pneumaticsParkBrakeCylinder.getMaximumPressure() - pneumaticsParkBrakeCylinder.getPressure()) / pneumaticsParkBrakeCylinder.getMaximumPressure()) * (float)brakeForce.get() * 0.25F * getBrakeEffectiveness();

		/* Cap the total amount of possible brake force with the configurable part value (brakeForce). */
		float totalForce = brakeCylinderForce + parkBrakeCylinderForce;
		if (totalForce > brakeForce.get())
		{
			totalForce = brakeForce.get();
		}

		return totalForce; // [kN]
	}

	/**
	 * Return this bogie's normalised local direction vector, relative to the
	 * bogie's in-world position.
	 *
	 * @return The normalised direction {@link net.minecraft.util.Vec3 vector}.
	 */
	public Vec3 getDirection()
	{
		return dir.get();
	}

	/**
	 * Returns whether this is a non-dummy bogie with brakes.
	 * 
	 * @return {@code true} if this bogie {@link #hasBrakes has brakes} and is not a {@link #getIsDummy() dummy} bogie.
	 */
	protected boolean getHasBrakes()
	{
		return (!getIsDummy() && hasBrakes.get());
	}

	/**
	 * Return whether this bogie has a motor and thus can convert supplied tractive forces into movement.
	 * 
	 * @return {@code true} if this is bogie has a motor.
	 */
	public boolean getHasMotor()
	{
		return hasMotor.get();
	}

	/**
	 * Return whether this bogie is a dummy bogie.<br>
	 * <br>
	 * Dummy bogies are treated like normal parts and don't influence path finding or section determiniation.<br>
	 * Places where dummy bogies may be used are e.g. locomotives with three bogies; the center bogie would be a dummy then.
	 * 
	 * @return {@code true} if this bogie is a dummy bogie.
	 */
	public boolean getIsDummy()
	{
		return isDummy.get();
	}

	/**
	 * Return the maximum speed (in km/h) this bogie can handle.
	 */
	public int getMaxSpeed()
	{
		return maxSpeed.get();
	}

	/**
	 * Return the default offset of this bogie along local Y-axis.
	 */
	public float getOffsetY()
	{
		return localY.get();
	}

	/**
	 * Return the in-world X-position of this bogie.
	 */
	public double getPosX()
	{
		return (isClient() && clientPos != null) ? clientPos.xCoord : serverPos.get().xCoord;
	}

	/**
	 * Return the in-world Y-position of this bogie.
	 */
	public double getPosY()
	{
		return (isClient() && clientPos != null) ? clientPos.yCoord : serverPos.get().yCoord;
	}

	/**
	 * Return the in-world Z-position of this bogie.
	 */
	public double getPosZ()
	{
		return (isClient() && clientPos != null) ? clientPos.zCoord : serverPos.get().zCoord;
	}

	/**
	 * Return the previous tick's in-world X-position of this bogie.
	 */
	public double getPrevPosX()
	{
		return prevPos.get().xCoord;
	}

	/**
	 * Return the previous tick's in-world Y-position of this bogie.
	 */
	public double getPrevPosY()
	{
		return prevPos.get().yCoord;
	}

	/**
	 * Return the previous tick's in-world Z-position of this bogie.
	 */
	public double getPrevPosZ()
	{
		return prevPos.get().zCoord;
	}

	/**
	 * Return the previous tick's in-world bogie rotation about local Z.
	 */
	public float getPrevRotationPitch()
	{
		return prevWorldRotation.get()[2];
	}

	/**
	 * Return the previous tick's in-world bogie rotation about local X.
	 */
	public float getPrevRotationRoll()
	{
		return prevWorldRotation.get()[0];
	}

	/**
	 * Return the previous tick's in-world bogie rotation about local Y.
	 */
	public float getPrevRotationYaw()
	{
		return prevWorldRotation.get()[1];
	}

	/**
	 * Return the in-world bogie rotation about local Z.
	 */
	public float getRotationPitch()
	{
		return worldRotation.get()[2];
	}

	/**
	 * Return the in-world bogie rotation about local X.
	 */
	public float getRotationRoll()
	{
		return worldRotation.get()[0];
	}

	/**
	 * Return the in-world bogie rotation about local Y.
	 */
	public float getRotationYaw()
	{
		return worldRotation.get()[1];
	}

	/**
	 * Returns whether this bogie type has a parent train which has been added to a remote (=client) world.<br>
	 * Used in conjunction with {@link #clientPos}.
	 * 
	 * @return {@code true} if the {@link #parent} part's train was spawned in a remote world.
	 */
	protected final boolean isClient()
	{
		return (getTrain() != null && getTrain().worldObj.isRemote);
	}

	/**
	 * Called by a {@link VehicleSection} upon its instantiation to notify this bogie that it was added to the given section.
	 */
	public void onAddedToSection(VehicleSection section)
	{
		/*
		 * This method is handy to determine which section(s) this bogie belongs to.
		 * One example is intercirculation rendering between two sections.
		 */
	}

	@Override
	public void onTick()
	{
		/* Backup previous tick's position and rotation. */
		prevPos.get().xCoord = getPosX();
		prevPos.get().yCoord = getPosY();
		prevPos.get().zCoord = getPosZ();
		prevWorldRotation.get()[0] = getRotationRoll() % 360.0F;
		prevWorldRotation.get()[1] = getRotationYaw() % 360.0F;
		prevWorldRotation.get()[2] = getRotationPitch() % 360.0F;

		/* Update side-specific data. */
		Train train = parent.getTrain();
		if (!train.worldObj.isRemote)
		{
			/*
			 * Arcade Braking
			 */
			{
				if (brakeArcade.get() != brakeArcadeTarget.get())
				{
					float brakeMovement = (brakeArcadeTarget.get() - brakeArcade.get()) / 10.0F;

					if (Math.abs(brakeMovement) < 0.001F)
					{
						brakeArcade.set(brakeArcadeTarget.get());
					}
					else
					{
						if (brakeMovement > 0.01F)
						{
							brakeMovement = 0.01F;
						}
						else if (brakeMovement < -0.01F)
						{
							brakeMovement = -0.01F;
						}
					}
				}
			}

			/*
			 * Parking Brake
			 */
			{
				if (breakerParkBrakeRelease.get())
				{
					if (pneumaticsParkBrakeCylinder.getPressure() < pneumaticsParkBrakeCylinder.getMaximumPressure() - 5.0F)
					{
						PneumaticsHandler.INSTANCE.equaliseOneWay(pneumaticsMainReservoirPipe, pneumaticsParkBrakeCylinder, 25.0F);
					}
				}
				else
				{
					if (!pneumaticsParkBrakeCylinder.isEmpty())
					{
						PneumaticsHandler.INSTANCE.vent(pneumaticsParkBrakeCylinder, 40.0F);
					}
				}
			}

			/*
			 * Brake Temperature(s)
			 */
			{
				/* Brake pad material heats up. */
				final float lastSpeed = (float)train.getLastSpeed();
				if (brakeArcade.get() > 0.0F)
				{
					brakeTempSurface.set(brakeTempSurface.get() + (Math.abs(lastSpeed) * brakeArcade.get()) * 0.15F);
				}
				else if (pneumaticsBrakeCylinder.getPressure() > 0.0F)
				{
					brakeTempSurface.set(brakeTempSurface.get() + (Math.abs(lastSpeed) * pneumaticsBrakeCylinder.getPressure()) * 0.015F);
				}

				/* Brake pad and brake core exchange heat. */
				float brakeCoreTemp = brakeTempCore.get();
				brakeTempCore.set(brakeCoreTemp + (brakeTempSurface.get() - brakeCoreTemp) / 700.0F);

				float brakeSurfaceTemp = brakeTempSurface.get();
				brakeTempSurface.set(brakeSurfaceTemp + (brakeTempCore.get() - brakeSurfaceTemp) / 70.0F);

				/* Brake pads cool down slightly (due to air/ wind/ etc). */
				brakeSurfaceTemp = brakeTempSurface.get();
				brakeSurfaceTemp += (20.0F - brakeSurfaceTemp) * (0.001F + Math.abs(lastSpeed * 0.02F));
				brakeTempSurface.set(brakeSurfaceTemp);
			}
		}
		else
		{
			/*
			 * Offset client-sided bogie position, if applicable.
			 */
			updateClientPos();
		}
	}

	/**
	 * <p>
	 * Overrides the client-sided position with the given values and repositions the bounding box.
	 * </p>
	 * 
	 * <p>
	 * <i>Client-side only.</i>
	 * </p>
	 */
	@SideOnly(Side.CLIENT)
	protected void setClientPos(double x, double y, double z)
	{
		/* Create vector if it doesn't exist yet. */
		if (clientPos == null)
		{
			clientPos = Vec3.createVectorHelper(0, 0, 0);
		}

		/* Override position. */
		clientPos.xCoord = x;
		clientPos.yCoord = y;
		clientPos.zCoord = z;

		/* Update bounding box. */
		getParent().getBoundingBox().setBB(getParent().initBoundingBox().offset(x, y, z));
	}

	/**
	 * Set the local direction of this bogie's movement relative to the in-world position to given X, Y and Z values, respectively.<br>
	 * It is recommended to <b>normalise</b> the vector.
	 *
	 * @see #getDirection()
	 */
	public void setDirection(double x, double y, double z)
	{
		dir.set(Vec3.createVectorHelper(x, y, z).normalize());
	}

	@Override
	public void setPosition(double x, double y, double z)
	{
		serverPos.set(Vec3.createVectorHelper(x, y, z));
	}

	/**
	 * Similar to {@code setPosition(double, double, double)}, however also applies the given coordinates to the previous position.
	 * 
	 * @see zoranodensha.api.vehicles.part.VehParBase#setPosition(double, double, double)
	 * @see #getPrevPosX()
	 */
	public void setPositionWithPrev(double x, double y, double z)
	{
		parent.setPosition(x, y, z);
		prevPos.set(serverPos.get());
	}

	/**
	 * Overrides position and rotation data with given argument data, respectively.<br>
	 * This is used to prepare vehicle spawns.
	 */
	public void setSpawnData(double x, double y, double z, float roll, float yaw, float pitch)
	{
		/* Current position and rotation. */
		setPosition(x, y, z);
		worldRotation.set(new float[] { roll, yaw, pitch });

		/* Previous position and rotation. */
		prevPos.get().xCoord = getPosX();
		prevPos.get().yCoord = getPosY();
		prevPos.get().zCoord = getPosZ();
		prevWorldRotation.get()[0] = getRotationRoll() % 360.0F;
		prevWorldRotation.get()[1] = getRotationYaw() % 360.0F;
		prevWorldRotation.get()[2] = getRotationPitch() % 360.0F;
	}

	/**
	 * Set the in-world bogie rotation.
	 *
	 * @param x - Roll rotation.
	 * @param y - Yaw rotation.
	 * @param z - Pitch rotation.
	 */
	public void setWorldRotation(float x, float y, float z)
	{
		x = VehicleHelper.normalise(x, getRotationRoll());
		y = VehicleHelper.normalise(y, getRotationYaw());
		z = VehicleHelper.normalise(z, getRotationPitch());
		worldRotation.set(new float[] { x, y, z });
	}

	/**
	 * Helper method to update this bogie's client-sided world position.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 */
	@SideOnly(Side.CLIENT)
	protected void updateClientPos()
	{
		/* Ensure this bogie is part of a train spawned on client-side. */
		if (!isClient() || clientPos == null)
		{
			return;
		}

		/* If the server position reference has changed, update tick-wise movement distance. */
		Vec3 vecServerPos = serverPos.get();
		if (prevServerPos != vecServerPos)
		{
			updateDiffPos();
		}

		/* Apply movement distance, if existent. */
		if (moveDistPerTick != null)
		{
			//@formatter:off
			setClientPos(clientPos.xCoord + moveDistPerTick.xCoord,
			             clientPos.yCoord + moveDistPerTick.yCoord,
			             clientPos.zCoord + moveDistPerTick.zCoord);
			--moveStepCounter;
			//@formatter:on
		}

		/* If the step counter has run out, clear tick-wise movement distance value. */
		if (moveStepCounter <= 0)
		{
			moveDistPerTick = null;
			moveStepCounter = 0;

			/* Also check whether server- and client positions still differ and trigger update if they do. */
			double absDiffX = Math.abs(vecServerPos.xCoord - clientPos.xCoord);
			double absDiffY = Math.abs(vecServerPos.yCoord - clientPos.yCoord);
			double absDiffZ = Math.abs(vecServerPos.zCoord - clientPos.zCoord);

			if (absDiffX > 0.001 || absDiffY > 0.001 || absDiffZ > 0.001)
			{
				updateDiffPos();
			}
		}
	}

	/**
	 * Client-sided helper method to update {@link #moveDistPerTick}.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 */
	@SideOnly(Side.CLIENT)
	protected void updateDiffPos()
	{
		Vec3 serverPosVec = serverPos.get();
		int steps = getMovementSteps();

		/*
		 * Update previous server position. Interpolate with last known "new" server position, if necessary.
		 */
		if (moveDistPerTick != null && prevServerPos != null)
		{
			/* Number of steps already taken. */
			int cnt = steps - moveStepCounter;

			/* The last "new" server position, offset by the number of steps already taken. */
			// @formatter:off
			Vec3 newPos = Vec3.createVectorHelper(prevServerPos.xCoord + moveDistPerTick.xCoord * cnt,
			                                      prevServerPos.yCoord + moveDistPerTick.yCoord * cnt,
			                                      prevServerPos.zCoord + moveDistPerTick.zCoord * cnt);

			/* Now set value to center between the actual server-sided position and the last "new" position. */
			newPos = Vec3.createVectorHelper((newPos.xCoord + serverPosVec.xCoord) / 2,
			                                 (newPos.yCoord + serverPosVec.yCoord) / 2,
			                                 (newPos.zCoord + serverPosVec.zCoord) / 2);
			// @formatter:on

			/* Assign last known server position, and apply newly calculated position as server-sided position. */
			prevServerPos = serverPosVec;
			serverPosVec = newPos;
		}
		else
		{
			prevServerPos = serverPosVec;
		}

		/*
		 * Assign tick-wise movement distance (i.e. distance from current position to server position divided by number of steps).
		 * Reset step counter.
		 */
		//@formatter:off
		moveStepCounter = steps;
		moveDistPerTick = Vec3.createVectorHelper((serverPosVec.xCoord - clientPos.xCoord) / steps,
		                                          (serverPosVec.yCoord - clientPos.yCoord) / steps,
		                                          (serverPosVec.zCoord - clientPos.zCoord) / steps);
		//@formatter:on
	}

	/**
	 * Determines the number of steps a movement update shall take, relative to
	 * the specified config setting as well as the user's current ping.
	 */
	@SideOnly(Side.CLIENT)
	@SuppressWarnings("unchecked")
	protected static int getMovementSteps()
	{
		/* Determine number of steps per movement update. */
		int steps = 6;
		{
			/* Try to retrieve the player's ping. */
			int ping = -1;
			{
				EntityClientPlayerMP thePlayer = Minecraft.getMinecraft().thePlayer;
				if (thePlayer != null && thePlayer.sendQueue != null)
				{
					if (!thePlayer.sendQueue.playerInfoList.isEmpty())
					{
						for (GuiPlayerInfo playerInfo : (List<GuiPlayerInfo>)thePlayer.sendQueue.playerInfoList)
						{
							if (playerInfo.name.equals(thePlayer.getCommandSenderName()))
							{
								ping = playerInfo.responseTime;
								break;
							}
						}
					}
				}
			}

			/* If successful, scale MOVEMENT_TIME against current player ping. */
			if (ping > 0)
			{
				/* Milliseconds per update is milliseconds per step times number of steps. */
				int millis_per_update = MOVEMENT_TIME * steps;

				/* Override number of steps depending on ping, then clamp. */
				steps = (int)(steps * ((double)ping / millis_per_update));
				if (steps < 1)
				{
					steps = 1;
				}
			}
		}
		return steps;
	}

	/**
	 * Helper method to update client-sided time for movement update application.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 * 
	 * @param time - Time (in milliseconds) during which each movement update shall be applied by default. Must not be less than {@code 10}.
	 * @throws IllegalArgumentException If {@code time} is less than {@code 10}.
	 * 
	 * @see #MOVEMENT_TIME
	 */
	@SideOnly(Side.CLIENT)
	public static final void updateMovementTime(int time) throws IllegalArgumentException
	{
		if (time < 10)
		{
			throw new IllegalArgumentException("Invalid movement time!");
		}
		MOVEMENT_TIME = time;
	}
}
