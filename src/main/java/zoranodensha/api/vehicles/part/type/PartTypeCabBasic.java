package zoranodensha.api.vehicles.part.type;

import java.util.HashMap;
import java.util.UUID;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import org.apache.logging.log4j.Level;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.IDriverKey;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.handlers.PneumaticsHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.*;
import zoranodensha.api.vehicles.part.type.APartTypeCoupling.APartTypeCouplingAuto;
import zoranodensha.api.vehicles.part.type.PartTypeDoor.EDoorState;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode;
import zoranodensha.api.vehicles.part.type.cab.PropAlerter;
import zoranodensha.api.vehicles.part.type.cab.PropButton;
import zoranodensha.api.vehicles.part.type.cab.PropButton.PropButtonLock;
import zoranodensha.api.vehicles.part.type.cab.PropDestination;
import zoranodensha.api.vehicles.part.type.cab.PropReverser;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.core.ModCenter;


/**
 * <p>
 * This part type provides a common cab implementation.
 * </o>
 *
 * <p>
 * Zora no Densha provides key detection for a specified set of keys. Any key presses of a player
 * sitting in this part type will be received and handled here. There is no need for custom key
 * handling or packet transfer.
 * </p>
 * <p>
 * This cab type provides following functionality:
 * <ul>
 * <li><b>Alerter timer with confirmation button</b></li>
 * <li><b>Destination declaration using the</b> {@code /setdest} <b>command</b></li>
 * <li><b>Reverser and dynamic throttle</b></li>
 * <li>Various buttons, such as:</li>
 * <ul>
 * <li>Automatic decoupling</li>
 * <li><b>Door state toggle</b></li>
 * <li><b>Emergency brake</b></li>
 * <li><b>Head lights</b></li>
 * <li><b>Horn</b></li>
 * <li>Interior lights (cab)</li>
 * <li>Interior lights (train)</li>
 * <li>Various multi-purpose buttons</li>
 * </ul>
 * </ul>
 *
 * <p>
 * Some functionality may not be supported by certain types of cabs.<br>
 * However, any functionality marked in bold <b>must</b> be supported by the implementing cab.
 * </p>
 *
 * @author Leshuwa Kaiheiwa
 */
public abstract class PartTypeCabBasic extends PartTypeSeat
{
	/*
	 * Custom property classes.
	 */
	/**
	 * Alerter timer of this cab.
	 */
	public final PropAlerter alerter;
	/**
	 * Destination declared by this cab.
	 */
	public final PropDestination destination;
	/**
	 * This cab's {@link zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser reverser} state.
	 */
	public final PropReverser reverser;
	/*
	 * Pneumatics
	 */
	/**
	 * The vessel which the cab controls to manipulate the brake pipe of the train during pneumatic braking.
	 */
	public final PropVessel brakeValve;
	/**
	 * A small volume of air which controls the brake pipe pressure.
	 */
	public final PropVessel equalisingReservoir;
	
	
	/*
	 * Standard property classes.
	 */
	public final PropString uuid;
	/**
	 * {@code true} if this cab is allowed to generate its own compressed air to bypass the requirement for a compressor in the train.
	 */
	private final PropBoolean arcadeCompressor;
	/**
	 * {@code true} while this cab enforces a train halt.
	 */
	private final PropBoolean forceStop;
	/**
	 * This cab's current {@link zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode light mode}.
	 */
	public final PropEnum<ELightMode> lightMode;
	
	/*
	 * Button properties.
	 */
	public final PropButton button_afb_decrease;
	public final PropButton button_afb_increase;
	public final PropButton button_alerter;
	public final PropButton button_brake_decrease;
	public final PropButton button_brake_increase;
	public final PropButton button_brakeDynamic_decrease;
	public final PropButton button_brakeDynamic_increase;
	public final PropButtonLock button_brakeMode;
	public final PropButton button_decouple;
	public final PropButtonLock button_doors_left;
	public final PropButtonLock button_doors_right;
	public final PropButtonLock button_emergency_brake;
	public final PropButton button_horn;
	public final PropButtonLock button_lights_beams;
	public final PropButtonLock button_lights_cab;
	public final PropButtonLock button_lights_train;
	public final PropButton button_motor_disable;
	public final PropButton button_motor_enable;
	public final PropButtonLock button_multi_purpose_1;
	public final PropButtonLock button_multi_purpose_2;
	public final PropButtonLock button_multi_purpose_3;
	public final PropButtonLock button_multi_purpose_4;
	public final PropButtonLock button_pantograph_back;
	public final PropButtonLock button_pantograph_front;
	public final PropButton button_parkBrake_apply;
	public final PropButton button_parkBrake_release;
	public final PropButton button_reverser_backward;
	public final PropButton button_reverser_forward;
	public final PropButton button_throttle_decrease;
	public final PropButton button_throttle_increase;
	
	/**
	 * {@link java.util.HashMap HashMap} holding all mappings between {@link ECabKey keys} and
	 * {@link zoranodensha.api.vehicles.part.type.cab.PropButton buttons}.
	 */
	protected final HashMap<ECabKey, PropButton> buttonMap;
	
	/*
	 * Miscellaneous Values
	 */
	/**
	 * A counter that makes sure the cab does not perform any intensive code too often.
	 */
	private int compressorUpdateCounter = 60;
	
	
	public PartTypeCabBasic(VehParBase parent)
	{
		this(parent, PartTypeCabBasic.class.getSimpleName());
	}
	
	public PartTypeCabBasic(VehParBase parent, String name)
	{
		super(parent, name);
		
		/* Initialise button map. */
		buttonMap = new HashMap<ECabKey, PropButton>();
		
		/* Create UUID */
		addProperty(uuid = (PropString) new PropString(parent, "cabUUID")
		{
			@Override
			protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
			{
				if(nbt.hasKey(nbtKey))
				{
					set(nbt.getString(nbtKey));
				}
				else
				{
					set(UUID.randomUUID().toString());
				}
				return true;
			}
		}.setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		
		/*
		 * Register all cab functionality properties.
		 */
		addProperty(alerter = new PropAlerter(this, "alerter"));
		addProperty(destination = new PropDestination(this, "destination"));
		addProperty(reverser = new PropReverser(this, "reverser"));
		
		addProperty(arcadeCompressor = new PropBoolean(parent, "arcadeCompressor"));
		addProperty(forceStop = (PropBoolean) new PropBoolean(parent, "doForceStop").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(lightMode = (PropEnum<ELightMode>) new PropEnum<ELightMode>(parent, ELightMode.OFF, "lightMode").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		
		/*
		 * Pneumatics
		 */
		addProperty(brakeValve = (PropVessel) new PropVessel(parent, 20.0F, 1000.0F, 0.0F, "brakeValve").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(equalisingReservoir = (PropVessel) new PropVessel(parent, 5.0F, 1000.0F, 0.0F, "equalisingReservoir").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		
		/*
		 * Initialise buttons.
		 */
		addProperty(button_alerter = new PropButton(this, ECabKey.ALERTER)
		{
			@Override
			protected void onPressed()
			{
				/* Reset alerter while respective button is pressed. */
				alerter.reset();
			}
		});
		
		/*
		 * Main Control Levers
		 */
		addProperty(button_afb_decrease = new PropButton(this, ECabKey.AFB_DECREASE));
		addProperty(button_afb_increase = new PropButton(this, ECabKey.AFB_INCREASE));
		addProperty(button_brake_decrease = new PropButton(this, ECabKey.BRAKE_DECREASE));
		addProperty(button_brakeDynamic_decrease = new PropButton(this, ECabKey.BRAKE_DYNAMIC_DECREASE));
		addProperty(button_brakeDynamic_increase = new PropButton(this, ECabKey.BRAKE_DYNAMIC_INCREASE));
		addProperty(button_brake_increase = new PropButton(this, ECabKey.BRAKE_INCREASE));
		addProperty(button_throttle_decrease = new PropButton(this, ECabKey.THROTTLE_DECREASE));
		addProperty(button_throttle_increase = new PropButton(this, ECabKey.THROTTLE_INCREASE));
		
		addProperty(button_brakeMode = new PropButtonLock(this, ECabKey.BRAKE_MODE));
		
		addProperty(button_decouple = new PropButton(this, ECabKey.DECOUPLE)
		{
			@Override
			protected void onPressed()
			{
				VehicleSection section = getSection();
				if (section != null && getTrain().getLastSpeed() == 0.0)
				{
					/* Decouple the closest coupling upon button press. */
					OrderedTypesList<APartTypeCouplingAuto> couplingsAuto = section.parts.filter(OrderedTypesList.ASELECTOR_COUPLING_AUTO);
					if (!couplingsAuto.isEmpty())
					{
						couplingsAuto.getClosestPartType(getLocalX()).onDecouple();
					}
				}
			}
		});
		
		addProperty(button_doors_left = new PropButtonLock(this, ECabKey.DOORS_LEFT));
		addProperty(button_doors_right = new PropButtonLock(this, ECabKey.DOORS_RIGHT));
		addProperty(button_emergency_brake = new PropButtonLock(this, ECabKey.EMERGENCY_BRAKE)
		{
			@Override
			protected void onPressed()
			{
				/* Enable emergency state on server side when pressed. */
				setEmergency();
			}
		});
		
		addProperty(button_horn = new PropButton(this, ECabKey.HORN)
		{
			@Override
			protected void onPressed()
			{
				VehicleSection section = getSection();
				if (section != null)
				{
					/* Select closest horn part - if existent - and play horn. */
					OrderedTypesList<PartTypeHorn> horns = section.parts.filter(OrderedTypesList.SELECTOR_HORN);
					if (!horns.isEmpty())
					{
						horns.getClosestPartType(getLocalX()).soundHorn();
					}
				}
			}
		});
		
		addProperty(button_lights_beams = new PropButtonLock(this, ECabKey.LIGHTS_BEAMS)
		{
			@Override
			protected void onPressed()
			{
				/* Update all lamps which point in the same direction as this cab. */
				onUpdateLamps();
			}
			
			@Override
			protected void onReleased()
			{
				/* Update all lamps which point in the same direction as this cab. */
				onUpdateLamps();
			}
		});
		
		addProperty(button_lights_cab = new PropButtonLock(this, ECabKey.LIGHTS_CAB));
		// addProperty(button_lights_headlights = new PropButton(this, ECabKey.LIGHTS_HEADLIGHTS)
		// {
		// @Override
		// public String getTooltip()
		// {
		// /* Append the light mode to the head light button label. */
		// String mode = StatCollector.translateToLocal(String.format("%s.tooltip.lightmode.%s", PartTypeCab.class.getName().toLowerCase(), lightMode.get().toString().toLowerCase()));
		// return String.format("%s (%s)", super.getTooltip(), mode);
		// }
		//
		// @Override
		// protected void onPressed()
		// {
		// /* Switch to next light mode when pressed. */
		// ELightMode[] lightModes = ELightMode.values();
		// int nextMode = (lightMode.get().ordinal() + 1) % lightModes.length;
		// lightMode.set(lightModes[nextMode]);
		//
		// /* Update all lamps which point in the same direction as this cab. */
		// onUpdateLamps();
		// }
		// });
		
		/*
		 * Motors
		 */
		addProperty(button_motor_disable = new PropButton(this, ECabKey.MOTOR_DISABLE)
		{
			@Override
			protected void onPressed()
			{
				if (getParent().getTrain() != null)
				{
					/*
					 * Get all the diesel motors in the train.
					 */
					ConcatenatedTypesList<PartTypeEngineDiesel> dieselEngines = getParent().getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_ENGINE_DIESEL);
					
					/*
					 * Go through all the motors and trip their ignition breaker switches.
					 */
					for (PartTypeEngineDiesel engine : dieselEngines)
					{
						engine.ignition.trip();
					}
				}
			}
		});
		addProperty(button_motor_enable = new PropButton(this, ECabKey.MOTOR_ENABLE)
		{
			@Override
			protected void onPressed()
			{
				if (getParent().getTrain() != null)
				{
					/*
					 * Get all the diesel motors in the train.
					 */
					ConcatenatedTypesList<PartTypeEngineDiesel> dieselEngines = getParent().getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_ENGINE_DIESEL);
					
					/*
					 * Go through all the motors and set their ignition breaker switches.
					 */
					for (PartTypeEngineDiesel engine : dieselEngines)
					{
						engine.ignition.set();
					}
				}
			}
		});
		
		addProperty(button_lights_train = new PropButtonLock(this, ECabKey.LIGHTS_TRAIN));
		addProperty(button_multi_purpose_1 = new PropButtonLock(this, ECabKey.MULTI_PURPOSE_1));
		addProperty(button_multi_purpose_2 = new PropButtonLock(this, ECabKey.MULTI_PURPOSE_2));
		addProperty(button_multi_purpose_3 = new PropButtonLock(this, ECabKey.MULTI_PURPOSE_3));
		addProperty(button_multi_purpose_4 = new PropButtonLock(this, ECabKey.MULTI_PURPOSE_4));
		addProperty(button_pantograph_back = new PropButtonLock(this, ECabKey.PANTOGRAPH_BACK));
		addProperty(button_pantograph_front = new PropButtonLock(this, ECabKey.PANTOGRAPH_FRONT));
		addProperty(button_reverser_backward = new PropButton(this, ECabKey.REVERSER_BACKWARD)
		{
			@Override
			protected void onPressed()
			{
				reverser.set(reverser.get().prev());
			}
		});
		addProperty(button_parkBrake_apply = new PropButton(this, ECabKey.PARK_BRAKE_APPLY)
		{
			@Override
			protected void onPressed()
			{
				if (getParent().getTrain() != null)
				{
					/*
					 * Get all the bogies in the train.
					 */
					ConcatenatedTypesList<PartTypeBogie> allBogies = getParent().getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
					
					/*
					 * Trip the park brake release breaker on each bogie.
					 */
					for (PartTypeBogie bogie : allBogies)
					{
						bogie.breakerParkBrakeRelease.trip();
					}
				}
			}
		});
		addProperty(button_parkBrake_release = new PropButton(this, ECabKey.PARK_BRAKE_RELEASE)
		{
			@Override
			protected void onPressed()
			{
				if (getParent().getTrain() != null)
				{
					/*
					 * Get all the bogies in the train.
					 */
					ConcatenatedTypesList<PartTypeBogie> allBogies = getParent().getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
					
					/*
					 * Set the park brake release breaker on each bogie.
					 */
					for (PartTypeBogie bogie : allBogies)
					{
						bogie.breakerParkBrakeRelease.set();
					}
				}
			}
		});
		
		addProperty(button_reverser_forward = new PropButton(this, ECabKey.REVERSER_FORWARD)
		{
			@Override
			protected void onPressed()
			{
				reverser.set(reverser.get().next());
			}
		});
	}
	
	
	@Override
	protected void addProperty(IPartProperty<?> property)
	{
		super.addProperty(property);
		
		/* In case a button was added, re-register all buttons into button map. */
		if (property instanceof PropButton)
		{
			buttonMap.clear();
			
			for (IPartProperty<?> prop : getProperties())
			{
				if (prop instanceof PropButton)
				{
					PropButton button = (PropButton) prop;
					if (button.cabKey != null)
					{
						buttonMap.put(button.cabKey, button);
					}
				}
			}
		}
	}
	
	
	/**
	 * <p>
	 * This method should return the current position of the main brake lever.
	 * </p>
	 * <p>
	 * <b>The value returned by this method may not always represent the physical position of the brake lever. The value may be modified by the cab's safety systems to automatically apply the
	 * brakes.</b> If you need the accurate physical position of the brake lever, without adjustment from the cab safety systems, use {@link PartTypeCabBasic#getBrakeLevelPhysical()}.
	 * </p>
	 */
	public abstract float getBrakeLevelLogical();
	
	
	/**
	 * <p>
	 * This method should return the physical position of the main brake lever.
	 * </p>
	 *
	 * @return - A {@code float} with a range from {@code 0.0F} (0%, brakes most released) to {@code 1.0F} (100%, brakes most applied).
	 */
	public abstract float getBrakeLevelPhysical();
	
	
	/**
	 * Return a new String array holding the destination data.
	 *
	 * @return Destination data of this cab in a new array. May be {@code null}.
	 */
	public String[] getDestination()
	{
		return destination.getDestination();
	}
	
	
	/**
	 * Getter to determine whether this cab is enforcing a train halt.
	 */
	public boolean getDoForceStop()
	{
		return forceStop.get();
	}
	
	
	/**
	 * <p>
	 * Gets the current dynamic brake level requested by this cab. The value returned is the physical position of the dynamic brake lever, in some cases modified by the cab safety systems to initiate
	 * dynamic braking.
	 * </p>
	 *
	 * @return - The current dynamic brake level requested by this cab, ranging from {@code 0.0F} (0%, OFF) to {@code 1.0F} (100%, MAXIMUM DYNAMIC BRAKING).
	 */
	public abstract float getDynamicBrakeLevelLogical();
	
	
	/**
	 * <p>
	 * Gets the current physical position of the dynamic brake lever.
	 * </p>
	 * <p>
	 * If this cab does not contain a dynamic brake lever, this method will return {@code 0.0F}.
	 * </p>
	 *
	 * @return - The current physical position of the dynamic brake lever, ranging from {@code 0.0F} (0%, OFF) to {@code 1.0F} (100%, MAXIMUM DYNAMIC BRAKING).
	 */
	public abstract float getDynamicBrakeLevelPhysical();
	
	
	/**
	 * Returns whether this is the parent train's {@link zoranodensha.api.vehicles.Train#getActiveCab() active cab}.
	 *
	 * @return {@code true} if this is an active cab.
	 */
	public boolean getIsActive()
	{
		return (getTrain() != null) && (getTrain().getActiveCab() == this);
	}
	
	
	/**
	 * Returns whether this cab allows another cab to become the new active cab in a train as defined in {@link zoranodensha.api.vehicles.Train#updateActiveCab() updateActiveCab()}.<br>
	 * <br>
	 * Use this method to check for preconditions before switching active cabs, e.g. to check if a player has turned off the engines or if the driver's key was pulled.
	 *
	 * @return {@code true} if another cab may override this cab as active cab.
	 */
	public boolean getMayOverrideActive()
	{
		return (getPassenger() == null);
	}
	
	
	/**
	 * Determines whether the {@link #forceStop} flag may be reset and thus whether an applied emergency brake may be released.
	 *
	 * @return {@code true} if the emergency brake may be released.
	 */
	public boolean getMayReleaseForceStop()
	{
		return (getTrain().getLastSpeed() == 0.0) && (getThrottlePhysical() == 0.0F);
	}
	
	/**
	 * Return the current reverser state.
	 *
	 * @return The current {@link #reverser} state
	 */
	public EReverser getReverserState()
	{
		return reverser.get();
	}
	
	
	/**
	 * <p>
	 * Gets the current throttle level as is represented physically by the lever, unless a different value is required by the cab safety systems, in which case that value is returned instead.
	 * </p>
	 * <p>
	 * If the physical position of the lever is required without any safety system interference, use {@link PartTypeCabBasic#getThrottlePhysical()} instead.
	 * </p>
	 *
	 * @return - Current physical throttle lever position ranging from {@code 0.0F} (0%) to {@code 1.0F} (100%).
	 */
	public float getThrottleLogical()
	{
		float throttle = getThrottlePhysical();
		
		/*
		 * This will make sure that the train doesn't keep accelerating if the emergency brakes are applied.
		 */
		if (getDoForceStop() && throttle > 0.0F)
		{
			throttle = 0.0F;
		}
		
		return throttle;
	}
	
	
	/**
	 * <p>
	 * Gets the current throttle level as is represented physically by the lever.
	 * </p>
	 *
	 * @return - Current physical throttle lever position ranging from {@code 0.0F} (0%) to {@code 1.0F} (100%).
	 */
	public abstract float getThrottlePhysical();
	
	
	/**
	 * Called whenever the given key's state changes.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 *
	 * @param cabKey    - {@link ECabKey Key} whose state changed.
	 * @param isKeyDown - {@code true} if the key's respective key binding is pressed.
	 */
	@SideOnly(Side.CLIENT)
	public void onClientKeyChange(ECabKey cabKey, boolean isKeyDown)
	{
		buttonMap.get(cabKey).onClientKeyChange(isKeyDown);
	}
	
	@Override
	public void onTick()
	{
		super.onTick();
		
		/* Abort if the train isn't added to its chunk yet. */
		Train train = getTrain();
		if (!train.addedToChunk)
		{
			return;
		}
		
		
		if (!train.worldObj.isRemote)
		{
			/*
			 * Run alerter and force-stop updates.
			 */
			
			/* If the train is moving and either this is the train's active cab or the alerter has been running, update alerter. */
			if ((getIsActive() || alerter.getIsRunning()) && train.getLastSpeed() != 0.0)
			{
				alerter.update();
			}
			else
			{
				/* Otherwise, reset alerter timer. */
				alerter.reset();
				
				/* Additionally, if the train stands still and throttle and reverser are in neutral, disable force stop. */
				if (getMayReleaseForceStop())
				{
					forceStop.set(false);
				}
			}
			
			
			/*
			 * Run Arcade Compressor Check
			 */
			if (compressorUpdateCounter <= 0)
			{
				ConcatenatedTypesList<PartTypeCompressor> trainCompressors = parent.getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_COMPRESSOR);
				
				arcadeCompressor.set(trainCompressors.size() <= 0);
				compressorUpdateCounter = 60;
			}
			else
			{
				compressorUpdateCounter--;
			}
		}
	}
	
	/**
	 * Separate method to update the train's brakes.<br>
	 * <br>
	 * Usually, this gets called during train updates if this cab is the parent train's active cab.
	 * However, if the alerter has run out this method may be called even when this cab isn't the currently active cab.
	 */
	public void onUpdateBrakes()
	{
		/* Safety check to ensure the parent train exists. */
		Train train = getTrain();
		if (train == null)
		{
			return;
		}
		
		
		/*
		 * Pneumatics
		 */
		if (!train.worldObj.isRemote)
		{
			ConcatenatedTypesList<PartTypeBogie> trainBogies = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
			{
				PartTypeBogie thisBogie = trainBogies.getClosestPartType(this.getLocalX());
				PneumaticsHandler pneumaticsHandler = PneumaticsHandler.INSTANCE;
				
				
				/*
				 * Sound Effect Values
				 */
				{
					brakeValve.valueModification = 0.0F;
					brakeValve.valueBefore = brakeValve.getPressure();
					equalisingReservoir.valueModification = 0.0F;
					
					brakeValve.setHasChanged(true);
					equalisingReservoir.setHasChanged(true);
				}
				
				/*
				 * Arcade Compressor
				 */
				if (arcadeCompressor.get())
				{
					if (thisBogie.pneumaticsMainReservoirPipe.getPressure() < 850.0F)
					{
						thisBogie.pneumaticsMainReservoirPipe.setPressure(850.0F);
					}
				}
				
				
				/*
				 * Brake Stand Logic
				 */
				int step = Math.round(getBrakeLevelPhysical() * 8.0F); // FIXME @Jaffa - This is hardcoded
				float pilotValveTargetPressure = 500.0F - (step * 21.0F) - (step > 0 ? 10.0F : 0.0F);
				float pilotValveOffset = equalisingReservoir.getPressure() - pilotValveTargetPressure;
				
				/*
				 * Pilot Valve Charging
				 */
				if (pilotValveOffset < -2.0F)
				{
					pneumaticsHandler.equalise(thisBogie.pneumaticsMainReservoirPipe, equalisingReservoir, 30.0F);
				}
				
				/*
				 * Pilot Valve Venting
				 */
				if (button_brakeMode.get() || getReverserState() == EReverser.LOCKED)
				{
					if (pilotValveOffset > 2.0F)
					{
						pneumaticsHandler.vent(equalisingReservoir, 40.0F);
					}
				}
				
				/*
				 * Emergency Valve
				 */
				if (step >= 8 || forceStop.get() || getReverserState().equals(EReverser.LOCKED))
				{
					pneumaticsHandler.vent(brakeValve, 2000.0F);
				}
				/*
				 * Relay Valve
				 */
				else
				{
					float relayValveOffset = equalisingReservoir.getPressure() - brakeValve.getPressure();
					
					if (relayValveOffset > 2.0F)
					{
						pneumaticsHandler.equalise(thisBogie.pneumaticsMainReservoirPipe, brakeValve, Math.min(Math.abs(relayValveOffset - 2.0F) * 2.0F, 175.0F));
					}
					else if (relayValveOffset < -2.0F)
					{
						pneumaticsHandler.vent(brakeValve, Math.abs(relayValveOffset + 2.0F) * 2.0F);
					}
				}
				
				/*
				 * The final step is to connect the cab's brake valve instance to the train's brake pipe.
				 */
				{
					for (int iteration = 0; iteration < 15; iteration++)
					{
						pneumaticsHandler.equalise(brakeValve, thisBogie.pneumaticsBrakePipe, 300.0F);
					}
				}
				
				/*
				 * Sound effect values
				 */
				{
					brakeValve.valueAfter = brakeValve.getPressure();
				}
			}
		}
	}
	
	/**
	 * Helper method to update all {@link zoranodensha.api.vehicles.part.type.PartTypeLamp lamps} in this {{@link #getSection() section}, if applicable.
	 */
	protected void onUpdateLamps()
	{
		/* Ensure the section exists. */
		VehicleSection section = getSection();
		if (section == null)
		{
			return;
		}
		
		/* Retrieve all lamps and check whether there exists any. */
		OrderedTypesList<PartTypeLamp> lamps = section.parts.filter(OrderedTypesList.SELECTOR_LAMP);
		if (lamps.isEmpty())
		{
			return;
		}
		
		/* Check whether their rotation matches ours and, if so, apply new light data. */
		final float thisLampYaw = getParent().getRotation()[1];
		for (PartTypeLamp lamp : lamps)
		{
			if (Math.abs(lamp.getParent().getRotation()[1] - thisLampYaw) <= 90.0F)
			{
				lamp.setFromLightMode(lightMode.get(), button_lights_beams.isActive());
			}
		}
	}
	
	/**
	 * Called during update routines by the parent {@link zoranodensha.api.vehicles.Train train} to load this cab's data into the train.<br>
	 * <br>
	 * Override state data of part types in the entire train here.
	 */
	public void onUpdateTrain()
	{
		/*
		 * Update brake data.
		 */
		onUpdateBrakes();
		
		/*
		 * Update any other data.
		 */
		
		/* Prepare some data beforehand. */
		Train train = getTrain();
		final EDoorState doorState = EDoorState.fromFlags(button_doors_left.get(), button_doors_right.get());
		
		/* Doors */
		ConcatenatedTypesList<PartTypeDoor> doors = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_DOOR);
		if (!doors.isEmpty())
		{
			for (PartTypeDoor door : doors)
			{
				door.setDoorState(doorState);
			}
		}
		
		/* Pantographs */
		for (VehicleData vehicle : train)
		{
			for (VehicleSection section : vehicle)
			{
				/* For each vehicle section containing pantographs.. */
				OrderedTypesList<PartTypePantograph> pantographs = section.parts.filter(OrderedTypesList.SELECTOR_PANTOGRAPH);
				if (!pantographs.isEmpty())
				{
					/* ..calculate the center of the section.. */
					AxisAlignedBB minBB = section.parts.get(0).getLocalBoundingBox();
					AxisAlignedBB maxBB = section.parts.get(section.parts.size() - 1).getLocalBoundingBox();
					float localCenterX = (float) ((minBB.minX + maxBB.maxX) / 2);
					
					/* ..determine whether to inverse the direction, depending on where the cab is looking at.. */
					boolean isInverse = VehicleHelper.getAngle(null, PartHelper.getViewVec(getParent(), true)) > 90.0;
					
					/* ..and then toggle pantographs on either side. */
					final boolean backUp = button_pantograph_back.get();
					final boolean frontUp = button_pantograph_front.get();
					
					for (PartTypePantograph pantograph : pantographs)
					{
						if (pantograph.getParent().getOffset()[0] < localCenterX)
						{
							pantograph.setDoExtend(isInverse ? frontUp : backUp);
						}
						else
						{
							pantograph.setDoExtend(isInverse ? backUp : frontUp);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Assign an array holding destination data to this cab.<br>
	 * <br>
	 * The destination info may be displayed by applicable parts in the train.
	 *
	 * @param dest - An array of destinations, or {@code null} if there is none set.
	 */
	public void setDestination(@Nullable String... dest)
	{
		destination.setDestination(dest);
	}
	
	/**
	 * Called to set the cab's controls into an emergency state.
	 */
	public void setEmergency()
	{
		/* Enforce a stop only if we aren't already standing still. */
		if (!getMayReleaseForceStop())
		{
			forceStop.set(true);
			onUpdateBrakes();
		}
	}
	
	@Override
	public boolean setPassenger(Entity entity)
	{
		/* Run permission checks before calling super class method. */
		if (getParent().getIsFinished() && entity instanceof EntityLivingBase)
		{
			EntityLivingBase entityLiving = (EntityLivingBase) entity;
			ItemStack heldItem = entityLiving.getHeldItem();
			
			if (heldItem != null && heldItem.getItem() instanceof IDriverKey)
			{
				if (((IDriverKey) heldItem.getItem()).getCanAccess(this, entityLiving))
				{
					return super.setPassenger(entity);
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Called to check for driver alertness.<br>
	 * <br>
	 * This may be called especially by signals or magnets at dangerous places.
	 */
	public void setRequestAlertness()
	{
		/* Abort if we are on a remote world, the train isn't moving, or if this isn't the train's active cab. */
		Train train = getTrain();
		if (train != null && !train.worldObj.isRemote && train.getLastSpeed() != 0.0 && getIsActive())
		{
			alerter.requestAlertness();
		}
	}
	
	/**
	 * Called whenever the parent train tries to set this cab as active cab.<br>
	 * Check for cab-dependent preconditions here (e.g. presence of a driver or a key).
	 *
	 * @return {@code true} if this cab may become the new active cab.
	 * @see zoranodensha.api.vehicles.Train#getActiveCab() getActiveCab()
	 */
	public boolean trySetActive()
	{
		return (getPassenger() instanceof EntityPlayer);
	}
	
	
	/**
	 * Cab keys handled by the default cab part type.<br>
	 * <br>
	 * Cabs will be notified upon state changes of these keys.<br>
	 * Zora no Densha handles key bindings and presses internally in an attempt to maintain a global
	 * keyboard layout across all implemented cabs.
	 */
	public enum ECabKey
	{
		/**
		 * Decrease the AFB speed limit.
		 */
		AFB_DECREASE,
		/**
		 * Increase the AFB speed limit.
		 */
		AFB_INCREASE,
		/**
		 * Confirm driver alertness.
		 */
		ALERTER,
		/**
		 * Decrease brake controller.
		 */
		BRAKE_DECREASE,
		/**
		 * Decrease dynamic brake controller.
		 */
		BRAKE_DYNAMIC_DECREASE,
		/**
		 * Increase dynamic brake controller.
		 */
		BRAKE_DYNAMIC_INCREASE,
		/**
		 * Increase brake controller.
		 */
		BRAKE_INCREASE,
		/**
		 * Switches between different braking modes, usually between Electro-Pneumatic and Pneumatic modes of operation.
		 */
		BRAKE_MODE,
		/**
		 * Deactivate automatic couplings. Mostly common in cabs of trains with auto-couplers.
		 */
		DECOUPLE,
		/**
		 * (Un-)lock doors on the left.
		 */
		DOORS_LEFT,
		/**
		 * (Un-)lock doors on the right.
		 */
		DOORS_RIGHT,
		/**
		 * Emergency brake.
		 */
		EMERGENCY_BRAKE,
		/**
		 * Sound the cab's horn.
		 */
		HORN,
		/**
		 * Change whether head lights shall, if active, render beams.
		 */
		LIGHTS_BEAMS,
		/**
		 * Toggle cab lighting.
		 */
		LIGHTS_CAB,
		/**
		 * Toggle train lighting.
		 */
		LIGHTS_TRAIN,
		/**
		 * Generic button to disable/shut down any motors on the train.
		 */
		MOTOR_DISABLE,
		/**
		 * Generic button to enable/start up any motors on the train.
		 */
		MOTOR_ENABLE,
		/**
		 * Multi-purpose key 1.
		 */
		MULTI_PURPOSE_1,
		/**
		 * Multi-purpose key 2.
		 */
		MULTI_PURPOSE_2,
		/**
		 * Multi-purpose key 3.
		 */
		MULTI_PURPOSE_3,
		/**
		 * Multi-purpose key 4.
		 */
		MULTI_PURPOSE_4,
		/**
		 * Raises back pantographs.
		 */
		PANTOGRAPH_BACK,
		/**
		 * Raises front pantographs.
		 */
		PANTOGRAPH_FRONT,
		/**
		 * Applies the parking brakes along the entire train.
		 */
		PARK_BRAKE_APPLY,
		/**
		 * Releases the parking brakes along the entire train.
		 */
		PARK_BRAKE_RELEASE,
		/**
		 * Move the reverser one level backwards.
		 */
		REVERSER_BACKWARD,
		/**
		 * Move the reverser one level forward.
		 */
		REVERSER_FORWARD,
		/**
		 * Decrease the throttle.
		 */
		THROTTLE_DECREASE,
		/**
		 * Increase the throttle.
		 */
		THROTTLE_INCREASE,
	}
}