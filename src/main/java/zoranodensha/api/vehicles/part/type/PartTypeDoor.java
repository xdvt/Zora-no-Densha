package zoranodensha.api.vehicles.part.type;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Vec3;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.IDriverKey;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Activatable;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * Doors of any kind.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PartTypeDoor extends APartType implements IPartType_Activatable, IPartType_Tickable
{
	/** The point at which the doors are shut at, between {@code 0.0F} and {@code 1.0F}. */
	public final PropFloatBounded doorShutAt;
	/** The door shutting speed, in meters per tick. */
	public final PropFloatBounded doorSpeed;

	/** Left door's state. {@code 0.0F} when fully closed. Door moves between {@code 0.0F} and {@code 1.0F}, post/pre move phase until {@code 2.0F}. */
	public final PropFloatBounded doorStateL;
	/** Right door's state. {@code 0.0F} when fully closed. Door moves between {@code 0.0F} and {@code 1.0F}, post/pre move phase until {@code 2.0F}. */
	public final PropFloatBounded doorStateR;
	/** {@code true} if the left door set is unlocked. */
	public final PropBoolean isOpenLeft;
	/** {@code true} if the right door set is unlocked. */
	public final PropBoolean isOpenRight;
	/** This door's {@link EDoorState state}, indicating which door set(s) are (un-)locked. */
	public final PropEnum<EDoorState> doorState;



	public PartTypeDoor(VehParBase parent)
	{
		super(parent, PartTypeDoor.class.getSimpleName());
		addProperty(doorShutAt = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 0.0F, 1.0F, "doorShutAt").setConfigurable());
		addProperty(doorSpeed = (PropFloatBounded)new PropFloatBounded(parent, 0.01667F, 0.0F, 1.0F, "doorSpeed").setConfigurable());
		addProperty(doorStateL = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 2.0F, "doorStateL").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(doorStateR = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 2.0F, "doorStateR").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(isOpenLeft = (PropBoolean)new PropBoolean(parent, "isOpenLeft").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(isOpenRight = (PropBoolean)new PropBoolean(parent, "isOpenRight").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(doorState = (PropEnum<EDoorState>)new PropEnum<EDoorState>(parent, EDoorState.LOCK_ALL, "doorState").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
	}


	/**
	 * Door state value at which the doors are shut.
	 */
	public float doorShutAt()
	{
		return doorShutAt.get();
	}

	/**
	 * Getter for value of {@link #doorStateL}.
	 */
	public float doorStateL()
	{
		return doorStateL.get();
	}

	/**
	 * Getter for value of {@link #doorStateL}.
	 */
	public float doorStateR()
	{
		return doorStateR.get();
	}

	/**
	 * Determines whether door states should be inverted, depending on current direction vector orientation.
	 * 
	 * @return {@code true} to invert door states, {@code false} if not.
	 */
	public boolean getDoInvertDoorStates()
	{
		Train train = getParent().getTrain();
		if (train != null && getSection() != null)
		{
			/* Retrieve direction vector of the closest bogie. */
			PartTypeBogie bogie = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY).getClosestPartType(getParent().getOffset()[0]);
			if (bogie != null)
			{
				/* Get the direction vector's yaw rotation relative to base vector (1,0,0). */
				Vec3 dir = bogie.getDirection();
				if (dir != null)
				{
					/* Create local vectors pointing to either side. */
					Vec3 vec = Vec3.createVectorHelper(0, 1, 0);
					Vec3 vecL = dir.crossProduct(vec);
					Vec3 vecR = dir.crossProduct(vec);
					vecL.xCoord = -vecL.xCoord;
					vecL.yCoord = -vecL.yCoord;
					vecL.zCoord = -vecL.zCoord;

					/* Create vector pointing to local left, rotated by section and part rotation. */
					vec = Vec3.createVectorHelper(0, 0, -1);
					vec.rotateAroundY(-getSection().getRotationYaw() * VehicleHelper.RAD_FACTOR);
					vec.rotateAroundY(getParent().getRotation()[1] * VehicleHelper.RAD_FACTOR);

					/* If the distance is shorter to the right-hand vector, invert door states. */
					return (vec.squareDistanceTo(vecR) < vec.squareDistanceTo(vecL));
				}
			}
		}
		return false;
	}

	/**
	 * Calculates the global look vector of the specified door side, originating from the parent vehicle part's position in the world.<br>
	 * Uses the closest bogie's {@link zoranodensha.api.vehicles.part.type.PartTypeBogie#getDirection() direction} vector to determine the front.
	 * 
	 * @param doorSide - Global {@link EDoorSide side} of the door whose look vector is requested, relative to the train's front view.
	 * @return The door's look vector, or {@code null} if it couldn't be calculated.
	 */
	public Vec3 getDoorLookVec(EDoorSide doorSide)
	{
		/* Retrieve direction vector of the closest bogie. */
		Train train = getTrain();
		if (train != null)
		{
			PartTypeBogie bogie = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY).getClosestPartType(getParent().getOffset()[0]);
			if (bogie != null)
			{
				Vec3 dir = bogie.getDirection();
				if (dir != null)
				{
					/* Create local vector pointing to the side. */
					Vec3 vec = dir.crossProduct(Vec3.createVectorHelper(0, 1, 0));

					/* Invert left side. */
					if (doorSide == EDoorSide.LEFT)
					{
						vec.xCoord = -vec.xCoord;
						vec.yCoord = -vec.yCoord;
						vec.zCoord = -vec.zCoord;
					}

					return vec;
				}
			}
		}

		return null;
	}

	/**
	 * Return this door's door state.
	 * 
	 * @return The current {@link EDoorState door state}.
	 */
	public EDoorState getDoorState()
	{
		return doorState.get();
	}

	/**
	 * Returns the door state depending on inversion flags.
	 */
	public EDoorState getDoorStateGlobal()
	{
		switch (getDoorState())
		{
			case UNLOCK_LEFT:
				return getDoInvertDoorStates() ? EDoorState.UNLOCK_RIGHT : EDoorState.UNLOCK_LEFT;

			case UNLOCK_RIGHT:
				return getDoInvertDoorStates() ? EDoorState.UNLOCK_LEFT : EDoorState.UNLOCK_RIGHT;

			default:
				return getDoorState();
		}
	}

	/**
	 * Helper method to determine whether there are any physically opened doors in the given train.
	 * 
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} to check inside.
	 * @return {@code true} if there are any doors that are physically open.
	 */
	public static boolean getDoorsOpen(Train train)
	{
		for (PartTypeDoor door : train.getVehiclePartTypes(OrderedTypesList.SELECTOR_DOOR))
		{
			if (!door.getIsDoorClosed())
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Helper method to determine whether there are any unlocked doors in the given train.
	 * 
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} to check inside.
	 * @return {@code true} if there are any unlocked doors in the train.
	 */
	public static boolean getDoorsUnlocked(Train train)
	{
		for (PartTypeDoor door : train.getVehiclePartTypes(OrderedTypesList.SELECTOR_DOOR))
		{
			if (door.getDoorState() != EDoorState.LOCK_ALL)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets whether the doors of this type are physically closed.
	 * 
	 * @return - Whether the doors are physically closed.
	 */
	public boolean getIsDoorClosed()
	{
		return doorStateL() <= doorShutAt() && doorStateR() <= doorShutAt();
	}
	/**
	 * Called on <i>server</i> worlds to play a door opening chime sound.
	 */
	protected void playOpeningChimeSound()
	{
		;
	}

	/**
	 * Called on <i>server</i> worlds to play a shutting sound.
	 */
	protected void playDoorShutSound()
	{
		;
	}

	/**
	 * Called on <i>server</i> worlds to play a warning sound.
	 */
	protected void playWarningChimeSound()
	{
		;
	}

	@Override
	public boolean onActivated(EntityPlayer player)
	{
		/* Ensure the player exists and isn't sneaking. */
		if (player == null || player.isSneaking())
		{
			return false;
		}

		/* Also make sure our parent part is finished and we are on a non-remote world in a spawned train. */
		Train train = getTrain();
		if (!getParent().getIsFinished() || train == null || !train.addedToChunk || train.worldObj.isRemote)
		{
			return false;
		}

		/*
		 * Check if player is close enough to this door and ensure that there is a section associated with the parent part.
		 */
		VehicleSection thisSection = getSection();
		Vec3 pos = PartHelper.getPosition(getParent());

		if (thisSection == null || player.getDistanceSq(pos.xCoord, pos.yCoord, pos.zCoord) > 9.0)
		{
			return false;
		}

		/*
		 * Determine which door the player stands in front of.
		 */
		boolean isLeft;
		{
			/* Retrieve left and right doors' look vectors and ensure neither is null. */
			Vec3 vecL = getDoorLookVec(EDoorSide.LEFT);
			Vec3 vecR = getDoorLookVec(EDoorSide.RIGHT);

			if (vecL == null || vecR == null)
			{
				return false;
			}

			/* Offset both vectors and determine which position the player is closer to. */
			Vec3 playerPos = Vec3.createVectorHelper(player.posX - pos.xCoord, player.posY - pos.yCoord, player.posZ - pos.zCoord);
			isLeft = playerPos.squareDistanceTo(vecL) < playerPos.squareDistanceTo(vecR);

			/* Invert result if required to accommodate to local rotation. */
			if (getDoInvertDoorStates())
			{
				isLeft = !isLeft;
			}
		}

		/* Determine whether the player holds a valid driver key. */
		IDriverKey iDriverKey = null;
		{
			ItemStack heldItem = player.getHeldItem();
			if (heldItem != null && heldItem.getItem() instanceof IDriverKey)
			{
				iDriverKey = (IDriverKey)heldItem.getItem();
			}
		}

		/*
		 * If the door is closed, open it if unlocked or force-open it if the player holds a driver's key.
		 */
		if (isLeft)
		{
			if (getDoorStateGlobal().isUnlockedLeft())
			{
				if (!isOpenLeft.get())
				{
					setOpen(EDoorSide.LEFT, true);
					return true;
				}
				else if (doorStateL() < 0.75F)
				{
					return false;
				}
			}
			else if (iDriverKey == null)
			{
				return false;
			}
		}
		else
		{
			if (getDoorStateGlobal().isUnlockedRight())
			{
				if (!isOpenRight.get())
				{
					setOpen(EDoorSide.RIGHT, true);
					return true;
				}
				else if (doorStateR() < 0.75F)
				{
					return false;
				}
			}
			else if (iDriverKey == null)
			{
				return false;
			}
		}

		/*
		 * If the door is open seat the player.
		 */
		if (iDriverKey != null)
		{
			OrderedTypesList<PartTypeCabBasic> cabs = thisSection.parts.filter(OrderedTypesList.SELECTOR_CAB);
			if (!cabs.isEmpty())
			{
				for (PartTypeCabBasic cab : cabs)
				{
					if (iDriverKey.getCanAccess(cab, player) && cab.setPassenger(player))
					{
						return true;
					}
				}
			}
		}
		else
		{
			OrderedTypesList<PartTypeSeat> seats = thisSection.parts.filter(OrderedTypesList.SELECTOR_SEAT_NO_CAB);
			if (!seats.isEmpty())
			{
				PartTypeSeat seat;
				while (!seats.isEmpty())
				{
					seat = seats.remove(VehicleHelper.ran.nextInt(seats.size()));
					if (seat.setPassenger(player))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void onTick()
	{
		Train train = getTrain();
		boolean isRemote = train.worldObj.isRemote;

		if (!isRemote)
		{
			EDoorState doorState = getDoorStateGlobal();
			boolean isChimePlaying = true;
			
			if (doorState.isUnlockedLeft()||doorState.isUnlockedRight())
			{
				playOpeningChimeSound();
			}
			if (isOpenLeft.get() && !doorState.isUnlockedLeft())
			{
				setOpen(EDoorSide.LEFT, false);
			}

			if (isOpenRight.get() && !doorState.isUnlockedRight())
			{
				setOpen(EDoorSide.RIGHT, false);
			}
		}

		float prev = doorStateL();
		if (isOpenLeft.get())
		{
			if (prev < 2.0F)
			{
				doorStateL.set(prev + doorSpeed.get());
			}
		}
		else
		{
			if (prev > 0.0F)
			{
				doorStateL.set(prev - doorSpeed.get());

				/* Play warn sound and, if applicable, shut sound. */
				playWarningChimeSound();

				if (prev > doorShutAt() && doorStateL() <= doorShutAt())
				{
					playDoorShutSound();
				}
			}
		}

		prev = doorStateR();
		if (isOpenRight.get())
		{
			if (prev < 2.0F)
			{
				doorStateR.set(prev + doorSpeed.get());
			}
		}
		else
		{
			if (prev > 0.0F)
			{
				doorStateR.set(prev - doorSpeed.get());

				/* Play warn sound and, if applicable, shut sound. */
				playWarningChimeSound();

				if (prev > doorShutAt() && doorStateR() <= doorShutAt())
				{
					playDoorShutSound();
				}
			}
		}
	}

	/**
	 * Update the state of this door.
	 *
	 * @param doorState - The {@link EDoorState door state} to set.
	 */
	public void setDoorState(EDoorState doorState)
	{
		this.doorState.set(doorState);
	}

	/**
	 * Set whether the given door is physically open.
	 * 
	 * @param doorSide - Local {@link EDoorSide side} of the respective door.
	 * @param isOpen - {@code true} if the respective door is opened, {@code false} if not.
	 */
	public void setOpen(EDoorSide doorSide, boolean isOpen)
	{
		switch (doorSide)
		{
			case LEFT:
				isOpenLeft.set(isOpen);
				break;

			case RIGHT:
				isOpenRight.set(isOpen);
				break;
		}
	}



	/**
	 * Various lock states of this door.<br>
	 * These states only depict whether doors on a certain side are locked,
	 * not whether they are physically open.
	 * 
	 * @author Leshuwa Kaiheiwa
	 */
	public enum EDoorState
	{
		LOCK_ALL,
		UNLOCK_LEFT,
		UNLOCK_RIGHT,
		UNLOCK_ALL;

		/**
		 * @return {@code true} if the left set of doors is unlocked.
		 */
		public boolean isUnlockedLeft()
		{
			return (this == UNLOCK_LEFT || this == UNLOCK_ALL);
		}

		/**
		 * @return {@code true} if the right set of doors is unlocked.
		 */
		public boolean isUnlockedRight()
		{
			return (this == UNLOCK_RIGHT || this == UNLOCK_ALL);
		}

		/**
		 * Determines the door state matching the given combination of boolean flags.
		 *
		 * @param leftUnlock - {@code true} if the left set of doors is unlocked.
		 * @param rightUnlock - {@code true} if the right set of doors is unlocked.
		 * @return An {@link EDoorState} matching the given boolean flags.
		 */
		public static EDoorState fromFlags(boolean leftUnlock, boolean rightUnlock)
		{
			if (leftUnlock)
			{
				return (rightUnlock) ? UNLOCK_ALL : UNLOCK_LEFT;
			}
			return (rightUnlock) ? UNLOCK_RIGHT : LOCK_ALL;
		}

		/**
		 * Try to parse a door state from the given integer.
		 *
		 * @return The matching {@link EDoorState} or {@link #LOCK_ALL} if none matched.
		 */
		public static EDoorState fromInt(int i)
		{
			for (EDoorState state : values())
			{
				if (state.ordinal() == i)
				{
					return state;
				}
			}
			return LOCK_ALL;
		}
	}

	/**
	 * Helper enum to select either the left or right door(s).
	 * 
	 * @author Leshuwa Kaiheiwa
	 */
	public enum EDoorSide
	{
		LEFT,
		RIGHT
	}
}