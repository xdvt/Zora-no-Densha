package zoranodensha.api.vehicles.part.type.seat;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.type.PartTypeDoor;
import zoranodensha.api.vehicles.part.type.PartTypeDoor.EDoorSide;
import zoranodensha.api.vehicles.part.type.PartTypeDoor.EDoorState;
import zoranodensha.api.vehicles.part.type.PartTypeSeat;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * Helper class containing functionality for the seats' ASM hook.<br>
 * This hook tries to move players dismounting from a {@link zoranodensha.api.vehicles.part.type.PartTypeSeat seat}
 * to a safe and unoccupied position.
 * 
 * @author Leshuwa Kaiheiwa
 */
public final class ASMHook
{
	/**
	 * Part type {@link zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector selector} which selects opened {@link zoranodensha.api.vehicles.part.type.PartTypeDoor doors} only.
	 */
	public static final ITypesSelector<PartTypeDoor> openDoorsSelector = new ITypesSelector<PartTypeDoor>()
	{
		@Override
		public boolean addToList(APartType type)
		{
			return (type instanceof PartTypeDoor) && ((PartTypeDoor)type).getDoorState() != EDoorState.LOCK_ALL;
		}

		@Override
		public Class<PartTypeDoor> getTypeClass()
		{
			return PartTypeDoor.class;
		}
	};



	/**
	 * Moves the given passenger to the given in-world position.
	 * 
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} the passenger was sitting in.
	 * @param passenger - The {@link net.minecraft.entity.Entity passenger} to move.
	 * @param position - Position to move the passenger to.
	 * @return {@code true} if successful.
	 */
	public static boolean movePassengerToPosition(Train train, EntityLivingBase passenger, Vec3 position)
	{
		/* Calculate presumed passenger bounding box at the new position. */
		//@formatter:off
		AxisAlignedBB nextPositionBB = passenger.boundingBox.getOffsetBoundingBox(position.xCoord - passenger.posX,
		                                                                          position.yCoord - passenger.posY,
		                                                                          position.zCoord - passenger.posZ);
		//@formatter:on

		/* Ensure there is no intersecting bounding box at the new position. */
		if (!train.worldObj.getCollidingBoundingBoxes(train, nextPositionBB).isEmpty())
		{
			return false;
		}

		/* And also make sure there is a block below, no more than 1.5m away. */
		if (train.worldObj.func_147461_a(nextPositionBB.getOffsetBoundingBox(0.0, -2.0, 0.0)).isEmpty())
		{
			return false;
		}

		//@formatter:off
		passenger.setPositionAndRotation(position.xCoord,
		                                 position.yCoord,
		                                 position.zCoord,
		                                 passenger.rotationYaw,
		                                 passenger.rotationPitch);
		//@formatter:on

		return true;
	}

	/**
	 * Called to dismount the given entity from its seat into nearby clear space, preferring
	 * {@link zoranodensha.api.vehicles.part.type.PartTypeDoor doors} as means of exit.<br>
	 * This method is <b>invoked via Reflection</b>.
	 *
	 * @param passenger - {@link net.minecraft.entity.EntityLivingBase Entity} trying to dismount.
	 * @return {@code true} if successful.
	 */
	public static boolean movePassengerToSafePlace(EntityLivingBase passenger)
	{
		/*
		 * === Reference updates ===
		 */

		/* Ensure the passenger rides a seat. */
		Entity ridingEntity = passenger.ridingEntity;
		if (!(ridingEntity instanceof EntitySeat))
		{
			return false;
		}

		/* If the seat entity has a part, update the part's passenger field. */
		PartTypeSeat seat = ((EntitySeat)ridingEntity).getPartType();
		if (seat == null)
		{
			if (!ridingEntity.worldObj.isRemote)
			{
				ridingEntity.setDead();
			}
			return false;
		}
		seat.setPassenger(null);

		/*
		 * === Player positioning ===
		 */

		/* Get the train and section which the seat belongs to. Abort if either is missing. */
		Train train = seat.getTrain();
		if (train == null)
		{
			return false;
		}

		VehicleSection section = seat.getSection();
		if (section == null)
		{
			return false;
		}

		/*
		 * Now try to get the closest unlocked door in the section:
		 * 
		 * 1. Get the section's parts;
		 * 2. Filter them, so only doors (PartTypeDoor) which are (partially) unlocked are selected;
		 * 3. Out of all remaining doors, select the door that is closest to the vehicle part's local X-position.
		 */
		PartTypeDoor closestUnlockedDoor = section.parts.filter(openDoorsSelector).getClosestPartType(seat.getParent().getOffset()[0]);

		/*
		 * If a valid door was found, try to move the player to either open side.
		 */
		if (closestUnlockedDoor != null)
		{
			EDoorState doorState = closestUnlockedDoor.getDoorState();
			Vec3 doorView;
			Vec3 newPos;

			if (doorState.isUnlockedLeft())
			{
				doorView = closestUnlockedDoor.getDoorLookVec(EDoorSide.LEFT);
				newPos = PartHelper.getPosition(closestUnlockedDoor.getParent()).addVector(doorView.xCoord * 2, doorView.yCoord * 2, doorView.zCoord * 2);

				if (movePassengerToPosition(train, passenger, newPos))
				{
					if (closestUnlockedDoor.getDoInvertDoorStates())
					{
						closestUnlockedDoor.setOpen(EDoorSide.RIGHT, true);
						closestUnlockedDoor.doorStateR.set(2.0F);
					}
					else
					{
						closestUnlockedDoor.setOpen(EDoorSide.LEFT, true);
						closestUnlockedDoor.doorStateL.set(2.0F);
					}
					return true;
				}
			}

			if (doorState.isUnlockedRight())
			{
				doorView = closestUnlockedDoor.getDoorLookVec(EDoorSide.RIGHT);
				newPos = PartHelper.getPosition(closestUnlockedDoor.getParent()).addVector(doorView.xCoord * 2, doorView.yCoord * 2, doorView.zCoord * 2);

				if (movePassengerToPosition(train, passenger, newPos))
				{
					if (closestUnlockedDoor.getDoInvertDoorStates())
					{
						closestUnlockedDoor.setOpen(EDoorSide.LEFT, true);
						closestUnlockedDoor.doorStateL.set(2.0F);
					}
					else
					{
						closestUnlockedDoor.setOpen(EDoorSide.RIGHT, true);
						closestUnlockedDoor.doorStateR.set(2.0F);
					}
					return true;
				}
			}
		}

		/*
		 * If we are still unsuccessful, place the player next to the seat.
		 */

		/* Get the closest bogie's direction vector. We can assume there is at least one bogie present and spare the null-check. */
		Vec3 dir = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY).getClosestPartType(seat.getParent().getOffset()[0]).getDirection();
		dir = dir.normalize();
		dir.rotateAroundY(90.0F * VehicleHelper.RAD_FACTOR);

		/* Use the direction vector as offset relative to the seat's position to get the new target position. */
		Vec3 newPos = PartHelper.getPosition(seat.getParent()).addVector(dir.xCoord * 2, dir.yCoord * 2, dir.zCoord * 2);

		/* Try to move the passenger to the new target position. */
		return movePassengerToPosition(train, passenger, newPos);
	}
}