package zoranodensha.api.vehicles.part.type;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;



/**
 * Electric engines, using power input from pantographs.
 */
public class PartTypeEngineElectric extends APartTypeEngine
{
	/** Efficiency of this engine. */
	public final PropFloatBounded efficiency;
	/** Motor activity value, bounded between {@code 0.0F} and {@code 1.0F}. */
	public final PropFloatBounded motorActivity;
	/** A velocity value that represents the change of the motorActivity value, used to make the motors react more realistically. */
	private final PropFloat motorActivityVelocity;
	/**
	 * Motor polarity (whether it's accelerating or braking), between {@code -1.0F} for dynamic braking and {@code 1.0F} for accelerating.
	 */
	public final PropFloatBounded motorPolarity;
	/** Responsiveness of this engine. */
	public final PropFloatBounded responsiveness;
	/** This engine's maximum power output, in kiloWatts (kW). */
	public final PropIntegerBounded outputKW;



	public PartTypeEngineElectric(VehParBase parent)
	{
		super(parent, PartTypeEngineElectric.class.getSimpleName());

		addProperty(efficiency = (PropFloatBounded)new PropFloatBounded(parent, 0.85F, 0.01F, 1.0F, "efficiency").setConfigurable());
		addProperty(motorActivity = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 1.0F, "motorActivity").setSyncDir(ESyncDir.CLIENT));
		addProperty(motorActivityVelocity = new PropFloat(parent, "motorActivityVelocity"));
		addProperty(motorPolarity = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, -1.0F, 1.0F, "motorPolarity").setSyncDir(ESyncDir.CLIENT));
		addProperty(outputKW = (PropIntegerBounded)new PropIntegerBounded(parent, 1, 16000, "outputKW").setConfigurable());
		addProperty(responsiveness = (PropFloatBounded)new PropFloatBounded(parent, 0.5F, 0.001F, 1.0F, "responsiveness").setConfigurable());
	}


	/**
	 * Helper method to determine power output of this engine.
	 *
	 * @return This engine's current power output, in kiloNewton [kN].
	 */
	protected float getPowerOutput()
	{
		float force = 0.0F;
		PartTypeCabBasic cab = parent.getTrain().getActiveCab();
		EReverser cabReverser;

		/*
		 * Disable motors if there is no active cab.
		 */
		if (cab == null)
		{
			return 0.0F;
		}

		cabReverser = cab.getReverserState();

		/*
		 * Motor Accelerating
		 */
		if (motorPolarity.get() >= 0.60F)
		{
			force = outputKW.get();
			force *= efficiency.get() * motorActivity.get();
			force /= ((float)Math.abs(parent.getTrain().getLastSpeed() * 20.0F) + 10.0F);

			if (!cabReverser.isForwardMovement())
			{
				force *= -1.0F;
			}
		}

		/*
		 * Motor Dynamically Braking
		 */
		if (motorPolarity.get() <= -0.60F)
		{
			float trainSpeed = (float)Math.abs(parent.getTrain().getLastSpeed() * 20.0F * 3.6F);

			/*
			 * Cap the measured speed to 30 km/h, which will stop the dynamic braking effectiveness to skyrocket at high speeds.
			 */
			trainSpeed = Math.min(trainSpeed, 30.0F);

			if (trainSpeed >= 5.0F)
			{
				force = (float)(trainSpeed * -2.5F) * efficiency.get() * motorActivity.get();

				if (force < -outputKW.get())
				{
					force = -outputKW.get();
				}
				else if (force > outputKW.get())
				{
					force = outputKW.get();
				}
			}
			else
			{
				force = 0.0F;
			}
		}

		return force;
	}

	@Override
	public float getProducedForce()
	{
		/* If motor activity is not zero, determine engine output. Otherwise return nothing. */
		return (motorActivity.get() != 0.0F) ? getPowerOutput() : 0.0F; // [kN]
	}

	@Override
	public void onTick()
	{
		/* Only update on server worlds. */
		Train train = getTrain();
		if (train == null || train.worldObj.isRemote || !train.addedToChunk)
		{
			return;
		}

		/* Retrieve active cab and, if it exists, its reverser state and throttle level. */
		EReverser reverser = EReverser.LOCKED;
		float throttle = 0.0F;
		float dynamicBrake = 0.0F;
		{
			PartTypeCabBasic activeCab = train.getActiveCab();
			if (activeCab != null)
			{
				reverser = activeCab.getReverserState();
				throttle = activeCab.getThrottleLogical();
				dynamicBrake = activeCab.getDynamicBrakeLevelLogical();
			}
		}

		/*
		 * Determine the current 'activity' of the motor.
		 * The activity will increase during throttle application,
		 * and will return to 0 when idling or when the reverser is locked.
		 */

		/* Determine target activity the motor will try to achieve. */
		float activityTarget = 0.0F;
		float polarityTarget = 0.0F;

		if (reverser.isNeutral() || reverser == EReverser.LOCKED)
		{
			activityTarget = 0.0F;
			polarityTarget = 0.0F;
		}
		else
		{
			if (throttle > 0.0F && dynamicBrake <= 0.0F)
			{
				activityTarget = throttle;
				polarityTarget = 1.0F;
			}

			if (throttle <= 0.0F && dynamicBrake > 0.0F && (train.getLastSpeed() * 20.0F * 3.6F) >= 7.0F)
			{
				activityTarget = dynamicBrake;
				polarityTarget = -1.0F;
			}

			if (throttle > 0.0F && dynamicBrake > 0.0F)
			{
				activityTarget = 0.0F;
				polarityTarget = 0.0F;
			}

			if (throttle <= 0.0F && dynamicBrake <= 0.0F)
			{
				activityTarget = 0.0F;
				polarityTarget = 0.0F;
			}
		}

		/*
		 * Determine the change in activity & polarity for the traction motors.
		 */

		/* The motor polarity is correct. */
		if (motorPolarity.get() == polarityTarget)
		{
			if (motorActivity.get() != activityTarget)
			{
				motorActivityVelocity.set((activityTarget - motorActivity.get()) * 0.05F);

				if (motorActivityVelocity.get() > 0.05F)
				{
					motorActivityVelocity.set(0.05F);
				}
				else if (motorActivityVelocity.get() < -0.05F)
				{
					motorActivityVelocity.set(-0.05F);
				}
			}
		}

		/* The motor polarity needs to change. */
		else
		{
			motorActivity.set(motorActivity.get() * 0.95F);
			motorActivityVelocity.set(0.0F);

			if (motorPolarity.get() > polarityTarget)
			{
				motorPolarity.set(motorPolarity.get() - 0.04F);
			}

			if (motorPolarity.get() < polarityTarget)
			{
				motorPolarity.set(motorPolarity.get() + 0.04F);
			}
		}

		/*
		 * Motor activity velocity
		 */
		motorActivity.set(motorActivity.get() + motorActivityVelocity.get());

	}
}