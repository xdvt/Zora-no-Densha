package zoranodensha.api.vehicles.part.type.cab;

import java.util.zip.ZipFile;

import javax.annotation.Nullable;

import org.apache.logging.log4j.Level;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.StatCollector;
import net.minecraft.util.Vec3;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.ITickableProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic.ECabKey;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.core.ModCenter;



/**
 * <p>
 * Button properties used in {@link zoranodensha.api.vehicles.part.type.PartTypeCab cab part types}.<br>
 * These provide functionality to hook onto the event of keyboard key presses.<br>
 * Actual key presses are registered through Zora no Densha's input handler and are then handed down to the respective cab type.
 * </p>
 * 
 * <p>
 * This default class handles standard button mechanics of a button that is pressed down while its respective keyboard key is pressed down.<br>
 * Once the keyboard key is released, the button goes up again.
 * </p>
 * 
 * <p>
 * To hook onto a one-time event upon button press, use {@link #onPressed()}.<br>
 * In order to execute an action <i>while</i> the button is pressed, use {@link #whilePressed()}.<br>
 * When trying to get the button's state from outside, use {@link #isActive()}.
 * </p>
 * 
 * <p>
 * <b>A note on the difference between keyboard-linked buttons and cab buttons:</b><br>
 * Properties of this class which have been instantiated with a {@link #cabKey} are automatically linked to a key on the player's actual keyboard.<br>
 * However, this class can also be used to simulate keys in the cab which are accessible only by hovering over them using the implementation in
 * {@link zoranodensha.api.vehicles.part.type.PartTypeCab PartTypeCab}. In either case can this class be used to catch button presses.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PropButton extends PropBoolean implements ITickableProperty<Boolean>
{
	/** The {@link zoranodensha.api.vehicles.part.type.PartTypeCabBasic.ECabKey cab key} handled by this button. May be {@code null} if this button doesn't represent a cab key. */
	public final ECabKey cabKey;

	/** This button's hitbox, positioned relative to the parent vehicle part and sized according to the {@link #buttonSize button size}. May be {@code null}. */
	protected AxisAlignedBB buttonHitbox;
	/** Size of this button. Used for reference between NBT calls. May be {@code null}. */
	protected EButtonSize buttonSize;



	/**
	 * Create a cab button which represents the given cab key.
	 * If no cab key is specified, needs a name to specify the name used for tooltips of this button.
	 * 
	 * @param parent - Parent {@link zoranodensha.api.vehicles.part.VehParBase vehicle part}.
	 * @param cabKey - {@link zoranodensha.api.vehicles.part.type.PartTypeCabBasic.ECabKey Cab key} represented by this button. May be {@code null}.
	 * @param name - <i>Must be specified</i> if {@code cabKey} is {@code null}.
	 */
	public PropButton(VehParBase parent, @Nullable ECabKey cabKey, @Nullable String name)
	{
		super(parent, String.format("button_%s", (cabKey != null) ? cabKey.toString().toLowerCase() : name));
		this.cabKey = cabKey;

		setValidTagCalls(EValidTagCalls.PACKET_SAVE);
		setSyncDir(ESyncDir.BOTH);
	}

	/**
	 * Constructor for custom button properties in XML files.<br>
	 * Omits the {@code button_} prefix as the author of an XML file should know whether or not they want a prefix.
	 */
	public PropButton(VehParBase parent, String name)
	{
		super(parent, name);
		this.cabKey = null;

		setValidTagCalls(EValidTagCalls.PACKET_SAVE);
		setSyncDir(ESyncDir.BOTH);
	}

	/**
	 * Constructor taking the parent cab and either a cab key or the button's name <i>without</i> the {@code button_} prefix.
	 */
	public PropButton(PartTypeCabBasic cab, @Nullable ECabKey cabKey, @Nullable String name)
	{
		this(cab.getParent(), cabKey, name);
	}

	/**
	 * Simpler constructor taking the parent cab and a mandatory cab key.
	 */
	public PropButton(PartTypeCabBasic cab, ECabKey cabKey)
	{
		this(cab.getParent(), cabKey, null);
	}


	@Override
	public PropButton copy(VehParBase parent)
	{
		/*
		 * Unlike screen buttons, a standard button may be specified as custom XML property.
		 */
		PropButton button = new PropButton(parent, getName());
		if (buttonHitbox != null && buttonSize != null)
		{
			button.buttonHitbox = buttonHitbox.copy();
			button.buttonSize = buttonSize;
		}
		return button;
	}

	/**
	 * Returns a hitbox for this button, if applicable.
	 * 
	 * @return The button's {@link #buttonHitbox hitbox}, or {@code null} if there is none specified.
	 */
	@SideOnly(Side.CLIENT)
	public AxisAlignedBB getHitbox()
	{
		return buttonHitbox;
	}

	/**
	 * Tries to return a localised tooltip describing this button.
	 * Returns an unlocalised tooltip on failure.
	 * 
	 * @return A tooltip describing this button.
	 */
	public String getTooltip()
	{
		return StatCollector.translateToLocal(String.format("%s.tooltip.%s", PartTypeCab.class.getName().toLowerCase(), getName()));
	}

	/**
	 * Returns whether this button is active - i.e. whether it is pressed or not.<br>
	 * Actually just a different name for the {@link #get()} method.
	 * 
	 * @return {@code true} if this button is active.
	 */
	public boolean isActive()
	{
		return get();
	}

	/**
	 * Returns whether the given button is enabled.
	 * 
	 * @return {@code true} if this button is enabled.
	 */
	public boolean isEnabled()
	{
		return true;
	}

	/**
	 * Determines whether input (i.e. button presses) are allowed on this button.
	 * 
	 * @return {@code true} if this button may register button presses.
	 */
	public boolean isInputAllowed()
	{
		return isEnabled();
	}

	/**
	 * Called whenever this button's keyboard key state changes.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 * 
	 * @param isKeyDown - {@code true} if the keyboard key is pressed.
	 */
	@SideOnly(Side.CLIENT)
	public void onClientKeyChange(boolean isKeyDown)
	{
		set(isKeyDown);
	}

	/**
	 * Called on server worlds whenever this button is pressed.
	 */
	protected void onPressed()
	{
		;
	}

	/**
	 * Called on server worlds whenever this button was released.
	 */
	protected void onReleased()
	{
		;
	}

	@Override
	public void onUpdate()
	{
		/* Otherwise tick whilePressed() on server worlds. */
		if (isActive() && !getParent().getTrain().worldObj.isRemote)
		{
			whilePressed();
		}
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		try
		{
			//@formatter:off
			String[] values = s.split(",");
			setPositionAndSize((float)Double.parseDouble(values[0].trim()),
			                   (float)Double.parseDouble(values[1].trim()),
			                   (float)Double.parseDouble(values[2].trim()),
			                   EButtonSize.valueOf(values[3].toUpperCase().trim()));
			return true;
			//@formatter:on
		}
		catch (Exception e)
		{
			return false;
		}
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		/* If existent, read button hitbox data. */
		if (nbt.hasKey(nbtKey + "_size"))
		{
			int size = nbt.getInteger(nbtKey + "_size");
			if (size < 0 || size >= EButtonSize.values().length)
			{
				size = 0;
			}
			setPositionAndSize(nbt.getFloat(nbtKey + "_x"), nbt.getFloat(nbtKey + "_y"), nbt.getFloat(nbtKey + "_z"), EButtonSize.values()[size]);
		}
		return super.readFromNBT(nbt, nbtKey);
	}

	@Override
	public boolean set(Object property)
	{
		/* If we get a String, it may contain button hitbox data. */
		if (property instanceof String)
		{
			return parse((String)property, null);
		}

		/* Otherwise let super class handle boolean values. */
		if (super.set(property))
		{
			/* On server worlds, notify of button (un-)presses. */
			if (getParent().getTrain() != null && !getParent().getTrain().worldObj.isRemote)
			{
				if (!isInputAllowed())
				{
					return false;
				}

				if (isActive())
				{
					onPressed();
				}
				else
				{
					onReleased();
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Assigns a local position to this button and declares this button's hitbox' size.
	 * 
	 * @param x - X-position of this button, relative to the {@link #getParent() parent} vehicle part.
	 * @param y - Y-position of this button, relative to the parent vehicle part.
	 * @param z - Z-position of this button, relative to the parent vehicle part.
	 * @param size - The {@link EButtonSize size} of this button's hitbox.
	 * @return This button.
	 */
	public PropButton setPositionAndSize(float x, float y, float z, EButtonSize size)
	{
		buttonHitbox = AxisAlignedBB.getBoundingBox(x - size.size, y - size.size, z - size.size, x + size.size, y + size.size, z + size.size);
		buttonSize = size;
		return this;
	}

	/**
	 * Called while this button is down on server side, additionally to {@link #onPressed()}.
	 */
	protected void whilePressed()
	{
		;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		super.writeToNBT(nbt, nbtKey);

		/* Write button hitbox data to NBT if existent. */
		if (buttonHitbox != null && buttonSize != null)
		{
			Vec3 pos = VehicleHelper.getPosition(buttonHitbox);
			nbt.setFloat(nbtKey + "_x", (float)pos.xCoord);
			nbt.setFloat(nbtKey + "_y", (float)pos.yCoord);
			nbt.setFloat(nbtKey + "_z", (float)pos.zCoord);
			nbt.setInteger(nbtKey + "_size", buttonSize.ordinal());
		}
	}



	/**
	 * Locks into the lowered position once pressed and needs a second press to release.
	 */
	public static class PropButtonLock extends PropButton
	{
		/**
		 * See {@link PropButton#PropButton(VehParBase, ECabKey, String)} for further info.
		 */
		public PropButtonLock(VehParBase parent, @Nullable ECabKey cabKey, @Nullable String name)
		{
			super(parent, cabKey, name);
		}

		/**
		 * See {@link PropButton#PropButton(PartTypeCabBasic, ECabKey)} for further info.
		 */
		public PropButtonLock(PartTypeCabBasic cab, ECabKey cabKey)
		{
			super(cab, cabKey);
		}

		/**
		 * Constructor for custom button properties in XML files.
		 */
		public PropButtonLock(VehParBase parent, String name)
		{
			super(parent, name);
		}


		@Override
		public PropButtonLock copy(VehParBase parent)
		{
			/*
			 * Unlike screen buttons, a lock button may also be specified as custom XML property.
			 */
			PropButtonLock button = new PropButtonLock(parent, getName());
			if (buttonHitbox != null && buttonSize != null)
			{
				button.buttonHitbox = buttonHitbox.copy();
				button.buttonSize = buttonSize;
			}
			return button;
		}

		@Override
		@SideOnly(Side.CLIENT)
		public void onClientKeyChange(boolean isKeyDown)
		{
			if (isKeyDown)
			{
				set(!get());
			}
		}
	}

	/**
	 * Buttons used in conjunction with {@link zoranodensha.api.vehicles.part.type.cab.AScreen cab screens}.
	 */
	public static class PropButtonScreen extends PropButton
	{
		/** This button's parent {@link zoranodensha.api.vehicles.part.type.cab.AScreen screen}. */
		protected final DDU screen;

		/**
		 * The NBT data of this button will be saved with its unique DDU parent name added to it, however this String will remain as the base name of the button. This is the string used to get the
		 * correct texture .png for this button.
		 */
		String buttonTextureName = "";



		/**
		 * Initialises a new instance of the {@link zoranodensha.api.vehicles.part.type.cab.PropButton.PropButtonScreen} class.
		 * 
		 * @param screen - This button instance requires a parent DDU instance to be linked to. This argument specifies the appropriate screen to use.
		 * @param buttonName - The name of the button. This name will change the texture of the button accordingly.
		 */
		public PropButtonScreen(DDU screen, String buttonName)
		{
			super(screen.mtms.parentCab, null, buttonName + "_" + screen.dduSpecificName);
			this.screen = screen;
			this.buttonTextureName = buttonName;
		}

		@Override
		public PropButtonScreen copy(VehParBase parent)
		{
			/*
			 * Screen button properties must not be specified as custom XML properties.
			 * Therefore, we can ignore the copy operation.
			 */
			return null;
		}

		/**
		 * Gets the base string to use to get the correct .png texture to use for this button.
		 */
		public String getButtonTextureName()
		{
			return "button_" + this.buttonTextureName;
		}

		@Override
		public String getTooltip()
		{
			return StatCollector.translateToLocal(String.format("%s.tooltip.%s", PartTypeCab.class.getName().toLowerCase(), getButtonTextureName()));
		}

		@Override
		public boolean isInputAllowed()
		{
			return super.isInputAllowed() && screen.isInputAllowed();
		}

		@Override
		public void onClientKeyChange(boolean isKeyDown)
		{
			super.onClientKeyChange(isKeyDown);
		}

		@Override
		protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
		{
			/*
			 * Use screen.dduSpecificName here in the nbt key name?
			 */
			boolean value = super.readFromNBT(nbt, nbtKey);
			this.buttonTextureName = nbt.getString(nbtKey + ".textureName");

			return value;
		}

		@Override
		protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
		{
			nbt.setString(nbtKey + ".textureName", this.buttonTextureName);
			super.writeToNBT(nbt, nbtKey);
		}
	}

	/**
	 * Enum describing the size of a button.<br>
	 * Used to calculate the dimensions of a button's hitbox.
	 */
	public enum EButtonSize
	{
		SMALL(0.005F),
		MEDIUM(0.01F),
		LARGE(0.025F);



		/** Hitbox radius. */
		public final float size;



		EButtonSize(float size)
		{
			this.size = size;
		}
	}
}