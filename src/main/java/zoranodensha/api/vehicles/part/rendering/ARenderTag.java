package zoranodensha.api.vehicles.part.rendering;

import java.util.ArrayList;

import org.apache.commons.lang3.ArrayUtils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.IXMLProperty;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.xml.XMLTreeNode;



/**
 * Render tags used to define rendering of XML-based part models.<br>
 * <i>Client-side only.</i>
 * 
 * @author Leshuwa Kaiheiwa
 */
@SideOnly(Side.CLIENT)
public abstract class ARenderTag
{
	/** Array of child tags. May be {@code null}. */
	private ARenderTag[] children;
	/** Item render types during which this transformation may be applied. If {@code null}, applies transformation always. */
	protected ArrayList<ItemRenderType> renderTypes;



	/**
	 * Parses render types from the given node for this render tag.
	 * 
	 * @see net.minecraftforge.client.IItemRenderer.ItemRenderType ItemRenderType
	 */
	protected ARenderTag(XMLTreeNode node)
	{
		XMLTreeNode renderTypesNode = node.getChildByName("renderTypes");
		if (renderTypesNode != null)
		{
			String[] arr = renderTypesNode.getValue().split(",");
			for (String s : arr)
			{
				s = s.trim().toUpperCase();

				try
				{
					ItemRenderType renderType = ItemRenderType.valueOf(s);
					if (renderTypes == null)
					{
						renderTypes = new ArrayList<ItemRenderType>();
					}
					renderTypes.add(renderType);
				}
				catch (IllegalArgumentException e)
				{
					APILogger.warn(ARenderTag.class, String.format("Ignoring invalid ItemRenderType constant: '%s'", s));
				}
			}
		}
	}

	/**
	 * Add the given render tag as child to this render tag.
	 * 
	 * @param child - Render tag to append.
	 */
	public void addChild(ARenderTag child)
	{
		if (children == null || children.length == 0)
		{
			children = new ARenderTag[] { child };
		}
		else
		{
			children = ArrayUtils.add(children, child);
		}
	}

	/**
	 * Apply the transformation represented by this render tag.
	 * 
	 * @param partialTick - Partial tick time, usually between {@code 0.0F} and {@code 1.0F}.
	 * @param renderType - Current render call's item {@link net.minecraftforge.client.IItemRenderer.ItemRenderType render type}. {@code null} if no item is being rendered.
	 */
	public void apply(float partialTick, ItemRenderType renderType)
	{
		if (children != null)
		{
			for (ARenderTag renderTag : children)
			{
				renderTag.apply(partialTick, renderType);
			}
		}
	}


	/**
	 * Helper method to search for a property specified by the given String of given type in the given part.
	 * 
	 * @param propertyToFind - A path of format {@code $PART_TYPE.PROPERTY_NAME} or {@code $PROPERTY_NAME} which specifies what is searched for.
	 * @param typeClass - Expected class type of the property.
	 * @param part - {@link zoranodensha.api.vehicles.part.VehParBase Part} which will be scanned for the specified property.
	 * @return The property specified by the given path, or {@code null} if it couldn't be found.
	 */
	public static <T extends APartProperty<?>> T findProperty(String propertyToFind, Class<T> typeClass, VehParBase part)
	{
		if (propertyToFind.startsWith("$"))
		{
			propertyToFind = propertyToFind.substring(1);
		}

		if (!propertyToFind.isEmpty())
		{
			String[] s = propertyToFind.split("\\.");
			switch (s.length)
			{
				case 1:
					for (IXMLProperty<?> partProperty : part.getXMLProperties())
					{
						if (partProperty.getName().equals(s[0]) && typeClass.isInstance(partProperty))
						{
							return typeClass.cast(partProperty);
						}
					}
					break;

				case 2:
					for (APartType partType : part.getPartTypes())
					{
						if (partType.getName().equals(s[0]))
						{
							for (IPartProperty<?> partProperty : partType.getProperties())
							{
								if (partProperty.getName().equals(s[1]) && typeClass.isInstance(partProperty))
								{
									return typeClass.cast(partProperty);
								}
							}
							break;
						}
					}
					break;
			}
		}
		return null;
	}
}