package zoranodensha.api.vehicles.part.rendering;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.xml.XMLTreeNode;



/**
 * Matrix render tag. Wraps all children of this tag into a translation matrix.<br>
 * <i>Client-side only.</i>
 * 
 * @see zoranodensha.api.vehicles.part.xml.ERendererTags#MATRIX MATRIX
 * @see org.lwjgl.opengl.GL11#glPushMatrix() glPushMatrix()
 * @see org.lwjgl.opengl.GL11#glPopMatrix() glPopMatrix()
 */
@SideOnly(Side.CLIENT)
public class Matrix extends ARenderTag
{
	public Matrix(XMLTreeNode node)
	{
		super(node);
	}

	@Override
	public void apply(float partialTick, ItemRenderType renderType)
	{
		GL11.glPushMatrix();
		{
			super.apply(partialTick, renderType);
		}
		GL11.glPopMatrix();
	}
}