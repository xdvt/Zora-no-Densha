package zoranodensha.api.vehicles.part.rendering;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * An interface implemented by all classes rendering {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts}.
 * 
 * @author Leshuwa Kaiheiwa
 */
@SideOnly(Side.CLIENT)
public interface IVehiclePartRenderer
{
	/**
	 * Create a copy of this renderer for the given vehicle part.
	 */
	IVehiclePartRenderer copy(VehParBase copyPart);

	/**
	 * Return {@code true} if this part renders in the given render pass.<br>
	 * Can be used to render (semi-)transparent parts.<br>
	 * <br>
	 * Render passes are:<br>
	 * 0 = Default render pass<br>
	 * 1 = Alpha-channel render pass, used to render transparent objects.
	 *
	 * @param pass - Current render pass.
	 * @return Whether this part renders in the given pass.
	 */
	boolean renderInPass(int pass);

	/**
	 * Render a vehicle part model.
	 *
	 * @param partialTick - Partial tick time, used for position interpolation.
	 */
	void renderPart(float partialTick);

	/**
	 * Render a vehicle part model as item.
	 *
	 * @param type - {@link net.minecraftforge.client.IItemRenderer.ItemRenderType Render type}.
	 */
	void renderPartItem(IItemRenderer.ItemRenderType type);



	/**
	 * Parts with non-generic rendering behaviour should implement this interface to gain control over pre- and post- render actions.
	 */
    interface IVehiclePartSpecialRenderer extends IVehiclePartRenderer
	{
		/**
		 * Called to undo any manual preparation of the OpenGL render engine.<br>
		 * <br>
		 * During vehicle rendering, this method will be run on the same matrix as {@link #renderPart(float)}
		 * and {@link #renderPart_Pre(float)}.<br>
		 * This method is useful if there is need to configure OpenGL after the part was rendered.
		 */
		void renderPart_Post();

		/**
		 * Called to allow manual preparation of the OpenGL render engine.<br>
		 * <br>
		 * During vehicle rendering, this method will be run on the same matrix as {@link #renderPart(float)}
		 * and {@link #renderPart_Post()}.<br>
		 * This method is useful if there is need to configure OpenGL before default preparation is applied,
		 * or to override default preparation.
		 *
		 * @param partialTick - Partial tick time, used for position interpolation.
		 * @return {@code true} to cancel default OpenGL preparation routines.
		 */
		boolean renderPart_Pre(float partialTick);
	}
}
