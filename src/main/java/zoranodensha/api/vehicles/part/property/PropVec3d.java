package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding {@link net.minecraft.util.Vec3 vectors}.
 */
public class PropVec3d extends APartProperty<Vec3> implements IXMLProperty<Vec3>
{
	public PropVec3d(VehParBase parent, String name)
	{
		super(parent, Vec3.class, name);
		set(Vec3.createVectorHelper(0, 0, 0));
	}


	@Override
	public PropVec3d copy(VehParBase copyParent)
	{
		PropVec3d prop = new PropVec3d(copyParent, getName());
		if (get() != null)
		{
			prop.set(Vec3.createVectorHelper(get().xCoord, get().yCoord, get().zCoord));
		}

		return prop;
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		String[] s_arr = s.split(",");
		if (s_arr.length == 3)
		{
			double[] arr = new double[3];
			for (int i = 0; i < arr.length; ++i)
			{
				try
				{
					arr[i] = Double.parseDouble(s_arr[i]);
				}
				catch (NumberFormatException e)
				{
					return false;
				}
			}
			return set(arr);
		}
		return false;
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey + "_x"))
		{
			set(Vec3.createVectorHelper(nbt.getDouble(nbtKey + "_x"), nbt.getDouble(nbtKey + "_y"), nbt.getDouble(nbtKey + "_z")));
			return true;
		}
		return false;
	}

	@Override
	public boolean set(Object property)
	{
		if (property instanceof double[])
		{
			double[] arr = (double[])property;
			if (arr.length == 3)
			{
				property = Vec3.createVectorHelper(arr[0], arr[1], arr[2]);
			}
		}

		if (property instanceof Vec3)
		{
			Vec3 oldVal = get();
			if (oldVal != null)
			{
				Vec3 newVal = (Vec3)property;
				if (Double.doubleToLongBits(newVal.xCoord) == Double.doubleToLongBits(oldVal.xCoord))
				{
					if (Double.doubleToLongBits(newVal.yCoord) == Double.doubleToLongBits(oldVal.yCoord))
					{
						if (Double.doubleToLongBits(newVal.zCoord) == Double.doubleToLongBits(oldVal.zCoord))
						{
							return false;
						}
					}
				}
			}
			return super.set(property);
		}
		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		Vec3 vec = get();
		nbt.setDouble(nbtKey + "_x", vec.xCoord);
		nbt.setDouble(nbtKey + "_y", vec.yCoord);
		nbt.setDouble(nbtKey + "_z", vec.zCoord);
	}
}