package zoranodensha.api.vehicles.part.property;

import org.apache.logging.log4j.Level;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.common.core.ModCenter;



/**
 * <h1>Pressure Vessel Property</h1>
 * <p>
 * Property to represent an air pressure vessel of some kind (e.g. pipe, tank, etc).
 * </p>
 * <p>
 * This property's float value ({@link PropFloat#get()}) is a representation of pressure within this value, <b>measured in {@code kPa}</b>.
 * </p>
 * 
 * @author Jaffa
 */
public class PropVessel extends PropFloatBounded
{
	/** The capacity of this air pressure vessel in {@code L} (litres). */
	private float capacity;

	public float valueAfter;
	public float valueBefore;
	public float valueModification;



	/**
	 * Initialises a new instance of the {@link zoranodensha.api.vehicles.part.property.PropVessel} class.
	 * 
	 * @param parent - The parent vehicle part that this air pressure vessel belongs to.
	 * @param capacity - The capacity of this air pressure vessel in {@code L} (litres).
	 * @param maximumPressure - The maximum pressure this vessel can hold.
	 * @param startingPressure - The starting pressure to create this vessel at.
	 * @param name - The identifying name to use for this property.
	 */
	public PropVessel(VehParBase parent, float capacity, float maximumPressure, float startingPressure, String name)
	{
		super(parent, startingPressure * capacity, 0.0F, maximumPressure * capacity, name);

		this.capacity = capacity;
		this.valueAfter = 0.0F;
		this.valueBefore = 0.0F;
		this.valueModification = 0.0F;
	}

	/**
	 * @return - The capacity of this vessel in {@code L} (litres).
	 */
	public float getCapacity()
	{
		return this.capacity;
	}

	/**
	 * @return - The maximum possible pressure of this vessel in {@code kPa}.
	 */
	public float getMaximumPressure()
	{
		return this.max / this.capacity;
	}

	/**
	 * @return - The pressure within this vessel, in {@code kPa}.
	 */
	public float getPressure()
	{
		return this.get() / capacity;
	}

	/**
	 * Gets the 'units' of pressure this vessel has. For example, a 1 {@code L} container with 50 {@code kPa} contains 50 'pressure units' whilst a 3 {@code L} container with 50 {@code kPa} contains
	 * 150 'pressure units'.
	 * 
	 * @return - A {@code float} representing the value of pressure units this vessel has. Divide this number by this vessel's capacity to get the pressure of this vessel in {@code kPa}.
	 */
	public float getUnits()
	{
		return this.get();
	}

	/**
	 * Called to increase the pressure in this vessel by the supplied number of air units {@code (capacity * maxPressure = maxUnits)}.
	 * 
	 * @param units - The number of air units to increase this vessel's pressure by.
	 */
	public void increase(float units)
	{
		/* Zero catch. */
		if (units <= 0.0F)
		{
			return;
		}

		/* If the new pressure would exceed the maximum pressure of this vessel, make sure no extra air gets in. */
		if (get() + units > max)
		{
			valueModification += max - get();
			set(max);
		}
		else
		{
			valueModification += units;
			set(get() + units);
		}
	}

	/**
	 * @return - {@code true} if this vessel's pressure is 0 {@code kPa}.
	 */
	public boolean isEmpty()
	{
		return getPressure() <= 0.0F;
	}

	/**
	 * @return - {@code true} if this vessel's pressure has reached its maximum supported pressure.
	 */
	public boolean isFull()
	{
		return getPressure() >= this.max;
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey + "_vesselCapacity"))
		{
			this.capacity = nbt.getFloat(nbtKey + "_vesselCapacity");
		}

		if (nbt.hasKey(nbtKey + "_valueAfter"))
		{
			this.valueAfter = nbt.getFloat(nbtKey + "_valueAfter");
		}

		if (nbt.hasKey(nbtKey + "_valueBefore"))
		{
			this.valueBefore = nbt.getFloat(nbtKey + "_valueBefore");
		}

		if (nbt.hasKey(nbtKey + "_valueModification"))
		{
			this.valueModification = nbt.getFloat(nbtKey + "_valueModification");
		}

		return super.readFromNBT(nbt, nbtKey);
	}

	/**
	 * Called to set this pressure vessel to an exact pressure.
	 * 
	 * @param pressure - The pressure to set this vessel to, in {@code kPa}.
	 * @return - If the pressure of the vessel was changed as a result of this method, {@code true} will be returned. If the pressure remains the same, a {@code false} will be returned.
	 */
	public boolean setPressure(float pressure)
	{
		float units = pressure * getCapacity();

		return set(units);
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		super.writeToNBT(nbt, nbtKey);

		nbt.setFloat(nbtKey + "_vesselCapacity", this.capacity);
		nbt.setFloat(nbtKey + "_valueAfter", this.valueAfter);
		nbt.setFloat(nbtKey + "_valueBefore", this.valueBefore);
		nbt.setFloat(nbtKey + "_valueModification", this.valueModification);
	}

}
