package zoranodensha.api.vehicles.part.property;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding an array of {@link net.minecraftforge.fluids.FluidTank fluid tanks}.
 */
public class PropTank extends APartProperty<FluidTank[]>
{
	/**
	 * Initialises this tank property with empty tank data.
	 */
	public PropTank(VehParBase parent, String name)
	{
		this(parent, new FluidTank[0], name);
	}

	/**
	 * Create a new tank property with given tank data.
	 */
	public PropTank(VehParBase parent, FluidTank[] tanks, String name)
	{
		super(parent, FluidTank[].class, name);
		set(tanks);
	}

	/**
	 * Create a new array of tanks of given size, with given capacity per tank.
	 */
	public PropTank(VehParBase parent, int size, int capacityPerTank, String name)
	{
		this(parent, new FluidTank[size], name);

		FluidTank[] tanks = get();
		for (int i = 0; i < tanks.length; ++i)
		{
			tanks[i] = new FluidTankProperty(this, capacityPerTank);
		}
	}


	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		/* Retrieve all tanks. */
		FluidTank[] tanks = get();
		if (tanks == null || tanks.length == 0 || !nbt.hasKey(nbtKey))
		{
			/* If there are no tanks specified, try to read dimensions from NBT. */
			if ((tanks == null || tanks.length == 0) && nbt.hasKey(nbtKey + "_size"))
			{
				tanks = new FluidTank[nbt.getInteger(nbtKey + "_size")];
			}
			else
			{
				return false;
			}
		}

		/* Retrieve list of NBT tags holding tank data. */
		NBTTagList tagList = nbt.getTagList(nbtKey, 10);
		NBTTagCompound nbt0;
		byte slot;

		for (int i = 0; i < tagList.tagCount(); ++i)
		{
			/* Grab tag at given index. */
			nbt0 = tagList.getCompoundTagAt(i);
			slot = nbt0.getByte("slot");

			/* If slot ID is valid and tank exists, let tank read data from NBT tag. */
			if (slot >= 0 && slot < tanks.length && tanks[slot] != null)
			{
				tanks[slot].readFromNBT(nbt0);
			}
		}

		return true;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		/* Retrieve all tanks. */
		FluidTank[] tanks = get();
		if (tanks == null)
		{
			return;
		}

		/* Create a list of NBT tags holding tank data. */
		NBTTagList tagList = new NBTTagList();
		NBTTagCompound nbt0;

		for (int slotID = 0; slotID < tanks.length; ++slotID)
		{
			/* Retrieve the slot's content. */
			nbt0 = new NBTTagCompound();
			nbt0.setByte("Slot", (byte)slotID);

			/* Write the tank, if existent. */
			if (tanks[slotID] != null)
			{
				tanks[slotID].writeToNBT(nbt0);
			}

			/* Append to tag list. */
			tagList.appendTag(nbt0);
		}

		/* Append tag list and write tank count to NBT. */
		nbt.setTag(nbtKey, tagList);
		nbt.setInteger(nbtKey + "_size", tanks.length);
	}



	/**
	 * Subclass of Forge's default {@link net.minecraftforge.fluids.FluidTank fluid tank} implementation,
	 * adding callbacks to the parent property.
	 */
	public static class FluidTankProperty extends FluidTank
	{
		/** This fluid tank's parent property. */
		private final PropTank parent;



		public FluidTankProperty(PropTank parent, int capacity)
		{
			super(capacity);
			this.parent = parent;
		}

		@Override
		public FluidStack drain(int maxDrain, boolean doDrain)
		{
			FluidStack ret = super.drain(maxDrain, doDrain);
			if (ret != null && doDrain)
			{
				parent.setHasChanged(true);
			}
			return ret;
		}

		@Override
		public int fill(FluidStack resource, boolean doFill)
		{
			int ret = super.fill(resource, doFill);
			if (ret > 0 && doFill)
			{
				parent.setHasChanged(true);
			}
			return ret;
		}
	}
}