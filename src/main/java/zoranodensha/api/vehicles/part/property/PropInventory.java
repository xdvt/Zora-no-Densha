package zoranodensha.api.vehicles.part.property;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding an array of {@link net.minecraft.item.ItemStack item stacks}, used as inventory.
 */
public class PropInventory extends APartProperty<ItemStack[]>
{
	/**
	 * Initialises an empty inventory property.
	 */
	public PropInventory(VehParBase parent, String name)
	{
		this(parent, 0, name);
	}

	/**
	 * Create an inventory property with specified inventory size.
	 * 
	 * @param parent - This property's parent {@link zoranodensha.api.vehicles.part.VehParBase part}.
	 * @param inventorySize - Number of slots this inventory has.
	 * @param name - Name of this property.
	 */
	public PropInventory(VehParBase parent, int inventorySize, String name)
	{
		super(parent, ItemStack[].class, name);
		set(new ItemStack[inventorySize]);
	}


	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		/*
		 * Retrieve inventory.
		 */
		ItemStack[] inv = get();
		if (inv == null || !nbt.hasKey(nbtKey))
		{
			return false;
		}

		/*
		 * Retrieve property data from NBT.
		 */
		NBTTagList tagList;
		{
			NBTBase tag = nbt.getTag(nbtKey);
			if (tag instanceof NBTTagCompound)
			{
				NBTTagCompound data = (NBTTagCompound)tag;
				if (data.hasKey("size"))
				{
					setSize(data.getInteger("size"));
				}

				tagList = data.getTagList("slots", 10);
			}
			else
			{
				// TODO COMPATIBILITY - Legacy implementation so older inventories can convert well.
				tagList = (NBTTagList)tag;
			}
		}

		/*
		 * Populate inventory from NBT data.
		 */
		{
			NBTTagCompound nbt0;
			byte slot;

			for (int i = 0; i < tagList.tagCount(); ++i)
			{
				/* Grab tag at given index. */
				nbt0 = tagList.getCompoundTagAt(i);
				slot = nbt0.getByte("slot");

				/* If slot ID is valid, try to read and place an ItemStack in the respective slot. */
				if (slot >= 0 && slot < inv.length)
				{
					inv[slot] = ItemStack.loadItemStackFromNBT(nbt0);
				}
			}
		}

		return true;
	}

	/**
	 * Override this property's number of slots.<br>
	 * <br>
	 * Note: This implementation does not take items into account which may potentially
	 * be eliminated if the new inventory size is smaller than the previous size.
	 */
	public void setSize(int newSize)
	{
		/* If the current size is already equal to the specified value, do nothing. */
		ItemStack[] oldInv = get();
		if (oldInv != null && oldInv.length == newSize)
		{
			return;
		}

		/* Otherwise merge old inventory into new inventory. */
		ItemStack[] newInv = new ItemStack[newSize];
		if (oldInv != null)
		{
			System.arraycopy(oldInv, 0, newInv, 0, Math.min(oldInv.length, newInv.length));
		}
		set(newInv);
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		/* Retrieve inventory. */
		ItemStack[] inv = get();
		if (inv == null)
		{
			return;
		}

		/* Create a list of NBT tags holding each inventory slot. */
		NBTTagList tagList = new NBTTagList();
		NBTTagCompound nbt0;

		for (int slotID = 0; slotID < inv.length; ++slotID)
		{
			/* Retrieve the slot's content. */
			nbt0 = new NBTTagCompound();
			nbt0.setByte("slot", (byte)slotID);

			/* Write the stack, if existent. */
			if (inv[slotID] != null)
			{
				inv[slotID].writeToNBT(nbt0);
			}

			/* Append to tag list. */
			tagList.appendTag(nbt0);
		}

		/* Write inventory size and slot data, then append to given NBT tag. */
		NBTTagCompound data = new NBTTagCompound();
		data.setInteger("size", inv.length);
		data.setTag("slots", tagList);
		nbt.setTag(nbtKey, data);
	}
}