package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding long values.
 */
public class PropLong extends APartProperty<Long> implements IXMLProperty<Long>
{
	public PropLong(VehParBase parent, String name)
	{
		this(parent, 0L, name);
	}

	public PropLong(VehParBase parent, long val, String name)
	{
		super(parent, Long.class, name);
		set(val);
	}


	@Override
	public PropLong copy(VehParBase parent)
	{
		return new PropLong(parent, get(), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		try
		{
			return super.set(Long.parseLong(s));
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			set(nbt.getLong(nbtKey));
			return true;
		}
		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		nbt.setLong(nbtKey, get());
	}



	/**
	 * Properties holding bounded long values.
	 */
	public static class PropLongBounded extends PropLong
	{
		/** Lower bound, inclusive. */
		protected final long min;
		/** Upper bound, inclusive. */
		protected final long max;



		public PropLongBounded(VehParBase parent, long min, long max, String name)
		{
			this(parent, 0L, min, max, name);
		}

		public PropLongBounded(VehParBase parent, long val, long min, long max, String name)
		{
			super(parent, val, name);

			this.min = min;
			this.max = max;

			/*
			 * Initial value of 'val' might not have been assigned since it was initialised before its bounds.
			 * Ensure the value will be assigned after bounds are defined.
			 */
			set(val);
		}


		@Override
		public PropLongBounded copy(VehParBase parent)
		{
			return new PropLongBounded(parent, get(), min, max, getName());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Long)
			{
				Long newVal = (Long)property;
				if (newVal < min)
				{
					newVal = min;
				}
				else if (newVal > max)
				{
					newVal = max;
				}
				return super.set(newVal);
			}
			return false;
		}
	}
}