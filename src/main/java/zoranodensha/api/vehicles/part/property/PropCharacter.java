package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding character values.
 */
public class PropCharacter extends APartProperty<Character> implements IXMLProperty<Character>
{
	public PropCharacter(VehParBase parent, String name)
	{
		this(parent, (char)0, name);
	}

	public PropCharacter(VehParBase parent, Character val, String name)
	{
		super(parent, Character.class, name);
		set(val);
	}


	@Override
	public PropCharacter copy(VehParBase parent)
	{
		return new PropCharacter(parent, get(), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		return (s.length() == 1) && super.set(s.charAt(0));
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			set((char)nbt.getInteger(nbtKey));
			return true;
		}
		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		nbt.setInteger(nbtKey, get());
	}



	/**
	 * Properties holding bounded integer values.
	 */
	public static class PropCharacterBounded extends PropCharacter
	{
		/** Lower bound, inclusive. */
		protected final Character min;
		/** Upper bound, inclusive. */
		protected final Character max;



		public PropCharacterBounded(VehParBase parent, Character min, Character max, String name)
		{
			this(parent, (char)0, min, max, name);
		}

		public PropCharacterBounded(VehParBase parent, Character val, Character min, Character max, String name)
		{
			super(parent, val, name);

			this.min = min;
			this.max = max;

			/*
			 * Initial value of 'val' might not have been assigned since it was initialised before its bounds.
			 * Ensure the value will be assigned after bounds are defined.
			 */
			set(val);
		}


		@Override
		public PropCharacterBounded copy(VehParBase parent)
		{
			return new PropCharacterBounded(parent, get(), min, max, getName());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Character)
			{
				Character newVal = (Character)property;
				if (newVal < min)
				{
					newVal = min;
				}
				else if (newVal > max)
				{
					newVal = max;
				}
				return super.set(newVal);
			}
			return false;
		}
	}
}