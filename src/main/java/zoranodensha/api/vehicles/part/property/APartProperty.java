package zoranodensha.api.vehicles.part.property;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.event.property.PropertyMissingNBTEvent;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Abstract implementation of vehicle part properties.
 * 
 * @see zoranodensha.api.vehicles.part.property.IPartProperty IPartProperty
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class APartProperty<TYPE> implements IPartProperty<TYPE>
{
	/*
	 * Finalised values.
	 */
	/** Property key. */
	protected final int key;

	/** Parent {@link zoranodensha.api.vehicles.part.VehParBase vehicle part}. */
	protected final VehParBase parent;
	/** Type class of the wrapped property. */
	protected final Class<TYPE> clazz;

	/** {@link net.minecraft.nbt.NBTTagCompound NBT} key String, used for writing to and reading from NBT. Automatically generated from part and property name. */
	protected final String nbtKey;
	/** Property name String. */
	protected final String name;
	/** Unlocalised property name String. */
	protected final String unlocalisedName;

	/*
	 * Values assigned by setters.
	 */
	/** {@code true} if this property may be configured in the Model Editor. */
	protected boolean isConfigurable;
	/** An enum declaring which {@link zoranodensha.api.util.ETagCall NBT tag calls} are valid for this property. Defaults to {@link EValidTagCalls#ANY ANY}. */
	protected EValidTagCalls validTagCalls = EValidTagCalls.ANY;
	/** {@link zoranodensha.api.util.ESyncDir Synchronisation direction} of this property. */
	protected ESyncDir syncDir = ESyncDir.NOSYNC;
	/** Wrapped property. May be {@code null} during constructor call, but should generally be non-{@code null}. */
	protected TYPE property;

	/**
	 * System time stamp of the last property change using {@link #set(Object)}.<br>
	 * If the property is set during synchronisation {@link #readFromNBT(NBTTagCompound, ETagCall) read}, the time stamp gets overridden by the packet's time stamp.
	 */
	protected long changeTime;
	/** {@code true} after this property has changed and waits for synchronisation. */
	protected boolean hasChanged;



	/**
	 * Constructor for a basic vehicle part property.
	 * 
	 * @param parent - Parent vehicle part of this vehicle part property.
	 * @param clazz - Class type of the held property.
	 * @param name - Name of this property.
	 * 
	 * @throws NullPointerException If either argument was {@code null}.
	 */
	public APartProperty(VehParBase parent, Class<TYPE> clazz, String name) throws NullPointerException
	{
		if (parent == null || clazz == null || name == null)
		{
			throw new NullPointerException(String.format("Parent or class type were null! [P:%b, C:%b, N:%b]", (parent != null), (clazz != null), (name != null)));
		}

		this.parent = parent;
		this.clazz = clazz;
		this.name = name;
		this.nbtKey = parent.getName() + "_" + name;
		this.unlocalisedName = "zoranodensha.editor.prop." + name;
		this.key = getNextUnusedKey();
	}


	@Override
	public TYPE get()
	{
		return property;
	}

	@Override
	public boolean getHasChanged()
	{
		return hasChanged;
	}

	@Override
	public boolean getIsConfigurable()
	{
		return isConfigurable;
	}

	@Override
	public boolean getIsValidTagCall(ETagCall tagCall)
	{
		return validTagCalls.isValid(tagCall);
	}

	@Override
	public int getKey()
	{
		return key;
	}

	@Override
	public String getName()
	{
		return name;
	}

	/**
	 * Determines an unused ID key for this property.
	 */
	private int getNextUnusedKey()
	{
		int maxKey = 0;
		for (IPartProperty<?> prop : getParent().getProperties())
		{
			if (prop.getKey() > maxKey)
			{
				maxKey = prop.getKey();
			}
		}
		return (maxKey + 1);
	}

	@Override
	public VehParBase getParent()
	{
		return parent;
	}

	@Override
	public ESyncDir getSyncDir()
	{
		return syncDir;
	}

	@Override
	public Class<TYPE> getTypeClass()
	{
		return clazz;
	}

	@Override
	public String getUnlocalisedName()
	{
		return unlocalisedName;
	}

	@Override
	public EValidTagCalls getValidTagCalls()
	{
		return validTagCalls;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt, ETagCall callType)
	{
		/* Ensure this property has an NBT tag key and the given call type is valid. */
		if (nbtKey == null || !validTagCalls.isValid(callType))
		{
			return;
		}

		/* In case of bi-directional synchronisation, handle NBT reading differently. */
		if (ESyncDir.BOTH == getSyncDir() && ETagCall.PACKET == callType)
		{
			long changeTimePacket = nbt.getLong(nbtKey + "_CHANGE_TIME");
			if (changeTimePacket > changeTime)
			{
				readFromNBTWithEvent(nbt);

				if (getHasChanged())
				{
					changeTime = changeTimePacket;
				}
			}
		}

		/* Otherwise simply read the data from NBT without further ado. */
		else
		{
			readFromNBTWithEvent(nbt);
		}
	}

	/**
	 * Helper method to read this property from the given NBT tag, also ensuring a potential legacy check is executed via events.
	 * 
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to read from.
	 * @see zoranodensha.api.vehicles.event.property.PropertyMissingNBTEvent PropertyMissingNBTEvent
	 */
	private void readFromNBTWithEvent(NBTTagCompound nbt)
	{
		/* If the first NBT read call failed, post PropertyMissingNBTEvent and let custom implementation handle further actions. */
		if (!readFromNBT(nbt, nbtKey))
		{
			MinecraftForge.EVENT_BUS.post(new PropertyMissingNBTEvent(nbt, nbtKey, this));
		}
	}

	/**
	 * Actually handles reading from NBT.
	 * 
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to read from.
	 * @param nbtKey - NBT key String of this property.
	 * @return {@code true} if successful.
	 */
	protected abstract boolean readFromNBT(NBTTagCompound nbt, String nbtKey);

	@Override
	public boolean set(Object val)
	{
		if ((property == null || !property.equals(val)) && clazz.isInstance(val))
		{
			property = clazz.cast(val);
			setHasChanged(true);
			return true;
		}
		return false;
	}

	@Override
	public APartProperty<TYPE> setConfigurable()
	{
		isConfigurable = true;
		return this;
	}

	@Override
	public void setHasChanged(boolean hasChanged)
	{
		/* Update change flag and time stamp, where applicable. */
		if (getSyncDir() != ESyncDir.NOSYNC)
		{
			this.hasChanged = hasChanged;

			/* Update change time stamp if required. */
			if (this.hasChanged && getSyncDir() == ESyncDir.BOTH)
			{
				changeTime = System.currentTimeMillis();
			}
		}

		/* Notify parent part of property change. */
		if (this.hasChanged)
		{
			parent.onPropertyChanged(this);
		}
	}

	@Override
	public APartProperty<TYPE> setSyncDir(ESyncDir syncDir)
	{
		if (syncDir != null)
		{
			this.syncDir = syncDir;
		}
		return this;
	}

	@Override
	public APartProperty<TYPE> setValidTagCalls(EValidTagCalls validTagCalls)
	{
		this.validTagCalls = validTagCalls;
		return this;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt, ETagCall callType)
	{
		if (nbtKey != null && validTagCalls.isValid(callType))
		{
			writeToNBT(nbt, nbtKey);

			/* Write change time stamp if this property gets synchronised bi-directionally. */
			if (ETagCall.PACKET == callType && getSyncDir() == ESyncDir.BOTH)
			{
				nbt.setLong(nbtKey + "_CHANGE_TIME", changeTime);
			}
		}
	}

	/**
	 * Actually handles writing to NBT.
	 * 
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to write to.
	 * @param nbtKey - NBT key String of this property.
	 */
	protected abstract void writeToNBT(NBTTagCompound nbt, String nbtKey);
}