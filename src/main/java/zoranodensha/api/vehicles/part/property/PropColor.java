package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding {@link zoranodensha.api.util.ColorRGBA RGB color} values.
 */
public class PropColor extends APartProperty<ColorRGBA> implements IXMLProperty<ColorRGBA>
{
	public PropColor(VehParBase parent, String name)
	{
		this(parent, new ColorRGBA(), name);
	}

	public PropColor(VehParBase parent, ColorRGBA val, String name)
	{
		super(parent, ColorRGBA.class, name);
		set(val);
	}


	/**
	 * Bridge method to {@link zoranodensha.api.util.ColorRGBA#apply() apply()}<br>
	 * <br>
	 * <i>Client-side only.</i>
	 */
	@SideOnly(Side.CLIENT)
	public void apply()
	{
		get().apply();
	}

	/**
	 * Applies the wrapped color directly to the given {@link net.minecraft.client.renderer.Tessellator tessellator}.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 */
	@SideOnly(Side.CLIENT)
	public void apply(Tessellator tessellator)
	{
		ColorRGBA color = get();
		tessellator.setColorRGBA_F(color.getR(), color.getG(), color.getB(), color.getA());
	}

	@Override
	public PropColor copy(VehParBase parent)
	{
		return new PropColor(parent, (get() == null ? null : get().clone()), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		return set(s);
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (get().readFromNBT(nbt, nbtKey))
		{
			getParent().onPropertyChanged(this);
			return true;
		}
		return false;
	}

	/**
	 * Overwrite the previous property with the given <b>non-null</b> value.
	 * 
	 * @param property - The new {@link zoranodensha.api.util.ColorRGBA color} to set. May also be a triplet of {@code float}s.
	 * @return {@code true} if the new property was successfully set.
	 */
	@Override
	public boolean set(Object property)
	{
		if (property instanceof Integer)
		{
			ColorRGBA col = new ColorRGBA();
			col.set((Integer)property);
			property = col;
		}
		else if (property instanceof String)
		{
			ColorRGBA col = new ColorRGBA();
			col.set((String)property);
			property = col;
		}
		else if (property instanceof float[])
		{
			float[] f = (float[])property;
			if (f.length == 3)
			{
				ColorRGBA col = new ColorRGBA();
				col.setColor(f[0], f[1], f[2]);
				property = col;
			}
			else if (f.length == 4)
			{
				ColorRGBA col = new ColorRGBA();
				col.setColor(f[0], f[1], f[2], f[3]);
				property = col;
			}
		}
		return super.set(property);
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		get().writeToNBT(nbt, nbtKey);
	}
}