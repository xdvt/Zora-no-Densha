package zoranodensha.api.vehicles.part.property;

/**
 * <h1>Tickable Property</h1>
 * <hr>
 * 
 * <p>
 * Part {@link zoranodensha.api.vehicles.part.property.IPartProperty properties} which require update ticks.<br>
 * A property's update method is always called <b>after</b> the parent part's update method, if applicable.
 * </p>
 * 
 * @see zoranodensha.api.vehicles.part.property.IPartProperty IPartProperty
 * 
 * @author Leshuwa Kaiheiwa
 */
public interface ITickableProperty<TYPE> extends IPartProperty<TYPE>
{
	/**
	 * Called to update this property.
	 */
	void onUpdate();
}