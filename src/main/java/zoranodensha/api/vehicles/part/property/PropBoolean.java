package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding boolean values.
 */
public class PropBoolean extends APartProperty<Boolean> implements IXMLProperty<Boolean>
{
	public PropBoolean(VehParBase parent, String name)
	{
		this(parent, false, name);
	}

	public PropBoolean(VehParBase parent, boolean val, String name)
	{
		super(parent, Boolean.class, name);
		set(val);
	}

	@Override
	public PropBoolean copy(VehParBase parent)
	{
		return new PropBoolean(parent, get(), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		if (Boolean.TRUE.toString().equalsIgnoreCase(s))
		{
			return set(true);
		}
		else if (Boolean.FALSE.toString().equalsIgnoreCase(s))
		{
			return set(false);
		}
		return false;
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			set(nbt.getBoolean(nbtKey));
			return true;
		}
		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		nbt.setBoolean(nbtKey, get());
	}
}