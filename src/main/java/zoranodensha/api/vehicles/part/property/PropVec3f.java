package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding arrays of properties.<br>
 * <br>
 * These properties require an array to be specified in the constructor.
 */
public class PropVec3f extends APartProperty<float[]> implements IXMLProperty<float[]>
{
	/**
	 * Property holding a triplet of floats, initialised by default to {@code 0.0F} each.
	 */
	public PropVec3f(VehParBase parent, String name)
	{
		this(parent, 0, 0, 0, name);
	}

	/**
	 * Property holding a given <b>triplet</b> of floats.
	 */
	public PropVec3f(VehParBase parent, float x, float y, float z, String name)
	{
		super(parent, float[].class, name);
		set(new float[] { x, y, z });
	}


	@Override
	public PropVec3f copy(VehParBase copyParent)
	{
		PropVec3f prop = new PropVec3f(copyParent, getName());
		if (get() != null)
		{
			prop.set(new float[] { get()[0], get()[1], get()[2] });
		}

		return prop;
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		String[] s_arr = s.split(",");
		if (s_arr.length == 3)
		{
			float[] arr = new float[3];
			for (int i = 0; i < arr.length; ++i)
			{
				try
				{
					arr[i] = Float.parseFloat(s_arr[i]);
				}
				catch (NumberFormatException e)
				{
					return false;
				}
			}
			return set(arr);
		}
		return false;
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey + "_x"))
		{
			set(new float[] { nbt.getFloat(nbtKey + "_x"), nbt.getFloat(nbtKey + "_y"), nbt.getFloat(nbtKey + "_z") });
			return true;
		}
		return false;
	}

	@Override
	public boolean set(Object property)
	{
		if (property instanceof float[])
		{
			float[] oldVal = get();
			if (oldVal == null)
			{
				return super.set(property);
			}

			float[] newVal = (float[])property;
			boolean sync = false;

			if (oldVal.length == newVal.length)
			{
				for (int i = 0; i < newVal.length; ++i)
				{
					if (Float.floatToIntBits(newVal[i]) != Float.floatToIntBits(oldVal[i]))
					{
						sync = true;
						break;
					}
				}
			}

			if (super.set(property))
			{
				setHasChanged(sync);
				return true;
			}
		}
		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		float[] f = get();
		nbt.setFloat(nbtKey + "_x", f[0]);
		nbt.setFloat(nbtKey + "_y", f[1]);
		nbt.setFloat(nbtKey + "_z", f[2]);
	}
}