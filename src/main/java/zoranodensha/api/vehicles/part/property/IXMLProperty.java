package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import zoranodensha.api.vehicles.part.VehParBase;



/**
 * <h1>XML Property</h1>
 * <hr>
 * 
 * <p>
 * Properties implementing this interface may be used in and parsed from XML files.
 * </p>
 * 
 * <p>
 * Note:<br>
 * Just because a property doesn't implement this interface it doesn't mean it will not be present in a vehicle part type.
 * I.e. if a vehicle part type contained an {@link zoranodensha.api.vehicles.part.property.PropInventory inventory},
 * the inventory type couldn't be changed from within the XML file. <br>
 * However, it is still present in the parsed vehicle part type.
 * </p>
 * 
 * @see zoranodensha.api.vehicles.part.property.IPartProperty IPartProperty
 * 
 * @author Leshuwa Kaiheiwa
 */
public interface IXMLProperty<TYPE> extends IPartProperty<TYPE>
{
	/**
	 * Parse and set a property value from the given String in this property.
	 * 
	 * @param s - String representation of the property.
	 * @param zipFile - {@link java.util.zip.ZipFile ZIP-file} from which this property is being parsed. Can be used for resource location references.
	 * @return {@code true} if the value has been successfully parsed <b>and</b> set.
	 */
	boolean parse(String s, ZipFile zipFile);

	/**
	 * Create and return a deep copy of this property.
	 * 
	 * @param copyParent - The copy's parent {@link zoranodensha.api.vehicles.part.VehParBase vehicle part}.
	 */
	IXMLProperty<TYPE> copy(VehParBase copyParent);
}