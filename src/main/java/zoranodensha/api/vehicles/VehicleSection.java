package zoranodensha.api.vehicles;

import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.annotation.Nullable;
import javax.annotation.Nonnull;

import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.boundingBox.IBoundingBoxProvider;
import zoranodensha.api.vehicles.boundingBox.SectionBoundingBox;
import zoranodensha.api.vehicles.handlers.MovementHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.api.vehicles.util.PositionStackEntry;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * <h1>Vehicle Section</h1>
 * <hr>
 * 
 * <p>
 * Vehicle sections are groups of {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts}
 * which share rotation and position data in the world.
 * </p>
 * 
 * <p>
 * Every {@link zoranodensha.api.vehicles.VehicleData vehicle} can be split into one or more vehicle sections.
 * Therefore, every {@link zoranodensha.api.vehicles.Train train} contains at least as many sections as vehicles.
 * Sections are created from each vehicle separately, and divide a train between {@link zoranodensha.api.vehicles.part.type.PartTypeBogie bogies}
 * or pairs of couplings.
 * </p>
 * 
 * <p>
 * In order for a bogie to be incorporated into sections, {@link zoranodensha.api.vehicles.part.type.PartTypeBogie#getIsDummy() getIsDummy()}
 * must return {@code false}. Either bogie may be shared with up to one more section of the same vehicle.
 * </p>
 * 
 * <p>
 * Implements {@link java.lang.Iterable} for iteration over the parent vehicle's bogies.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class VehicleSection implements IBoundingBoxProvider, Iterable<PartTypeBogie>
{
	/*
	 * Final fields
	 */

	/** {@link zoranodensha.api.vehicles.VehicleData vehicle} which this section belongs to. */
	public final VehicleData vehicle;
	/** Front {@link zoranodensha.api.vehicles.part.type.PartTypeBogie bogie} of this section. If a section contains exactly one bogie, it will be this one. */
	public final PartTypeBogie bogieF;
	/** Back {@link zoranodensha.api.vehicles.part.type.PartTypeBogie bogie} of this section, might be {@code null}. */
	public final PartTypeBogie bogieB;
	/** This section's {@link zoranodensha.api.vehicles.part.util.OrderedPartsList parts list}. This list does <b>not</b> contain the section's bogie(s). */
	public final OrderedPartsList parts;
	/** {@link zoranodensha.api.vehicles.boundingBox.SectionBoundingBox Bounding box} of this vehicle section. */
	public final SectionBoundingBox boundingBox;

	/*
	 * Fields populated by the section division handler, after this section was initialised.
	 */

	/** Section in front of (i.e. whose center has a <b>higher</b> value on local X axis) this one, might be {@code null}. */
	public VehicleSection sectionF = null;
	/** Section behind (i.e. whose center has a <b>lower</b> value on local X axis) this one, might be {@code null}. */
	public VehicleSection sectionB;

	/*
	 * Position data.
	 */

	/** X-position. */
	private double x;
	/** Y-position. */
	private double y;
	/** Z-position. */
	private double z;

	/** Previous X-position. */
	private double prevX;
	/** Previous Y-position. */
	private double prevY;
	/** Previous Z-position. */
	private double prevZ;

	/*
	 * Rotation data.
	 */

	/** X-rotation (roll). */
	private float roll;
	/** Y-rotation (yaw). */
	private float yaw;
	/** Z-rotation (pitch). */
	private float pitch;

	/** Previous X-rotation (roll). */
	private float prevRoll;
	/** Previous Y-rotation (yaw). */
	private float prevYaw;
	/** Previous Z-rotation (pitch). */
	private float prevPitch;

	/*
	 * Local center vector.
	 */

	/** Local center X-position. */
	public final float centerX;
	/** Local center Y-position. */
	public final float centerY;
	/** Local center Z-position. */
	public final float centerZ;



	/**
	 * Create a new vehicle section with given data.
	 *
	 * @param vehicle - The {@link zoranodensha.api.vehicles.VehicleData vehicle} this section belongs to.
	 * @param parts - List of {@link zoranodensha.api.vehicles.part.util.OrderedPartsList parts} which belong to this section.
	 * @param bogieF - Front bogie, see {@link #bogieF} for more info.
	 * @param bogieB - Back bogie, see {@link #bogieB} for more info. May be {@code null}.
	 * 
	 * @throws NullPointerException If the front bogie is {@code null}.
	 * @throws IllegalArgumentException If either non-{@code null} bogie is a {@link zoranodensha.api.vehicles.part.type.PartTypeBogie#getIsDummy() dummy} bogie.
	 */
	public VehicleSection(VehicleData vehicle, OrderedPartsList parts, PartTypeBogie bogieF, @Nullable PartTypeBogie bogieB)
	{
		/*
		 * Verify arguments.
		 */
		if (bogieF == null)
		{
			throw new NullPointerException("Front bogie was null!");
		}
		else if (bogieF.getIsDummy())
		{
			throw new IllegalArgumentException("Front bogie was a dummy!");
		}
		else if (bogieB != null && bogieB.getIsDummy())
		{
			throw new IllegalArgumentException("Back bogie was a dummy!");
		}

		/*
		 * Assign fields.
		 */
		this.vehicle = vehicle;
		this.parts = parts;
		this.bogieF = bogieF;
		this.bogieB = bogieB;
		this.boundingBox = new SectionBoundingBox(this);

		/*
		 * Determine local setSpawnDataFromBogies from local bogie offsets.
		 */
		float[] posFront = bogieF.getParent().getOffset();
		if (bogieB != null)
		{
			float[] posBack = bogieB.getParent().getOffset();
			centerX = (posFront[0] + posBack[0]) * 0.5F;
			centerY = (posFront[1] + posBack[1]) * 0.5F;
			centerZ = (posFront[2] + posBack[2]) * 0.5F;
		}
		else
		{
			centerX = posFront[0];
			centerY = posFront[1];
			centerZ = posFront[2];
		}

		/*
		 * Update this section's world position and rotation from its bogie(s).
		 */
		if (bogieB != null)
		{
			setSpawnDataFromBogies();
		}
		else
		{
			setSpawnData(bogieF.getPosX(), bogieF.getPosY(), bogieF.getPosZ(), bogieF.getRotationRoll(), bogieF.getRotationYaw(), bogieF.getRotationPitch());
		}

		prevX = x;
		prevY = y;
		prevZ = z;

		/*
		 * Notify bogies and parts that they were added to a section.
		 */
		bogieF.onAddedToSection(this);

		if (bogieB != null)
		{
			bogieB.onAddedToSection(this);
		}

		for (VehParBase part : parts)
		{
			part.onAddedToSection(this);
		}
	}


	/**
	 * Returns the bounding box of this vehicle section.
	 * 
	 * @return This section's {@link zoranodensha.api.vehicles.boundingBox.SectionBoundingBox bounding box}.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public SectionBoundingBox getBoundingBox()
	{
		return boundingBox;
	}

	/**
	 * Returns the last section whose {@link #sectionB} field is not {@code null}.
	 * 
	 * @return Back-most section connected to this one.
	 */
	public VehicleSection getFinalBack()
	{
		return (sectionB != null) ? sectionB.getFinalBack() : this;
	}

	/**
	 * Returns the last section whose {@link #sectionF} field is not {@code null}.
	 * 
	 * @return Front-most section connected to this one.
	 */
	public VehicleSection getFinalFront()
	{
		return (sectionF != null) ? sectionF.getFinalFront() : this;
	}

	/**
	 * Returns the in-world X-position.
	 */
	public double getPosX()
	{
		return x;
	}

	/**
	 * Returns the in-world Y-position.
	 */
	public double getPosY()
	{
		return y;
	}

	/**
	 * Returns the in-world Z-position.
	 */
	public double getPosZ()
	{
		return z;
	}

	/**
	 * Returns the previous in-world X-position.
	 */
	public double getPrevPosX()
	{
		return prevX;
	}

	/**
	 * Returns the previous in-world Y-position.
	 */
	public double getPrevPosY()
	{
		return prevY;
	}

	/**
	 * Returns the previous in-world Z-position.
	 */
	public double getPrevPosZ()
	{
		return prevZ;
	}

	/**
	 * Returns the previous pitch rotation.
	 */
	public float getPrevRotationPitch()
	{
		return prevPitch;
	}

	/**
	 * Returns the previous roll rotation.
	 */
	public float getPrevRotationRoll()
	{
		return prevRoll;
	}

	/**
	 * Returns the previous yaw rotation.
	 */
	public float getPrevRotationYaw()
	{
		return prevYaw;
	}

	/**
	 * Returns the pitch rotation.
	 */
	public float getRotationPitch()
	{
		return pitch;
	}

	/**
	 * Returns the roll rotation.
	 */
	public float getRotationRoll()
	{
		return roll;
	}

	/**
	 * Returns the yaw rotation.
	 */
	public float getRotationYaw()
	{
		return yaw;
	}

	/**
	 * Returns the vehicle this section is part of.
	 * 
	 * @return This section's parent {@link zoranodensha.api.vehicles.VehicleData vehicle}.
	 */
	public VehicleData getVehicle()
	{
		return vehicle;
	}

	/**
	 * Returns the train this section is part of.
	 * 
	 * @return {@link zoranodensha.api.vehicles.Train Train} of this section. May be {@code null}.
	 */
	public Train getTrain()
	{
		return getVehicle().getTrain();
	}

	@Nonnull
	@Override
	public Iterator<PartTypeBogie> iterator()
	{
		/*
		 * Iterates over all bogies in the parent vehicle.
		 */
		return new Iterator<PartTypeBogie>()
		{
			private VehicleSection currSection = VehicleSection.this;
			private PartTypeBogie currBogie = currSection.bogieF;



			@Override
			public boolean hasNext()
			{
				return (currBogie != null);
			}

			@Override
			public PartTypeBogie next()
			{
				if (!hasNext())
				{
					throw new NoSuchElementException();
				}

				PartTypeBogie ret = currBogie;
				{
					if (currSection.sectionB != null)
					{
						/* If there's another section to come, select the following section's front bogie. */
						currSection = currSection.sectionB;
						currBogie = currSection.bogieF;
					}
					else if (currBogie == currSection.bogieF)
					{
						/* Otherwise return the last section's back bogie. */
						currBogie = currSection.bogieB;
					}
					else
					{
						/* End of iteration. */
						currBogie = null;
					}
				}
				return ret;
			}

			@Override
			public void remove()
			{
				throw new UnsupportedOperationException("remove");
			}
		};
	}

	/**
	 * Assigns the given section as {@link #sectionB back} of this section,
	 * and assigns this section as the given section's front section.
	 * 
	 * @param section - New back of this section. May be {@code null}.
	 */
	public void setBackSection(@Nullable VehicleSection section)
	{
		sectionB = section;
		if (sectionB != null)
		{
			sectionB.sectionF = this;
		}
	}

	/**
	 * Set this section's in-world position.<br>
	 * <br>
	 * Updates part positions and previous position fields.
	 *
	 * @param x - New X-position in the world.
	 * @param y - New Y-position in the world.
	 * @param z - New Z-position in the world.
	 * 
	 * @see #updatePartsPos()
	 */
	public void setPosition(double x, double y, double z)
	{
		prevX = this.x;
		prevY = this.y;
		prevZ = this.z;

		this.x = x;
		this.y = y;
		this.z = z;

		updatePartsPos();
	}

	/**
	 * Set this section's rotation. Clamps the given angles to previous angles.<br>
	 * <br>
	 * Does <b>not</b> update part positions.
	 *
	 * @param roll - Rotation about X-axis.
	 * @param yaw - Rotation about Y-axis.
	 * @param pitch - Rotation about Z-axis.
	 */
	public void setRotation(float roll, float yaw, float pitch)
	{
		prevRoll = this.roll % 360.0F;
		prevYaw = this.yaw % 360.0F;
		prevPitch = this.pitch % 360.0F;

		this.roll = VehicleHelper.normalise(roll, prevRoll);
		this.yaw = VehicleHelper.normalise(yaw, prevYaw);
		this.pitch = VehicleHelper.normalise(pitch, prevPitch);

		/*
		 * This prevents an error where current and previous rotation values get incorrectly interpolated while rendering.
		 */
		if (Math.abs((this.roll - prevRoll) * 0.5F) >= 10.0F)
		{
			if (prevRoll < this.roll)
				prevRoll += 360.0F;
			else
				this.roll += 360.0F;
		}

		if (Math.abs((this.yaw - prevYaw) * 0.5F) >= 10.0F)
		{
			if (prevYaw < this.yaw)
				prevYaw += 360.0F;
			else
				this.yaw += 360.0F;
		}

		if (Math.abs((this.pitch - prevPitch) * 0.5F) >= 10.0F)
		{
			if (prevPitch < this.pitch)
				prevPitch += 360.0F;
			else
				this.pitch += 360.0F;
		}
	}

	/**
	 * Overrides position and rotation data with given argument data, respectively.<br>
	 * This is used to prepare vehicle spawns.
	 */
	public void setSpawnData(double x, double y, double z, float roll, float yaw, float pitch)
	{
		prevRoll = this.roll;
		prevYaw = this.yaw;
		prevPitch = this.pitch;

		this.roll = VehicleHelper.normalise(roll);
		this.yaw = VehicleHelper.normalise(yaw);
		this.pitch = VehicleHelper.normalise(pitch);

		setPosition(x, y, z);
	}

	/**
	 * Calculates this section's spawn data (i.e. initial position and rotation) from its bogies' positions.
	 */
	public void setSpawnDataFromBogies()
	{
		PositionStackEntry pse = MovementHandler.INSTANCE.interpolateCenter(bogieF.getPosX(), bogieF.getPosY() - bogieF.getOffsetY(), bogieF.getPosZ(), bogieB.getPosX(), bogieB.getPosY() - bogieB.getOffsetY(), bogieB.getPosZ());
		setSpawnData(pse.x, pse.y + centerY, pse.z, pse.roll, pse.yaw, pse.pitch);
	}

	@Override
	public String toString()
	{
		/* Common data */
		String clazz = getClass().getSimpleName();
		String uvid = getVehicle().getUVID().toString();
		String train = (getTrain() != null) ? String.valueOf(getTrain().getEntityId()) : "~NULL~";

		/* Specific data */
		String bogieF = String.valueOf(this.bogieF.getParent().getID());
		String bogieB = (this.bogieB != null) ? String.valueOf(this.bogieB.getParent().getID()) : "~NULL~";

		return String.format("%s[UVID=%s | Train=%s | F-Bogie=%s B-Bogie=%s | %.2fx %.2fy %.2fz | %.2fcx %.2fcy %.2fcz | Parts=%d]", clazz, uvid, train, bogieF, bogieB, x, y, z, centerX, centerY, centerZ, parts.size());
	}

	/**
	 * Updates in-world placement of this section's {@link #parts vehicle parts}.
	 */
	public void updatePartsPos()
	{
		/* Prepare update; calculate rotation angles. */
		float rollR = -roll * VehicleHelper.RAD_FACTOR;
		float yawR = -yaw * VehicleHelper.RAD_FACTOR;
		float pitchR = -pitch * VehicleHelper.RAD_FACTOR;
		float[] off;
		Vec3 pos = Vec3.createVectorHelper(0, 0, 0);

		/* Assign new position to all parts. */
		for (VehParBase part : parts)
		{
			/* Local part position. */
			off = part.getOffset();
			pos.xCoord = off[0] - centerX;
			pos.yCoord = off[1] - centerY;
			pos.zCoord = off[2] - centerZ;

			/* Rotate position vector. */
			pos.rotateAroundZ(pitchR);
			pos.rotateAroundY(yawR);
			pos.rotateAroundX(rollR);

			/* Apply part position relative to section position. */
			part.setPosition(pos.xCoord + getPosX(), pos.yCoord + getPosY(), pos.zCoord + getPosZ());
		}
	}
}