package zoranodensha.api.vehicles.util;

import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRailBase;
import net.minecraft.world.World;
import zoranodensha.api.vehicles.handlers.MovementHandler;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;



/**
 * Wrapper class for track objects, holding a track's world position and the track's block type.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class TrackObj
{
	public final World world;
	public final Block block;
	public final int x;
	public final int y;
	public final int z;



	public TrackObj(World world, Block block, int x, int y, int z)
	{
		this.world = world;
		this.block = block;
		this.x = x;
		this.y = y;
		this.z = z;
	}


	/**vehiclePart
	 * Called to determine the friction of the given bogie on this track.
	 * 
	 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} which friction is to be determined.
	 * @return Bogie friction (in kiloNewton - kN) on this track. Positive values will slow bogies down, while negative values will speed bogies up.
	 */
	public final float bogieFriction(PartTypeBogie bogie)
	{
		return (block instanceof ITrack) ? ((ITrack)block).bogieFriction(this, bogie) : 0.0F;
	}

	/**
	 * Called to clamp the given position to this track object, optionally taking the given bogie into account.
	 * 
	 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} to clamp.May be {@code null}.
	 * @param pse - New {@link zoranodensha.api.vehicles.util.PositionStackEntry position}. Modify this in order to determine the clamped position.
	 * @return {@code true} if the bogie was clamped to a track.
	 */
	public final boolean clampToTrack(@Nullable PartTypeBogie bogie, PositionStackEntry pse)
	{
		/*
		 * Implementation for vanilla tracks.
		 */
		if (block instanceof BlockRailBase)
		{
			switch (((BlockRailBase)block).getBasicRailMetadata(world, null, x, y, z))
			{
				case 0:
					pse.setPosition(x + 0.5F, y, pse.z);
					pse.setRotation(0F, 90F, 0F);

					if (bogie != null)
					{
						pse.y += bogie.getParent().getOffset()[1] + 0.0625F;
					}
					return true;

				case 1:
					pse.setPosition(pse.x, y, z + 0.5F);
					pse.setRotation(0F, 0F, 0F);

					if (bogie != null)
					{
						pse.y += bogie.getParent().getOffset()[1] + 0.0625F;
					}
					return true;

				default:
					return false;
			}
		}

		/*
		 * All other implementing track types.
		 */
		return (block instanceof ITrack) && ((ITrack)block).clampToTrack(this, bogie, pse);
	}

	/**
	 * Checks whether the block at specified position is a valid track block, as defined in
	 * {@link ITrack#isTrack(TrackObj, zoranodensha.api.vehicles.part.type.PartTypeBogie) isTrack()}.<br>
	 * If so, returns a new {@link TrackObj} instance.
	 * 
	 * @param world - {@link net.minecraft.world.World World} object of the block.
	 * @param x - Block's X-coordinate.
	 * @param y - Block's Y-coordinate.
	 * @param z - Block's Z-coordinate.
	 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} asking for validation, may be {@code null}.
	 * @return A new {@link TrackObj} instance, or {@code null} if the block wasn't a valid track.
	 */
	public static final TrackObj isTrack(World world, int x, int y, int z, @Nullable PartTypeBogie bogie)
	{
		/* Get block at given position. */
		Block block = world.getBlock(x, y, z);

		/*
		 * Implementation for vanilla tracks.
		 */
		if (BlockRailBase.func_150051_a(block))
		{
			/* If the track isn't straight, it may not be used. */
			int meta = ((BlockRailBase)block).getBasicRailMetadata(world, null, x, y, z);
			if (meta > 1)
			{
				return null;
			}

			/* If there is a bogie, ensure bogie rotation matches metadata. */
			if (bogie != null)
			{
				float trackYaw = (meta == 1) ? 0.0F : 90.0F;
				if (!MovementHandler.INSTANCE.getIsRotationMatching(bogie.getRotationYaw(), trackYaw, 20.0F))
				{
					return null;
				}
			}

			/* Otherwise, it's fine. */
			return new TrackObj(world, block, x, y, z);
		}

		/*
		 * All other implementing track types.
		 */
		if (block instanceof ITrack)
		{
			TrackObj track = new TrackObj(world, block, x, y, z);
			if (((ITrack)block).isTrack(track, bogie))
			{
				return track;
			}
		}

		return null;
	}



	/**
	 * Interface implemented by {@link net.minecraft.block.Block Blocks} in order to be recognised by
	 * {@link zoranodensha.api.vehicles.part.type.PartTypeBogie bogies}.
	 */
	public interface ITrack
	{
		/**
		 * Called to determine the friction of the given bogie on this track.
		 * 
		 * @param track - {@link zoranodensha.api.vehicles.util.TrackObj Position} of the respective track.
		 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} which friction is to be determined.
		 * @return Bogie friction (in kiloNewton - kN) on this track. Positive values will slow bogies down, while negative values will speed bogies up.
		 */
		float bogieFriction(TrackObj track, PartTypeBogie bogie);

		/**
		 * Called to clamp the given position to this track, optionally taking the given bogie into account.
		 * 
		 * @param track - {@link zoranodensha.api.vehicles.util.TrackObj Position} of the respective track.
		 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} to clamp. May be {@code null}.
		 * @param pse - New {@link zoranodensha.api.vehicles.util.PositionStackEntry position}. Modify this in order to determine the clamped position.
		 * @return {@code true} if the bogie was clamped to a track.
		 */
		boolean clampToTrack(TrackObj track, @Nullable PartTypeBogie bogie, PositionStackEntry pse);

		/**
		 * Determine whether there is a valid track at the given coordinates
		 * which may be used by the given bogie.<br>
		 * The given bogie, if existent, may be used to e.g. exclude bogies from certain types of track.
		 * 
		 * @param track - {@link zoranodensha.api.vehicles.util.TrackObj Position} of the respective track.
		 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} asking for validation, may be {@code null}.
		 * @return {@code true} if the track at the given position can be used.
		 */
		boolean isTrack(TrackObj track, @Nullable PartTypeBogie bogie);
	}
}