package zoranodensha.api.vehicles.util;

import java.util.Random;

import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;



/**
 * Class for general vehicle-related helper methods.
 */
public final class VehicleHelper
{
	/** Static {@link java.util.Random Random} instance. */
	public static final Random ran = new Random();
	/** Factor to multiply with an angle (in degrees) to convert it to radians. */
	public static final float RAD_FACTOR = (float)(Math.PI / 180.0D);
	/** Factor to multiply with an angle (in radians) to convert it to degrees. */
	public static final float DEG_FACTOR = (float)(180.0D / Math.PI);



	/**
	 * Compute the angle in degrees between the given base vector and the other vector.
	 *
	 * @param base - The base {@link net.minecraft.util.Vec3 vector}. If {@code null}, will set default vector pointing towards positive X; {@code (1.0, 0.0, 0.0)}.
	 * @param other - The other vector.
	 * @return The angle between the given vectors, in degrees ranging from {@code 0.0D} to {@code 180.0D}.
	 */
	public static final double getAngle(Vec3 base, Vec3 other)
	{
		if (other == null)
		{
			return 0.0F;
		}

		double d0;
		if (base == null)
		{
			d0 = other.xCoord / other.lengthVector();
		}
		else
		{
			d0 = (base.dotProduct(other)) / base.lengthVector() * other.lengthVector();
		}

		return Math.acos(MathHelper.clamp_double(d0, -1.0D, 1.0D)) * DEG_FACTOR;
	}

	/**
	 * Returns the depth of intersection for the given bounding boxes' bounds.
	 * 
	 * @param min0 - First {@link net.minecraft.util.AxisAlignedBB bounding box}' lower border.
	 * @param max0 - First bounding box' upper border.
	 * @param min1 - Second bounding box' lower border.
	 * @param max1 - Second bounding box' upper border.
	 * @return Depth of intersection for the given bounds, or {@code NaN} if the bounds don't intersect.
	 */
	public static double getIntersectionDepth(double min0, double max0, double min1, double max1)
	{
		/* Ensure there is collision happening. */
		if (max1 > min0 && min1 < max0)
		{
			double diff = max0 - min1;
			return (Math.abs(diff) < 0.001D) ? 0.0D : diff;
		}

		/* Otherwise return NaN. */
		return Double.NaN;
	}

	/**
	 * Returns true if the given ItemStack is a rail vehicle blueprint.
	 */
	public static final boolean getIsBlueprint(ItemStack itemStack)
	{
		if (itemStack == null || !itemStack.hasTagCompound())
		{
			return true;
		}
		return itemStack.getTagCompound().getBoolean(Train.EDataKey.IS_BLUEPRINT.key);
	}

	/**
	 * Returns the client-sided player entity.
	 */
	@SideOnly(Side.CLIENT)
	public static final EntityPlayer getPlayer_onClient()
	{
		return Minecraft.getMinecraft().thePlayer;
	}

	/**
	 * Returns the server-sided player entity from the given context.
	 */
	public static final EntityPlayer getPlayer_onServer(MessageContext ctx)
	{
		return ctx.getServerHandler().playerEntity;
	}

	/**
	 * Retrieves a player from the given message context.
	 *
	 * @param ctx - The {@link MessageContext context} to retrieve the player from.
	 * @return An {@link net.minecraft.entity.player.EntityPlayer player} that was retrieved.
	 */
	public static final EntityPlayer getPlayerFromContext(MessageContext ctx)
	{
		if (Side.CLIENT.equals(ctx.side))
		{
			return getPlayer_onClient();
		}
		return getPlayer_onServer(ctx);
	}

	/**
	 * Calculates the position of the given AxisAlignedBB and returns it as Vec3.
	 *
	 * @param aabb - The {@link AxisAlignedBB} whose position is to be calculated.
	 * @return The part's position as Vec3.
	 */
	public static final Vec3 getPosition(AxisAlignedBB aabb)
	{
		return Vec3.createVectorHelper((aabb.minX + aabb.maxX) * 0.5D, (aabb.minY + aabb.maxY) * 0.5D, (aabb.minZ + aabb.maxZ) * 0.5D);
	}

	/**
	 * Tries to read the vehicle name from the given train's NBT tag.<br>
	 * <br>
	 * This requires that there is <b>exactly</b> one vehicle contained by the given train's NBT tag.
	 * 
	 * @return The vehicle name, or {@code null} if unsuccessful.
	 */
	public static final String getVehicleName(NBTTagCompound nbt)
	{
		if (nbt.hasKey(Train.EDataKey.VEHICLE_DATA.key))
		{
			NBTTagList tagList = nbt.getTagList(Train.EDataKey.VEHICLE_DATA.key, 10);
			nbt = tagList.getCompoundTagAt(0);

			if (nbt.hasKey(VehicleData.EDataKey.NAME.key))
			{
				return nbt.getString(VehicleData.EDataKey.NAME.key);
			}
		}

		return null;
	}

	/**
	 * Clamps the given argument into the interval {@code [0,360)}.
	 *
	 * @param val - The angle to clamp, in degrees.
	 * @return The argument, clamped between {@code 0.0F} (inclusive) and {@code 360.0F} (exclusive).
	 */
	public static float normalise(float val)
	{
		val %= 360F;
		while (val < 0.0F)
		{
			val += 360F;
		}
		return val;
	}

	/**
	 * Clamps both arguments into the interval {@code [0,360)}, then compares whether the clamped first argument or its inverse is closer to the second clamped argument.
	 *
	 * @param val - Argument to clamp.
	 * @param cmp - Argument to compare to.
	 * @return The clamped first argument.
	 */
	public static float normalise(float val, float cmp)
	{
		/*
		 * Normalise angles, compute inverse angle.
		 */
		cmp = normalise(cmp);
		val = normalise(val);
		final float inv = (val >= 180F) ? (val - 180F) : (val + 180F);

		/*
		 * Calculate each vector. We don't use Minecraft's implementation for performance reasons.
		 */
		float vecCmp_x = MathHelper.cos(cmp * RAD_FACTOR), vecCmp_z = -MathHelper.sin(cmp * RAD_FACTOR);
		float vecVal_x = MathHelper.cos(val * RAD_FACTOR), vecVal_z = -MathHelper.sin(val * RAD_FACTOR);
		float vecInv_x = MathHelper.cos(inv * RAD_FACTOR), vecInv_z = -MathHelper.sin(inv * RAD_FACTOR);

		/*
		 * Get the angle between the respective vector pairs. Since all vectors share a length of 1, simply get the arc cosine of their dot product, clamped between -1 and 1.
		 */
		double cmp_val = Math.acos(MathHelper.clamp_double((vecCmp_x * vecVal_x + vecCmp_z * vecVal_z), -1, 1));
		double cmp_inv = Math.acos(MathHelper.clamp_double((vecCmp_x * vecInv_x + vecCmp_z * vecInv_z), -1, 1));

		/*
		 * Compare the angles (in Radians, between 0 to PI).
		 */
		if (cmp_inv < cmp_val)
		{
			return inv;
		}
		return val;
	}
}