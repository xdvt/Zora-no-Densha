package zoranodensha.api.vehicles.handlers;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.block.Block;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.boundingBox.MultiBoundingBox;
import zoranodensha.api.vehicles.boundingBox.SectionBoundingBox;
import zoranodensha.api.vehicles.event.living.LivingHitByTrainEvent;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.util.PositionStackEntry;



/**
 * Default collision handler class, called to check for block and entity collision of {@link zoranodensha.api.vehicles.Train trains},
 * as well as to simulate push/pull mechanics between trains.<br>
 * <br>
 * This class handles:<br>
 * > Block and Entity collision checks for single vehicle sections<br>
 * > Push/pull mechanism between trains
 * 
 * @author Leshuwa Kaiheiwa
 */
public class CollisionHandler
{
	/**
	 * A re-used {@link java.util.ArrayList ArrayList}, used by {@link #gravitateBogie(PartTypeBogie, PositionStackEntry, PositionStackEntry) gravitateBogie()} and
	 * {@link #getCollidingBlocks(AxisAlignedBB, Train) getCollidingBlocks()}.
	 */
	private static final ArrayList<AxisAlignedBB> boxesCache = new ArrayList<AxisAlignedBB>();
	/** A re-used {@link java.util.ArrayList ArrayList}, used by {@link #getCollidingEntities(World, AxisAlignedBB, IEntitySelector) getCollidingEntities()}. */
	private static final ArrayList<Entity> entitiesCache = new ArrayList<Entity>();

	/** {@link net.minecraft.command.IEntitySelector Entity selector} to choose anything but {@link zoranodensha.api.vehicles.Train trains}. */
	public static final IEntitySelector selectorNoTrains = new EntitySelectorNoTrains();
	/** {@link net.minecraft.command.IEntitySelector Entity selector} to choose only {@link zoranodensha.api.vehicles.Train trains}. */
	public static final IEntitySelector selectorTrains = new EntitySelectorTrains(null);

	/** "Gravity" for bogies; movement in meters per tick down the Y-axis. */
	public static float BOGIE_GRAVITY = 0.16F;
	/** Coefficient of restitution, used when calculating motion of entities colliding with trains. */
	public static float COEFFICIENT_RESTITUTION = 0.95F;

	/** A reference to the currently set collision handler instance. */
	public static CollisionHandler INSTANCE = new CollisionHandler();


	/**
	 * Notifies all blocks colliding with the given train's bogies.<br>
	 * <br>
	 * This is a replacement call to Minecraft's {@link net.minecraft.block.Block#onEntityCollidedWithBlock(World, int, int, int, Entity)}.
	 * 
	 * @param train - The {@link zoranodensha.api.vehicles.Train train} whose bogies to collide.
	 */
	public void collideBogiesWithBlocks(Train train)
	{
		for (VehicleData vehicle : train)
		{
			for (PartTypeBogie bogie : vehicle.getSection())
			{
				AxisAlignedBB aabb = bogie.getParent().getBoundingBox();
				int minX = MathHelper.floor_double(aabb.minX);
				int minY = MathHelper.floor_double(aabb.minY);
				int minZ = MathHelper.floor_double(aabb.minZ);
				int maxX = MathHelper.floor_double(aabb.maxX + 1.0D);
				int maxY = MathHelper.floor_double(aabb.maxY + 1.0D);
				int maxZ = MathHelper.floor_double(aabb.maxZ + 1.0D);
				World world = train.worldObj;
				Block block;

				for (int x = minX; x < maxX; ++x)
				{
					for (int z = minZ; z < maxZ; ++z)
					{
						if (world.blockExists(x, 64, z))
						{
							for (int y = minY; y < maxY; ++y)
							{
								/* Ensure supplied coordinates are within world bounds. */
								if (x >= -30000000 && x < 30000000 && z >= -30000000 && z < 30000000)
								{
									block = world.getBlock(x, y, z);
									block.onEntityCollidedWithBlock(world, x, y, z, train);
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Run entity collision checks for the given train.<br>
	 * <br>
	 * Since collision between trains is handled during update routines (see {@link #collideWithSolids(Train, double)} collideWithSolids()),
	 * this method ignores other trains.
	 *
	 * @param train - The {@link zoranodensha.api.vehicles.Train train} to check collisions for.
	 */
	public void collideWithEntities(Train train)
	{
		/* Prepare outer train mask for quicker performance. */
		AxisAlignedBB mask = AxisAlignedBB.getBoundingBox(0, 0, 0, 0, 0, 0);
		mask.setBB(train.getBoundingBox());

		/* If there are entities who intersected with the mask.. */
		ArrayList<Entity> collisionCandidates = getCollidingEntities(train.worldObj, mask, selectorNoTrains);
		if (!collisionCandidates.isEmpty())
		{
			/* ..check for each entity.. */
			for (Entity collisionCandidate : collisionCandidates)
			{
				/* ..whether it actually collides with the train.. */
				if (train.getBoundingBox().intersectsWith(collisionCandidate.boundingBox))
				{
					/* ..and if so, apply collision logic. */
					train.applyEntityCollision(collisionCandidate);
				}
			}
		}
	}

	/**
	 * Run collision checks against blocks and other trains for the given train.
	 * 
	 * @param train - The {@link zoranodensha.api.vehicles.Train train} to move.
	 * @param movementDistanceInitial - The distance the given train is supposed to move, in meters.
	 * @return The distance to actually move, modified to match possible obstructions.
	 */
	public double collideWithSolids(Train train, double movementDistanceInitial)
	{
		/* If there's nothing to move, don't bother checking. */
		if (movementDistanceInitial == 0.0)
		{
			return 0.0;
		}

		/* Entity selector that selects any train except the given one. */
		EntitySelectorTrains selectorTrain = new EntitySelectorTrains(train);

		/* Distance (in meters) each taken step crosses. */
		final double STEP_DISTANCE = 0.5;

		/* Absolute value of the initial movement distance. */
		double distAbs = Math.abs(movementDistanceInitial);

		/* Number of full movement steps. */
		int movementFullSteps = (int)(distAbs / STEP_DISTANCE);

		/*
		 * Go through all sections and do a "rough" approximation.
		 * Select the section who collided the earliest with blocks or trains.
		 */
		double movementDistance = Double.NaN;
		{
			for (VehicleData vehicle : train)
			{
				/* For each section in this train.. */
				for (VehicleSection section : vehicle)
				{
					/* ..get its bounding box and expand its wrapper box.. */
					SectionBoundingBox sectionBB = section.getBoundingBox();
					{
						sectionBB.minX -= STEP_DISTANCE;
						sectionBB.minY -= STEP_DISTANCE;
						sectionBB.minZ -= STEP_DISTANCE;
						sectionBB.maxX += STEP_DISTANCE;
						sectionBB.maxY += STEP_DISTANCE;
						sectionBB.maxZ += STEP_DISTANCE;
					}

					/* ..get the direction vector of its front bogie.. */
					Vec3 vecDir = section.bogieF.getDirection();

					/* ..try to move the section as far as possible.. */
					double movementDistanceSection = 0.0;
					{
						for (int i = movementFullSteps; i >= 0; --i)
						{
							// Determine movement distance in this step.
							double movementDistanceStep; // FIXME @Leshuwa - Reversing trains don't seem to collide with walls until hitting the front vehicle..?
							{
								if (i == 0)
								{
									movementDistanceStep = Math.copySign(distAbs - (movementFullSteps * STEP_DISTANCE), movementDistanceInitial);
								}
								else
								{
									movementDistanceStep = Math.copySign(STEP_DISTANCE, movementDistanceInitial);
								}
							}

							// Move bounding box by distance.
							// @formatter:off
							sectionBB.offset(vecDir.xCoord * movementDistanceStep,
							                 vecDir.yCoord * movementDistanceStep,
							                 vecDir.zCoord * movementDistanceStep);
							// @formatter:on

							// If this section collided with blocks or trains, check whether it has moved the least distance so far and break.
							if (!getCollidingBlocks(section.getBoundingBox(), train).isEmpty() || !getCollidingEntities(train.worldObj, section.getBoundingBox(), selectorTrain).isEmpty())
							{
								break;
							}

							// If this section collided neither with blocks nor trains, increase the distance moved by this section.
							movementDistanceSection += movementDistanceStep;
						}
					}

					/* ..and update the train's moved distance with the distance moved by this section. */
					if (Double.isNaN(movementDistance) || movementDistanceSection < movementDistance)
					{
						movementDistance = movementDistanceSection;
					}
				}
			}
		}
		return (Double.isNaN(movementDistance) ? 0.0 : movementDistance);
	}

	/**
	 * Determine whether the given bogie may gravitate towards the ground.<br>
	 * More precisely, iterates through all sections of the bogie's parent train and,
	 * if the bogie belongs to any such section, checks whether they already collide with any blocks.
	 * If neither section collides, the bogie may gravitate downwards.
	 * 
	 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} for which to run checks.
	 * @return {@code true} if the given bogie may fall down.
	 */
	public double getBogieGravitationDistance(PartTypeBogie bogie)
	{
		/* The distance a bogie falls per tick. */
		double gravity = BOGIE_GRAVITY;

		/*
		 * If parent train ignores collision, fall happily into nowhere.
		 */
		Train train = bogie.getTrain();
		if (train.noClip)
		{
			return gravity;
		}

		/*
		 * Otherwise try to determine how far the bogie may gravitate.
		 */
		for (VehicleSection section : bogie.getVehicle())
		{
			/* Select whichever section our bogie belongs to, irrespective of its position therein. */
			if (section.bogieB != bogie && section.bogieF != bogie)
			{
				continue;
			}
			
			/* Collision mask used to find collision candidates. */
			MultiBoundingBox collisionMask = section.getBoundingBox().addCoord(0.0, -BOGIE_GRAVITY, 0.0);

			/* Check whether any part collides with blocks. */
			ArrayList<AxisAlignedBB> collidingBlocks = getCollidingBlocks(collisionMask, train);
			if (!collidingBlocks.isEmpty())
			{
				final double minY = bogie.getParent().getBoundingBox().minY;
				for (AxisAlignedBB aabb : collidingBlocks)
				{
					/* If the collision box's top is above the bogie collision's bottom, we assume collision. */
					if (aabb.maxY >= minY)
					{
						return 0.0;
					}

					/* Otherwise get the minimum between the fall distance and the distance to the bounding box. */
					gravity = Math.min(gravity, minY - aabb.maxY);
				}
			}
			
			/* Also check for nearby trains. */
			ArrayList<Entity> collisionCandidates = getCollidingEntities(train.worldObj, collisionMask, new EntitySelectorTrains(train));
			if (!collisionCandidates.isEmpty())
			{
				for (Entity e : collisionCandidates)
				{
					if (e.getBoundingBox().intersectsWith(bogie.getParent().getBoundingBox()))
					{
						return 0.0;
					}
				}
			}
		}

		return gravity;
	}

	/**
	 * Determines all bounding boxes of non-track blocks which intersect with the given bounding box.
	 * 
	 * @param mask - Selection {@link net.minecraft.util.AxisAlignedBB box}.
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} reference, can be used by tracks to cancel collision with trains.
	 * @return All intersecting bounding boxes of blocks which are <b>not</b> subclass of {@link zoranodensha.api.vehicles.util.TrackObj.ITrack ITrack}.
	 * 
	 * @see net.minecraft.world.World#func_147461_a(AxisAlignedBB)
	 */
	public ArrayList<AxisAlignedBB> getCollidingBlocks(AxisAlignedBB mask, Train train)
	{
		boxesCache.clear();
		{
			int minX = MathHelper.floor_double(mask.minX);
			int minY = MathHelper.floor_double(mask.minY);
			int minZ = MathHelper.floor_double(mask.minZ);
			int maxX = MathHelper.floor_double(mask.maxX + 1.0D);
			int maxY = MathHelper.floor_double(mask.maxY + 1.0D);
			int maxZ = MathHelper.floor_double(mask.maxZ + 1.0D);
			World world = train.worldObj;
			Block block;

			for (int x = minX; x < maxX; ++x)
			{
				for (int z = minZ; z < maxZ; ++z)
				{
					if (world.blockExists(x, 64, z))
					{
						for (int y = minY; y < maxY; ++y)
						{
							/* Ensure supplied coordinates are within world bounds. */
							if (x >= -30000000 && x < 30000000 && z >= -30000000 && z < 30000000)
							{
								block = world.getBlock(x, y, z);
							}
							else
							{
								continue;
							}

							/* Add the block's collision boxes to the list. */
							block.addCollisionBoxesToList(world, x, y, z, mask, boxesCache, train);
						}
					}
				}
			}
		}
		return boxesCache;
	}

	/**
	 * Computes a list of all entities from the given world which collide with the given mask.<br>
	 * Optionally, the result may be filtered with the given entity selector.
	 * 
	 * @param world - {@link net.minecraft.world.World World} object of the entities to filter.
	 * @param mask - {@link net.minecraft.util.AxisAlignedBB Bounding box} that entities need to intersect with.
	 * @param selector - An {@link net.minecraft.command.IEntitySelector entity selector} to filter the resulting entities. May be {@code null}.
	 * @return The list of (filtered) entities. The returned list will be re-used by the next call.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<Entity> getCollidingEntities(World world, AxisAlignedBB mask, @Nullable IEntitySelector selector)
	{
		entitiesCache.clear();

		final int chunkMinX = MathHelper.floor_double((mask.minX - World.MAX_ENTITY_RADIUS) * 0.0625D);
		final int chunkMinY = MathHelper.floor_double((mask.minY - World.MAX_ENTITY_RADIUS) * 0.0625D);
		final int chunkMinZ = MathHelper.floor_double((mask.minZ - World.MAX_ENTITY_RADIUS) * 0.0625D);
		final int chunkMaxX = MathHelper.floor_double((mask.maxX + World.MAX_ENTITY_RADIUS) * 0.0625D);
		final int chunkMaxY = MathHelper.floor_double((mask.maxY + World.MAX_ENTITY_RADIUS) * 0.0625D);
		final int chunkMaxZ = MathHelper.floor_double((mask.maxZ + World.MAX_ENTITY_RADIUS) * 0.0625D);
		final IChunkProvider chunkProvider = world.getChunkProvider();

		AxisAlignedBB aabb;
		List<Entity> list;
		Entity entity;
		Chunk chunk;
		int minY;
		int maxY;

		for (int chunkX = chunkMinX; chunkX <= chunkMaxX; ++chunkX)
		{
			for (int chunkZ = chunkMinZ; chunkZ <= chunkMaxZ; ++chunkZ)
			{
				if (chunkProvider.chunkExists(chunkX, chunkZ))
				{
					chunk = chunkProvider.provideChunk(chunkX, chunkZ);
					minY = MathHelper.clamp_int(chunkMinY, 0, chunk.entityLists.length - 1);
					maxY = MathHelper.clamp_int(chunkMaxY, 0, chunk.entityLists.length - 1);

					for (int chunkY = minY; chunkY <= maxY; ++chunkY)
					{
						list = chunk.entityLists[chunkY];
						if (!list.isEmpty())
						{
							for (int i = list.size() - 1; i >= 0; --i)
							{
								entity = list.get(i);
								if (entity != null && (selector == null || selector.isEntityApplicable(entity)))
								{
									aabb = entity.getBoundingBox();
									if (aabb == null)
									{
										aabb = entity.boundingBox;
									}

									if (mask.intersectsWith(aabb))
									{
										entitiesCache.add(entity);
									}
								}
							}
						}
					}
				}
			}
		}

		return entitiesCache;
	}

	/**
	 * Calculates the mass of the Entity, assuming that<br>
	 * a) the Entity fills out its bounding box, and<br>
	 * b) the Entity's density is roughly that of flesh.
	 */
	public float getEntityMass(Entity entity)
	{
		/* We assume the Entity's density is roughly that of flesh. */
		return (entity.width * entity.width * entity.height) * (1050.0F / 9.81F);
	}

	/**
	 * Determines whether the given bogie is on ground.
	 * 
	 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} to check for.
	 * @return {@code true} of the bogie is on ground, {@code false} if not.
	 */
	public boolean getIsBogieOnGround(PartTypeBogie bogie)
	{
		return !bogie.getTrain().worldObj.func_147461_a(bogie.getParent().getBoundingBox().expand(0.0, 0.125D, 0.0D)).isEmpty();
	}

	/**
	 * Exerts gravity on the given bogie and runs vertical block collision.
	 *
	 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} to check for.
	 * @param pse - Next bogie {@link zoranodensha.api.vehicles.util.PositionStackEntry position}
	 * @param pse_prev - Bogie position during the previous tick.
	 */
	public void gravitateBogie(PartTypeBogie bogie, PositionStackEntry pse, PositionStackEntry pse_prev)
	{
		/* Let the bogie fall if neither section collides with solid blocks. */
		pse.y -= getBogieGravitationDistance(bogie);

		/*
		 * Abort further collision if parent train ignores bounding boxes.
		 */
		Train parent = bogie.getParent().getTrain();
		if (parent.noClip)
		{
			return;
		}

		/* Ensure the chunk at the given block position exists. */
		int x = MathHelper.floor_double(pse.x);
		int z = MathHelper.floor_double(pse.z);
		World world = parent.worldObj;
		if (!world.blockExists(x, 0, z))
		{
			return;
		}

		/* If chunk exists, proceed with determining the y position to begin iteration at. */
		int y, y_min;
		if (pse.y > pse_prev.y)
		{
			y = MathHelper.floor_double(pse.y);
			y_min = MathHelper.floor_double(pse_prev.y);
		}
		else
		{
			y = MathHelper.floor_double(pse_prev.y);
			y_min = MathHelper.floor_double(pse.y);
		}
		--y_min;

		/* Prepare mask box. */
		double diffY = pse.y - pse_prev.y, diffNew;
		AxisAlignedBB mask = bogie.getParent().initBoundingBox().offset(pse.x, pse.y, pse.z);
		AxisAlignedBB aabb;

		if (diffY > 0.0D)
		{
			mask.maxY += diffY;
		}
		else if (diffY < 0.0D)
		{
			mask.minY -= diffY;
		}

		/* Then iterate upwards, searching for the first block that provides a collision box to collide with. */
		while (y >= y_min)
		{
			boxesCache.clear();
			world.getBlock(x, y, z).addCollisionBoxesToList(world, x, y, z, mask, boxesCache, null);

			if (!boxesCache.isEmpty())
			{
				for (int i = boxesCache.size() - 1; i >= 0; --i)
				{
					aabb = boxesCache.get(i);
					/*
					 * Taken from: AxisAlignedBB#calculateYOffset(AxisAlignedBB, double)
					 * 
					 * This skips some checks which should return true anyways,
					 * since the selected blocks are straight below the PSE's position.
					 * And since a block's collision doesn't extend beyond the block bounds,
					 * we can safely assume aforementioned checks to succeed anyways.
					 */
					if (diffY > 0.0D && mask.maxY <= aabb.minY)
					{
						diffNew = aabb.minY - mask.maxY;
						if (diffNew < diffY)
						{
							diffY = diffNew;
						}
					}
					else if (diffY < 0.0D && mask.minY >= aabb.maxY)
					{
						diffNew = aabb.maxY - mask.minY;
						if (diffNew > diffY)
						{
							diffY = diffNew;
						}
					}
				}
				pse.y -= diffY;
				boxesCache.clear();
				return;
			}

			--y;
		}
	}

	/**
	 * Called when the given entity collides with the given train.
	 *
	 * @param train - The {@link zoranodensha.api.vehicles.Train train} colliding with the entity.
	 * @param entity - The {@link net.minecraft.entity.Entity entity} the vehicle has collided with.
	 */
	public void onEntityCollision(Train train, Entity entity)
	{
		/*
		 * Collisions between trains are handled during position updates. Also ignore collisions with standing trains.
		 */
		if (entity instanceof Train || Math.abs(train.getLastSpeed()) < MovementHandler.MOVEMENT_THRESHOLD)
		{
			return;
		}

		/*
		 * Get the closest bogie to retrieve the movement direction from.
		 */
		PartTypeBogie bogie = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY).getClosestPartType(entity.posX, entity.posY, entity.posZ);
		if (bogie == null)
		{
			return;
		}

		/*
		 * @formatter:off
		 *
		 * 		 Ma * Va + Mb * Vb + Mb * (Vb - Va) * C
		 * Vf = ----------------------------------------
		 * 						Ma + Mb
		 *
		 * Where;
		 * Vf: Final velocity after impact (Object A)
		 * Va: Initial velocity (Object A)
		 * Vb: Initial velocity (Object B)
		 * Ma: Mass (Object A)
		 * Mb: Mass (Object B)
		 * C: Coefficient of restitution
		 *
		 * @formatter:on
		 */

		/* Prepare values. */
		final float ONE_BY_GRAVITY_1000 = (1000.0F / 9.81F);
		float massTrain = train.getMass() * ONE_BY_GRAVITY_1000;
		float massEntity = getEntityMass(entity);
		float mass = 1.0F / (massTrain + massEntity);
		double entityMotionSq = (entity.motionX * entity.motionX + entity.motionY * entity.motionY + entity.motionZ * entity.motionZ);

		/* Determine motion from bogie. */
		Vec3 vec = bogie.getDirection();
		double speed = train.getLastSpeed();
		double vehMotionX = vec.xCoord * speed;
		double vehMotionY = vec.yCoord * speed;
		double vehMotionZ = vec.zCoord * speed;

		/*
		 * Apply change of velocity to the entity.
		 */
		entity.addVelocity(	(massEntity * entity.motionX + massTrain * vehMotionX + massTrain * (vehMotionX - entity.motionX) * COEFFICIENT_RESTITUTION) * mass,
							(massEntity * entity.motionY + massTrain * vehMotionY + massTrain * (vehMotionY - entity.motionY) * COEFFICIENT_RESTITUTION) * mass,
							(massEntity * entity.motionZ + massTrain * vehMotionZ + massTrain * (vehMotionZ - entity.motionZ) * COEFFICIENT_RESTITUTION) * mass);
		entity.velocityChanged = true;

		/*
		 * Apply damage if entity is a living entity.
		 */
		if (entity instanceof EntityLivingBase)
		{
			/*
			 * It was initially planned to throw the entity back first, if the damage exceeds its maximum health.
			 * This was discarded due to time constrains with the reason that high-speed collisions wouldn't really throw an entity back anyways, but rather tear apart.
			 * This is justified as we generally assume all trains to have flat noses, even though previously mentioned reason is terribly incorrect for aerodynamically shaped trains.
			 */
			float damage = (float)((entity.motionX * entity.motionX + entity.motionY * entity.motionY + entity.motionZ * entity.motionZ) - entityMotionSq) * 10.0F;
			EntityLivingBase entityLiving = (EntityLivingBase)entity;
			LivingHitByTrainEvent livingHitEvent = new LivingHitByTrainEvent(train, entityLiving, damage);

			/* Only deal damage if the event isn't cancelled. */
			if (!MinecraftForge.EVENT_BUS.post(livingHitEvent))
			{
				if (livingHitEvent.damageSource != null)
				{
					entityLiving.attackEntityFrom(livingHitEvent.damageSource, damage);
				}
			}
		}
	}

	/**
	 * Call to statically set a new collision handler.
	 *
	 * @param handler - The new CollisionHandler to set.
	 */
	public static final void overrideHandler(CollisionHandler handler)
	{
		INSTANCE = handler;
	}



	/**
	 * {@link net.minecraft.command.IEntitySelector Entity selector} that selects any live entity that is not a {@link zoranodensha.api.vehicles.Train train}.
	 * 
	 * @author Leshuwa Kaiheiwa
	 */
	public static class EntitySelectorNoTrains implements IEntitySelector
	{
		@Override
		public boolean isEntityApplicable(Entity entity)
		{
			return !(entity instanceof Train) && entity.isEntityAlive();
		}
	}

	/**
	 * {@link net.minecraft.command.IEntitySelector Entity selector} which selects only {@link zoranodensha.api.vehicles.Train trains}.
	 * 
	 * @author Leshuwa Kaiheiwa
	 */
	public static class EntitySelectorTrains implements IEntitySelector
	{
		/** The {@link zoranodensha.api.vehicles.Train train} this selector shall ignore. May be {@code null}. */
		private final Train trainToIgnore;



		public EntitySelectorTrains(@Nullable Train trainToIgnore)
		{
			this.trainToIgnore = trainToIgnore;
		}

		@Override
		public boolean isEntityApplicable(Entity entity)
		{
			if (entity instanceof Train && entity != trainToIgnore)
			{
				Train train = (Train)entity;
				if (train.getHasVehicles())
				{
					return entity.isEntityAlive();
				}
			}
			return false;
		}
	}
}