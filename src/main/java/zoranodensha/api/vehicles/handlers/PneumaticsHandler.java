package zoranodensha.api.vehicles.handlers;

import java.util.ArrayList;
import java.util.Random;

import org.apache.logging.log4j.Level;

import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.property.PropVessel;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic;
import zoranodensha.api.vehicles.part.type.PartTypeEngineElectric;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.core.ModCenter;



/**
 * <p>
 * A class which provides various methods to allow for the realistic calculation
 * of air pressure dynamics in trains.
 * </p>
 * 
 * @author Jaffa
 */
public class PneumaticsHandler
{
	/**
	 * <p>
	 * A reference to the current main, static instance of this class.
	 * </p>
	 */
	public static PneumaticsHandler INSTANCE = new PneumaticsHandler();

	/**
	 * <p>
	 * A multiplier which is applied to all pneumatics calculations. This can be
	 * adjusted to either slow down or speed up pneumatics-based physics.
	 * </p>
	 * <p>
	 * Used primarily for debugging purposes.
	 * </p>
	 */
	public static float PNEUMATICS_COEFFICIENT = 1.0F;



	/**
	 * <h1>{@link PneumaticsHandler#equalise(PropVessel, PropVessel, float)}</h1>
	 * <p>
	 * <b>A method which handles the logic of air pressure transfer between two
	 * 'vessels' (containers of air pressure).</b>
	 * </p>
	 * <p>
	 * Calling this method and passing two different air pressure vessels as
	 * parameters will cause the air pressure to change in those two vessels, as if
	 * they were connected with an air pipe or hose. Air pressure will 'flow' from
	 * the vessel with the greater pressure to the vessel with the lower pressure
	 * until they end up at the same pressure (represented as {@code kPa},
	 * kilopascals).
	 * </p>
	 * <p>
	 * This method must be called on every server-side update tick on those same
	 * vessels to ensure the rate of change in air pressure is consistent and
	 * realistic.
	 * </p>
	 * <hr>
	 * <p>
	 * The two supplied pressure vessels must have differing pressure levels. Their
	 * capacities does not matter, but their {@code kPa} value must be different, or
	 * else this method will automatically return with no change in pressures.
	 * </p>
	 * 
	 * @param pressureA - The first vessel of the air pressure transfer.
	 * @param pressureB - The second vessel of the air pressure transfer.
	 * @param flowMaximum - The maximum rate of flow between the vessels, similar in
	 *            effect to controlling the diameter of the pipe/hose
	 *            connecting the two supplied vessels. <b>Passing a
	 *            {@code 0.0F} for this parameter will allow for an
	 *            unlimited flow rate.</b>
	 */
	public void equalise(PropVessel pressureA, PropVessel pressureB, float flowMaximum)
	{
		/*
		 * Cancel if the maximum flow rate is too low.
		 */
		if (flowMaximum < 0.0F)
		{
			return;
		}

		/*
		 * Cancel if the vessels don't have any pressure difference.
		 */
		if (pressureA.getPressure() == pressureB.getPressure())
		{
			return;
		}

		/*
		 * Cancel if either of the vessels has a '0' capacity.
		 */
		if (pressureA.getCapacity() <= 0.0F || pressureB.getCapacity() <= 0.0F)
		{
			return;
		}

		float pressureUnitsTotal = 0.0F;
		float pressureEach = 0.0F;
		float capacityTotal = 0.0F;

		float pressureATarget = 0.0F;
		float pressureBTarget = 0.0F;

		float pressureUnitsDifferenceA = 0.0F;
		float pressureUnitsDifferenceB = 0.0F;

		if (pressureA.getPressure() != pressureB.getPressure())
		{
			if (pressureA.getPressure() > pressureB.getPressure() && pressureB.isFull())
			{
				return;
			}

			if (pressureA.getPressure() < pressureB.getPressure() && pressureA.isFull())
			{
				return;
			}

			pressureUnitsTotal = pressureA.getUnits() + pressureB.getUnits();
			capacityTotal = pressureA.getCapacity() + pressureB.getCapacity();

			pressureEach = pressureUnitsTotal / capacityTotal;

			pressureATarget = pressureEach * pressureA.getCapacity();
			pressureBTarget = pressureEach * pressureB.getCapacity();

			pressureUnitsDifferenceA = pressureATarget - pressureA.getUnits();
			pressureUnitsDifferenceB = pressureBTarget - pressureB.getUnits();

			if (pressureUnitsDifferenceA > 0.0F)
			{
				if (pressureA.isFull())
				{
					pressureUnitsDifferenceA *= 0.0F;
					pressureUnitsDifferenceB *= 0.0F;
				}
			}

			if (pressureUnitsDifferenceB > 0.0F)
			{
				if (pressureB.isFull())
				{
					pressureUnitsDifferenceA *= 0.0F;
					pressureUnitsDifferenceB *= 0.0F;
				}
			}

			/*
			 * Pressure Smoothing
			 */
			float maxUnitsTotal = (pressureA.getMaximumPressure() * pressureA.getCapacity()) + (pressureB.getMaximumPressure() * pressureB.getCapacity());
			float amount = (Math.abs(pressureUnitsDifferenceA) + Math.abs(pressureUnitsDifferenceB)) / (maxUnitsTotal * 2.0F);
			if (amount > 1.0F)
			{
				amount = 1.0F;
			}
			if (amount > 0.0F)
			{
				pressureUnitsDifferenceA *= Math.sqrt(amount);
				pressureUnitsDifferenceB *= Math.sqrt(amount);
			}

			/*
			 * Flow Cap
			 */
			if (flowMaximum > 0.0F)
			{
				if (pressureUnitsDifferenceA > flowMaximum)
				{
					pressureUnitsDifferenceA = flowMaximum;
				}
				else if (pressureUnitsDifferenceA < -flowMaximum)
				{
					pressureUnitsDifferenceA = -flowMaximum;
				}

				if (pressureUnitsDifferenceB > flowMaximum)
				{
					pressureUnitsDifferenceB = flowMaximum;
				}
				else if (pressureUnitsDifferenceB < -flowMaximum)
				{
					pressureUnitsDifferenceB = -flowMaximum;
				}
			}

			pressureA.valueModification += pressureUnitsDifferenceA;
			pressureB.valueModification += pressureUnitsDifferenceB;

			pressureUnitsDifferenceA *= PNEUMATICS_COEFFICIENT;
			pressureUnitsDifferenceB *= PNEUMATICS_COEFFICIENT;

			pressureA.set(pressureA.get() + pressureUnitsDifferenceA);
			pressureB.set(pressureB.get() + pressureUnitsDifferenceB);
		}

	}

	public void equaliseOneWay(PropVessel pressureFrom, PropVessel pressureTo, float flowMaximum)
	{
		if (flowMaximum < 0.0F)
		{
			return;
		}

		if (pressureFrom.getPressure() > pressureTo.getPressure() && !pressureTo.isFull())
		{
			equalise(pressureFrom, pressureTo, flowMaximum);
		}
	}

	/**
	 * Performs a pneumatic update tick for the specified pipe.
	 * 
	 * @param pipe - The pipe for which the pneumatic logic is to be calculated.
	 */
	public void equalisePipe(ArrayList<PropVessel> pipe)
	{
		for (int i = 0; i < 10; i++)
		{
			/*
			 * Don't run if the pipe only has 1 value.
			 */
			if (pipe.size() <= 1)
			{
				return;
			}

			/*
			 * Variables
			 */
			int index;
			float left;
			float right;

			/*
			 * Placeholder arrays for each direction of the calculation
			 */
			float[] pipeForward = new float[pipe.size()];
			float[] pipeBackward = new float[pipe.size()];

			/*
			 * Pre-populated values
			 */
			pipeForward[pipe.size() - 1] = pipe.get(pipe.size() - 1).getPressure();
			pipeBackward[0] = pipe.get(0).getPressure();

			/*
			 * Forward direction
			 */
			for (index = 0; index < pipe.size() - 1; index++)
			{
				left = pipe.get(index).getPressure();
				right = pipe.get(index + 1).getPressure();

				pipeForward[index] = (left + right) / 2.0F;
			}

			/*
			 * Backward direction
			 */
			for (index = pipe.size() - 1; index >= 1; index--)
			{
				right = pipe.get(index).getPressure();
				left = pipe.get(index - 1).getPressure();

				pipeBackward[index] = (left + right) / 2.0F;
			}

			/*
			 * Calculate averages
			 */
			for (index = 0; index < pipe.size(); index++)
			{
				float forwardBackwardAverage = (pipeForward[index] + pipeBackward[index]) / 2.0F;

				pipe.get(index).setPressure(forwardBackwardAverage);
			}

			/*
			 * Garbage collection - not sure if this is necessary.
			 */
			pipeForward = null;
			pipeBackward = null;
		}
	}

	/**
	 * <p>
	 * Called to perform a pneumatics update tick onto the specified train.
	 * </p>
	 * 
	 * @param train - The train to perform the update tick on.
	 */
	public void updateTrainPneumatics(Train train)
	{
		/*
		 * Get all bogies in the train to calculate brake pipe and main reservoir pipe
		 * pressures.
		 */
		ConcatenatedTypesList<PartTypeBogie> bogieTypes = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
		if (bogieTypes.size() <= 0)
		{
			return;
		}

		/*
		 * Remember the pressure of all bogie pressure vessels before doing any
		 * pneumatics logic. Used for sound effects.
		 */
		for (PartTypeBogie bogie : bogieTypes)
		{
			bogie.pneumaticsSupplementaryReservoir.valueBefore = bogie.pneumaticsSupplementaryReservoir.getUnits();
			bogie.pneumaticsBrakeCylinder.valueBefore = bogie.pneumaticsBrakeCylinder.getUnits();
			bogie.pneumaticsBrakePipe.valueBefore = bogie.pneumaticsBrakePipe.getUnits();
			bogie.pneumaticsMainReservoirPipe.valueBefore = bogie.pneumaticsMainReservoirPipe.getUnits();
			bogie.pneumaticsParkBrakeCylinder.valueBefore = bogie.pneumaticsParkBrakeCylinder.getUnits();
		}

		/*
		 * Continuous Air Pipes (Brake Pipe, Main Reservoir Pipe)
		 */
		{
			/*
			 * Create some ArrayList instances to hold the continuous air pipes of the
			 * train.
			 */
			ArrayList<PropVessel> trainBrakePipe = new ArrayList<PropVessel>();
			ArrayList<PropVessel> trainMainReservoirPipe = new ArrayList<PropVessel>();

			/*
			 * Go through all the bogies in the train and add it to the continuous main
			 * reservoir pipe list.
			 */
			for (int index = 0; index < bogieTypes.size(); index++)
			{
				trainBrakePipe.add(bogieTypes.get(index).pneumaticsBrakePipe);
				trainMainReservoirPipe.add(bogieTypes.get(index).pneumaticsMainReservoirPipe);
			}

			/*
			 * Call an update tick on each of the continuous pipes.
			 */
			equalisePipe(trainBrakePipe);
			equalisePipe(trainMainReservoirPipe);
		}

		/*
		 * Brake Systems
		 */
		{
			/*
			 * This is the signal value for EP brakes throughout the train.
			 */
			float targetEP = 0.0F;

			/*
			 * Get the active cab to see if EP brakes are energised. If no active cab can be
			 * found, default to having no EP brakes.
			 */
			if (train.getActiveCab() != null)
			{
				if (!train.getActiveCab().button_brakeMode.get() && train.getActiveCab().reverser.get() != EReverser.LOCKED)
				{
					targetEP = train.getActiveCab().getBrakeLevelLogical();
				}
			}

			/*
			 * Go through each bogie throughout the train.
			 */
			for (PartTypeBogie bogie : bogieTypes)
			{
				/*
				 * Supplementary Reservoir
				 */
				{
					/*
					 * Charge the supplementary reservoir from the main reservoir pipe.
					 */
					equalise(bogie.pneumaticsMainReservoirPipe, bogie.pneumaticsSupplementaryReservoir, 80.0F);
				}

				/*
				 * Triple Valve
				 */
				{
					float AR = bogie.pneumaticsAuxiliaryReservoir.getPressure();
					float BP = bogie.pneumaticsBrakePipe.getPressure();
					float CR = bogie.pneumaticsControlReservoir.getPressure();
					float DBC = bogie.pneumaticsDummyBrakeCylinder.getPressure();

					/*
					 * Establish Control Reservoir
					 */
					if (BP + 15.0F > CR)
					{
						equaliseOneWay(bogie.pneumaticsBrakePipe, bogie.pneumaticsControlReservoir, 30.0F);
					}
					else if (BP < CR / 2.0F)
					{
						vent(bogie.pneumaticsControlReservoir, 20.0F);
					}

					/*
					 * Application
					 */
					if (BP < AR - 3.0F)
					{
						float difference = Math.abs(BP - (AR - 3.0F));

						equalise(bogie.pneumaticsAuxiliaryReservoir, bogie.pneumaticsDummyBrakeCylinder, difference < 4.0F ? 8.0F : 25.0F);
					}

					/*
					 * Release
					 */
					else if (BP > AR)
					{
						if (BP + DBC > CR)
						{
							vent(bogie.pneumaticsDummyBrakeCylinder, 25.0F);
						}

						equalise(bogie.pneumaticsBrakePipe, bogie.pneumaticsAuxiliaryReservoir, 40.0F);
					}
				}

				/*
				 * Supplementary Brake System
				 */
				{
					float targetCylinderPressure = bogie.pneumaticsDummyBrakeCylinder.getPressure() * 2.0F;
					float targetDistance = bogie.pneumaticsTripleValvePipe.getPressure() - targetCylinderPressure;
					float speed;

					/*
					 * Application
					 */
					if (targetDistance < 0.0F)
					{
						equalise(bogie.pneumaticsSupplementaryReservoir, bogie.pneumaticsTripleValvePipe, Math.min(25.0F, 0.0F + Math.abs(targetDistance) * 3.0F));
					}

					/*
					 * Release
					 */
					else if (targetDistance > 0.0F)
					{
						vent(bogie.pneumaticsTripleValvePipe, Math.min(25.0F, 0.0F + (targetDistance) * 3.0F));
					}
				}

				/*
				 * EP Unit Functionality
				 */
				if (bogie.hasEPUnit.get())
				{
					float adjustedTargetEP = targetEP;

					/*
					 * If this bogie has a motor, control EP/Dynamic blending.
					 */
					PartTypeEngineElectric bogieEngine = bogie.getParent().getPartType(PartTypeEngineElectric.class);
					if (bogieEngine != null)
					{
						/*
						 * Check if the electric engine in this bogie is braking using the dynamic brake. If it is, we need to adjust the adjustedTargetEP level to blend the two braking systems
						 * together.
						 */
						if (bogieEngine.motorPolarity.get() <= -0.75F)
						{
							final float bogieTractiveEffort = (float)bogie.tractiveEffort.get();
							final float brakingForce = Math.abs(bogieEngine.getProducedForce());
							float dynamicBrakingEffort = brakingForce / bogieTractiveEffort;

							adjustedTargetEP -= dynamicBrakingEffort;

							if (adjustedTargetEP < 0.0F)
							{
								adjustedTargetEP = 0.0F;
							}
							else if (adjustedTargetEP > 1.0F)
							{
								adjustedTargetEP = 1.0F;
							}
						}
					}

					/*
					 * EP Braking Logic
					 */
					if (adjustedTargetEP >= 0.0F)
					{
						float targetCylinderPressure = adjustedTargetEP * 300.0F;
						float BC = bogie.pneumaticsBrakeCylinder.getPressure();

						float targetEPActivity = (targetCylinderPressure - BC) / 50.0F;
						float oldEPActivity = bogie.epValveActivity.get();

						bogie.epValveActivity.set(oldEPActivity + ((targetEPActivity - oldEPActivity) * 0.3F));

						if (bogie.epValveActivity.get() > 0.0F)
						{
							equaliseOneWay(bogie.pneumaticsMainReservoirPipe, bogie.pneumaticsEPPipe, 30.0F * Math.abs(bogie.epValveActivity.get()));
						}
						else if (bogie.epValveActivity.get() < 0.0F)
						{
							vent(bogie.pneumaticsEPPipe, 40.0F * Math.abs(bogie.epValveActivity.get()));
						}
					}
					else
					{
						if (bogie.pneumaticsEPPipe.getPressure() > 0.0F)
						{
							vent(bogie.pneumaticsEPPipe, 30.0F);
						}
					}
				}
				else
				{
					if (bogie.pneumaticsEPPipe.getPressure() > 0.0F)
					{
						vent(bogie.pneumaticsEPPipe, 25.0F);
					}
				}

				/*
				 * Double Check Valve
				 */
				{
					float leftPressure = bogie.pneumaticsEPPipe.getPressure();
					float rightPressure = bogie.pneumaticsTripleValvePipe.getPressure();

					ArrayList<PropVessel> pipeFlow = new ArrayList<PropVessel>();
					pipeFlow.add(bogie.pneumaticsBrakeCylinder);

					if (leftPressure > rightPressure)
					{
						pipeFlow.add(bogie.pneumaticsEPPipe);

						equalisePipe(pipeFlow);
					}
					else
					{
						pipeFlow.add(bogie.pneumaticsTripleValvePipe);

						equalisePipe(pipeFlow);
					}
				}
			}
		}

		/*
		 * Remember the pressure of all bogie pressure vessels after pneumatics logic
		 * has been done. Used for sound effects.
		 */
		for (PartTypeBogie bogie : bogieTypes)
		{
			bogie.pneumaticsSupplementaryReservoir.valueAfter = bogie.pneumaticsSupplementaryReservoir.getUnits();
			bogie.pneumaticsBrakeCylinder.valueAfter = bogie.pneumaticsBrakeCylinder.getUnits();
			bogie.pneumaticsBrakePipe.valueAfter = bogie.pneumaticsBrakePipe.getUnits();
			bogie.pneumaticsMainReservoirPipe.valueAfter = bogie.pneumaticsMainReservoirPipe.getUnits();
			bogie.pneumaticsParkBrakeCylinder.valueAfter = bogie.pneumaticsParkBrakeCylinder.getUnits();
		}
	}

	/**
	 * <p>
	 * This method is called to 'vent' or 'expel' the air pressure from an air
	 * vessel to atmosphere.
	 * </p>
	 * 
	 * @param vessel - The vessel to expend the air pressure from.
	 * @param flowMaximum - The maximum rate at which the air can be expelled from
	 *            the air vessel.
	 */
	public void vent(PropVessel vessel, float flowMaximum)
	{
		/*
		 * If the air vessel is already empty, return.
		 */
		if (vessel.isEmpty())
		{
			return;
		}

		/*
		 * Calculate how much to decrease the air pressure by.
		 */
		float decrease = vessel.get() * -0.5F;

		/*
		 * Cap the decrease amount by the specified maximum flow rate.
		 */
		if (flowMaximum > 0.0F)
		{
			if (decrease > flowMaximum)
			{
				decrease = flowMaximum;
			}
			else if (decrease < -flowMaximum)
			{
				decrease = -flowMaximum;
			}
		}

		float amount = Math.abs(vessel.getUnits() / ((vessel.getCapacity() * vessel.getMaximumPressure()) * 2.0F));
		if (amount > 1.0F)
		{
			amount = 1.0F;
		}
		if (amount > 0.0F)
		{
			decrease *= Math.sqrt(amount);
		}

		/*
		 * Take into account the pneumatics coefficient.
		 */
		decrease *= PNEUMATICS_COEFFICIENT;

		/*
		 * Make the changes to the pressure vessel.
		 */
		vessel.set(vessel.get() + decrease);
		vessel.valueModification += decrease;
	}

}
