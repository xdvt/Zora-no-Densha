package zoranodensha.api.vehicles.handlers;

import java.util.ArrayList;
import java.util.Iterator;

import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;



/**
 * Default section division handler class. Its methods will be called to divide a {@link zoranodensha.api.vehicles.Train train} into
 * {@link zoranodensha.api.vehicles.VehicleSection sections}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class SectionDivisionHandler
{
	/** A reference to the currently set collision handler instance. */
	public static SectionDivisionHandler INSTANCE = new SectionDivisionHandler();

	/** A vehicle part {@link zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector selector} that selects any part <b>but</b> non-dummy bogies. */
	public static final IPartsSelector SELECTOR_ANY_BUT_BOGIE = new IPartsSelector()
	{
		@Override
		public boolean addToList(VehParBase part)
		{
			PartTypeBogie bogie = part.getPartType(PartTypeBogie.class);
			return (bogie == null) || !OrderedTypesList.SELECTOR_BOGIE_NODUMMY.addToList(bogie);
		}
	};



	/**
	 * Helper method to add all parts in the given list whose local X position is greater than or equal to the given value
	 * to a new list. The given list will be modified.
	 * 
	 * @param parts - List of parts to select from. Will be modified.
	 * @param minLocalX - Minimum local X offset required by a part to be added.
	 * @return A {@link zoranodensha.api.vehicles.part.util.OrderedPartsList list} of all matching parts.
	 */
	protected OrderedPartsList getSubList(OrderedPartsList parts, float minLocalX)
	{
		OrderedPartsList res = new OrderedPartsList();
		{
			/* Ensure we received something to do. */
			if (!parts.isEmpty())
			{
				/* Iterating from back (-X) to front (+X).. */
				Iterator<VehParBase> itera = parts.iterator();
				while (itera.hasNext())
				{
					/* ..compare each part's local X-offset.. */
					VehParBase part = itera.next();
					if (part.getOffset()[0] >= minLocalX)
					{
						/* ..and add it to the resulting list while removing it from the given list. */
						res.add(part);
						itera.remove();
					}
				}
			}
		}
		return res;
	}

	/**
	 * Divides the given vehicle into vehicle sections.
	 * 
	 * @param vehicle - {@link zoranodensha.api.vehicles.VehicleData Vehicle} to divide.
	 * @return The first (i.e. front-most) section of the vehicle, or {@code null} if the vehicle couldn't be sectioned.
	 */
	public VehicleSection getSectionedVehicle(VehicleData vehicle)
	{
		/*
		 * Retrieve bogies and vehicle parts from vehicle.
		 */
		OrderedTypesList<PartTypeBogie> vehicleBogies = vehicle.getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
		OrderedPartsList vehicleParts = vehicle.getParts(SELECTOR_ANY_BUT_BOGIE);
		VehicleSection frontSection = null;

		/*
		 * Split vehicle parts and bogies into sections.
		 */
		while (!vehicleBogies.isEmpty())
		{
			/* Get the bogies. Remove the front bogie from list as this is the only section where it may be the front bogie. */
			PartTypeBogie bogieF = vehicleBogies.remove(0);
			PartTypeBogie bogieB = null;

			if (!vehicleBogies.isEmpty())
			{
				bogieB = vehicleBogies.get(0);
			}

			/* Decide where to split the section's parts list, and retrieve parts list. */
			float minPartX;
			{
				if (bogieB != null && vehicleBogies.size() > 1)
				{
					minPartX = bogieB.getParent().getOffset()[0];
				}
				else
				{
					minPartX = -Float.MAX_VALUE;
					vehicleBogies.clear();
				}
			}
			OrderedPartsList sectionParts = getSubList(vehicleParts, minPartX);

			/* Finally, create section from gathered data and append. */
			VehicleSection section = new VehicleSection(vehicle, sectionParts, bogieF, bogieB);
			if (frontSection == null)
			{
				/* If this is the first section, it is the vehicle's front. */
				frontSection = section;
			}
			else
			{
				/* Otherwise append it to the last section of the sectioned vehicle. */
				frontSection.getFinalBack().setBackSection(section);
			}
		}

		return frontSection;
	}

	/**
	 * Helper method to determine the section(s) the given bogie belongs to.<br>
	 * Note that this method was made for bogies which are not contained in their section's / sections' parts list(s).
	 * 
	 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} whose section(s) are to be determined.
	 * @return A list containing all {@link zoranodensha.api.vehicles.VehicleSection sections} the given bogie belongs to.
	 */
	public ArrayList<VehicleSection> getSectionsOf(PartTypeBogie bogie)
	{
		ArrayList<VehicleSection> sections = new ArrayList<VehicleSection>();
		{
			for (VehicleSection section : bogie.getVehicle())
			{
				if (section.bogieB == bogie || section.bogieF == bogie)
				{
					sections.add(section);
				}
			}
		}
		return sections;
	}

	/**
	 * Call to statically set a new section division handler.
	 *
	 * @param handler - The new SectionDivisionHandler to set.
	 */
	public static final void overrideHandler(SectionDivisionHandler handler)
	{
		INSTANCE = handler;
	}
}