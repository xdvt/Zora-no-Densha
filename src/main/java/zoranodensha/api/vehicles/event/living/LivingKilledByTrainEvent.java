package zoranodensha.api.vehicles.event.living;

import cpw.mods.fml.common.eventhandler.Cancelable;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent;
import zoranodensha.api.vehicles.Train;



/**
 * Fired when a {@link Train} has killed a {@link EntityLivingBase living entity}.<br>
 * Set to be fired in {@link Train#onKillEntity(EntityLivingBase) onKillEntity()}.<br>
 * <br>
 * This event is not {@link Cancelable}.<br>
 * <br>
 * This event has no result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
public class LivingKilledByTrainEvent extends LivingEvent
{
	/** The {@link Train} inflicting the damage. */
	public final Train train;



	/**
	 * Construct a new event.
	 *
	 * @param train - {@link Train} that killed the given entity.
	 * @param victim - The {@link net.minecraft.entity.EntityLivingBase entity} that was killed.
	 */
	public LivingKilledByTrainEvent(Train train, EntityLivingBase victim)
	{
		super(victim);
		this.train = train;
	}
}
