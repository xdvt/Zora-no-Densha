package zoranodensha.api.vehicles.event.train;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;



/**
 * Fired whenever a {@link Train} is about to be rendered.<br>
 * <br>
 * This event does not have a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
@SideOnly(Side.CLIENT)
public class TrainRenderEvent extends ATrainEvent
{
	/**
	 * Construct a new event.
	 *
	 * @param train - The train that is about to be rendered.
	 */
	public TrainRenderEvent(Train train)
	{
		super(train);
	}



	/**
	 * Fired before rendering routine begins. This event is not {@link Cancelable}.
	 */
	@SideOnly(Side.CLIENT)
	public static class Post extends TrainRenderEvent
	{
		public Post(Train train)
		{
			super(train);
		}
	}


	/**
	 * Fired before rendering routine begins.<br>
	 * <br>
	 * This event is {@link Cancelable}.<br>
	 * If this event is canceled, the train won't render.
	 */
	@Cancelable
	@SideOnly(Side.CLIENT)
	public static class Pre extends TrainRenderEvent
	{
		/** The partial tick value of the current tick, expected to be between {@code 0.0F} and {@code 1.0F}. */
		public final float partialTick;
		/** The current render pass. */
		public final int pass;



		public Pre(Train train, float partialTick, int pass)
		{
			super(train);

			this.partialTick = partialTick;
			this.pass = pass;
		}
	}
}
