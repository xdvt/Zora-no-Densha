package zoranodensha.api.vehicles.event.train;

import cpw.mods.fml.common.eventhandler.Event;
import zoranodensha.api.vehicles.Train;



/**
 * Fired whenever a {@link Train} is updated.
 */
public abstract class TrainUpdateEvent extends ATrainEvent
{
	public TrainUpdateEvent(Train train)
	{
		super(train);
	}



	/**
	 * Fired by the given {@link Train} after finishing its update routine.
	 */
	@Event.HasResult
	public static class TrainUpdateFinishedEvent extends TrainUpdateEvent
	{
		public TrainUpdateFinishedEvent(Train train)
		{
			super(train);
		}
	}


	/**
	 * Fired during update routines by the given {@link Train} right before actually moving to its new position.<br>
	 * Any functionality that serves the purpose to suppress speed should subscribe to this event in order to
	 * gain control over applied movement speed.<br>
	 * <br>
	 * This event has a Result. {@link HasResult}.
	 */
	@Event.HasResult
	public static class TrainUpdateSpeedEvent extends TrainUpdateEvent
	{
		/**
		 * The speed of the train in meters per tick [m/tick]. May be negative to indicate backward movement.
		 */
		public double result;



		/**
		 * @param train - Train whose speed is about to be updated
		 * @param speed - The speed calculated during update routines, in meters per tick [m/tick]. May be negative to indicate backward movement.
		 */
		public TrainUpdateSpeedEvent(Train train, double speed)
		{
			super(train);
			result = speed;
		}
	}
}
