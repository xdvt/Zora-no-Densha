package zoranodensha.api.vehicles.event.train;

import cpw.mods.fml.common.eventhandler.Cancelable;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;



/**
 * Fired just after a {@link Train} was marked for destruction.<br>
 * It will be destroyed in the next game tick.<br>
 * Set to be fired in {@link Train#setDead() setDead()}.<br>
 * <br>
 * This event is not {@link Cancelable}.<br>
 * <br>
 * This event does not have a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
public class TrainDestroyedEvent extends ATrainEvent
{
	public TrainDestroyedEvent(Train vehicle)
	{
		super(vehicle);
	}
}