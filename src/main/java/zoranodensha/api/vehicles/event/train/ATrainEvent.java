package zoranodensha.api.vehicles.event.train;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityEvent;
import zoranodensha.api.vehicles.Train;



/**
 * Fired when something involving a {@link Train} is about to happen.<br>
 * <br>
 * Events of this type are fired on the {@link MinecraftForge#EVENT_BUS}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class ATrainEvent extends EntityEvent
{
	/** The train in question. */
	public final Train train;



	public ATrainEvent(Train train)
	{
		super(train);
		this.train = train;
	}
}
