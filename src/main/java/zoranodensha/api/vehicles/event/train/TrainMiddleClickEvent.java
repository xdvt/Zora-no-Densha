package zoranodensha.api.vehicles.event.train;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;



/**
 * Fired when a {@link Train} is middle-clicked on.<br>
 * Set to be fired in {@link Train#getPickedResult(net.minecraft.util.MovingObjectPosition) getPickedResult()}.<br>
 * <br>
 * This event is {@link Cancelable}.<br>
 * <br>
 * If this event is canceled, there will be no ItemStack returned.<br>
 * <br>
 * This event has a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
@Cancelable
@Event.HasResult
public class TrainMiddleClickEvent extends ATrainEvent
{
	/** Information about the ray trace hit on the train entity. */
	public final MovingObjectPosition movObjPos;

	/** The {@link ItemStack} that should be returned. */
	public ItemStack result;



	public TrainMiddleClickEvent(Train train, MovingObjectPosition movObjPos)
	{
		super(train);
		this.movObjPos = movObjPos;
	}
}
