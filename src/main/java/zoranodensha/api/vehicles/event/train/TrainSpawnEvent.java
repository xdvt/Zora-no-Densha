package zoranodensha.api.vehicles.event.train;

import javax.annotation.Nullable;

import cpw.mods.fml.common.eventhandler.Cancelable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;



/**
 * Fired just before a {@link Train} gets be spawned using {@link World#spawnEntityInWorld(net.minecraft.entity.Entity) spawnEntityInWorld()},
 * either from a vehicle ItemStack or from a vehicle blueprint.<br>
 * <br>
 * This event is {@link Cancelable}.<br>
 * <br>
 * If this event is canceled, the train won't be spawned.<br>
 * <br>
 * This event does not have a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
@Cancelable
public class TrainSpawnEvent extends ATrainEvent
{
	/** The player spawning the train. May be {@code null}. */
	public final EntityPlayer player;
	/** The world the train is about to be spawned in. */
	public final World world;

	/** The X-coordinate of the selected position. */
	public final int x;
	/** The Y-coordinate of the selected position. */
	public final int y;
	/** The Z-coordinate of the selected position. */
	public final int z;



	/**
	 * Construct a new event with the given arguments.
	 *
	 * @param world - See {@link #world}.
	 * @param x - See {@link #x}.
	 * @param y - See {@link #y}.
	 * @param z - See {@link #z}.
	 * @param player - See {@link #player}.
	 * @param train - The train involved. May not be fully initialised.
	 */
	public TrainSpawnEvent(World world, int x, int y, int z, @Nullable EntityPlayer player, Train train)
	{
		super(train);

		this.world = world;
		this.x = x;
		this.y = y;
		this.z = z;
		this.player = player;
	}
}
