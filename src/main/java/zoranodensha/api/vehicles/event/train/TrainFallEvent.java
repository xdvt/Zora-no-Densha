package zoranodensha.api.vehicles.event.train;

import cpw.mods.fml.common.eventhandler.Cancelable;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;



/**
 * Fired whenever a {@link Train} is about to fall and hurt its passengers.<br>
 * Set to be fired in {@link Train#fall(float) fall()}.<br>
 * <br>
 * This event is {@link Cancelable}.<br>
 * <br>
 * If this event is canceled, there will be no damage to passengers.<br>
 * <br>
 * This event does not have a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
@Cancelable
public class TrainFallEvent extends ATrainEvent
{
	/** The height fallen. */
	public final float height;



	/**
	 * Construct a new event.
	 *
	 * @param train - The train that has fallen.
	 * @param height - The height fallen.
	 */
	public TrainFallEvent(Train train, float height)
	{
		super(train);
		this.height = height;
	}
}
