package zoranodensha.api.vehicles.event.partType;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.part.type.APartType;

/**
 * Fired to call when a {@link zoranodensha.api.vehicles.part.type.APartType part type} wishes to open a GUI.<br>
 * If applicable, default implementation of Zora no Densha already provides data to the result.<br>
 * This event is posted on remote (server) worlds only.<br>
 * <br>
 * This event is {@link Cancelable}.<br>
 * <br>
 * This event has a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
@Cancelable
@Event.HasResult
public class PartTypeGUIOpenEvent extends APartTypeEvent
{
    /** The player involved in the event. */
    public final EntityPlayer player;

    /** Mod instance of the mod whose GUI is to be opened. */
    public Object mod;
    /** The respective mod's internal GUI ID. */
    public int modGuiID;

    public PartTypeGUIOpenEvent(APartType partType, EntityPlayer player)
    {
        super(partType);
        this.player = player;
    }
}
