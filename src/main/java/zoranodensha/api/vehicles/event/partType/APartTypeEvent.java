package zoranodensha.api.vehicles.event.partType;

import cpw.mods.fml.common.eventhandler.Event;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.part.type.APartType;


/**
 * Fired when something involving a {@link zoranodensha.api.vehicles.part.type.APartType part type}
 * is about to happen.<br>
 * <br>
 * Events of this type are fired on the {@link MinecraftForge#EVENT_BUS}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class APartTypeEvent extends Event
{
	/** The part type involved in this event. */
	public final APartType partType;


	public APartTypeEvent(APartType partType)
	{
		this.partType = partType;
	}
}
