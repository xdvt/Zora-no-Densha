package zoranodensha.api.vehicles.event.property;

import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.part.property.IPartProperty;



/**
 * <p>
 * Fired when a {@link zoranodensha.api.vehicles.part.property.IPartProperty property} failed its {@link net.minecraft.nbt.NBTTagCompound NBT} read call.
 * </p>
 * 
 * <p>
 * Triggered by default Zora no Densha part properties whenever their
 * {@link zoranodensha.api.vehicles.part.property.IPartProperty#readFromNBT(NBTTagCompound, ETagCall) NBT read call}
 * failed under valid circumstances, i.e. when the {@link zoranodensha.api.util.EValidTagCalls call type} was valid but the
 * method was still unable to read from NBT.<br>
 * This usually happens when a property's NBT key changes between updates.
 * </p>
 * 
 * <p>
 * To prevent data loss, mods can hook into this event and determine whether the property's parent part belongs to them.<br>
 * If so, they can try to attempt another NBT read call under their own guidance.<br>
 * The simplest case would involve retrying the read call with a different NBT key. However, also more complex cases might arise where a property's NBT value
 * can be interpreted differently to convert old NBT data into a new format.
 * </p>
 * 
 * <p>
 * This event does not have a {@link cpw.mods.fml.common.eventhandler.Event.HasResult result}.<br>
 * <br>
 * This event cannot be {@link cpw.mods.fml.common.eventhandler.Cancelable cancelled}.<br>
 * <br>
 * Events of this type are fired on the {@link MinecraftForge#EVENT_BUS}.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PropertyMissingNBTEvent extends Event
{
	/** The key used to (unsuccessfully) read from {@link net.minecraft.nbt.NBTTagCompound NBT}. */
	public final String nbtKey;
	/** {@link net.minecraft.nbt.NBTTagCompound NBT tag} that the property tried to read from. */
	public final NBTTagCompound nbtTag;
	/** {@link zoranodensha.api.vehicles.part.property.IPartProperty Property} whose read call failed. */
	public final IPartProperty<?> property;



	public PropertyMissingNBTEvent(NBTTagCompound nbtTag, String nbtKey, IPartProperty<?> property)
	{
		this.nbtTag = nbtTag;
		this.nbtKey = nbtKey;
		this.property = property;
	}
}