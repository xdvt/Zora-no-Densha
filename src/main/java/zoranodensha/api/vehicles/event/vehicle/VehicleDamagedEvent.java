package zoranodensha.api.vehicles.event.vehicle;

import cpw.mods.fml.common.eventhandler.Cancelable;
import net.minecraft.util.DamageSource;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;



/**
 * Fired whenever a {@link VehicleData vehicle} in a {@link Train} is about to get damaged.<br>
 * Set to be fired in {@link Train#attackEntityFrom(DamageSource, float) attackEntityFrom()}.<br>
 * <br>
 * This event is {@link Cancelable}.<br>
 * <br>
 * If this event is canceled, the RailVehicle won't receive damage.<br>
 * <br>
 * This event does not have a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
@Cancelable
public class VehicleDamagedEvent extends AVehicleEvent
{
	/** The {@link DamageSource}. */
	public final DamageSource source;
	/** The amount of damage inflicted. */
	public final float damage;



	/**
	 * Construct a new event.
	 *
	 * @param train - Train involved.
	 * @param vehicle - The vehicle that is about to be damaged.
	 * @param source - Damage source.
	 * @param damage - Amount of damage.
	 */
	public VehicleDamagedEvent(Train train, VehicleData vehicle, DamageSource source, float damage)
	{
		super(train, vehicle);

		this.source = source;
		this.damage = damage;
	}
}
