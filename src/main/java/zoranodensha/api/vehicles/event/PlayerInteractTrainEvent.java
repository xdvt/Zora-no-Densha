package zoranodensha.api.vehicles.event;

import cpw.mods.fml.common.eventhandler.Cancelable;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import zoranodensha.api.vehicles.Train;



/**
 * Fired when a {@link Train} is interacted with by a {@link EntityPlayer player}.<br>
 * Set to be fired in {@link Train#interactFirst(EntityPlayer) interactFirst()}.<br>
 * <br>
 * This event is not {@link Cancelable}.<br>
 * <br>
 * This event does not have a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
public class PlayerInteractTrainEvent extends PlayerEvent
{
	/** The {@link Train} in question. */
	public final Train train;



	/**
	 * Construct a new event.
	 *
	 * @param train - See {@link #train}.
	 * @param player - The player in question.
	 */
	public PlayerInteractTrainEvent(Train train, EntityPlayer player)
	{
		super(player);
		this.train = train;
	}
}
