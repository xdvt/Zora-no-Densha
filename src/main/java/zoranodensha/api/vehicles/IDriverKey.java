package zoranodensha.api.vehicles;

import javax.annotation.Nullable;

import net.minecraft.entity.EntityLivingBase;
import zoranodensha.api.vehicles.part.type.APartType;



/**
 * An interface implemented by all items that function as driver's key.<br>
 * Used to access {@link zoranodensha.api.vehicles.part.type.APartType part types},
 * such as {@link zoranodensha.api.vehicles.part.type.PartTypeCabBasic cabs}
 * or {@link zoranodensha.api.vehicles.part.type.PartTypeDoor doors}.
 */
public interface IDriverKey
{
	/**
	 * Determine whether this item may access the given vehicle part type.
	 * 
	 * @param type - {@link zoranodensha.api.vehicles.part.type.APartType Part type} to access.
	 * @param entity - The {@link net.minecraft.entity.EntityLivingBase entity} holding this item, may be {@code null}.
	 * @return {@code true} if the given part type can be accessed with this key, {@code false} if not.
	 */
	boolean getCanAccess(APartType type, @Nullable EntityLivingBase entity);
}