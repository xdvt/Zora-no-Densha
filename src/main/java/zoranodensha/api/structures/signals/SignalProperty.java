package zoranodensha.api.structures.signals;

import net.minecraft.nbt.NBTTagCompound;



/**
 * <p>
 * This class contains a single proeprty which is managed by a signal.
 * </p>
 * 
 * @author Jaffa
 */
public class SignalProperty
{
	private static final String NBTKEY_VALUE = "_value";
	private static final String NBTKEY_MINIMUM = "_minimum";
	private static final String NBTKEY_MAXIMUM = "_maximum";

	/**
	 * The current value of the signal property.
	 */
	private int value = 0;
	/**
	 * The lowest value that the {@link SignalProperty#value} field can be.
	 */
	private int minimum = 0;
	/**
	 * The highest value that the {@link SignalProperty#value} field can be.
	 */
	private int maximum = 0;



	/**
	 * <p>
	 * A blank constructor, all fields will be at their default values.
	 * </p>
	 */
	public SignalProperty()
	{
		;
	}


	/**
	 * <p>
	 * Initialises a new instance of the {@link SignalProperty} class.
	 * </p>
	 * 
	 * @param value - The starting value of the signal property.
	 * @param minimum - The minimum value of the signal property.
	 * @param maximum - The maximum value of the signal property.
	 */
	public SignalProperty(int value, int minimum, int maximum)
	{
		this.value = value;
		this.minimum = minimum;
		this.maximum = maximum;
	}


	/**
	 * <p>
	 * Gets the current value of this signal proeprty.
	 * </p>
	 * 
	 * @return - An {@code int} which is the current value of the signal property.
	 */
	public int get()
	{
		return value;
	}


	/**
	 * <p>
	 * Called to load the saved state of a signal property saved under the supplied key.
	 * </p>
	 * 
	 * @param nbt - The {@link NBTTagCompound} to read from.
	 * @param key - The key which represents the saved signal property to load from.
	 */
	public void readFromNBT(NBTTagCompound nbt, String key)
	{
		value = nbt.getInteger(key + NBTKEY_VALUE);
		minimum = nbt.getInteger(key + NBTKEY_MINIMUM);
		maximum = nbt.getInteger(key + NBTKEY_MAXIMUM);
	}


	/**
	 * <p>
	 * Sets the value of this signal property.
	 * </p>
	 * 
	 * @param value - The new value to assign.
	 */
	public void set(int value)
	{
		this.value = value;
	}


	/**
	 * <p>
	 * Increments the value of the signal property by 1, wrapping around back to the minimum value if the maximum value is exceeded.
	 * </p>
	 */
	public void setNext()
	{
		this.value++;

		if (value > maximum)
		{
			value = minimum;
		}
	}


	/**
	 * <p>
	 * Called to save the state of this signal property to the supplied {@link NBTTagCompound} under the specified key.
	 * </p>
	 * 
	 * @param nbt - The {@link NBTTagCompound} to save to.
	 * @param key - The key to save the signal property under.
	 */
	public void writeToNBT(NBTTagCompound nbt, String key)
	{
		nbt.setInteger(key + NBTKEY_VALUE, value);
		nbt.setInteger(key + NBTKEY_MINIMUM, minimum);
		nbt.setInteger(key + NBTKEY_MAXIMUM, maximum);
	}
}
