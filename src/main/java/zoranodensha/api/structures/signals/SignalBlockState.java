package zoranodensha.api.structures.signals;

import java.util.ArrayList;

import javax.annotation.Nullable;



/**
 * This class is what a signal will see when its checks the block ahead.
 * An instance of this class will tell the signal how far ahead the track goes until the next signal, what the next signal is, whether there is a train in the block section, etc.
 * 
 * @author Jaffa
 */
public class SignalBlockState
{
	/** The length of this signal block in {@code m}. */
	public float blockLength = 0.0F;

	/** This value is {@code true} if this block is terminated by a buffer track (usually treated as a fixed red signal). */
	public boolean hasBuffer = false;

	/** This value is {@code true} if this block section contains any part of a {@link zoranodensha.api.vehicles.Train}. */
	public boolean hasTrain = false;

	/** This value is {@code true} if there is a train after this signal, but only up to the next signal facing the other direction. */
	public boolean hasTrainInInterlocking = false;

	/** This value is {@code true} if this block section contains a diverging route (such as a diverging turnout). */
	public boolean isDivergingRoute = false;

	/** The signal at the end of this block section. If no such signal exists, this value will be null (for example, a track which ends at a wall or void). */
	@Nullable public ISignal nextSignal;

	/** A list of coordinates (x, y, z, x, y, z, etc) which make up the path this signal block is made up of. Used for debugging the Balise Pulse path. */
	public ArrayList<Double> pathDebug;
}
