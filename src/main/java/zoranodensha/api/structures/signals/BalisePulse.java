package zoranodensha.api.structures.signals;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.logging.log4j.Level;

import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.World;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.vehicles.handlers.CollisionHandler;
import zoranodensha.api.vehicles.part.type.cab.mrtms.Landmark;
import zoranodensha.api.vehicles.part.type.cab.mrtms.Landscape;
import zoranodensha.api.vehicles.util.PositionStackEntry;
import zoranodensha.api.vehicles.util.TrackObj;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseGag;
import zoranodensha.common.core.ModCenter;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_1843_1843;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_Straight_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Switch_0000_1131;
import zoranodensha.trackpack.common.section.ZnD_Switch_1131_0000;



/**
 * A class used to allow signals and MTCS systems to follow the track ahead so they have the ability to check for trains, signals, and signs.
 * 
 * @author Jaffa
 */
public class BalisePulse
{
	/** The position at which this balise pulse is currently located. */
	public Vec3 position;

	/** The direction at which the balise pulse is facing, used for movement calculations. */
	public Vec3 direction;
	public Vec3 previousDirection;



	/**
	 * Initialises a new instance of the {@link zoranodensha.api.structures.signals.BalisePulse} class.
	 */
	public BalisePulse()
	{
		this(Vec3.createVectorHelper(0.0D, 0.0D, 0.0D), Vec3.createVectorHelper(1.0D, 0.0D, 0.0D));
	}


	/**
	 * Initialises a new instance of the {@link zoranodensha.api.structures.signals.BalisePulse} class.
	 * 
	 * @param position - The starting position of the balise pulse. Measured as an in-world position.
	 * @param direction - The starting direction of the balise pulse. This should be assigned to a value that sends it in the correct direction down the track.
	 */
	public BalisePulse(Vec3 position, Vec3 direction)
	{
		this.position = position;
		this.direction = direction;
		this.previousDirection = direction;
	}

	/**
	 * Called by an MTCS instance in a train cab to get a 'Landscape' of the track ahead. This will give MTCS an idea of what to expect ahead in terms of speed limits, signals, and buffer stops.
	 * 
	 * @param world - The world instance passed from the train cab.
	 * @return - A {@link zoranodensha.api.vehicles.part.type.cab.mrtms.Landscape} instance.
	 */
	public Landscape getLandscape(World world)
	{
		return getLandscape(world, 20.0F, 0.0F, false);
	}

	/**
	 * Called by an MTCS instance in a train cab to get a 'Landscape' of the track ahead. This will give MTCS an idea of what to expect ahead in terms of speed limits, signals, and buffer stops.
	 * 
	 * @param world - The world instance passed from the train cab.
	 * @param trainLength - How long the train is in {@code m}, to ensure chunks are loaded around the train itself.
	 * @param trainSpeed - How fast the train is moving in {@code km/h}, to ensure chunks are loaded ahead of the train at an appropriate distance.
	 * @return - A {@link zoranodensha.api.vehicles.part.type.cab.mrtms.Landscape} instance.
	 */
	public Landscape getLandscape(World world, float trainLength, float trainSpeed, boolean calculateChunks)
	{
		Landscape landscape = new Landscape();

		/*
		 * Keep track of where the balise pulse starts so we can move it back to its original position later once chunkloading stuff is done.
		 */
		Vec3 originalPosition = Vec3.createVectorHelper(position.xCoord, position.yCoord, position.zCoord);
		Vec3 originalDirection = Vec3.createVectorHelper(direction.xCoord, direction.yCoord, direction.zCoord);
		{
			/*
			 * The distance at which MTCS will scan the track ahead increases as the train moves faster. From a minimum of 512 metres, increased by the train speed in km/h*10.
			 */
			final float maximumDistance = 1024.0F + (Math.abs(trainSpeed) * 10.0F);
			final float distancePerTick = 0.5F;
			float distanceSinceChunk = 0.0F;

			int currentChunkX, previousChunkX = 0;
			int currentChunkZ, previousChunkZ = 0;

			ArrayList<ChunkCoordIntPair> chunks = new ArrayList<ChunkCoordIntPair>();

			if (calculateChunks)
			{
				/*
				 * Flip the balise pulse around to prepare for moving backwards along the train.
				 */
				direction.xCoord *= -1.0F;
				direction.yCoord *= -1.0F;
				direction.zCoord *= -1.0F;

				/*
				 * Ensure chunks are loaded around the train.
				 */
				for (float distance = 0.0F; distance > -(Math.abs(trainLength)); distance -= 8.0F)
				{
					/*
					 * The balise pulse moves backwards along the length of the train (8 metres at a time) to determine which chunks to load.
					 */
					movementTick(8.0F, world);

					int referenceChunkX = (int)position.xCoord;
					int referenceChunkZ = (int)position.zCoord;

					/*
					 * Two for loops are used each going from -1 to +1, this allows a 'sweep' of all chunks in a 3x3 square around the balise pulse. This makes sure that things next to the train
					 * including
					 * signals and other tracks are also chunk loaded to ensure the whole railway works properly.
					 */
					for (int x = -1; x <= 1; x++)
					{
						for (int z = -1; z <= 1; z++)
						{
							/*
							 * Bit shifting is used to convert the current block position into a chunk position (each chunk is 16 blocks on a side).
							 */
							currentChunkX = (referenceChunkX + (x * 12)) >> 4;
							currentChunkZ = (referenceChunkZ + (z * 12)) >> 4;
							if (currentChunkX != previousChunkX || currentChunkZ != previousChunkZ)
							{
								/*
								 * We are in a different chunk, add it to the list.
								 */
								chunks.add(new ChunkCoordIntPair(currentChunkX, currentChunkZ));
							}
							previousChunkX = currentChunkX;
							previousChunkZ = currentChunkZ;
						}
					}
				}
			}

			/*
			 * At this point, the chunkloading code for the area around the train is finished and we will reset the balise pulse to the original position and direction so it can start moving
			 * forward and creating the MTCS landscape.
			 */
			position = originalPosition;
			direction = originalDirection;

			/*
			 * Main Loop
			 */
			for (float distance = 0.0F; distance < maximumDistance; distance += distancePerTick)
			{
				/* Move the pulse */
				movementTick(distancePerTick, world);
				distanceSinceChunk += distancePerTick;


				/*
				 * Load chunks ahead of the train.
				 * This code will run every 8 metres, or half of a chunk length, to make sure no chunks are missed along the way.
				 */
				if (distanceSinceChunk >= 8.0F && calculateChunks)
				{
					distanceSinceChunk = 0.0F;

					/*
					 * The 'chunkDistance' value is how far ahead of the train we wish to load chunks.
					 * If the train is moving really quickly, this value will increase because we want to load chunks far ahead of the train to make sure signals are loaded and the fast moving train
					 * is not affected by glitching signals and tracks. If the train is moving really slowly, this value is lower and chunks are not loaded as far ahead of the train as when it is
					 * moving quickly.
					 */
					float chunkDistance = Math.abs(trainSpeed) * 3.0F;

					/*
					 * The chunk distance is clamped so chunks aren't loaded infinitely far away from the train.
					 */
					if (chunkDistance > 300.0F)
					{
						chunkDistance = 300.0F;
					}
					else if (chunkDistance < 30.0F)
					{
						chunkDistance = 30.0F;
					}

					if (distance <= chunkDistance)
					{
						int referenceChunkX = (int)position.xCoord;
						int referenceChunkZ = (int)position.zCoord;

						/*
						 * Two for-loops are used to sweep a 3x3 area of chunks around the balise pulse. This makes sure no trackside infrastructure is unloaded (e.g. signals, redstone, and other
						 * tracks).
						 */
						for (int x = -1; x <= 1; x++)
						{
							for (int z = -1; z <= 1; z++)
							{
								/*
								 * Bit shifting is used to convert the balise pulse position into a chunk coordinate (each chunk is 16 blocks on a side so bitwise operations make this easy).
								 */
								currentChunkX = (referenceChunkX + (x * 12)) >> 4;
								currentChunkZ = (referenceChunkZ + (z * 12)) >> 4;
								if (currentChunkX != previousChunkX || currentChunkZ != previousChunkZ)
								{
									/*
									 * If we're in a different chunk than what was detected before, add it to the list of chunks to be loaded.
									 */
									chunks.add(new ChunkCoordIntPair(currentChunkX, currentChunkZ));
								}
								previousChunkX = currentChunkX;
								previousChunkZ = currentChunkZ;
							}
						}
					}
				}


				/* Check for track under the pulse and break if none was found. */
				TrackObj trackUnder = getTrackBelowThis(world);
				if (trackUnder == null)
				{
					break;
				}

				/* Increase the length of the landscape instance. */
				landscape.increaseTotalLength(distancePerTick);

				/*
				 * Ignore any signals or obstacles really close to the front of the train.
				 */
				if (distance < 3.0F)
				{
					continue;
				}

				/*
				 * Search for signals/station magnets/terminating location magnets.
				 * This is done by trying to find a signal magnet underneath this balise pulse, and if one exists, getting the signal from it.
				 */
				TileEntity tile = world.getTileEntity(trackUnder.x, trackUnder.y, trackUnder.z);
				if (tile instanceof ITrackBase)
				{
					ITrackBase trackSection = (ITrackBase)tile;
					if (trackSection.getInstanceOfShape() instanceof ISignalMagnet)
					{
						ISignalMagnet magnet = (ISignalMagnet)trackSection.getInstanceOfShape();
						if (trackSection.getField(ZnD_Straight_0000_0000.INDEX_HASMAGNET, false, Boolean.class))
						{
							/*
							 * Here, the direction of the balise pulse is compared to the direction of the signal magnet. This makes sure that the balise pulse only detects signal magnets that are
							 * facing the 'correct' direction relative to the balise pulse, and not signal magnets that are facing the other way (presumably for signals in the opposite direction).
							 */
							Vec3 magnetDirection;
							switch (trackSection.getOrientation())
							{
								case 0: {
									magnetDirection = Vec3.createVectorHelper(1.0D, 0.0D, 0.0D);
									break;
								}

								case 1: {
									magnetDirection = Vec3.createVectorHelper(0.0D, 0.0D, 1.0D);
									break;
								}

								case 2: {
									magnetDirection = Vec3.createVectorHelper(-1.0D, 0.0D, 0.0D);
									break;
								}

								case 3: {
									magnetDirection = Vec3.createVectorHelper(0.0D, 0.0D, -1.0D);
									break;
								}

								default: {
									magnetDirection = Vec3.createVectorHelper(0.0D, 1.0D, 0.0D);
									break;
								}
							}

							/*
							 * The signal magnet is considered to be facing the same direction as the balise pulse if their angles are less than 45 degrees apart.
							 */
							if (VehicleHelper.getAngle(direction, magnetDirection) < 45.0D)
							{
								boolean foundStation = magnet.getIsStation(trackSection);
								boolean foundTermination = magnet.getIsTermination(trackSection);
								boolean added = false;

								/*
								 * The magnet is linked to a signal.
								 */
								if (magnet.getLinkedSignal(trackSection) != null)
								{
									ISignal signal = magnet.getLinkedSignal(trackSection);

									if (signal.getIndication().getSpeedLimit() >= 0)
									{
										landscape.addLandmark(new Landmark(distance, foundStation, foundTermination, -1.0F, magnet.getLinkedSignal(trackSection).getIndication().getSpeedLimit()));

										added = true;
									}
								}

								/*
								 * The magnet is linked to a speed sign.
								 */
								if (magnet.getTrackSpeed(trackSection) > 0)
								{
									landscape.addLandmark(new Landmark(distance, foundStation, foundTermination, -1.0F, (float)magnet.getTrackSpeed(trackSection)));

									added = true;
								}

								/*
								 * The magnet is either a station or terminating point. In this case, make the MTCS system think there is an obstruction here so it slows down for it.
								 */
								if (!added && (foundStation || foundTermination))
								{
									landscape.addLandmark(new Landmark(distance, foundStation, foundTermination, 0.0F, -1.0F));
								}
							}
						}
					}
				}
			}

			landscape.setChunkList(chunks);
		}

		position = originalPosition;
		direction = originalDirection;

		return landscape;
	}


	/**
	 * Called by signals so they can get a {@link zoranodensha.api.structures.signals.SignalBlockState} instance of the track ahead.
	 * 
	 * @param world - The world instance passed by the signal.
	 * @return - A {@link zoranodensha.api.structures.signals.SignalBlockState} instance containing information about the track ahead of the signal.
	 */
	public SignalBlockState getSignalBlockState(World world)
	{
		SignalBlockState signalBlockState = new SignalBlockState();

		/*
		 * Store the original position and direction so we can revert back to it.
		 */
		Vec3 originalPosition = position;
		Vec3 originalDirection = direction;
		{
			final float maximumDistance = 2048.0F;
			final float distancePerTick = 0.5F;

			/*
			 * Variables to reuse during the loop.
			 */
			int[] cachedSwitchCoordinates = null;
			int cachedSwitchPath = 0;
			Vec3 beginSwitchDirection = Vec3.createVectorHelper(1.0F, 0.0F, 0.0F);
			boolean stillInsideInterlocking = true;

			/*
			 * Initial Position for the debug coordinates.
			 */
			signalBlockState.pathDebug = new ArrayList<Double>();
			signalBlockState.pathDebug.add(originalPosition.xCoord);
			signalBlockState.pathDebug.add(originalPosition.yCoord);
			signalBlockState.pathDebug.add(originalPosition.zCoord);

			/*
			 * Main Loop
			 */
			mainLoop: for (float distance = 0.0F; distance < maximumDistance; distance += distancePerTick)
			{
				/*
				 * Move the balise pulse.
				 */
				movementTick(distancePerTick, world);

				/*
				 * Update the debug path information.
				 */
				if (distance <= 100.0F)
				{
					signalBlockState.pathDebug.add(position.xCoord);
					signalBlockState.pathDebug.add(position.yCoord);
					signalBlockState.pathDebug.add(position.zCoord);
				}


				/* Check for track under the pulse and break if none was found. */
				TrackObj trackUnder = getTrackBelowThis(world);
				if (trackUnder == null)
				{
					signalBlockState.blockLength = distance;
					break;
				}

				/*
				 * We need to figure out the source coordinates of whatever track we're on.
				 */
				int[] trackSourceCoordinates;

				TileEntity tile = world.getTileEntity(trackUnder.x, trackUnder.y, trackUnder.z);
				if (tile instanceof TileEntityTrackBase)
				{
					trackSourceCoordinates = new int[] { tile.xCoord, tile.yCoord, tile.zCoord };
				}
				else if (tile instanceof TileEntityTrackBaseGag)
				{
					trackSourceCoordinates = ((TileEntityTrackBaseGag)tile).getSourceCoords();
				}
				else
				{
					trackSourceCoordinates = null;
				}

				/*
				 * Now do the rest of the tests.
				 */
				if (tile instanceof ITrackBase)
				{
					ITrackBase trackSection = (ITrackBase)tile;

					/*
					 * Check for trains
					 */
					/*
					 * Don't check for trains closer than 3 metres to the signal.
					 * Sometimes trains that are right next to a signal will cause the signal to display STOP, and thus make the train think it's right in front of a red signal.
					 */
					if (distance >= 3.0F)
					{
						AxisAlignedBB mask = AxisAlignedBB.getBoundingBox(position.xCoord - 0.5D, position.yCoord - 1.0D, position.zCoord - 0.5D, position.xCoord + 0.5D, position.yCoord + 2.0D, position.zCoord + 0.5D);
						ArrayList<Entity> collisionCandidates = CollisionHandler.INSTANCE.getCollidingEntities(world, mask, CollisionHandler.selectorTrains);
						if (!collisionCandidates.isEmpty())
						{
							for (Entity train : collisionCandidates)
							{
								if (train.getBoundingBox().intersectsWith(mask))
								{
									if (stillInsideInterlocking)
									{
										signalBlockState.hasTrainInInterlocking = true;
									}

									signalBlockState.hasTrain = true;
									break mainLoop;
								}
							}
						}
					}

					/*
					 * Check for buffers.
					 */
					if (trackSection.getInstanceOfShape() instanceof ZnD_StraightBuffer_0000_0000 || trackSection.getInstanceOfShape() instanceof ZnD_StraightBuffer_1131_1131
							|| trackSection.getInstanceOfShape() instanceof ZnD_StraightBuffer_1843_1843 || trackSection.getInstanceOfShape() instanceof ZnD_StraightBuffer_4500_4500)
					{
						signalBlockState.blockLength = distance;
						signalBlockState.hasBuffer = true;
						break;
					}

					/*
					 * Check for junctions.
					 */
					if (trackSection.getInstanceOfShape() instanceof ZnD_Switch_0000_1131 || trackSection.getInstanceOfShape() instanceof ZnD_Switch_1131_0000)
					{
						TileEntity switchSpecificTileEntity = world.getTileEntity(tile.xCoord, tile.yCoord, tile.zCoord);

						/*
						 * Pulse is entering switch for the first time.
						 */
						if (cachedSwitchCoordinates == null)
						{
							/* Get the details of the situation as the pulse enters the switch - this will be used later to see if the switch is set against the movement of the train. */
							cachedSwitchCoordinates = trackSourceCoordinates;
							cachedSwitchPath = trackSection.getPath();
							beginSwitchDirection = direction;
						}

						/*
						 * Pulse is already on a switch.
						 */
						else
						{
							/* Has the pulse entered a new switch straight away? */
							if (!Arrays.equals(cachedSwitchCoordinates, trackSourceCoordinates))
							{
								/* Check if the pulse has travelled through the switch in the wrong direction. */
								if (cachedSwitchPath == 0)
								{
									if (VehicleHelper.getAngle(direction, beginSwitchDirection) >= 3.0D)
									{
										signalBlockState.blockLength = distance;
										signalBlockState.hasBuffer = false;

										break;
									}
								}
								else if (cachedSwitchPath > 0)
								{
									if (VehicleHelper.getAngle(direction, beginSwitchDirection) < 3.0D)
									{
										signalBlockState.blockLength = distance;
										signalBlockState.hasBuffer = false;

										break;
									}
								}

								/* Take down the details of the new switch. */
								cachedSwitchCoordinates = trackSourceCoordinates;
								cachedSwitchPath = trackSection.getPath();
								beginSwitchDirection = direction;
							}
						}

						/*
						 * Set the signal block state 'diverging route' flag to true, which will cause the signal to show a reduced speed limit.
						 */
						if (trackSection.getPath() != 0)
						{
							signalBlockState.isDivergingRoute = true;
						}
					}

					/*
					 * Balise pulse is not on a switch.
					 */
					else
					{
						/*
						 * Has the balise pulse just left a switch?
						 */
						if (cachedSwitchCoordinates != null)
						{
							/*
							 * Check if the pulse has travelled through the switch in the wrong direction.
							 */
							if (cachedSwitchPath == 0)
							{
								if (VehicleHelper.getAngle(direction, beginSwitchDirection) >= 3.0D)
								{
									signalBlockState.blockLength = distance;
									signalBlockState.hasBuffer = false;

									break;
								}
							}
							if (cachedSwitchPath > 0)
							{
								if (VehicleHelper.getAngle(direction, beginSwitchDirection) < 3.0D)
								{
									signalBlockState.blockLength = distance;
									signalBlockState.hasBuffer = false;

									break;
								}
							}

							cachedSwitchCoordinates = null;
							cachedSwitchPath = 0;
							beginSwitchDirection = Vec3.createVectorHelper(1.0D, 0.0D, 0.0D);
						}
					}


					/*
					 * Check for buffers.
					 */
					if (trackSection.getInstanceOfShape() instanceof ZnD_StraightBuffer_0000_0000 || trackSection.getInstanceOfShape() instanceof ZnD_StraightBuffer_1131_1131
							|| trackSection.getInstanceOfShape() instanceof ZnD_StraightBuffer_1843_1843 || trackSection.getInstanceOfShape() instanceof ZnD_StraightBuffer_4500_4500)
					{
						signalBlockState.blockLength = distance;
						signalBlockState.hasBuffer = true;
						break;
					}


					/*
					 * Balise pulse has reached a signal magnet.
					 */
					if (trackSection.getInstanceOfShape() instanceof ISignalMagnet && distance >= 2.0F)
					{
						ISignalMagnet magnet = (ISignalMagnet)trackSection.getInstanceOfShape();

						if (trackSection.getField(ZnD_Straight_0000_0000.INDEX_HASMAGNET, false, Boolean.class))
						{

							/*
							 * Make sure the magnet is pointing in the same direction as the balise pulse.
							 */
							Vec3 magnetDirection;
							switch (trackSection.getOrientation())
							{
								case 0: {
									magnetDirection = Vec3.createVectorHelper(1.0D, 0.0D, 0.0D);
									break;
								}

								case 1: {
									magnetDirection = Vec3.createVectorHelper(0.0D, 0.0D, 1.0D);
									break;
								}

								case 2: {
									magnetDirection = Vec3.createVectorHelper(-1.0D, 0.0D, 0.0D);
									break;
								}

								case 3: {
									magnetDirection = Vec3.createVectorHelper(0.0D, 0.0D, -1.0D);
									break;
								}

								default: {
									magnetDirection = Vec3.createVectorHelper(0.0D, 1.0D, 0.0D);
									break;
								}
							}

							/*
							 * Check for signals that are in the same direction as the balise pulse.
							 */
							if (VehicleHelper.getAngle(direction, magnetDirection) < 45.0D)
							{
								if (magnet.getLinkedSignal(trackSection) != null)
								{
									if (!magnet.getLinkedSignal(trackSection).isRepeater())
									{
										signalBlockState.nextSignal = magnet.getLinkedSignal(trackSection);
										signalBlockState.blockLength = distance;

										break;
									}
									else
									{
										if (magnet.getLinkedSignal(trackSection).getIndication().getBlocksFree() >= 1)
										{
											signalBlockState.nextSignal = magnet.getLinkedSignal(trackSection);
											signalBlockState.blockLength = distance;

											break;
										}
									}
								}
							}

							/*
							 * Also check for signals that are facing away from the balise pulse.
							 */
							else
							{
								if (magnet.getLinkedSignal(trackSection) != null)
								{
									/*
									 * A signal facing the other way marks the end of this signal's 'interlocked' area. We need to remember that.
									 */
									stillInsideInterlocking = false;


									if (magnet.getLinkedSignal(trackSection).isOneWay())
									{
										/*
										 * At this point the balise pulse has reached a signal facing the other way that identifies as a 'one way' signal, so we aren't allowed to go any further.
										 */
										signalBlockState.blockLength = distance;
										signalBlockState.hasBuffer = false;

										break;
									}
								}
							}

						}
					}
				}
			}

		}

		position = originalPosition;
		direction = originalDirection;

		return signalBlockState;
	}


	/**
	 * Called to perform a 'movement tick' to this balise pulse. When this is called, the balise pulse will move forward by the distance specified in the arguments of this method (distance) and
	 * automatically snap itself onto any tracks below it as it goes along.
	 * 
	 * @param distance - How far (in metres) to move this balise pulse in this movement tick.
	 * @param world - A world instance that this balise pulse will use to read the track information beneath it.
	 */
	private void movementTick(float distance, World world)
	{
		previousDirection = direction;

		PositionStackEntry previousPos = new PositionStackEntry(position.xCoord, position.yCoord, position.zCoord);
		previousPos.yaw = (float)(-Math.atan2(direction.zCoord, direction.xCoord) * VehicleHelper.DEG_FACTOR);
		PositionStackEntry currentPos = previousPos.copy();


		/*
		 * Actually moving the pulse.
		 */
		currentPos.offset(direction.xCoord * distance, direction.yCoord * distance, direction.zCoord * distance);

		/*
		 * Detecting the track below the pulse.
		 */
		TrackObj trackUnder = getTrackBelowThis(world);

		/*
		 * We're going to skip gravitation of the pulse instance because the logic requires a bogie instance.
		 */

		/*
		 * Positioning on track
		 */
		boolean isOnTrack = false;
		{
			if (trackUnder != null)
			{
				PositionStackEntry newPos = currentPos.copy();

				isOnTrack = trackUnder.clampToTrack(null, newPos);

				if (isOnTrack)
				{
					currentPos.setPosition(newPos.x, newPos.y, newPos.z);
				}
			}
		}

		/*
		 * Update new position and direction
		 */
		position = Vec3.createVectorHelper(currentPos.x, currentPos.y, currentPos.z);
		direction = Vec3.createVectorHelper(currentPos.x - previousPos.x, currentPos.y - previousPos.y, currentPos.z - previousPos.z).normalize();
	}


	/**
	 * <p>
	 * Helper method which gets a {@link zoranodensha.api.vehicles.util.TrackObj} instance of the track currently underneath this Balise Pulse's location.
	 * </p>
	 * <p>
	 * This method will return {@code null} if there is no track found.
	 * </p>
	 * 
	 * @param world - The current world object which is used to access the blocks underneath this balise pulse.
	 * @return - A {@link zoranodensha.api.vehicles.util.TrackObj} instance if a track is found underneath this balise pulse, but if no tracks are found, {@code null}.
	 */
	private TrackObj getTrackBelowThis(World world)
	{
		TrackObj trackUnder = null;

		int x = MathHelper.floor_double(position.xCoord);
		int y = MathHelper.floor_double(position.yCoord + 0.125D);
		int z = MathHelper.floor_double(position.zCoord);

		for (int i = 0; i <= 1; i++)
		{
			trackUnder = TrackObj.isTrack(world, x, y - i, z, null);

			if (trackUnder != null)
				break;
		}

		return trackUnder;
	}

}
