package zoranodensha.api.structures.signals;

import net.minecraft.nbt.NBTTagCompound;



/**
 * <h1>SignalIndication</h1>
 * <p>
 * A class containing information conveyed by a signal.
 * </p>
 * 
 * @author Jaffa
 */
public class SignalIndication
{

	private static final String NBTKEY_BLOCKSFREE = "_blocksFree";
	private static final String NBTKEY_NEXTSPEEDLIMIT = "_nextSpeedLimit";
	private static final String NBTKEY_SPEEDLIMIT = "_speedLimit";

	/**
	 * <p>
	 * The number of signal blocks that are unoccupied in front of this signal.
	 * </p>
	 */
	private int blocksFree;


	/**
	 * <p>
	 * The speed limit of the next signal.
	 * </p>
	 */
	private float nextSpeedLimit;


	/**
	 * <p>
	 * The speed limit of this signal.
	 * </p>
	 */
	private float speedLimit;



	/**
	 * <p>
	 * Initialises a new instance of the {@link zoranodensha.api.structures.signals.SignalIndication} class.
	 * </p>
	 * <p>
	 * This constructor will populate all fields with fail-safe values.
	 * </p>
	 */
	public SignalIndication()
	{
		blocksFree = 0;
		nextSpeedLimit = 0.0F;
		speedLimit = 0.0F;
	}


	/**
	 * <p>
	 * Initialises a new instance of the {@link zoranodensha.api.structures.signals.SignalIndication} class.
	 * </p>
	 * 
	 * @param blocksFree - How many blocks are unoccupied in front of the signal.
	 * @param speedLimit - The speed limit at this current signal.
	 * @param nextSpeedLimit - The speed limit of the upcoming signal.
	 */
	public SignalIndication(int blocksFree, float speedLimit, float nextSpeedLimit)
	{
		this.blocksFree = blocksFree;
		this.nextSpeedLimit = nextSpeedLimit;
		this.speedLimit = speedLimit;
	}


	/**
	 * @return - The number of unoccupied signal blocks beyond this signal.
	 */
	public int getBlocksFree()
	{
		return blocksFree;
	}


	/**
	 * @return - The speed limit at the upcoming signal.
	 */
	public float getNextSpeedLimit()
	{
		return nextSpeedLimit;
	}


	/**
	 * @return - The speed limit at this signal.
	 */
	public float getSpeedLimit()
	{
		return speedLimit;
	}


	/**
	 * @return - Whether this indication requires the train to stop at the signal.
	 */
	public boolean isStopIndication()
	{
		if (blocksFree <= 0)
		{
			return true;
		}

		if (speedLimit == 0)
		{
			return true;
		}

		return false;
	}


	/**
	 * <p>
	 * Reads a {@link zoranodensha.api.structures.signals.SignalIndication} instance from the specified NBT Tag Compound.
	 * </p>
	 * 
	 * @param nbt - The {@link net.minecraft.nbt.NBTTagCompound} to read from.
	 * @param key - The {@code String} key to look under when searching for previously saved data.
	 * @return - A previously saved {@link SignalIndication}, or if there is data missing from the NBT, an instance of {@link SignalIndication} which contains fail-safe values.
	 */
	public static SignalIndication readFromNBT(NBTTagCompound nbt, String key)
	{
		SignalIndication retreivedIndication = new SignalIndication();

		if (nbt.hasKey(key + NBTKEY_BLOCKSFREE))
			retreivedIndication.blocksFree = nbt.getInteger(key + NBTKEY_BLOCKSFREE);

		if (nbt.hasKey(key + NBTKEY_NEXTSPEEDLIMIT))
			retreivedIndication.nextSpeedLimit = nbt.getFloat(key + NBTKEY_NEXTSPEEDLIMIT);

		if (nbt.hasKey(key + NBTKEY_SPEEDLIMIT))
			retreivedIndication.speedLimit = nbt.getFloat(key + NBTKEY_SPEEDLIMIT);

		return retreivedIndication;
	}


	/**
	 * <p>
	 * Writes the information in this class into the specified NBT Tag Compound.
	 * </p>
	 * 
	 * @param nbt - The {@link net.minecraft.nbt.NBTTagCompound} instance to save to.
	 * @param key - The {@code String} key that this instance will be saved under.
	 */
	public void writeToNBT(NBTTagCompound nbt, String key)
	{
		nbt.setInteger(key + NBTKEY_BLOCKSFREE, blocksFree);
		nbt.setFloat(key + NBTKEY_NEXTSPEEDLIMIT, nextSpeedLimit);
		nbt.setFloat(key + NBTKEY_SPEEDLIMIT, speedLimit);
	}

}
