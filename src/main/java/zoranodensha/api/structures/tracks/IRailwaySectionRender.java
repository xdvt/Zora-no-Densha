package zoranodensha.api.structures.tracks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;



/**
 * An interface that has to be implemented by all classes rendering {@link IRailwaySection} instances.
 */
@SideOnly(Side.CLIENT)
public interface IRailwaySectionRender
{
	/**
	 * Render a track model.
	 *
	 * @param detail - The level of detail to render: 0 = No detail/ Very basic model; 1 = Medium detail; 2 = Full detail.
	 * @param gagTrack - 0 if actual track, 1 if non-obstructed track, 2 if obstructed track.
	 * @param section - The {@link IRailwaySection} instance of this track.
	 * @param trackBase - The {@link ITrackBase} instance of the track.
	 * @param x - The x coordinate on the screen.
	 * @param y - The y coordinate on the screen.
	 * @param z - The z coordinate on the screen.
	 * @param f - The partial tick time since the last render tick.
	 */
	void renderTrack(int detail, int gagTrack, IRailwaySection section, ITrackBase trackBase, float x, float y, float z, float f);
}
