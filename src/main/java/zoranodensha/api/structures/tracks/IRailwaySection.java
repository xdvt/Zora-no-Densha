package zoranodensha.api.structures.tracks;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;



/**
 * An interface for all Railway Section instances. Implement this to add a new Railway Section to the game.
 */
public interface IRailwaySection
{
	/**
	 * Returns true if this IRailwaySection can stay at its position. Passed coordinates are those of the source tile. Breaks the track if false returns.
	 */
	boolean getCanStay(World world, int x, int y, int z, int rotation, EDirection dir);

	/**
	 * Return this RailwaySection instance's EShape name. Used by e.g. item rendering.
	 */
	EShape getEShapeType();

	/**
	 * Return all relative coordinates for gag blocks of this RailwaySection in a List.
	 */
	List<int[]> getGagBlocks(EDirection dir);

	/**
	 * Return the length of this RailwaySection.
	 */
	int getLength();

	/**
	 * Return the name of this RailwaySection instance.
	 */
	String getName();

	/**
	 * Return the Path instance corresponding to the given path number.
	 *
	 * @param path - The path number
	 */
	Path getPath(int path);

	/**
	 * Calculate the position of the given Entity vehicle and return it as a double array. See
	 * {@link zoranodensha.api.structures.tracks.ITrackBase#getPositionOnTrack(Object, double, double, double, float, float)} getPositionOnTrack()} for further information. Return {@code null} if this
	 * IRailwaySection instance does not calculate position offset on its own and/ or if default position/ rotation calculation shall be applied. It is recommended to use the local X-coordinate in
	 * combination with a function/ Path, whereas a local Z-coordinate can be used to compare the actual position of a vehicle in order to differentiate between different paths (e.g. on a switch or
	 * cross track).
	 *
	 * @param trackBase - The ITrackBase instance of this track.
	 * @param vehX - The vehicle's X-position
	 * @param vehY - The vehicle's Y-position
	 * @param vehZ - The vehicle's Z-position
	 * @param vehicle - Vehicle object asking for the new position, may be {@code null}.
	 * @param tileEntity - The TileEntity of the ITrackBase instance.
	 * @param localX - The local X-coordinate of the vehicle. Usually within {@code -0.5D} and {@code length + 0.5D}.
	 * @param localZ - The local Z-coordinate of the vehicle. May be negative.
	 * @return Null if this IRailwaySection instance does not inherit own calculation. Otherwise a multi-dimensional array of double values where each array contains three values for position/
	 *         rotation, respectively.
	 */
	double[][] getPositionOnTrack(ITrackBase trackBase, double vehX, double vehY, double vehZ, @Nullable Object vehicle, TileEntity tileEntity, double localX, double localZ);

	/**
	 * Return the {@link IRailwaySectionRender} class for this IRailwaySection instance.
	 */
	@SideOnly(Side.CLIENT)
	IRailwaySectionRender getRender();

	/**
	 * Return a RenderBoundingBox for this instance.
	 */
	@SideOnly(Side.CLIENT)
	AxisAlignedBB getRenderBoundingBox(double x, double y, double z);

	/**
	 * Return a List of Integers containing all valid path numbers. The default path number (which is 0) is a prerequisite, as the default path will be written to NBT.
	 */
	List<Integer> getValidPaths();

	/**
	 * Called to initialize and return the Object Map (i.e. this track's field Map).
	 */
	Map<Integer, Object> initializeFields();

	/**
	 * Called to initialize and return the given ITrackBase track instance as a TileEntity instance. If the TileEntity does not implement ITrackBase, an IllegalArgumentException will be thrown.
	 */
	TileEntity initializeTrack(ITrackBase trackBase);

	/**
	 * Return true if this IRailwaySection is a maintenance track type.
	 */
	boolean isMaintenanceTrack();

	/**
	 * Called to read custom data from the given NBTTagCompound.
	 */
	void onReadFromNBT(NBTTagCompound nbt, ITrackBase trackBase);

	/**
	 * Called when this IRailwaySection name is (partially) destroyed. This is a method handle sent by Block's removedByPlayer(...) method.
	 *
	 * @param tileEntity - The ITrackBase we are dealing with. Use this to access the World and coordinate fields.
	 * @param player - The player trying to edit this block.
	 * @return A List of ItemStack instances to drop. Return null to suppress a block update or add a null ItemStack to disassemble the track by one piece (for example to remove fastening and leave
	 *         track bed).
	 */
	List<ItemStack> onTrackDestroyed(TileEntity tileEntity, @Nullable EntityPlayer player);

	/**
	 * Called when a player tries to place/ extend/ repair a track of this IRailwaySection name.
	 *
	 * @param trackBase - The ITrackBase instance of the track.
	 * @param player - The player trying to execute this action.
	 * @param isRemote - True if this is a client world.
	 * @return True if successful.
	 */
	boolean onTrackWorkedOn(ITrackBase trackBase, EntityPlayer player, boolean isRemote);

	/**
	 * Called when a track of this IRailwaySection name is about to be extended/ repaired.
	 *
	 * @param trackBase - The ITrackBase instance of the track.
	 * @param inventory - The inventory of the Entity trying to execute this action.
	 * @param isRemote - True if this is a client world.
	 * @return True if successful.
	 */
	boolean onTrackWorkedOn(ITrackBase trackBase, IInventory inventory, boolean isRemote);

	/**
	 * Called by the track's onUpdate method. Use this to move switch tongues, update/ decay stress, ...
	 *
	 * @param world - This track's World object.
	 * @param x - This track's x-coordinate.
	 * @param y - This track's y-coordinate.
	 * @param z - This track's z-coordinate.
	 * @param trackBase - The ITrackBase instance of this track.
	 * @param params - The Object Map containing all arguments of this IRailwaySection that are handled internally.
	 * @return The altered Map of Objects that was passed in as argument.
	 */
	Map<Integer, Object> onUpdate(World world, int x, int y, int z, ITrackBase trackBase, Map<Integer, Object> params);

	/**
	 * Called to write custom data in the given NBTTagCompound.
	 */
	void onWriteToNBT(NBTTagCompound nbt, ITrackBase trackBase);
}
