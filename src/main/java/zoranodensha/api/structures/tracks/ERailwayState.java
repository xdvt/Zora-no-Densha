package zoranodensha.api.structures.tracks;

/**
 * An enum of all states a track might have.
 */
public enum ERailwayState
{
	/** The track has a track bed. */
	TRACKBED(0),
	/** The track has a track bed and fastening. */
	FASTENED(1),
	/** The track has a track bed, fastening, and rails. */
	FINISHED(2),
	/** The track has a track bed, fastening, rails and crossing plates. */
	PLATED(3),
	/** The track is outlined as a building guide. */
	BUILDINGGUIDE(4);

	private final int state;



	ERailwayState(int state)
	{
		this.state = state;
	}


	public boolean isFinished()
	{
		switch (this)
		{
			default:
				return false;
			case FINISHED:
			case PLATED:
				return true;
		}
	}

	public int toInt()
	{
		return state;
	}

	@Override
	public String toString()
	{
		switch (this)
		{
			default:
				return "<Error>";
			case TRACKBED:
				return "Trackbed";
			case FASTENED:
				return "Fastened";
			case FINISHED:
				return "Finished";
			case PLATED:
				return "Plated";
			case BUILDINGGUIDE:
				return "Building Guide";
		}
	}

	public static ERailwayState toState(int i)
	{
		switch (i)
		{
			default:
				return null;
			case 0:
				return TRACKBED;
			case 1:
				return FASTENED;
			case 2:
				return FINISHED;
			case 3:
				return PLATED;
			case 4:
				return BUILDINGGUIDE;
		}
	}
}
