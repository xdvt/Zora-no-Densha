package zoranodensha.api.structures.tracks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import zoranodensha.api.items.IItemTrackPart;
import zoranodensha.trackpack.common.section.ARailwaySectionTrackPackSignalMagnet;
import zoranodensha.trackpack.common.section.ZnD_Straight_0000_0000;



public abstract class RailwaySectionBase implements IRailwaySection
{
	protected int fastenings;
	protected int length;
	protected int plates;
	protected int rails;
	protected int trackbeds;
	protected String name;
	protected Path[] paths;



	public RailwaySectionBase(int trackbeds, int fastenings, int rails, int plates, String name, int length, Path[] paths)
	{
		this.fastenings = fastenings;
		this.length = length;
		this.plates = plates;
		this.rails = rails;
		this.trackbeds = trackbeds;
		this.name = name;
		this.paths = paths;
	}

	@Override
	public boolean getCanStay(World world, int x, int y, int z, int rotation, EDirection dir)
	{
		List<int[]> list = getGagBlocksWithRotation(0, rotation, getGagBlocks(dir));
		Iterator<int[]> itera = list.iterator();
		while (itera.hasNext())
		{
			int[] i = itera.next();
			if (!world.getBlock(x + i[0], y + i[1] - 1, z + i[2]).isSideSolid(world, x + i[0], y + i[1] - 1, z + i[2], ForgeDirection.UP))
			{
				return false;
			}
		}
		return true;
	}


	@Override
	public int getLength()
	{
		return length;
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public Path getPath(int i)
	{
		return paths[i];
	}

	@Override
	public double[][] getPositionOnTrack(ITrackBase trackBase, double vehX, double vehY, double vehZ, @Nullable Object vehicle, TileEntity tileEntity, double localX, double localZ)
	{
		return null;
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox(double x, double y, double z)
	{
		return AxisAlignedBB.getBoundingBox(x - length, y, z - length, x + length + 1.0D, y + 1.0D, z + length + 1.0D);
	}

	@Override
	public List<Integer> getValidPaths()
	{
		List<Integer> paths = new ArrayList<Integer>();
		paths.add(0);
		return paths;
	}

	@Override
	public Map<Integer, Object> initializeFields()
	{
		Map<Integer, Object> map = new HashMap<Integer, Object>();
		return map;
	}

	@Override
	public TileEntity initializeTrack(ITrackBase trackBase)
	{
		trackBase.setField(0, (getEShapeType() == EShape.CROSS || getEShapeType() == EShape.SWITCH) ? 2 : 0);
		trackBase.setField(1, 0);
		trackBase.setField(10, null);
		return (TileEntity)trackBase;
	}

	@Override
	public void onReadFromNBT(NBTTagCompound nbt, ITrackBase trackBase)
	{
		for (int i = 0; i < 2; ++i)
		{
			String s = nbt.getString("FieldDec_" + i);
			if ("int".equals(s))
			{
				trackBase.setField(i, nbt.getInteger("Field_" + i));
			}
		}
	}

	@Override
	public boolean onTrackWorkedOn(ITrackBase trackBase, EntityPlayer player, boolean isRemote)
	{
		ItemStack currentItem = player.getHeldItem();
		if (currentItem == null)
		{
			return false;
		}

		boolean flag0 = player.capabilities.isCreativeMode;
		int damage = trackBase.getField(1, 0, Integer.class);

		if (currentItem.getItem() instanceof IItemTrackPart)
		{
			if (((IItemTrackPart)currentItem.getItem()).type(currentItem) != null)
			{
				switch (((IItemTrackPart)currentItem.getItem()).type(currentItem))
				{
					case TRACKBED: {
						if ((trackBase.getStateOfShape().equals(ERailwayState.BUILDINGGUIDE) || damage % 2 != 0) && trackbeds > 0)
						{
							if (flag0 || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), trackbeds, currentItem.getItemDamage()), 0))
							{
								if (damage % 2 != 0)
								{
									if (!isRemote && (trackBase.getField(0, 0, Integer.class)) % 2 == currentItem.getItemDamage())
									{
										trackBase.setField(1, damage - 1);
										getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), trackbeds, currentItem.getItemDamage()), 1);
									}
								}
								else if (!isRemote)
								{
									trackBase.setField(0, currentItem.getItemDamage() + trackBase.getField(0, 0, Integer.class));
									trackBase.setStateOfShape(ERailwayState.TRACKBED);
									if (!flag0)
									{
										getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), trackbeds, currentItem.getItemDamage()), 1);
									}
								}
								return true;
							}
						}
						break;
					}

					case RAIL: {
						if ((trackBase.getStateOfShape().equals(ERailwayState.FASTENED) || damage > 1) && rails > 0)
						{
							if (flag0 || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), rails, 2), 1))
							{
								if (!isRemote)
								{
									if (damage > 1)
									{
										trackBase.setField(1, damage - 2);
									}
									else
									{
										trackBase.setStateOfShape(ERailwayState.FINISHED);
									}
								}
								return true;
							}
						}
						break;
					}

					case PLATE: {
						if (trackBase.getStateOfShape().equals(ERailwayState.FINISHED) && plates > 0)
						{
							if (flag0 || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), plates, 3), 1))
							{
								if (!isRemote)
								{
									trackBase.setStateOfShape(ERailwayState.PLATED);
								}
								return true;
							}
						}
						break;
					}

					case FASTENING: {
						if ((trackBase.getStateOfShape().equals(ERailwayState.TRACKBED) && damage == 0) && fastenings > 0)
						{
							if (flag0 || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), fastenings, 4), 1))
							{
								if (!isRemote)
								{
									trackBase.setStateOfShape(ERailwayState.FASTENED);
								}
								return true;
							}
						}
						break;
					}

					case MAGNET: {
						if (trackBase.getInstanceOfShape() instanceof ARailwaySectionTrackPackSignalMagnet)
						{
							if (trackBase.getStateOfShape().equals(ERailwayState.FINISHED))
							{
								if (!trackBase.getField(ARailwaySectionTrackPackSignalMagnet.INDEX_HASMAGNET, false, Boolean.class))
								{
									if (flag0 || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), 1, 7), 1))
									{
										if (!isRemote)
										{
											trackBase.setField(ARailwaySectionTrackPackSignalMagnet.INDEX_HASMAGNET, true);
										}

										return true;
									}
								}
							}
						}

						break;
					}
				}
			}
		}
		return false;
	}

	@Override
	public boolean onTrackWorkedOn(ITrackBase trackBase, IInventory inventory, boolean isRemote)
	{
		return false;
	}

	@Override
	public Map<Integer, Object> onUpdate(World world, int x, int y, int z, ITrackBase trackBase, Map<Integer, Object> params)
	{
		return params;
	}

	@Override
	public void onWriteToNBT(NBTTagCompound nbt, ITrackBase trackBase)
	{
		for (int i = 0; i < 2; ++i)
		{
			nbt.setString("FieldDec_" + i, "int");
			nbt.setInteger("Field_" + i, trackBase.getField(i, 0, Integer.class));
		}

		for (int i = 0; i < paths.length; ++i)
		{
			paths[i].writeToNBT(nbt);
		}
	}

	/**
	 * Rotates the given List of coordinate triplets to match the given new
	 * rotation.
	 * <p>
	 * Rotation flags as following:<br>
	 * 0 = North<br>
	 * 1 = East<br>
	 * 2 = South<br>
	 * 3 = West
	 * <p>
	 * This method only supports calculation on the x- and z-axis.
	 */
	public static List<int[]> getGagBlocksWithRotation(int rotationOld, int rotationNew, List<int[]> coordinates)
	{
		List<int[]> newCoords = new ArrayList<int[]>();
		Iterator<int[]> itera = coordinates.iterator();
		int rotation = rotationOld > rotationNew ? (rotationOld + rotationNew) % 4 : 4 - ((rotationOld + rotationNew) % 4);

		while (itera.hasNext())
		{
			int[] i = itera.next();
			int x;
			int z;

			switch (rotation)
			{
				default:
					x = i[0];
					z = i[2];
					break;
				case 1:
					x = i[2];
					z = -i[0];
					break;
				case 2:
					x = -i[0];
					z = -i[2];
					break;
				case 3:
					x = -i[2];
					z = i[0];
					break;
			}

			newCoords.add(i.length == 4 ? new int[] { x, i[1], z, i[3] } : new int[] { x, i[1], z });
		}
		return newCoords;
	}

	/**
	 * Returns true if the given ItemStack exists in the given inventory, counting
	 * all present item stacks together. Flag 0 will check whether the itemStack is
	 * present and return true if so, Flag 1 will behave as flag 0 but will also
	 * remove the item stack.
	 */
	public static boolean getInventorySufficientStack(IInventory inventory, ItemStack itemStack, int flag)
	{
		if (inventory != null && itemStack != null && flag > -1)
		{
			int count = 0;

			for (int i = 0; i < inventory.getSizeInventory(); ++i)
			{
				ItemStack itemStack0 = inventory.getStackInSlot(i);
				if (itemStack0 != null && itemStack0.getItem().equals(itemStack.getItem()) && itemStack0.getItemDamage() == itemStack.getItemDamage())
				{
					count += itemStack0.stackSize;
				}
			}

			if (count >= itemStack.stackSize && flag < 2)
			{
				if (flag == 0)
				{
					return true;
				}

				int max = itemStack.stackSize;

				for (int i = 0; i < inventory.getSizeInventory(); ++i)
				{
					ItemStack itemStack0 = inventory.getStackInSlot(i);
					if (itemStack0 != null && itemStack0.getItem().equals(itemStack.getItem()) && itemStack0.getItemDamage() == itemStack.getItemDamage())
					{
						if (max >= itemStack0.stackSize)
						{
							inventory.setInventorySlotContents(i, null);
							max -= itemStack0.stackSize;

							if (max == 0)
							{
								return true;
							}
						}
						else
						{
							itemStack0.stackSize -= max;
							return true;
						}
					}
				}
			}
		}
		return false;
	}
}