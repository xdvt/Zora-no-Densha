package zoranodensha.api.structures;

import java.util.ArrayList;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import zoranodensha.api.structures.tracks.EDirection;



/**
 * The interface that has to be extended in order to add content to the EngineerTable of Zora no Densha.
 */
public interface IEngineerTableExtension
{
	/**
	 * Called to retrieve all ingredients for internal registration to the Engineer's Table registry.
	 */
	ArrayList<EngineerTableIngredientHelper> registerIngredients();

	/**
	 * Called to retrieve all recipes for internal registration to the Engineer's Table registry.
	 */
	ArrayList<EngineerTableRecipeHelper> registerRecipes();



	/**
	 * A helper class to pack ingredient data for registration at Zora no Densha.
	 */
	final class EngineerTableIngredientHelper
	{
		/** The ItemStack instance to register. */
		public final ItemStack itemStack;
		/** The name of this ingredient. */
		public final String name;
		/** The page(s) where the ingredient is supposed to appear. */
		public final EEngineerTableTab[] types;



		/**
		 * Pack an Engineer's Table ingredient for registration at Zora no Densha.
		 *
		 * @param item - The ItemStack instance to register
		 * @param type - The name of the registered ingredient; Used to avoid multiple registration on a single ingredient
		 * @param tabs - The page(s) where the ingredient is supposed to appear
		 */
		public EngineerTableIngredientHelper(Item item, String type, EEngineerTableTab... tabs)
		{
			this(new ItemStack(item, 0, 0), type, tabs);
		}

		/**
		 * Pack an Engineer's Table ingredient for registration at Zora no Densha.
		 *
		 * @param stack - The ItemStack instance to register
		 * @param type - The name of the registered ingredient; Used to avoid multiple registration on a single ingredient
		 * @param tabs - The page(s) where the ingredient is supposed to appear
		 */
		public EngineerTableIngredientHelper(ItemStack stack, String type, EEngineerTableTab... tabs)
		{
			itemStack = stack;
			name = type;
			types = tabs;
		}
	}

	/**
	 * A helper class to pack EngineerTableRecipe data for registration at Zora no Densha.
	 */
	final class EngineerTableRecipeHelper
	{
		/** The page where the ingredients appear at. */
		public final EEngineerTableTab type;
		/** The resulting IRailwaySection's name. */
		public final String result;
		/** The EDirection the resulting IRailwaySection will face. */
		public final EDirection dir;
		/** The recipe matrix. */
		public final String[] matrices;
		/** The Object declaration of the matrix' indices. */
		public final Object[] indices;



		/**
		 * Pack an EngineerTable recipe for registration at Zora no Densha.
		 *
		 * @param type - The ingredient's {@link EEngineerTableTab}
		 * @param result - The name of the resulting {@link zoranodensha.api.structures.tracks.IRailwaySection IRailwaySection}
		 * @param dir - The {@link EDirection} the resulting IRailwaySection will face
		 * @param matrices - The recipe matrix; An array of Strings with five places each. Use like Minecraft's recipes. Fill blank places with whitespace (i.e. space bar)
		 * @param indices - The indices used in the matrix with their corresponding ItemStack representation
		 */
		public EngineerTableRecipeHelper(EEngineerTableTab type, String result, EDirection dir, String[] matrices, Object... indices)
		{
			this.type = type;
			this.result = result;
			this.dir = dir;
			this.matrices = matrices;
			this.indices = indices;
		}
	}
}
