package zoranodensha.api.util;


/**
 * <p>
 * A 3D vector (x, y, z) where the values are integers.
 * </p>
 * 
 * @author Jaffa
 */
public class Vec3i
{
	/**
	 * Vector X Component
	 */
	public int x;
	/**
	 * Vector Y Component
	 */
	public int y;
	/**
	 * Vector Z Component
	 */
	public int z;



	/**
	 * Creates a new 4D integer-based vector, with all zero values.
	 */
	public Vec3i()
	{
		this(0, 0, 0);
	}

	/**
	 * Creates a new 4D integer-based vector, with all values initialised as the supplied parameter.
	 * 
	 * @param value - All values of this vector will be initialised with this value.
	 */
	public Vec3i(int value)
	{
		this(value, value, value);
	}

	/**
	 * Creates a new 4D integer-based vector, with values initialised as the supplied arguments.
	 * 
	 * @param x - X Component
	 * @param y - Y Component
	 * @param z - Z Component
	 * @param w - W Component
	 */
	public Vec3i(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}
}
