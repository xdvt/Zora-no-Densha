package zoranodensha.api.util;

/**
 * An enum used to declare the NBT write/ read call type. I.e. only write/ read data relevant to the given type.
 * 
 * @author Leshuwa Kaiheiwa
 */
public enum ETagCall
{
	/**
	 * Write/ read a vehicle's ItemStack NBT data. Used when interacting with a vehicle's item.
	 */
	ITEM,
	/**
	 * Write/ read NBT data for a synchronisation packet. Used when a synchronisation packet is sent (usually from server to client).
	 */
	PACKET,
	/**
	 * Write/ read NBT data when saving/ loading a world or chunk. Used when the world or a chunk is saved or loaded.
	 */
	SAVE
}
