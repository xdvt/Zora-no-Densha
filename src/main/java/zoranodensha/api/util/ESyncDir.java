package zoranodensha.api.util;

/**
 * Enum values that indicate the direction of data synchronisation between client and server.
 * 
 * @author Leshuwa Kaiheiwa
 */
public enum ESyncDir
{
	/** Don't synchronise this value. */
	NOSYNC,
	/** Synchronise to clients. */
	CLIENT,
	/** Synchronise to the server. */
	SERVER,
	/** Synchronise bi-directionally. */
	BOTH;


	/**
	 * @return {@code true} if the given direction matches this value's direction.
	 */
	public boolean getMatchesDirection(ESyncDir syncDir)
	{
		if (this == syncDir)
		{
			return true;
		}
		
		switch (this)
		{
			case NOSYNC:
				return false;
			
			case BOTH:
				return (syncDir != NOSYNC);
			
			default:
				return (syncDir == BOTH);
		}
	}
	
	/**
	 * @return {@code true} if this direction indicates synchronisation to client/ remote worlds.
	 */
	public boolean toClient()
	{
		return (this == CLIENT || this == BOTH);
	}

	/**
	 * @return {@code true} if this direction indicates synchronisation to server/ non-remote worlds.
	 */
	public boolean toServer()
	{
		return (this == SERVER || this == BOTH);
	}
}
