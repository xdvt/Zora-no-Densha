package zoranodensha.signals.common;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.signals.ISpeedSign;
import zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign;



/**
 * An abstract implementation of a speed sign class. This type should be used as a base for any custom speed signs unless you want to write the class from scratch whilst implementing
 * {@link ISpeedSign}.
 * 
 * @author Jaffa
 */
public abstract class ASpeedSign implements ISpeedSign
{
	/**
	 * This key is used when saving or reading the {@link ASpeedSign#speedLimit} field.
	 */
	protected static final String NBTKEY_SPEEDLIMIT = "speedLimit";
	/**
	 * This key is used when saving or reading the {@link ASpeedSign#offset} field.
	 */
	protected static final String NBTKEY_OFFSET = "offset";

	/**
	 * The Minecraft tile entity that this speed sign instance points to.
	 */
	protected TileEntitySpeedSign speedSignTile;

	/**
	 * The number that this sign will display, in {@code km/h}.
	 */
	protected int speedLimit;

	/**
	 * The physical offset of this speed sign. {@code -1} for a left offset, {@code 0} for a centre offset (no offset), {@code +1} for a right offset.
	 */
	protected int offset;

	/**
	 * The unique unlocalised name of this speed sign used when reading and writing from the NBT system and registering this speed sign in the registry.
	 */
	protected String name;



	/**
	 * Initialises a new instance of the {@link ASpeedSign} class. A default speed limit of {@code 40 km/h} will be assigned in this constructor.
	 * 
	 * @param name - The unique unlocalised name of this speed sign which is used when reading and writing from the NBT system and when registering the speed sign in the registry.
	 */
	public ASpeedSign(String name)
	{
		this.name = name;

		/*
		 * Default speed limit is 40.
		 */
		setSpeedLimit(40);

		/*
		 * Default offset is +1 (to the right).
		 */
		setOffset(1);
	}


	@Override
	public int getMetaData()
	{
		return speedSignTile != null ? speedSignTile.getBlockMetadata() : 0;
	}


	@Override
	public String getName()
	{
		return this.name;
	}


	@Override
	public int getSpeedLimit()
	{
		return this.speedLimit;
	}


	@Override
	public TileEntity getTileEntity()
	{
		return this.speedSignTile;
	}


	@Override
	public int getX()
	{
		return speedSignTile.xCoord;
	}


	@Override
	public int getY()
	{
		return speedSignTile.yCoord;
	}


	@Override
	public int getZ()
	{
		return speedSignTile.zCoord;
	}


	@Override
	public void markForUpdate()
	{
		if (speedSignTile != null)
		{
			speedSignTile.markDirty();

			if (speedSignTile.hasWorldObj())
			{
				speedSignTile.getWorldObj().markBlockForUpdate(getX(), getY(), getZ());
			}
		}
	}


	@Override
	public void onTileEntityChange(TileEntity tile)
	{
		if (tile instanceof TileEntitySpeedSign)
		{
			this.speedSignTile = (TileEntitySpeedSign)tile;
			markForUpdate();
		}
	}


	@Override
	public void onUpdate()
	{
		;
	}


	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		setSpeedLimit(nbt.getInteger(NBTKEY_SPEEDLIMIT));
		setOffset(nbt.getInteger(NBTKEY_OFFSET));
	}


	/**
	 * Called to update this speed sign's physical offset.
	 */
	public void setOffset(int newOffset)
	{
		/*
		 * If the value is different, change it and update the tile entity.
		 */
		if (newOffset != this.offset)
		{
			this.offset = newOffset;
			markForUpdate();
		}
		else
		{
			return;
		}
	}


	@Override
	public void setSpeedLimit(int newSpeedLimit)
	{
		if (newSpeedLimit < 0)
		{
			newSpeedLimit = 0;
		}

		/*
		 * Only mark this tile entity for an update if the number has actually changed, to avoid unnecessary block updates.
		 */
		if (newSpeedLimit != this.speedLimit)
		{
			this.speedLimit = newSpeedLimit;
			markForUpdate();
		}
		else
		{
			return;
		}
	}


	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		if (!speedSignTile.hasWorldObj())
		{
			return;
		}

		nbt.setInteger(NBTKEY_SPEEDLIMIT, getSpeedLimit());
		nbt.setInteger(NBTKEY_OFFSET, offset);
	}

}
