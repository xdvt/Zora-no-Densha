package zoranodensha.signals;


import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.signals.common.ASignal;



/**
 * A class representing an incandescent lamp of a signal. A single signal
 * instance may have signal lamp instances in order to keep track of the
 * realistic fading between signal aspects.
 * 
 * @author Jaffa
 */
public class SignalLamp
{

	private static final String nbtPrefix = "SignalLamp_";

	/**
	 * The ID of this signal lamp, so it is not confused with other lamps of the
	 * signal.
	 */
	public int ID;

	/**
	 * The current brightness of the lamp, which is updated according to the
	 * assigned state of the lamp.<br>
	 * This value is clamped between {@code 0.0F} and {@code 1.0F}.
	 */
	private float intensity;
	private float lastIntensity;

	/**
	 * The color which the lamp displays.
	 */
	private ELampMode lampMode;
	/**
	 * The color which the lamp will attempt to switch to.
	 */
	private ELampMode lampModeTarget;

	/**
	 * The current state of the lamp, on or off.
	 */
	private boolean state;

	/**
	 * {@code true} for an LED lamp, {@code false} for an incandescent lamp. Each type will have a slightly different appearance and animations.
	 */
	private boolean lampType;



	/**
	 * Initialises a new instance of the {@link zoranodensha.signals.SignalLamp}
	 * class.
	 * 
	 * @param ID - The unique identifier to assign to this lamp to
	 *            differentiate it from other lamps from the same signal.
	 * @param startingState - The state at which the lamp should start at.
	 * @param startingMode - The mode at which the lamp should start at.
	 * @param lampType - {@code true} for an LED lamp, {@code false} for an incandescent lamp. Each type will have a slightly different appearance and animations.
	 */
	public SignalLamp(int ID, boolean startingState, ELampMode startingMode, boolean lampType)
	{
		/*
		 * ID
		 */
		this.ID = ID;

		/*
		 * Lamp State
		 */
		state = startingState;

		/*
		 * Lamp Intensity
		 */
		intensity = startingState ? 1.0F : 0.0F;
		lastIntensity = intensity;

		/*
		 * Lamp Color
		 */
		lampMode = startingMode;
		lampModeTarget = lampMode;

		/*
		 * Lamp Type
		 */
		this.lampType = lampType;
	}


	/**
	 * Returns the currently displayed color of the lamp.
	 * 
	 * @return - A {@link zoranodensha.api.util.ColorRGB} instance.
	 */
	public ColorRGBA getColor()
	{
		return lampMode.toColorRGBA();
	}


	/**
	 * Gets the current intensity of the lamp, clamped between {@code 0.0F} and
	 * {@code 1.0F}.
	 */
	public float getIntensity()
	{
		return getIntensity(1.0F);
	}


	public float getIntensity(float partialTick)
	{
		float toReturn;

		toReturn = lastIntensity + ((intensity - lastIntensity) * partialTick);

		/*
		 * Incandescent bulbs will be slightly duller than LED bulbs.
		 */
		if (!lampType)
		{
			toReturn *= 0.8F;
		}

		if (toReturn < 0.0F)
			return 0.0F;
		if (toReturn > 1.0F)
			return 1.0F;
		return toReturn;
	}


	/**
	 * Gets the type of lamp this instance represents, {@code true} for an LED-type lamp or {@code false} for an older incandescent lamp.
	 */
	public boolean getLampType()
	{
		return this.lampType;
	}


	/**
	 * Gets the current state of the lamp, either {@code true} or {@code false}.
	 */
	public boolean getState()
	{
		return state;
	}


	/**
	 * Updates this lamp instance based on the supplied {@link NBTTagCompound}.
	 * 
	 * @param nbt - The {@link NBTTagCompound} containing data regarding this lamp.
	 */
	public void readFromNBT(NBTTagCompound nbt)
	{
		state = nbt.getBoolean(nbtPrefix + ID + "state");
		intensity = nbt.getFloat(nbtPrefix + ID + "intensity");
		lampMode = ELampMode.readFromNBT(nbt, nbtPrefix + ID + "color");
		lampModeTarget = ELampMode.readFromNBT(nbt, nbtPrefix + ID + "color_target");
		lampType = nbt.getBoolean(nbtPrefix + ID + "lampType");
	}


	/**
	 * Assigns a new color to the lamp. It will take a moment for the lamp to switch
	 * over to the specified color.
	 * 
	 * @param newMode - The new color to assign to the lamp.
	 */
	public void setMode(ELampMode newMode)
	{
		if (lampMode != newMode)
		{
			lampModeTarget = newMode;
		}
	}


	/**
	 * Sets the state of this lamp.
	 * 
	 * @param state - The state to set this lamp to, as a {@code boolean}.
	 */
	public void setState(boolean state)
	{
		this.state = state;
	}


	/**
	 * Performs an update tick on this lamp.
	 */
	public void update()
	{
		lastIntensity = intensity;

		// Determine the target intensity.
		float intensityTarget = 0.0F;
		if (lampMode.equals(lampModeTarget) && state)
		{
			intensityTarget = 1.0F;
		}

		// Adjust the intensity based on the target.
		if (lampType)
		{
			/*
			 * LED bulb changes instantly.
			 */
			intensity = intensityTarget;
		}
		else
		{
			/*
			 * Incandescent bulb fades in and out.
			 */
			intensity += (intensityTarget - intensity) / 3.0F;
		}

		if (intensity < 0.0F)
			intensity = 0.0F;
		if (intensity > 1.0F)
			intensity = 1.0F;

		/*
		 * Allow the lamp to change colours.
		 */
		if (intensity <= 0.025F && !lampMode.equals(lampModeTarget))
		{
			lampMode = lampModeTarget;
		}
	}


	/**
	 * Writes the data of this lamp instance to the specified
	 * {@link NBTTagCompound}.
	 * 
	 * @param nbt - The {@link NBTTagCompound} to write the data to.
	 */
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setBoolean(nbtPrefix + ID + "state", state);
		nbt.setFloat(nbtPrefix + ID + "intensity", intensity);
		lampMode.writeToNBT(nbt, nbtPrefix + ID + "color");
		lampModeTarget.writeToNBT(nbt, nbtPrefix + ID + "color_target");
		nbt.setBoolean(nbtPrefix + ID + "lampType", lampType);
	}



	/**
	 * An enum used by Signal Lamps.
	 * 
	 * @author Jaffa
	 */
	public enum ELampMode
	{
		GREEN,
		AMBER,
		RED,
		LUNAR,
		WHITE;



		/**
		 * Parses the specified ordinal of
		 * {@link zoranodensha.signals.SignalLamp.ELampMode}.
		 * 
		 * @return An {@link zoranodensha.signals.SignalLamp.ELampMode}.
		 */
		public static ELampMode parseLampMode(int ordinal)
		{
			for (ELampMode mode : values())
			{
				if (mode.ordinal() == ordinal)
				{
					return mode;
				}
			}

			/*
			 * As a fail-safe, return red if the parse didn't succeed.
			 */
			return ELampMode.RED;
		}


		/**
		 * Reads a {@link zoranodensha.signals.SignalLamp.ELampMode} from the given
		 * {@link net.minecraft.nbt.NBTTagCompound}.
		 * 
		 * @param nbt - The {@link net.minecraft.nbt.NBTTagCompound} to read from.
		 * @param key - The key to use when reading from the compound.
		 * @return - An {@link zoranodensha.signals.SignalLamp.ELampMode}.
		 */
		public static ELampMode readFromNBT(NBTTagCompound nbt, String key)
		{
			return ELampMode.parseLampMode(nbt.getInteger(key));
		}


		/**
		 * Gets a {@link zoranodensha.api.util.ColorRGB} instance based on this.
		 * 
		 * @return - A {@link zoranodensha.api.util.ColorRGB} instance.
		 */
		public ColorRGBA toColorRGBA()
		{
			switch (this)
			{
				case GREEN:
					return ASignal.SIGNAL_GREEN;

				case AMBER:
					return ASignal.SIGNAL_AMBER;

				case RED:
					return ASignal.SIGNAL_RED;

				case LUNAR:
					return ASignal.SIGNAL_LUNAR;

				case WHITE:
					return ASignal.SIGNAL_WHITE;

				default:
					return ASignal.SIGNAL_RED;
			}
		}


		/**
		 * Writes this enum to the specified {@link net.minecraft.nbt.NBTTagCompound}.
		 * 
		 * @param nbt - The {@link net.minecraft.nbt.NBTTagCompound} to write to.
		 * @param key - The key to use when writing to the
		 *            {@link net.minecraft.nbt.NBTTagCompound}.
		 */
		public void writeToNBT(NBTTagCompound nbt, String key)
		{
			nbt.setInteger(key, ordinal());
		}
	}

}
