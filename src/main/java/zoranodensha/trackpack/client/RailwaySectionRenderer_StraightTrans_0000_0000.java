package zoranodensha.trackpack.client;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class RailwaySectionRenderer_StraightTrans_0000_0000 implements IRailwaySectionRender
{
	private static final ResourceLocation[] textures = new ResourceLocation[] {
			new ResourceLocation(ModData.ID, "textures/tracks/Track_straight_wood.png"),
			new ResourceLocation(ModData.ID, "textures/tracks/Track_straight_concrete.png") };
	private static final ObjTrackSpeedy model = new ObjTrackSpeedy(ModCenter.DIR_RESOURCES_TRACKS + "ModelTrack_straightTrans_1x2_0000");



	@Override
	public void renderTrack(int detail, int gagTrack, IRailwaySection section, ITrackBase trackBase, float x, float y, float z, float f)
	{
		/* First the rotation, later the damage of the track. */
		int i = trackBase.getOrientation();
		if (gagTrack > 0)
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(textures[1]);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glTranslatef(x + 0.5F, y, z + 0.5F);
			if (gagTrack == 1)
			{
				GL11.glColor4f(0.5F, 0.5F, 1.0F, 0.5F);
			}
			else
			{
				GL11.glColor4f(1.0F, 0.5F, 0.5F, 0.5F);
			}
			if (i % 2 != 0)
			{
				GL11.glRotatef(i * 90.0F - 180.0F, 0.0F, 1.0F, 0.0F);
			}
			else
			{
				GL11.glRotatef(i * 90.0F, 0.0F, 1.0F, 0.0F);
			}
			model.trackbed.render();
			model.fastening.render();
			model.rail.render();
			GL11.glDisable(GL11.GL_BLEND);
		}
		else
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(textures[trackBase.getField(0, 0, Integer.class) % 2]);
			GL11.glTranslatef(x + 0.5F, y, z + 0.5F);
			if (i % 2 != 0)
			{
				GL11.glRotatef(i * 90.0F - 180.0F, 0.0F, 1.0F, 0.0F);
			}
			else
			{
				GL11.glRotatef(i * 90.0F, 0.0F, 1.0F, 0.0F);
			}

			Tessellator tessellator = Tessellator.instance;
			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				TesselatorVertexState vertexState = trackBase.getField(10, null, TesselatorVertexState.class);
				if (vertexState == null)
				{
					tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
					i = trackBase.getField(1, 0, Integer.class);
					switch (trackBase.getStateOfShape())
					{
						case PLATED:
						case FINISHED:
							(i > 1 ? model.rail_broken : model.rail).render(tessellator);
						case FASTENED:
							if (i != 1)
							{
								model.fastening.render(tessellator);
							}
						case TRACKBED:
							(i % 2 != 0 ? model.trackbed_broken : model.trackbed).render(tessellator);
							break;
						case BUILDINGGUIDE:
							model.guide.render(tessellator);
							break;
					}
					trackBase.setField(10, VertexStateCreator_Tris.getVertexState());
				}
				else
				{
					tessellator.setVertexState(vertexState);
				}
			}
			tessellator.draw();
		}
	}
}
