package zoranodensha.trackpack.client;

import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.RailwaySectionBase;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class RailwaySectionRenderer_StraightSlope_0000_0000 implements IRailwaySectionRender
{
	private static final ResourceLocation[] textures = new ResourceLocation[] {
			new ResourceLocation(ModData.ID, "textures/tracks/Track_straight_wood.png"),
			new ResourceLocation(ModData.ID, "textures/tracks/Track_straight_concrete.png")
			};
	private static final ObjTrackSpeedy model = new ObjTrackSpeedy(ModCenter.DIR_RESOURCES_TRACKS + "ModelTrack_straightSlope_1x10_0000");



	/**
	 * Helper method to render a base block.
	 */
	private static void renderBaseBlock(World world, int x, int y, int z, float height, boolean renderConcreteBed)
	{
		/*
		 * Ensure the block at referenced position exists.
		 */
		Block blockAt = world.getBlock(x, y, z);
		boolean isConcreteBlock = (blockAt == ModCenter.BlockBallastConcrete);

		if (blockAt == Blocks.air || (!world.isSideSolid(x, y, z, ForgeDirection.UP) && !isConcreteBlock))
		{
			return;
		}

		if (blockAt == Blocks.grass)
		{
			blockAt = Blocks.dirt;
		}

		/*
		 * Prepare textures and vertices.
		 */
		Minecraft.getMinecraft().renderEngine.bindTexture(TextureMap.locationBlocksTexture);
		RenderBlocks renderBlocks = RenderBlocks.getInstance();
		IIcon icon;

		final int meta = world.getBlockMetadata(x, y, z);
		final float maxX = 0.5F;
		final float maxZ = 0.5F;
		float minU, maxU, minV, maxV;

		int col = blockAt.colorMultiplier(world, x, y, z);
		float rgb_R = (col >> 16 & 255) / 255.0F;
		float rgb_G = (col >> 8 & 255) / 255.0F;
		float rgb_B = (col & 255) / 255.0F;
		GL11.glColor4f(rgb_R, rgb_G, rgb_B, 1.0F);

		/*
		 * Render all faces in order.
		 */
		Tessellator tessellator = Tessellator.instance;
		GL11.glPushMatrix();
		{
			/* Vertical offset in case the block is a concrete block. */
			if (isConcreteBlock && renderConcreteBed)
			{
				GL11.glTranslatef(0.0F, 0.0315F, 0.0F);
			}

			/* Top (+Y) */
			icon = renderBlocks.getBlockIconFromSideAndMetadata(blockAt, 1, meta);
			minU = icon.getMinU();
			maxU = icon.getMaxU();
			minV = icon.getMinV();
			maxV = icon.getMaxV();

			tessellator.startDrawingQuads();
			tessellator.setNormal(0.0F, 1.0F, 0.0F);
			tessellator.addVertexWithUV(maxX, height, maxZ, maxU, minV); // Bottom-Right
			tessellator.addVertexWithUV(maxX, height, -maxZ, maxU, maxV); // Top-Right
			tessellator.addVertexWithUV(-maxX, height - 0.1F, -maxZ, minU, maxV); // Top-Left
			tessellator.addVertexWithUV(-maxX, height - 0.1F, maxZ, minU, minV); // Bottom-Left
			tessellator.draw();

			if (blockAt == Blocks.grass)
			{
				GL11.glColor4f(1, 1, 1, 1);
			}

			/* Front (+X) */
			icon = renderBlocks.getBlockIconFromSideAndMetadata(blockAt, 5, meta);
			minU = icon.getMinU();
			maxU = icon.getMaxU();
			minV = icon.getInterpolatedV(16.0D - 16.0D * height);
			maxV = icon.getMaxV();

			tessellator.startDrawingQuads();
			tessellator.setNormal(1.0F, 0.0F, 0.0F);
			tessellator.addVertexWithUV(maxX, 0.0F, -maxZ, maxU, maxV);
			tessellator.addVertexWithUV(maxX, height, -maxZ, maxU, minV);
			tessellator.addVertexWithUV(maxX, height, maxZ, minU, minV);
			tessellator.addVertexWithUV(maxX, 0.0F, maxZ, minU, maxV);
			tessellator.draw();

			/* Back (-X) */
			if (height > 0.1F)
			{
				icon = renderBlocks.getBlockIconFromSideAndMetadata(blockAt, 4, meta);
				minU = icon.getMinU();
				maxU = icon.getMaxU();
				minV = icon.getInterpolatedV(16.0D - 16.0D * (height - 0.1F));
				maxV = icon.getMaxV();

				tessellator.startDrawingQuads();
				tessellator.setNormal(-1.0F, 0.0F, 0.0F);
				tessellator.addVertexWithUV(-maxX, 0.0F, maxZ, maxU, maxV);
				tessellator.addVertexWithUV(-maxX, height - 0.1F, maxZ, maxU, minV);
				tessellator.addVertexWithUV(-maxX, height - 0.1F, -maxZ, minU, minV);
				tessellator.addVertexWithUV(-maxX, 0.0F, -maxZ, minU, maxV);
				tessellator.draw();
			}

			/* Right (+Z) */
			icon = renderBlocks.getBlockIconFromSideAndMetadata(blockAt, 3, meta);
			minU = icon.getMinU();
			maxU = icon.getMaxU();
			minV = icon.getInterpolatedV(16.0D - 16.0D * height);
			maxV = icon.getMaxV();

			tessellator.startDrawingQuads();
			tessellator.setNormal(0.0F, 0.0F, 1.0F);
			tessellator.addVertexWithUV(maxX, 0.0F, maxZ, maxU, maxV);
			tessellator.addVertexWithUV(maxX, height, maxZ, maxU, minV);
			tessellator.addVertexWithUV(-maxX, height - 0.1F, maxZ, minU, icon.getInterpolatedV(16.0D - 16.0D * (height - 0.1F)));
			tessellator.addVertexWithUV(-maxX, 0.0F, maxZ, minU, maxV);
			tessellator.draw();

			/* Left (-Z) */
			icon = renderBlocks.getBlockIconFromSideAndMetadata(blockAt, 2, meta);
			minU = icon.getMinU();
			maxU = icon.getMaxU();
			minV = icon.getInterpolatedV(16.0D - 16.0D * height);
			maxV = icon.getMaxV();

			tessellator.startDrawingQuads();
			tessellator.setNormal(0.0F, 0.0F, -1.0F);
			tessellator.addVertexWithUV(maxX, height, -maxZ, minU, minV);
			tessellator.addVertexWithUV(maxX, 0.0F, -maxZ, minU, maxV);
			tessellator.addVertexWithUV(-maxX, 0.0F, -maxZ, maxU, maxV);
			tessellator.addVertexWithUV(-maxX, height - 0.1F, -maxZ, maxU, icon.getInterpolatedV(16.0D - 16.0D * (height - 0.1F)));
			tessellator.draw();
		}
		GL11.glPopMatrix();
	}

	@Override
	public void renderTrack(int detail, int gagTrack, IRailwaySection section, ITrackBase trackBase, float x, float y, float z, float f)
	{
		/* First the rotation, later the damage of the track. */
		int i = trackBase.getOrientation();
		if (gagTrack > 0)
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(textures[1]);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glTranslatef(x + 0.5F, y, z + 0.5F);
			
			if (gagTrack == 1)
			{
				GL11.glColor4f(0.5F, 0.5F, 1.0F, 0.5F);
			}
			else
			{
				GL11.glColor4f(1.0F, 0.5F, 0.5F, 0.5F);
			}
			if (i % 2 != 0)
			{
				GL11.glRotatef(i * 90.0F - 180.0F, 0.0F, 1.0F, 0.0F);
			}
			else
			{
				GL11.glRotatef(i * 90.0F, 0.0F, 1.0F, 0.0F);
			}
			
			model.trackbed.render();
			model.fastening.render();
			model.rail.render();
			GL11.glDisable(GL11.GL_BLEND);
		}
		else
		{
			/*
			 * Prepare render.
			 */
			GL11.glPushMatrix();
			GL11.glTranslatef(x + 0.5F, y, z + 0.5F);
			
			if (i % 2 != 0)
			{
				GL11.glRotatef(i * 90.0F - 180.0F, 0.0F, 1.0F, 0.0F);
			}
			else
			{
				GL11.glRotatef(i * 90.0F, 0.0F, 1.0F, 0.0F);
			}

			/*
			 * Render slope base.
			 */
			if (trackBase instanceof TileEntity)
			{
				TileEntity tile = (TileEntity)trackBase;
				List<int[]> gags = RailwaySectionBase.getGagBlocksWithRotation(0, i, section.getGagBlocks(trackBase.getDirectionOfSection()));
				float height = 0.0F;

				GL11.glPushMatrix();
				for (int[] gag : gags)
				{
					boolean renderConcreteBed = trackBase.getStateOfShape().isFinished() && (trackBase.getField(0, 0, Integer.class) % 2 != 0);
					renderBaseBlock(tile.getWorldObj(), tile.xCoord + gag[0], tile.yCoord - 1, tile.zCoord + gag[2], (height += 0.1F), renderConcreteBed);
					GL11.glTranslatef(1.0F, 0.0F, 0.0F);
				}
				GL11.glPopMatrix();
				GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			}

			/*
			 * Render track.
			 */
			Minecraft.getMinecraft().renderEngine.bindTexture(textures[trackBase.getField(0, 0, Integer.class) % 2]);

			Tessellator tessellator = Tessellator.instance;
			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				TesselatorVertexState vertexState = trackBase.getField(10, null, TesselatorVertexState.class);
				if (vertexState == null)
				{
					tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
					i = trackBase.getField(1, 0, Integer.class);
					switch (trackBase.getStateOfShape())
					{
						case PLATED:
						case FINISHED:
							model.rail.render(tessellator);
						case FASTENED:
							model.fastening.render(tessellator);
						case TRACKBED:
							model.trackbed.render(tessellator);
							break;
						case BUILDINGGUIDE:
							model.guide.render(tessellator);
							break;
					}
					trackBase.setField(10, VertexStateCreator_Tris.getVertexState());
				}
				else
				{
					tessellator.setVertexState(vertexState);
				}
			}
			tessellator.draw();
			GL11.glPopMatrix();

			TrackRenderHelper.renderConcreteBed(trackBase, x, y, z);
		}
	}
}
