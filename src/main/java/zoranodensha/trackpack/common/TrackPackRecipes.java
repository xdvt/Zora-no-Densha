package zoranodensha.trackpack.common;

import static zoranodensha.api.structures.EEngineerTableTab.TRACKS;

import java.util.ArrayList;

import net.minecraft.item.ItemStack;
import zoranodensha.api.structures.IEngineerTableExtension;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.common.core.ModCenter;



public class TrackPackRecipes implements IEngineerTableExtension
{
	static final String BLADES   = "switch_blades";
	static final String BUFFER   = "buffer";
	static final String MOTOR    = "switch_motor";
	static final String RAIL     = "rails";
	static final String STILTS   = "stilts";
	static final String TRACKBED = "trackbed";



	@Override
	public ArrayList<EngineerTableIngredientHelper> registerIngredients()
	{
		ArrayList<EngineerTableIngredientHelper> list = new ArrayList<EngineerTableIngredientHelper>();
		list.add(new EngineerTableIngredientHelper(new ItemStack(ModCenter.ItemEngineerTableTracks, 0, 0), RAIL, TRACKS));
		list.add(new EngineerTableIngredientHelper(new ItemStack(ModCenter.ItemEngineerTableTracks, 0, 1), TRACKBED, TRACKS));
		list.add(new EngineerTableIngredientHelper(new ItemStack(ModCenter.ItemEngineerTableTracks, 0, 2), MOTOR, TRACKS));
		list.add(new EngineerTableIngredientHelper(new ItemStack(ModCenter.ItemEngineerTableTracks, 0, 3), BLADES, TRACKS));
		list.add(new EngineerTableIngredientHelper(new ItemStack(ModCenter.ItemEngineerTableTracks, 0, 4), STILTS, TRACKS));
		list.add(new EngineerTableIngredientHelper(new ItemStack(ModCenter.ItemEngineerTableTracks, 0, 5), BUFFER, TRACKS));
		return list;
	}

	@Override
	public ArrayList<EngineerTableRecipeHelper> registerRecipes()
	{
		ArrayList<EngineerTableRecipeHelper> list = new ArrayList<EngineerTableRecipeHelper>();
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Cross_0000_0000",  			EDirection.NONE,   new String[] { "     ", "  R  ", " RTR ", "  R  ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Cross_0000_1131",  			EDirection.LEFT,   new String[] { "TRTR ", "RTTR ", " RTR ", " RTTR", " RTRT" }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Cross_0000_1131",  			EDirection.RIGHT,  new String[] { " RTRT", " RTTR", " RTR ", "RTTR ", "TRTR " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Cross_4500_4500",  			EDirection.NONE,   new String[] { "     ", " R R ", "  T  ", " R R ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Cross10_1131_1131",  		EDirection.NONE,   new String[] { "TR RT", "RTRTR", " RTR ", "RTRTR", "TR RT" }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Cross5_1131_1131",  		EDirection.NONE,   new String[] { "     ", "RTRTR", " RTR ", "RTRTR", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_0000_1131",  			EDirection.LEFT,   new String[] { "RT R ", " RTR ", " RT R", "  RTR", "  RTR" }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_0000_1131",  			EDirection.LEFT,   new String[] { "RTR  ", "RTR  ", "R TR ", " RTR ", " R TR" }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_0000_1131",  			EDirection.RIGHT,  new String[] { " R TR", " RTR ", "R TR ", "RTR  ", "RTR  " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_0000_1131",  			EDirection.RIGHT,  new String[] { "  RTR", "  RTR", " RT R", " RTR ", "RT R " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_1131_1843",  			EDirection.LEFT,   new String[] { "RT R ", " RTR ", "  RTR", "  RTR", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_1131_1843",  			EDirection.LEFT,   new String[] { "     ", "RTR  ", "RTR  ", " RTR ", " R TR" }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_1131_1843",  			EDirection.RIGHT,  new String[] { " R TR", " RTR ", "RTR  ", "RTR  ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_1131_1843",  			EDirection.RIGHT,  new String[] { "     ", "  RTR", "  RTR", " RTR ", "RT R " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_1843_4500",  			EDirection.LEFT,   new String[] { "R TR ", " RTR ", " RTR ", "  RTR", "   RT" }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve_1843_4500",  			EDirection.RIGHT,  new String[] { " RT R", " RTR ", " RTR ", "RTR  ", "TR   " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve10_1843_4500",  		EDirection.LEFT,   new String[] { "TR   ", "RTR  ", " RTR ", " RTR ", " RT R" }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Curve10_1843_4500",  		EDirection.RIGHT,  new String[] { "   RT", "  RTR", " RTR ", " RTR ", "R TR " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_SCurve_0000_0000", 			EDirection.LEFT,   new String[] { "   RR", "  RTT", "RRTRR", "TTR  ", "RR   " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_SCurve_0000_0000", 			EDirection.RIGHT,  new String[] { "   RR", "  RTT", "RRTRR", "TTR  ", "RR   " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Slope_0000_0000",  			EDirection.NONE,   new String[] { "    R", "   RT", "  RT ", "     ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Slope_0000_0000",  			EDirection.NONE,   new String[] { "R    ", "TR   ", " TR  ", "     ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Slope16_0000_0000",  		EDirection.NONE,   new String[] { "    R", "   RT", "  RT ", " RT  ", "RT   " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Slope16_0000_0000",  		EDirection.NONE,   new String[] { "R    ", "TR   ", " TR  ", "  TR ", "   TR" }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight_0000_0000",		EDirection.NONE,   new String[] { "     ", "     ", " RTR ", "     ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight_0000_0000",		EDirection.NONE,   new String[] { "     ", "  R  ", "  T  ", "  R  ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight2_0000_0000",		EDirection.NONE,   new String[] { " RTR ", " RTR ", "     ", "     ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight2_0000_0000",		EDirection.NONE,   new String[] { "     ", "     ", "     ", " RTR ", " RTR " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight4_0000_0000",		EDirection.NONE,   new String[] { " RTR ", " RTR ", " RTR ", " RTR ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight4_0000_0000",		EDirection.NONE,   new String[] { "     ", " RTR ", " RTR ", " RTR ", " RTR " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight8_0000_0000",		EDirection.NONE,   new String[] { " RTR ", " RTR ", " RTR ", " RTR ", " RTR " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightBuffer_0000_0000",	EDirection.NONE,   new String[] { "     ", "  B  ", " RTR ", " RTR ", "     " }, "R", RAIL, "T", TRACKBED, "B", BUFFER));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightBuffer_1131_1131",  EDirection.LEFT,   new String[] { "RBR  ", "RT R ", " RTR ", " R TR", "  RTR" }, "R", RAIL, "T", TRACKBED, "B", BUFFER));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightBuffer_1131_1131", 	EDirection.RIGHT,  new String[] { "  RBR", " R TR", " RTR ", "RT R ", "RTR  " }, "R", RAIL, "T", TRACKBED, "B", BUFFER));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightBuffer_1843_1843", 	EDirection.LEFT,   new String[] { " B   ", "RT R ", " RTR ", " R TR", "     " }, "R", RAIL, "T", TRACKBED, "B", BUFFER));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightBuffer_1843_1843", 	EDirection.RIGHT,  new String[] { "   B ", " R TR", " RTR ", "RT R ", "     " }, "R", RAIL, "T", TRACKBED, "B", BUFFER));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightBuffer_4500_4500", 	EDirection.NONE,   new String[] { "     ", "  RB ", " RTR ", " TR  ", "     " }, "R", RAIL, "T", TRACKBED, "B", BUFFER));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightBuffer_4500_4500", 	EDirection.NONE,   new String[] { "     ", "BR   ", " RTR ", "   RT", "     " }, "R", RAIL, "T", TRACKBED, "B", BUFFER));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightMaint_0000_0000",	EDirection.NONE,   new String[] { "     ", "     ", " RPR ", "     ", "     " }, "R", RAIL, "P", STILTS));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightMaint_0000_0000",	EDirection.NONE,   new String[] { "     ", "  R  ", "  P  ", "  R  ", "     " }, "R", RAIL, "P", STILTS));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight_1131_1131",     	EDirection.LEFT,   new String[] { "RTR  ", "RT R ", " RTR ", " R TR", "  RTR" }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight_1131_1131", 		EDirection.RIGHT,  new String[] { "  RTR", " R TR", " RTR ", "RT R ", "RTR  " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight_1843_1843", 		EDirection.LEFT,   new String[] { "     ", "RT R ", " RTR ", " R TR", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight_1843_1843", 		EDirection.RIGHT,  new String[] { "     ", " R TR", " RTR ", "RT R ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight_4500_4500", 		EDirection.NONE,   new String[] { "     ", " R   ", "  T  ", "   R ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight_4500_4500", 		EDirection.NONE,   new String[] { "     ", "   R ", "  T  ", " R   ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight2_4500_4500", 		EDirection.NONE,   new String[] { "     ", "  RT ", " RTR ", " TR  ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight2_4500_4500", 		EDirection.NONE,   new String[] { "     ", "TR   ", " RTR ", "   RT", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight4_4500_4500", 		EDirection.NONE,   new String[] { "     ", "  RT ", " RTR ", "RTR  ", "TR   " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight4_4500_4500", 		EDirection.NONE,   new String[] { "   RT", "  RTR", " RTR ", " TR  ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Straight8_4500_4500", 		EDirection.NONE,   new String[] { "   RT", "  RTR", " RTR ", "RTR  ", "TR   " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightTrans_0000_0000", 	EDirection.NONE,   new String[] { "     ", "     ", " RTR ", " R R ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_StraightTrans_0000_0000", 	EDirection.NONE,   new String[] { "     ", " R R ", " RTR ", "     ", "     " }, "R", RAIL, "T", TRACKBED));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Switch_0000_1131", 			EDirection.LEFT,   new String[] { "RTRTR", " RTTR", " RTTR", " MRBR", "  RTR" }, "R", RAIL, "T", TRACKBED, "M", MOTOR, "B", BLADES));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Switch_0000_1131", 			EDirection.RIGHT,  new String[] { "  RTR", " MRBR", " RTTR", " RTTR", "RTRTR" }, "R", RAIL, "T", TRACKBED, "M", MOTOR, "B", BLADES));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Switch_1131_0000", 			EDirection.LEFT,   new String[] { "RTR  ", "RBRM ", "RTTR ", "RTTR ", "RTRTR" }, "R", RAIL, "T", TRACKBED, "M", MOTOR, "B", BLADES));
		list.add(new EngineerTableRecipeHelper(TRACKS, "ZnD_Switch_1131_0000", 			EDirection.RIGHT,  new String[] { "RTRTR", "RTTR ", "RTTR ", "RBRM ", "RTR  " }, "R", RAIL, "T", TRACKBED, "M", MOTOR, "B", BLADES));
		return list;
	}
}
