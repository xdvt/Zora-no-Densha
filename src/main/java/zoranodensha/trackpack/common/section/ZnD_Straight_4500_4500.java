package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.api.structures.signals.ISignalMagnet;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;



public class ZnD_Straight_4500_4500 extends ARailwaySectionTrackPackSignalMagnet
{

	/**
	 * Initialises a new instance of the {@link ZnD_Straight_4500_4500} class.
	 */
	public ZnD_Straight_4500_4500()
	{
		super(1, 1, 1, 1, "ZnD_Straight_4500_4500", 1, new Path[] {
				new Path(new String[] { "x", "1" }, null)
				});
	}


	@SideOnly(Side.CLIENT)
	public ZnD_Straight_4500_4500(IRailwaySectionRender render)
	{
		this();
		this.render = render;
	}


	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}


	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			list.add(new int[] { 0, 0, -1 });
			list.add(new int[] { 0, 0, 0 });
			list.add(new int[] { 0, 0, 1 });
		}
		return list;
	}

}
