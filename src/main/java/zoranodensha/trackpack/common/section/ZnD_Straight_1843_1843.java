package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.Path;



public class ZnD_Straight_1843_1843 extends RailwaySectionTrackPack
{
	public ZnD_Straight_1843_1843()
	{
		super(3, 3, 3, 3, "ZnD_Straight_1843_1843", 3, new Path[] {
				new Path(new String[] { "(0.3333 * x) + 0.5", "0.4096" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_Straight_1843_1843(IRailwaySectionRender render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.LEFT)
		{
			for (int i = 0; i < length; ++i)
			{
				for (int j = 0; j < 2; ++j)
				{
					list.add(new int[] { i, 0, -j });
				}
			}
		}
		else if (dir == EDirection.RIGHT)
		{
			for (int i = 0; i < length; ++i)
			{
				for (int j = 0; j < 2; ++j)
				{
					list.add(new int[] { -i, 0, -j });
				}
			}
		}
		return list;
	}
}
