package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mods.railcraft.api.core.items.IToolCrowbar;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.tracks.ERailwayState;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;
import zoranodensha.api.structures.tracks.RailwaySectionBase;
import zoranodensha.common.core.ModCenter;


public abstract class RailwaySectionTrackPack extends RailwaySectionBase
{
	@SideOnly(Side.CLIENT)
	protected IRailwaySectionRender render;


	public RailwaySectionTrackPack(int trackbeds, int fastenings, int rails, int plates, String name, int length, Path[] paths)
	{
		super(trackbeds, fastenings, rails, plates, name, length, paths);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IRailwaySectionRender getRender()
	{
		return render;
	}

	@Override
	public boolean isMaintenanceTrack()
	{
		return false;
	}

	@Override
	public List<ItemStack> onTrackDestroyed(TileEntity tileEntity, EntityPlayer player)
	{
		if (player == null || !player.capabilities.isCreativeMode)
		{
			ITrackBase trackBase = (ITrackBase) tileEntity;
			List<ItemStack> drops = new ArrayList<ItemStack>();
			boolean flag = player == null || (player.getHeldItem() != null && player.getHeldItem().getItem() instanceof IToolCrowbar);
			switch (trackBase.getStateOfShape())
			{
				case BUILDINGGUIDE:
				{
					drops.add(null);
					return drops;
				}
				case TRACKBED:
				{
					if (flag)
					{
						drops.add(new ItemStack(ModCenter.ItemTrackPart, trackbeds, trackBase.getField(0, 0, Integer.class) % 2));
						drops.add(null);
					}
					else if (trackBase.getField(1, 0, Integer.class) == 0)
					{
						trackBase.setField(1, 1);
						return null;
					}
					else
					{
						drops.add(null);
					}
					return drops;
				}
				case FASTENED:
				{
					if (flag)
					{
						drops.add(new ItemStack(ModCenter.ItemTrackPart, fastenings, 4));
					}
					trackBase.setStateOfShape(ERailwayState.TRACKBED);
					return drops;
				}
				case FINISHED:
				{
					int damage = trackBase.getField(1, 0, Integer.class);
					if (damage < 2)
					{
						if (flag)
						{
							if (trackBase.getInstanceOfShape() instanceof ZnD_Straight_0000_0000)
							{
								if (trackBase.getField(ZnD_Straight_0000_0000.INDEX_HASMAGNET, false, Boolean.class))
								{
									drops.add(new ItemStack(ModCenter.ItemTrackPart, 1, 7));
									trackBase.setField(ZnD_Straight_0000_0000.INDEX_HASMAGNET, false);
									trackBase.setStateOfShape(ERailwayState.FINISHED);

									return drops;
								}
							}

							drops.add(new ItemStack(ModCenter.ItemTrackPart, rails, 2));
							trackBase.setStateOfShape(ERailwayState.FASTENED);
							return drops;
						}
						trackBase.setField(1, damage + 2);
						return null;
					}
					trackBase.setStateOfShape(ERailwayState.FASTENED);
					trackBase.setField(1, damage - 2);
					break;
				}
				case PLATED:
				{
					if (flag)
					{
						drops.add(new ItemStack(ModCenter.ItemTrackPart, plates, 3));
					}
					trackBase.setStateOfShape(ERailwayState.FINISHED);
					return drops;
				}
			}
		}
		return null;
	}
}
