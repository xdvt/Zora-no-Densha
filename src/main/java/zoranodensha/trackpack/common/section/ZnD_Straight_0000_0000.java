package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Level;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.api.structures.signals.ISignalMagnet;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;
import zoranodensha.api.vehicles.Train;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.items.ItemTrackPart;
import zoranodensha.signals.common.ASignal;



public class ZnD_Straight_0000_0000 extends ARailwaySectionTrackPackSignalMagnet
{

	/**
	 * Initialises a new instance of the {@link zoranodensha.trackpack.common.section.ZnD_Straight_0000_0000} class.
	 */
	public ZnD_Straight_0000_0000()
	{
		super(1, 1, 1, 1, "ZnD_Straight_0000_0000", 1, new Path[] {
				new Path(new String[] { "0.5", "0" }, null)
				});
	}


	@SideOnly(Side.CLIENT)
	public ZnD_Straight_0000_0000(IRailwaySectionRender render)
	{
		this();
		this.render = render;
	}


	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}


	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			list.add(new int[] { 0, 0, 0 });
		}
		return list;
	}

}
