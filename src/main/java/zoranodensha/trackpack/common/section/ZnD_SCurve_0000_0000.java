package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.Path;



public class ZnD_SCurve_0000_0000 extends RailwaySectionTrackPack
{
	public ZnD_SCurve_0000_0000()
	{
		super(8, 8, 8, 0, "ZnD_SCurve_0000_0000", 8, new Path[] {
				new Path(new String[] { "1 / (1 + exp(4 - x)) + 0.5", "exp(x-4) / (exp(x-4) + 1)^2" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_SCurve_0000_0000(IRailwaySectionRender render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.CURVE;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.LEFT)
		{
			for (int i = 0; i < length; ++i)
			{
				for (int j = 0; j < 2; ++j)
				{
					if ((i == 0 && j == 1) || (i == 7 && j == 0))
					{
						continue;
					}
					
					list.add(new int[] { i, 0, -j });
				}
			}
		}
		else if (dir == EDirection.RIGHT)
		{
			for (int i = 0; i < length; ++i)
			{
				for (int j = 0; j < 2; ++j)
				{
					if ((i == 0 && j == 1) || (i == 7 && j == 0))
					{
						continue;
					}
					
					list.add(new int[] { -i, 0, -j });
				}
			}
		}
		return list;
	}
}
