package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;
import zoranodensha.api.vehicles.handlers.MovementHandler;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.util.PositionStackEntry;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;



public class ZnD_Switch_0000_1131 extends RailwaySectionTrackPack
{
	public ZnD_Switch_0000_1131()
	{
		super(	7, 11, 11, 0, "ZnD_Switch_0000_1131", 10, new Path[] {
				new Path(new String[] { "0.5", "0" }, null),
				new Path(new String[] { "(0.0001 * x^3) + (0.009 * x^2) + 0.5", "(0.0003 * x^2) + (0.02214 * x)" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_Switch_0000_1131(IRailwaySectionRender render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.SWITCH;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.LEFT)
		{
			for (int i = 0; i < length; ++i)
			{
				if (i > 3 || i == 2)
				{
					for (int j = 0; j < 2; ++j)
					{
						list.add(new int[] { i, 0, -j });
					}
				}
				else
				{
					list.add(new int[] { i, 0, 0 });
				}
			}
		}
		else if (dir == EDirection.RIGHT)
		{
			for (int i = length - 1; i >= 0; --i)
			{
				if (i > 3 || i == 2)
				{
					for (int j = 0; j < 2; ++j)
					{
						list.add(new int[] { -i, 0, -j });
					}
				}
				else
				{
					list.add(new int[] { -i, 0, 0 });
				}
			}
		}
		return list;
	}

	@Override
	public double[][] getPositionOnTrack(ITrackBase track, double vehX, double vehY, double vehZ, @Nullable Object vehicle, TileEntity tile, double localX, double localZ)
	{
		/* Prepare return value, retrieve current path and side orientation. */
		Path path = getPath(track.getPath());
		int rota = track.getOrientation();
		boolean isRight = track.getDirectionOfSection() == EDirection.RIGHT;

		/*
		 * Determine whether set path or other path will be selected.
		 */
		boolean useSetPath = localX <= 1.0D;

		if (vehicle instanceof PartTypeBogie)
		{
			PartTypeBogie bogie = (PartTypeBogie)vehicle;
			double prevLocalX = TileEntityTrackBase.getLocalX(isRight ? (rota + 2) : rota, bogie.getPrevPosX(), bogie.getPrevPosZ(), tile.xCoord, tile.zCoord, track.getLengthOfSection());

			/* If we're heading for the split-track end, use the set path. */
			if (localX > prevLocalX)
			{
				useSetPath = true;
			}
		}
		else if (vehicle instanceof EntityMinecart)
		{
			EntityMinecart cart = (EntityMinecart)vehicle;
			double prevLocalX = TileEntityTrackBase.getLocalX(isRight ? (rota + 2) : rota, cart.prevPosX, cart.prevPosZ, tile.xCoord, tile.zCoord, track.getLengthOfSection());

			/* If we're heading for the split-track end, use the set path. */
			if (localX > prevLocalX)
			{
				useSetPath = true;
			}
		}
		else if (vehicle instanceof PositionStackEntry)
		{
			PositionStackEntry pse = (PositionStackEntry)vehicle;
			double prevLocalX = TileEntityTrackBase.getLocalX(isRight ? (rota + 2) : rota, pse.x, pse.z, tile.xCoord, tile.zCoord, track.getLengthOfSection());

			/* If we're heading for the split-track end, use the set path. */
			if (localX > prevLocalX)
			{
				useSetPath = true;
			}
		}

		if (!useSetPath)
		{
			Path pathSet = path;
			Path pathOther = getPath(track.getPath() > 0 ? 0 : 1);
			double[] posSet = pathSet.calculatePosition(localX);
			double[] posOther = pathOther.calculatePosition(localX);

			path = (Math.abs(posOther[0] - localZ) < Math.abs(posSet[0] - localZ)) ? pathOther : pathSet;
		}

		/*
		 * Apply new position and rotation data.
		 */
		double[][] ret = new double[][] { path.calculatePosition(localX), path.calculateRotation(localX) };

		for (int i = 0; i < 3; ++i)
		{
			switch (i)
			{
				case 0:
					ret[1][i] *= (isRight ? -45.0D : 45.0D);
					ret[1][i] += (rota % 4) * 90.0D;
					break;
				default:
					ret[1][i] *= 45.0D;
					break;
			}
			ret[1][i] %= 360.0D;
		}

		double[] pos = new double[] { 0.0D, tile.yCoord, 0.0D };

		switch (rota % 4)
		{
			case 0:
				pos[0] = vehX + ret[0][2];
				pos[2] = tile.zCoord - ret[0][0] + 1.0D;
				break;
			case 1:
				pos[0] = tile.xCoord + ret[0][0];
				pos[2] = vehZ + ret[0][2];
				break;
			case 2:
				pos[0] = vehX + ret[0][2];
				pos[2] = tile.zCoord + ret[0][0];
				break;
			case 3:
				pos[0] = tile.xCoord - ret[0][0] + 1.0D;
				pos[2] = vehZ + ret[0][2];
				break;
		}

		ret[0] = pos;
		return ret;
	}

	@Override
	public List<Integer> getValidPaths()
	{
		List<Integer> paths = new ArrayList<Integer>();
		paths.add(0);
		paths.add(1);
		return paths;
	}

	@Override
	public TileEntity initializeTrack(ITrackBase trackBase)
	{
		trackBase.setField(0, 2); // Texture name
		trackBase.setField(1, 0); // Damage
		trackBase.setField(2, 0.0F); // Rotation of switch blades
		trackBase.setField(3, false); // Manual path override
		return (TileEntity)trackBase;
	}

	@Override
	public void onReadFromNBT(NBTTagCompound nbt, ITrackBase trackBase)
	{
		for (int i = 0; i < 4; ++i)
		{
			String s = nbt.getString("FieldDec_" + i);
			if ("int".equals(s))
			{
				trackBase.setField(i, nbt.getInteger("Field_" + i));
			}
			else if ("float".equals(s))
			{
				trackBase.setField(i, nbt.getFloat("Field_" + i));
			}
			else if ("boolean".contentEquals(s))
			{
				trackBase.setField(i, nbt.getBoolean("Field_" + i));
			}
			else
			{
				switch (i)
				{
					case 0:
						trackBase.setField(i, 2);
						break;
					case 1:
						trackBase.setField(i, 0);
						break;
					case 2:
						trackBase.setField(i, 0.0F);
						break;
					case 3:
						trackBase.setField(i, false);
						break;
				}
			}
		}
	}

	@Override
	public boolean onTrackWorkedOn(ITrackBase trackBase, EntityPlayer player, boolean isRemote)
	{
		if (super.onTrackWorkedOn(trackBase, player, isRemote))
		{
			return true;
		}
		if (!isRemote)
		{
			/* Switch path inversion flag upon manual interaction. */
			boolean isPathInverse = trackBase.getField(3, false, Boolean.class);
			trackBase.setField(3, !isPathInverse);
		}
		return true;
	}

	@Override
	public Map<Integer, Object> onUpdate(World world, int x, int y, int z, ITrackBase trackBase, Map<Integer, Object> params)
	{
		/* Update active path. */
		boolean isDivergingPath = world.isBlockIndirectlyGettingPowered(x, y, z);
		if (trackBase.getField(3, false, Boolean.class))
		{
			// Override diverging path after manual path change.
			isDivergingPath = !isDivergingPath;
		}
		
		int targetPath = (isDivergingPath) ? 1 : 0;
		if (trackBase.getPath() != targetPath)
		{
			trackBase.setPath(targetPath);
		}
		
		/* Update switch blade rotation. */
		float bladeRotation = (Float)params.get(2);
		if (trackBase.getPath() != 0)
		{
			if (bladeRotation < 2.1F)
			{
				bladeRotation = MathHelper.clamp_float(bladeRotation + 0.05F, 0.0F, 2.1F);
			}
		}
		else
		{
			if (bladeRotation > 0.0F)
			{
				bladeRotation = MathHelper.clamp_float(bladeRotation - 0.05F, 0.0F, 2.1F);
			}
		}

		if ((Float)params.get(2) != bladeRotation)
		{
			// Clear model cache on update.
			params.put(2, bladeRotation);
			params.put(10, null);
		}

		return super.onUpdate(world, x, y, z, trackBase, params);
	}

	@Override
	public void onWriteToNBT(NBTTagCompound nbt, ITrackBase trackBase)
	{
		for (int i = 0; i < 4; ++i)
		{
			Object obj = trackBase.getFieldUnchecked(i);
			if (obj instanceof Integer)
			{
				nbt.setString("FieldDec_" + i, "int");
				nbt.setInteger("Field_" + i, (Integer)obj);
			}
			else if (obj instanceof Float)
			{
				nbt.setString("FieldDec_" + i, "float");
				nbt.setFloat("Field_" + i, (Float)obj);
			}
			else if (obj instanceof Boolean)
			{
				nbt.setString("FieldDec_" + i, "boolean");
				nbt.setBoolean("Field_" + i, (Boolean)obj);
			}
		}
	}
}
