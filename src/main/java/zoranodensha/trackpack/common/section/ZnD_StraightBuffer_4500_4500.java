package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mods.railcraft.api.core.items.IToolCrowbar;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.items.ItemTrackPart;



public class ZnD_StraightBuffer_4500_4500 extends RailwaySectionTrackPack
{
	public ZnD_StraightBuffer_4500_4500()
	{
		super(2, 2, 2, 1, "ZnD_StraightBuffer_4500_4500", 2, new Path[] {
				new Path(new String[] { "x", "1" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_StraightBuffer_4500_4500(IRailwaySectionRender render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			for (int i = 0; i < length; ++i)
			{	
				for (int j = (i - 1); j < (2 + i); ++j)
				{
					if (i + 1 == length && j == i)
					{
						list.add(new int[] { i, 0, -j, 1 });
					}
					else
					{
						list.add(new int[] { i, 0, -j, 0 });
					}
				}
			}
		}
		return list;
	}

	@Override
	public TileEntity initializeTrack(ITrackBase trackBase)
	{
		super.initializeTrack(trackBase);
		trackBase.setField(2, false);
		return (TileEntity)trackBase;
	}

	@Override
	public void onReadFromNBT(NBTTagCompound nbt, ITrackBase trackBase)
	{
		for (int i = 0; i < 3; ++i)
		{
			String s = nbt.getString("FieldDec_" + i);
			if ("int".equals(s))
			{
				trackBase.setField(i, nbt.getInteger("Field_" + i));
			}
			else if ("boolean".equals(s))
			{
				trackBase.setField(i, nbt.getBoolean("Field_" + i));
			}
		}
	}

	@Override
	public List<ItemStack> onTrackDestroyed(TileEntity tileEntity, EntityPlayer player)
	{
		if ((player == null || !player.capabilities.isCreativeMode) && ((ITrackBase)tileEntity).getField(2, false, Boolean.class))
		{
			if (player == null || (player.getHeldItem() != null && player.getHeldItem().getItem() instanceof IToolCrowbar))
			{
				((ITrackBase)tileEntity).setField(2, false);
				ModCenter.BlockTrackBase.dropBlockAsItem(tileEntity.getWorldObj(), tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord, new ItemStack(ModCenter.ItemTrackPart, 1, 6));
			}
			return null;
		}
		
		return super.onTrackDestroyed(tileEntity, player);
	}

	@Override
	public boolean onTrackWorkedOn(ITrackBase trackBase, EntityPlayer player, boolean isRemote)
	{
		if (super.onTrackWorkedOn(trackBase, player, isRemote))
		{
			return true;
		}
		ItemStack currentItem = player.getHeldItem();
		if (currentItem == null)
		{
			return false;
		}
		if (currentItem.getItem() instanceof ItemTrackPart && currentItem.getItemDamage() == 6)
		{
			if (!trackBase.getField(2, false, Boolean.class))
			{
				if (player.capabilities.isCreativeMode || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), 1, 6), 1))
				{
					trackBase.setField(2, true);
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void onWriteToNBT(NBTTagCompound nbt, ITrackBase trackBase)
	{
		super.onWriteToNBT(nbt, trackBase);
		nbt.setString("FieldDec_2", "boolean");
		nbt.setBoolean("Field_2", trackBase.getField(2, false, Boolean.class));
	}
}
