package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.Path;



public class ZnD_StraightSlope16_0000_0000 extends RailwaySectionTrackPack
{
	public ZnD_StraightSlope16_0000_0000()
	{
		// Note: We're sticking to the "old" naming convention to match existing slope tracks.
		super(16, 16, 16, 0, "ZnD_Slope16_0000_0000", 16, new Path[] {
				new Path(new String[] {        "0.5", "0" },
				         new String[] { "0.0625 * x", "0.0625" })
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_StraightSlope16_0000_0000(IRailwaySectionRender render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			for (int i = 0; i < length; ++i)
			{
				// localX, localY, localZ, height
				list.add(new int[] { i, 0, 0, i });
			}
		}
		return list;
	}
}
