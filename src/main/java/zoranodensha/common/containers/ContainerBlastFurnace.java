package zoranodensha.common.containers;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import zoranodensha.common.blocks.tileEntity.TileEntityBlastFurnace;
import zoranodensha.common.containers.slot.SlotRestrictive;



public class ContainerBlastFurnace extends ContainerInventory
{
	public ContainerBlastFurnace(InventoryPlayer inventory, TileEntityBlastFurnace tileEntity)
	{
		super(tileEntity);

		addSlotToContainer(new SlotRestrictive(tileEntity, 0, 56, 17));
		addSlotToContainer(new SlotRestrictive(tileEntity, 1, 56, 53));
		addSlotToContainer(new SlotRestrictive(tileEntity, 2, 116, 35));

		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 9; ++j)
			{
				addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}

		for (int i = 0; i < 9; ++i)
		{
			addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 142));
		}
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotId)
	{
		ItemStack itemStack = null;
		Slot slot = (Slot)inventorySlots.get(slotId);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemStack0 = slot.getStack();
			itemStack = itemStack0.copy();

			if (slotId == 2)
			{
				if (!mergeItemStack(itemStack0, 3, 39, true))
				{
					return null;
				}

				slot.onSlotChange(itemStack0, itemStack);
			}
			else if (slotId > 2)
			{
				if (tileEntity.isItemValidForSlot(0, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 0, 1, false))
					{
						return null;
					}
				}
				else if (tileEntity.isItemValidForSlot(1, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 1, 2, false))
					{
						return null;
					}
				}
				else if (tileEntity.isItemValidForSlot(2, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 2, 3, false))
					{
						return null;
					}
				}
				else if (slotId > 2 && slotId < 30)
				{
					if (!mergeItemStack(itemStack0, 30, 39, false))
					{
						return null;
					}
				}
				else if (slotId > 29 && !mergeItemStack(itemStack0, 3, 30, false))
				{
					return null;
				}
			}
			else if (!mergeItemStack(itemStack0, 3, 39, false))
			{
				return null;
			}

			if (itemStack0.stackSize == 0)
			{
				slot.putStack((ItemStack)null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemStack0.stackSize == itemStack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(player, itemStack0);
		}

		return itemStack;
	}
}
