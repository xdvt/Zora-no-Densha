package zoranodensha.common.containers.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;



public class SlotEngineerTableRecipe extends Slot
{
	public SlotEngineerTableRecipe(IInventory inventory, int id, int x, int y)
	{
		super(inventory, id, x, y);
	}

	@Override
	public ItemStack decrStackSize(int dec)
	{
		return null;
	}

	@Override
	public int getSlotStackLimit()
	{
		return 0;
	}

	@Override
	public boolean isItemValid(ItemStack itemStack)
	{
		return (itemStack != null && itemStack.stackSize == 0);
	}
}
