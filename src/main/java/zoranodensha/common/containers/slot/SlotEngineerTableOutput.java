package zoranodensha.common.containers.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;



public class SlotEngineerTableOutput extends Slot
{
	private final ItemStack[] acceptedStacks;



	public SlotEngineerTableOutput(IInventory inventory, int id, int x, int y, ItemStack... itemStacks)
	{
		super(inventory, id, x, y);

		acceptedStacks = itemStacks;
	}

	@Override
	public boolean isItemValid(ItemStack itemStack)
	{
		if (itemStack == null)
		{
			return true;
		}

		int meta = itemStack.getItemDamage();
		Item item = itemStack.getItem();

		for (ItemStack stack : acceptedStacks)
		{
			if (stack.getItemDamage() == meta && stack.getItem().equals(item))
			{
				return true;
			}
		}

		return false;
	}
}
