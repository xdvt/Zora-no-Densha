package zoranodensha.common.containers.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;



public class SlotRestrictive extends Slot
{
	private final int stackSize;



	public SlotRestrictive(IInventory inventory, int id, int x, int y)
	{
		this(inventory, id, x, y, inventory.getInventoryStackLimit());
	}

	public SlotRestrictive(IInventory inventory, int id, int x, int y, int stackSize)
	{
		super(inventory, id, x, y);

		this.stackSize = stackSize;
	}

	@Override
	public int getSlotStackLimit()
	{
		return stackSize;
	}

	@Override
	public boolean isItemValid(ItemStack itemStack)
	{
		return inventory.isItemValidForSlot(slotNumber, itemStack);
	}
}
