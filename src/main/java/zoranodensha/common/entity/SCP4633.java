package zoranodensha.common.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.world.World;
import zoranodensha.common.util.ZnDMathHelper;



public class SCP4633 extends EntityMob
{

	private static final int DW_AGE = 12;

	private static final int MAXIMUM_AGE = 20;



	public SCP4633(World world)
	{
		super(world);

		setSize(0.5F, 1.0F);
		setHealth(10.0F);
	}

	@Override
	public boolean canBePushed()
	{
		return false;
	}

	@Override
	public boolean doesEntityNotTriggerPressurePlate()
	{
		return true;
	}

	@Override
	protected void entityInit()
	{
		super.entityInit();

		getDataWatcher().addObject(DW_AGE, new Integer(0));
	}

	@Override
	protected void fall(float distance)
	{
		return;
	}

	public float getAlpha()
	{
		double age = getSCPAge() / (double)MAXIMUM_AGE;

		return (float)Math.abs(Math.sin(age * 180.0D * ZnDMathHelper.RAD_MULTIPLIER));
	}

	@Override
	protected String getHurtSound()
	{
		return null;
	}

	private int getSCPAge()
	{
		return getDataWatcher().getWatchableObjectInt(DW_AGE);
	}

	private int increaseSCPAge()
	{
		int newAge = getSCPAge() + 1;

		setSCPAge(newAge);

		return newAge;
	}

	@Override
	protected boolean isAIEnabled()
	{
		return false;
	}

	@Override
	public void onEntityUpdate()
	{
		super.onEntityUpdate();

		if (!worldObj.isRemote)
		{
			if (increaseSCPAge() > MAXIMUM_AGE)
			{
				setDead();
			}
		}
	}


	@Override
	public void onUpdate()
	{
		super.onUpdate();

		Entity cp = worldObj.getClosestPlayerToEntity(this, 256.0D);
		if (cp != null)
		{
			float interpolation = 8.0F;
			setPositionAndUpdate(posX + (cp.posX - posX) / interpolation, cp.posY - 0.3D, posZ + (cp.posZ - posZ) / interpolation);

			motionY = 0.0D;
		}

	}

	private void setSCPAge(int age)
	{
		getDataWatcher().updateObject(DW_AGE, age);
	}

	@Override
	protected void updateFallState(double d, boolean b)
	{
		return;
	}

}
