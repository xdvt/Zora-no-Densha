package zoranodensha.common.entity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import zoranodensha.api.structures.tracks.ITrackBase;



public class EntityTrackOutline extends Entity
{
	public ITrackBase trackBase;
	@SideOnly(Side.CLIENT) public boolean isClear;



	public EntityTrackOutline(World world)
	{
		super(world);

		setSize(0.1F, 0.1F);
	}

	@SideOnly(Side.CLIENT)
	public EntityTrackOutline(World world, double x, double y, double z, boolean isClear, ITrackBase trackBase)
	{
		this(world);

		setPosition(x, y, z);
		this.isClear = isClear;
		this.trackBase = trackBase;
	}

	@Override
	protected void entityInit()
	{
	}

	@Override
	@SideOnly(Side.CLIENT)
	public int getBrightnessForRender(float f0)
	{
		return 0xF00000;
	}

	@Override
	public void onEntityUpdate()
	{
		setDead();
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound p_70037_1_)
	{
	}

	@Override
	public boolean shouldRenderInPass(int pass)
	{
		return pass == 1;
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound p_70014_1_)
	{
	}
}
