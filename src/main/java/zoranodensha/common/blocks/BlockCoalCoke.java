package zoranodensha.common.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class BlockCoalCoke extends Block
{
	private static boolean registered = false;



	public BlockCoalCoke()
	{
		super(Material.rock);

		setBlockName(ModData.ID + ".blocks.coalCoke");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(5.0F);
		setResistance(10.0F);
	}


	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister iconReg)
	{
		blockIcon = iconReg.registerIcon(ModData.ID + ":blockCoalCoke");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		OreDictionary.registerOre("blockCoalCoke", this);
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this), "coalCoke", "coalCoke", "coalCoke", "coalCoke", "coalCoke", "coalCoke", "coalCoke", "coalCoke", "coalCoke"));
	}
}
