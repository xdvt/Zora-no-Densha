package zoranodensha.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.world.Explosion;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.fluids.EFluid;



/**
 * Explosion code taken from Buildcraft's GitHub.
 */
public class BlockFluidFlammable extends BlockFluid
{
	public BlockFluidFlammable(Fluid fluid, EFluid type)
	{
		super(fluid, type);
	}

	@Override
	public boolean canDropFromExplosion(Explosion explosion)
	{
		return false;
	}

	@Override
	public int getFireSpreadSpeed(IBlockAccess world, int x, int y, int z, ForgeDirection face)
	{
		return 300;
	}

	@Override
	public int getFlammability(IBlockAccess world, int x, int y, int z, ForgeDirection face)
	{
		return 300;
	}

	@Override
	public boolean isFireSource(World world, int x, int y, int z, ForgeDirection side)
	{
		return true;
	}

	@Override
	public boolean isFlammable(IBlockAccess world, int x, int y, int z, ForgeDirection face)
	{
		return true;
	}

	@Override
	public void onBlockExploded(World world, int x, int y, int z, Explosion explosion)
	{
		if (ModCenter.cfg.blocks_items.enableFuelExplosion)
		{
			world.setBlockToAir(x, y, z);

			for (ForgeDirection dir : ForgeDirection.VALID_DIRECTIONS)
			{
				Block block = world.getBlock(x + dir.offsetX, y + dir.offsetY, z + dir.offsetZ);

				if (block instanceof BlockFluid)
				{
					world.scheduleBlockUpdate(x + dir.offsetX, y + dir.offsetY, z + dir.offsetZ, block, 2);
				}
				else
				{
					world.notifyBlockOfNeighborChange(x + dir.offsetX, y + dir.offsetY, z + dir.offsetZ, block);
				}
			}

			onBlockDestroyedByExplosion(world, x, y, z, explosion);
		}
		else
		{
			super.onBlockExploded(world, x, y, z, explosion);
		}
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		super.onNeighborBlockChange(world, x, y, z, block);

		if (world.provider.dimensionId == -1 && ModCenter.cfg.blocks_items.enableFuelExplosion)
		{
			world.setBlockToAir(x, y, z);
			world.newExplosion(null, x, y, z, 3.0F, true, true);
		}
	}
}
