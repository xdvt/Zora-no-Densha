package zoranodensha.common.blocks;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import zoranodensha.common.blocks.tileEntity.TileEntityBallastGravel;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



public class BlockBallastGravelCorner extends BlockBallastGravel implements ITileEntityProvider
{
	public BlockBallastGravelCorner()
	{
		super();

		isBlockContainer = true;
		setBlockName(ModData.ID + ".blocks.ballastGravelCorner");
		setCreativeTab(null);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB axisAlignedBB, List list, Entity entity)
	{
		int meta = world.getBlockMetadata(x, y, z);

		if (meta > 8)
		{
			super.addCollisionBoxesToList(world, x, y, z, axisAlignedBB, list, entity);
		}
		else if (meta > 0)
		{
			float[] f;

			for (int i = 1; i < 5; ++i)
			{
				f = getArray(meta, 0.25F * i);
				setBlockBounds(f[0], f[1], f[2], f[3], f[4], f[5]);
				super.addCollisionBoxesToList(world, x, y, z, axisAlignedBB, list, entity);
			}
		}
		else
		{
			super.addCollisionBoxesToList(world, x, y, z, axisAlignedBB, list, entity);
		}

		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int oldMeta)
	{
		super.breakBlock(world, x, y, z, block, oldMeta);
		world.removeTileEntity(x, y, z);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityBallastGravel();
	}

	private float[] getArray(int meta, float f)
	{
		if (meta > 4)
		{
			return new float[] { (meta < 7 ? f : 0.0F), 0.0F, (meta == 6 || meta == 7 ? f : 0.0F), (meta > 6 ? 1.0F - f : 1.0F), f, (meta == 5 || meta == 8 ? 1.0F - f : 1.0F) };
		}

		return new float[] { (meta == 2 ? f : 0.0F), 0.0F, (meta == 3 ? f : 0.0F), (meta == 4 ? 1.0F - f : 1.0F), f, (meta == 1 ? 1.0F - f : 1.0F) };
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean getCanBlockGrass()
	{
		return false;
	}

	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer player)
	{
		return ModCenter.BlockBallastGravel.getPickBlock(target, world, x, y, z, player);
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean isSideSolid(IBlockAccess world, int x, int y, int z, ForgeDirection side)
	{
		return side.equals(ForgeDirection.DOWN);
	}

	@Override
	public boolean onBlockEventReceived(World world, int x, int y, int z, int eventNum, int arg)
	{
		super.onBlockEventReceived(world, x, y, z, eventNum, arg);
		TileEntity tileEntity = world.getTileEntity(x, y, z);

		return (tileEntity != null ? tileEntity.receiveClientEvent(eventNum, arg) : false);
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean shouldSideBeRendered(IBlockAccess world, int x, int y, int z, int side)
	{
		if (side != 0)
		{
			int meta = world.getBlockMetadata(x, y, z);

			if (meta < 5 || meta > 8)
			{
				switch (side)
				{
					case 1:
						return !(meta == 0);

					case 2:
						return !(meta == 1 || meta == 9 || meta == 12);

					case 3:
						return !(meta == 3 || meta == 10 || meta == 11);

					case 4:
						return !(meta == 4 || meta == 11 || meta == 12);

					case 5:
						return !(meta == 2 || meta == 10 || meta == 9);
				}
			}
		}

		return true;
	}
}
