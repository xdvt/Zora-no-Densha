package zoranodensha.common.blocks.material;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;



public class MaterialTrack extends Material
{
	public MaterialTrack()
	{
		super(MapColor.ironColor);

		setRequiresTool();
	}
}
