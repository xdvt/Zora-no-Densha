package zoranodensha.common.blocks;

import java.util.ArrayList;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackDecoration;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.items.ItemTrackPart;



public class BlockTrackDecoration extends BlockContainer
{
	public BlockTrackDecoration()
	{
		super(new Material(MapColor.ironColor));

		setBlockName(ModData.ID + ".blocks.trackDecoration");
		setHardness(0.4F);
	}
	
	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta)
	{
		dropBlockAsItem(world, x, y, z, meta, 0);
		super.breakBlock(world, x, y, z, block, meta);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityTrackDecoration();
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		setBlockBoundsBasedOnState(world, x, y, z);
		return AxisAlignedBB.getBoundingBox(x + minX, y + minY, z + minZ, x + maxX, y + maxY, z + maxZ);
	}

	@Override
	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune)
	{
		ArrayList<ItemStack> drops = new ArrayList<ItemStack>();

		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof TileEntityTrackDecoration)
		{
			drops.add(new ItemStack(ModCenter.ItemTrackPart, ((TileEntityTrackDecoration)tileEntity).getPileSize() + 1, (metadata / 4)));
		}
		
		return drops;
	}

	@Override
	public Item getItemDropped(int meta, Random ran, int i)
	{
		return ModCenter.ItemTrackPart;
	}

	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer player)
	{
		return new ItemStack(ModCenter.ItemTrackPart, 1, (world.getBlockMetadata(x, y, z) / 4));
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float xf, float yf, float zf)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof TileEntityTrackDecoration)
		{
			TileEntityTrackDecoration tileEntityDecoration = (TileEntityTrackDecoration)tileEntity;

			int pileSize = tileEntityDecoration.getPileSize();
			int meta = world.getBlockMetadata(x, y, z);
			ItemStack playerItem = player.getHeldItem();

			if (playerItem != null && playerItem.getItem() instanceof ItemTrackPart && playerItem.getItemDamage() == (meta / 4) && pileSize < 7 && tileEntityDecoration.changePileSize(0))
			{
				if (!player.capabilities.isCreativeMode)
				{
					--playerItem.stackSize;
				}

				return true;
			}
			else if ((playerItem == null || !(playerItem.getItem() instanceof ItemTrackPart)) && tileEntityDecoration.changePileSize(1))
			{
				player.inventory.addItemStackToInventory(new ItemStack(ModCenter.ItemTrackPart, 1, (meta / 4)));

				return true;
			}
		}

		return false;
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!world.isRemote && world.isAirBlock(x, y - 1, z))
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);
			if (tileEntity instanceof TileEntityTrackDecoration)
			{
				this.dropBlockAsItem(world, x, y, z, new ItemStack(ModCenter.ItemTrackPart, ((TileEntityTrackDecoration)tileEntity).getPileSize() + 1, (world.getBlockMetadata(x, y, z) / 4)));
			}

			world.setBlockToAir(x, y, z);
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		blockIcon = icon.registerIcon(ModData.ID + ":blockCollisionbox");
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof TileEntityTrackDecoration)
		{
			int size = ((TileEntityTrackDecoration)tileEntity).getPileSize();

			if (size < 5)
			{
				setBlockBounds(0F, 0F, 0F, 1F, (world.getBlockMetadata(x, y, z) % 4 < 2 ? 0.12375F : 0.09375F), 1F);
			}
			else
			{
				setBlockBounds(0F, 0F, 0F, 1F, (world.getBlockMetadata(x, y, z) % 4 < 2 ? 0.06125F : 0.09375F), 1F);
			}
		}
		else
		{
			setBlockBounds(0F, 0F, 0F, 1F, 1F, 1F);
		}
	}
}
