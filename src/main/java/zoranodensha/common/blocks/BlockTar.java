package zoranodensha.common.blocks;

import java.util.Random;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class BlockTar extends Block
{
	private static boolean registered = false;



	public BlockTar()
	{
		super(Material.rock);

		setBlockName(ModData.ID + ".blocks.tar");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(5.0F);
		setResistance(10.0F);
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		setBlockBoundsBasedOnState(world, x, y, z);

		return AxisAlignedBB.getBoundingBox(x + minX, y + minY, z + minZ, x + maxX, y + maxY, z + maxZ);
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean isSideSolid(IBlockAccess world, int x, int y, int z, ForgeDirection side)
	{
		return (world.getBlockMetadata(x, y, z) == 7 ? true : side.equals(ForgeDirection.DOWN));
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float fx, float fy, float fz)
	{
		if (player != null)
		{
			ItemStack playerItem = player.getHeldItem();

			if (playerItem != null && playerItem.getItem() != null)
			{
				if (playerItem.getItem() == Item.getItemFromBlock(ModCenter.BlockTar))
				{
					Block block = world.getBlock(x, y, z);
					int meta = world.getBlockMetadata(x, y, z);

					if (block instanceof BlockTar && meta < 7)
					{
						if (!player.capabilities.isCreativeMode)
						{
							--playerItem.stackSize;
						}

						return world.setBlockMetadataWithNotify(x, y, z, meta + 1, 2);
					}
				}
				else if (playerItem.getItem().getHarvestLevel(playerItem, "shovel") > 0)
				{
					Block block = world.getBlock(x, y, z);
					int meta = world.getBlockMetadata(x, y, z);

					if (block instanceof BlockTar && meta > 0)
					{
						world.setBlockMetadataWithNotify(x, y, z, meta - 1, 2);

						if (player.capabilities.isCreativeMode)
						{
							return true;
						}

						return player.inventory.addItemStackToInventory(new ItemStack(ModCenter.BlockTar));
					}
				}
			}
		}

		return false;
	}

	@Override
	public int quantityDropped(int meta, int fortune, Random random)
	{
		return (meta + 1);
	}


	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister iconReg)
	{
		blockIcon = iconReg.registerIcon(ModData.ID + ":blockTar");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}
		registered = true;

		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 8), ModCenter.ItemBucketTar));
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, (0.125F * (world.getBlockMetadata(x, y, z) + 1)), 1.0F);
	}

	@Override
	public void setBlockBoundsForItemRender()
	{
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
	}
}
