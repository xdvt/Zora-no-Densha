package zoranodensha.common.blocks;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mods.railcraft.api.carts.bore.IMineable;
import net.minecraft.block.BlockOre;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;



public class BlockOreShale extends BlockOre implements IMineable
{
	private static boolean registered = false;



	public BlockOreShale()
	{
		super();

		setBlockName(ModData.ID + ".blocks.ore.shale");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(3.0F);
		setResistance(5.0F);
		setStepSound(soundTypePiston);
	}

	@Override
	public boolean canMineBlock(World world, int x, int y, int z, EntityMinecart bore, ItemStack head)
	{
		return world.getBlock(x, y, z) == ModCenter.BlockOreShale;
	}

	@Override
	public int getExpDrop(IBlockAccess world, int metadata, int fortuneModifier)
	{
		return (fortuneModifier > 0 ? ZnDMathHelper.ranInt(2, 4) + ZnDMathHelper.ran.nextInt(fortuneModifier) : ZnDMathHelper.ranInt(2, 4));
	}

	@Override
	public Item getItemDropped(int i0, Random ran, int i1)
	{
		return ModCenter.ItemOilShale;
	}

	@Override
	public int quantityDropped(Random ran)
	{
		return 2 + ran.nextInt(3);
	}


	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister iconReg)
	{
		blockIcon = iconReg.registerIcon(ModData.ID + ":blockOreShale");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		OreDictionary.registerOre("oreOilShale", this);
	}
}
