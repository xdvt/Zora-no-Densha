package zoranodensha.common.blocks.tileEntity;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;
import zoranodensha.common.blocks.BlockBlastFurnace;
import zoranodensha.common.blocks.BlockCokeOven;
import zoranodensha.common.blocks.BlockRefinery;
import zoranodensha.common.core.ModCenter;



public class TileEntityItemFluidHandler extends TileEntity implements IFluidHandler, ISidedInventory
{
	private IFluidHandler iFluidHandler;
	private ISidedInventory iSidedInventory;
	private int[] coords = new int[] { -1, -1, -1 };
	private int timer;



	public TileEntityItemFluidHandler()
	{
	}

	@Override
	public boolean canDrain(ForgeDirection from, Fluid fluid)
	{
		return refreshInterfacesWithCheck(0) && iFluidHandler.canDrain(from, fluid);
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side)
	{
		return refreshInterfacesWithCheck(1) && iSidedInventory.canExtractItem(slot, itemStack, side);
	}

	@Override
	public boolean canFill(ForgeDirection from, Fluid fluid)
	{
		return refreshInterfacesWithCheck(0) && iFluidHandler.canFill(from, fluid);
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack itemStack, int side)
	{
		return refreshInterfacesWithCheck(1) && iSidedInventory.canInsertItem(slot, itemStack, side);
	}

	@Override
	public void closeInventory()
	{
		if (refreshInterfacesWithCheck(1))
		{
			iSidedInventory.closeInventory();
		}
	}

	@Override
	public ItemStack decrStackSize(int slot, int decr)
	{
		return (refreshInterfacesWithCheck(1) ? iSidedInventory.decrStackSize(slot, decr) : null);
	}

	@Override
	public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain)
	{
		return (refreshInterfacesWithCheck(0) ? iFluidHandler.drain(from, resource, doDrain) : null);
	}

	@Override
	public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain)
	{
		return (refreshInterfacesWithCheck(0) ? iFluidHandler.drain(from, maxDrain, doDrain) : null);
	}

	@Override
	public int fill(ForgeDirection from, FluidStack resource, boolean doFill)
	{
		return (refreshInterfacesWithCheck(0) ? iFluidHandler.fill(from, resource, doFill) : 0);
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		return (refreshInterfacesWithCheck(1) ? iSidedInventory.getAccessibleSlotsFromSide(side) : new int[0]);
	}

	@Override
	public String getInventoryName()
	{
		return (refreshInterfacesWithCheck(1) ? iSidedInventory.getInventoryName() : "");
	}

	@Override
	public int getInventoryStackLimit()
	{
		return (refreshInterfacesWithCheck(1) ? iSidedInventory.getInventoryStackLimit() : 0);
	}

	@Override
	public int getSizeInventory()
	{
		return (refreshInterfacesWithCheck(1) ? iSidedInventory.getSizeInventory() : 0);
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return (refreshInterfacesWithCheck(1) ? iSidedInventory.getStackInSlot(slot) : null);
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot)
	{
		return (refreshInterfacesWithCheck(1) ? iSidedInventory.getStackInSlotOnClosing(slot) : null);
	}

	@Override
	public FluidTankInfo[] getTankInfo(ForgeDirection from)
	{
		return (refreshInterfacesWithCheck(0) ? iFluidHandler.getTankInfo(from) : new FluidTankInfo[0]);
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return refreshInterfacesWithCheck(1) && iSidedInventory.hasCustomInventoryName();
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack)
	{
		return refreshInterfacesWithCheck(1) && iSidedInventory.isItemValidForSlot(slot, itemStack);
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return refreshInterfacesWithCheck(1) && iSidedInventory.isUseableByPlayer(player);
	}

	@Override
	public void openInventory()
	{
		if (refreshInterfacesWithCheck(1))
		{
			iSidedInventory.openInventory();
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		for (int i = 0; i < coords.length; ++i)
		{
			coords[i] = nbt.getInteger("Coordinates_" + i);
		}
	}

	public void refreshInterfaces()
	{
		if (hasWorldObj())
		{
			TileEntity tileEntity = worldObj.getTileEntity(coords[0], coords[1], coords[2]);

			if (tileEntity instanceof IFluidHandler)
			{
				iFluidHandler = (IFluidHandler)tileEntity;
			}

			if (tileEntity instanceof ISidedInventory)
			{
				iSidedInventory = (ISidedInventory)tileEntity;
			}

			if (iSidedInventory == null && iFluidHandler == null)
			{
				Block block = getBlockType();

				if (block == ModCenter.BlockBlastFurnace)
				{
					tileEntity = ((BlockBlastFurnace)block).getTileCoordinates(worldObj, xCoord, yCoord, zCoord);
				}
				else if (block == ModCenter.BlockCokeOven)
				{
					tileEntity = ((BlockCokeOven)block).getTileCoordinates(worldObj, xCoord, yCoord, zCoord);
				}
				else if (block == ModCenter.BlockRefinery)
				{
					tileEntity = ((BlockRefinery)block).getTileCoordinates(worldObj, xCoord, yCoord, zCoord, getBlockMetadata());
				}
				else if (block == ModCenter.BlockRetorter)
				{
					switch (getBlockMetadata())
					{
						case 2:
							tileEntity = worldObj.getTileEntity(xCoord, yCoord, zCoord - 1);
							break;

						case 3:
							tileEntity = worldObj.getTileEntity(xCoord - 1, yCoord, zCoord);
							break;

						case 4:
							tileEntity = worldObj.getTileEntity(xCoord, yCoord, zCoord + 1);
							break;

						case 5:
							tileEntity = worldObj.getTileEntity(xCoord + 1, yCoord, zCoord);
							break;
					}
				}

				if (tileEntity != null)
				{
					if (tileEntity instanceof IFluidHandler)
					{
						iFluidHandler = (IFluidHandler)tileEntity;
					}

					if (tileEntity instanceof ISidedInventory)
					{
						iSidedInventory = (ISidedInventory)tileEntity;
					}

					coords[0] = tileEntity.xCoord;
					coords[1] = tileEntity.yCoord;
					coords[2] = tileEntity.zCoord;
				}
			}

			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
		}
	}

	private boolean refreshInterfacesWithCheck(int type)
	{
		if (type == 0)
		{
			if (iFluidHandler == null)
			{
				refreshInterfaces();
			}

			return iFluidHandler != null;
		}

		if (iSidedInventory == null)
		{
			refreshInterfaces();
		}

		return iSidedInventory != null;
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack itemStack)
	{
		if (refreshInterfacesWithCheck(1))
		{
			iSidedInventory.setInventorySlotContents(slot, itemStack);
		}
	}

	@Override
	public void updateEntity()
	{
		if (timer > 0)
		{
			--timer;
		}
		else
		{
			if (iFluidHandler == null && iSidedInventory == null)
			{
				refreshInterfaces();
				timer = ModCenter.cfg.blocks_items.multiBlockUpdate;
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		for (int i = 0; i < coords.length; ++i)
		{
			nbt.setInteger("Coordinates_" + i, coords[i]);
		}
	}
}
