package zoranodensha.common.blocks.tileEntity;

import java.util.ArrayList;
import java.util.Iterator;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.EEngineerTableTab;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.containers.ContainerEngineerTable;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.registry.ModRecipeRegistry;
import zoranodensha.common.core.registry.ModRecipeRegistry.EngineerTableRecipe;
import zoranodensha.common.util.Helpers;



public class TileEntityEngineerTable extends TileEntity implements ISidedInventory
{
	private static final int[] slotsAccept = new int[] { 0, 1, 2 };
	private static final int[] slotsReject = new int[] { };
	private static final String NBT_KEY_USEDBY = "UsedBy";

	/** All statically saved Presets from imported files */
	public static ArrayList<ItemStack> presetsImport = new ArrayList<ItemStack>();
	/** The status of the "crafting" process of the blueprint */
	public int process;
	/** The current tab's page */
	public int page;
	/** ID of the EntityPlayer using this EngineerTable */
	private int usedByID = -1;
	/** The EntityPlayer using this EngineerTable */
	private EntityPlayer usedBy;
	/** The NBTTagCompound of the currently producing train. {@code null} if there is no train blueprint being printed at the moment */
	public NBTTagCompound nbtTrain;
	/** The current tab */
	public EEngineerTableTab tab = EEngineerTableTab.TRACKS;
	/** The EngineerTableRecipe instance that currently matches the recipe matrix. Null if there's no match. */
	private EngineerTableRecipe recipeMatching;
	/** A custom name for this block, if set */
	private String customName;
	/** The contents of the process slots */
	public ItemStack[] slots = new ItemStack[43]; // 0: Paper | 1: Ink | 2: Output | [3;17]: Tables | [18;42]: Recipe
	/** All saved Presets */
	public ArrayList<ItemStack> presets = new ArrayList<ItemStack>();



	public TileEntityEngineerTable()
	{
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side)
	{
		return (side == 1);
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack itemStack, int side)
	{
		return isItemValidForSlot(slot, itemStack);
	}

	@Override
	public void closeInventory()
	{
	}

	@Override
	public ItemStack decrStackSize(int slot, int dec)
	{
		if (slot > -1 && slot < slots.length && slots[slot] != null)
		{
			ItemStack itemstack;

			if (slots[slot].stackSize <= dec)
			{
				itemstack = slots[slot];

				slots[slot] = null;
				markForUpdate();

				return itemstack;
			}

			itemstack = slots[slot].splitStack(dec);

			if (slots[slot].stackSize == 0)
			{
				slots[slot] = null;
			}

			markForUpdate();

			return itemstack;
		}

		return null;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		return (side == 1 ? slotsAccept : slotsReject);
	}

	protected boolean getCanProcess()
	{
		return (slots[0] != null && slots[1] != null && slots[2] == null && isRecipeValid());
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		nbt.setInteger(NBT_KEY_USEDBY, usedByID);

		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, nbt);
	}

	@Override
	public String getInventoryName()
	{
		return (hasCustomInventoryName() ? customName : worldObj.getBlock(xCoord, yCoord, zCoord).getUnlocalizedName());
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 1;
	}

	public int getPageLimit()
	{
		int tabLength = ModRecipeRegistry.getTabLength(tab.toInt());

		if (tab.isTrainTab())
		{
			tabLength += presets.size() + TileEntityEngineerTable.presetsImport.size();
		}

		return (tabLength / 15);
	}

	public int getProgress()
	{
		return (process * 18 / 100);
	}

	/**
	 * Returns the first EngineerTableRecipe instance that matches the given matrix.
	 */
	public EngineerTableRecipe getRecipeFirstMatching(ItemStack[] matrix)
	{
		switch (tab)
		{
			case TRACKS:
				Iterator<EngineerTableRecipe> itera = ModRecipeRegistry.getRecipesTracks().iterator();

				while (itera.hasNext())
				{
					EngineerTableRecipe recipe = itera.next();

					if (recipe.getIsRecipeMatching(matrix))
					{
						return recipe;
					}
				}
				break;
		}

		return null;
	}

	@Override
	public int getSizeInventory()
	{
		return (tab.isTrainTab() ? (slots.length - 25) : slots.length);
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		if (tab.isTrainTab())
		{
			if (slot < slots.length - 25 && slot > -1)
			{
				return slots[slot];
			}
		}
		else if (slot < slots.length && slot > -1)
		{
			return slots[slot];
		}

		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot)
	{
		if (slot >= 0 && slot < getSizeInventory() && slots[slot] != null)
		{
			ItemStack itemStack = slots[slot];
			slots[slot] = null;

			return itemStack;
		}

		return null;
	}

	public int getUsedBy()
	{
		return usedByID;
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return !Helpers.isUndefined(customName);
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack)
	{
		return ((slot > 1) ? false : (slot == 1) ? (itemStack.getItem() == Items.dye && itemStack.getItemDamage() == 0) : (itemStack.getItem() == Items.paper));
	}

	public boolean isProcessing()
	{
		return process > 0;
	}

	public boolean isRecipeValid()
	{
		return (tab.isTrainTab() ? nbtTrain != null : recipeMatching != null);
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this && player.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D, zCoord + 0.5D) <= 64.0D;
	}

	public void markForUpdate()
	{
		markDirty();
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
	}

	public void notifyOfButtonPressed(int button)
	{
		if (button >= 0)
		{
			if (button < 3)
			{
				EEngineerTableTab tab = EEngineerTableTab.getFromInt(button);

				if (this.tab != tab)
				{
					page = 0;
					nbtTrain = null;
				}

				this.tab = tab;
			}
			else if (button == 3)
			{
				--page;

				if (page < 0)
				{
					page = 0;
				}
			}
			else if (button == 4)
			{
				++page;

				int max = getPageLimit();

				if (page > max)
				{
					page = max;
				}
			}

			markForUpdate();
		}
	}

	@Override
	public void onDataPacket(NetworkManager networkManager, S35PacketUpdateTileEntity packet)
	{
		NBTTagCompound nbt = packet.func_148857_g();
		readFromNBT(nbt);

		if (nbt.hasKey(NBT_KEY_USEDBY))
		{
			usedByID = nbt.getInteger(NBT_KEY_USEDBY);
		}
	}

	/**
	 * Called upon change to presets.
	 *
	 * @param nbt - The NBTTagCompound of the preset in question
	 * @param flag - Determines the action to take using the given preset; remove (0), save (1), move to begin (2), create blueprint (3).
	 * @return True if successful.
	 */
	public boolean onPresetChange(NBTTagCompound nbt, byte flag)
	{
		if (nbt == null)
		{
			return false;
		}

		String name = VehicleHelper.getVehicleName(nbt);
		if (Helpers.isUndefined(name))
		{
			return false;
		}

		if (flag == 3)
		{
			nbtTrain = nbt;
			return true;
		}

		NBTTagCompound nbt0;
		ItemStack itemStack;
		int i;

		for (i = 0; i < presets.size(); ++i)
		{
			itemStack = presets.get(i);

			if (itemStack != null)
			{
				nbt0 = itemStack.getTagCompound();
				if (nbt0 != null && name.equalsIgnoreCase(VehicleHelper.getVehicleName(nbt0)))
				{
					itemStack = presets.remove(i);

					if (flag == 2)
					{
						presets.add(0, itemStack);
					}

					break;
				}
			}
		}

		if (flag == 1)
		{
			itemStack = new ItemStack(ModCenter.ItemTrain, 0, 0);
			itemStack.setTagCompound(nbt);
			presets.add(i, itemStack);
		}

		markForUpdate();

		return true;
	}

	@Override
	public void openInventory()
	{
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		slots = new ItemStack[slots.length];
		presets = new ArrayList<ItemStack>();
		process = nbt.getInteger("Process");
		NBTTagList tagList = nbt.getTagList("Items", 10);
		NBTTagCompound nbt0;

		for (int i = 0; i < tagList.tagCount(); ++i)
		{
			nbt0 = tagList.getCompoundTagAt(i);
			byte b = nbt0.getByte("Slot");

			if (b >= 0 && b < slots.length)
			{
				try
				{
					slots[b] = ItemStack.loadItemStackFromNBT(nbt0);
				}
				catch (Throwable t)
				{
				}
			}
		}

		tagList = nbt.getTagList("Presets", 10);
		ItemStack itemStack;

		for (int i = 0; i < tagList.tagCount(); ++i)
		{
			try
			{
				itemStack = ItemStack.loadItemStackFromNBT(tagList.getCompoundTagAt(i));
				presets.add(itemStack);
			}
			catch (Throwable t)
			{
			}
		}

		if (nbt.hasKey("CustomName", 8))
		{
			customName = nbt.getString("CustomName");
		}
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack itemStack)
	{
		if (slot < 0 || slot >= getSizeInventory())
		{
			return;
		}

		slots[slot] = itemStack;

		if (slot >= 18 && slot <= 42)
		{
			ItemStack[] matrix = new ItemStack[25];

			for (int i = 0; i < 25; ++i)
			{
				matrix[i] = slots[i + 18];
			}

			recipeMatching = getRecipeFirstMatching(matrix);
		}

		if (itemStack != null && itemStack.stackSize > getInventoryStackLimit())
		{
			itemStack.stackSize = getInventoryStackLimit();
		}

		markForUpdate();
	}

	public void setIsUsed(EntityPlayer entity)
	{
		if (!worldObj.isRemote)
		{
			usedByID = (entity != null ? entity.getEntityId() : -1);
			usedBy = entity;
			markForUpdate();
		}
	}

	@Override
	public void updateEntity()
	{
		if (worldObj.isRemote)
		{
			return;
		}

		if (!(usedBy != null && usedBy.openContainer instanceof ContainerEngineerTable))
		{
			setIsUsed(null);
		}

		if (getCanProcess())
		{
			if (process > 100)
			{
				ItemStack result = null;

				if (tab.isTrainTab())
				{
					result = new ItemStack(ModCenter.ItemTrain);
					nbtTrain.setBoolean(Train.EDataKey.IS_BLUEPRINT.key, true);
					result.setTagCompound(nbtTrain);
					nbtTrain = null;
				}
				else if (recipeMatching != null)
				{
					result = recipeMatching.getResult();

					if (result.stackSize < 1)
					{
						result.stackSize = 1;
					}
				}

				if (result != null)
				{
					process = 0;
					decrStackSize(0, 1);
					decrStackSize(1, 1);
					setInventorySlotContents(2, result);
					markForUpdate();
				}
			}
			else
			{
				++process;
			}
		}
		else
		{
			process = 0;
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		NBTTagList tagList = new NBTTagList();
		NBTTagCompound nbt0;

		for (int i = 0; i < slots.length; ++i)
		{
			if (slots[i] != null)
			{
				nbt0 = new NBTTagCompound();
				slots[i].writeToNBT(nbt0).setByte("Slot", (byte)i);
				tagList.appendTag(nbt0);
			}
		}

		nbt.setInteger("Process", process);
		nbt.setTag("Items", tagList);

		tagList = new NBTTagList();
		Iterator<ItemStack> itera = presets.iterator();

		while (itera.hasNext())
		{
			try
			{
				nbt0 = new NBTTagCompound();
				itera.next().writeToNBT(nbt0);
				tagList.appendTag(nbt0);
			}
			catch (Exception e)
			{
				ModCenter.log.warn("[TileEntityEngineerTable] Caught an exception while writing preset data to NBT. (@ " + xCoord + "x " + yCoord + "y " + zCoord + "z)");
			}
		}

		nbt.setTag("Presets", tagList);

		if (hasCustomInventoryName())
		{
			nbt.setString("CustomName", customName);
		}
	}
}
