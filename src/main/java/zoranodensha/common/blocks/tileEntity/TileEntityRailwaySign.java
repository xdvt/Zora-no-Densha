package zoranodensha.common.blocks.tileEntity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import zoranodensha.common.core.ModCenter;



public class TileEntityRailwaySign extends TileEntity
{
	/** The integer representation of this sign's content(s). Index 0 is the top, index 1 is the bottom sign. */
	int[][] data = new int[][] { { -1, -1 }, { -1, -1 } };



	public TileEntityRailwaySign()
	{
	}

	@Override
	public boolean canUpdate()
	{
		return false;
	}

	/**
	 * Clears the sign of the given slot.
	 */
	public void clearSignIn(int slot)
	{
		data[slot][0] = -1;
		data[slot][1] = -1;
		markForUpdate();
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);

		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, nbt);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public double getMaxRenderDistanceSquared()
	{
		return (ModCenter.cfg.rendering.detailOther > 0 ? 16384.0D : super.getMaxRenderDistanceSquared());
	}

	@SideOnly(Side.CLIENT)
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord, xCoord + 1.0D, yCoord + 1.8D, zCoord + 1.0D);
	}

	/**
	 * Returns an integer representing the Metadata of the given slot.
	 * Metadata is defined as:
	 * 0 - Facing south
	 * 1 - Facing west
	 * 2 - Facing north
	 * 3 - Facing east
	 */
	public int getRotationMetadata(int slot)
	{
		return data[slot][1];
	}

	/**
	 * Returns an integer representation of the ERailwaySign of the given slot.
	 */
	public int getSignID(int slot)
	{
		return data[slot][0];
	}

	public void markForUpdate()
	{
		markDirty();
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
	}

	@Override
	public void onDataPacket(NetworkManager networkManager, S35PacketUpdateTileEntity packet)
	{
		readFromNBT(packet.func_148857_g());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		for (int i = 0; i < 2; ++i)
		{
			for (int j = 0; j < 2; ++j)
			{
				data[i][j] = nbt.getInteger("data_" + i + "_" + j);
			}
		}
	}

	/**
	 * Sets a pair of integers as 1) the integer representation of the ERailwaySign and 2) its Metadata rotation.
	 *
	 * @return True if successful.
	 */
	public boolean setSignIn(int slot, int sign, int meta)
	{
		if (data[slot][0] != -1 || meta < 0 || meta > 3)
		{
			return false;
		}

		data[slot][0] = sign;
		data[slot][1] = meta;
		markForUpdate();

		return true;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		for (int i = 0; i < 2; ++i)
		{
			for (int j = 0; j < 2; ++j)
			{
				nbt.setInteger("data_" + i + "_" + j, data[i][j]);
			}
		}
	}
}
