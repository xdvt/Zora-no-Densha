package zoranodensha.common.blocks.tileEntity;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import zoranodensha.common.core.ModCenter;



public class TileEntityBallastGravel extends TileEntity
{
	@SideOnly(Side.CLIENT) private AxisAlignedBB renderBoundingBox;



	@Override
	public boolean canUpdate()
	{
		return false;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public double getMaxRenderDistanceSquared()
	{
		return (ModCenter.cfg.rendering.detailTracks > 0 ? 16384.0D : super.getMaxRenderDistanceSquared());
	}

	@SideOnly(Side.CLIENT)
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		if (renderBoundingBox == null)
		{
			renderBoundingBox = AxisAlignedBB.getBoundingBox(xCoord - 0.5D, yCoord - 0.5D, zCoord - 0.5D, xCoord + 1.5D, yCoord + 1.5D, zCoord + 1.5D);
		}

		return renderBoundingBox;
	}
}
