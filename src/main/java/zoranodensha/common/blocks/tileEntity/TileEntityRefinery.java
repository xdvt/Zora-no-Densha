package zoranodensha.common.blocks.tileEntity;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;
import zoranodensha.common.blocks.BlockRefinery;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.ZnDMathHelper;



public class TileEntityRefinery extends TileEntityContainerInventory implements IFluidHandler
{
	private static final int[] slotsAccept = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	private static final int[] slotsReject = new int[] { };
	private static final Map<Fluid, int[]> fluids = new HashMap<Fluid, int[]>();
	
	static
	{
		// Type; Temp. in deg.; Time in ticks; Min. amount in mB; output1; output2; output3; output4; output5.
		fluids.put(ModCenter.FluidOilBioCrude, new int[] { 0, 65, 400, 1000, 200, 325, 475, 0, 0 });
		fluids.put(ModCenter.FluidOilShaleCrude, new int[] { 1, 350, 220, 1000, 150, 150, 300, 200, 200 });
	}
	
	private final FluidTank[] tanks = new FluidTank[6];
	private int gasFuel;
	private int slotMoveCooldown;
	public float temperature = 20.0F;
	private Random ran = new Random();
	
	/*
	 * 0 = Bucket Input
	 * 1 = Fuel Input
	 * 2 = Bucket Output
	 * 3~7 = Bucket Input
	 * 8~12 = Bucket Output
	 */
	public TileEntityRefinery()
	{
		super(new ItemStack[13], "refinery");
		
		Fluid[] tankFluids = getTankFluidsFromType(null);
		for (int i = 1; i < tanks.length; ++i)
		{
			tanks[i] = new FluidTank(tankFluids[i-1], 0, 16000);
		}
		tanks[0] = new FluidTank(ModCenter.FluidOilShaleCrude, 0, 16000);
	}


	@Override
	public boolean canDrain(ForgeDirection from, Fluid fluid)
	{
		return (from.equals(ForgeDirection.DOWN) && !fluids.containsKey(fluid));
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side)
	{
		return (side == 0 && (slot == 2 || slot > 7));
	}

	@Override
	public boolean canFill(ForgeDirection from, Fluid fluid)
	{
		return from.equals(ForgeDirection.DOWN) && fluids.containsKey(fluid);
	}

	@Override
	protected boolean canFinish()
	{
		FluidStack tankFluid = tanks[0].getFluid();
		if (tankFluid != null)
		{
			return fluids.containsKey(tankFluid.getFluid()) && data[2] >= fluids.get(tankFluid.getFluid())[2];
		}
		return false;
	}

	@Override
	public boolean canInsertItem(int slot, ItemStack itemStack, int side)
	{
		return (slot != 2 && slot < 8) && super.canInsertItem(slot, itemStack, side);
	}

	@Override
	protected boolean canProcess()
	{
		if (tanks[0].getFluid() == null)
		{
			return false;
		}
		
		Fluid fluid = tanks[0].getFluid().getFluid();
		if (fluids.containsKey(fluid))
		{
			int[] i0 = fluids.get(fluid);
			int i1 = 0;
			int i2 = 0;

			for (int i3 = 1; i3 < tanks.length; ++i3)
			{
				switch (i3)
				{
					case 1:

						i2 = tanks[i3].fill(new FluidStack(i0[0] == 0 ? ModCenter.FluidOilCreosote : ModCenter.FluidOilMachinery, i0[i3 + 3]), false);

						if (i2 > 0 && i2 >= (i0[i3 + 3] / 2))
						{
							++i1;
						}
						else if (tanks[i3].getFluidAmount() == 0)
						{
							tanks[i3].setFluid(new FluidStack((i0[0] == 0 ? ModCenter.FluidOilCreosote : ModCenter.FluidOilMachinery), 0));
							++i1;
						}
						break;

					case 2:

						i2 = tanks[i3].fill(new FluidStack(i0[0] == 0 ? FluidRegistry.WATER : ModCenter.FluidOilLubricant, i0[i3 + 3]), false);

						if (i2 > 0 && i2 >= (i0[i3 + 3] / 2))
						{
							++i1;
						}
						else if (tanks[i3].getFluidAmount() == 0)
						{
							tanks[i3].setFluid(new FluidStack((i0[0] == 0 ? FluidRegistry.WATER : ModCenter.FluidOilLubricant), 0));
							++i1;
						}
						break;

					default:

						if (tanks[i3].fill(new FluidStack(getTankFluidsFromType(fluid)[i3-1], i0[i3 + 3]), false) >= (i0[i3 + 3] / 2))
						{
							++i1;
						}
						break;
				}
			}

			if (i1 == 5 && tanks[0].getFluidAmount() >= i0[3])
			{
				return canUseTemperature();
			}
		}

		return false;
	}

	public boolean canUseTemperature()
	{
		int[] i0 = getTankResults();
		if (i0 != null)
		{
			int i1 = getTemperature() - 2;
			int i2 = getTemperature() + 2;

			if (i1 < i0[1] && i2 > i0[1])
			{
				return true;
			}
			else if (getTemperature() < i0[1])
			{
				temperature += ZnDMathHelper.ran.nextFloat() * 0.4F;
			}
		}

		return false;
	}

	@Override
	public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain)
	{
		if (canDrain(from, resource.getFluid()))
		{
			for (int i = 1; i < tanks.length; ++i)
			{
				if (tanks[i].getFluid() != null && tanks[i].getFluid().isFluidEqual(resource))
				{
					FluidStack fluidStack = tanks[i].drain(resource.amount, doDrain);
					if (fluidStack != null && fluidStack.amount > 0)
					{
						markForUpdate();
						return fluidStack;
					}
				}
			}
		}
		return null;
	}

	@Override
	public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain)
	{
		for (int i = 1; i < tanks.length; ++i)
		{
			if (tanks[i].getFluidAmount() > 0)
			{
				FluidStack fluidStack = tanks[i].drain(maxDrain, doDrain);
				if (fluidStack != null && fluidStack.amount > 0)
				{
					markForUpdate();
					return fluidStack;
				}
			}
		}
		return null;
	}

	@Override
	public int fill(ForgeDirection from, FluidStack resource, boolean doFill)
	{
		if (canFill(from, resource.getFluid()))
		{
			if (tanks[0].getFluidAmount() == 0 && (tanks[0].getFluid() == null || !tanks[0].getFluid().isFluidEqual(resource)))
			{
				if (fluids.get(resource.getFluid())[0] == 0)
				{
					tanks[0].setFluid(new FluidStack(ModCenter.FluidOilBioCrude, 0));
				}
				else
				{
					tanks[0].setFluid(new FluidStack(ModCenter.FluidOilShaleCrude, 0));
				}
			}

			if (tanks[0].getFluid().isFluidEqual(resource))
			{
				markForUpdate();
				return tanks[0].fill(resource, doFill);
			}
		}
		return 0;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		return (side == 0 ? slotsAccept : slotsReject);
	}

	public int getProcessScaled()
	{
		int[] i = getTankResults();
		if (i != null)
		{
			return (data[2] * 48 / i[2]);
		}
		return 0;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return AxisAlignedBB.getBoundingBox(xCoord - 1.0D, yCoord - BlockRefinery.HEIGHT, zCoord - 1.0D, xCoord + 2.0D, yCoord + 1.0D, zCoord + 2.0D);
	}

	public FluidTank getTank(int index)
	{
		return (index > -1 && index < tanks.length ? tanks[index] : null);
	}

	@Override
	public FluidTankInfo[] getTankInfo(ForgeDirection from)
	{
		FluidTankInfo[] info = new FluidTankInfo[tanks.length];
		for (int i = 0; i < tanks.length; ++i)
		{
			info[i] = tanks[i].getInfo();
		}
		return info;
	}

	public int[] getTankResults()
	{
		FluidTank tank = getTank(0);
		if (tank != null && tank.getFluid() != null)
		{
			return fluids.get(tank.getFluid().getFluid());
		}
		return null;
	}

	public int getTemperature()
	{
		return (int)temperature;
	}

	@Override
	public void invalidate()
	{
		super.invalidate();
		worldObj.func_147453_f(xCoord, yCoord, zCoord, ModCenter.BlockRefinery);
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack)
	{
		if (!super.isItemValidForSlot(slot, itemStack) || slot > 7 || slot == 2)
		{
			return false;
		}

		if (slot == 0)
		{
			FluidStack fluidStack = FluidContainerRegistry.getFluidForFilledItem(itemStack);
			return fluidStack != null && fluids.containsKey(fluidStack.getFluid()) && (slots[0] == null || slots[0].stackSize < slots[0].getMaxStackSize());
		}
		else if (slot == 1)
		{
			return getBurnTime(itemStack) > 0 && (slots[slot] == null || slots[slot].stackSize < slots[slot].getMaxStackSize());
		}
		else
		{
			return FluidContainerRegistry.isEmptyContainer(itemStack) && (slots[slot] == null || slots[slot].stackSize < slots[slot].getMaxStackSize());
		}
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this && player.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D, zCoord + 0.5D) <= 256.0D;
	}

	@Override
	protected void onRefuel()
	{
		if (gasFuel >= 200)
		{
			data[0] += 200;
			data[1] = 200;
			gasFuel -= 200;
		}
		else
		{
			if (getBurnTime(slots[1]) > 0)
			{
				if (slots[1] != null)
				{
					if (FluidContainerRegistry.isContainer(slots[1]))
					{
						ItemStack itemStack = FluidContainerRegistry.drainFluidContainer(slots[1]);
						if (itemStack != null)
						{
							if (slots[2] == null)
							{
								data[1] = data[0] = getBurnTime(slots[1]);
								setInventorySlotContents(2, itemStack);
								decrStackSize(1, 1);
							}
							else if (slots[2].getItem().equals(itemStack.getItem()) && slots[2].getItemDamage() == itemStack.getItemDamage())
							{
								data[1] = data[0] = getBurnTime(slots[1]);
								++slots[2].stackSize;
								decrStackSize(1, 1);
							}
						}
					}
					else
					{
						data[1] = data[0] = getBurnTime(slots[1]);
						--slots[1].stackSize;

						if (slots[1].stackSize == 0)
						{
							slots[1] = slots[1].getItem().getContainerItem(slots[1]);
						}
					}
				}
			}
		}
	}

	@Override
	protected void onSmelt()
	{
		int[] i0 = getTankResults();
		if (i0 != null)
		{
			FluidStack fluidStack = tanks[0].drain(i0[3], true);
			if (fluidStack != null && fluidStack.amount == i0[3])
			{
				Fluid[] tankFluids = getTankFluidsFromType(fluidStack.getFluid());
				for (int i1 = 1; i1 < tanks.length; ++i1)
				{
					tanks[i1].fill(new FluidStack(tankFluids[i1-1], i0[i1 + 3]), true);
				}

				if (i0[0] != 0)
				{
					gasFuel += 11;
				}
			}
			else
			{
				tanks[0].fill(fluidStack, true);
			}
		}
	}
	
	private static Fluid[] getTankFluidsFromType(@Nullable Fluid fluid)
	{
		if (ModCenter.FluidOilBioCrude.equals(fluid))
		{
			return new Fluid[] {
					ModCenter.FluidOilCreosote,
					FluidRegistry.WATER,
					ModCenter.FluidFuelDiesel,
					ModCenter.FluidFuelPetrol,
					ModCenter.FluidTar
			};
		}

		return new Fluid[] {
				ModCenter.FluidOilMachinery,
				ModCenter.FluidOilLubricant,
				ModCenter.FluidFuelDiesel,
				ModCenter.FluidFuelPetrol,
				ModCenter.FluidTar
		};
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		gasFuel = nbt.getInteger("GasFuel");
		temperature = nbt.getFloat("Temperature");

		for (int i = 0; i < tanks.length; ++i)
		{
			tanks[i].readFromNBT(nbt.getCompoundTag("fluidStack_" + i));
		}
	}

	@Override
	public void updateEntity()
	{
		boolean hasBurntime = data[0] > 0;
		boolean doUpdate = false;

		if (hasBurntime)
		{
			--data[0];
		}

		if (!worldObj.isRemote)
		{
			if (slotMoveCooldown > 0)
			{
				--slotMoveCooldown;
			}
			else
			{
				for (int i = 0; i < slots.length; ++i)
				{
					if (i == 0)
					{
						FluidStack fluidStack = FluidContainerRegistry.getFluidForFilledItem(slots[i]);
						if (fluidStack != null)
						{
							if (tanks[0].getFluidAmount() == 0 && fluids.containsKey(fluidStack.getFluid()))
							{
								if (fluids.get(fluidStack.getFluid())[0] == 0)
								{
									tanks[0].setFluid(new FluidStack(ModCenter.FluidOilBioCrude, 0));
								}
								else
								{
									tanks[0].setFluid(new FluidStack(ModCenter.FluidOilShaleCrude, 0));
								}
							}

							if (tanks[0].fill(fluidStack, false) == FluidContainerRegistry.BUCKET_VOLUME)
							{
								ItemStack itemStack = FluidContainerRegistry.drainFluidContainer(slots[i]);
								if (itemStack != null)
								{
									if (slots[i + 2] == null)
									{
										setInventorySlotContents(i + 2, itemStack);
										tanks[0].fill(fluidStack, true);
										decrStackSize(i, 1);
									}
									else if ((slots[i + 2].getItem().equals(itemStack.getItem()) && slots[i + 2].getItemDamage() == itemStack.getItemDamage())
											&& slots[i + 2].stackSize < slots[i + 2].getMaxStackSize())
									{
										slots[i + 2].stackSize += 1;
										tanks[0].fill(fluidStack, true);
										decrStackSize(i, 1);
									}
								}
							}
						}
					}
					else if (i >= 3 && i <= 7)
					{
						ItemStack itemStack = FluidContainerRegistry.fillFluidContainer(tanks[i - 2].drain(FluidContainerRegistry.BUCKET_VOLUME, false), slots[i]);

						if (tanks[i - 2].getFluidAmount() >= FluidContainerRegistry.BUCKET_VOLUME && itemStack != null)
						{
							if (slots[i + 5] == null || (itemStack.getItem().equals(slots[i + 5].getItem()) && itemStack.getItemDamage() == slots[i + 5].getItemDamage()
									&& slots[i + 5].stackSize < slots[i + 5].getMaxStackSize()))
							{
								itemStack = FluidContainerRegistry.fillFluidContainer(tanks[i - 2].drain(FluidContainerRegistry.BUCKET_VOLUME, true), slots[i]);

								if (itemStack != null)
								{
									decrStackSize(i, 1);

									if (slots[i + 5] == null)
									{
										setInventorySlotContents(i + 5, itemStack);
									}
									else
									{
										++slots[i + 5].stackSize;
									}
								}
							}
						}
					}
				}

				slotMoveCooldown = 10;
			}

			if (data[0] != 0 || (slots[1] != null && tanks[0].getFluidAmount() > 0))
			{
				if (data[0] == 0 && canProcess())
				{
					onRefuel();
					doUpdate = true;
				}

				if (data[0] > 0 && canProcess())
				{
					++data[2];

					if (canFinish())
					{
						onSmelt();
						data[2] = 0;
						doUpdate = true;
					}
				}
				else
				{
					data[2] = 0;
				}
			}

			if (hasBurntime != data[0] > 0)
			{
				doUpdate = true;
			}

			if (data[0] == 0)
			{
				data[2] = 0;

				if (temperature != 20.0F)
				{
					temperature -= 0.02F;

					if (temperature < 20.0F)
					{
						temperature = 20.0F;
					}
				}

				doUpdate = true;
			}
		}
		else if (data[0] > 0)
		{
			double d0 = xCoord - 0.15D;
			double d1 = yCoord + 0.6D;
			double d2 = zCoord - 0.45D;
			double d3 = ran.nextDouble() * 0.15D;

			worldObj.spawnParticle("smoke", d0 + d3, d1, d2 + d3, 0.0D, ran.nextDouble() * 0.05D, 0.0D);
			worldObj.spawnParticle("flame", d0 + d3, d1 + (d3 * 0.5D), d2 + d3, 0.0D, 0.0D, 0.0D);
			worldObj.spawnParticle("flame", d0 + d3, (d1 + (d3 * 0.5D)) * 4.0D, d2 + d3, 0.0D, 0.0D, 0.0D);
		}

		if (doUpdate)
		{
			markForUpdate();
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		nbt.setInteger("GasFuel", gasFuel);
		nbt.setFloat("Temperature", temperature);

		for (int i = 0; i < tanks.length; ++i)
		{
			nbt.setTag("fluidStack_" + i, tanks[i].writeToNBT(new NBTTagCompound()));
		}
	}
}
