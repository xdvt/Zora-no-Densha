package zoranodensha.common.blocks.tileEntity;

import java.util.ArrayList;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.util.PositionStackEntry;
import zoranodensha.api.vehicles.util.TrackObj;
import zoranodensha.client.ResourceManagerReloadListener;
import zoranodensha.common.blocks.platforms.Default_L;
import zoranodensha.common.blocks.platforms.Default_M;
import zoranodensha.common.blocks.platforms.Filler_M;
import zoranodensha.common.blocks.platforms.Modern_L;
import zoranodensha.common.blocks.platforms.Modern_M;
import zoranodensha.common.core.ModCenter;



public class TileEntityPlatform extends TileEntity
{
	/** {@link net.minecraft.item.ItemStack Block item} whose texture is rendered on this platform. May be {@code null}. */
	private ItemStack blockTexture;
	/** {@link APlatformType Type} of this platform block. Defines height and shape. Initialised on creation. */
	private APlatformType type;

	@SideOnly(Side.CLIENT) private TesselatorVertexState vertexState;
	@SideOnly(Side.CLIENT) private long vertexStateCreation;
	@SideOnly(Side.CLIENT) private float offNegZ;
	@SideOnly(Side.CLIENT) private float offPosZ;



	/**
	 * Default constructor for reading from NBT.
	 */
	public TileEntityPlatform()
	{
		this(0);
	}

	/**
	 * Initialise a new platform with given type ID.
	 */
	public TileEntityPlatform(int typeID)
	{
		setType(APlatformType.fromID(typeID));
	}


	@Override
	public boolean canUpdate()
	{
		return false;
	}

	/**
	 * Returns the block item whose texture is rendered on this platform.
	 * 
	 * @return {@link net.minecraft.item.ItemStack Block item} whose texture is rendered on this platform, may be {@code null}.
	 */
	public ItemStack getBlockTexture()
	{
		return blockTexture;
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, nbt);
	}

	@SideOnly(Side.CLIENT)
	public float getOffsetNegZ()
	{
		return offNegZ;
	}

	@SideOnly(Side.CLIENT)
	public float getOffsetPosZ()
	{
		return offPosZ;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public double getMaxRenderDistanceSquared()
	{
		double dist = 8.0 + ModCenter.cfg.rendering.detailOther * 4.0;
		dist *= Minecraft.getMinecraft().gameSettings.renderDistanceChunks;
		if (dist < 16.0)
		{
			dist = 16.0;
		}
		return dist * dist;
	}

	/**
	 * Returns the platform {@link APlatformType type} of this tile entity.
	 */
	public APlatformType getType()
	{
		return type;
	}

	@SideOnly(Side.CLIENT)
	public TesselatorVertexState getVertexState()
	{
		/* If the resource manager was reloaded after the current vertex state's creation, reset the vertex state. */
		if (vertexState != null && ResourceManagerReloadListener.getLastReloadTime() >= vertexStateCreation)
		{
			setVertexState(null);
			return null;
		}
		return vertexState;
	}

	@Override
	public void onDataPacket(NetworkManager networkManager, S35PacketUpdateTileEntity packet)
	{
		readFromNBT(packet.func_148857_g());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		/* Read block texture. */
		ItemStack blockItem = null;
		if (nbt.hasKey("blockTexture"))
		{
			ItemStack itemStack = ItemStack.loadItemStackFromNBT(nbt.getCompoundTag("blockTexture"));
			if (itemStack != null && itemStack.getItem() != null)
			{
				blockItem = itemStack;
			}
		}
		setBlockTexture(blockItem);

		/* Read platform type. */
		setType(APlatformType.fromString(nbt.getString("type")));

		/* Reset vertex state, if applicable. */
		if (worldObj != null && worldObj.isRemote)
		{
			setVertexState(null);
		}
	}

	/**
	 * Called when any neighbour block updates, to update the lengths on either side.
	 */
	@SideOnly(Side.CLIENT)
	public void recalculateSides()
	{
		/*
		 * Reset values first.
		 */
		offNegZ = 0.0F;
		offPosZ = 0.0F;

		/*
		 * Determine track at position the platform is pointing at.
		 */
		int x = xCoord;
		int z = zCoord;

		int meta = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
		switch (meta)
		{
			default:
				++z;
				break;

			case 1:
				--x;
				break;

			case 2:
				--z;
				break;

			case 3:
				++x;
				break;
		}

		TrackObj track = TrackObj.isTrack(worldObj, x, yCoord, z, null);
		if (track == null)
		{
			return;
		}

		/*
		 * If track exists, prepare positioning data.
		 */
		PositionStackEntry pseNeg = new PositionStackEntry(track.x + 0.5, track.y, track.z + 0.5, 0, (meta % 2 == 0) ? 90 : 0, 0);
		PositionStackEntry psePos = new PositionStackEntry(track.x + 0.5, track.y, track.z + 0.5, 0, (meta % 2 == 0) ? 90 : 0, 0);

		switch (meta)
		{
			default:
				psePos.x -= 0.5;
				pseNeg.x += 0.5;
				break;

			case 1:
				psePos.z -= 0.5;
				pseNeg.z += 0.5;
				break;

			case 2:
				psePos.x += 0.5;
				pseNeg.x -= 0.5;
				break;

			case 3:
				psePos.z += 0.5;
				pseNeg.z -= 0.5;
				break;
		}

		track.clampToTrack(null, pseNeg);
		track.clampToTrack(null, psePos);

		switch (meta)
		{
			default:
				offNegZ = (float)(pseNeg.z - (zCoord + 0.5) - 1.0);
				offPosZ = (float)(psePos.z - (zCoord + 0.5) - 1.0);
				break;

			case 1:
				offNegZ = (float)((xCoord + 0.5) - pseNeg.x - 1.0);
				offPosZ = (float)((xCoord + 0.5) - psePos.x - 1.0);
				break;

			case 2:
				offNegZ = (float)((zCoord + 0.5) - pseNeg.z - 1.0);
				offPosZ = (float)((zCoord + 0.5) - psePos.z - 1.0);
				break;

			case 3:
				offNegZ = (float)(pseNeg.x - (xCoord + 0.5) - 1.0);
				offPosZ = (float)(psePos.x - (xCoord + 0.5) - 1.0);
				break;
		}
	}

	/**
	 * Overrides the block whose texture is rendered on this platform.
	 * 
	 * @param blockItem - {@link net.minecraft.item.ItemStack Block item} whose texture shall be rendered on this platform, may be {@code null}.
	 * @return {@code true} if the block has been applied.
	 */
	public boolean setBlockTexture(@Nullable ItemStack blockItem)
	{
		if (blockItem != null)
		{
			Block block = Block.getBlockFromItem(blockItem.getItem());
			if (block == null || block instanceof ITileEntityProvider)
			{
				return false;
			}
			else if (!block.isOpaqueCube() || !block.renderAsNormalBlock() || (block == Blocks.stone) && blockItem.getItemDamage() == 0)
			{
				return false;
			}
		}

		blockTexture = blockItem;
		markDirty();

		if (worldObj != null)
		{
			worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
		}

		return true;
	}

	/**
	 * Overrides the {@link #type} of this platform.
	 */
	private void setType(APlatformType type)
	{
		this.type = type;
	}

	@SideOnly(Side.CLIENT)
	public void setVertexState(TesselatorVertexState vertexState)
	{
		this.vertexState = vertexState;
		this.vertexStateCreation = (vertexState != null) ? System.currentTimeMillis() : 0L;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		/* Write block item. */
		if (blockTexture != null)
		{
			nbt.setTag("blockTexture", blockTexture.writeToNBT(new NBTTagCompound()));
		}

		/* Write platform type. */
		nbt.setString("type", type.toString());
	}



	/**
	 * Platform types, defining edge shape as well as platform height.
	 */
	public static abstract class APlatformType
	{
		/** Cached types of platform shapes. */
		public static final ArrayList<APlatformType> types = new ArrayList<APlatformType>();
		/** Type ID. */
		private final int id;



		/**
		 * Add this type to the list of types.<br>
		 * Subclass implementations should also register their recipes at this point.
		 */
		public APlatformType()
		{
			id = types.size();
		}

		/**
		 * Return the height of this platform type, in meters.
		 */
		public float getHeight()
		{
			return 0.5F;
		}

		/**
		 * Returns the unique identifier of this type.
		 */
		public final int getID()
		{
			return id;
		}

		/**
		 * Render this platform type.
		 */
		@SideOnly(Side.CLIENT)
		public void render(IIcon icon, float offsetNegZ, float offsetPosZ, @Nullable TileEntityPlatform tilePlatform)
		{
			Tessellator tessellator = Tessellator.instance;
			tessellator.addTranslation(-0.5F, 0.0F, -0.5F);
			{
				renderNegX(tessellator, icon, offsetNegZ, offsetPosZ);
				renderPosX(tessellator, icon, offsetNegZ, offsetPosZ);

				renderNegY(tessellator, icon, offsetNegZ, offsetPosZ);
				renderPosY(tessellator, icon, offsetNegZ, offsetPosZ);

				renderNegZ(tessellator, icon, offsetNegZ, offsetPosZ);
				renderPosZ(tessellator, icon, offsetNegZ, offsetPosZ);
			}
			tessellator.setTranslation(0.0, 0.0, 0.0);
		}

		/**
		 * Vertex order is counter-clockwise, starting at bottom-right.
		 * 
		 * @param tessellator
		 * @param icon
		 * @param v1 - Bottom-right vertex
		 * @param v2 - Bottom-left vertex
		 * @param v3 - Top-left vertex
		 * @param v4 - Top-right vertex
		 * @param nX - Normal vector X-coordinate
		 * @param nY - Normal vector Y-coordinate
		 * @param nZ - Normal vector Z-coordinate
		 */
		@SideOnly(Side.CLIENT)
		protected void renderFace(Tessellator tessellator, IIcon icon, double[] v1, double[] v2, double[] v3, double[] v4, int nX, int nY, int nZ)
		{
			/*
			 * Determine U and V coordinates.
			 */
			int indexU, indexV;
			if (nX != 0)
			{
				indexU = 2;
				indexV = 1;
			}
			else if (nZ != 0)
			{
				indexU = 0;
				indexV = 1;
			}
			else
			{
				indexU = 2;
				indexV = 0;
			}

			/*
			 * Render face.
			 */
			tessellator.setNormal(nX, nY, nZ);
			tessellator.addVertexWithUV(v1[0], v1[1], v1[2], icon.getInterpolatedU(16 * v1[indexU]), icon.getInterpolatedV(16 * v1[indexV]));
			tessellator.addVertexWithUV(v2[0], v2[1], v2[2], icon.getInterpolatedU(16 * v2[indexU]), icon.getInterpolatedV(16 * v2[indexV]));
			tessellator.addVertexWithUV(v3[0], v3[1], v3[2], icon.getInterpolatedU(16 * v3[indexU]), icon.getInterpolatedV(16 * v3[indexV]));
			tessellator.addVertexWithUV(v4[0], v4[1], v4[2], icon.getInterpolatedU(16 * v4[indexU]), icon.getInterpolatedV(16 * v4[indexV]));
		}

		@SideOnly(Side.CLIENT)
		protected abstract void renderNegX(Tessellator tessellator, IIcon icon, float offsetNegZ, float offsetPosZ);

		@SideOnly(Side.CLIENT)
		protected abstract void renderPosX(Tessellator tessellator, IIcon icon, float offsetNegZ, float offsetPosZ);

		@SideOnly(Side.CLIENT)
		protected abstract void renderNegY(Tessellator tessellator, IIcon icon, float offsetNegZ, float offsetPosZ);

		@SideOnly(Side.CLIENT)
		protected abstract void renderPosY(Tessellator tessellator, IIcon icon, float offsetNegZ, float offsetPosZ);

		@SideOnly(Side.CLIENT)
		protected abstract void renderNegZ(Tessellator tessellator, IIcon icon, float offsetNegZ, float offsetPosZ);

		@SideOnly(Side.CLIENT)
		protected abstract void renderPosZ(Tessellator tessellator, IIcon icon, float offsetNegZ, float offsetPosZ);

		/**
		 * Splits the given face data into smaller faces which fit default square size.
		 * 
		 * @see #renderFace(Tessellator, IIcon, double[], double[], double[], double[], int, int, int) renderFace()
		 */
		@SideOnly(Side.CLIENT)
		protected void renderSplitFace(Tessellator tessellator, IIcon icon, double[] v1, double[] v2, double[] v3, double[] v4, int nX, int nY, int nZ)
		{
			/*
			 * Determine U and V coordinates.
			 */
			int indexU, indexV;
			if (nX != 0)
			{
				indexU = 2;
				indexV = 1;
			}
			else if (nZ != 0)
			{
				indexU = 0;
				indexV = 1;
			}
			else
			{
				indexU = 2;
				indexV = 0;
			}

			/*
			 * Split face by UV bounds.
			 */
			double[] v12, v32;
			v12 = splitEdge(v1, v2, indexU, indexV);
			v32 = splitEdge(v3, v2, indexU, indexV);

			if (v12 != null && v32 != null && nY == 1)
			{
				// FIXME PLATFORMS - Split platform faces if they exceed their UV bounds.
			}

			renderFace(tessellator, icon, v1, v2, v3, v4, nX, nY, nZ);
		}

		/**
		 * Returns a new vertex if the edge specified by given vertices
		 * along the given axis' index crosses a UV bound.
		 */
		@SideOnly(Side.CLIENT)
		private double[] splitEdge(double[] v, double[] w, int indexU, int indexV)
		{
			/* Don't bother splitting if the vertices overlap. */
			Vec3 vec = Vec3.createVectorHelper(w[0] - v[0], w[1] - v[1], w[2] - v[2]);
			if (vec.squareDistanceTo(0, 0, 0) < 0.001D)
			{
				return null;
			}

			/* Ensure the given edge crosses UV bounds. */
			if ((int)v[indexV] == (int)w[indexV])
			{
				return null;
			}

			int bound;
			double dist;

			if (v[indexV] > w[indexV])
			{
				/* Determine upper bound and target distance from lower vertex. */
				bound = (int)w[indexV] + 1;
				dist = bound - v[indexV];
			}
			else
			{
				bound = (int)v[indexV] + 1;
				dist = bound - w[indexV];
			}

			/* Create vertex and align. */
			double[] vert = new double[] { w[0], w[1], w[2] };
			vert[indexV] += dist;
			vert[indexU] += dist * ((w[indexU] - v[indexU]) / (w[indexV] - v[indexV]));

			return vert;
		}

		@Override
		public String toString()
		{
			return getClass().getSimpleName();
		}

		/**
		 * Update this block's bounding box, depending on the give metadata. <br>
		 * Called by
		 * {@link zoranodensha.common.blocks.BlockPlatform#setBlockBoundsBasedOnState(net.minecraft.world.IBlockAccess, int, int, int) setBlockBoundsBasedOnState()}
		 */
		public void updateBoundingBox(Block block, int meta)
		{
			switch (meta)
			{
				default:
					block.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, getHeight(), 1.0F);
					break;

				case 0:
					block.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, getHeight(), 0.625F);
					break;

				case 1:
					block.setBlockBounds(1.0F - 0.625F, 0.0F, 0.0F, 1.0F, getHeight(), 1.0F);
					break;

				case 2:
					block.setBlockBounds(0.0F, 0.0F, 1.0F - 0.625F, 1.0F, getHeight(), 1.0F);
					break;

				case 3:
					block.setBlockBounds(0.0F, 0.0F, 0.0F, 0.625F, getHeight(), 1.0F);
					break;
			}
		}

		/**
		 * Tries to match the given type ID to a platform type.
		 * If none was found, returns the first index in the list of types.
		 */
		public static APlatformType fromID(int typeID)
		{
			for (APlatformType type : types)
			{
				if (type.getID() == typeID)
				{
					return type;
				}
			}
			return types.get(0);
		}

		/**
		 * Tries to match the given type name to a platform type.
		 * If none was found, returns the first index in the list of types.
		 */
		public static APlatformType fromString(String name)
		{
			for (APlatformType type : types)
			{
				if (type.toString().equalsIgnoreCase(name))
				{
					return type;
				}
			}
			return types.get(0);
		}

		/**
		 * Register all platform types as well as their recipes.
		 */
		public static void register()
		{
			types.add(new Default_L());
			types.add(new Default_M());
			types.add(new Modern_L());
			types.add(new Modern_M());
			types.add(new Filler_M());
		}
	}
}