package zoranodensha.common.blocks.tileEntity;

import java.util.HashMap;
import java.util.Map;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.OreDictionaryHelper;
import zoranodensha.common.util.ZnDMathHelper;



public class TileEntityRetorter extends TileEntityContainerInventory implements IFluidHandler
{
	private static final int[] slotsAccept = new int[] { 0, 1, 2, 3 };
	private static final int[] slotsReject = new int[] { };
	private static final Map<Item, int[]> itemsShale = new HashMap<Item, int[]>();
	private static final Map<Item, int[]> itemsBio = new HashMap<Item, int[]>();
	// 0 = Item Input; 1 = Fuel Input; 2 = Bucket Output
	static
	{
		for (Item item : OreDictionaryHelper.getIndicesInOreDictionary("treeSapling"))
		{
			itemsBio.put(item, new int[] { 25, 25 });
		}

		for (Item item : OreDictionaryHelper.getIndicesInOreDictionaryExtended("food"))
		{
			itemsBio.put(item, new int[] { 30, 30 });
		}

		for (Item item : OreDictionaryHelper.getIndicesInOreDictionaryExtended("seed"))
		{
			itemsBio.put(item, new int[] { 25, 25 });
		}

		itemsBio.put(Items.melon_seeds, new int[] { 25, 25 });
		itemsBio.put(Items.pumpkin_seeds, new int[] { 25, 25 });
		itemsBio.put(Items.wheat_seeds, new int[] { 25, 25 });

		for (Item item : OreDictionaryHelper.getIndicesInOreDictionary("stone"))
		{
			itemsShale.put(item, new int[] { 30, 30 });
		}

		itemsShale.put(ModCenter.ItemOilShale, new int[] { 250, 250 });
	}
	
	private final FluidTank tank = new FluidTank(ModCenter.FluidOilShaleCrude, 0, 16000);
	private int slotMoveCooldown;



	public TileEntityRetorter()
	{
		super(new ItemStack[4], "retorter");
	}


	@Override
	public boolean canDrain(ForgeDirection from, Fluid fluid)
	{
		return (tank.getFluid() != null) && tank.getFluid().isFluidEqual(new FluidStack(fluid, 0));
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side)
	{
		return (slot == 3);
	}

	@Override
	public boolean canFill(ForgeDirection from, Fluid fluid)
	{
		return false;
	}

	@Override
	protected boolean canFinish()
	{
		return data[2] == 200;
	}

	@Override
	protected boolean canProcess()
	{
		boolean flag = false;

		if (slots[0] != null)
		{
			int[] i = itemsBio.get(slots[0].getItem());
			if (i != null)
			{
				flag = (tank.getFluid() != null) && (tank.getFluid().getFluid() == ModCenter.FluidOilBioCrude);
				if (!flag)
				{
					if (tank.getFluidAmount() == 0)
					{
						tank.setFluid(new FluidStack(ModCenter.FluidOilBioCrude, 0));
						flag = true;
					}
				}
			}
			else
			{
				i = itemsShale.get(slots[0].getItem());
				if (i != null)
				{
					flag = (tank.getFluid() != null) && (tank.getFluid().getFluid() == ModCenter.FluidOilShaleCrude);
					if (!flag)
					{
						if (tank.getFluidAmount() == 0)
						{
							tank.setFluid(new FluidStack(ModCenter.FluidOilShaleCrude, 0));
							flag = true;
						}
					}
				}
			}
		}

		return flag && ((tank.getCapacity() - tank.getFluidAmount()) > 0);
	}

	@Override
	public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain)
	{
		if (tank.getFluid() != null && tank.getFluid().isFluidEqual(resource))
		{
			FluidStack fluidStack = tank.drain(resource.amount, doDrain);
			if (fluidStack != null && fluidStack.amount > 0)
			{
				markForUpdate();
				return fluidStack;
			}
		}
		return null;
	}

	@Override
	public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain)
	{
		FluidStack fluidStack = tank.drain(maxDrain, doDrain);
		if (fluidStack != null && fluidStack.amount > 0)
		{
			markForUpdate();
			return fluidStack;
		}

		return null;
	}

	@Override
	public int fill(ForgeDirection from, FluidStack resource, boolean doFill)
	{
		return 0;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		return (side < 2 || (side < 4 && getBlockMetadata() == 1) || (side < 6 && getBlockMetadata() == 0) ? slotsAccept : slotsReject);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return AxisAlignedBB.getBoundingBox(xCoord - 1.0D, yCoord, zCoord - 1.0D, xCoord + 2.0D, yCoord + 1.0D, zCoord + 2.0D);
	}

	public FluidTank getTank()
	{
		return tank;
	}

	@Override
	public FluidTankInfo[] getTankInfo(ForgeDirection from)
	{
		return new FluidTankInfo[] { tank.getInfo() };
	}

	@Override
	public void invalidate()
	{
		super.invalidate();
		worldObj.func_147453_f(xCoord, yCoord, zCoord, ModCenter.BlockRetorter);
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack)
	{
		if (!super.isItemValidForSlot(slot, itemStack))
		{
			return false;
		}

		switch (slot)
		{
			case 0:
				return itemsShale.containsKey(itemStack.getItem()) || itemsBio.containsKey(itemStack.getItem());

			case 1:
				return !(itemsShale.containsKey(itemStack.getItem()) || itemsBio.containsKey(itemStack.getItem())) && getBurnTime(itemStack) > 0 && !FluidContainerRegistry.isContainer(itemStack);

			case 2:
				return FluidContainerRegistry.isEmptyContainer(itemStack) && (slots[2] == null || slots[2].stackSize < slots[2].getMaxStackSize());

			default:
				return false;
		}
	}

	@Override
	protected void onSmelt()
	{
		int[] i = itemsShale.get(slots[0].getItem());
		if (i == null)
		{
			i = itemsBio.get(slots[0].getItem());
		}

		if (i != null)
		{
			tank.fill(new FluidStack(tank.getFluid().getFluid(), ZnDMathHelper.ranInt(i[0], i[1])), true);
			decrStackSize(0, 1);
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		tank.readFromNBT(nbt.getCompoundTag("fluidStack"));
	}

	@Override
	public void updateEntity()
	{
		int[] i = null;
		int flag;

		if (slots[0] != null)
		{
			i = itemsShale.get(slots[0].getItem());
			flag = (i != null ? 2 : 0);

			if (i == null)
			{
				i = itemsBio.get(slots[0].getItem());
				flag = (i != null ? 1 : 0);
			}
		}
		else
		{
			flag = 0;
		}

		super.updateEntity();

		if (!worldObj.isRemote)
		{
			if (slotMoveCooldown > 0)
			{
				--slotMoveCooldown;
			}
			else
			{
				ItemStack itemStack = FluidContainerRegistry.fillFluidContainer(tank.drain(FluidContainerRegistry.BUCKET_VOLUME, false), slots[2]);

				if (tank.getFluidAmount() >= FluidContainerRegistry.BUCKET_VOLUME && itemStack != null)
				{
					if (slots[3] == null
							|| (itemStack.getItem().equals(slots[3].getItem()) && itemStack.getItemDamage() == slots[3].getItemDamage() && slots[3].stackSize < slots[3].getMaxStackSize()))
					{
						itemStack = FluidContainerRegistry.fillFluidContainer(tank.drain(FluidContainerRegistry.BUCKET_VOLUME, true), slots[2]);

						if (itemStack != null)
						{
							decrStackSize(2, 1);

							if (slots[3] == null)
							{
								setInventorySlotContents(3, itemStack);
							}
							else
							{
								++slots[3].stackSize;
							}
						}
					}
				}

				slotMoveCooldown = 10;
			}

			if (tank.getFluidAmount() == 0)
			{
				switch (flag)
				{
					case 1:
						if (tank.getFluid() == null || tank.getFluid().getFluid() != ModCenter.FluidOilBioCrude)
						{
							tank.setFluid(new FluidStack(ModCenter.FluidOilBioCrude, 0));
						}
						break;

					case 2:
						if (tank.getFluid() == null || tank.getFluid().getFluid() != ModCenter.FluidOilShaleCrude)
						{
							tank.setFluid(new FluidStack(ModCenter.FluidOilShaleCrude, 0));
						}
						break;
				}
			}
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setTag("fluidStack", tank.writeToNBT(new NBTTagCompound()));
	}
}
