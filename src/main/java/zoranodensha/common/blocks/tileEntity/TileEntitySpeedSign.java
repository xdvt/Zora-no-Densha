package zoranodensha.common.blocks.tileEntity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.signals.ASpeedSignRegistry;
import zoranodensha.api.structures.signals.ISpeedSign;

public class TileEntitySpeedSign extends TileEntity
{
	/**
	 * The {@link zoranodensha.api.structures.signals.ISpeedSign} instance assigned
	 * to this tile entity.
	 */
	private ISpeedSign sign;


	/**
	 * Initialises a new instance of the
	 * {@link zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign} class.
	 */
	public TileEntitySpeedSign()
	{
		sign = null;
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound descriptionPacket = new NBTTagCompound();
		writeToNBT(descriptionPacket);

		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, descriptionPacket);
	}

	/**
	 * @return - This tile entity's assigned
	 *         {@link zoranodensha.api.structures.signals.ISpeedSign} instance. May
	 *         be {@code null}.
	 */
	public ISpeedSign getSpeedSign()
	{
		return sign;
	}

	@Override
	public void onDataPacket(NetworkManager manager, S35PacketUpdateTileEntity packet)
	{
		readFromNBT(packet.func_148857_g());
	}

	@Override
	public void updateEntity()
	{
		super.updateEntity();

		/*
		 * Also update the speed sign instance if there is one.
		 */
		if (sign != null)
		{
			sign.onUpdate();
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		if (nbt.getBoolean("speedSign"))
		{
			ISpeedSign freshSpeedSign = ASpeedSignRegistry.getSpeedSignFromName(nbt.getString("name"));

			if (freshSpeedSign != null)
			{
				this.setSpeedSign(freshSpeedSign);
				this.sign.readFromNBT(nbt);
			}
		}
	}

	public TileEntitySpeedSign setSpeedSign(ISpeedSign newSign)
	{
		this.sign = newSign;
		this.sign.onTileEntityChange(this);

		return this;
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		nbt.setBoolean("speedSign", sign != null);

		if (sign != null)
		{
			nbt.setString("name", sign.getName());

			sign.writeToNBT(nbt);
		}
	}
}
