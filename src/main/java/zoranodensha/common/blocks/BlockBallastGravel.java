package zoranodensha.common.blocks;

import java.util.Random;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFalling;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class BlockBallastGravel extends BlockFalling
{
	private static boolean registered = false;



	public BlockBallastGravel()
	{
		super(ModCenter.Material_Ballast);

		setBlockName(ModData.ID + ".blocks.ballastGravel");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(2.5F);
		this.setHarvestLevel("shovel", 1);
		setResistance(4.5F);
		setStepSound(Block.soundTypeGravel);
	}

	@Override
	public void func_149828_a(World world, int x, int y, int z, int meta)
	{
		super.func_149828_a(world, x, y, z, meta);

		Block block = world.getBlock(x, y, z);
		if (block instanceof BlockBallastGravel)
		{
			world.setBlock(x, y, z, ModCenter.BlockBallastGravel);
		}
	}

	@Override
	public Item getItemDropped(int meta, Random ran, int fortune)
	{
		return Item.getItemFromBlock(ModCenter.BlockBallastGravel);
	}

	@Override
	public boolean isSideSolid(IBlockAccess world, int x, int y, int z, ForgeDirection side)
	{
		return true;
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack itemStack)
	{
		onNeighborBlockChange(world, x, y, z, this);
		world.notifyBlocksOfNeighborChange(x, y, z, this);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		onMetadataChange(world, x, y, z);

		super.onNeighborBlockChange(world, x, y, z, block);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		blockIcon = icon.registerIcon(ModData.ID + ":blockBallastGravel");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 2), new ItemStack(Blocks.gravel), new ItemStack(Blocks.gravel), new ItemStack(Blocks.gravel)));
	}


	/**
	 * Static helper method to re-calculate block metadata depending on the surroundings.
	 *
	 * @param world - A reference to the World object this block is in.
	 * @param x - The X-coordinate of this block.
	 * @param y - The Y-coordinate of this block.
	 * @param z - The Z-coordinate of this block.
	 */
	public static void onMetadataChange(World world, int x, int y, int z)
	{
		if (world.isRemote)
		{
			return;
		}

		int meta = world.getBlockMetadata(x, y, z);

		if (world.isSideSolid(x, y + 1, z, ForgeDirection.DOWN) || world.getTileEntity(x, y + 1, z) instanceof ITrackBase)
		{
			if (meta != 0 || world.getBlock(x, y, z) != ModCenter.BlockBallastGravel)
			{
				world.setBlock(x, y, z, ModCenter.BlockBallastGravel, 0, 2);
				world.notifyBlocksOfNeighborChange(x, y, z, ModCenter.BlockBallastGravel);
			}

			return;
		}

		int newMeta;
		int north = (world.getBlock(x, y, z - 1) == ModCenter.BlockBallastGravelCorner ? world.getBlockMetadata(x, y, z - 1) : world.isSideSolid(x, y, z - 1, ForgeDirection.SOUTH) ? 0 : -2);
		int east = (world.getBlock(x + 1, y, z) == ModCenter.BlockBallastGravelCorner ? world.getBlockMetadata(x + 1, y, z) : world.isSideSolid(x + 1, y, z, ForgeDirection.WEST) ? 0 : -2);
		int south = (world.getBlock(x, y, z + 1) == ModCenter.BlockBallastGravelCorner ? world.getBlockMetadata(x, y, z + 1) : world.isSideSolid(x, y, z + 1, ForgeDirection.NORTH) ? 0 : -2);
		int west = (world.getBlock(x - 1, y, z) == ModCenter.BlockBallastGravelCorner ? world.getBlockMetadata(x - 1, y, z) : world.isSideSolid(x - 1, y, z, ForgeDirection.EAST) ? 0 : -2);
		boolean northSouth = north > -2 && south > -2;
		boolean eastWest = east > -2 && west > -2;

		if (northSouth && eastWest)
		{
			if ((south == 2 || south == 5) && (west == 1 || west == 5))
			{
				newMeta = 9;
			}
			else if ((north == 2 || north == 6) && (west == 3 || west == 6))
			{
				newMeta = 10;
			}
			else if ((north == 4 || north == 7) && (east == 3 || east == 7))
			{
				newMeta = 11;
			}
			else if ((south == 4 || south == 8) && (east == 1 || east == 8))
			{
				newMeta = 12;
			}
			else
			{
				newMeta = 0;
			}
		}
		else if (northSouth && !eastWest)
		{
			if ((north == 2 || north == 6 || north == 9) && (south == 2 || south == 5 || south == 10))
			{
				newMeta = 2;
			}
			else if ((north == 4 || north == 7 || north == 12) && (south == 4 || south == 8 || south == 11))
			{
				newMeta = 4;
			}
			else if (east == 0 || east == 4 || east == 9 || east == 12)
			{
				newMeta = 2;
			}
			else if (west == 0 || west == 2 || west == 9 || west == 12)
			{
				newMeta = 4;
			}
			else
			{
				newMeta = -1;
			}
		}
		else if (!northSouth && eastWest)
		{
			if ((east == 1 || east == 8 || east == 9) && (west == 1 || west == 5 || west == 12))
			{
				newMeta = 1;
			}
			else if ((east == 3 || east == 7 || east == 10) && (west == 3 || west == 6 || west == 11))
			{
				newMeta = 3;
			}
			else if (north == 0 || north == 3 || north == 10 || north == 11)
			{
				newMeta = 1;
			}
			else if (south == 0 || south == 1 || south == 10 || south == 11)
			{
				newMeta = 3;
			}
			else
			{
				newMeta = -1;
			}
		}
		else
		{
			if (north > -2 && east > -2)
			{
				if ((north == 4 || north == 7) && (east == 3 || east == 7))
				{
					newMeta = 11;
				}
				else
				{
					newMeta = 5;
				}
			}
			else if (east > -2 && south > -2)
			{
				if ((south == 4 || south == 8) && (east == 1 || east == 8))
				{
					newMeta = 12;
				}
				else
				{
					newMeta = 6;
				}
			}
			else if (south > -2 && west > -2)
			{
				if ((south == 2 || south == 5) && (west == 1 || west == 5))
				{
					newMeta = 9;
				}
				else
				{
					newMeta = 7;
				}
			}
			else if (west > -2 && north > -2)
			{
				if ((north == 2 || north == 6) && (west == 3 || west == 6))
				{
					newMeta = 10;
				}
				else
				{
					newMeta = 8;
				}
			}
			else
			{
				newMeta = -1;
			}
		}

		if (newMeta > -1 && newMeta != meta)
		{
			if (newMeta == 0)
			{
				world.setBlock(x, y, z, ModCenter.BlockBallastGravel, 0, 2);
				world.notifyBlocksOfNeighborChange(x, y, z, ModCenter.BlockBallastGravel);
			}
			else
			{
				world.setBlock(x, y, z, ModCenter.BlockBallastGravelCorner, newMeta, 2);
				world.notifyBlocksOfNeighborChange(x, y, z, ModCenter.BlockBallastGravelCorner);
			}
		}
	}
}
