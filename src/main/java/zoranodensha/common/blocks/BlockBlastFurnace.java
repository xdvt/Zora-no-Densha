package zoranodensha.common.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.blocks.tileEntity.TileEntityBlastFurnace;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.handlers.EGui;



public class BlockBlastFurnace extends BlockMultiBlock
{
	private static boolean registered = false;



	public BlockBlastFurnace()
	{
		super(4, EGui.BLAST_FURNACE, ModCenter.BlockBlastFurnace);

		setBlockName(ModData.ID + ".blocks.blastFurnace");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(3.5F);
		setResistance(15.0F);
	}

	@Override
	protected boolean checkStructure(World world, int x, int y, int z, boolean checkMeta, boolean hasTileEntity)
	{
		int flag = 0;

		for (int i = -1; i < 2; ++i)
		{
			for (int j = -3; j < 1; ++j)
			{
				for (int k = -1; k < 2; ++k)
				{
					if (world.getBlock(x + i, y + j, z + k) == ModCenter.BlockBlastFurnace)
					{
						if (checkMeta)
						{
							if (hasTileEntity)
							{
								int meta = 1;

								switch (j)
								{
									case -2:
										if ((i != 0 && k == 0) || (i == 0 && k != 0))
										{
											meta = 2;
										}
										break;

									case -1:
										if (i == 0 && k == 0)
										{
											continue;
										}
										break;

									case 0:
										if (i == 0 && k == 0)
										{
											meta = 3;
										}
										break;
								}

								if (world.getBlockMetadata(x + i, y + j, z + k) == meta)
								{
									++flag;
								}
							}
							else if (world.getBlockMetadata(x + i, y + j, z + k) == 0)
							{
								++flag;
							}
						}
						else
						{
							++flag;
						}
					}
				}
			}
		}

		return flag == 34 && world.getBlock(x, y - 1, z) == Blocks.air && world.getBlock(x, y - 2, z) == Blocks.air;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return (meta == 3 ? new TileEntityBlastFurnace() : super.createNewTileEntity(world, meta));
	}

	@Override
	public TileEntityBlastFurnace getTileCoordinates(World world, int x, int y, int z)
	{
		for (int i = -1; i < 2; ++i)
		{
			for (int j = 0; j < 5; ++j)
			{
				for (int k = -1; k < 2; ++k)
				{
					if (checkStructure(world, x + i, y + j, z + k, false, false))
					{
						checkStructureAndRefresh(world, x + i, y + j, z + k, world.getTileEntity(x + i, y + j, z + k) instanceof TileEntityBlastFurnace);
						TileEntity tileEntity = world.getTileEntity(x + i, y + j, z + k);

						if (tileEntity instanceof TileEntityBlastFurnace)
						{
							return ((TileEntityBlastFurnace)tileEntity);
						}
					}
				}
			}
		}

		return null;
	}


	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			icons[i] = icon.registerIcon(ModData.ID + ":blockBlastFurnace_" + i);
		}
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 4), "SBS", "BLB", "SBS", 'L', new ItemStack(Items.lava_bucket), 'B', new ItemStack(Items.netherbrick), 'S', "sand"));
	}
}
