package zoranodensha.common.blocks.platforms;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.blocks.tileEntity.TileEntityPlatform.APlatformType;
import zoranodensha.common.core.ModCenter;



public class Modern_M extends APlatformType
{
	public Modern_M()
	{
		super();
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModCenter.BlockPlatform, 3, getID()), "   ", " SS", " S ", 'S', new ItemStack(Blocks.stone_slab)));
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderNegX(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 0.0, 0.0, 0.0 };
		v2 = new double[] { 0.0, 0.0, 1.0 };
		v3 = new double[] { 0.0, getHeight(), 1.0 };
		v4 = new double[] { 0.0, getHeight(), 0.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, -1, 0, 0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderNegY(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 0.0, 0.0, 1.0 };
		v2 = new double[] { 0.0, 0.0, 0.0 };
		v3 = new double[] { 0.375 + negZ, 0.0, 0.0 };
		v4 = new double[] { 0.375 + posZ, 0.0, 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, -1, 0);

		v1 = new double[] { 0.375 + posZ, 0.125, 1.0 };
		v2 = new double[] { 0.375 + negZ, 0.125, 0.0 };
		v3 = new double[] { 0.625 + negZ, 0.125, 0.0 };
		v4 = new double[] { 0.625 + posZ, 0.125, 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, -1, 0);
	}

	@Override
	protected void renderNegZ(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 0.625 + negZ, 0.125, 0.0 };
		v2 = new double[] { 0.0, 0.125, 0.0 };
		v3 = new double[] { 0.0, getHeight(), 0.0 };
		v4 = new double[] { 0.625 + negZ, getHeight(), 0.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, 0, -1);

		v1 = new double[] { 0.375 + negZ, 0.0, 0.0 };
		v2 = new double[] { 0.0, 0.0, 0.0 };
		v3 = new double[] { 0.0, 0.125, 0.0 };
		v4 = new double[] { 0.375 + negZ, 0.125, 0.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, 0, -1);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderPosX(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 0.625 + posZ, 0.125, 1.0 };
		v2 = new double[] { 0.625 + negZ, 0.125, 0.0 };
		v3 = new double[] { 0.625 + negZ, getHeight(), 0.0 };
		v4 = new double[] { 0.625 + posZ, getHeight(), 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 1, 0, 0);

		v1 = new double[] { 0.375 + posZ, 0.0, 1.0 };
		v2 = new double[] { 0.375 + negZ, 0.0, 0.0 };
		v3 = new double[] { 0.375 + negZ, 0.125, 0.0 };
		v4 = new double[] { 0.375 + posZ, 0.125, 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 1, 0, 0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderPosY(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 0.0625 + posZ, getHeight(), 1.0 };
		v2 = new double[] { 0.0625 + negZ, getHeight(), 0.0 };
		v3 = new double[] { 0.0, getHeight(), 0.0 };
		v4 = new double[] { 0.0, getHeight(), 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, 1, 0);

		v1 = new double[] { 0.25 + posZ, getHeight(), 1.0 };
		v2 = new double[] { 0.25 + negZ, getHeight(), 0.0 };
		v3 = new double[] { 0.0625 + negZ, getHeight(), 0.0 };
		v4 = new double[] { 0.0625 + posZ, getHeight(), 1.0 };
		renderSplitFace(tessellator, Blocks.wool.getIcon(0, 0), v1, v2, v3, v4, 0, 1, 0);

		v1 = new double[] { 0.625 + posZ, getHeight(), 1.0 };
		v2 = new double[] { 0.625 + negZ, getHeight(), 0.0 };
		v3 = new double[] { 0.25 + negZ, getHeight(), 0.0 };
		v4 = new double[] { 0.25 + posZ, getHeight(), 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, 1, 0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderPosZ(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 0.0, 0.125, 1.0 };
		v2 = new double[] { 0.625 + posZ, 0.125, 1.0 };
		v3 = new double[] { 0.625 + posZ, getHeight(), 1.0 };
		v4 = new double[] { 0.0, getHeight(), 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, 0, 1);

		v1 = new double[] { 0.0, 0.0, 1.0, 1.0 };
		v2 = new double[] { 0.375 + posZ, 0.0, 1.0 };
		v3 = new double[] { 0.375 + posZ, 0.125, 1.0 };
		v4 = new double[] { 0.0, 0.125, 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, 0, 1);
	}
}