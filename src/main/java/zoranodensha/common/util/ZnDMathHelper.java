package zoranodensha.common.util;

import java.util.Random;

import net.minecraft.util.MathHelper;



/**
 * An extended version of Minecraft's MathHelper class.
 * Some methods may be copied; All credit goes to the rightful authors.
 */
public abstract class ZnDMathHelper
{
	public static final double RAD_MULTIPLIER = Math.PI / 180.0D;
	public static final double DEG_MULTIPLIER = 180.0D / Math.PI;

	/** A random number generator instance. */
	public static final Random ran = new Random();




	/**
	 * Returns the Metadata rotation from the given yaw rotation.<br>
	 * <br>
	 * North: 0<br>
	 * East: 1<br>
	 * South: 2<br>
	 * West: 3
	 */
	public static final int getRotation(float rotationYaw)
	{
		return MathHelper.floor_float((rotationYaw / 90.0F) + 2.5F) & 3;
	}

	/**
	 * Returns a randomly generated number bound within the range of the given arguments.
	 */
	public static final int ranInt(int min, int max)
	{
		return (ran.nextInt((max - min) + 1) + min);
	}
}
