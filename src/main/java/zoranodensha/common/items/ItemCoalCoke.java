package zoranodensha.common.items;

import java.util.List;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import zoranodensha.common.blocks.tileEntity.TileEntityContainerInventory;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemCoalCoke extends Item
{
	private static boolean registered = false;



	public ItemCoalCoke()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setUnlocalizedName(ModData.ID + ".items.coalCoke");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean par4)
	{
		list.add(TileEntityContainerInventory.getBurnTime(itemStack) + " Fuel Units");
	}


	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemCoalCoke");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		OreDictionary.registerOre("coalCoke", this);
		OreDictionary.registerOre("fuelCoke", this);
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 9), "blockCoalCoke"));
	}
}
