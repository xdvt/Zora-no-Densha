package zoranodensha.common.items;

import java.util.List;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemRailwaySign extends Item
{
	private static boolean registered = false;


	private IIcon[] icons = new IIcon[28];



	public ItemRailwaySign()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setHasSubtypes(true);
		setMaxDamage(0);
		setMaxStackSize(3);
		setUnlocalizedName(ModData.ID + ".items.railwaySign");
	}

	@Override
	public IIcon getIconFromDamage(int i)
	{
		return icons[i];
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List list)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public String getUnlocalizedName(ItemStack itemStack)
	{
		int i = MathHelper.clamp_int(itemStack.getItemDamage(), 0, icons.length - 1);

		return super.getUnlocalizedName() + "_" + i;
	}


	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			icons[i] = iconReg.registerIcon(ModData.ID + ":itemRailwaySign_" + i);
		}
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 0), "WPW", "BPW", "WPW", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 1), "WPW", "BPB", "WPW", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 2), "BPW", "BPW", "BPW", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 3), "BPW", "BPB", "BPW", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 4), "WPB", "BPW", "WPB", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 5), "BPB", "WPW", "BPB", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 6), "BPW", "WPB", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 7), "BPB", "WPB", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 8), "BPB", "WPW", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 9), "WPB", "BPW", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 10), " B ", "BPB", "WPB", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 11), "BPW", "BPW", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 12), "BPB", "BPW", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'W', "dyeWhite"));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 12), new ItemStack(this, 1, 17)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 13), new ItemStack(this, 1, 12)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 14), new ItemStack(this, 1, 13)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 15), new ItemStack(this, 1, 14)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 16), new ItemStack(this, 1, 15)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 17), new ItemStack(this, 1, 16)));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 18), "BPO", "OPB", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'O', "dyeOrange"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 19), "OPB", "BPO", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'O', "dyeOrange"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 20), "BPB", "OPO", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'O', "dyeOrange"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 21), "OPO", "BPB", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'O', "dyeOrange"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 22), "BPB", "BPO", "   ", 'P', "plateSteel", 'B', "dyeBlack", 'O', "dyeOrange"));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 22), new ItemStack(this, 1, 27)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 23), new ItemStack(this, 1, 22)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 24), new ItemStack(this, 1, 23)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 25), new ItemStack(this, 1, 24)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 26), new ItemStack(this, 1, 25)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 1, 27), new ItemStack(this, 1, 26)));
	}
}
