package zoranodensha.common.items;

import java.util.List;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.items.IItemTrackPart;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;



public class ItemTrackPart extends Item implements IItemTrackPart
{
	private static boolean registered = false;


	private IIcon[] icons = new IIcon[8];



	public ItemTrackPart()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setHasSubtypes(true);
		setMaxDamage(0);
		setMaxStackSize(64);
		setUnlocalizedName(ModData.ID + ".items.trackParts");
	}

	@Override
	public IIcon getIconFromDamage(int i)
	{
		return icons[i];
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List list)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public String getUnlocalizedName(ItemStack itemStack)
	{
		int i = MathHelper.clamp_int(itemStack.getItemDamage(), 0, icons.length - 1);

		return super.getUnlocalizedName() + "_" + i;
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float px, float py, float pz)
	{
		Block block = world.getBlock(x, y, z);

		if (block == Blocks.snow_layer && (world.getBlockMetadata(x, y, z) & 7) < 1)
		{
			side = 1;
		}
		else if (block != Blocks.vine && block != Blocks.tallgrass && block != Blocks.deadbush && !block.isReplaceable(world, x, y, z))
		{
			switch (side)
			{
				case 0:
					--y;
					break;

				case 1:
					++y;
					break;

				case 2:
					--z;
					break;

				case 3:
					++z;
					break;

				case 4:
					--x;
					break;

				case 5:
					++x;
					break;
			}
		}

		if (itemStack == null || itemStack.stackSize == 0 || itemStack.getItemDamage() >= 3 || !player.canPlayerEdit(x, y, z, side, itemStack))
		{
			return false;
		}
		else if (world.canPlaceEntityOnSide(ModCenter.BlockTrackDecoration, x, y, z, false, side, player, itemStack) && world.isSideSolid(x, y - 1, z, ForgeDirection.UP))
		{
			if (placeBlockAt(itemStack, player, world, x, y, z, side, px, py, pz, ModCenter.BlockTrackDecoration.onBlockPlaced(world, x, y, z, side, px, py, pz, ((itemStack.getItemDamage() * 4) + ZnDMathHelper.getRotation(player.rotationYawHead)))))
			{
				world.playSoundEffect(	x + 0.5D, y + 0.5D, z + 0.5D, ModCenter.BlockTrackDecoration.stepSound.func_150496_b(), (ModCenter.BlockTrackDecoration.stepSound.getVolume() + 1.0F) / 2.0F,
										ModCenter.BlockTrackDecoration.stepSound.getPitch() * 0.8F);
				--itemStack.stackSize;
			}

			return true;
		}

		return false;
	}

	/**
	 * Called to actually place the block, after the location is determined
	 * and all permission checks have been made.
	 *
	 * @param itemStack - The item stack that was used to place the block. This can be changed inside the method.
	 * @param player - The player who is placing the block. Can be null if the block is not being placed by a player.
	 * @param side - The side the player (or machine) right-clicked on.
	 */
	public boolean placeBlockAt(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ, int meta)
	{
		if (!world.setBlock(x, y, z, ModCenter.BlockTrackDecoration, meta, 3))
		{
			return false;
		}

		if (world.getBlock(x, y, z) == ModCenter.BlockTrackDecoration)
		{
			ModCenter.BlockTrackDecoration.onBlockPlacedBy(world, x, y, z, player, itemStack);
			ModCenter.BlockTrackDecoration.onPostBlockPlaced(world, x, y, z, meta);
		}

		return true;
	}


	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			icons[i] = iconReg.registerIcon(ModData.ID + ":itemTrackPart_" + i);
		}
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		/* Wooden track bed */
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 8, 0), "L L", "L L", "   ", 'L', "logWood"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 8, 0), "   ", "L L", "L L", 'L', "logWood"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 16, 0), "L L", "LSL", "   ", 'L', "logWood", 'S', "bucketCreosote"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 16, 0), "   ", "L L", "LSL", 'L', "logWood", 'S', "bucketCreosote"));

		/* Concrete track bed */
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 16, 1), "S S", "S S", "S S", 'S', ModCenter.BlockBallastConcrete));

		/* Rails */
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 16, 2), "S S", "S S", "S S", 'S', "ingotSteel"));

		/* Plates */
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 8, 3), "PPP", "SSS", "   ", 'S', "plateSteel", 'P', "dustPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 8, 3), "   ", "PPP", "SSS", 'S', "plateSteel", 'P', "dustPlastic"));

		/* Clamps */
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 8, 4), "S S", " L ", "   ", 'S', "nuggetSteel", 'L', "plateSteel"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 8, 4), "   ", "S S", " L ", 'S', "nuggetSteel", 'L', "plateSteel"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 16, 4), "S S", " P ", " L ", 'S', "nuggetSteel", 'L', "plateSteel", 'P', "sheetPlastic"));

		/* Stilts */
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 16, 5), "SDS", " S ", " S ", 'S', "ingotSteel", 'D', "dyeYellow"));

		/* Buffer */
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 6), "PPP", " S ", "S S", 'S', "ingotSteel", 'P', "plateSteel"));

		/* Magnet */
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 2, 7), " Y ", " S ", " D ", 'Y', new ItemStack(Items.dye, 1, 11), 'S', "plateSteel", 'D', new ItemStack(ModCenter.ItemPart, 1, 6)));
	}

	@Override
	public ETrackPart type(ItemStack itemStack)
	{
		switch (itemStack.getItemDamage())
		{
			case 0:
			case 1:
				return ETrackPart.TRACKBED;

			case 2:
				return ETrackPart.RAIL;

			case 3:
				return ETrackPart.PLATE;

			case 4:
				return ETrackPart.FASTENING;

			case 6:
				return ETrackPart.BUFFER;

			case 7:
				return ETrackPart.MAGNET;
		}

		return null;
	}
}
