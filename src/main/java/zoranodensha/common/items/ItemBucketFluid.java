package zoranodensha.common.items;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDispenser;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.dispenser.IBehaviorDispenseItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import zoranodensha.common.blocks.tileEntity.TileEntityContainerInventory;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.fluids.EFluid;



public class ItemBucketFluid extends ItemBucket
{
	public ItemBucketFluid(Block block, EFluid type)
	{
		super(block);

		setContainerItem(Items.bucket);
		setCreativeTab(ModCreativeTabs.generic);
		setUnlocalizedName(ModData.ID + ".items.bucket_" + type.ordinal());

		/* Register this bucket with corresponding behavior. */
		Object obj = BlockDispenser.dispenseBehaviorRegistry.getObject(Items.water_bucket);
		if (obj instanceof IBehaviorDispenseItem)
		{
			BlockDispenser.dispenseBehaviorRegistry.putObject(this, obj);
		}
		else
		{
			ModCenter.log.warn(String.format("[%s] Couldn't register fluid bucket dispense behaviour.", ItemBucketFluid.class.getSimpleName()));
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean par4)
	{
		if (TileEntityContainerInventory.getBurnTime(itemStack) > 0)
		{
			list.add(TileEntityContainerInventory.getBurnTime(itemStack) + " Fuel Units");
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemBucket_" + this.getUnlocalizedName().substring(this.getUnlocalizedName().length() - 1));
	}
}
