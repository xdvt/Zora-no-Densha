package zoranodensha.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.common.blocks.BlockTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseGag;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.registry.ModTrackRegistry;
import zoranodensha.common.network.packet.PacketChatMessage;
import zoranodensha.common.util.ZnDMathHelper;



public class ItemRailwayTool extends Item
{
	private static boolean registered = false;



	public ItemRailwayTool()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setMaxStackSize(1);
		setUnlocalizedName(ModData.ID + ".items.railwayTool");
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float px, float py, float pz)
	{
		if (!player.capabilities.isCreativeMode || !ModCenter.cfg.blocks_items.enableInstantTracks || itemStack == null)
		{
			return false;
		}

		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof ITrackBase)
		{
			if (!itemStack.hasTagCompound())
			{
				itemStack.setTagCompound(new NBTTagCompound());
			}

			if (tileEntity instanceof TileEntityTrackBaseGag)
			{
				int[] i = ((TileEntityTrackBaseGag)tileEntity).getSourceCoords();
				if (i == null || i.length != 3)
				{
					world.setBlockToAir(x, y, z);
					return false;
				}

				tileEntity = world.getTileEntity(i[0], i[1], i[2]);
				if (!(tileEntity instanceof ITrackBase))
				{
					return false;
				}
			}

			NBTTagCompound trackNBT = new NBTTagCompound();
			tileEntity.writeToNBT(trackNBT);
			itemStack.getTagCompound().setTag("trackNBT", trackNBT);

			if (!world.isRemote)
			{
				PacketChatMessage.sendMessageToPlayer(player, ".text.railwayTool.copied");
			}

			return true;
		}

		if (itemStack.hasTagCompound() && itemStack.getTagCompound().hasKey("trackNBT"))
		{
			NBTTagCompound trackNBT = (NBTTagCompound)itemStack.getTagCompound().getTag("trackNBT");
			if (trackNBT != null)
			{
				Block block = world.getBlock(x, y, z);

				if (block == Blocks.snow_layer && (world.getBlockMetadata(x, y, z) & 7) < 1)
				{
					side = 1;
				}
				else if (block != Blocks.vine && block != Blocks.tallgrass && block != Blocks.deadbush && !block.isReplaceable(world, x, y, z))
				{
					switch (side)
					{
						case 0:
							--y;
							break;

						case 1:
							++y;
							break;

						case 2:
							--z;
							break;

						case 3:
							++z;
							break;

						case 4:
							--x;
							break;

						case 5:
							++x;
							break;
					}
				}

				if (itemStack.stackSize == 0 || !player.canPlayerEdit(x, y, z, side, itemStack))
				{
					return false;
				}
				else if (world.canPlaceEntityOnSide(ModCenter.BlockTrackBase, x, y, z, false, side, player, itemStack) && world.isSideSolid(x, y - 1, z, ForgeDirection.UP))
				{
					EDirection dir = EDirection.fromString(trackNBT.getString("Direction"));
					IRailwaySection section = ModTrackRegistry.getSection(trackNBT.getString("Instance"));
					int rotation = (ZnDMathHelper.getRotation(player.rotationYawHead) + (dir == EDirection.RIGHT ? 1 : 3)) % 4;

					if (!BlockTrackBase.interactWithGagBlocks(world, x, y, z, rotation, section, dir, 1))
					{
						return false;
					}

					trackNBT.setInteger("SectionRotation", rotation);
					tileEntity = new TileEntityTrackBase();
					tileEntity.readFromNBT(trackNBT);

					world.setBlock(x, y, z, ModCenter.BlockTrackBase, 0, 2);
					world.setTileEntity(x, y, z, tileEntity);

					if ((tileEntity = world.getTileEntity(x, y, z)) instanceof ITrackBase)
					{
						ITrackBase trackBase = (ITrackBase)tileEntity;

						if (trackBase.getInstanceOfShape() != null)
						{
							world.markBlockForUpdate(x, y, z);
							world.notifyBlockChange(x, y, z, ModCenter.BlockTrackBase);

							return BlockTrackBase.interactWithGagBlocks(world, x, y - 1, z, trackBase.getOrientation(), section, trackBase.getDirectionOfSection(), 4);
						}
					}

					BlockTrackBase.interactWithGagBlocks(world, x, y, z, rotation, section, dir, 2);
					world.removeTileEntity(x, y, z);
					world.setBlockToAir(x, y, z);
				}
			}
		}

		return false;
	}


	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemRailwayTool");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		GameRegistry.addRecipe(new ShapedOreRecipe(	new ItemStack(this), "WC ", "CMB", " PB", 'W', new ItemStack(ModCenter.ItemPart, 1, 6), 'C', "circuitBasic", 'M', new ItemStack(ModCenter.ItemPart, 1, 2), 'B', "dyeBlack", 'P',
													new ItemStack(ModCenter.ItemPart, 1, 0)));
	}
}
