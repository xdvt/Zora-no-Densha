package zoranodensha.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemPlastic extends Item
{
	private static boolean registered = false;



	public ItemPlastic()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setUnlocalizedName(ModData.ID + ".items.plastic");
	}


	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemPlastic");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}
		registered = true;

		OreDictionary.registerOre("dustPlastic", this);
		GameRegistry.addSmelting(new ItemStack(Item.getItemFromBlock(ModCenter.BlockTar), 1, 0), new ItemStack(this, 4), 0.05F);
	}
}
