package zoranodensha.common.items;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import zoranodensha.common.core.ModData;



public class ItemEngineerTableTrains extends Item
{
	private IIcon[] icons = new IIcon[1];



	public ItemEngineerTableTrains()
	{
		setHasSubtypes(true);
		setMaxDamage(0);
		setMaxStackSize(0);
		setUnlocalizedName(ModData.ID + ".items.engineerTableTrains");
	}

	@Override
	public IIcon getIconFromDamage(int i)
	{
		return icons[i];
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List list)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public String getUnlocalizedName(ItemStack itemStack)
	{
		int i = MathHelper.clamp_int(itemStack.getItemDamage(), 0, icons.length - 1);

		return super.getUnlocalizedName() + "_" + i;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister iconReg)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			icons[i] = iconReg.registerIcon(ModData.ID + ":itemEngineerTableTrains");
		}
	}
}
