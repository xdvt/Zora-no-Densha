package zoranodensha.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.IDriverKey;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemDriverKey extends Item implements IDriverKey
{
	private static boolean registered = false;



	public ItemDriverKey()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setMaxStackSize(1);
		setUnlocalizedName(ModData.ID + ".items.driverKey");
	}

	@Override
	public boolean getCanAccess(APartType type, EntityLivingBase entity)
	{
		return (entity instanceof EntityPlayer);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemKey");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}
		registered = true;

		if (ModCenter.cfg.blocks_items.enableKeyCrafting)
		{
			GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this), "  S", "SS ", "SS ", 'S', "nuggetSteel"));
			GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this), " SS", " SS", "S  ", 'S', "nuggetSteel"));
			GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this), "S  ", " SS", " SS", 'S', "nuggetSteel"));
			GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this), "SS ", "SS ", "  S", 'S', "nuggetSteel"));
		}
	}
}
