package zoranodensha.common.items;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.client.resources.I18n;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.ERailwayState;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.blocks.BlockTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.registry.ModTrackRegistry;
import zoranodensha.common.entity.EntityTrackOutline;
import zoranodensha.common.util.ZnDMathHelper;



public class ItemEngineersBlueprint extends Item
{
	public ItemEngineersBlueprint()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setMaxStackSize(1);
		setUnlocalizedName(ModData.ID + ".items.engineersBlueprint");
	}

	@Override
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		if (itemStack.hasTagCompound())
		{
			NBTTagCompound nbt = itemStack.getTagCompound();
			if (nbt.hasKey("Instance"))
			{
				EDirection dir = EDirection.fromString(nbt.getString("Flag"));
				String name = "track." + ModData.ID + "." + nbt.getString("Instance");

				return (super.getItemStackDisplayName(itemStack) + " §7" + (ModCenter.cfg.general.isRemoteEnv ? I18n.format(name) : name)
						+ (dir != EDirection.NONE ? " (" + dir.toString() + ")" : ""));
			}
			
			String s = VehicleHelper.getVehicleName(nbt);
			if (s != null)
			{
				return super.getItemStackDisplayName(itemStack) + (nbt.getBoolean(Train.EDataKey.IS_EPIC.key) ? " §d" : " §7") + s;
			}
		}

		return super.getItemStackDisplayName(itemStack);
	}

	@Override
	public boolean getShareTag()
	{
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void getSubItems(Item item, CreativeTabs creativeTab, List list)
	{
		ModTrackRegistry.addBlueprintsToList(list);
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float px, float py, float pz)
	{
		if (itemStack == null || itemStack.stackSize == 0 || !itemStack.hasTagCompound())
		{
			return false;
		}

		NBTTagCompound nbt = itemStack.getTagCompound();

		if (nbt.hasKey("Type"))
		{
			Block block = world.getBlock(x, y, z);

			if (block == Blocks.snow_layer && (world.getBlockMetadata(x, y, z) & 7) < 1)
			{
				side = 1;
			}
			else if (block != Blocks.vine && block != Blocks.tallgrass && block != Blocks.deadbush && !block.isReplaceable(world, x, y, z))
			{
				switch (side)
				{
					case 0:
						--y;
						break;

					case 1:
						++y;
						break;

					case 2:
						--z;
						break;

					case 3:
						++z;
						break;

					case 4:
						--x;
						break;

					case 5:
						++x;
						break;
				}
			}

			if (itemStack.getItemDamage() >= 3 || !player.canPlayerEdit(x, y, z, side, itemStack))
			{
				return false;
			}
			else if (world.canPlaceEntityOnSide(ModCenter.BlockTrackBase, x, y, z, false, side, player, itemStack) && world.isSideSolid(x, y - 1, z, ForgeDirection.UP))
			{
				String type = nbt.getString("Type");

				if ("RailwaySection".equals(type) && nbt.hasKey("Instance"))
				{
					EDirection dir = EDirection.fromString(nbt.getString("Flag"));
					IRailwaySection section = ModTrackRegistry.getSection(nbt.getString("Instance"));

					if (section != null)
					{
						int rotation = (ZnDMathHelper.getRotation(player.rotationYawHead) + (dir == EDirection.RIGHT ? 1 : 3)) % 4;

						if (!BlockTrackBase.interactWithGagBlocks(world, x, y, z, rotation, section, dir, 1))
						{
							return false;
						}

						TileEntity tileEntity = section.initializeTrack(new TileEntityTrackBase(rotation, ERailwayState.BUILDINGGUIDE, dir, section));

						if (tileEntity instanceof ITrackBase)
						{
							world.setBlock(x, y, z, ModCenter.BlockTrackBase, 0, 2);
							world.setTileEntity(x, y, z, tileEntity);
							world.markBlockForUpdate(x, y, z);
							world.notifyBlockChange(x, y, z, ModCenter.BlockTrackBase);
							world.playSoundEffect(x, y, z, ModData.ID + ":block_placement", 0.3F, 1.0F);

							return true;
						}

						throw new IllegalArgumentException("[ModTrackRegistry] A mod initialised a track's TileEntity without implementing ITrackBase!");
					}
				}
			}
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onUpdate(ItemStack itemStack, World world, Entity entity, int slot, boolean isHeld)
	{
		if (isHeld && world.isRemote)
		{ /* The client-side call is fully intended. */

			if (itemStack == null || itemStack.stackSize == 0 || !itemStack.hasTagCompound() || !itemStack.getTagCompound().hasKey("Type"))
			{
				return;
			}

			MovingObjectPosition movObjPos = Minecraft.getMinecraft().renderViewEntity.rayTrace(7.0D, 1.0F);

			if (movObjPos == null)
			{
				return;
			}

			int x = movObjPos.blockX;
			int y = movObjPos.blockY;
			int z = movObjPos.blockZ;
			int side = movObjPos.sideHit;

			Block block = world.getBlock(x, y, z);

			if (block == Blocks.snow_layer && (world.getBlockMetadata(x, y, z) & 7) < 1)
			{
				side = 1;
			}
			else if (block != Blocks.vine && block != Blocks.tallgrass && block != Blocks.deadbush && !block.isReplaceable(world, x, y, z))
			{
				switch (side)
				{
					case 0:
						--y;
						break;

					case 1:
						++y;
						break;

					case 2:
						--z;
						break;

					case 3:
						++z;
						break;

					case 4:
						--x;
						break;

					case 5:
						++x;
						break;
				}
			}

			if (itemStack.getItemDamage() >= 3 || (entity instanceof EntityPlayer && !((EntityPlayer)entity).canPlayerEdit(x, y, z, side, itemStack)))
			{
				return;
			}

			NBTTagCompound nbt = itemStack.getTagCompound();
			String type = nbt.getString("Type");

			if ("RailwaySection".equals(type) && nbt.hasKey("Instance"))
			{
				EDirection dir = EDirection.fromString(nbt.getString("Flag"));
				IRailwaySection section = ModTrackRegistry.getSection(nbt.getString("Instance"));
				int rotation = (ZnDMathHelper.getRotation(entity.rotationYaw) + (dir == EDirection.RIGHT ? 1 : 3)) % 4;

				if (section != null)
				{
					TileEntity tileEntity = section.initializeTrack(new TileEntityTrackBase(rotation, ERailwayState.FINISHED, dir, section));

					if (tileEntity instanceof ITrackBase)
					{
						EntityTrackOutline entityTrack = new EntityTrackOutline(world, x, y, z, (world.canPlaceEntityOnSide(ModCenter.BlockTrackBase, x, y, z, false, side, entity, itemStack)
								&& world.isSideSolid(x, y - 1, z, ForgeDirection.UP) && BlockTrackBase.interactWithGagBlocks(world, x, y, z, rotation, section, dir, 0)), (ITrackBase)tileEntity);
						world.loadedEntityList.add(entityTrack);
						world.onEntityAdded(entityTrack);
					}
					else
					{
						throw new IllegalArgumentException("[ModTrackRegistry] A mod initialised a track's TileEntity without implementing ITrackBase!");
					}
				}
			}
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemEngineersBlueprint");
	}
}
