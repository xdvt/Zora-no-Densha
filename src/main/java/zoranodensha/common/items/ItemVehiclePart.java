package zoranodensha.common.items;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.AVehiclePartRegistry;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.registry.ModPartRegistry;



public class ItemVehiclePart extends Item
{
	public ItemVehiclePart()
	{
		setCreativeTab(ModCreativeTabs.vehicleParts);
		setMaxDamage(0);
		setMaxStackSize(8);
		setUnlocalizedName(ModData.ID + ".items.vehiclePart");
	}


	@Override
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		VehParBase part = AVehiclePartRegistry.getPartFromItemStack(itemStack);
		if (part != null)
		{
			return (StatCollector.translateToLocal(part.getUnlocalizedName()) + " [" + super.getItemStackDisplayName(itemStack) + "]");
		}

		return super.getItemStackDisplayName(itemStack);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List list)
	{
		list.addAll(ModPartRegistry.getPartItemStacks());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		/* Empty on purpose. */
	}
}