package zoranodensha.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import zoranodensha.common.blocks.BlockRefinery;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemMachineKit extends Item
{
	private static boolean registered = false;



	public ItemMachineKit()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setMaxStackSize(16);
		setUnlocalizedName(ModData.ID + ".items.machineKit");
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float fx, float fy, float fz)
	{
		if (!world.isRemote && itemStack != null)
		{
			Block block = world.getBlock(x, y, z);

			if (block == Blocks.iron_block)
			{
				int type = (side == 5 || side == 4) ? 0 : (side == 3 || side == 2) ? 1 : -1;

				if (type > -1)
				{
					boolean isSurvival = !player.capabilities.isCreativeMode;
					int[][] offset = new int[][] { { 0, 1 }, { 1, 0 } };

					if (isSurvival && itemStack.stackSize < 3)
					{
						return false;
					}
					else if (world.getBlock(x - offset[type][0], y, z - offset[type][1]) == Blocks.iron_block && world.getBlock(x + offset[type][0], y, z + offset[type][1]) == Blocks.iron_block)
					{
						world.setBlock(x - offset[type][0], y, z - offset[type][1], ModCenter.BlockRetorter, 4 + type, 2);
						world.setBlock(x, y, z, ModCenter.BlockRetorter, type, 2);
						world.setBlock(x + offset[type][0], y, z + offset[type][1], ModCenter.BlockRetorter, 2 + type, 2);

						if (isSurvival)
						{
							itemStack.stackSize -= 3;
						}

						return true;
					}
				}
			}
			else
			{
				BlockRefinery.refreshLists();
				boolean isSurvival = !player.capabilities.isCreativeMode;

				if (isSurvival && itemStack.stackSize < 4)
				{
					return false;
				}
				else if (BlockRefinery.blocksListSteel.contains(world.getBlock(x, y, z)) || BlockRefinery.blocksListStone.contains(world.getBlock(x, y, z)))
				{
					for (int j = 0; j < BlockRefinery.HEIGHT + 1; ++j)
					{
						for (int i = -1; i < 2; ++i)
						{
							for (int k = -1; k < 2; ++k)
							{
								if (BlockRefinery.canPlaceAt(world, x + i, y + j, z + k, true))
								{
									if (isSurvival)
									{
										itemStack.stackSize -= 4;
									}

									return true;
								}
							}
						}
					}
				}
			}
		}

		return false;
	}


	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemMachineKit");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		GameRegistry.addRecipe(new ShapelessOreRecipe(	new ItemStack(this), new ItemStack(ModCenter.ItemPart, 1, 7), "circuitBasic", new ItemStack(ModCenter.ItemPart, 1, 2), "ingotIron", "plateSteel",
														"plateSteel"));
	}
}
