package zoranodensha.common.items;

import java.util.List;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemPart extends Item
{
	private static boolean registered = false;


	private IIcon[] icons = new IIcon[8];



	public ItemPart()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setHasSubtypes(true);
		setMaxDamage(0);
		setUnlocalizedName(ModData.ID + ".items.part");
	}

	@Override
	public IIcon getIconFromDamage(int i)
	{
		return icons[i];
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List list)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			// Skip antenna and large monitor parts.
			if (i == 4 || i == 5)
			{
				continue;
			}
				
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public String getUnlocalizedName(ItemStack itemStack)
	{
		int i = MathHelper.clamp_int(itemStack.getItemDamage(), 0, icons.length - 1);

		return super.getUnlocalizedName() + "_" + i;
	}


	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister iconReg)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			icons[i] = iconReg.registerIcon(ModData.ID + ":itemPart_" + i);
		}
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		/*
		 * Metadata Table:
		 *
		 * 0: Plastic Casing
		 * 1: Circuit
		 * 2: Small Monitor
		 * 3: Medium Monitor
		 * 4: Large Monitor
		 * 5: Antenna
		 * 6: Receiver/Sensor
		 * 7: Steel Casing
		 */
		OreDictionary.registerOre("circuitBasic", new ItemStack(this, 1, 1));
		OreDictionary.registerOre("steelCasing", new ItemStack(this, 1, 7));
		
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 0), "PPP", "P P", "   ", 'P', "sheetPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 0), "   ", "P P", "PPP", 'P', "sheetPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 0), "P P", "PPP", "   ", 'P', "sheetPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 0), "   ", "PPP", "P P", 'P', "sheetPlastic"));

		GameRegistry.addRecipe(new ShapedOreRecipe(	new ItemStack(this, 2, 1), "DGD", "RER", "PPP", 'D', "dustGlowstone", 'G', "dyeGreen", 'R', "dustRedstone", 'E', Items.repeater, 'P',
													"dustPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 2, 1), "DGD", "RER", " S ", 'D', "dustGlowstone", 'G', "dyeGreen", 'R', "dustRedstone", 'E', Items.repeater, 'S', "plateSteel"));

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 2), "GRG", "CPC", "   ", 'G', "dustGlowstone", 'R', "dustRedstone", 'C', "circuitBasic", 'P', "sheetPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 2), "   ", "GRG", "CPC", 'G', "dustGlowstone", 'R', "dustRedstone", 'C', "circuitBasic", 'P', "sheetPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 2), "GRG", "CSC", "   ", 'G', "dustGlowstone", 'R', "dustRedstone", 'C', "circuitBasic", 'S', "plateSteel"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 2), "   ", "GRG", "CSC", 'G', "dustGlowstone", 'R', "dustRedstone", 'C', "circuitBasic", 'S', "plateSteel"));

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 3), "C C", "GRG", "CPC", 'G', "dustGlowstone", 'R', "dustRedstone", 'C', "circuitBasic", 'P', "sheetPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 3), "C C", "GRG", "CSC", 'G', "dustGlowstone", 'R', "dustRedstone", 'C', "circuitBasic", 'S', "plateSteel"));

		// Large monitor and antenna have been disabled as they are unused.
		// GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 4), "CPC", "GRG", "CPC", 'G', "dustGlowstone", 'R', "dustRedstone", 'C', "circuitBasic", 'P', new ItemStack(this, 1, 0)));

		// GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 2, 5), "I I", " R ", "   ", 'I', "nuggetSteel", 'R', "dustRedstone"));
		// GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 2, 5), "   ", "I I", " R ", 'I', "nuggetSteel", 'R', "dustRedstone"));

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 6), "   ", "CRR", "PPP", 'C', "circuitBasic", 'R', "dustRedstone", 'P', "dustPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 6), "CRR", "PPP", "   ", 'C', "circuitBasic", 'R', "dustRedstone", 'P', "dustPlastic"));

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 7), "SSS", "S S", "   ", 'S', "plateSteel"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 7), "   ", "S S", "SSS", 'S', "plateSteel"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 7), "S S", "SSS", "   ", 'S', "plateSteel"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 1, 7), "   ", "SSS", "S S", 'S', "plateSteel"));
	}
}
