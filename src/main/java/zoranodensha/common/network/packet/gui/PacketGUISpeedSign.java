package zoranodensha.common.network.packet.gui;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.signals.ISpeedSign;
import zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign;
import zoranodensha.signals.common.ASpeedSign;



public class PacketGUISpeedSign implements IMessage
{

	private int teX, teY, teZ;

	private Integer newSpeedLimit = null;
	private Integer newOffset = null;



	/**
	 * Initialises a new instance of the
	 * {@link zoranodensha.common.network.packet.gui.PacketGUISpeedSign} class.
	 */
	public PacketGUISpeedSign()
	{

	}


	/**
	 * Initialises a new instance of the
	 * {@link zoranodensha.common.network.packet.gui.PacketGUISpeedSign} class.
	 * 
	 * @param x - The X position of the tile entity.
	 * @param y - The Y position of the tile entity.
	 * @param z - The Z position of the tile entity.
	 */
	public PacketGUISpeedSign(int x, int y, int z)
	{
		/*
		 * Set the tile entity position based on the specified parameters.
		 */
		this.teX = x;
		this.teY = y;
		this.teZ = z;
	}


	/**
	 * Adds a new {@code offset} field to this packet.
	 * 
	 * @param offset - The new offset to assign to the speed sign. {@code -1} for a left offset, {@code 0} for a centre offset, {@code +1} for a right offset.
	 * @return - This packet, for easier instantiation.
	 */
	public PacketGUISpeedSign addNewOffset(int offset)
	{
		newOffset = offset;

		return this;
	}


	/**
	 * Adds a new {@code speedLimit} field to this packet.
	 * 
	 * @param limit - The new speed limit to assign to the speed limit sign.
	 * @return - This packet, for easier instantiation.
	 */
	public PacketGUISpeedSign addNewSpeedLimit(int limit)
	{
		newSpeedLimit = limit;

		return this;
	}


	@Override
	public void fromBytes(ByteBuf buffer)
	{
		/*
		 * Read the tile entity position.
		 */
		teX = buffer.readInt();
		teY = buffer.readInt();
		teZ = buffer.readInt();

		/*
		 * Speed Limit
		 */
		if (buffer.readBoolean())
		{
			newSpeedLimit = buffer.readInt();
		}
		else
		{
			newSpeedLimit = null;
		}

		/*
		 * Offset
		 */
		if (buffer.readBoolean())
		{
			newOffset = buffer.readInt();
		}
		else
		{
			newOffset = null;
		}
	}


	@Override
	public void toBytes(ByteBuf buffer)
	{
		/*
		 * Save tile entity position.
		 */
		buffer.writeInt(teX);
		buffer.writeInt(teY);
		buffer.writeInt(teZ);

		/*
		 * Save new speed limit.
		 */
		if (newSpeedLimit != null)
		{
			buffer.writeBoolean(true);
			buffer.writeInt(newSpeedLimit);
		}
		else
		{
			buffer.writeBoolean(false);
		}

		/*
		 * Save new offset.
		 */
		if (newOffset != null)
		{
			buffer.writeBoolean(true);
			buffer.writeInt(newOffset);
		}
		else
		{
			buffer.writeBoolean(false);
		}
	}



	/**
	 * Internal handler class for
	 * {@link zoranodensha.common.network.packet.gui.PacketGUISpeedSign} messages.
	 * 
	 * @author Jaffa
	 */
	public static class Handler implements IMessageHandler<PacketGUISpeedSign, IMessage>
	{

		@Override
		public IMessage onMessage(PacketGUISpeedSign message, MessageContext context)
		{
			TileEntity tileEntity = context.getServerHandler().playerEntity.worldObj.getTileEntity(message.teX, message.teY, message.teZ);

			if (tileEntity instanceof TileEntitySpeedSign)
			{
				ISpeedSign sign = ((TileEntitySpeedSign)tileEntity).getSpeedSign();

				/*
				 * Go through each possible field and apply them if they are applicable.
				 */
				if (message.newSpeedLimit != null)
				{
					sign.setSpeedLimit(message.newSpeedLimit);
				}

				/*
				 * The offset field is only applicable to ASpeedSign instances.
				 */
				if (message.newOffset != null)
				{
					if (sign instanceof ASpeedSign)
					{
						ASpeedSign abstractSpeedSign = (ASpeedSign)sign;

						abstractSpeedSign.setOffset(message.newOffset);
					}
				}
			}

			return null;
		}
	}

}
