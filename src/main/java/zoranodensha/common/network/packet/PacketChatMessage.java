package zoranodensha.common.network.packet;

import javax.annotation.Nullable;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



/**
 * Packet to send a chat message to the client.
 */
public class PacketChatMessage implements IMessage
{
	private String[] msg;

	@Nullable private EnumChatFormatting format;



	public PacketChatMessage()
	{
		;
	}

	public PacketChatMessage(@Nullable EnumChatFormatting format, String... msg)
	{
		this.format = format;
		this.msg = msg;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		/* Read message Strings. */
		msg = new String[buf.readInt()];
		for (int i = msg.length - 1; i >= 0; --i)
		{
			msg[i] = ByteBufUtils.readUTF8String(buf);
		}

		/* Read format name. */
		if (buf.readBoolean())
		{
			format = EnumChatFormatting.getValueByName(ByteBufUtils.readUTF8String(buf));
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		/* Write message Strings. */
		buf.writeInt(msg.length);
		for (String s : msg)
		{
			ByteBufUtils.writeUTF8String(buf, s);
		}

		/* Write format name. */
		if (format != null)
		{
			buf.writeBoolean(true);
			ByteBufUtils.writeUTF8String(buf, format.getFriendlyName());
		}
		else
		{
			buf.writeBoolean(false);
		}
	}

	/**
	 * Static helper method to send a number of messages to the given player.
	 */
	public static void sendMessageToPlayer(EntityPlayer player, String... msg)
	{
		ModCenter.snw.sendTo(new PacketChatMessage(EnumChatFormatting.YELLOW, msg), (EntityPlayerMP)player);
	}



	public static class Handler implements IMessageHandler<PacketChatMessage, IMessage>
	{
		@Override
		public IMessage onMessage(PacketChatMessage packet, MessageContext ctx)
		{
			EntityPlayer player = VehicleHelper.getPlayerFromContext(ctx);

			for (int i = 0; i < packet.msg.length; ++i)
			{
				ChatComponentTranslation text = new ChatComponentTranslation(ModData.ID + packet.msg[i]);
				if (packet.format != null)
				{
					text.setChatStyle(new ChatStyle().setColor(packet.format));
				}
				player.addChatMessage(text);
			}

			return null;
		}
	}
}