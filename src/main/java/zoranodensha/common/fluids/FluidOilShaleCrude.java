package zoranodensha.common.fluids;

import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;



public class FluidOilShaleCrude extends Fluid
{
	public FluidOilShaleCrude()
	{
		super("oilShaleCrude");

		setBlock(ModCenter.BlockFluidOilShaleCrude);
		setDensity(992);
		setTemperature(288);
		setViscosity(2400);
	}

	// @Override
	// public int getColor() {
	//
	// return 0x292929;
	// }

	@Override
	public IIcon getFlowingIcon()
	{
		return block.getIcon(2, 0);
	}

	@Override
	public IIcon getStillIcon()
	{
		return block.getIcon(0, 0);
	}
}
