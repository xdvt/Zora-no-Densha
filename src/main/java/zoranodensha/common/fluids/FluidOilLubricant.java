package zoranodensha.common.fluids;

import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;



public class FluidOilLubricant extends Fluid
{
	public FluidOilLubricant()
	{
		super("oilLubricant");

		setBlock(ModCenter.BlockFluidOilLubricant);
		setDensity(950);
		setTemperature(288);
		setViscosity(7500);
	}

	// @Override
	// public int getColor() {
	//
	// return 0x505050;
	// }

	@Override
	public IIcon getFlowingIcon()
	{
		return block.getIcon(2, 0);
	}

	@Override
	public IIcon getStillIcon()
	{
		return block.getIcon(0, 0);
	}
}
