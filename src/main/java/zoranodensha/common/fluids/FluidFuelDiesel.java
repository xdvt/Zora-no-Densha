package zoranodensha.common.fluids;

import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;



public class FluidFuelDiesel extends Fluid
{
	public FluidFuelDiesel()
	{
		super("fuelDiesel");

		setBlock(ModCenter.BlockFluidFuelDiesel);
		setDensity(832);
		setTemperature(313);
		setViscosity(900);
	}

	// @Override
	// public int getColor() {
	//
	// return 0xBBA021;
	// }

	@Override
	public IIcon getFlowingIcon()
	{
		return block.getIcon(2, 0);
	}

	@Override
	public IIcon getStillIcon()
	{
		return block.getIcon(0, 0);
	}
}
