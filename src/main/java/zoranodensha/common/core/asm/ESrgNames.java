package zoranodensha.common.core.asm;

/**
 * Various (de-)obfuscated method names used by Zora no Densha and its API packages.
 */
public enum ESrgNames
{
	/*
	 * Fields
	 */
	F_fallDistance("fallDistance", "field_70143_R"),
	F_isInReverse("isInReverse", "field_70499_f"),
	F_defaultResourcePacks("defaultResourcePacks", "field_110449_ao"),

	/*
	 * Methods
	 */
	M_applyDrag("applyDrag", "func_94101_h"),
	M_dismountEntity("dismountEntity", "func_110145_l"),
	M_endSection("endSection", "func_76319_b"),
	M_fall("fall", "func_70069_a"),
	M_func_145775_I("func_145775_I"),
	M_func_145821_a("func_145821_a"),
	M_getYOffset("getYOffset", "func_70033_W"),
	M_glDepthMask("glDepthMask"),
	M_onUpdate("onUpdate", "func_70071_h_"),
	M_renderDebugBoundingBox("renderDebugBoundingBox", "func_85094_b"),
	M_setRotation("setRotation", "func_70101_b"),

	/*
	 * Classes
	 */
	C_Block("net/minecraft/block/Block", "aji"),
	C_Entity("net/minecraft/entity/Entity", "sa"),
	C_EntityLivingBase("net/minecraft/entity/EntityLivingBase", "sv"),
	C_EntityMinecart("net/minecraft/entity/item/EntityMinecart", "xl"),
	C_GL11("org/lwjgl/opengl/GL11"),
	C_Profiler("net/minecraft/profiler/Profiler", "qi"),
	C_RenderManager("net/minecraft/client/renderer/entity/RenderManager", "bnn"),
	;


	private String deobfName;
	private String obfName;



	private ESrgNames(String deobfName, String obfName)
	{
		this.deobfName = deobfName;
		this.obfName = obfName;
	}
	
	private ESrgNames(String obfName)
	{
		this(obfName, obfName);
	}


	/**
	 * Helper method that can be called with a cached flag that indicates (de-)obfuscated environment.
	 * <p>
	 *
	 * To know whether the game runs in a (de-)obfuscated environment, assign {@code Boolean.TRUE.equals(Launch.blackboard.get("fml.deobfuscatedEnvironment"))} to a boolean field.
	 */
	public String val(boolean isDeobf)
	{
		return isDeobf ? deobfName : obfName;
	}
}
