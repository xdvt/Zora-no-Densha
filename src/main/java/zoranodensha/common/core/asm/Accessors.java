package zoranodensha.common.core.asm;

import static zoranodensha.common.core.asm.ESrgNames.F_isInReverse;
import static zoranodensha.common.core.asm.ESrgNames.M_applyDrag;
import static zoranodensha.common.core.asm.ESrgNames.M_fall;
import static zoranodensha.common.core.asm.ESrgNames.M_func_145775_I;
import static zoranodensha.common.core.asm.ESrgNames.M_func_145821_a;
import static zoranodensha.common.core.asm.ESrgNames.M_setRotation;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityMinecart;
import zoranodensha.common.core.ModCenter;



/**
 * Helper class to access protected methods and fields.
 */
public class Accessors
{
	/**
	 * Tries to find the specified field from within the given object.
	 * 
	 * @param obj - Object whose field shall be accessed.
	 * @param fieldName - {@link zoranodensha.common.core.asm.ESrgNames Name} of the field to retrieve.
	 * @return The reflected {@link java.lang.reflect.Field field}, or {@code null} if unsuccessful.
	 */
	public static final Field getField(Object obj, ESrgNames fieldName)
	{
		Field field = null;

		for (Class<?> clss = obj.getClass(); clss != null && !clss.equals(Object.class); clss = clss.getSuperclass())
		{
			try
			{
				field = clss.getDeclaredField(fieldName.val(ModCenter.cfg.general.isDeobfEnv));
				field.setAccessible(true);
			}
			catch (NoSuchFieldException e)
			{
				/* Silent catch. */
			}
			catch (SecurityException e)
			{
				/* Silent catch. */
			}
		}

		return field;
	}

	/**
	 * Tries to find the specified method from within the given object.
	 * 
	 * @param obj - Object whose method shall be accessed.
	 * @param methodName - {@link zoranodensha.common.core.asm.ESrgNames Name} of the method to retrieve.
	 * @param parameterTypes - Array of method parameter type classes.
	 * @return The reflected {@link java.lang.reflect.Method method}, or {@code null} if unsuccessful.
	 * 
	 * @throws IllegalArgumentException If the specified method didn't exist.
	 */
	public static final Method getMethod(Object obj, ESrgNames methodName, Class<?>... parameterTypes)
	{
		Method method = null;
		String s = methodName.val(ModCenter.cfg.general.isDeobfEnv);

		for (Class<?> clss = obj.getClass(); clss != null && !clss.equals(Object.class); clss = clss.getSuperclass())
		{
			try
			{
				if ((method = clss.getDeclaredMethod(s, parameterTypes)) != null)
				{
					method.setAccessible(true);
					return method;
				}
			}
			catch (NoSuchMethodException e)
			{
				/* Silent catch. */
			}
			catch (SecurityException e)
			{
				/* Silent catch. */
			}
		}

		return null;
	}


	public static final void applyDrag(EntityMinecart cart)
	{
		try
		{
			getMethod(cart, M_applyDrag).invoke(cart);
		}
		catch (Exception e)
		{
			/* Silent catch. */
		}
	}

	/**
	 * Called to bypass the access restriction imposed by Minecraft on Entity's fall.
	 */
	public static final void fall(Entity entity, float distance)
	{
		try
		{
			getMethod(entity, M_fall, float.class).invoke(entity, distance);
		}
		catch (Exception e)
		{
			/* Silent catch. */
		}
	}

	public static final void func_145775_I(EntityMinecart cart)
	{
		try
		{
			getMethod(cart, M_func_145775_I).invoke(cart);
		}
		catch (Exception e)
		{
			/* Silent catch. */
		}
	}

	public static final void func_145821_a(EntityMinecart cart, Object... params)
	{
		try
		{
			getMethod(cart, M_func_145821_a, int.class, int.class, int.class, double.class, double.class, Block.class, int.class).invoke(cart, params);
		}
		catch (Exception e)
		{
			/* Silent catch. */
		}
	}

	public static final boolean isInReverse(EntityMinecart cart)
	{
		try
		{
			return getField(cart, F_isInReverse).getBoolean(cart);
		}
		catch (Exception e)
		{
			/* Silent catch. */
		}
		return false;
	}

	public static final void isInReverse(EntityMinecart cart, boolean flag)
	{
		try
		{
			getField(cart, F_isInReverse).setBoolean(cart, flag);
		}
		catch (Exception e)
		{
			/* Silent catch. */
		}
	}

	public static final void setRotation(EntityMinecart cart, Object... params)
	{
		try
		{
			getMethod(cart, M_setRotation, float.class, float.class).invoke(cart, params);
		}
		catch (Exception e)
		{
			/* Silent catch. */
		}
	}
}