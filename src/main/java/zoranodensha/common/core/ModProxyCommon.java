package zoranodensha.common.core;

import java.util.ArrayList;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.structures.EEngineerTableTab;
import zoranodensha.api.structures.IEngineerTableExtension;
import zoranodensha.api.structures.IEngineerTableExtension.EngineerTableIngredientHelper;
import zoranodensha.api.structures.IEngineerTableExtension.EngineerTableRecipeHelper;
import zoranodensha.api.structures.signals.ASignalRegistry;
import zoranodensha.api.structures.signals.ASpeedSignRegistry;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.api.structures.signals.ISpeedSign;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.AVehiclePartRegistry;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.api.vehicles.part.xml.XMLPartFactory;
import zoranodensha.common.core.cfg.ModConfig;
import zoranodensha.common.core.handlers.EventHandler;
import zoranodensha.common.core.registry.ModPartRegistry;
import zoranodensha.common.core.registry.ModRecipeRegistry;
import zoranodensha.common.core.registry.ModTrackRegistry;
import zoranodensha.signals.common.zd.SignalZD_Block;
import zoranodensha.signals.common.zd.SignalZD_Distant;
import zoranodensha.signals.common.zd.SignalZD_Dwarf;
import zoranodensha.signals.common.zd.SignalZD_Flag;
import zoranodensha.signals.common.zd.SignalZD_Tram;
import zoranodensha.signals.common.zd.SpeedSignZD;
import zoranodensha.trackpack.common.TrackPackRecipes;
import zoranodensha.trackpack.common.section.ZnD_Cross10_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_Cross5_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_Cross_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Cross_0000_1131;
import zoranodensha.trackpack.common.section.ZnD_Cross_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_Curve10_1843_4500;
import zoranodensha.trackpack.common.section.ZnD_Curve_0000_1131;
import zoranodensha.trackpack.common.section.ZnD_Curve_1131_1843;
import zoranodensha.trackpack.common.section.ZnD_Curve_1843_4500;
import zoranodensha.trackpack.common.section.ZnD_SCurve_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight2_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight2_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_Straight4_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight4_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_Straight8_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight8_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_1843_1843;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_StraightMaint_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightSlope16_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightSlope_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightTrans_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_Straight_1843_1843;
import zoranodensha.trackpack.common.section.ZnD_Straight_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_Switch_0000_1131;
import zoranodensha.trackpack.common.section.ZnD_Switch_1131_0000;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicCube;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicText;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicTriangle;
import zoranodensha.vehicleParts.common.parts.bogie.*;
import zoranodensha.vehicleParts.common.parts.buffer.VehParBufferBR101;
import zoranodensha.vehicleParts.common.parts.buffer.VehParBufferDBpza;
import zoranodensha.vehicleParts.common.parts.buffer.VehParBufferRound;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateBR101;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateCabBR101;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateEndSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateFrameSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateModFreightEnd;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateModFreightFlat;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateModFreightMid;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateModFreightTrans;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisBR101;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisBottomDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisCabBR101;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisCabDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisCabFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisDoorsDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisDoorsFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisEndFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisEndSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisFillerFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisRoofSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisStairsDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisTopDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisVestibuleDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisVestibuleRoofDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisWallSWF;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerJanney;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerScrew;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerSfbg;
import zoranodensha.vehicleParts.common.parts.engine.VehParEngineDieselF7;
import zoranodensha.vehicleParts.common.parts.engine.VehParEngineElectricBR101;
import zoranodensha.vehicleParts.common.parts.other.VehParBufferBaseDBpza;
import zoranodensha.vehicleParts.common.parts.other.VehParCompressor;
import zoranodensha.vehicleParts.common.parts.other.VehParDestBoard;
import zoranodensha.vehicleParts.common.parts.other.VehParDetailSWF;
import zoranodensha.vehicleParts.common.parts.other.VehParElectricsFLIRT;
import zoranodensha.vehicleParts.common.parts.other.VehParFenderBR101;
import zoranodensha.vehicleParts.common.parts.other.VehParFenderFLIRT;
import zoranodensha.vehicleParts.common.parts.other.VehParIntercirculationDBpza;
import zoranodensha.vehicleParts.common.parts.other.VehParLamp;
import zoranodensha.vehicleParts.common.parts.other.VehParOilTanksBR101;
import zoranodensha.vehicleParts.common.parts.other.VehParPantograph;
import zoranodensha.vehicleParts.common.parts.other.VehParPloughBR101;
import zoranodensha.vehicleParts.common.parts.other.VehParPloughFLIRT;
import zoranodensha.vehicleParts.common.parts.other.VehParStairsDBpza;
import zoranodensha.vehicleParts.common.parts.seat.VehParCab;
import zoranodensha.vehicleParts.common.parts.seat.VehParSeat;
import zoranodensha.vehicleParts.common.parts.transport.VehParInventory;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportContainer;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportHopper;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportOpen;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportTankContainer;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportWood;



public class ModProxyCommon
{
	/**
	 * Creates a new instance of the mod's configuration class, depending on whether
	 * this is a remote or server proxy.
	 */
	public ModConfig getModConfig()
	{
		return new ModConfig();
	}

	/**
	 * Returns a suited {@link net.minecraft.entity.player.EntityPlayer player}
	 * instance depending on the given context.
	 */
	public EntityPlayer getPlayer(MessageContext context)
	{
		return context.getServerHandler().playerEntity;
	}


	/**
	 * Called to run registration during the main initialisation phase of Forge.
	 */
	public void mainInit(FMLInitializationEvent event)
	{
		registerEventHandlers();
		registerRenderers();
		registerAddOns();
	}


	/**
	 * Called to run registration during the post initalisation phase of Forge.
	 */
	public void postInit(FMLPostInitializationEvent event)
	{
		ModPartRegistry.registerRecipes();
		ASignalRegistry.registerRecipes();
		ASpeedSignRegistry.registerRecipes();
	}


	/**
	 * Registers all default add-ons, such as the default Track Pack and default
	 * Vehicle Parts.
	 */
	public void registerAddOns()
	{
		/*
		 * Phase 1: Register IRailwaySections
		 */
		ArrayList<IRailwaySection> iRailwaySections = new ArrayList<IRailwaySection>();
		registerDefault_IRailwaySection(iRailwaySections);

		for (IRailwaySection section : iRailwaySections)
		{
			ModTrackRegistry.registerSection(section);
		}

		/*
		 * Phase 2: Register default IVehicleParts
		 */
		OrderedPartsList parts = new OrderedPartsList();
		registerDefault_IVehiclePart(parts);

		for (VehParBase part : parts)
		{
			AVehiclePartRegistry.registerPart(part);
		}

		/* Phase 2a: Parse XML-based vehicle parts from add-on directory. */
		for (VehParBase part : XMLPartFactory.parseDirectory(ModCenter.DIR_ADDONS))
		{
			AVehiclePartRegistry.registerPart(part);
		}

		/*
		 * Phase 3a: Register default ISignals
		 */
		ArrayList<ISignal> signals = new ArrayList<ISignal>();
		registerDefault_ISignal(signals);

		for (ISignal signal : signals)
		{
			ASignalRegistry.registerSignal(signal);
		}

		/*
		 * Phase 3b: Register default ISpeedSigns
		 */
		ArrayList<ISpeedSign> speedSigns = new ArrayList<ISpeedSign>();
		registerDefault_ISpeedSign(speedSigns);

		for (ISpeedSign speedSign : speedSigns)
		{
			ASpeedSignRegistry.registerSpeedSign(speedSign);
		}

		/*
		 * Phase 4: Register IEngineerTableExtensions
		 */
		ArrayList<IEngineerTableExtension> iEngineerTableExtensions = new ArrayList<IEngineerTableExtension>();
		registerDefault_IEngineerTableExtension(iEngineerTableExtensions);

		for (IEngineerTableExtension ext : iEngineerTableExtensions)
		{
			for (EngineerTableIngredientHelper ingredient : ext.registerIngredients())
			{
				ModRecipeRegistry.registerEngineerTableIngredient(ingredient);
			}

			for (EngineerTableRecipeHelper recipe : ext.registerRecipes())
			{
				if (recipe.type == EEngineerTableTab.TRACKS)
				{
					ModRecipeRegistry.registerTrackRecipe(recipe.result, recipe.dir, recipe.matrices, recipe.indices);
				}
			}
		}
	}


	protected void registerDefault_IEngineerTableExtension(ArrayList<IEngineerTableExtension> iEngineerTableExtensions)
	{
		iEngineerTableExtensions.add(new TrackPackRecipes());
	}


	protected void registerDefault_IRailwaySection(ArrayList<IRailwaySection> iRailwaySections)
	{
		iRailwaySections.add(new ZnD_Cross_0000_0000());
		iRailwaySections.add(new ZnD_Cross_0000_1131());
		iRailwaySections.add(new ZnD_Cross_4500_4500());
		iRailwaySections.add(new ZnD_Cross10_1131_1131());
		iRailwaySections.add(new ZnD_Cross5_1131_1131());
		iRailwaySections.add(new ZnD_Curve_0000_1131());
		iRailwaySections.add(new ZnD_Curve_1131_1843());
		iRailwaySections.add(new ZnD_Curve_1843_4500());
		iRailwaySections.add(new ZnD_Curve10_1843_4500());
		iRailwaySections.add(new ZnD_SCurve_0000_0000());
		iRailwaySections.add(new ZnD_Straight_0000_0000());
		iRailwaySections.add(new ZnD_Straight_1131_1131());
		iRailwaySections.add(new ZnD_Straight_1843_1843());
		iRailwaySections.add(new ZnD_Straight_4500_4500());
		iRailwaySections.add(new ZnD_Straight2_4500_4500());
		iRailwaySections.add(new ZnD_Straight4_4500_4500());
		iRailwaySections.add(new ZnD_Straight8_4500_4500());
		iRailwaySections.add(new ZnD_Straight2_0000_0000());
		iRailwaySections.add(new ZnD_Straight4_0000_0000());
		iRailwaySections.add(new ZnD_Straight8_0000_0000());
		iRailwaySections.add(new ZnD_StraightMaint_0000_0000());
		iRailwaySections.add(new ZnD_StraightBuffer_0000_0000());
		iRailwaySections.add(new ZnD_StraightBuffer_1131_1131());
		iRailwaySections.add(new ZnD_StraightBuffer_1843_1843());
		iRailwaySections.add(new ZnD_StraightBuffer_4500_4500());
		iRailwaySections.add(new ZnD_StraightSlope_0000_0000());
		iRailwaySections.add(new ZnD_StraightSlope16_0000_0000());
		iRailwaySections.add(new ZnD_StraightTrans_0000_0000());
		iRailwaySections.add(new ZnD_Switch_0000_1131());
		iRailwaySections.add(new ZnD_Switch_1131_0000());
	}

	protected void registerDefault_ISignal(ArrayList<ISignal> iSignals)
	{
		iSignals.add(new SignalZD_Block());
		iSignals.add(new SignalZD_Distant());
		iSignals.add(new SignalZD_Dwarf());
		iSignals.add(new SignalZD_Flag());
		iSignals.add(new SignalZD_Tram());
	}

	protected void registerDefault_ISpeedSign(ArrayList<ISpeedSign> iSpeedSigns)
	{
		iSpeedSigns.add(new SpeedSignZD());
	}

	protected void registerDefault_IVehiclePart(OrderedPartsList parts)
	{
		parts.add(new VehParAxisBR101());
		parts.add(new VehParAxisDBpza());
		parts.add(new VehParAxisFLIRT());
		parts.add(new VehParAxisFLIRTTrailer());
		parts.add(new VehParAxisFreight());
		parts.add(new VehParAxisY25());
		parts.add(new VehParBaseplateBR101());
		parts.add(new VehParBaseplateCabBR101());
		parts.add(new VehParBaseplateFrameSWF());
		parts.add(new VehParBaseplateModFreightEnd());
		parts.add(new VehParBaseplateModFreightFlat());
		parts.add(new VehParBaseplateModFreightMid());
		parts.add(new VehParBaseplateModFreightTrans());
		parts.add(new VehParBaseplateEndSWF());
		parts.add(new VehParBaseplateSWF());
		parts.add(new VehParBasicCube());
		parts.add(new VehParBasicText());
		parts.add(new VehParBasicTriangle());
		parts.add(new VehParBufferBaseDBpza());
		parts.add(new VehParBufferBR101());
		parts.add(new VehParBufferDBpza());
		parts.add(new VehParBufferRound());
		parts.add(new VehParCab());
		parts.add(new VehParChassisBottomDBpza());
		parts.add(new VehParChassisBR101());
		parts.add(new VehParChassisCabBR101());
		parts.add(new VehParChassisCabDBpza());
		parts.add(new VehParChassisCabFLIRT());
		parts.add(new VehParChassisDoorsDBpza());
		parts.add(new VehParChassisDoorsFLIRT());
		parts.add(new VehParChassisEndFLIRT());
		parts.add(new VehParChassisEndSWF());
		parts.add(new VehParChassisFillerFLIRT());
		parts.add(new VehParChassisFLIRT());
		parts.add(new VehParChassisRoofSWF());
		parts.add(new VehParChassisStairsDBpza());
		parts.add(new VehParChassisTopDBpza());
		parts.add(new VehParChassisVestibuleDBpza());
		parts.add(new VehParChassisVestibuleRoofDBpza());
		parts.add(new VehParChassisWallSWF());
		parts.add(new VehParCompressor());
		parts.add(new VehParCouplerJanney());
		parts.add(new VehParCouplerScrew());
		parts.add(new VehParCouplerSfbg());
		parts.add(new VehParDestBoard());
		parts.add(new VehParDetailSWF());
		parts.add(new VehParElectricsFLIRT());
		parts.add(new VehParEngineDieselF7());
		parts.add(new VehParEngineElectricBR101());
		parts.add(new VehParFenderBR101());
		parts.add(new VehParFenderFLIRT());
		parts.add(new VehParIntercirculationDBpza());
		parts.add(new VehParLamp());
		parts.add(new VehParOilTanksBR101());
		parts.add(new VehParPantograph());
		parts.add(new VehParPloughBR101());
		parts.add(new VehParPloughFLIRT());
		parts.add(new VehParSeat());
		parts.add(new VehParStairsDBpza());
		parts.add(new VehParTransportContainer());
		parts.add(new VehParInventory());
		parts.add(new VehParTransportHopper());
		parts.add(new VehParTransportOpen());
		parts.add(new VehParTransportTankContainer());
		parts.add(new VehParTransportWood());
	}


	public void registerEventHandlers()
	{
		MinecraftForge.EVENT_BUS.register(new EventHandler());
		FMLCommonHandler.instance().bus().register(new EventHandler());
	}


	public void registerRenderers()
	{
		;
	}
}
