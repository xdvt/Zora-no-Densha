package zoranodensha.common.core;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;



/**
 * This class stores all {@link net.minecraft.creativetab.CreativeTabs creative tabs} implemented by Zora no Densha.
 */
public abstract class ModCreativeTabs
{
	@SideOnly(Side.CLIENT) public static ItemStack modTabGenStack;


	/**
	 * Generic creative tab for pretty much anything else, especially items and blocks.
	 */
	public static CreativeTabs generic = new CreativeTabs(ModData.ID + ".tabs.generic")
	{
		@Override
		public Item getTabIconItem()
		{
			return ModCenter.ItemRailwayTool;
		}
	};

	/**
	 * All {@link zoranodensha.common.presets.APreset presets} will be displayed here.
	 */
	public static CreativeTabs presets = new CreativeTabs(ModData.ID + ".tabs.presets")
	{
		@Override
		@SideOnly(Side.CLIENT)
		public ItemStack getIconItemStack()
		{
			return (modTabGenStack != null) ? modTabGenStack : super.getIconItemStack();
		}

		@Override
		public Item getTabIconItem()
		{
			return ModCenter.ItemRailwayTool;
		}
	};

//	/**
//	 * All {@link zoranodensha.api.structures.signals.ISignal signals} will be displayed here.
//	 */
//	public static CreativeTabs signals = new CreativeTabs(ModData.ID + ".tabs.signals")
//	{
//		@Override
//		@SideOnly(Side.CLIENT)
//		public Item getTabIconItem()
//		{
//			return Item.getItemFromBlock(ModCenter.BlockSignal);
//		}
//	};

	/**
	 * All {@link zoranodensha.api.vehicles.vehiclePart.IVehiclePart vehicle parts} will be displayed here.
	 */
	public static CreativeTabs vehicleParts = new CreativeTabs(ModData.ID + ".tabs.vehicleParts")
	{
		@Override
		@SideOnly(Side.CLIENT)
		public Item getTabIconItem()
		{
			return Item.getItemFromBlock(ModCenter.BlockEngineerTable);
		}
	};
}
