package zoranodensha.common.core.cfg;

public class Ctgy_WorldGen extends AConfigCategory
{
	public boolean enableOreSpawn;
	public int veinSize;

	public Ctgy_WorldGen()
	{
		super("worldgen");
	}

	@Override
	protected void loadCategory()
	{
		enableOreSpawn = getBoolean("oregen", true);
		veinSize = getInt("vein_size", 16, 1, 128);
	}
}