package zoranodensha.common.core.cfg;

import java.util.Calendar;



public class Ctgy_EasterEggs extends AConfigCategory
{
	public boolean enableChristmas;
	public boolean enableEaster;
	public boolean enableHalloween;
	public boolean enableNewYear;



	public Ctgy_EasterEggs()
	{
		super("easter_eggs");
	}

	@Override
	protected void loadCategory()
	{
		Calendar cal = Calendar.getInstance();
		final int month = cal.get(Calendar.MONTH);
		final int day = cal.get(Calendar.DAY_OF_MONTH);

		/* Enable Christmas content all throughout December. */
		enableChristmas = getBoolean("christmas", true) && (month == Calendar.DECEMBER);

		/* Enable Easter content during second half of April. */
		enableEaster = getBoolean("easter", true) && (month == Calendar.APRIL && day >= 15);

		/* Enable Halloween content during second half of October. */
		enableHalloween = getBoolean("halloween", true) && (month == Calendar.OCTOBER && day >= 15);

		/* Enable New Year's content on 31st of December and 1st of January. */
		enableNewYear = getBoolean("new_year", true) && ((month == Calendar.DECEMBER && day == 31) || (month == Calendar.JANUARY && day == 1));
	}
}