package zoranodensha.common.core.cfg;



public class Ctgy_Trains extends AConfigCategory
{
	public int generalSpeedLimit;
	public boolean enableBlueprintSpawnAnyTrack;
	public boolean enableFreightKeepInventory;
	public float velocityScale;



	public Ctgy_Trains()
	{
		super("trains");
	}

	@Override
	protected void loadCategory()
	{
		generalSpeedLimit = getInt("limit", 0, 0, 1000);
		enableBlueprintSpawnAnyTrack = getBoolean("blueprints_on_any_track", false);
		enableFreightKeepInventory = getBoolean("freight_keep_inventory", false);
		velocityScale = getFloat("velocity_scale", 0.5F, 0.1F, 1.0F);
	}
}