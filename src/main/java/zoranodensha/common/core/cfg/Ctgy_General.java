package zoranodensha.common.core.cfg;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.launchwrapper.Launch;



public class Ctgy_General extends AConfigCategory
{
	public boolean isRemoteEnv;
	public boolean isDeobfEnv;



	public Ctgy_General()
	{
		super("");
	}

	@Override
	public void load()
	{
		loadCategory();
	}

	@Override
	protected void loadCategory()
	{
		isRemoteEnv = Side.CLIENT.equals(FMLCommonHandler.instance().getEffectiveSide());
		isDeobfEnv = Boolean.TRUE.equals(Launch.blackboard.get("fml.deobfuscatedEnvironment"));
	}
}