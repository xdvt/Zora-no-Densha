package zoranodensha.common.core.cfg;

import cpw.mods.fml.common.Loader;



public class Ctgy_Compat extends AConfigCategory
{
	public boolean isLoaded_Railcraft;



	public Ctgy_Compat()
	{
		super("");
	}

	@Override
	public void load()
	{
		loadCategory();
	}

	@Override
	protected void loadCategory()
	{
		isLoaded_Railcraft = Loader.isModLoaded("Railcraft");
	}
}