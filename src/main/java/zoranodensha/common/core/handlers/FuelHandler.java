package zoranodensha.common.core.handlers;

import cpw.mods.fml.common.IFuelHandler;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import zoranodensha.common.core.ModCenter;



public class FuelHandler implements IFuelHandler
{
	@Override
	public int getBurnTime(ItemStack itemStack)
	{
		Item item = itemStack.getItem();
		if (item != null)
		{
			if (item == Item.getItemFromBlock(ModCenter.BlockCoalCoke))
			{
				return 32000;
			}
			else if (item == Item.getItemFromBlock(ModCenter.BlockEngineerTable))
			{
				return 300;
			}
			else if (item == ModCenter.ItemBucketFuelDiesel)
			{
				return 1800;
			}
			else if (item == ModCenter.ItemBucketFuelPetrol)
			{
				return 2000;
			}
			else if (item == ModCenter.ItemBucketOilCreosote)
			{
				return 600;
			}
			else if (item == ModCenter.ItemCoalCoke)
			{
				return 3200;
			}
			else if (item == Item.getItemFromBlock(ModCenter.BlockFluidFuelDiesel))
			{
				return 1800;
			}
			else if (item == Item.getItemFromBlock(ModCenter.BlockFluidFuelPetrol))
			{
				return 2000;
			}
			else if (item == Item.getItemFromBlock(ModCenter.BlockFluidOilCreosote))
			{
				return 600;
			}
		}
		return 0;
	}
}
