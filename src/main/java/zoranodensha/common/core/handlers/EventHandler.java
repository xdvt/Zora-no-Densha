package zoranodensha.common.core.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.client.event.ConfigChangedEvent.OnConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.ChunkPosition;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.event.world.ExplosionEvent;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.cfg.ModConfig;
import zoranodensha.common.util.TrackHelpers;



/**
 * Event handler class for all non-vehicle events.
 */
public class EventHandler extends VehicleEventHandler
{
	public static Map<Block, Item> fluidBucketList = new HashMap<Block, Item>();
	static
	{
		fluidBucketList.put(ModCenter.BlockFluidFuelDiesel, ModCenter.ItemBucketFuelDiesel);
		fluidBucketList.put(ModCenter.BlockFluidFuelPetrol, ModCenter.ItemBucketFuelPetrol);
		fluidBucketList.put(ModCenter.BlockFluidOilBioCrude, ModCenter.ItemBucketOilBioCrude);
		fluidBucketList.put(ModCenter.BlockFluidOilCreosote, ModCenter.ItemBucketOilCreosote);
		fluidBucketList.put(ModCenter.BlockFluidOilLubricant, ModCenter.ItemBucketOilLubricant);
		fluidBucketList.put(ModCenter.BlockFluidOilMachinery, ModCenter.ItemBucketOilMachinery);
		fluidBucketList.put(ModCenter.BlockFluidOilShaleCrude, ModCenter.ItemBucketOilShaleCrude);
		fluidBucketList.put(ModCenter.BlockFluidTar, ModCenter.ItemBucketTar);
	}



	@SubscribeEvent
	public void bucketFillEvent(FillBucketEvent event)
	{
		int x = event.target.blockX;
		int y = event.target.blockY;
		int z = event.target.blockZ;

		Item item = fluidBucketList.get(event.world.getBlock(x, y, z));
		if (item != null && event.world.getBlockMetadata(x, y, z) == 0)
		{
			event.world.setBlockToAir(x, y, z);
			event.result = new ItemStack(item);
			event.setResult(Result.ALLOW);
		}
	}

	@SubscribeEvent
	public void configChangedEvent(OnConfigChangedEvent event)
	{
		if (ModData.ID.equals(event.modID) && ModConfig.config.hasChanged())
		{
			ModCenter.cfg.init();
		}
	}

	@SubscribeEvent
	public void entityJoinWorldEvent(EntityJoinWorldEvent event)
	{
		if (event.entity instanceof EntityItem)
		{
			EntityItem entity = (EntityItem)event.entity;
			if (entity.getEntityItem() != null)
			{
				/*
				 * This fixes an issue where corner blocks of ballast gravel
				 * could be spawned as item through falling blocks. 
				 */
				ItemStack itemStack = entity.getEntityItem();
				if (itemStack.getItem() == Item.getItemFromBlock(ModCenter.BlockBallastGravelCorner))
				{
					ItemStack newItem = new ItemStack(ModCenter.BlockBallastGravel, itemStack.stackSize, itemStack.getItemDamage());
					entity.setEntityItemStack(newItem);
				}
			}
		}
	}

	@SubscribeEvent
	public void explosionEvent(ExplosionEvent.Detonate event)
	{
		if (!ModCenter.cfg.blocks_items.enableTrackExplosion)
		{
			List<ChunkPosition> affectedBlocks = event.getAffectedBlocks();
			Iterator<ChunkPosition> itera = affectedBlocks.iterator();

			List<ChunkPosition> removedBlocks = new ArrayList<ChunkPosition>();

			while (itera.hasNext())
			{
				ChunkPosition cp = itera.next();
				Block block = event.world.getBlock(cp.chunkPosX, cp.chunkPosY, cp.chunkPosZ);

				if (block == ModCenter.BlockTrackBase || block == ModCenter.BlockTrackBaseGag)
				{
					boolean flag = true;

					for (int i1 = 0; flag; --i1)
					{
						if (TrackHelpers.canBeSustained(event.world, cp.chunkPosX, cp.chunkPosY + i1, cp.chunkPosZ))
						{
							flag = false;
							removedBlocks.add(new ChunkPosition(cp.chunkPosX, cp.chunkPosY + i1 - 1, cp.chunkPosZ));
						}

						removedBlocks.add(new ChunkPosition(cp.chunkPosX, cp.chunkPosY + i1, cp.chunkPosZ));
					}
				}
			}

			itera = affectedBlocks.iterator();

			while (itera.hasNext())
			{
				ChunkPosition cp = itera.next();

				if (removedBlocks.contains(cp))
				{
					itera.remove();
				}
			}
		}
	}
}
