package zoranodensha.common.core.handlers;

/**
 * Identifiers for GUIs implemented by Zora no Densha.
 * 
 * @author Leshuwa Kaiheiwa
 */
public enum EGui
{
	BLAST_FURNACE,
	COKE_OVEN,
	ENGINEER_TABLE,
	ENGINEER_TABLE_TRAINS,
	RAILWAY_TOOL,
	REFINERY,
	RETORTER,
	SIGNAL,
	SPEED_SIGN,
	VEHPAR_TRANSPORT_ITEM,
	VEHPAR_TRANSPORT_TANK;
	
	
	/**
	 * Parses given integer for a matching GUI type.
	 * 
	 * @return Matching GUI type, or {@code null} if unsuccessful.
	 */
	public static EGui fromInt(int i)
	{
		for (EGui value : values())
		{
			if (value.ordinal() == i)
			{
				return value;
			}
		}
		return null;
	}
}