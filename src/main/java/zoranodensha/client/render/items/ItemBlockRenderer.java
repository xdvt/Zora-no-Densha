package zoranodensha.client.render.items;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;



@SideOnly(Side.CLIENT)
public class ItemBlockRenderer implements IItemRenderer
{
	private final TileEntitySpecialRenderer tileEntitySpecialRenderer;



	public ItemBlockRenderer(TileEntitySpecialRenderer tesr)
	{
		tileEntitySpecialRenderer = tesr;
	}

	@Override
	public boolean handleRenderType(ItemStack itemStack, ItemRenderType type)
	{
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data)
	{
		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
		}

		GL11.glDisable(GL11.GL_LIGHTING);
		tileEntitySpecialRenderer.renderTileEntityAt(null, 0.0D, 0.0D, 0.0D, itemStack.getItemDamage());
		GL11.glEnable(GL11.GL_LIGHTING);
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack itemStack, ItemRendererHelper helper)
	{
		return true;
	}
}
