package zoranodensha.client.render.items;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.AVehiclePartRegistry;
import zoranodensha.api.vehicles.rendering.TrainRenderer;



@SideOnly(Side.CLIENT)
public class ItemPartRenderer implements IItemRenderer
{
	@Override
	public boolean handleRenderType(ItemStack itemStack, ItemRenderType type)
	{
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data)
	{
		VehParBase part = AVehiclePartRegistry.getPartFromItemStack(itemStack);
		if (part != null)
		{
			TrainRenderer.renderPass = 0;
			part.getRenderer().renderPartItem(type);
		}
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack itemStack, ItemRendererHelper helper)
	{
		return true;
	}
}