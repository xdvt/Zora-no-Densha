package zoranodensha.client.render.items;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.common.core.registry.ModTrackRegistry;



public class ItemBlueprintRenderer implements IItemRenderer
{
	public ItemBlueprintRenderer()
	{
	}

	@Override
	public boolean handleRenderType(ItemStack itemStack, ItemRenderType type)
	{
		return type.equals(ItemRenderType.INVENTORY);
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data)
	{
		if (itemStack == null)
		{
			return;
		}

		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		RenderItem.getInstance().renderIcon(0, 0, itemStack.getIconIndex(), 16, 16);
		GL11.glDisable(GL11.GL_ALPHA_TEST);

		if (itemStack.hasTagCompound())
		{
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;

			NBTTagCompound nbt = itemStack.getTagCompound();
			if (nbt.hasKey("Instance"))
			{
				IRailwaySection section = ModTrackRegistry.getSection(nbt.getString("Instance"));
				if (section != null)
				{
					String label = section.getEShapeType().toLabel();
					int offset = 0;
					
					switch (EDirection.fromString(nbt.getString("Flag")))
					{
						case NONE:
							offset = 8 - fontRenderer.getStringWidth(label) / 2;
							break;
						
						case LEFT:
							offset = 1;
							break;
							
						case RIGHT:
							offset = 16 - fontRenderer.getStringWidth(label);
							break;
					}
					
					fontRenderer.drawStringWithShadow(label, offset, 8, 0xFFFFFF);
				}
			}
		}

		GL11.glEnable(GL11.GL_LIGHTING);
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack itemStack, ItemRendererHelper helper)
	{
		return false;
	}
}
