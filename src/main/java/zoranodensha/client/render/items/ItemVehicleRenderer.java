package zoranodensha.client.render.items;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.client.render.entity.EntityRendererVehicleModel;
import zoranodensha.common.items.ItemTrain;



@SideOnly(Side.CLIENT)
public class ItemVehicleRenderer implements IItemRenderer
{
	private final EntityRendererVehicleModel renderVehicleModel = new EntityRendererVehicleModel();



	@Override
	public boolean handleRenderType(ItemStack itemStack, ItemRenderType type)
	{
		return !VehicleHelper.getIsBlueprint(itemStack);
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data)
	{
		GL11.glPushAttrib(GL12.GL_RESCALE_NORMAL);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		renderVehicleModel.renderTrainItem(((ItemTrain)itemStack.getItem()).getVehicle(itemStack.getTagCompound()), type);
		GL11.glPopAttrib();
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack itemStack, ItemRendererHelper helper)
	{
		return true;
	}
}
