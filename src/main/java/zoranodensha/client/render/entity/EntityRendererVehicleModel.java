package zoranodensha.client.render.entity;

import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.client.gui.table.model.TrainModelRender;
import zoranodensha.client.gui.table.model.VehicleModel;



@SideOnly(Side.CLIENT)
public class EntityRendererVehicleModel extends TrainRenderer
{
	public static boolean ignorePartState;



	@Override
	public void doRender(Entity entity, double x, double y, double z, float f, float partialTick)
	{
		if (entity instanceof VehicleModel)
		{
			doRender2((VehicleModel)entity, (float)x, (float)y, (float)z);
		}
	}

	/**
	 * Render a {@link zoranodensha.api.vehicles.Train train} as item.
	 */
	public void renderTrainItem(VehicleModel model, IItemRenderer.ItemRenderType type)
	{
		if (model == null)
		{
			return;
		}

		GL11.glPushMatrix();

		Vector3f vecCenter = VehicleModel.getCenter(model.getVehicleParts());
		float scale = 0.75F;

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
		}
		else
		{
			Vector3f vec = getOutmostPoint(model);
			vec = new Vector3f(Math.abs(vecCenter.x) - Math.abs(vec.x), Math.abs(vecCenter.y) - Math.abs(vec.y), Math.abs(vecCenter.z) - Math.abs(vec.z));

			scale /= vec.length();
			if (scale > 0.75F)
			{
				scale = 0.75F;
			}
		}

		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		GL11.glScalef(scale, scale, scale);
		doRender2(model, -vecCenter.x, -vecCenter.y, -vecCenter.z);
		GL11.glPopMatrix();
	}

	/**
	 * Helper method to render a vehicle model in two render passes.
	 */
	private static void doRender2(VehicleModel model, float x, float y, float z)
	{
		final int prevPass = TrainRenderer.renderPass;
		TrainRenderer.partialTick = 0.0F;

		for (int pass = 0; pass <= 1; ++pass)
		{
			/* Prepare OpenGL settings depending on render pass. */
			TrainModelRender.pushRenderPass(pass);

			/* Set render pass and render model. */
			TrainRenderer.renderPass = pass;
			renderVehicleModel(model, x, y, z);
		}

		/* Undo changes to OpenGL. */
		GL11.glDepthMask(true);
		TrainRenderer.renderPass = prevPass;
	}

	/**
	 * Return the out most point of the given train as a {@link Vec3}.
	 */
	@SideOnly(Side.CLIENT)
	public static Vector3f getOutmostPoint(VehicleModel model)
	{
		AxisAlignedBB aabb;
		double posX = 0.0D;
		double posY = 0.0D;
		double posZ = 0.0D;
		double dX;
		double dY;
		double dZ;

		for (VehParBase part : model.getVehicleParts())
		{
			aabb = part.getLocalBoundingBox();

			if ((dX = Math.max(Math.abs(aabb.maxX), Math.abs(aabb.minX))) > posX)
			{
				posX = dX;
			}

			if ((dY = Math.max(Math.abs(aabb.maxY), Math.abs(aabb.minY))) > posY)
			{
				posY = dY;
			}

			if ((dZ = Math.max(Math.abs(aabb.maxZ), Math.abs(aabb.minZ))) > posZ)
			{
				posZ = dZ;
			}
		}

		return new Vector3f((float)posX, (float)posY, (float)posZ);
	}

	/**
	 * Render the bounding boxes of all parts in the given list with given color.
	 *
	 * @param parts - {@link java.util.List List} containing all parts whose bounding boxes shall be rendered.
	 * @param col - Color of the rendered bounding boxes, in hexadecimal (HEX) format.
	 */
	public static final void renderBoundingBoxes(List<VehParBase> parts, int col)
	{
		GL11.glDepthMask(false);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);

		for (VehParBase part : parts)
		{
			RenderGlobal.drawOutlinedBoundingBox(part.getBoundingBox(), col);
		}

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDepthMask(true);
	}

	private static void renderVehicleModel(VehicleModel model, float x, float y, float z)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, z);
		{
			/* First render all unselected parts. */
			for (VehParBase part : model.getVehicleParts())
			{
				renderPart(part, false);
			}

			/*
			 * If we ignore part states, render a model with selection and transformation lists.
			 */
			if (ignorePartState)
			{
				/* Then render all selected parts. */
				for (VehParBase part : model.selection)
				{
					part.getBoundingBox().setBB(part.getLocalBoundingBox());
					renderPart(part, false);
				}
				renderBoundingBoxes(model.selection, 0x0000FF);

				/* Finally render, if existent, the current object transformation. */
				if (model.objTrans != null && model.objTrans.render())
				{
					for (VehParBase part : model.objTrans.parts)
					{
						part.getBoundingBox().setBB(part.getLocalBoundingBox());
						renderPart(part, false);
					}
					renderBoundingBoxes(model.objTrans.parts, 0xFF8C00);
				}
			}
		}
		GL11.glPopMatrix();
	}
}
