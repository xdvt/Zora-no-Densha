package zoranodensha.client.render.entity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.structures.tracks.IRailwaySectionRender;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.entity.EntityTrackOutline;



@SideOnly(Side.CLIENT)
public class EntityRendererTrackOutline extends Render
{
	@Override
	public void doRender(Entity entity, double x, double y, double z, float f0, float f1)
	{
		if (entity instanceof EntityTrackOutline)
		{
			if (((EntityTrackOutline)entity).trackBase != null)
			{
				doRender0((EntityTrackOutline)entity, (float)x, (float)y, (float)z, f0);
			}
			else
			{
				entity.setDead();
			}
		}
	}

	private void doRender0(EntityTrackOutline entity, float x, float y, float z, float f)
	{
		ITrackBase trackBase = entity.trackBase;
		IRailwaySectionRender tesr = trackBase.getInstanceOfShape().getRender();

		if (tesr != null)
		{
			GL11.glPushMatrix();
			tesr.renderTrack(ModCenter.cfg.rendering.detailTracks, (entity.isClear ? 1 : 2), trackBase.getInstanceOfShape(), trackBase, x, y, z, f);
			GL11.glPopMatrix();
		}
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return null;
	}
}
