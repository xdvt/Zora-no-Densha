package zoranodensha.client.render.entity;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import zoranodensha.common.core.ModData;
import zoranodensha.common.entity.SCP4633;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_Lamp;



public class EntityRendererSCP extends Render
{
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/entity/scp.png");

	private float playerYaw;
	private int ticks;



	@Override
	public void doRender(Entity entity, double x, double y, double z, float something, float partialTick)
	{
		ticks++;
		if (ticks > 10)
		{
			ticks = 0;

			playerYaw = entity.worldObj.getClosestPlayerToEntity(entity, 128.0D).rotationYawHead;
		}

		if (entity instanceof SCP4633)
		{
			/*
			 * Render
			 */
			SCP4633 entitySCP = (SCP4633)entity;
			GL11.glColor4f(1.0F, 1.0F, 1.0F, entitySCP.getAlpha());

			GL11.glPushMatrix();
			{
				GL11.glTranslated(x + (entity.motionX * partialTick), y + 0.5D, z + (entity.motionZ * partialTick));
				GL11.glRotatef(-90.0F + playerYaw, 0.0F, -1.0F, 0.0F);

				RenderUtil.lightmapPush();
				{
					RenderUtil.lightmapBright();
					GL11.glEnable(GL11.GL_BLEND);

					Minecraft.getMinecraft().renderEngine.bindTexture(texture);
					VehParRenderer_Lamp.model_flare.obj.renderAll();

					GL11.glDisable(GL11.GL_BLEND);
				}
				RenderUtil.lightmapPop();
			}
			GL11.glPopMatrix();

		}
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return null;
	}

}
