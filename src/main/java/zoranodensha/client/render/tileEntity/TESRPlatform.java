package zoranodensha.client.render.tileEntity;

import javax.annotation.Nullable;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import zoranodensha.common.blocks.tileEntity.TileEntityPlatform;
import zoranodensha.common.blocks.tileEntity.TileEntityPlatform.APlatformType;



@SideOnly(Side.CLIENT)
public class TESRPlatform extends TileEntitySpecialRenderer
{
	/** Default texture's block item. */
	private static final ItemStack defaultTexture = new ItemStack(Blocks.stone);



	/**
	 * Helper method to retrieve an icon texture from the given platform.
	 */
	public static IIcon getBlockTexture(@Nullable TileEntityPlatform tile)
	{
		ItemStack blockTexture = null;

		if (tile != null)
		{
			blockTexture = tile.getBlockTexture();
		}

		if (blockTexture == null)
		{
			blockTexture = defaultTexture;
		}

		return Block.getBlockFromItem(blockTexture.getItem()).getIcon(0, blockTexture.getItemDamage());
	}

	/**
	 * Helper method to determine model rotation from the given tile entity.
	 * 
	 * @return Model yaw rotation, in degrees.
	 */
	private static float getRotationFromTile(@Nullable TileEntity tile)
	{
		if (tile != null)
		{
			int meta = tile.getBlockMetadata() + 1;
			return (meta % 2 == 0) ? meta * 90 : meta * 90 + 180;
		}
		return 0.0F;
	}

	/**
	 * Determines the offset of how far the platform block extends outwards, on local negative Z (left side).
	 */
	private static float getOffsetNegZ(@Nullable TileEntityPlatform tile)
	{
		return (tile != null) ? tile.getOffsetNegZ() : 0.0F;
	}

	/**
	 * Determines the offset of how far the platform block extends outwards, on local positive Z (right side).
	 */
	private static float getOffsetPosZ(@Nullable TileEntityPlatform tile)
	{
		return (tile != null) ? tile.getOffsetPosZ() : 0.0F;
	}

	@Override
	public void renderTileEntityAt(TileEntity tile, double x, double y, double z, float scale)
	{
		APlatformType typePlatform = APlatformType.fromID((int)scale);

		GL11.glPushMatrix();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glTranslated(x + 0.5D, y, z + 0.5D);
		GL11.glRotatef(getRotationFromTile(tile), 0, 1, 0);
		{
			if (tile == null)
			{
				GL11.glEnable(GL11.GL_LIGHTING);
				GL11.glTranslatef(0.125F, -0.125F, 0.0F);
			}

			TileEntityPlatform tilePlatform = null;
			if (tile instanceof TileEntityPlatform)
			{
				tilePlatform = (TileEntityPlatform)tile;
				tilePlatform.recalculateSides();
				typePlatform = tilePlatform.getType();
			}

			Minecraft.getMinecraft().renderEngine.bindTexture(TextureMap.locationBlocksTexture);
			Tessellator tessellator = Tessellator.instance;
			tessellator.startDrawingQuads();

			if (tilePlatform != null)
			{
				TesselatorVertexState vertexState = tilePlatform.getVertexState();
				if (vertexState == null)
				{
					// FIXME PLATFORMS - Reactivate this once dynamic platform faces render correctly.
					// typePlatform.render(getBlockTexture(tilePlatform), getOffsetNegZ(tilePlatform), getOffsetPosZ(tilePlatform), tilePlatform);

					tessellator.setTranslation(0.0, 0.0, 0.0);
					typePlatform.render(getBlockTexture(tilePlatform), getOffsetNegZ(null), getOffsetPosZ(null), tilePlatform);
					tilePlatform.setVertexState(tessellator.getVertexState(0, 0, 0));
				}
				else
				{
					tessellator.setVertexState(vertexState);
				}
			}
			else
			{
				typePlatform.render(getBlockTexture(null), 0, 0, null);
			}

			tessellator.draw();
		}
		GL11.glPopMatrix();
	}
}