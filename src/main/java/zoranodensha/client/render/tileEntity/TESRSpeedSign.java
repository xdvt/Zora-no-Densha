package zoranodensha.client.render.tileEntity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.signals.ISpeedSign;
import zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign;



@SideOnly(Side.CLIENT)
public class TESRSpeedSign extends ABackfallTESR
{

	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float partialTick)
	{
		if (tileEntity instanceof TileEntitySpeedSign)
		{
			ISpeedSign speedSign = ((TileEntitySpeedSign)tileEntity).getSpeedSign();

			if (speedSign != null)
			{
				GL11.glPushMatrix();
				{
					/*
					 * Position
					 */
					GL11.glTranslated(x + 0.5D, y, z + 0.5D);

					/*
					 * Rotation
					 */
					int meta = tileEntity.getBlockMetadata();
					GL11.glRotatef(270.0F + (meta * -22.5F), 0.0F, 1.0F, 0.0F);

					/*
					 * Render the speed sign
					 */
					speedSign.render(partialTick);
				}
				GL11.glPopMatrix();
			}
			else
			{
				renderQuestionMark(x, y, z);
			}
		}
		else
		{
			renderQuestionMark(x, y, z);
		}
	}

}
