package zoranodensha.client.render.tileEntity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackDecoration;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class TESRTrackDecoration extends TileEntitySpecialRenderer
{
	private static final ResourceLocation[] textures =
			new ResourceLocation[] { new ResourceLocation(ModData.ID, "textures/tracks/Track_straight_wood.png"), new ResourceLocation(ModData.ID, "textures/tracks/Track_straight_concrete.png") };

	private static final IModelCustom[] models =
			new IModelCustom[] { AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, ModCenter.DIR_RESOURCES_TRACKS + "ModelTrack_decoration_pileRails.obj")),
					AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, ModCenter.DIR_RESOURCES_TRACKS + "ModelTrack_decoration_pileTies.obj")) };



	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float f)
	{
		int meta = tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord);

		if (tileEntity instanceof TileEntityTrackDecoration)
		{
			IModelCustom model = models[meta / 4 < 2 ? 1 : 0];
			bindTexture(textures[(meta / 4) == 0 ? 0 : 1]);

			GL11.glPushMatrix();
			GL11.glTranslated(x + 0.5D, y, z + 0.5D);
			GL11.glRotatef((meta % 4) * 90.0F, 0.0F, 1.0F, 0.0F);

			for (int i = 0; i <= ((TileEntityTrackDecoration)tileEntity).getPileSize(); ++i)
			{
				GL11.glPushMatrix();
				model.renderPart("tie" + i);
				GL11.glPopMatrix();
			}

			GL11.glPopMatrix();
		}
	}
}
