package zoranodensha.client.render.tileEntity.models;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;



@SideOnly(Side.CLIENT)
public class ModelPlatformCornerInner extends ModelBase
{
	ModelRenderer Base0;
	ModelRenderer Base1;
	ModelRenderer FrontPanel0A;
	ModelRenderer FrontPanel1A;
	ModelRenderer FrontPanel0B;
	ModelRenderer FrontPanel1B;



	public ModelPlatformCornerInner()
	{
		textureWidth = 64;
		textureHeight = 64;

		Base0 = new ModelRenderer(this, 0, 0);
		Base0.addBox(-8F, -4F, 0F, 16, 12, 8);
		Base0.setRotationPoint(0F, 0F, 0F);
		Base0.setTextureSize(64, 64);
		Base0.mirror = true;
		setRotation(Base0, 0F, 0F, 0F);
		Base1 = new ModelRenderer(this, 20, 20);
		Base1.addBox(0F, -4F, -8F, 8, 12, 8);
		Base1.setRotationPoint(0F, 0F, 0F);
		Base1.setTextureSize(64, 64);
		Base1.mirror = true;
		setRotation(Base1, 0F, 0F, 0F);
		FrontPanel0A = new ModelRenderer(this, 0, 20);
		FrontPanel0A.addBox(-7F, 3F, -2F, 7, 5, 2);
		FrontPanel0A.setRotationPoint(0F, 0F, 0F);
		FrontPanel0A.setTextureSize(64, 64);
		FrontPanel0A.mirror = true;
		setRotation(FrontPanel0A, 0F, 0F, 0F);
		FrontPanel1A = new ModelRenderer(this, 0, 27);
		FrontPanel1A.addBox(-2F, 3F, -7F, 2, 5, 5);
		FrontPanel1A.setRotationPoint(0F, 0F, 0F);
		FrontPanel1A.setTextureSize(64, 64);
		FrontPanel1A.mirror = true;
		setRotation(FrontPanel1A, 0F, 0F, 0F);
		FrontPanel0B = new ModelRenderer(this, 0, 37);
		FrontPanel0B.addBox(-8F, -4F, -2F, 8, 2, 2);
		FrontPanel0B.setRotationPoint(0F, 0F, 0F);
		FrontPanel0B.setTextureSize(64, 64);
		FrontPanel0B.mirror = true;
		setRotation(FrontPanel0B, 0F, 0F, 0F);
		FrontPanel1B = new ModelRenderer(this, 0, 41);
		FrontPanel1B.addBox(-2F, -4F, -8F, 2, 2, 6);
		FrontPanel1B.setRotationPoint(0F, 0F, 0F);
		FrontPanel1B.setTextureSize(64, 64);
		FrontPanel1B.mirror = true;
		setRotation(FrontPanel1B, 0F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);

		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		Base0.render(f5);
		Base1.render(f5);
		FrontPanel0A.render(f5);
		FrontPanel1A.render(f5);
		FrontPanel0B.render(f5);
		FrontPanel1B.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}
}
