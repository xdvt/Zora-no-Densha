package zoranodensha.client.render.tileEntity.models;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;



@SideOnly(Side.CLIENT)
public class ModelPlatformEnd extends ModelBase
{
	ModelRenderer Base;
	ModelRenderer FrontPanel1;
	ModelRenderer FrontPanel2;



	public ModelPlatformEnd()
	{
		textureWidth = 64;
		textureHeight = 64;

		Base = new ModelRenderer(this, 0, 0);
		Base.addBox(-8F, -4F, 0F, 16, 12, 8);
		Base.setRotationPoint(0F, 0F, 0F);
		Base.setTextureSize(64, 64);
		Base.mirror = true;
		setRotation(Base, 0F, 0F, 0F);
		FrontPanel1 = new ModelRenderer(this, 0, 30);
		FrontPanel1.addBox(-7F, 3F, -2F, 14, 5, 2);
		FrontPanel1.setRotationPoint(0F, 0F, 0F);
		FrontPanel1.setTextureSize(64, 64);
		FrontPanel1.mirror = true;
		setRotation(FrontPanel1, 0F, 0F, 0F);
		FrontPanel2 = new ModelRenderer(this, 0, 45);
		FrontPanel2.addBox(-8F, -4F, -2F, 16, 2, 2);
		FrontPanel2.setRotationPoint(0F, 0F, 0F);
		FrontPanel2.setTextureSize(64, 64);
		FrontPanel2.mirror = true;
		setRotation(FrontPanel2, 0F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		Base.render(f5);
		FrontPanel1.render(f5);
		FrontPanel2.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

}
