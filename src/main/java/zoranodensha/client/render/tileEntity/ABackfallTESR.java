package zoranodensha.client.render.tileEntity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public abstract class ABackfallTESR extends TileEntitySpecialRenderer
{
	private static final IModelCustom modelMissing = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/blocks/missing.obj"));
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/blocks/missing.png");



	public void renderQuestionMark(double x, double y, double z)
	{
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);

		GL11.glPushMatrix();
		GL11.glTranslated(x + 0.5D, y, z + 0.5D);
		modelMissing.renderAll();
		GL11.glPopMatrix();
	}
}