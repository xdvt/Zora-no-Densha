package zoranodensha.client;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.common.gameevent.InputEvent.KeyInputEvent;
import cpw.mods.fml.common.gameevent.TickEvent.RenderTickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.event.GuiScreenEvent.ActionPerformedEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.packet.TrainMessage_RayTrace;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Activatable;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic;
import zoranodensha.api.vehicles.part.type.PartTypeSeat;
import zoranodensha.api.vehicles.part.type.seat.EntitySeat;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.client.entityFX.EParticles;
import zoranodensha.client.render.ICachedVertexState;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.handlers.EventHandler;
import zoranodensha.common.util.Helpers;
import zoranodensha.vehicleParts.client.CabKeyRegistry;
import zoranodensha.vehicleParts.client.CabKeyRegistry.CabKeyBinding;
import zoranodensha.vehicleParts.client.render.seat.VehParRenderer_Seat;



@SideOnly(Side.CLIENT)
public class ClientEventHandler extends EventHandler
{
	@SubscribeEvent
	public void actionPerformedEvent(ActionPerformedEvent event)
	{
		/* Only trigger if the new GUI is null and the event wasn't cancelled. */
		if (event.isCanceled() || !Minecraft.getMinecraft().isGamePaused())
		{
			return;
		}

		/* If applicable, reset all cached vertex states (and clean list in the process). */
		Iterator<WeakReference<ICachedVertexState>> itera = ICachedVertexState.objectList.iterator();
		WeakReference<ICachedVertexState> ref;
		ICachedVertexState cvs;

		int removals = 0;
		int resets = 0;

		while (itera.hasNext())
		{
			ref = itera.next();
			if ((cvs = ref.get()) != null)
			{
				cvs.onVertexStateReset();
				++resets;
			}
			else
			{
				itera.remove();
				++removals;
			}
		}

		ModCenter.log.debug(String.format("Reset %d cached vertex states and removed %d dead references.", resets, removals));
	}

	@SubscribeEvent
	public void inputEvent(InputEvent event)
	{
		Minecraft mc = Minecraft.getMinecraft();

		/*
		 * Ray trace key bindings.
		 */
		{
			KeyBinding key = null;
			boolean isRightClick = false;

			if (mc.gameSettings.keyBindUseItem.getIsKeyPressed())
			{
				key = mc.gameSettings.keyBindUseItem;
				isRightClick = true;
			}
			else if (mc.gameSettings.keyBindAttack.getIsKeyPressed())
			{
				key = mc.gameSettings.keyBindAttack;
			}

			if (key != null)
			{
				/* Prepare ray trace vector. */
				double d0 = mc.playerController.getBlockReachDistance();
				Vec3 vec3_Pos = mc.renderViewEntity.getPosition(1.0F);
				Vec3 vec3_Look = mc.renderViewEntity.getLook(1.0F);
				Vec3 vec3_Ray = vec3_Pos.addVector(vec3_Look.xCoord * d0, vec3_Look.yCoord * d0, vec3_Look.zCoord * d0);
				ArrayList<VehParBase> hitCandidates = new ArrayList<VehParBase>();

				/* Ray trace on client through all loaded trains. */
				for (Object entity : mc.theWorld.loadedEntityList)
				{
					if (entity instanceof Train)
					{
						Train train = (Train)entity;
						if (train.isEntityAlive())
						{
							/* Any vehicle part in the train is a potential hit candidate. */
							List<VehParBase> parts = new ArrayList<VehParBase>(train.getVehicleParts(OrderedPartsList.SELECTOR_ANY));

							/* If this is a right click, purge all non-activatable parts from the list. */
							if (isRightClick)
							{
								Iterator<VehParBase> itera = parts.iterator();
								outer: while (itera.hasNext())
								{
									for (APartType type : itera.next().getPartTypes())
									{
										if (type instanceof IPartType_Activatable)
										{
											continue outer;
										}
									}
									itera.remove();
								}
							}

							/* Now run a ray trace through all vehicle parts and add matching parts to the candidates list. */
							PartHelper.rayTraceParts(hitCandidates, vec3_Pos, vec3_Ray, null, parts);
						}
					}
				}

				/* If not empty, send hit candidates to server. Also clear the respective key binding. */
				if (!hitCandidates.isEmpty())
				{
					while (key.isPressed())
					{
						;
					}

					ModCenter.snw.sendToServer(new TrainMessage_RayTrace(isRightClick, hitCandidates, vec3_Pos, vec3_Ray));
					mc.thePlayer.swingItem();
				}
			}
		}

		/*
		 * Other key bindings.
		 */
		if (event instanceof KeyInputEvent)
		{
			EntityPlayer player = Minecraft.getMinecraft().thePlayer;
			if (player.ridingEntity instanceof EntitySeat)
			{
				PartTypeSeat seat = ((EntitySeat)player.ridingEntity).getPartType();
				if (seat instanceof PartTypeCabBasic)
				{
					ArrayList<CabKeyBinding> keyBindings = CabKeyRegistry.eventTick(Keyboard.getEventKey(), Keyboard.getEventKeyState());
					if (!keyBindings.isEmpty())
					{
						/* Notify respective cab of key presses. */
						PartTypeCabBasic cab = (PartTypeCabBasic)seat;
						for (CabKeyBinding keyBinding : keyBindings)
						{
							cab.onClientKeyChange(keyBinding.getCabKey(), keyBinding.isPressed());
						}

						/* Unpress Minecraft keybinds. */
						for (KeyBinding keyBinding_mc : mc.gameSettings.keyBindings)
						{
							/* If the key binding's key code is the same as the event key, clear it. */
							if (keyBinding_mc.getKeyCode() == Keyboard.getEventKey())
							{
								while (keyBinding_mc.isPressed())
								{
									;
								}
							}
						}

						/* Close any GUI that may have been opened by the previous key press. Limited number of attempts to prevent freezing the game. */
						for (int maxIterations = 4; mc.currentScreen != null && maxIterations >= 0; --maxIterations)
						{
							mc.displayGuiScreen(null);
						}
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void livingAttackEvent(LivingAttackEvent event)
	{
		if (event.isCanceled() || event.source == null || !ModCenter.cfg.rendering.enableRealisticBlood)
		{
			return;
		}

		Entity entity = event.source.getEntity();
		if (entity instanceof Train)
		{
			Helpers.spawnParticles(EParticles.BLOOD, event.entityLiving, entity.getEntityId(), (int)(event.ammount + 0.5F));
		}
	}

	@SubscribeEvent
	public void onRenderGui(RenderGameOverlayEvent.Post event)
	{
		if (event.type != ElementType.EXPERIENCE || !ModCenter.cfg.rendering.enableCabTooltips)
		{
			return;
		}

		/*
		 * Cab Tooltips
		 */
		EntityLivingBase viewEntity = Minecraft.getMinecraft().renderViewEntity;
		if (viewEntity != null && viewEntity.ridingEntity instanceof EntitySeat)
		{
			/* Only render in first person view. */
			Minecraft mc = Minecraft.getMinecraft();
			if (mc.gameSettings.thirdPersonView != 0)
			{
				return;
			}

			/* Determine whether the viewing entity sits in a cab. */
			EntitySeat seat = (EntitySeat)viewEntity.ridingEntity;
			if (seat.getPartType() instanceof PartTypeCab)
			{
				/* If so, render the hovered button's tool tip, if existent. */
				PartTypeCab cab = (PartTypeCab)seat.getPartType();
				if (cab.hoveredButton != null)
				{
					ScaledResolution resolution = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
					int width = resolution.getScaledWidth();
					int height = resolution.getScaledHeight();
					mc.fontRenderer.drawStringWithShadow(cab.hoveredButton.getTooltip().trim(), (width / 2) + 4, (height / 2) + 4, 0xDDDD00);
				}
			}
		}
	}

	@SubscribeEvent
	public void renderHandEvent(RenderHandEvent event)
	{
		if (ModCenter.cfg.rendering.enableCameraAlignment)
		{
			EntityLivingBase viewEntity = Minecraft.getMinecraft().renderViewEntity;
			if (viewEntity != null && viewEntity.ridingEntity instanceof EntitySeat)
			{
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public void renderLivingEventPre(RenderLivingEvent.Pre event)
	{
		if (!VehParRenderer_Seat.bypassEventCancellation && MinecraftForgeClient.getRenderPass() >= 0)
		{
			if (event.entity != null && event.entity.ridingEntity instanceof EntitySeat)
			{
				event.setCanceled(true);
			}
		}
	}



	/** A float wrapper holding the previous FOV value. {@code null} if the current FOV value isn't modified. */
	private Float prevFOV;
	/** A float wrapper holding the interpolated yaw angle of the section owning the seat ridden by the player. */
	private Float interpolatedYaw;



	/**
	 * This event listener overrides the in-game FOV modifier while sitting in a cab.
	 */
	@SubscribeEvent
	public void renderTickEvent(RenderTickEvent event)
	{
		GameSettings gameSettings = Minecraft.getMinecraft().gameSettings;
		EntityLivingBase viewEntity = Minecraft.getMinecraft().renderViewEntity;

		switch (event.phase)
		{
			case START:
				if (viewEntity != null && viewEntity.ridingEntity instanceof EntitySeat)
				{
					/* Retrieve ridden seat part and ensure it exists. */
					PartTypeSeat seat = ((EntitySeat)viewEntity.ridingEntity).getPartType();
					if (seat == null)
					{
						break;
					}

					/* Modify FOV while riding a cab seat. */
					if (!Minecraft.getMinecraft().isGamePaused())
					{
						if (seat instanceof PartTypeCabBasic && gameSettings.thirdPersonView == 0)
						{
							prevFOV = new Float(gameSettings.fovSetting);
							gameSettings.fovSetting = prevFOV * ModCenter.cfg.rendering.modifierCabFOV;
						}
					}

					/* Align view while riding any seat. */
					if (ModCenter.cfg.rendering.enableCameraAlignment)
					{
						VehicleSection section = seat.getSection();
						if (section != null)
						{
							interpolatedYaw = new Float(section.getPrevRotationYaw() + (section.getRotationYaw() - section.getPrevRotationYaw()) * event.renderTickTime);

							viewEntity.rotationYaw += interpolatedYaw;
							viewEntity.prevRotationYaw += interpolatedYaw;

							viewEntity.rotationYawHead += interpolatedYaw;
							viewEntity.prevRotationYawHead += interpolatedYaw;
						}
					}
				}
				break;

			case END:
				if (prevFOV != null)
				{
					gameSettings.fovSetting = prevFOV;
					prevFOV = null;
				}

				if (interpolatedYaw != null)
				{
					viewEntity.rotationYaw -= interpolatedYaw;
					viewEntity.prevRotationYaw -= interpolatedYaw;

					viewEntity.rotationYawHead -= interpolatedYaw;
					viewEntity.prevRotationYawHead -= interpolatedYaw;

					interpolatedYaw = null;
				}
				break;
		}
	}
}
