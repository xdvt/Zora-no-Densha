package zoranodensha.client.entityFX;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFireworkSparkFX;
import net.minecraft.world.World;
import zoranodensha.common.util.ZnDMathHelper;



@SideOnly(Side.CLIENT)
public class EntityPowerSpark extends EntityFireworkSparkFX
{
	public EntityPowerSpark(World world, double x, double y, double z)
	{
		super(world, x, y, z, ZnDMathHelper.ran.nextDouble() * 0.5D, ZnDMathHelper.ran.nextDouble() * 0.15D, ZnDMathHelper.ran.nextDouble() * 0.5D, Minecraft.getMinecraft().effectRenderer);

		setParticleTextureIndex(160);
		setColour(0xD3FEFF);
		setTrail(false);
		setTwinkle(true);

		particleScale *= rand.nextFloat() * 0.4D;
		particleMaxAge = 16 + rand.nextInt(4);
	}
}
