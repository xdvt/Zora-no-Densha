package zoranodensha.client.entityFX;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.particle.EntityDropParticleFX;
import net.minecraft.entity.Entity;
import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.util.ZnDMathHelper;



@SideOnly(Side.CLIENT)
public class EntityBloodParticle extends EntityDropParticleFX
{
	protected boolean hasCollided = false;



	public EntityBloodParticle(Entity victim, Train train)
	{
		super(	train.worldObj, victim.posX + (ZnDMathHelper.ran.nextFloat() - 0.5F), victim.posY + (ZnDMathHelper.ran.nextFloat() - 0.5F), victim.posZ + (ZnDMathHelper.ran.nextFloat() - 0.5F),
				Material.air);

		PartTypeBogie bogie = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY).getClosestPartType(victim.posX, victim.posY, victim.posZ);
		if (bogie != null)
		{
			final double speed = train.getLastSpeed();
			Vec3 dir = bogie.getDirection();
			Vec3 vec = Vec3.createVectorHelper(dir.xCoord * speed, dir.yCoord * speed, dir.zCoord * speed);

			vec.rotateAroundZ((rand.nextFloat() - 0.5F) * 0.3F);
			vec.rotateAroundY((rand.nextFloat() - 0.5F) * 0.6F);
			vec.rotateAroundX((rand.nextFloat() - 0.5F) * 0.6F);

			setVelocity(vec.xCoord, vec.yCoord, vec.zCoord);
		}

		setRBGColorF(0.724F - (rand.nextFloat() * 0.15F), 0.055F, 0.055F);
		setParticleTextureIndex(0);

		particleScale = (rand.nextInt(20) == 0) ? (rand.nextFloat() * rand.nextFloat()) + 10.0F : (rand.nextFloat() * rand.nextFloat() * 3.0F) + 1.0F;
		particleMaxAge = 501 + rand.nextInt(500);
	}

	@Override
	public void onUpdate()
	{
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;

		float maxAge = particleMaxAge;
		if (worldObj.isRaining())
		{
			maxAge *= 0.75F;
		}

		if (++particleAge >= maxAge)
		{
			setDead();
		}

		if (!isCollided)
		{
			motionY -= (hasCollided ? (0.001D * particleScale) : 0.012D);
			moveEntity(motionX, motionY, motionZ);

			if (isCollided)
			{
				motionX = 0.0D;
				motionY = 0.0D;
				motionZ = 0.0D;
				hasCollided = true;
			}
			else
			{
				motionX *= 0.995D;
				motionZ *= 0.995D;
			}
		}
		else
		{
			moveEntity(0.0D, 0.0D, 0.0D);
		}
	}
}
