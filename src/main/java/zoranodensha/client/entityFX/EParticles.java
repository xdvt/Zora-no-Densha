package zoranodensha.client.entityFX;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;



@SideOnly(Side.CLIENT)
public enum EParticles
{
	BLOOD,
	SPARKS;
}
