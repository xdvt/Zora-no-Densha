package zoranodensha.client.cfg;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.common.core.cfg.AConfigCategory;
import zoranodensha.vehicleParts.client.CabKeyRegistry;
import zoranodensha.vehicleParts.client.CabKeyRegistry.CabKeyBinding;



@SideOnly(Side.CLIENT)
public class Ctgy_CabControls extends AConfigCategory
{
	public Ctgy_CabControls()
	{
		super("cab");
	}

	@Override
	protected void loadCategory()
	{
		for (CabKeyBinding key : CabKeyRegistry.keys)
		{
			String val = getString(key.toString(), Keyboard.getKeyName(key.keyCodeDefault));
			int keyCode = Keyboard.getKeyIndex(val.toUpperCase());

			if (keyCode != 0)
			{
				key.keyCode = keyCode;
			}
		}
	}
}