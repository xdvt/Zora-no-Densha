package zoranodensha.client.cfg;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.common.core.cfg.AConfigCategory;



@SideOnly(Side.CLIENT)
public class Ctgy_Rendering extends AConfigCategory
{
	public boolean enableCameraAlignment;
	public int detailOther;
	public int detailTracks;
	public int detailVehicles;
	public float modifierCabFOV;
	public boolean enableCabTooltips;
	public boolean enableFancyLights;
	public boolean enableRealisticBlood;



	public Ctgy_Rendering()
	{
		super("render");
	}

	@Override
	protected void loadCategory()
	{
		enableCameraAlignment = getBoolean("cam_align", true);
		detailOther = getInt("detail_other", 2, 0, 2);
		detailTracks = getInt("detail_tracks", 2, 0, 2);
		detailVehicles = getInt("detail_vehicles", 2, 0, 2);
		modifierCabFOV = getFloat("modifier_cab_fov", 0.75F, 0.5F, 1.0F);
		enableCabTooltips = getBoolean("tooltips", true);
		enableFancyLights = getBoolean("fancy_lights", true);
		enableRealisticBlood = getBoolean("collision", false);
	}
}