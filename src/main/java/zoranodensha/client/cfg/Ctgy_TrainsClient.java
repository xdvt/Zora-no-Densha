package zoranodensha.client.cfg;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.common.core.cfg.Ctgy_Trains;

@SideOnly(Side.CLIENT)
public class Ctgy_TrainsClient extends Ctgy_Trains
{
	@Override
	protected void loadCategory()
	{
		super.loadCategory();
		PartTypeBogie.updateMovementTime(getInt("movement_time", 40, 30, 100));
	}
}