package zoranodensha.client.gui;

import java.awt.Rectangle;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.api.structures.signals.SignalProperty;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.network.packet.gui.PacketGUISignal;



public class GUISignal extends GuiScreen
{
	/*
	 * Resources
	 */
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/guis/signalGui.png");

	/*
	 * Button Indices
	 */
	private static final int BUTTON_CLOSE = 0;
	private static final int BUTTON_SCROLLDOWN = 1;
	private static final int BUTTON_SCROLLUP = 2;
	private static final int BUTTON_RESET = 3;
	private static final int BUTTON_VALUE = 20; // This is an offset

	/*
	 * Dimensions
	 */
	private static Rectangle thisRectangle = new Rectangle(186, 218);

	/*
	 * Misc
	 */
	/**
	 * The maximum number of values that are visible in the scrollable viewport.
	 */
	private final int scrollViewportSize = 4;
	/**
	 * The greater this number, the more 'scrolled down' the viewport is.
	 */
	private int scroll = 0;

	/*
	 * References
	 */
	/**
	 * The signal this GUI refers to.
	 */
	private ISignal signalReference;



	/**
	 * Initialises a new instance of the {@link zoranodensha.client.gui.GUISignal}
	 * class.
	 * 
	 * @param signal - The signal in the game world which this GUI should refer to.
	 */
	public GUISignal(ISignal signal)
	{
		this.signalReference = signal;
	}


	@Override
	protected void actionPerformed(GuiButton button)
	{
		switch (button.id)
		{
			/*
			 * Close the GUI screen.
			 */
			case BUTTON_CLOSE: {
				mc.thePlayer.closeScreen();
				break;
			}

			/*
			 * Scroll the properties viewport up.
			 */
			case BUTTON_SCROLLUP: {
				scroll--;

				if (scroll < 0)
				{
					scroll = 0;
				}

				break;
			}

			/*
			 * Scroll the properties viewport down.
			 */
			case BUTTON_SCROLLDOWN: {
				int maxScroll = (scrollViewportSize * -1) + signalReference.getAdditionalProperties().size();

				if (scroll < maxScroll)
				{
					scroll++;
				}

				break;
			}

			case BUTTON_RESET: {
				ModCenter.snw.sendToServer(new PacketGUISignal(signalReference.getX(), signalReference.getY(), signalReference.getZ()).addNewProperty("reset", 0));

				this.initGui();
				break;
			}
		}

		if (button.id >= BUTTON_VALUE)
		{
			int valueIndex = button.id - BUTTON_VALUE;

			Object propertyAtIndex = signalReference.getAdditionalProperties().values().toArray()[valueIndex];
			SignalProperty property;

			if (propertyAtIndex instanceof SignalProperty)
			{
				property = (SignalProperty)propertyAtIndex;
				property.setNext();

				// Get the key
				Object[] keys = signalReference.getAdditionalProperties().keySet().toArray();
				String key = keys[valueIndex].toString();

				ModCenter.snw.sendToServer(new PacketGUISignal(signalReference.getX(), signalReference.getY(), signalReference.getZ()).addNewProperty(key, property.get()));

				this.initGui();
			}
		}
	}

	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTick)
	{
		int centreX = this.width / 2;
		String text;

		/*
		 * Default transparent black screen.
		 */
		drawDefaultBackground();

		/*
		 * Signal background.
		 */
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		drawTexturedModalRect(getLeft(), getTop(), 0, 0, thisRectangle.width, thisRectangle.height);

		/*
		 * Data
		 */
		text = StatCollector.translateToLocal(signalReference.getName() + ".name");
		drawCenteredString(fontRendererObj, text, centreX, getTop() + 12, 0x00FF00);

		text = translate("name") + ":";
		drawString(fontRendererObj, text, getLeft() + 12, getTop() + 28, 0x00FF00);
		text = signalReference.getID();
		drawString(fontRendererObj, text, getRight() - 80, getTop() + 28, 0x00FF00);

		text = translate("blocksFree") + ":";
		drawString(fontRendererObj, text, getLeft() + 12, getTop() + 40, 0x00FF00);
		if (signalReference.getIndication().getBlocksFree() > 10)
		{
			text = "10+";
		}
		else
		{
			text = String.valueOf(signalReference.getIndication().getBlocksFree());
		}
		drawString(fontRendererObj, text, getRight() - 80, getTop() + 40, 0x00FF00);

		text = translate("blockLength") + ":";
		drawString(fontRendererObj, text, getLeft() + 12, getTop() + 52, 0x00FF00);
		text = Math.round(signalReference.getBlockLength()) + " m";
		drawString(fontRendererObj, text, getRight() - 80, getTop() + 52, 0x00FF00);

		if (signalReference.getInterlockedTracks().size() > 0)
		{
			text = translate("interlockedYes") + ":";
			drawString(fontRendererObj, text, getLeft() + 12, getTop() + 64, 0x00FF00);
			text = String.valueOf(signalReference.getInterlockedTracks().size());
			drawString(fontRendererObj, text, getRight() - 80, getTop() + 64, 0x00FF00);
		}
		else
		{
			text = translate("interlockedNo");
			drawString(fontRendererObj, text, getLeft() + 12, getTop() + 64, 0x00FF00);
		}

		/*
		 * Keys
		 */
		Object[] keys = signalReference.getAdditionalProperties().keySet().toArray();
		if (keys.length > 0)
		{
			for (int i = scroll; i < keys.length && i < scrollViewportSize + scroll; i++)
			{
				text = StatCollector.translateToLocal(signalReference.getName() + "." + keys[i].toString());
				drawString(fontRendererObj, text, getLeft() + 10, getTop() + 114 + (i * 20) - (scroll * 20), 0xFFFFFF);
			}
		}
		else
		{
			text = translate("noProperties");
			drawCenteredString(fontRendererObj, text, centreX, getTop() + 120, 0xEEEEEE);
		}

		super.drawScreen(mouseX, mouseY, partialTick);
	}


	/**
	 * @return - The maximum Y value of the GUI screen.
	 */
	public int getBottom()
	{
		return getTop() + thisRectangle.height;
	}

	/**
	 * @return - The minimum X value of the GUI screen.
	 */
	public int getLeft()
	{
		return (width - thisRectangle.width) / 2;
	}

	/**
	 * @return - The maximum X value of the GUI screen.
	 */
	public int getRight()
	{
		return getLeft() + thisRectangle.width;
	}

	/**
	 * @return - The minimum Y value of the GUI screen.
	 */
	public int getTop()
	{
		return (height - thisRectangle.height) / 2;
	}

	@Override
	public void initGui()
	{
		super.initGui();

		/*
		 * Initialise the buttons.
		 */
		buttonList.clear();
		{
			/*
			 * Close
			 */
			buttonList.add(new GuiButton(BUTTON_CLOSE, getLeft() + thisRectangle.width - 79, getTop() + thisRectangle.height - 25, 75, 20, translate("close")));

			/*
			 * Properties
			 */
			Object[] values = signalReference.getAdditionalProperties().values().toArray();
			for (int i = scroll; i < values.length && i < scrollViewportSize + scroll; i++)
			{
				if (values[i] instanceof SignalProperty)
				{
					SignalProperty property = (SignalProperty)values[i];
					buttonList.add(new GuiButton(BUTTON_VALUE + i, getRight() - 61, getBottom() - 109 + (i * 20) - (scroll * 20), 55, 20, String.valueOf(property.get())));
				}
			}

			/*
			 * Up/Down
			 */
			buttonList.add(new GuiButton(BUTTON_SCROLLUP, getLeft() + 5, getBottom() - 25, 40, 20, "Up"));
			buttonList.add(new GuiButton(BUTTON_SCROLLDOWN, getLeft() + 50, getBottom() - 25, 40, 20, "Down"));

			/*
			 * Clear Interlock List
			 */
			if (signalReference.getInterlockedTracks().size() > 0)
			{
				buttonList.add(new GuiButton(BUTTON_RESET, getLeft() + 5, getTop() + 84, 177, 20, translate("automaticReset")));
			}
			else
			{
				GuiButton button = new GuiButton(BUTTON_RESET, getLeft() + 5, getTop() + 84, 177, 20, translate("automatic"));
				button.enabled = false;
				buttonList.add(button);
			}
		}
	}

	private String translate(String key)
	{
		return StatCollector.translateToLocal("zoranodensha.text.guiSignal." + key);
	}

	@Override
	public void updateScreen()
	{
		super.updateScreen();

		/*
		 * Refresh the GUI's data with the tile entity in case it changes.
		 */
		TileEntitySignal oldTileEntity = (TileEntitySignal)signalReference.getTileEntity();
		signalReference = oldTileEntity.getSignal();

		this.initGui();
	}



	/**
	 * A GUI text field which can accept only numbers.
	 * 
	 * @author Jaffa
	 */
	public static class NumberInput extends GuiTextField
	{
		/**
		 * Whether this value has been changed by the user.
		 */
		public boolean valueChanged = false;
		public boolean applyChanges = false;



		public NumberInput(FontRenderer fontRendererObj, int x, int y, int width, int height)
		{
			super(fontRendererObj, x, y, width, height);

			this.setEnabled(true);
		}


		@Override
		public boolean textboxKeyTyped(char c, int i)
		{
			if (!isFocused())
				return false;

			switch (c)
			{

				/* Enter/CR */
				case 13: {
					if (valueChanged && getText().length() > 0)
					{
						applyChanges = true;
						valueChanged = false;

						return true;
					}
					return false;
				}

				/* Backspace */
				case 8: {
					this.deleteFromCursor(-1);
					this.valueChanged = true;

					return true;
				}

				/* Any other key */
				default: {
					if (isNumber(c))
					{
						this.writeText(String.valueOf(c));
						this.valueChanged = true;

						return true;
					}

					return false;
				}

			}
		}


		/**
		 * Determines if the character specified is a numerical character.
		 * 
		 * @param c - The character to test.
		 * @return - {@link true} if the character is a numerical character,
		 *         {@link false} if not.
		 */
		private static boolean isNumber(char c)
		{
			return (c >= 48) && (c <= 57);
		}
	}

}
