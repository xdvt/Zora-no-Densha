package zoranodensha.client.gui;

import java.awt.Point;
import java.awt.Rectangle;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import zoranodensha.api.structures.signals.ISpeedSign;
import zoranodensha.client.gui.GUISignal.NumberInput;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.network.packet.gui.PacketGUISpeedSign;



/**
 * A GUI screen used to interface with a speed limit sign
 * ({@link zoranodensha.api.structures.signals.ISpeedSign}).
 * 
 * @author Jaffa
 */
public class GUISpeedSign extends GuiScreen
{
	/**
	 * The texture of the GUI background
	 */
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/guis/speedSignGui.png");

	/*
	 * Button Indices
	 */
	private static final int BUTTON_OFFSETLEFT = 0;
	private static final int BUTTON_OFFSETCENTRE = 1;
	private static final int BUTTON_OFFSETRIGHT = 2;

	/**
	 * A rectangle that is used to define the size and shape of this GUI.
	 */
	private static Rectangle thisRectangle = new Rectangle(70, 74);

	/**
	 * The speed sign this GUI represents.
	 */
	private ISpeedSign speedSign;

	/**
	 * The field in which the speed limit number is entered.
	 */
	private NumberInput speedLimitField;



	/**
	 * Constructor for the speed sign GUI class.
	 * 
	 * @param sign - The speed sign that this GUI will represent.
	 */
	public GUISpeedSign(ISpeedSign sign)
	{
		/*
		 * Assign the speed sign reference.
		 */
		this.speedSign = sign;
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		switch (button.id)
		{
			/*
			 * Offset Adjustments
			 */
			case BUTTON_OFFSETLEFT:
			case BUTTON_OFFSETCENTRE:
			case BUTTON_OFFSETRIGHT: {
				int offset;

				switch (button.id)
				{
					case BUTTON_OFFSETLEFT:
						offset = -1;
						break;

					case BUTTON_OFFSETCENTRE:
						offset = 0;
						break;

					case BUTTON_OFFSETRIGHT:
						offset = 1;
						break;

					default:
						offset = 0;
						break;
				}

				ModCenter.snw.sendToServer(new PacketGUISpeedSign(speedSign.getX(), speedSign.getY(), speedSign.getZ()).addNewOffset(offset));
				break;
			}
		}
	}


	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}


	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTick)
	{
		/*
		 * Determine the centre of the screen to draw the GUI correctly.
		 */
		Point centre = new Point(this.width / 2, this.height / 2);

		/*
		 * Render the default greyed-out background.
		 */
		drawDefaultBackground();

		/*
		 * Render the textured background.
		 */
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		drawTexturedModalRect(getLeft(), getTop(), 0, 0, thisRectangle.width, thisRectangle.height);

		speedLimitField.drawTextBox();

		if (speedLimitField.valueChanged)
		{
			drawCenteredString(fontRendererObj, "Hit [Enter] to apply the speed limit change.", centre.x, this.getBottom() + 30, 0xFFFF00);
		}

		/*
		 * Call the superclass method.
		 */
		super.drawScreen(mouseX, mouseY, partialTick);
	};


	/**
	 * Gets the position of the bottom-most point of the GUI window.
	 */
	public int getBottom()
	{
		return getTop() + thisRectangle.height;
	}


	/**
	 * Gets the position of the left-most point of the GUI window.
	 */
	public int getLeft()
	{
		return (width - thisRectangle.width) / 2;
	}


	/**
	 * Gets the position of the right-most point of the GUI window.
	 */
	public int getRight()
	{
		return getLeft() + thisRectangle.width;
	}


	/**
	 * Gets the position of the top-most point of the GUI window.
	 */
	public int getTop()
	{
		return (height - thisRectangle.height) / 2;
	}


	@Override
	public void initGui()
	{
		super.initGui();

		/*
		 * Initialise the speed limit field.
		 */
		speedLimitField = new NumberInput(this.fontRendererObj, getLeft() + 10, getBottom() - 50, 50, 20);
		speedLimitField.setMaxStringLength(3);
		speedLimitField.setText(String.valueOf(speedSign.getSpeedLimit()));
		speedLimitField.setFocused(true);

		/*
		 * Initialise all buttons.
		 */
		buttonList.clear();
		{
			/*
			 * Left/Centre/Right
			 */
			buttonList.add(new GuiButton(BUTTON_OFFSETLEFT, getLeft(), getBottom(), thisRectangle.width / 3, 20, translate("left")));
			buttonList.add(new GuiButton(BUTTON_OFFSETCENTRE, getLeft() + (thisRectangle.width / 3), getBottom(), thisRectangle.width / 3, 20, translate("centre")));
			buttonList.add(new GuiButton(BUTTON_OFFSETRIGHT, getLeft() + ((thisRectangle.width / 3) * 2), getBottom(), thisRectangle.width / 3, 20, translate("right")));
		}
	}


	@Override
	protected void keyTyped(char c, int i)
	{
		super.keyTyped(c, i);

		/*
		 * Update GUI components.
		 */
		speedLimitField.textboxKeyTyped(c, i);
	};


	/**
	 * Helper method to get the localised text from the current language file based on the supplied key.
	 */
	private static String translate(String key)
	{
		return StatCollector.translateToLocal("zoranodensha.text.guiSpeedSign." + key);
	}


	@Override
	public void updateScreen()
	{
		super.updateScreen();

		speedLimitField.updateCursorCounter();

		if (speedLimitField.applyChanges)
		{
			speedLimitField.applyChanges = false;
			speedLimitField.valueChanged = false;

			ModCenter.snw.sendToServer(new PacketGUISpeedSign(speedSign.getX(), speedSign.getY(), speedSign.getZ()).addNewSpeedLimit(Integer.parseInt(speedLimitField.getText())));

			mc.thePlayer.closeScreen();
		}

		/*
		 * TODO @Jaffa - Update the left centre right buttons.
		 */
	}
}
