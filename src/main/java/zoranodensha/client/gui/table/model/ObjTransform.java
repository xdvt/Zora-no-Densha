package zoranodensha.client.gui.table.model;

import java.util.Iterator;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.common.util.VecDir;



/**
 * Contains sub-classes that handle object transformation.
 */
@SideOnly(Side.CLIENT)
public abstract class ObjTransform
{
	public int axis = 0;
	protected final int mode;
	public OrderedPartsList parts;
	private final OrderedPartsList partsProtected = new OrderedPartsList();
	protected final Pivot pivot;
	protected final Vector3f objectSrc;
	protected Vector3f mousePos;



	public ObjTransform(int mode, float objX, float objY, float objZ, Pivot pivot, OrderedPartsList parts)
	{
		this.mode = mode;
		this.pivot = pivot;
		objectSrc = new Vector3f(objX, objY, objZ);
		partsProtected.addAll(parts);
		refreshParts();
	}

	public abstract void applyTransformation();

	public OrderedPartsList getUnchanged()
	{
		return partsProtected;
	}

	public abstract boolean isClipboard();

	public void refreshParts()
	{
		parts = new OrderedPartsList();
		Iterator<VehParBase> itera = partsProtected.iterator();
		NBTTagCompound nbt;
		VehParBase part;

		while (itera.hasNext())
		{
			try
			{
				nbt = new NBTTagCompound();

				part = itera.next();
				part.writeToNBT(nbt, ETagCall.ITEM);

				part = part.copy();
				part.readFromNBT(nbt, ETagCall.ITEM);
				part.getBoundingBox().setBB(part.getLocalBoundingBox());

				parts.add(part);
			}
			catch (Exception e)
			{
				/* Silent catch. */
			}
		}
	}

	public boolean render()
	{
		refreshParts();
		applyTransformation();

		/* Render transformation axis. */
		{
			final int depthFunc = GL11.glGetInteger(GL11.GL_DEPTH_FUNC);
			final boolean depthTest = GL11.glGetBoolean(GL11.GL_DEPTH_TEST);

			GL11.glDepthFunc(GL11.GL_ALWAYS);
			GL11.glEnable(GL11.GL_DEPTH_TEST);
			renderAxis();

			if (!depthTest)
			{
				GL11.glDisable(GL11.GL_DEPTH_TEST);
			}
			GL11.glDepthFunc(depthFunc);
		}

		return true;
	}

	public void renderAxis()
	{
		if (axis == 0)
		{
			return;
		}

		Vector3f vec = VehicleModel.getCenter(parts);

		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glBegin(GL11.GL_LINES);

		switch (axis)
		{
			case 1:
				GL11.glColor4f(1.0F, 0.2F, 0.2F, 1.0F);
				GL11.glVertex3f(vec.x + 100.0F, vec.y, vec.z);
				GL11.glVertex3f(vec.x - 100.0F, vec.y, vec.z);
				break;

			case 2:
				GL11.glColor4f(0.2F, 1.0F, 0.2F, 1.0F);
				GL11.glVertex3f(vec.x, vec.y + 100.0F, vec.z);
				GL11.glVertex3f(vec.x, vec.y - 100.0F, vec.z);
				break;

			case 3:
				GL11.glColor4f(0.2F, 0.2F, 1.0F, 1.0F);
				GL11.glVertex3f(vec.x, vec.y, vec.z + 100.0F);
				GL11.glVertex3f(vec.x, vec.y, vec.z - 100.0F);
				break;
		}

		GL11.glEnd();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);
	}

	public void updateMouse(VecDir vec)
	{
		mousePos = vec.getOffset(vec.getOnNormal(0.1F));
	}

	public static float getValueMostPresent(float f0, float f1, float f2, Quaternion quat)
	{
		if (f0 == f1 || f0 == f2)
		{
			return f0;
		}
		else if (f1 == f2)
		{
			return f1;
		}
		else
		{
			if (Math.abs(quat.x) > Math.abs(quat.y))
			{
				if (Math.abs(quat.x) > Math.abs(quat.z))
				{
					return (f1 + f2) / 2.0F;
				}

				return (f0 + f1) / 2.0F;
			}

			if (Math.abs(quat.y) > Math.abs(quat.z))
			{
				return (f0 + f2) / 2.0F;
			}

			return (f0 + f1) / 2.0F;
		}
	}

	public static final Vector3f rotateVecByQuat(Quaternion quat, Vector3f vec)
	{
		Vector3f vecQ = new Vector3f(quat.x, quat.y, quat.z);
		float s = quat.w;

		Vector3f u = new Vector3f(vecQ);
		u.scale(Vector3f.dot(vecQ, vec) * 2.0F);

		Vector3f v = new Vector3f(vec);
		v.scale(s * s - Vector3f.dot(vecQ, vecQ));

		Vector3f w = Vector3f.cross(vecQ, vec, null);
		w.scale(s * 2.0F);

		u.translate(v.x, v.y, v.z);
		u.translate(w.x, w.y, w.z);

		return u;
	}

	public static Vector3f updateMouseStatic(VecDir vec)
	{
		return vec.getOffset(vec.getOnNormal(0.1F));
	}
}