package zoranodensha.client.gui.table.model;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Project;
import org.lwjgl.util.vector.Vector3f;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.MathHelper;
import zoranodensha.common.util.VecDir;
import zoranodensha.common.util.ZnDMathHelper;



@SideOnly(Side.CLIENT)
public class Camera
{
	private static final float zNear = 0.1F;
	private static final float zFar = 50.0F;
	private static final float radius = 15.0F;
	private float offX;
	private float offY;
	private float offZ;
	private float fov;
	private double phi = 1.0D;
	private double theta = 1.0D;
	private boolean isOrtho;
	public int width;
	public int height;



	/**
	 * Initialise a Camera. Cameras are an assembly of data used during rendering.
	 *
	 * @param fov - The field of view (FOV) in degrees.
	 * @param width - The width of the camera window.
	 * @param height - The height of the camera window.
	 */
	public Camera(float fov, int width, int height)
	{
		offX = 0.0F;
		offY = 0.0F;
		offZ = 0.0F;
		this.width = width;
		this.height = height;
		this.fov = fov;
	}

	public Vector3f getViewVec()
	{
		return new Vector3f((float)(radius * Math.cos(theta) * Math.cos(phi)), (float)(radius * Math.sin(theta)), (float)(radius * Math.cos(theta) * Math.sin(phi)));
	}

	/**
	 * Offsets this Camera by the given values.
	 */
	public void offset(float x, float y)
	{
		Vector3f vecView = (Vector3f)getViewVec().negate();
		Vector3f vecSide = Vector3f.cross(vecView, new Vector3f(0.0F, 1.0F, 0.0F), null).normalise(null);
		Vector3f vecUp = Vector3f.cross(vecView, vecSide, null).normalise(null);

		if (x != 0.0F)
		{
			vecSide.scale(x * 0.03F);

			offX += vecSide.x;
			offY += vecSide.y;
			offZ += vecSide.z;
		}

		if (y != 0.0F)
		{
			vecUp.scale(y * 0.03F);

			offX += vecUp.x;
			offY += vecUp.y;
			offZ += vecUp.z;
		}
	}

	/**
	 * Creates a ray vector for the given x and y position and returns it.
	 */
	public VecDir ray(int x, int y, float winX, float winY)
	{
		if (x < 0 || x > width || y < 0 || y > height)
		{
			return null;
		}

		FloatBuffer modelMatrix = BufferUtils.createFloatBuffer(64);
		FloatBuffer projMatrix = BufferUtils.createFloatBuffer(64);
		FloatBuffer objNear = BufferUtils.createFloatBuffer(3);
		FloatBuffer objFar = BufferUtils.createFloatBuffer(3);
		IntBuffer viewport = BufferUtils.createIntBuffer(16);

		GL11.glGetFloat(GL11.GL_MODELVIEW_MATRIX, modelMatrix);
		GL11.glGetFloat(GL11.GL_PROJECTION_MATRIX, projMatrix);
		GL11.glGetInteger(GL11.GL_VIEWPORT, viewport);

		if (Project.gluUnProject(winX, winY, 0.0F, modelMatrix, projMatrix, viewport, objNear) && Project.gluUnProject(winX, winY, 1.0F, modelMatrix, projMatrix, viewport, objFar))
		{
			return new VecDir(objNear.get(0), objNear.get(1), objNear.get(2), objFar.get(0) - objNear.get(0), objFar.get(1) - objNear.get(1), objFar.get(2) - objNear.get(2));
		}

		return null;
	}

	/**
	 * Rotate this Camera by the passed in values.
	 *
	 * @param theta - The rotation around the X-axis, in radians.
	 * @param phi - The rotation around the Y-axis, in radians.
	 */
	public void rotate(float theta, float phi)
	{
		theta *= 0.006F;
		phi *= 0.006F;

		this.theta = MathHelper.clamp_double((this.theta + theta) * ZnDMathHelper.DEG_MULTIPLIER, -90.0D, 90.0D) * ZnDMathHelper.RAD_MULTIPLIER;
		this.phi = wrapAngleTo((this.phi + phi) * ZnDMathHelper.DEG_MULTIPLIER, 360.0D) * ZnDMathHelper.RAD_MULTIPLIER;
	}

	/**
	 * Inverses the current view.
	 */
	public void rotateInv()
	{
		if (theta >= 90 * ZnDMathHelper.RAD_MULTIPLIER)
		{
			rotateTo(-90.0F, (float)(phi * ZnDMathHelper.DEG_MULTIPLIER));
		}
		else if (theta <= -90 * ZnDMathHelper.RAD_MULTIPLIER)
		{
			rotateTo(90.0F, (float)(phi * ZnDMathHelper.DEG_MULTIPLIER));
		}
		else
		{
			rotateTo((float)(theta * ZnDMathHelper.DEG_MULTIPLIER), (float)(phi * ZnDMathHelper.DEG_MULTIPLIER) + 180);
		}
	}

	/**
	 * Set the angles of this Camera to the passed in values.
	 *
	 * @param theta - The rotation around the X-axis, in degrees.
	 * @param phi - The rotation around the Y-axis, in degrees.
	 */
	public void rotateTo(float theta, float phi)
	{
		this.theta = MathHelper.clamp_double(theta, -90.0D, 90.0D) * ZnDMathHelper.RAD_MULTIPLIER;
		this.phi = wrapAngleTo(phi, 360.0D) * ZnDMathHelper.RAD_MULTIPLIER;
	}

	/**
	 * Changes the perspective.
	 */
	public void setOrtho()
	{
		isOrtho = !isOrtho;
	}

	/**
	 * Set this Camera up for rendering.
	 */
	public void setup(int posX, int posY, float scaleX, float scaleY)
	{
		int x = MathHelper.floor_float(posX * scaleX);
		int y = MathHelper.floor_float(posY * scaleY);
		int i = MathHelper.ceiling_float_int(width * scaleX);
		int j = MathHelper.ceiling_float_int(height * scaleY);

		GL11.glColor4f(0.0F, 0.0F, 0.0F, 1.0F);
		GL11.glViewport(x, y, i, j);
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();

		if (isOrtho)
		{
			float fovMod = fov / 90.0F;
			float aspect = (width / height);
			double dX = (width / 14.0D) * fovMod;
			double dY = (height / 14.0D) * fovMod;

			if (width >= height)
			{
				GL11.glOrtho(-dX * aspect, dX * aspect, -dY, dY, zNear, zFar);
			}
			else
			{
				GL11.glOrtho(-dX, dX, -dY / aspect, dY / aspect, zNear, zFar);
			}
		}
		else
		{
			Project.gluPerspective(75.0F, width / height, zNear, zFar);
		}

		Vector3f vecView = getViewVec();
		Vector3f vecSide = Vector3f.cross(vecView, new Vector3f(0.0F, 1.0F, 0.0F), null).normalise(null);
		Vector3f vecUp = Vector3f.cross(vecView, vecSide, null).normalise(null).negate(null);

		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		Project.gluLookAt( /* eyeX */ offX + vecView.x, /* eyeY */ offY + vecView.y, /* eyeZ */ offZ + vecView.z, offX, offY, offZ, /* upvX */ vecUp.x, /* upvY */ vecUp.y, /* upvZ */ vecUp.z);
	}

	/**
	 * Zoom in/ out by the specified value delta.
	 */
	public void zoom(float delta)
	{
		if (isOrtho)
		{
			if (delta < 0.0F)
			{
				if (fov < 170.0F)
				{
					fov -= delta;

					if (fov > 170.0F)
					{
						fov = 170.0F;
					}
				}
			}
			else
			{
				if (fov > 2.0F)
				{
					fov -= delta;

					if (fov < 2.0F)
					{
						fov = 2.0F;
					}
				}
			}
		}
		else
		{
			Vector3f vecView = (Vector3f)getViewVec().negate();
			vecView.scale(delta * 0.03F);

			offX += vecView.x;
			offY += vecView.y;
			offZ += vecView.z;
		}
	}

	private static final double wrapAngleTo(double angle, double d0)
	{
		angle %= d0;

		if (angle >= d0)
		{
			angle -= d0;
		}
		else if (angle < -d0)
		{
			angle += d0;
		}

		return angle;
	}
}
