package zoranodensha.client.gui.table.model;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.common.util.VecDir;



/**
 * Object transformation class for scaling.
 */
@SideOnly(Side.CLIENT)
public class ObjTransform_Scale extends ObjTransform
{
	private final float length;



	public ObjTransform_Scale(int mode, float objX, float objY, float objZ, VecDir mouseVec, Pivot pivot, OrderedPartsList parts)
	{
		super(mode, objX, objY, objZ, pivot, parts);

		updateMouse(mouseVec);
		length = Vector3f.sub(mousePos, objectSrc, null).length();
	}

	@Override
	public void applyTransformation()
	{
		refreshParts();

		/*
		 * Determine part translation origin, if applicable.
		 */
		Vector3f translationOrigin = null;
		Vector3f translationPart;

		switch (this.mode)
		{
			case 1:
				translationOrigin = VehicleModel.getCenter(parts);
				break;

			case 2:
				translationOrigin = new Vector3f(pivot.x, pivot.y, pivot.z);
				break;
		}

		/*
		 * Calculate new scale from mouse vectors.
		 */
		float scale = Vector3f.sub(mousePos, objectSrc, null).length() - length + 1.0F;
		if (scale < 0.0001F)
		{
			scale = 0.0001F;
		}

		/*
		 * Iterate over all parts, apply new scale (and translation, if applicable).
		 */
		boolean axis = (this.axis == 0);
		float[] scaleArr;
		float[] offsetArr;

		for (VehParBase part : parts)
		{
			if (translationOrigin != null)
			{
				offsetArr = part.getOffset();

				translationPart = Vector3f.sub(new Vector3f(offsetArr[0], offsetArr[1], offsetArr[2]), translationOrigin, null);
				translationPart.scale(scale);
				translationPart.translate(translationOrigin.x, translationOrigin.y, translationOrigin.z);

				offsetArr[0] = translationPart.x;
				offsetArr[1] = translationPart.y;
				offsetArr[2] = translationPart.z;

				part.setOffset(offsetArr[0], offsetArr[1], offsetArr[2]);
			}

			scaleArr = part.getScale();

			if (axis)
			{
				scaleArr[0] *= scale;
				scaleArr[1] *= scale;
				scaleArr[2] *= scale;
			}
			else
			{
				scaleArr[this.axis - 1] *= scale;
			}

			part.setScale(scaleArr[0], scaleArr[1], scaleArr[2]);
		}
	}

	@Override
	public boolean isClipboard()
	{
		return false;
	}

	@Override
	public void renderAxis()
	{
		if (axis == 0 || parts.size() != 1)
		{
			super.renderAxis();
			return;
		}

		VehParBase part = parts.get(0);
		GL11.glPushMatrix();
		{
			float[] arr = part.getOffset();
			GL11.glTranslatef(arr[0], arr[1], arr[2]);

			arr = part.getRotation();
			GL11.glRotatef(arr[1], 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(arr[2], 0.0F, 0.0F, 1.0F);
			GL11.glRotatef(arr[0], 1.0F, 0.0F, 0.0F);

			GL11.glDisable(GL11.GL_TEXTURE_2D);
			GL11.glDisable(GL11.GL_LIGHTING);
			GL11.glDisable(GL11.GL_CULL_FACE);
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glBegin(GL11.GL_LINES);

			switch (axis)
			{
				case 1:
					GL11.glColor4f(1.0F, 0.2F, 0.2F, 1.0F);
					GL11.glVertex3f(100, 0, 0);
					GL11.glVertex3f(-100, 0, 0);
					break;

				case 2:
					GL11.glColor4f(0.2F, 1.0F, 0.2F, 1.0F);
					GL11.glVertex3f(0, 100, 0);
					GL11.glVertex3f(0, -100, 0);
					break;

				case 3:
					GL11.glColor4f(0.2F, 0.2F, 1.0F, 1.0F);
					GL11.glVertex3f(0, 0, 100);
					GL11.glVertex3f(0, 0, -100);
					break;
			}

			GL11.glEnd();
			GL11.glEnable(GL11.GL_TEXTURE_2D);
			GL11.glEnable(GL11.GL_LIGHTING);
			GL11.glEnable(GL11.GL_CULL_FACE);
			GL11.glDisable(GL11.GL_BLEND);
		}
		GL11.glPopMatrix();
	}
}