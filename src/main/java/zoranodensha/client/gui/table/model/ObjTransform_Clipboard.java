package zoranodensha.client.gui.table.model;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;



/**
 * Object transformation class for translation.
 */
@SideOnly(Side.CLIENT)
public class ObjTransform_Clipboard extends ObjTransform
{
	public ObjTransform_Clipboard(float objX, float objY, float objZ, OrderedPartsList parts)
	{
		super(0, objX, objY, objZ, null, parts);
	}

	@Override
	public void applyTransformation()
	{
		refreshParts();
	}

	@Override
	public boolean isClipboard()
	{
		return true;
	}

	@Override
	public boolean render()
	{
		return false;
	}
}
