package zoranodensha.client.gui.table.model;

import java.util.Iterator;

import org.lwjgl.util.vector.Vector3f;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;



/**
 * Object transformation class for translation.
 */
@SideOnly(Side.CLIENT)
public class ObjTransform_Translate extends ObjTransform
{
	public ObjTransform_Translate(float objX, float objY, float objZ, Vector3f mouseVec, OrderedPartsList parts)
	{
		super(0, mouseVec.x, mouseVec.y, mouseVec.z, null, parts);
		mousePos = mouseVec;
	}

	@Override
	public void applyTransformation()
	{
		refreshParts();

		Vector3f transVec = Vector3f.sub(mousePos, objectSrc, null);
		Iterator<VehParBase> itera = parts.iterator();
		VehParBase part;
		float[] f0;

		while (itera.hasNext())
		{
			part = itera.next();
			f0 = part.getOffset();

			switch (axis)
			{
				case 0:
					f0[0] += transVec.x;
					f0[1] += transVec.y;
					f0[2] += transVec.z;
					break;

				case 1:
					f0[0] += transVec.x;
					break;

				case 2:
					f0[1] += transVec.y;
					break;

				case 3:
					f0[2] += transVec.z;
					break;
			}

			part.setOffset(f0[0], f0[1], f0[2]);
			part.getBoundingBox().setBB(part.getLocalBoundingBox());
		}
	}

	@Override
	public boolean isClipboard()
	{
		return false;
	}
}