package zoranodensha.client.gui.table.model;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector3f;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;



@SideOnly(Side.CLIENT)
public class Pivot
{
	public float x;
	public float y;
	public float z;
	public float size;



	public Pivot(float size)
	{
		x = 0.0F;
		y = 0.0F;
		z = 0.0F;
		this.size = size;
	}

	/**
	 * Render this pivot.
	 */
	public void renderPivot()
	{
		GL11.glPushMatrix();
		GL11.glTranslatef(x, y, z);
		GL11.glBegin(GL11.GL_LINES);

		/* X-Axis: Red */
		GL11.glColor4f(1.0F, 0.2F, 0.2F, 1.0F);
		GL11.glVertex3f(0.0F, 0.0F, 0.0F);
		GL11.glVertex3f(size, 0.0F, 0.0F);

		/* Y-Axis: Green */
		GL11.glColor4f(0.2F, 1.0F, 0.2F, 1.0F);
		GL11.glVertex3f(0.0F, 0.0F, 0.0F);
		GL11.glVertex3f(0.0F, size, 0.0F);

		/* Z-Axis: Blue */
		GL11.glColor4f(0.2F, 0.2F, 1.0F, 1.0F);
		GL11.glVertex3f(0.0F, 0.0F, 0.0F);
		GL11.glVertex3f(0.0F, 0.0F, size);

		GL11.glEnd();
		GL11.glPopMatrix();
	}

	/**
	 * Set the new position of this pivot.
	 */
	public void setPosition(Vector3f vec)
	{
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}
}
