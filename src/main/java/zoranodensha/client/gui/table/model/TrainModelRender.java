package zoranodensha.client.gui.table.model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.util.vector.Vector3f;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.client.MinecraftForgeClient;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.AVehiclePartRegistry;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.buttons.EModelEditorKeys;
import zoranodensha.client.gui.table.buttons.partEdit.ButtonPartEditL;
import zoranodensha.client.gui.table.buttons.partEdit.ButtonPartEditR;
import zoranodensha.client.render.ICachedVertexState;
import zoranodensha.client.render.entity.EntityRendererVehicleModel;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.VecDir;



/**
 * The class used to render and interact with the Engineer's Table GUI's train model.<br>
 * <br>
 * <br>
 * This class's {@code renderWorld()} method has been adapted from<br>
 * https://github.com/SleepyTrousers/EnderIO/blob/master/src/main/java/crazypants/enderio/gui/IoConfigRenderer.java (Accessed on 2016/03/02 17:30)<br>
 * <br>
 * Zora no Densha does not take any credit for the content of aforementioned method; All credit belongs to the rightful creator and author.
 * Variable names and syntax may have been changed, annotations may have been altered or removed.<br>
 * {@code renderWorld()} Copyright 2014 CrazyPants
 */
@SideOnly(Side.CLIENT)
public class TrainModelRender
{
	/** How transformation is applied. 0 = Individual origins; 1 = Center; 2 = Pivot; */
	private int transformMode = 0;
	private boolean changesMade;
	private boolean renderGrid = true;
	private boolean renderWorld;

	public Minecraft mc = Minecraft.getMinecraft();
	public World world = mc.thePlayer.worldObj;
	public VehicleModel model;
	public Camera camera = new Camera(75.0F, 90, 90);
	public Pivot pivot = new Pivot(2.0F);
	public ArrayList<NBTBase[]> undoList = new ArrayList<NBTBase[]>();

	private TrainModelHeightMap map = new TrainModelHeightMap(Blocks.dirt, world);
	private VecDir mouseVec;

	public ButtonPartEditL buttonPartEditL = new ButtonPartEditL(this);
	public ButtonPartEditR buttonPartEditR = new ButtonPartEditR(this);



	public TrainModelRender()
	{
		model = new VehicleModel(world);
	}

	public boolean canAddParts(int amount)
	{
		int totalNumParts;
		{
			totalNumParts = model.getVehicleParts().size() + model.selection.size() + amount;
			if (model.objTrans != null && !model.objTrans.isClipboard())
			{
				totalNumParts += model.objTrans.parts.size();
			}
		}

		return (totalNumParts <= 500);
	}

	protected boolean canSetPivotPos()
	{
		return (pivot != null && model.selection.isEmpty() && model.objTrans == null);
	}

	/**
	 * Draw the model screen.
	 */
	public void drawScreen(float partialTick, int posX, int posY, float scaleX, float scaleY)
	{
		if (renderWorld)
		{
			GL11.glScissor(MathHelper.floor_float(posX * scaleX), MathHelper.floor_float(posY * scaleY), MathHelper.ceiling_float_int(camera.width * scaleX), MathHelper.ceiling_float_int(camera.height * scaleY));
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
		}

		camera.setup(posX, posY, scaleX, scaleY);

		if (renderWorld)
		{
			renderModel(partialTick);
			renderWorld();
		}
		else
		{
			renderScene();
			renderModel(partialTick);
		}

		ScaledResolution scaledResolution = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
		mouseVec = camera.ray(	(Mouse.getX() * scaledResolution.getScaledWidth() / mc.displayWidth) - posX, (Mouse.getY() * scaledResolution.getScaledHeight() / mc.displayHeight - 1) - posY, Mouse.getX() + GUIEngineerTable.mouseOffX,
								Mouse.getY() + GUIEngineerTable.mouseOffY);

		if (model.objTrans != null && mouseVec != null)
		{
			model.objTrans.updateMouse(mouseVec);
		}

		GL11.glViewport(0, 0, mc.displayWidth, mc.displayHeight);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		mc.entityRenderer.setupOverlayRendering();
	}

	/**
	 * @return True if changes were applied to the model.
	 */
	public boolean getChangesMade()
	{
		return changesMade;
	}

	public boolean getRenderWorld()
	{
		return renderWorld;
	}

	public void keyTyped(char chara, int key)
	{
		if (buttonPartEditL.isTextboxSelected())
		{
			buttonPartEditL.keyTyped(chara, key);
		}
		else if (buttonPartEditR.isTextboxSelected())
		{
			buttonPartEditR.keyTyped(chara, key);
		}
		else
		{
			switch (chara)
			{
				case '1':
					/* Right Perspective = +Z -> 0 */
					camera.rotateTo(0.0F, 90.0F);
					return;

				case '2':
					/* 15° down */
					camera.rotate((-15.0F / 0.006F) * VehicleHelper.RAD_FACTOR, 0.0F);
					return;

				case '3':
					/* Front Perspective = +X -> 0 */
					camera.rotateTo(0.0F, 0.0F);
					return;

				case '4':
					/* 15° left */
					camera.rotate(0.0F, (15.0F / 0.006F) * VehicleHelper.RAD_FACTOR);
					return;

				case '5':
					camera.setOrtho();
					return;

				case '6':
					/* 15° right */
					camera.rotate(0.0F, (-15.0F / 0.006F) * VehicleHelper.RAD_FACTOR);
					return;

				case '7':
					/* Top Perspective = +Y -> 0 */
					camera.rotateTo(90.0F, 0.0F);
					return;

				case '8':
					/* 15° up */
					camera.rotate((15.0F / 0.006F) * VehicleHelper.RAD_FACTOR, 0.0F);
					return;

				case '9':
					/* Invert Perspective. */
					camera.rotateInv();
					return;
			}

			if (GuiScreen.isCtrlKeyDown() && EModelEditorKeys.KeyZ.isPressed(key))
			{
				popUndo();
			}
			else if (model.objTrans != null)
			{
				if (GuiScreen.isCtrlKeyDown() && EModelEditorKeys.KeyV.isPressed(key) && model.objTrans.isClipboard())
				{
					pushUndo();
					selectionClear(true);
					selectionAddObjTrans();

					return;
				}

				if (EModelEditorKeys.KeyX.isPressed(key))
				{
					model.objTrans.axis = 1;
				}
				else if (EModelEditorKeys.KeyY.isPressed(key))
				{
					model.objTrans.axis = 2;
				}
				else if (EModelEditorKeys.KeyZ.isPressed(key))
				{
					model.objTrans.axis = 3;
				}
			}
			else if (model.selection.size() > 0 && mouseVec != null)
			{
				ObjTransform objTrans = null;
				Vector3f vec0 = VehicleModel.getCenter(model.selection);

				if (GuiScreen.isCtrlKeyDown() && EModelEditorKeys.KeyC.isPressed(key))
				{
					model.objTrans = new ObjTransform_Clipboard(vec0.x, vec0.y, vec0.z, model.selection);
					selectionClear(true);
					return;
				}
				else if (EModelEditorKeys.KeyG.isPressed(key))
				{
					objTrans = new ObjTransform_Translate(vec0.x, vec0.y, vec0.z, ObjTransform.updateMouseStatic(mouseVec), model.selection);
				}
				else if (EModelEditorKeys.KeyR.isPressed(key))
				{
					objTrans = new ObjTransform_Rotate(transformMode, vec0.x, vec0.y, vec0.z, mouseVec, camera, pivot, model.selection);
				}
				else if (EModelEditorKeys.KeyS.isPressed(key))
				{
					objTrans = new ObjTransform_Scale(transformMode, vec0.x, vec0.y, vec0.z, mouseVec, pivot, model.selection);
				}
				else if (EModelEditorKeys.KeyX.isPressed(key))
				{
					pushUndo();
					model.selection.clear();
				}

				if (objTrans != null)
				{
					if (!objTrans.isClipboard())
					{
						pushUndo();
					}
					model.objTrans = objTrans;
					model.selection.clear();
				}
			}
		}
	}

	public void loadModelFromNBT(NBTTagCompound nbt)
	{
		try
		{
			model = new VehicleModel(world);
			model.readEntityFromNBT(nbt);
		}
		catch (NullPointerException npe)
		{
			ModCenter.log.warn("[TrainModelRender] Caught exception while constructing model from NBT", npe);
		}
	}

	public void mouseClicked(int x, int y, int button)
	{
		buttonPartEditL.mouseClicked(x, y, button);
		buttonPartEditR.mouseClicked(x, y, button);
	}

	public void popUndo()
	{
		if (!undoList.isEmpty() && model != null)
		{
			NBTBase[] nbts = undoList.remove(0);
			model.readEntityFromNBT((NBTTagCompound)nbts[0]);
			model.selection.clear();

			if (nbts[1] != null)
			{
				NBTTagList tagList = (NBTTagList)nbts[1];
				NBTTagCompound nbt;
				VehParBase part;

				for (int i = tagList.tagCount() - 1; i >= 0; --i)
				{
					nbt = tagList.getCompoundTagAt(i);
					part = AVehiclePartRegistry.getPart(nbt.getString(VehParBase.NBT_KEY));

					if (part != null)
					{
						part.readFromNBT(nbt, ETagCall.ITEM);
						model.selection.add(part);
					}
				}
			}

			setChangesMade(true);
		}
	}

	public void pushUndo()
	{
		if (model != null)
		{
			NBTTagCompound nbtModel = new NBTTagCompound();
			NBTTagList nbtSelection = null;
			model.writeEntityToNBT(nbtModel, ETagCall.ITEM);

			if (!model.selection.isEmpty())
			{
				nbtSelection = new NBTTagList();
				NBTTagCompound nbt;

				for (VehParBase part : model.selection)
				{
					part.writeToNBT(nbt = new NBTTagCompound(), ETagCall.ITEM);
					nbt.setString(VehParBase.NBT_KEY, part.getName());
					nbtSelection.appendTag(nbt);
				}
			}

			undoList.add(0, new NBTBase[] { nbtModel, nbtSelection });
		}
	}

	/**
	 * Casts a ray trace from the given position.
	 *
	 * @param addToSelection - If true, adds the found object - if existent - to the preset WITHOUT clearing the preset.
	 */
	public void rayTace(int lastMouseButton, boolean addToSelection)
	{
		if (mouseVec != null)
		{
			if (model.objTrans != null && !model.objTrans.isClipboard())
			{
				if (lastMouseButton == 0)
				{
					selectionAddObjTrans();
				}
				else if (this.selectionClear(false))
				{
					model.objTrans = null;
				}

				return;
			}

			if (lastMouseButton != 0)
			{
				return;
			}

			double closestDist = Double.MAX_VALUE;
			double iteratedDist;

			VehParBase closestPart = null;
			VehParBase iteratedPart;
			MovingObjectPosition movObjPos;

			Vector3f vec = mouseVec.getOffset(mouseVec.getOnNormal(1.0F));
			Vec3 point = Vec3.createVectorHelper(mouseVec.point.x, mouseVec.point.y, mouseVec.point.z);
			Vec3 normal = Vec3.createVectorHelper(vec.x, vec.y, vec.z);
			Iterator<VehParBase> itera = model.getVehicleParts().iterator();

			while (itera.hasNext())
			{
				iteratedPart = itera.next();
				iteratedPart.getBoundingBox().setBB(iteratedPart.getLocalBoundingBox());

				if ((movObjPos = iteratedPart.getBoundingBox().calculateIntercept(point, normal)) != null)
				{
					iteratedDist = (movObjPos.hitVec.xCoord * movObjPos.hitVec.xCoord + movObjPos.hitVec.yCoord * movObjPos.hitVec.yCoord + movObjPos.hitVec.zCoord * movObjPos.hitVec.zCoord);

					if (iteratedDist < closestDist)
					{
						closestPart = iteratedPart;
					}
				}
			}

			if (closestPart != null)
			{
				if (addToSelection)
				{
					if (model.selection.contains(closestPart))
					{
						return;
					}
				}

				pushUndo();

				if (!addToSelection)
				{
					this.selectionClear(true);
				}

				selectionAdd(closestPart, null, false);
			}
			else if (addToSelection)
			{
				if (canSetPivotPos())
				{
					pivot.setPosition(mouseVec.getOffset(mouseVec.getOnNormal()));
				}
			}
			else
			{
				pushUndo();
				this.selectionClear(true);
			}
		}
	}

	/**
	 * Renders the model into the scene.
	 */
	public void renderModel(float partialTick)
	{
		EntityRendererVehicleModel.ignorePartState = true;
		RenderManager.instance.renderEntityWithPosYaw(model, 0.0D, 0.0D, 0.0D, partialTick, 0.0F);
		EntityRendererVehicleModel.ignorePartState = false;
	}

	/**
	 * Renders floor grid and axial pointers into the scene.
	 */
	private void renderScene()
	{
		if (!renderGrid)
		{
			return;
		}

		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glBegin(GL11.GL_LINES);

		/* XY-Plane: Grey */
		GL11.glColor4f(0.4F, 0.4F, 0.4F, 0.25F);

		final int i = 10;
		int j = i;

		for (int k = -j; k <= i; ++k)
		{
			GL11.glVertex3f(-j, 0.0F, k);
			GL11.glVertex3f(j, 0.0F, k);
			GL11.glVertex3f(k, 0.0F, -j);
			GL11.glVertex3f(k, 0.0F, j);
		}

		GL11.glEnd();

		if (pivot != null)
		{
			final int depthFunc = GL11.glGetInteger(GL11.GL_DEPTH_FUNC);
			final boolean depthTest = GL11.glGetBoolean(GL11.GL_DEPTH_TEST);

			GL11.glDepthFunc(GL11.GL_ALWAYS);
			GL11.glEnable(GL11.GL_DEPTH_TEST);

			pivot.renderPivot();

			if (!depthTest)
			{
				GL11.glDisable(GL11.GL_DEPTH_TEST);
			}

			GL11.glDepthFunc(depthFunc);
		}

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);
	}

	public boolean renderSelectionData()
	{
		return (!model.selection.isEmpty() || (model.objTrans != null && !model.objTrans.isClipboard()));
	}

	public void renderSelectionData(GUIEngineerTable gui, float partialTick, int xPos, int yPos, float scaleX, float scaleY, int mouseX, int mouseY)
	{
		if (this.renderSelectionData())
		{
			GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);

			gui.mc.renderEngine.bindTexture(GUIEngineerTable.texture);
			RenderHelper.disableStandardItemLighting();
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

			buttonPartEditL.render(gui, xPos, yPos, scaleX, scaleY, mouseX, mouseY);
			buttonPartEditR.render(gui, xPos, yPos, scaleX, scaleY, mouseX, mouseY);

			RenderHelper.enableStandardItemLighting();
		}
	}

	/**
	 * Renders a dummy world into the scene.
	 */
	private void renderWorld()
	{
		/* Backup render passes. */
		final int savedPassWorld = ForgeHooksClient.getWorldRenderPass();
		final int savedPassEntity = MinecraftForgeClient.getRenderPass();
		Field worldRenderPass = null;

		try
		{
			worldRenderPass = ForgeHooksClient.class.getDeclaredField("worldRenderPass");
			worldRenderPass.setAccessible(true);
		}
		catch (Throwable t)
		{
		}

		/* Apply GL11 settings and Minecraft's texture map. */
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		RenderHelper.disableStandardItemLighting();
		mc.entityRenderer.disableLightmap(0);
		Minecraft.getMinecraft().renderEngine.bindTexture(TextureMap.locationBlocksTexture);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_ALPHA_TEST);

		GL11.glPushMatrix();
		GL11.glTranslatef(0.0F, -0.125F, 0.0F);
		{
			/* Iterate through both render passes and render. */
			for (int renderPass = 0; renderPass < 2; ++renderPass)
			{
				/* Prepare OpenGL settings depending on render pass. */
				pushRenderPass(renderPass);

				/* Set render passes. */
				if (worldRenderPass != null)
				{
					try
					{
						worldRenderPass.setInt(null, renderPass);
					}
					catch (Throwable t)
					{
					}
				}

				ForgeHooksClient.setRenderPass(renderPass);

				/* Render the map. */
				GL11.glPushMatrix();
				Tessellator.instance.startDrawingQuads();
				Tessellator.instance.setBrightness(15 << 20 | 15 << 4);
				map.render(renderPass);
				Tessellator.instance.draw();
				Tessellator.instance.setTranslation(0, 0, 0);
				GL11.glPopMatrix();

				RenderHelper.enableStandardItemLighting();
				GL11.glPushMatrix();
				map.renderTileEntityAndTrack(renderPass);
				GL11.glPopMatrix();

				/* Reset render passes. */
				if (worldRenderPass != null)
				{
					try
					{
						worldRenderPass.setInt(null, savedPassWorld);
					}
					catch (Throwable t)
					{
					}
				}

				ForgeHooksClient.setRenderPass(savedPassEntity);
			}
		}
		GL11.glPopMatrix();
		GL11.glDepthMask(true);
	}

	/**
	 * Adds the given IVehiclePart to the selection and removes it from the model - if existent.
	 */
	public void selectionAdd(VehParBase part, Iterator<VehParBase> itera, boolean isNew)
	{
		if (!isNew)
		{
			if (itera != null)
			{
				itera.remove();
			}
			else
			{
				model.removePart(part);
			}
		}

		model.selection.add(part);
		setChangesMade(true);
	}

	/**
	 * Adds the transformed IVehiclePart to the selection and clears the object transformation.
	 */
	public void selectionAddObjTrans()
	{
		if (model.objTrans != null)
		{
			if (model.objTrans.isClipboard() && !canAddParts(model.objTrans.parts.size()))
			{
				return;
			}

			model.objTrans.applyTransformation();

			for (VehParBase part : model.objTrans.parts)
			{
				model.selection.add(part);
				if (part instanceof ICachedVertexState)
				{
					((ICachedVertexState)part).onVertexStateReset();
				}
			}

			model.objTrans = null;
			setChangesMade(true);
		}
	}

	/**
	 * Clears the selection and adds all selected elements back to the model.
	 */
	public boolean selectionClear(boolean addToModel)
	{
		boolean flag = model.objTrans != null && model.objTrans.isClipboard();

		if (!flag)
		{
			selectionClearObjTrans();
		}

		if (addToModel && canAddParts(0))
		{
			for (VehParBase part : model.selection)
			{
				model.addPart(part);
				if (part instanceof ICachedVertexState)
				{
					((ICachedVertexState)part).onVertexStateReset();
				}
			}

			model.selection.clear();
		}

		return flag;
	}

	/**
	 * Clears the given part from the given Iterator and adds it back to the model part list.
	 */
	public void selectionClear(VehParBase part, Iterator<VehParBase> itera)
	{
		if (itera != null)
		{
			itera.remove();
			model.addPart(part);
			if (part instanceof ICachedVertexState)
			{
				((ICachedVertexState)part).onVertexStateReset();
			}
		}
	}

	/**
	 * Adds the currently transformed object back to the selection without transforming.
	 */
	public void selectionClearObjTrans()
	{
		if (model.objTrans != null)
		{
			if (!model.objTrans.isClipboard())
			{
				popUndo();
				// model.selection.addAll(model.objTrans.getUnchanged());
			}

			model.objTrans = null;
		}
	}

	/**
	 * Set if changes applied to the model. Usually called with "true", except for when the model was saved.
	 */
	public void setChangesMade(boolean changesMade)
	{
		this.changesMade = changesMade;
	}

	public void toggleGrid()
	{
		renderGrid = !renderGrid;
	}

	public void toggleMode()
	{
		++transformMode;

		if (transformMode > 2)
		{
			transformMode = 0;
		}
	}

	public void togglePivot()
	{
		if (pivot != null)
		{
			if (pivot.size < 2.0F)
			{
				pivot.size += 0.5F;
			}
			else
			{
				pivot = null;
			}
		}
		else
		{
			pivot = new Pivot(0.5F);
		}
	}

	public void toggleWorld()
	{
		renderWorld = !renderWorld;
	}

	public static void pushRenderPass(int pass)
	{
		switch (pass)
		{
			case 0:
				GL11.glEnable(GL11.GL_DEPTH_TEST);
				GL11.glDisable(GL11.GL_BLEND);
				GL11.glDepthMask(true);
				break;

			case 1:
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				GL11.glDepthMask(false);
				break;

			default:
				break;
		}
	}
}
