package zoranodensha.client.gui.table.model;

import org.lwjgl.util.vector.Vector3f;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.client.render.ICachedVertexState;



@SideOnly(Side.CLIENT)
public class VehicleModel extends Train
{
	@SideOnly(Side.CLIENT) public OrderedPartsList selection;
	@SideOnly(Side.CLIENT) public ObjTransform objTrans;



	@SideOnly(Side.CLIENT)
	public VehicleModel(World world)
	{
		super(world);

		setSize(0.0F, 0.0F);
		clearPartsList();
		selection = new OrderedPartsList();
	}


	/**
	 * Called to add the given part to this train's {@link #parts parts list}.<br>
	 * Triggers section and property update.
	 *
	 * @param part - {@link zoranodensha.api.vehicles.part.VehParBase Vehicle part} to add.
	 * @see #addPart(IVehiclePart, boolean)
	 */
	public void addPart(VehParBase part)
	{
		// Note: This goes against convention in VehicleData#getParts() and is only a temporary workaround. DO NOT DO THIS.
		getVehicleAt(0).getParts().add(part);
		onVehicleListChanged();

		if (part instanceof ICachedVertexState)
		{
			((ICachedVertexState)part).onVertexStateReset();
		}
	}

	public void clearPartsList()
	{
		String playerName = Minecraft.getMinecraft().thePlayer.getCommandSenderName();

		VehicleData vehicle = new VehicleData();
		vehicle.setCreatorName(playerName);

		getVehicleList().clear();
		getVehicleList().add(vehicle);
	}

	public OrderedPartsList getVehicleParts()
	{
		return getVehicleList().get(0).getParts();
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbt)
	{
		super.readEntityFromNBT(nbt, ETagCall.ITEM);

		if (worldObj != null && worldObj.isRemote)
		{
			setSize((float)(boundingBox.maxX - boundingBox.minX), (float)(boundingBox.maxY - boundingBox.minY));
		}
		else
		{
			setSize(1.0F, 1.0F);
		}

		if (getVehicleList().isEmpty())
		{
			getVehicleList().add(new VehicleData());
		}
	}
	
	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		/* Remove all vehicles from the train but the first one. */
		while (getVehicleCount() > 1)
		{
			removeVehicle(getVehicleAt(getVehicleCount() - 1));
		}
	}

	/**
	 * Remove the given part from this vehicle model.
	 *
	 * @param part - The {@link IVehiclePart part} that is to be removed.
	 * @return {@code true} if the part was successfully removed.
	 *
	 * @see java.util.ArrayList#remove(Object)
	 */
	public boolean removePart(VehParBase part)
	{
		if (getVehicleParts().remove(part))
		{
			if (part instanceof ICachedVertexState)
			{
				((ICachedVertexState)part).onVertexStateReset();
			}
			onVehicleListChanged();
		}
		return false;
	}

	/**
	 * Return a {@link Vector3f} pointing at the common center of all vehicle parts' translations in the given list.
	 */
	@SideOnly(Side.CLIENT)
	public static final Vector3f getCenter(OrderedPartsList list)
	{
		float[] pos = new float[3];
		float[] off;
		int cnt = list.size();
		int i;

		if (cnt <= 0)
		{
			return new Vector3f(0.0F, 0.0F, 0.0F);
		}

		for (VehParBase part : list)
		{
			off = part.getOffset();
			for (i = 0; i < pos.length; ++i)
			{
				pos[i] += off[i];
			}
		}

		if (cnt > 1)
		{
			float f = 1.0F / cnt;
			for (i = 0; i < pos.length; ++i)
			{
				pos[i] *= f;
			}
		}

		return new Vector3f(pos[0], pos[1], pos[2]);
	}
}