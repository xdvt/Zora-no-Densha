package zoranodensha.client.gui.table.buttons.partEdit;

import org.apache.commons.lang3.ArrayUtils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.property.APartProperty;



/**
 * Wrapper for primitive type properties.
 * Contains a dirty workaround to transform a primitive array into its wrapper type's array and back.
 */
@SideOnly(Side.CLIENT)
@SuppressWarnings("rawtypes")
public class PropWrapper<T> extends APartProperty
{
	public final APartProperty<?> wrappedProp;



	@SuppressWarnings("unchecked")
	public PropWrapper(APartProperty<?> wrappedProp)
	{
		super(wrappedProp.getParent(), wrappedProp.getClass(), wrappedProp.getName());
		this.wrappedProp = wrappedProp;
	}

	@Override
	public boolean equals(Object obj)
	{
		return wrappedProp.equals(obj);
	}

	@Override
	public T get()
	{
		return null;
	}

	@Override
	public boolean getHasChanged()
	{
		return wrappedProp.getHasChanged();
	}

	@Override
	public boolean getIsConfigurable()
	{
		return wrappedProp.getIsConfigurable();
	}

	@Override
	public boolean getIsValidTagCall(ETagCall tagCall)
	{
		return wrappedProp.getIsValidTagCall(tagCall);
	}

	@Override
	public ESyncDir getSyncDir()
	{
		return wrappedProp.getSyncDir();
	}

	@Override
	public String getUnlocalisedName()
	{
		return wrappedProp.getUnlocalisedName();
	}

	@Override
	public int hashCode()
	{
		return wrappedProp.hashCode();
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt, ETagCall callType)
	{
		wrappedProp.readFromNBT(nbt, callType);
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		return false;
	}

	@Override
	public boolean set(Object property)
	{
		return false;
	}

	@Override
	public APartProperty<?> setConfigurable()
	{
		return wrappedProp.setConfigurable();
	}

	@Override
	public void setHasChanged(boolean hasChanged)
	{
		wrappedProp.setHasChanged(hasChanged);
	}

	@Override
	public APartProperty<?> setSyncDir(ESyncDir syncDir)
	{
		return wrappedProp.setSyncDir(syncDir);
	}

	@Override
	public APartProperty<?> setValidTagCalls(EValidTagCalls validTagCalls)
	{
		return wrappedProp.setValidTagCalls(validTagCalls);
	}

	@Override
	public String toString()
	{
		return wrappedProp.toString();
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt, ETagCall callType)
	{
		wrappedProp.writeToNBT(nbt, callType);
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		;
	}



	public static class WrapperBoolean extends PropWrapper<Boolean[]>
	{
		public WrapperBoolean(APartProperty<boolean[]> wrappedProp)
		{
			super(wrappedProp);
		}

		@Override
		public Boolean[] get()
		{
			return ArrayUtils.toObject((boolean[])wrappedProp.get());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Boolean[])
			{
				return wrappedProp.set(ArrayUtils.toPrimitive((Boolean[])property));
			}
			else if (property instanceof boolean[])
			{
				return wrappedProp.set(property);
			}
			return super.set(property);
		}
	}

	public static class WrapperByte extends PropWrapper<Byte[]>
	{
		public WrapperByte(APartProperty<byte[]> wrappedProp)
		{
			super(wrappedProp);
		}

		@Override
		public Byte[] get()
		{
			return ArrayUtils.toObject((byte[])wrappedProp.get());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Byte[])
			{
				return wrappedProp.set(ArrayUtils.toPrimitive((Byte[])property));
			}
			else if (property instanceof byte[])
			{
				return wrappedProp.set(property);
			}
			return super.set(property);
		}
	}

	public static class WrapperCharacter extends PropWrapper<Character[]>
	{
		public WrapperCharacter(APartProperty<char[]> wrappedProp)
		{
			super(wrappedProp);
		}

		@Override
		public Character[] get()
		{
			return ArrayUtils.toObject((char[])wrappedProp.get());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Character[])
			{
				return wrappedProp.set(ArrayUtils.toPrimitive((Character[])property));
			}
			else if (property instanceof char[])
			{
				return wrappedProp.set(property);
			}
			return super.set(property);
		}
	}

	public static class WrapperDouble extends PropWrapper<Double[]>
	{
		public WrapperDouble(APartProperty<double[]> wrappedProp)
		{
			super(wrappedProp);
		}

		@Override
		public Double[] get()
		{
			return ArrayUtils.toObject((double[])wrappedProp.get());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Double[])
			{
				return wrappedProp.set(ArrayUtils.toPrimitive((Double[])property));
			}
			else if (property instanceof double[])
			{
				return wrappedProp.set(property);
			}
			return super.set(property);
		}
	}

	public static class WrapperFloat extends PropWrapper<Float[]>
	{
		public WrapperFloat(APartProperty<float[]> wrappedProp)
		{
			super(wrappedProp);
		}

		@Override
		public Float[] get()
		{
			return ArrayUtils.toObject((float[])wrappedProp.get());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Float[])
			{
				return wrappedProp.set(ArrayUtils.toPrimitive((Float[])property));
			}
			else if (property instanceof float[])
			{
				return wrappedProp.set(property);
			}
			return super.set(property);
		}
	}

	public static class WrapperInteger extends PropWrapper<Integer[]>
	{
		public WrapperInteger(APartProperty<int[]> wrappedProp)
		{
			super(wrappedProp);
		}

		@Override
		public Integer[] get()
		{
			return ArrayUtils.toObject((int[])wrappedProp.get());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Integer[])
			{
				return wrappedProp.set(ArrayUtils.toPrimitive((Integer[])property));
			}
			else if (property instanceof int[])
			{
				return wrappedProp.set(property);
			}
			return super.set(property);
		}
	}

	public static class WrapperLong extends PropWrapper<Long[]>
	{
		public WrapperLong(APartProperty<long[]> wrappedProp)
		{
			super(wrappedProp);
		}

		@Override
		public Long[] get()
		{
			return ArrayUtils.toObject((long[])wrappedProp.get());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Long[])
			{
				return wrappedProp.set(ArrayUtils.toPrimitive((Long[])property));
			}
			else if (property instanceof long[])
			{
				return wrappedProp.set(property);
			}
			return super.set(property);
		}
	}

	public static class WrapperShort extends PropWrapper<Short[]>
	{
		public WrapperShort(APartProperty<short[]> wrappedProp)
		{
			super(wrappedProp);
		}

		@Override
		public Short[] get()
		{
			return ArrayUtils.toObject((short[])wrappedProp.get());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Short[])
			{
				return wrappedProp.set(ArrayUtils.toPrimitive((Short[])property));
			}
			else if (property instanceof short[])
			{
				return wrappedProp.set(property);
			}
			return super.set(property);
		}
	}
}