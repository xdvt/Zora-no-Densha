package zoranodensha.client.gui.table.buttons;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import zoranodensha.client.gui.table.GUIEngineerTable;



@SideOnly(Side.CLIENT)
public class ButtonEngineerTable extends GuiButton
{
	public ButtonEngineerTable(int id, int xPos, int yPos, String displayString)
	{
		super(id, xPos, yPos, 10, 10, displayString);
	}

	@Override
	public void drawButton(Minecraft mc, int x, int y)
	{
		if (visible)
		{
			mc.getTextureManager().bindTexture(GUIEngineerTable.texture);

			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glEnable(GL11.GL_BLEND);
			OpenGlHelper.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);

			field_146123_n = (x >= xPosition && y >= yPosition && x < (xPosition + width) && y < (yPosition + height));
			drawTexturedModalRect(xPosition, yPosition, (field_146123_n ? 152 : 108), 29, width, height);
			mouseDragged(mc, x, y);
			drawCenteredString(	mc.fontRenderer, displayString, xPosition + (width / 2), yPosition + ((height - 8) / 2),
								(packedFGColour != 0 ? packedFGColour : !enabled ? 10526880 : field_146123_n ? 16777120 : 14737632));
		}
	}
}
