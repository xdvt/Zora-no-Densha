package zoranodensha.client.gui.table.buttons;

import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.MathHelper;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public abstract class ButtonPopUpYesNo implements IButtonPopUp
{
	protected int xPos;
	protected int yPos;
	protected int width;
	protected int height;
	protected String displayString;
	protected GuiButton buttonNo;
	protected GuiButton buttonYes;



	public ButtonPopUpYesNo(int id, int xPos, int yPos, EButtonDragDownType type)
	{
		this(id, xPos, yPos, I18n.format(ModData.ID + ".text.guiEngineer." + type.toString()));
	}

	public ButtonPopUpYesNo(int id, int xPos, int yPos, String displayString)
	{
		this.xPos = xPos;
		this.yPos = yPos;
		width = 120;
		height = 40;
		this.displayString = displayString;
		initButtons(id);
	}

	@Override
	public void actionPerformed(GUIEngineerTable gui, GuiButton button)
	{
		if (buttonNo != null && button.id == buttonNo.id)
		{
			actionPerformedNo(gui, button);
		}
		else if (buttonYes != null && button.id == buttonYes.id)
		{
			actionPerformedYes(gui, button);
		}
	}

	/**
	 * Called when the player clicks the negative ("No") button.
	 */
	protected void actionPerformedNo(GUIEngineerTable gui, GuiButton button)
	{
		gui.buttonPopUp = null;
		gui.initGui();
	}

	/**
	 * Called when the player clicks the affirmative ("Yes") button.
	 */
	protected abstract void actionPerformedYes(GUIEngineerTable gui, GuiButton button);

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addButtons(List buttonList, int index)
	{
		initButtons(index);

		buttonList.add(buttonNo);
		buttonList.add(buttonYes);
	}

	@Override
	public void draw(int mouseX, int mouseY, GUIEngineerTable gui)
	{
		gui.mc.getTextureManager().bindTexture(GUIEngineerTable.texture);
		gui.drawTexturedModalRect(xPos, yPos, 0, 0, width / 2, 5);
		gui.drawTexturedModalRect(xPos + (width / 2), yPos, 176 - (width / 2), 0, width / 2, 5);

		int i = 0;

		for (int j = height - 10; i < j; i += 10)
		{
			gui.drawTexturedModalRect(xPos, yPos + 5 + i, 0, 218, width, (j - i < 10) ? j - i : 10);
		}

		gui.drawTexturedModalRect(xPos, yPos + i + 5, 0, 213, width / 2, 5);
		gui.drawTexturedModalRect(xPos + (width / 2), yPos + i + 5, 176 - (width / 2), 213, width / 2, 5);

		buttonNo.drawButton(gui.mc, mouseX, mouseY);
		buttonYes.drawButton(gui.mc, mouseX, mouseY);

		if (gui.mc.fontRenderer.getStringWidth(displayString) > width - 10)
		{
			float scale = (float)(width - 10) / (float)gui.mc.fontRenderer.getStringWidth(displayString);

			GL11.glPushMatrix();
			GL11.glScalef(scale, 1.0F, 1.0F);
			gui.drawCenteredString(gui.mc.fontRenderer, displayString + "?", MathHelper.ceiling_float_int((xPos + (width / 2)) / scale), yPos + 5, 14737632);
			GL11.glPopMatrix();
		}
		else
		{
			gui.drawCenteredString(gui.mc.fontRenderer, displayString + "?", xPos + (width / 2), yPos + 5, 14737632);
		}
	}

	private void initButtons(int id)
	{
		buttonNo = new GuiButton(id, xPos + 24, yPos + height - 25, 30, 20, I18n.format(ModData.ID + ".text.general02"));
		buttonYes = new GuiButton(id + 1, xPos + width - 54, yPos + height - 25, 30, 20, I18n.format(ModData.ID + ".text.general03"));
	}

	@Override
	public void keyTyped(char c, int i)
	{
	}

	@Override
	public void mouseClicked(int x, int y, int button)
	{
	}
}
