package zoranodensha.client.gui.table.buttons;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;



/**
 * An enum holding all key bindings used by Zora no Densha.
 */
@SideOnly(Side.CLIENT)
public enum EModelEditorKeys
{
	/* Model Editor Preferences */
	KeyC("copy", Keyboard.KEY_C),
	KeyG("move", Keyboard.KEY_G),
	KeyR("rotate", Keyboard.KEY_R),
	KeyS("scale", Keyboard.KEY_S),
	KeyV("paste", Keyboard.KEY_V),
	KeyX("x", Keyboard.KEY_X),
	KeyY("y", Keyboard.KEY_Y),
	KeyZ("z", Keyboard.KEY_Z);

	private int key;
	public final String desc;
	public final int keyDefault;



	private EModelEditorKeys(String desc, int key)
	{
		this.desc = desc;
		this.key = keyDefault = key;
	}

	/**
	 * Returns {@code true} if this binding's key is pressed.
	 */
	public boolean isPressed()
	{
		return Keyboard.isKeyDown(key);
	}

	/**
	 * Returns {@code true} if this binding's key code matched the given key.<br>
	 * Does not actually check whether the key is pressed.
	 */
	public boolean isPressed(int keyCode)
	{
		return (key == keyCode);
	}

	/**
	 * Reset the key index.
	 */
	public void resetKey()
	{
		key = keyDefault;
	}

	/**
	 * Update the assigned key index.
	 */
	public void setKey(int key)
	{
		this.key = key;
	}
}
