package zoranodensha.client.gui.table.buttons;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;



@SideOnly(Side.CLIENT)
public enum EButtonDragDownType
{
	/** Create a new item of the selected type and add it to the selection. */
	ITEM_ADD_TO_MODEL("addToModel"),

	/** Remove all items of the given type from the selection. */
	ITEM_DESELECT_ALL("deselectAll"),

	/** Add all items of the given type to the selection. */
	ITEM_SELECT_ALL("selectAll"),

	/** Clear the model. */
	MENU_CLEAR_MODEL("clearModel"),

	/** Save the model as Preset. */
	MENU_SAVE_AS_PRESET("savePreset"),

	/** Toggle the grid displayed on the ground on XZ-axis. */
	MENU_TOGGLE_GRID("toggleGrid"),

	/** Toggle transformation mode. */
	MENU_TOGGLE_MODE("toggleMode"),

	/** Toggle orthographic or perspective view. */
	MENU_TOGGLE_ORTHO("toggleOrtho"),

	/** Toggle the pivot's size. */
	MENU_TOGGLE_PIVOT("togglePivot"),

	/** Toggle a dummy world to be displayed. Overrides {@code MENU_TOGGLE_PIVOT} and {@code MENU_TOGGLE_GRID}. */
	MENU_TOGGLE_WORLD("toggleWorld"),

	/** Create a Blueprint of the Preset. */
	PRESET_CREATE_BLUEPRINT("createBlueprint"),

	/** Export the selected Preset as .zndpreset file. */
	PRESET_EXPORT("exportPreset"),

	/** Move the Preset to the beginning of the list, displaying it on top of the table. */
	PRESET_MOVE_TO_BEGIN("presetMove"),

	/** Load the selected Preset. */
	PRESET_LOAD("presetLoad"),

	/** Remove the selected Preset. */
	PRESET_REMOVE("presetRemove"),

	/** Remov the selected Preset from the file directory. */
	PRESET_REMOVE_FILE("presetRemFile");

	private String name;



	private EButtonDragDownType(String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return name;
	}
}
