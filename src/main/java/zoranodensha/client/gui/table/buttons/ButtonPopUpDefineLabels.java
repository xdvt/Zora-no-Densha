package zoranodensha.client.gui.table.buttons;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.UVID;
import zoranodensha.api.vehicles.UVID.EIdentifier;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.network.packet.gui.PacketGUIEngineerTablePreset;
import zoranodensha.common.util.Helpers;



@SideOnly(Side.CLIENT)
public class ButtonPopUpDefineLabels implements IButtonPopUp
{
	private static final String TEXTFIELD_DEFAULT = EIdentifier.LABEL.defValue + " " + EIdentifier.NUMBER.defValue;
	private static final String INPUT_REGEX = "[A-Z]{3,5}+\\s??\\d{3}-\\d";

	private int xPos;
	private int yPos;
	private int width;

	private ItemStack itemStack;
	private GuiTextField input;
	private GuiButton button;



	public ButtonPopUpDefineLabels(int xPos, int yPos, ItemStack itemStack)
	{
		this.itemStack = itemStack;
		this.xPos = xPos;
		this.yPos = yPos;
		width = 120;
	}

	@Override
	public void actionPerformed(GUIEngineerTable gui, GuiButton button)
	{
		if (this.button != null && button.id == this.button.id)
		{
			/*
			 * Check input format.
			 */
			String s = input.getText().toUpperCase();
			if (!s.matches(INPUT_REGEX))
			{
				return;
			}

			String[] data = s.split(" ");
			try
			{
				NBTTagCompound nbt = itemStack.getTagCompound();
				NBTTagList tagList = nbt.getTagList(Train.EDataKey.VEHICLE_DATA.key, 10);
				NBTTagCompound nbt0 = tagList.getCompoundTagAt(0);

				if (Helpers.isUndefined(nbt0.getString(VehicleData.EDataKey.NAME.key)))
				{
					throw new IllegalArgumentException("Supplied vehicle's name is either null or empty.");
				}
				else if (nbt0.getTagList(VehicleData.EDataKey.PARTS.key, 10).tagCount() == 0)
				{
					throw new IllegalArgumentException("Supplied vehicle has no parts.");
				}
				else
				{
					Train train = new Train(Minecraft.getMinecraft().theWorld);
					train.readEntityFromNBT(nbt, ETagCall.ITEM);

					VehicleData vehicle = train.getVehicleAt(0);
					vehicle = new VehicleData(new UVID(data[0], data[1]), vehicle.getParts(OrderedPartsList.SELECTOR_ANY), vehicle.getCreatorName(), vehicle.getVehicleName());

					/* Clear train's vehicle list. */
					while (train.getVehicleCount() > 0)
					{
						train.removeVehicle(train.getVehicleAt(0));
					}

					train.addVehicle(vehicle);
					train.writeEntityToNBT(nbt = new NBTTagCompound(), ETagCall.ITEM);

					ModCenter.snw.sendToServer(new PacketGUIEngineerTablePreset(nbt, (byte)3));
				}

				gui.buttonPopUp = null;
				gui.initGui();
			}
			catch (Exception e)
			{
				ModCenter.log.warn("[EngineerTableModelEditor] Error while trying to create Blueprint", e);
			}
		}
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addButtons(List buttonList, int index)
	{
		input = new GuiTextField(Minecraft.getMinecraft().fontRenderer, xPos + 5, yPos + 8, 85, 16);
		input.setFocused(false);
		input.setText(TEXTFIELD_DEFAULT);
		input.setTextColor(7368816);

		buttonList.add(button = new GuiButton(index, xPos + 95, yPos + 6, 20, 20, "Ok"));
	}

	@Override
	public void draw(int mouseX, int mouseY, GUIEngineerTable gui)
	{
		gui.mc.getTextureManager().bindTexture(GUIEngineerTable.texture);
		gui.drawTexturedModalRect(xPos, yPos, 0, 0, width / 2, 5);
		gui.drawTexturedModalRect(xPos + (width / 2), yPos, 176 - (width / 2), 0, width / 2, 5);
		gui.drawTexturedModalRect(xPos, yPos + 5, 0, 218, width, 22);
		gui.drawTexturedModalRect(xPos, yPos + 27, 0, 213, width / 2, 5);
		gui.drawTexturedModalRect(xPos + (width / 2), yPos + 27, 176 - (width / 2), 213, width / 2, 5);

		button.drawButton(gui.mc, mouseX, mouseY);
		input.drawTextBox();
	}

	@Override
	public void keyTyped(char c, int i)
	{
		if (input.isFocused())
		{
			input.textboxKeyTyped(c, i);
		}
	}

	@Override
	public void mouseClicked(int x, int y, int button)
	{
		input.mouseClicked(x, y, button);

		if (input.isFocused())
		{
			if (TEXTFIELD_DEFAULT.equals(input.getText()))
			{
				input.setText("");
				input.setTextColor(14737632);
			}
		}
		else if ("".equals(input.getText()))
		{
			input.setText(TEXTFIELD_DEFAULT);
			input.setTextColor(7368816);
		}
	}
}
