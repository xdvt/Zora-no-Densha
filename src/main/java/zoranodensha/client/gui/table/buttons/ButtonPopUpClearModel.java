package zoranodensha.client.gui.table.buttons;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import zoranodensha.client.gui.table.GUIEngineerTable;



@SideOnly(Side.CLIENT)
public class ButtonPopUpClearModel extends ButtonPopUpYesNo
{
	public ButtonPopUpClearModel(int id, int xPos, int yPos, GUIEngineerTable parentGUI)
	{
		super(id, xPos, yPos, EButtonDragDownType.MENU_CLEAR_MODEL);
	}

	@Override
	protected void actionPerformedYes(GUIEngineerTable gui, GuiButton button)
	{
		gui.clearModel();
		gui.buttonPopUp = null;
		gui.initGui();
	}
}
