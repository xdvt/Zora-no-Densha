package zoranodensha.client.gui.table.buttons;

import java.io.File;
import java.util.Iterator;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.client.ModProxyClient;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.common.blocks.tileEntity.TileEntityEngineerTable;
import zoranodensha.common.containers.ContainerEngineerTable;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.registry.ModPartRegistry;
import zoranodensha.common.util.Helpers;



@SideOnly(Side.CLIENT)
public class ButtonPopUpRemoveFile extends ButtonPopUpYesNo
{
	private final ItemStack itemStack;



	public ButtonPopUpRemoveFile(int id, int xPos, int yPos, ItemStack itemStack)
	{
		super(id, xPos, yPos, EButtonDragDownType.PRESET_REMOVE_FILE);

		this.itemStack = itemStack;
	}

	@Override
	protected void actionPerformedYes(GUIEngineerTable gui, GuiButton button)
	{
		NBTTagCompound nbt = itemStack.getTagCompound();
		NBTTagList tagList = nbt.getTagList(Train.EDataKey.VEHICLE_DATA.key, 10);
		nbt = tagList.getCompoundTagAt(0);
		String name = nbt.getString(VehicleData.EDataKey.NAME.key);

		if (!Helpers.isUndefined(name))
		{
			Iterator<File> iteraFiles = Helpers.parseFor(ModPartRegistry.PRESET_SUFFIX, ModCenter.DIR_ADDONS, 5).iterator();
			boolean mayContinue = false;

			while (iteraFiles.hasNext())
			{
				File file = iteraFiles.next();

				if ((name + ModPartRegistry.PRESET_SUFFIX).equalsIgnoreCase(file.getName()))
				{
					file.delete();
					mayContinue = true;
					break;
				}
			}

			if (mayContinue)
			{
				Iterator<ItemStack> iteraItems = TileEntityEngineerTable.presetsImport.iterator();

				while (iteraItems.hasNext())
				{
					nbt = iteraItems.next().getTagCompound();
					if (nbt != null)
					{
						tagList = nbt.getTagList(Train.EDataKey.VEHICLE_DATA.key, 10);
						nbt = tagList.getCompoundTagAt(0);
						
						if (name.equalsIgnoreCase(nbt.getString(VehicleData.EDataKey.NAME.key)))
						{
							iteraItems.remove();
							break;
						}
					}
				}
			}
			else
			{
				ModCenter.log.warn("[ModProxy] Couldn't fully delete Preset file '" + name + "'.");
			}
		}
		else
		{
			ModCenter.log.warn("[ModProxy] Couldn't delete Preset file; Name was null or empty.");
		}

		gui.buttonPopUp = null;
		gui.initGui();

		ModProxyClient.parsePresets();

		Container container = Minecraft.getMinecraft().thePlayer.openContainer;

		if (container instanceof ContainerEngineerTable)
		{
			((ContainerEngineerTable)container).addSlotsToContainer();
		}
	}
}
