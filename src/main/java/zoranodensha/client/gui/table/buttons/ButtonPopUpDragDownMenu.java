package zoranodensha.client.gui.table.buttons;

import java.util.Iterator;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.AVehiclePartRegistry;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.model.TrainModelRender;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.registry.ModPartRegistry;
import zoranodensha.common.network.packet.gui.PacketGUIEngineerTablePreset;



@SideOnly(Side.CLIENT)
public class ButtonPopUpDragDownMenu implements IButtonPopUp
{
	private final ItemStack itemStack;
	private final ButtonDragDown[] buttons;
	private final int xPos;
	private final int yPos;



	public ButtonPopUpDragDownMenu(ItemStack itemStack, int firstID, int xPos, int yPos, EButtonDragDownType... options)
	{
		this.itemStack = itemStack;
		buttons = new ButtonDragDown[options.length];
		this.xPos = xPos;
		this.yPos = yPos;

		for (int i = 0; i < buttons.length; ++i)
		{
			buttons[i] = new ButtonDragDown(firstID + i, xPos + 6, yPos + (i * 12) + 6, options[i]);
		}
	}

	@Override
	public void actionPerformed(GUIEngineerTable gui, GuiButton guiButton)
	{
		VehParBase part;
		VehParBase type;
		Iterator<VehParBase> itera;
		ButtonDragDown button;

		if (guiButton instanceof ButtonDragDown)
		{
			button = (ButtonDragDown)guiButton;
		}
		else
		{
			return;
		}

		TrainModelRender render = gui.getRender();

		switch (button.type)
		{
			case ITEM_ADD_TO_MODEL:
				part = AVehiclePartRegistry.getPartFromItemStack(itemStack, true);
				if (part != null && render.canAddParts(1))
				{
					render.pushUndo();
					render.model.addPart(part);
					render.pushUndo();
					render.selectionAdd(part, null, false);
				}
				break;

			case ITEM_DESELECT_ALL:
				render.selectionClearObjTrans();
				itera = render.model.selection.iterator();
				type = AVehiclePartRegistry.getPartFromItemStack(itemStack);

				if (type != null)
				{
					render.pushUndo();

					while (itera.hasNext())
					{
						part = itera.next();

						if (part.getName().equals(type.getName()))
						{
							render.selectionClear(part, itera);
						}
					}
				}
				break;

			case ITEM_SELECT_ALL:
				if (render.model != null)
				{
					render.pushUndo();
					render.selectionClear(true);
					itera = render.model.getVehicleParts().iterator();
					type = AVehiclePartRegistry.getPartFromItemStack(itemStack);

					if (type != null)
					{
						while (itera.hasNext())
						{
							part = itera.next();

							if (part.getName().equals(type.getName()))
							{
								render.selectionAdd(part, itera, false);
							}
						}
					}
				}
				break;

			case MENU_CLEAR_MODEL:
				gui.buttonPopUp = new ButtonPopUpClearModel(2, (gui.width / 2) - 45, (gui.height / 2) - 16, gui);
				gui.initGui();
				break;

			case MENU_SAVE_AS_PRESET:
				gui.buttonPopUp = new ButtonPopUpSaveName((gui.width / 2) - 45, (gui.height / 2) - 16);
				gui.initGui();
				break;

			case MENU_TOGGLE_PIVOT:
				render.togglePivot();
				break;

			case MENU_TOGGLE_GRID:
				render.toggleGrid();
				break;

			case MENU_TOGGLE_MODE:
				render.toggleMode();
				break;

			case MENU_TOGGLE_ORTHO:
				render.camera.setOrtho();
				break;

			case MENU_TOGGLE_WORLD:
				render.toggleWorld();
				break;

			case PRESET_CREATE_BLUEPRINT:
				gui.buttonPopUp = new ButtonPopUpDefineLabels((gui.width / 2) - 45, (gui.height / 2) - 16, itemStack);
				gui.initGui();
				break;

			case PRESET_EXPORT:
				if (!ModPartRegistry.presetExport(itemStack.getTagCompound(), gui, false))
				{
					gui.buttonPopUp = null;
				}
				break;

			case PRESET_LOAD:
				if (render.getChangesMade())
				{
					gui.buttonPopUp = new ButtonPopUpSaveLoad(2, (gui.width / 2) - 45, (gui.height / 2) - 16, itemStack);
					gui.initGui();
				}
				else
				{
					gui.clearModel();
					render.loadModelFromNBT(itemStack.getTagCompound());
					gui.buttonPopUp = null;
					gui.initGui();
				}
				break;

			case PRESET_MOVE_TO_BEGIN:
				ModCenter.snw.sendToServer(new PacketGUIEngineerTablePreset(itemStack.getTagCompound(), (byte)2));
				gui.buttonPopUp = null;
				break;

			case PRESET_REMOVE:
				gui.buttonPopUp = new ButtonPopUpRemove(2, (gui.width / 2) - 45, (gui.height / 2) - 16, itemStack);
				gui.initGui();
				break;

			case PRESET_REMOVE_FILE:
				gui.buttonPopUp = new ButtonPopUpRemoveFile(2, (gui.width / 2) - 45, (gui.height / 2) - 16, itemStack);
				gui.initGui();
				break;
		}
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addButtons(List buttonList, int index)
	{
		for (int i = 0; i < buttons.length; ++i)
		{
			buttonList.add(buttons[i]);
		}
	}

	@Override
	public void draw(int mouseX, int mouseY, GUIEngineerTable gui)
	{
		if (isMouseWhithin(mouseX, mouseY))
		{
			for (int i = 0; i < buttons.length; ++i)
			{
				buttons[i].drawButton0(gui.mc, mouseX, mouseY);
			}
		}
		else
		{
			gui.buttonPopUp = null;
			gui.initGui();
		}
	}

	/**
	 * Returns true if the mouse is still close to or within this DDM instance.
	 */
	public boolean isMouseWhithin(int mouseX, int mouseY)
	{
		return !(mouseX < xPos || mouseY < yPos - 10 || mouseX >= xPos + 90 || mouseY >= yPos + (buttons.length * 12) + 10);
	}

	@Override
	public void keyTyped(char c, int i)
	{
	}

	@Override
	public void mouseClicked(int x, int y, int button)
	{
	}
}