package zoranodensha.client.gui.table.buttons;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.network.packet.gui.PacketGUIEngineerTablePreset;



public class ButtonPopUpRemove extends ButtonPopUpYesNo
{
	private final ItemStack itemStack;



	public ButtonPopUpRemove(int id, int xPos, int yPos, ItemStack itemStack)
	{
		super(id, xPos, yPos, EButtonDragDownType.PRESET_REMOVE);

		this.itemStack = itemStack;
	}

	@Override
	protected void actionPerformedYes(GUIEngineerTable gui, GuiButton button)
	{
		ModCenter.snw.sendToServer(new PacketGUIEngineerTablePreset(itemStack.getTagCompound(), (byte)0));
		gui.buttonPopUp = null;
		gui.initGui();
	}
}
