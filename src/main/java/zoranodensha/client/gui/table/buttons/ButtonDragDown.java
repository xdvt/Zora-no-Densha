package zoranodensha.client.gui.table.buttons;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.MathHelper;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class ButtonDragDown extends GuiButton
{
	public final EButtonDragDownType type;



	public ButtonDragDown(int id, int xPos, int yPos, EButtonDragDownType type)
	{
		super(id, xPos, yPos, 80, 12, I18n.format(ModData.ID + ".text.guiEngineer." + type.toString()));

		this.type = type;
	}

	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY)
	{
	}

	public void drawButton0(Minecraft mc, int mouseX, int mouseY)
	{
		if (!visible)
		{
			return;
		}

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(GUIEngineerTable.texture);

		field_146123_n = !(mouseX < xPosition || mouseY < yPosition || mouseX >= xPosition + width || mouseY >= yPosition + height);
		int state = getHoverState(field_146123_n) < 2 ? 0 : 1;

		GL11.glEnable(GL11.GL_BLEND);
		OpenGlHelper.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);

		drawTexturedModalRect(xPosition, yPosition, 205, 50 + (state * height), width / 2, height);
		drawTexturedModalRect(xPosition + (width / 2), yPosition, 256 - (width / 2), 50 + (state * height), width / 2, height);
		mouseDragged(mc, mouseX, mouseY);

		int l = 14737632;

		if (packedFGColour != 0)
		{
			l = packedFGColour;
		}
		else if (!enabled)
		{
			l = 10526880;
		}
		else if (field_146123_n)
		{
			l = 16777120;
		}

		if (mc.fontRenderer.getStringWidth(displayString) > width - 4)
		{
			float scale = (float)(width - 4) / (float)mc.fontRenderer.getStringWidth(displayString);

			GL11.glPushMatrix();
			GL11.glScalef(scale, 1.0F, 1.0F);
			drawCenteredString(mc.fontRenderer, displayString, MathHelper.ceiling_float_int((xPosition + width / 2) / scale), yPosition + (height - 8) / 2, l);
			GL11.glPopMatrix();
		}
		else
		{
			drawCenteredString(mc.fontRenderer, displayString, xPosition + width / 2, yPosition + (height - 8) / 2, l);
		}
	}
}
