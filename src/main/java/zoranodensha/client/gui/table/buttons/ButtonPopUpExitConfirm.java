package zoranodensha.client.gui.table.buttons;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.I18n;
import zoranodensha.api.structures.EEngineerTableTab;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.network.packet.gui.PacketGUIEngineerTable;



@SideOnly(Side.CLIENT)
public class ButtonPopUpExitConfirm extends ButtonPopUpYesNo
{
	private final EEngineerTableTab newTab;
	private GUIEngineerTable guiRef;



	public ButtonPopUpExitConfirm(int id, int xPos, int yPos, EEngineerTableTab newTab)
	{
		super(id, xPos, yPos, I18n.format(ModData.ID + ".text.guiEngineer.exitConfirm"));

		this.newTab = newTab;
	}

	@Override
	protected void actionPerformedYes(GUIEngineerTable gui, GuiButton button)
	{
		if (newTab == null)
		{
			guiRef = gui;
		}
		else
		{
			ModCenter.snw.sendToServer(new PacketGUIEngineerTable((byte)newTab.toInt()));
			gui.setTab(newTab);
			gui.initGui();
		}
	}

	@Override
	public void mouseClicked(int x, int y, int button)
	{
		if (guiRef != null)
		{
			guiRef.buttonPopUp = null;
			guiRef.initGui();
			guiRef.mc.thePlayer.closeScreen();
		}
	}
}
