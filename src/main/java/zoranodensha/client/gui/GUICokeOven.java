package zoranodensha.client.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidTank;
import zoranodensha.common.blocks.tileEntity.TileEntityCokeOven;
import zoranodensha.common.containers.ContainerCokeOven;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class GUICokeOven extends GuiContainer
{
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/guis/cokeOvenGui.png");
	private TileEntityCokeOven tileEntity;



	public GUICokeOven(InventoryPlayer inventory, TileEntityCokeOven tileEntity)
	{
		super(new ContainerCokeOven(inventory, tileEntity));

		this.tileEntity = tileEntity;
		xSize = 176;
		ySize = 166;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY)
	{
		int xPos = (width - xSize) / 2;
		int yPos = (height - ySize) / 2;
		int i2 = tileEntity.getBurnTimeScaled();

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		drawTexturedModalRect(xPos, yPos, 0, 0, xSize, ySize);

		if (tileEntity.data[0] > 0)
		{
			drawTexturedModalRect(xPos + 27, yPos + 58 - i2, 176, 55 - i2, 14, i2 + 1);
		}
		
		FluidTank fluidTank = tileEntity.getTank();
		if (fluidTank != null && fluidTank.getFluid() != null)
		{
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			mc.getTextureManager().bindTexture(TextureMap.locationBlocksTexture);
			i2 = fluidTank.getFluidAmount() / 340;
			IIcon icon = fluidTank.getFluid().getFluid().getIcon();
			Tessellator tessellator = Tessellator.instance;

			for (int i = 0; i < i2; i += 16)
			{
				int i3 = MathHelper.clamp_int((i2 - i), 0, 16);

				tessellator.startDrawingQuads();
				tessellator.addVertexWithUV(xPos + 108, yPos + 66 - i, 0, icon.getMinU(), icon.getInterpolatedV(0));
				tessellator.addVertexWithUV(xPos + 124, yPos + 66 - i, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(0));
				tessellator.addVertexWithUV(xPos + 124, yPos + 66 - i - i3, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(i3));
				tessellator.addVertexWithUV(xPos + 108, yPos + 66 - i - i3, 0, icon.getMinU(), icon.getInterpolatedV(i3));
				tessellator.draw();
			}
		}

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		drawTexturedModalRect(xPos + 108, yPos + 21, 176, 0, 6, 43);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		String s = I18n.format(tileEntity.hasCustomInventoryName() ? tileEntity.getInventoryName() : I18n.format(tileEntity.getInventoryName()));
		fontRendererObj.drawString(s, (xSize / 2) - (fontRendererObj.getStringWidth(s) / 2), 6, 4210752);
		fontRendererObj.drawString(I18n.format("container.inventory"), 8, ySize - 94, 4210752);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float f)
	{
		super.drawScreen(mouseX, mouseY, f);

		int xPos = (width - xSize) / 2;
		int yPos = (height - ySize) / 2;

		FluidTank fluidTank = tileEntity.getTank();
		if (mouseX > xPos + 106 && mouseX < xPos + 125 && mouseY > yPos + 17 && mouseY < yPos + 67)
		{
			List<String> listInfo = new ArrayList<String>();
			if (fluidTank.getFluid() != null) listInfo.add(fluidTank.getFluid().getFluid().getLocalizedName(fluidTank.getFluid()));
			listInfo.add("§7" + fluidTank.getFluidAmount() + " / " + fluidTank.getCapacity() + " mB");

			drawHoveringText(listInfo, mouseX, mouseY, fontRendererObj);
		}
	}
}
