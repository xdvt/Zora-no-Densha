package zoranodensha.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;



public class GUITooltip extends Gui
{
	public GUITooltip(Minecraft minecraft)
	{
		ScaledResolution resolution = new ScaledResolution(minecraft, minecraft.displayWidth, minecraft.displayHeight);
		
		int width = resolution.getScaledWidth();
		int height = resolution.getScaledHeight();
		
		drawString(minecraft.fontRenderer, "Click me!", (width / 2) + 4, (height / 2) + 4, 0xFFFF00);
	}
}
