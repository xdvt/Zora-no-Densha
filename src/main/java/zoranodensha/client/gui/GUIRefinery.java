package zoranodensha.client.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.IFluidTank;
import zoranodensha.common.blocks.tileEntity.TileEntityRefinery;
import zoranodensha.common.containers.ContainerRefinery;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class GUIRefinery extends GuiContainer
{
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/guis/refineryGui.png");
	private TileEntityRefinery tileEntity;



	public GUIRefinery(InventoryPlayer inventory, TileEntityRefinery tileEntity)
	{
		super(new ContainerRefinery(inventory, tileEntity));

		this.tileEntity = tileEntity;
		xSize = 176;
		ySize = 218;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY)
	{
		int xPos = (width - xSize) / 2;
		int yPos = (height - ySize) / 2;
		int i0 = tileEntity.getBurnTimeScaled();

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		drawTexturedModalRect(xPos, yPos, 0, 0, xSize, ySize);

		if (tileEntity.data[0] > 0)
		{
			drawTexturedModalRect(xPos + 37, yPos + 90 - i0, 176, 55 - i0, 14, i0 + 1);

			i0 = tileEntity.getProcessScaled();

			if (i0 > 0)
			{
				drawTexturedModalRect(xPos + 34, yPos + 73 - i0, 176, 104 - i0, 20, i0 + 1);
			}
		}

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(TextureMap.locationBlocksTexture);
		IFluidTank tank;
		int i1 = 0;

		while ((tank = tileEntity.getTank(i1)) != null)
		{
			if (tank.getFluid() != null)
			{
				i0 = tank.getFluidAmount() / 340;
				IIcon icon = tank.getFluid().getFluid().getIcon();
				Tessellator tessellator = Tessellator.instance;

				for (int i2 = 0; i2 < i0; i2 += 16)
				{
					int i3 = MathHelper.clamp_int((i0 - i2), 0, 16);
					int i4 = (i1 == 0 ? 8 : 42 + (i1 * 22));
					int i5 = 92;

					tessellator.startDrawingQuads();
					tessellator.addVertexWithUV(xPos + i4, yPos + i5 - i2, 0, icon.getMinU(), icon.getInterpolatedV(0));
					tessellator.addVertexWithUV(xPos + i4 + 16, yPos + i5 - i2, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(0));
					tessellator.addVertexWithUV(xPos + i4 + 16, yPos + i5 - i2 - i3, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(i3));
					tessellator.addVertexWithUV(xPos + i4, yPos + i5 - i2 - i3, 0, icon.getMinU(), icon.getInterpolatedV(i3));
					tessellator.draw();
				}
			}

			++i1;
		}

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);

		for (int i2 = 0; i2 < i1; ++i2)
		{
			drawTexturedModalRect(xPos + (i2 == 0 ? 8 : 42 + (i2 * 22)), yPos + 47, 176, 0, 6, 43);
		}
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		String s = I18n.format(tileEntity.hasCustomInventoryName() ? tileEntity.getInventoryName() : I18n.format(tileEntity.getInventoryName()));
		fontRendererObj.drawString(s, (xSize / 2) - (fontRendererObj.getStringWidth(s) / 2), 6, 4210752);
		fontRendererObj.drawString(I18n.format("container.inventory"), 8, ySize - 94, 4210752);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float f)
	{
		super.drawScreen(mouseX, mouseY, f);

		int xPos = (width - xSize) / 2;
		int yPos = (height - ySize) / 2;

		if (mouseX > xPos + 37 && mouseX < xPos + 53 && mouseY > yPos + 78 && mouseY < yPos + 94)
		{
			int i;
			int j = tileEntity.getTemperature();
			String s = "§7";

			if (tileEntity.getTankResults() != null)
			{
				i = tileEntity.getTankResults()[1];

				if (j > i - 2 && j < i + 2)
				{
					s = "§2";
				}
				else if (j >= i + 2)
				{
					s = "§3";
				}
				else if (j <= i - 2)
				{
					if (j >= (i - (i / 4)))
					{
						s = "§a";
					}
					else if (j >= i / 2)
					{
						s = "§b";
					}
				}
			}

			List<String> listInfo = new ArrayList<String>();
			listInfo.add(s + j + " °C");

			drawHoveringText(listInfo, mouseX, mouseY, fontRendererObj);
		}
		else
		{
			for (int i = 0; i < 7; ++i)
			{
				int xOff = (i == 0 ? 8 : 42 + (i * 22));
				int yOff = 47;

				if (mouseX > xPos + xOff - 1 && mouseX < xPos + xOff + 16 && mouseY > yPos + yOff - 1 && mouseY < yPos + yOff + 48)
				{
					FluidTank tank = tileEntity.getTank(i);
					if (tank != null)
					{
						List<String> listInfo = new ArrayList<String>();
						if (tank.getFluid() != null) listInfo.add(tank.getFluid().getFluid().getLocalizedName(tank.getFluid()));
						listInfo.add("§7" + tank.getFluidAmount() + " / " + tank.getCapacity() + " mB");

						drawHoveringText(listInfo, mouseX, mouseY, fontRendererObj);
					}

					return;
				}
			}
		}
	}
}
