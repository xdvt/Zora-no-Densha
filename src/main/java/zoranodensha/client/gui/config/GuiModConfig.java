package zoranodensha.client.gui.config;

import static zoranodensha.common.core.cfg.ModConfig.langKey;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.client.config.GuiConfig;
import cpw.mods.fml.client.config.IConfigElement;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.common.config.ConfigElement;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.cfg.ModConfig;



@SideOnly(Side.CLIENT)
public class GuiModConfig extends GuiConfig
{
	public GuiModConfig(GuiScreen parent)
	{
		super(parent, getConfigElements(), ModData.ID, false, false, I18n.format(langKey));

		titleLine2 = ModConfig.config.getConfigFile().getAbsolutePath();
	}


	/**
	 * Helper method to populate a list that contains all configuration elements.
	 */
	@SuppressWarnings("rawtypes")
	private static List<IConfigElement> getConfigElements()
	{
		ArrayList<IConfigElement> list = new ArrayList<IConfigElement>();
		for (String name : ModConfig.config.getCategoryNames())
		{
			list.add(new ConfigElement(ModConfig.config.getCategory(name)));
		}
		return list;
	}
}
