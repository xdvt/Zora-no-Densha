package zoranodensha.client.gui.config;

import java.util.Set;

import cpw.mods.fml.client.IModGuiFactory;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;



@SideOnly(Side.CLIENT)
public class GuiFactory implements IModGuiFactory
{
	@Override
	public RuntimeOptionGuiHandler getHandlerFor(RuntimeOptionCategoryElement element)
	{
		return null;
	}

	@Override
	public void initialize(Minecraft minecraftInstance)
	{
		/* Left blank. */
	}

	@Override
	public Class<? extends GuiScreen> mainConfigGuiClass()
	{
		return GuiModConfig.class;
	}

	@Override
	public Set<RuntimeOptionCategoryElement> runtimeGuiCategories()
	{
		return null;
	}
}
