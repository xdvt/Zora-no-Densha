package zoranodensha.vehicleParts.common.sounds;

import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.vehicles.part.property.PropSound.PropSoundRepeated;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.common.parts.engine.AVehParEngineDiesel;



public class DieselEngineSounds extends APartSounds
{
	/*
	 * Final properties.
	 */
	private static final int SOUND_STEPS = 8;

	/** The engine this sound wrapper belongs to. */
	protected final AVehParEngineDiesel engine;

	/*
	 * Instance properties.
	 */
	/** Sound Properties */
	protected PropSoundMotor[] soundRPMe = new PropSoundMotor[SOUND_STEPS];
	protected PropSoundMotor[] soundRPMi = new PropSoundMotor[SOUND_STEPS];

	/** Sound Property Volumes */
	public float[] volumeRPM = new float[SOUND_STEPS];

	/** Sound Property Pitches */
	public float[] pitchRPM = new float[SOUND_STEPS];



	public DieselEngineSounds(AVehParEngineDiesel engine)
	{
		this.engine = engine;

		for (int i = 0; i < SOUND_STEPS; i++)
		{
			engine.addProperty(soundRPMe[i] = new PropSoundMotor(this, i, false));
			engine.addProperty(soundRPMi[i] = new PropSoundMotor(this, i, true));
		}
	}

	public void onUpdate()
	{
		float rpm = engine.type_engine.getCurrentRPM();

		for (int i = 0; i < SOUND_STEPS; i++)
		{
			volumeRPM[i] = updateVolume(i, rpm);
			pitchRPM[i] = updatePitch(i, rpm);
		}
	}

	private float updatePitch(int index, float RPM)
	{
		float pitch = 1.0F;
		float level = MathHelper.clamp_float(RPM / engine.type_engine.maxRPM.get(), 0.0F, 1.0F);

		pitch = 1.0F + (((level * 8) - index) * 0.15F);
		pitch = MathHelper.clamp_float(pitch, 0.0F, 2.0F);

		return pitch;
	}

	private float updateVolume(int index, float RPM)
	{
		float volume = 0.0F;
		float level = MathHelper.clamp_float(RPM / engine.type_engine.maxRPM.get(), 0.0F, 1.0F);

		volume = 1.0F - (Math.abs((level * (SOUND_STEPS - 1)) - index) * 0.75F);
		volume = MathHelper.clamp_float(volume, 0.0F, 1.0F);

		switch (index)
		{
			case 0:
			case 1: {
				if (RPM < engine.type_engine.idleRPM.get() / 2.0F && RPM >= 0.0F)
				{
					volume *= RPM / (engine.type_engine.idleRPM.get() / 2.0F);
				}

				break;
			}
		}

		return volume;
	}



	public static class PropSoundMotor extends PropSoundRepeated
	{
		protected final DieselEngineSounds wrapper;
		protected final int index;
		protected final Boolean interior;



		public PropSoundMotor(DieselEngineSounds wrapper, int index, Boolean interior)
		{
			super(wrapper.engine, String.format("%s:vehPar_engineDiesel_rpm%d" + (interior != null ? (interior ? "_int" : "_ext") : ""), ModData.ID, index), "motorSounds");

			this.wrapper = wrapper;
			this.index = index;
			this.interior = interior;
		}

		@Override
		public Integer get()
		{
			return getVolume() > 0.0F ? -1 : 0;
		}

		protected float getVolume()
		{
			if (interior != null && wrapper.engine.getTrain().worldObj.isRemote)
			{
				if (isPlayerInside(wrapper.engine.getTrain()))
				{
					if (!interior)
					{
						return 0.0F;
					}
				}
				else
				{
					if (interior)
					{
						return 0.0F;
					}
				}
			}

			if (index < wrapper.volumeRPM.length)
			{
				return wrapper.volumeRPM[index];
			}
			return 0.0F;
		}

		protected float getPitch()
		{
			if (index < wrapper.pitchRPM.length)
			{
				return wrapper.pitchRPM[index];
			}
			return 0.5F;
		}

		@Override
		protected BasicSound getNewSound(ResourceLocation resLoc)
		{
			return new BasicSound(resLoc, this)
			{
				@Override
				public float getVolume()
				{
					return ((PropSoundMotor)prop).getVolume();
				};

				@Override
				public float getPitch()
				{
					return ((PropSoundMotor)prop).getPitch();
				};
			}.setCanRepeat();
		}
	}
}
