package zoranodensha.vehicleParts.common.sounds;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import zoranodensha.api.vehicles.Train;



public abstract class APartSounds
{
	/** Sound volume threshold. Any sounds below this volume won't be played. */
	public static final float SOUND_THRESHOLD = 0.01F;



	/**
	 * <p>
	 * Provides a method of determining if the current client
	 * player is currently sitting inside the train associated with this bogie. If
	 * this method returns {@code true}, relevant interior sounds should be played
	 * whilst muting exterior-only sounds.
	 * </p>
	 * <p>
	 * If the player is in third-person mode, this method will return {@code false}.
	 * </p>
	 *
	 * @return {@code true} if this method is called on the client-side and the
	 *         client player is sitting inside this bogie's train. Otherwise,
	 *         {@code false}.
	 */
	@SideOnly(Side.CLIENT)
	public static boolean isPlayerInside(Train train)
	{
		if (train.worldObj.isRemote)
		{
			if (Minecraft.getMinecraft() != null)
			{
				if (Minecraft.getMinecraft().thePlayer != null)
				{
					if (Minecraft.getMinecraft().gameSettings != null && Minecraft.getMinecraft().gameSettings.thirdPersonView != 0)
					{
						return false;
					}
					else if (train.isPassenger(Minecraft.getMinecraft().thePlayer))
					{
						return true;
					}
				}
			}
		}

		return false;
	}

}
