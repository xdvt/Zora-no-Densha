package zoranodensha.vehicleParts.common.sounds;

import org.apache.logging.log4j.Level;

import net.minecraft.client.Minecraft;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.property.PropSound.BasicSound;
import zoranodensha.api.vehicles.part.property.PropSound.PropSoundRepeated;
import zoranodensha.api.vehicles.part.type.PartTypeEngineElectric;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.common.parts.bogie.AVehParBogieImpl;



/**
 * <h1>ElectricEngineSounds</h1>
 * <hr>
 * <p>
 * A class which holds and manages a group of sound effects related to a
 * particular vehicle part. In this case, an electric engine. This can either be
 * the battery of a locomotive or the bogies of an EMU.
 * </p>
 * 
 * @see BogieSounds
 * @see DieselEngineSounds
 * @author jaffa
 */
public class ElectricEngineSounds extends APartSounds
{
	/**
	 * The parent vehicle part. All sound properties will be added to this instance
	 * when this class is initialised.
	 */
	protected final AVehParBaseImpl engineVehiclePart;
	/**
	 * The parent vehicle part type. This wrapper class will refer to this instance
	 * when determining values such as motor activity, motor polarity, etc.
	 */
	protected final PartTypeEngineElectric enginePartType;

	/*
	 * Properties that will be added to the parent Vehicle Part
	 */
	protected PropSound prop_soundTraction0i;
	protected PropSound prop_soundTraction1i;
	protected PropSound prop_soundTraction2i;
	protected PropSound prop_soundInverter0i;
	protected PropSound prop_soundInverter1i;
	protected PropSound prop_soundInverter2i;
	protected PropSound prop_soundInverter3i;

	protected PropSound prop_soundTraction0e;
	protected PropSound prop_soundTraction1e;
	protected PropSound prop_soundTraction2e;
	protected PropSound prop_soundInverter0e;
	protected PropSound prop_soundInverter1e;
	protected PropSound prop_soundInverter2e;
	protected PropSound prop_soundInverter3e;

	/*
	 * Volume Values
	 */
	public float volumeTraction0;
	public float volumeTraction1;
	public float volumeTraction2;
	public float volumeInverter0;
	public float volumeInverter1;
	public float volumeInverter2;
	public float volumeInverter3;

	/*
	 * Pitch Values
	 */
	public float pitchTraction0;
	public float pitchTraction1;
	public float pitchTraction2;
	public float pitchInverter0;
	public float pitchInverter1;
	public float pitchInverter2;
	public float pitchInverter3;



	/**
	 * Initialises a new instance of the {@link ElectricEngineSounds} class.
	 * 
	 * @param engineVehiclePart - The parent vehicle part.
	 * @param enginePartType - The parent vehicle part type.
	 */
	public ElectricEngineSounds(AVehParBaseImpl engineVehiclePart, PartTypeEngineElectric enginePartType)
	{
		/*
		 * Assign this wrapper's references to the parent vehicle part and part types.
		 */
		this.engineVehiclePart = engineVehiclePart;
		this.enginePartType = enginePartType;

		/*
		 * We then need to add the new sound properties (PropSoundTraction/Inverter) to
		 * the parent vehicle part. These sound property classes look back to this
		 * wrapper class to determine what volume and pitch they should play at. An
		 * integer is passed to each property so it knows which sound effect it is.
		 */
		this.engineVehiclePart.addProperty(prop_soundTraction0i = new PropSoundTraction(this, 0, true));
		this.engineVehiclePart.addProperty(prop_soundTraction1i = new PropSoundTraction(this, 1, true));
		this.engineVehiclePart.addProperty(prop_soundTraction2i = new PropSoundTraction(this, 2, true));
		this.engineVehiclePart.addProperty(prop_soundInverter0i = new PropSoundInverter(this, 0, true));
		this.engineVehiclePart.addProperty(prop_soundInverter1i = new PropSoundInverter(this, 1, true));
		this.engineVehiclePart.addProperty(prop_soundInverter2i = new PropSoundInverter(this, 2, true));
		this.engineVehiclePart.addProperty(prop_soundInverter3i = new PropSoundInverter(this, 3, true));

		this.engineVehiclePart.addProperty(prop_soundTraction0e = new PropSoundTraction(this, 0, false));
		this.engineVehiclePart.addProperty(prop_soundTraction1e = new PropSoundTraction(this, 1, false));
		this.engineVehiclePart.addProperty(prop_soundTraction2e = new PropSoundTraction(this, 2, false));
		this.engineVehiclePart.addProperty(prop_soundInverter0e = new PropSoundInverter(this, 0, false));
		this.engineVehiclePart.addProperty(prop_soundInverter1e = new PropSoundInverter(this, 1, false));
		this.engineVehiclePart.addProperty(prop_soundInverter2e = new PropSoundInverter(this, 2, false));
		this.engineVehiclePart.addProperty(prop_soundInverter3e = new PropSoundInverter(this, 3, false));
	}

	/**
	 * This method should be called each tick by the parent vehicle part to update
	 * all contained sound effects in this wrapper class.
	 */
	public void onUpdate()
	{
		/*
		 * Obtain attributes about the train and the electric engine to pass onto the
		 * sound effect update methods.
		 */
		float speed = (float)Math.abs(enginePartType.getTrain().getLastSpeed() * 3.6F * 20.0F);
		float motorActivity = enginePartType.motorActivity.get();
		float motorPolarity = enginePartType.motorPolarity.get();

		volumeTraction0 = updateVolumeTraction(speed, motorActivity, 0);
		volumeTraction1 = updateVolumeTraction(speed, motorActivity, 1);
		volumeTraction2 = updateVolumeTraction(speed, motorActivity, 2);

		pitchTraction0 = updatePitchTraction(speed, 0) - 0.25F;
		pitchTraction1 = updatePitchTraction(speed, 1) - 0.25F;
		pitchTraction2 = updatePitchTraction(speed, 2) - 0.25F;

		volumeInverter0 = updateVolumeInverter(speed, motorPolarity, 0);
		volumeInverter1 = updateVolumeInverter(speed, motorPolarity, 1);
		volumeInverter2 = updateVolumeInverter(speed, motorPolarity, 2);
		volumeInverter3 = updateVolumeInverter(speed, motorPolarity, 3);

		/*
		 * The pitch of all the inverter sound effects can stay at 1.0F (default).
		 */
		pitchInverter0 = 1.0F;
		pitchInverter1 = 1.0F;
		pitchInverter2 = 1.0F;
		pitchInverter3 = 1.0F;
	}

	/**
	 * Called to determine the correct pitch for the specified traction sound index
	 * at the specified speed.
	 * 
	 * @param speed - The current speed of the train.
	 * @param type - The index of the traction sound effect.
	 * @return - A {@code float} representing the pitch of the sound effect.
	 */
	private float updatePitchTraction(float speed, int type)
	{
		// Helper variable
		float pitch = 1.0F;

		switch (type)
		{
			case 0: // 0.0 to 30.0 km/h
			{
				if (speed >= 0.0F && speed < 30.0F)
				{
					pitch += (speed - 15.0F) / 60.0F;
				}
				break;
			}

			case 1: // 30.0 to 60.0 km/h
			{
				if (speed >= 2.0F && speed < 60.0F)
				{
					pitch += (speed - 30.0F) / 90.0F;
				}
				break;
			}

			case 2: // 60.0 to 90.0 km/h
			{
				if (speed >= 30.0F)
				{
					pitch += (speed - 60.0F) / 120.0F;
				}
				break;
			}

			default:
				break;
		}

		return pitch;
	}

	/**
	 * Called to determine the correct volume to use for the traction sound of index
	 * 'type', if the train is travelling at 'speed' km/h and the electric engine
	 * has a motor activity value of 'motorActivity'.
	 * 
	 * @param speed - The current speed of the train in km/h.
	 * @param motorActivity - The current motor activity value of the electric
	 *            engine.
	 * @param type - The index of traction sound to calculate for.
	 * @return - A {@code float} representing the volume of the sound effect. This
	 *         value is clamped between {@code 0.0F} and {@code 1.0F}.
	 */
	private float updateVolumeTraction(float speed, float motorActivity, int type)
	{
		float volume = 0.0F;

		switch (type)
		{
			case 0: // 0.0 to 30.0 km/h
			{
				if (speed > 0.0F && speed < 5.0F)
				{
					volume = speed / 5.0F;
				}
				else if (speed >= 5.0F && speed < 30.0F)
				{
					volume = 1.0F - ((speed - 5.0F) / 25.0F);
				}
				break;
			}

			case 1: // 30.0 to 60.0 km/h
			{
				if (speed > 2.0F && speed < 30.0F)
				{
					volume = (speed - 2.0F) / 28.0F;
				}
				else if (speed >= 30.0F && speed < 60.0F)
				{
					volume = 2.0F - (speed / 30.0F);
				}
				break;
			}

			case 2: // 60.0 to 90.0 km/h
			{
				if (speed > 30.0F && speed < 60.0F)
				{
					volume = (speed - 30.0F) / 30.0F;
				}
				else if (speed >= 60.0F)
				{
					volume = 1.0F;
				}
				break;
			}
		}

		/*
		 * The volume of the traction motor correlates with the current motor activity
		 * of the electric motor, so here the volume is multiplied by motorActivity.
		 */
		volume *= motorActivity;

		/*
		 * Clamp the value between 0 and 1.
		 */
		volume = MathHelper.clamp_float(volume, 0.0F, 1.0F);

		return volume;
	}

	/**
	 * Called to determine the correct volume to use for the inverter sound of index
	 * 'type', if the train is travelling at 'speed' km/h and the electric engine
	 * has a motor polarity value of 'motorPolarity'.
	 * 
	 * @param speed - The speed of the train in km/h.
	 * @param motorPolarity - The current polarity of the electric engine. This
	 *            value moves away from {@code 0.0F} when the motor is
	 *            ready to produce power, hence making the inverter sound
	 *            effects start playing.
	 * @param type - The index of inverter sound effect to calculate for.
	 * @return - A {@code float} representing the volume of the inverter sound
	 *         effect. This value is clamped between {@code 0.0F} and {@code 1.0F}.
	 */
	private float updateVolumeInverter(float speed, float motorPolarity, int type)
	{
		float volume = 0.0F;

		switch (type)
		{
			case 0: // 0.0 to 10.0 km/h
			{
				if (speed > 0.0F && speed < 2.0F)
				{
					volume = speed / 2.0F;
				}
				else if (speed >= 2.0F && speed < 5.0F)
				{
					volume = 1.0F;
				}
				else if (speed >= 5.0F && speed < 10.0F)
				{
					volume = 1.0F - (speed - 5.0F) / 5.0F;
				}
				break;
			}

			case 1: // 5.0 to 10.0 km/h
			{
				if (speed >= 5.0F && speed < 10.0F)
				{
					volume = (speed - 5.0F) / 5.0F;
				}

				break;
			}

			case 2: // 10.0 to 35.0 km/h
			{
				if (speed >= 10.0F && speed < 35.0F)
				{
					volume = 1.0F;
				}

				break;
			}

			case 3: // 35.0+ km/h
			{
				if (speed >= 35.0F && speed < 135.0F)
				{
					volume = 1.0F - ((speed - 35.0F) / 100.0F);
				}

				break;
			}
		}

		/*
		 * The volume of the inverter sound effect increases in volume when the motor
		 * polarity value of the electric motor moves away from 0. So here the volume
		 * value is simply multiplied by the absolute value of the motorPolarity value.
		 */
		volume *= Math.abs(motorPolarity);

		/*
		 * Clamp the result between 0 and 1 so it will be safe to use as a volume for a
		 * sound effect.
		 */
		volume = MathHelper.clamp_float(volume, 0.0F, 1.0F);

		return volume;
	}



	/**
	 * An abstract class which helps build the foundation of a Sound Effect Property
	 * which can be added to a vehicle part, and which will automatically update its
	 * pitch and volume based on the values provided in a supplied wrapper class
	 * (e.g. the {@link ElectricEngineSounds} class).
	 * 
	 * @author jaffa
	 * @see PropSoundRepeated
	 */
	public static abstract class AElectricEngineSound extends PropSoundRepeated
	{
		/**
		 * The parent electric engine sounds wrapper class. This class will hold all the
		 * necessary volume and pitch values (stored as {@code float}s) which this sound
		 * effect class will refer to when adjusting the volume and pitch.
		 */
		protected final ElectricEngineSounds wrapper;
		/**
		 * The index of this electric engine sound effect, so this class knows which
		 * values to read from the supplied wrapper class.
		 */
		protected final int index;
		protected final Boolean interior;



		/**
		 * Initialises a new instance of the {@link AElectricEngineSound} class.
		 * 
		 * @param wrapper - The electric engine sounds wrapper class.
		 * @param index - The index of electric engine sound this sound
		 *            property represents.
		 * @param resourceLocation - A {@code String} which points to the sound effect
		 *            file that will be played when this sound effect is
		 *            audible.
		 * @param name - A {@code String} that is used as the name of this
		 *            Property in the NBT system.
		 */
		public AElectricEngineSound(ElectricEngineSounds wrapper, int index, String resourceLocation, String name, Boolean interior)
		{
			super(wrapper.engineVehiclePart, interior != null ? (resourceLocation + (interior ? "_int" : "_ext")) : resourceLocation, name);

			this.wrapper = wrapper;
			this.index = index;
			this.interior = interior;
		}

		@Override
		public Integer get()
		{
			return getVolume() > 0.0F ? -1 : 0;
		}

		/**
		 * Called every tick, the value returned is the current pitch value in the
		 * wrapper class.
		 */
		protected abstract float getPitch();

		/**
		 * Called every tick, the value returned is the current volume value in the
		 * wrapper class.
		 */
		protected abstract float getVolume();

		@Override
		protected BasicSound getNewSound(ResourceLocation resLoc)
		{
			return new BasicSound(resLoc, this)
			{
				@Override
				public float getPitch()
				{
					return ((AElectricEngineSound)prop).getPitch();
				};

				@Override
				public float getVolume()
				{
					return ((AElectricEngineSound)prop).getVolume();
				}
			}.setCanRepeat();
		}
	}

	public static class PropSoundInverter extends AElectricEngineSound
	{
		public PropSoundInverter(ElectricEngineSounds wrapper, int index, Boolean interior)
		{
			super(wrapper, index, String.format("%s:vehPar_axisEngine_inverter%d", ModData.ID, index), "electricEngineSounds", interior);
		}

		@Override
		protected float getPitch()
		{
			switch (index)
			{
				case 0:
					return wrapper.pitchInverter0;

				case 1:
					return wrapper.pitchInverter1;

				case 2:
					return wrapper.pitchInverter2;

				case 3:
					return wrapper.pitchInverter3;

				default:
					return 1.0F;
			}
		}

		@Override
		protected float getVolume()
		{
			if (interior != null && wrapper.engineVehiclePart.getTrain().worldObj.isRemote)
			{
				if (isPlayerInside(wrapper.enginePartType.getTrain()))
				{
					if (!interior)
					{
						return 0.0F;
					}
				}
				else
				{
					if (interior)
					{
						return 0.0F;
					}
				}
			}

			switch (index)
			{
				case 0:
					return wrapper.volumeInverter0;

				case 1:
					return wrapper.volumeInverter1;

				case 2:
					return wrapper.volumeInverter2;

				case 3:
					return wrapper.volumeInverter3;

				default:
					return 0.0F;
			}
		}
	}

	public static class PropSoundTraction extends AElectricEngineSound
	{

		public PropSoundTraction(ElectricEngineSounds wrapper, int index, Boolean interior)
		{
			super(wrapper, index, String.format("%s:vehPar_axisEngine_traction%d", ModData.ID, index), "electricEngineSounds", interior);
		}

		protected float getPitch()
		{
			switch (index)
			{
				case 0:
					return wrapper.pitchTraction0;

				case 1:
					return wrapper.pitchTraction1;

				case 2:
					return wrapper.pitchTraction2;

				default:
					return 1.0F;
			}
		}

		protected float getVolume()
		{
			if (interior != null && wrapper.engineVehiclePart.getTrain().worldObj.isRemote)
			{
				if (isPlayerInside(wrapper.enginePartType.getTrain()))
				{
					if (!interior)
					{
						return 0.0F;
					}
				}
				else
				{
					if (interior)
					{
						return 0.0F;
					}
				}
			}

			switch (index)
			{
				case 0:
					return wrapper.volumeTraction0;

				case 1:
					return wrapper.volumeTraction1;

				case 2:
					return wrapper.volumeTraction2;

				default:
					return 0.0F;
			}
		}
	}
}
