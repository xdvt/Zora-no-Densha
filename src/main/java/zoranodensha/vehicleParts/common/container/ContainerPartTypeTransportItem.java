package zoranodensha.vehicleParts.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import zoranodensha.common.containers.slot.SlotRestrictive;



/**
 * Container class for default inventory of {@link zoranodensha.api.vehicles.part.type.PartTypeTransportItem}.
 * <p>
 * Code copied from Minecraft's {@link net.minecraft.inventory.ContainerChest ContainerChest}.<br>
 * No copyright claims for this class are made.
 */
public class ContainerPartTypeTransportItem extends Container
{
	private IInventory partInventory;
	private int invSize;



	public ContainerPartTypeTransportItem(IInventory playerInv, IInventory partInv)
	{
		partInventory = partInv;
		invSize = partInv.getSizeInventory();
		int rows = invSize / 9;
		int slotYOffset = (rows - 4) * 18;
		partInv.openInventory();

		for (int i = 0; i < rows; ++i)
		{
			for (int j = 0; j < 9; ++j)
			{
				addSlotToContainer(new SlotRestrictive(partInv, j + i * 9, 8 + j * 18, 18 + i * 18));
			}
		}

		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 9; ++j)
			{
				addSlotToContainer(new Slot(playerInv, j + i * 9 + 9, 8 + j * 18, 103 + i * 18 + slotYOffset));
			}
		}

		for (int i = 0; i < 9; ++i)
		{
			addSlotToContainer(new Slot(playerInv, i, 8 + i * 18, 161 + slotYOffset));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return partInventory.isUseableByPlayer(player);
	}

	@Override
	protected boolean mergeItemStack(ItemStack itemStack, int minSlot, int maxSlot, boolean countDown)
	{
		boolean success = false;
		int min = minSlot;

		if (countDown)
		{
			min = maxSlot - 1;
		}

		Slot slot;
		ItemStack itemStack1;
		if (itemStack.isStackable())
		{
			while (itemStack.stackSize > 0 && (!countDown && min < maxSlot || countDown && min >= minSlot))
			{
				if (partInventory.isItemValidForSlot(min, itemStack))
				{
					slot = (Slot)inventorySlots.get(min);
					itemStack1 = slot.getStack();
					if (itemStack1 != null && itemStack1.getItem() == itemStack.getItem() && (!itemStack.getHasSubtypes() || itemStack.getItemDamage() == itemStack1.getItemDamage())
							&& ItemStack.areItemStackTagsEqual(itemStack, itemStack1))
					{
						int l = itemStack1.stackSize + itemStack.stackSize;
						if (l <= itemStack.getMaxStackSize())
						{
							itemStack.stackSize = 0;
							itemStack1.stackSize = l;
							slot.onSlotChanged();
							success = true;
						}
						else if (itemStack1.stackSize < itemStack.getMaxStackSize())
						{
							itemStack.stackSize -= itemStack.getMaxStackSize() - itemStack1.stackSize;
							itemStack1.stackSize = itemStack.getMaxStackSize();
							slot.onSlotChanged();
							success = true;
						}
					}
				}

				if (countDown)
				{
					--min;
				}
				else
				{
					++min;
				}
			}
		}

		if (itemStack.stackSize > 0)
		{
			if (countDown)
			{
				min = maxSlot - 1;
			}
			else
			{
				min = minSlot;
			}

			while (!countDown && min < maxSlot || countDown && min >= minSlot)
			{
				if (partInventory.isItemValidForSlot(min, itemStack))
				{
					slot = (Slot)inventorySlots.get(min);
					itemStack1 = slot.getStack();
					if (itemStack1 == null)
					{
						slot.putStack(itemStack.copy());
						slot.onSlotChanged();
						itemStack.stackSize = 0;
						success = true;
						break;
					}
				}

				if (countDown)
				{
					--min;
				}
				else
				{
					++min;
				}
			}
		}

		return success;
	}

	@Override
	public void onContainerClosed(EntityPlayer player)
	{
		super.onContainerClosed(player);
		partInventory.closeInventory();
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotID)
	{
		ItemStack itemStack = null;
		Slot slot = (Slot)inventorySlots.get(slotID);
		if (slot != null && slot.getHasStack())
		{
			ItemStack slotStack = slot.getStack();
			itemStack = slotStack.copy();
			if (slotID < invSize)
			{
				if (!mergeItemStack(slotStack, invSize, inventorySlots.size(), true))
				{
					return null;
				}
			}
			else if (!mergeItemStack(slotStack, 0, invSize, false))
			{
				return null;
			}

			if (slotStack.stackSize == 0)
			{
				slot.putStack(null);
			}
			else
			{
				slot.onSlotChanged();
			}
		}

		return itemStack;
	}
}
