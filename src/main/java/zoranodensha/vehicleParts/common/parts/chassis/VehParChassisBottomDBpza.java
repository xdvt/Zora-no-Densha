package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisBottomDBpza;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisBottomDBpza extends AVehParChassisBase
{
	/** Property holding whether this part renders a UVID. */
	public PropBoolean hasUVID;
	public PropInteger prop_type;
	public PropColor colorBaseplate;
	public PropColor colorStripeBottom;



	/**
	 * Initialises a new instance of the {@link zoranodensha.vehicleParts.common.parts.chassis.VehParChassisBottomDBpza} class.
	 */
	public VehParChassisBottomDBpza()
	{
		this(0);
	}


	/**
	 * Initialises a new instance of the {@link zoranodensha.vehicleParts.common.parts.chassis.VehParChassisBottomDBpza} class.
	 * 
	 * @param type - The starting type to assign to this part.
	 */
	public VehParChassisBottomDBpza(int type)
	{
		super("VehParChassisBottomDBpza", 0.75F, 0.636F, 1.4F);

		addProperty(hasUVID = (PropBoolean)new PropBoolean(this, false, "hasUVID").setConfigurable());
		addProperty(prop_type = (PropInteger)new PropInteger.PropIntegerBounded(this, type, 0, 2, "type").setConfigurable());
		addProperty(colorBaseplate = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_baseplate").setConfigurable());
		addProperty(colorStripeBottom = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripe_bottom").setConfigurable());

		colorOut.set(ELiveryColor.WHITE.color());
		colorIn.set(ELiveryColor.GREY_LIGHT.color());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisBottomDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", "SPS", " P ", 'P', "plateSteel", 'S', Blocks.glass_pane));
	}
}