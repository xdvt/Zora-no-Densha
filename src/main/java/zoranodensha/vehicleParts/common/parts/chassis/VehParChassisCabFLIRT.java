package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisCabFLIRT;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisCabFLIRT extends AVehParChassisCabBase
{
	/** Door color. Only used if the cab has doors. */
	public PropColor colorDoors;
	/** Side color. */
	public PropColor colorSides;
	/** Color of the stripe below the front window. */
	public PropColor colorStripesFront;
	/** Color of the area surrounding lights. */
	public PropColor colorStripesLights;
	/** Top-most stripe's color. */
	public PropColor colorStripesTop;
	/** Color of stripes surrounding windows. */
	public PropColor colorStripesWindows;
	/** Window color. */
	public PropColor colorWindows;
	/** {@code true} if this part has a cab door. */
	public PropBoolean isDoorCab;



	public VehParChassisCabFLIRT()
	{
		super("VehParChassisCabFLIRT", 0.75F, 1.0F, 5.6F);

		colorIn.set(ELiveryColor.WHITE.color());
		colorOut.set(ELiveryColor.WHITE.color());

		addProperty(isDoorCab = (PropBoolean)new PropBoolean(this, "isDoorCab").setConfigurable());
		addProperty(colorSides = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_sides").setConfigurable());
		addProperty(colorStripesFront = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_stripes_front").setConfigurable());
		addProperty(colorStripesLights = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_stripes_lights").setConfigurable());
		addProperty(colorStripesTop = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_stripes_top").setConfigurable());
		addProperty(colorStripesWindows = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_windows").setConfigurable());
		addProperty(colorWindows = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color().setAlpha(0.75F), "color_windows").setConfigurable());
		addProperty(colorDoors = (PropColor)new PropColor(this, ELiveryColor.WHITE.color(), "color_doors")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return isDoorCab.get();
			};
		}.setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisCabFLIRT(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PMG", "PCG", "CPP", 'P', "plateSteel", 'C', "circuitBasic", 'G', "paneGlass", 'M', new ItemStack(ModCenter.ItemPart, 1, 2)));
	}
}