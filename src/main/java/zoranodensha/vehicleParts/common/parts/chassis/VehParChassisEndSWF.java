package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisEndSWF;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.presets.ELiveryColor;
import zoranodensha.vehicleParts.presets.SlidingWallFreight_S.EWallType;



public class VehParChassisEndSWF extends AVehParBaseImpl
{
	public PropEnum<EWallType> walltype;

	/** This part's base color. */
	public PropColor color;
	/** Bottom sides' color. */
	public PropColor colorBot;
	/** Top rounded sides' color. */
	public PropColor colorTop;
	/** Lock wheels' color. */
	public PropColor color_wheels;



	public VehParChassisEndSWF()
	{
		super("VehParChassisEndSWF", 0.1F, 0.9F, 0.50F);

		addProperty(walltype = (PropEnum<EWallType>)new PropEnum<EWallType>(this, EWallType.SOFT, "type").setConfigurable());
		addProperty(color = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color").setConfigurable());
		addProperty(colorBot = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_bottom").setConfigurable());
		addProperty(colorTop = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_top").setConfigurable());
		addProperty(color_wheels = (PropColor)new PropColor(this, ELiveryColor.GOLD.color(), "color_wheels").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisEndSWF(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " S ", "SSS", "SSS", 'S', "plateSteel"));
	}
}