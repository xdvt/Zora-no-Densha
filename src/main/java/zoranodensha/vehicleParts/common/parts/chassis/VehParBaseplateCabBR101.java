package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_BaseplateCabBR101;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParBaseplateCabBR101 extends AVehParBaseImpl
{
	/** Property holding stripe color. */
	public PropColor colorStripes;
	/** Property holding plate color. */
	public PropColor colorPlate;



	public VehParBaseplateCabBR101()
	{
		super("VehParBaseplateCabBR101", 0.75F, 0.215F, 2.0F);

		addProperty(colorStripes = (PropColor)new PropColor(this, ELiveryColor.NAVY.color(), "color_stripes").setConfigurable());
		addProperty(colorPlate = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_plate").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BaseplateCabBR101(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", "S S", "C C", 'P', "plateSteel", 'S', "ingotSteel", 'C', "dyeWhite"));
	}
}