package zoranodensha.vehicleParts.common.parts.chassis;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic;
import zoranodensha.api.vehicles.part.type.PartTypeHorn;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.core.ModData;



public abstract class AVehParChassisCabBase extends AVehParChassisBase
{
	/** This cab chassis' horn type. */
	public final PartTypeHorn type_horn;
	/** This cab chassis' light type, used to contain light mode data. */
	public final PartTypeLampImpl type_lamp;

	/**
	 * Cached {@link zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode light mode}.
	 * This is actually a hacky work-around to keep light mode data in a chassis.
	 * 
	 * <p>
	 * <b>DO NOT DO THIS IN THE FUTURE - Use separate lamps instead!</b>
	 * </p>
	 */
	protected final PropEnum<ELightMode> mode;



	public AVehParChassisCabBase(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2, heightBy2, mass);

		type_horn = new PartTypeHorn(this, new PropSound(this, ModData.ID + ":vehPar_cab_horn", "soundHorn"));
		type_lamp = new PartTypeLampImpl(this);

		addProperty(mode = (PropEnum<ELightMode>)new PropEnum<ELightMode>(this, ELightMode.OFF, "light_mode").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
	}

	/**
	 * Helper method to search for a cab whose bounding box intersects with ours.
	 *
	 * @return The first {@link zoranodensha.api.vehicles.part.type.PartTypeCabBasic cab} found, or {@code null} if none was found.
	 */
	public PartTypeCabBasic getCabInside()
	{
		AxisAlignedBB aabb = getLocalBoundingBox();
		for (PartTypeCabBasic part : getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_CAB))
		{
			if (aabb.intersectsWith(part.getParent().getLocalBoundingBox()))
			{
				return part;
			}
		}
		return null;
	}

	/**
	 * Return the currently cached {@link zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode light mode}.
	 */
	public ELightMode getLightMode()
	{
		return mode.get();
	}

	/**
	 * Called by this class' anonymous part type implementation of {@link zoranodensha.api.vehicles.part.type.APartType.IPartType_Activatable IPartType_Activatable}.<br>
	 * <br>
	 * Called on <i>non-remote worlds</i> only.
	 *
	 * @param player - The {@link net.minecraft.entity.player.EntityPlayer player} that clicked this part.
	 * @return {@code true} if something happens.
	 */
	public boolean onActivated(EntityPlayer player)
	{
		/* Ensure the player exists and isn't sneaking. */
		if (player == null || player.isSneaking())
		{
			return false;
		}

		/* Also make sure our parent part is finished and we are on a non-remote world in a spawned train. */
		Train train = getTrain();
		if (!getIsFinished() || train == null || !train.addedToChunk || train.worldObj.isRemote)
		{
			return false;
		}
		
		/* Get cab inside and, if existent, check whether the player may be seated. */
		PartTypeCabBasic cab = getCabInside();
		return (cab != null && cab.setPassenger(player));
	}

	@Override
	public void onPropertyChanged(IPartProperty<?> property)
	{
		if (type_horn != null && property != type_horn.sound)
		{
			super.onPropertyChanged(property);
		}
	}
}