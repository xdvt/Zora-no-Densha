package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisWallSWF;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.presets.ELiveryColor;
import zoranodensha.vehicleParts.presets.SlidingWallFreight_S.EWallType;



public class VehParChassisWallSWF extends AVehParBaseImpl
{
	/** Type of this part. Either soft, cornered wall (Alpha) or solid, rounded wall (Beta). */
	public PropEnum<EWallType> wallType;

	/** Property holding whether this part renders a UVID. */
	public PropBoolean hasUVID;
	/** Property holding whether this part is full length or not. */
	public PropBoolean isDouble;
	/** Property which controls whether the details of this part are reversed. */
	public PropBoolean isReversed;

	/** Bottom frame color. */
	public PropColor colorFrameBot;
	/** Top frame color. */
	public PropColor colorFrameTop;
	/** Bottom panel color. */
	public PropColor colorPanelBot;
	/** Top panel color. */
	public PropColor colorPanelTop;
	/** Detail color. */
	public PropColor colorDetail;
	/** Interior color. */
	public PropColor colorInside;



	public VehParChassisWallSWF()
	{
		super("VehParChassisWallSWF", 0.265F, 0.815F, 0.18F);

		addProperty(wallType = (PropEnum<EWallType>)new PropEnum<EWallType>(this, EWallType.SOFT, "type").setConfigurable());
		addProperty(hasUVID = (PropBoolean)new PropBoolean(this, true, "hasUVID").setConfigurable());
		addProperty(isDouble = (PropBoolean)new PropBoolean(this, true, "isDouble")
		{
			@Override
			public boolean set(Object property)
			{
				if (super.set(property))
				{
					if (getTrain() != null)
					{
						getTrain().refreshProperties();
					}
					return true;
				}
				return false;
			}
		}.setConfigurable());

		addProperty(isReversed = (PropBoolean)new PropBoolean(this, false, "isReversed")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return isDouble.get();
			};
		}.setConfigurable());

		addProperty(colorFrameBot = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_frame_bot").setConfigurable());
		addProperty(colorFrameTop = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_frame_top").setConfigurable());
		addProperty(colorPanelBot = (PropColor)new PropColor(this, ELiveryColor.WHITE.color(), "color_panel_bot").setConfigurable());
		addProperty(colorPanelTop = (PropColor)new PropColor(this, ELiveryColor.WHITE.color(), "color_panel_top").setConfigurable());
		addProperty(colorInside = (PropColor)new PropColor(this, ELiveryColor.WHITE.color(), "color_inside").setConfigurable());
		addProperty(colorDetail = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_detail")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return isDouble.get();
			};
		}.setConfigurable());
	}

	@Override
	public float getMass()
	{
		int i = isDouble.get() ? 2 : 1;
		return super.getMass() * i;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisWallSWF(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		ItemStack result = new ItemStack(itemStack.getItem(), 2, itemStack.getItemDamage());
		result.setTagCompound(itemStack.getTagCompound());
		GameRegistry.addRecipe(new ShapedOreRecipe(result, "  S", " SP", " SP", 'S', "plateSteel", 'P', "sheetPlastic"));
	}
}