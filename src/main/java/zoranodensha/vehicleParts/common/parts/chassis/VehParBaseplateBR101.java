package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_BaseplateBR101;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParBaseplateBR101 extends AVehParBaseImpl
{
	/** Property holding whether this part renders a UVID. */
	public PropBoolean hasUVID;
	/** Property holding this part's color. */
	public PropColor color;



	public VehParBaseplateBR101()
	{
		super("VehParBaseplateBR101", 0.75F, 0.215F, 2.25F);

		addProperty(hasUVID = (PropBoolean)new PropBoolean(this, true, "hasUVID").setConfigurable());
		addProperty(color = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BaseplateBR101(this) : renderer);
	}

	@Override
	public void onPropertyChanged(IPartProperty<?> property)
	{
		if (property != hasUVID)
		{
			super.onPropertyChanged(property);
		}
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", "SSS", "P P", 'P', "plateSteel", 'S', "ingotSteel"));
	}
}