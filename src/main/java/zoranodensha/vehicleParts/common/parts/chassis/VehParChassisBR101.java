package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisBR101;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisBR101 extends AVehParChassisBase
{
	/** Property holding roof color. */
	public PropColor colorRoof;
	/** Property holding stripes color. */
	public PropColor colorStripes;



	public VehParChassisBR101()
	{
		super("VehParChassisBR101", 0.75F, 0.72F, 3.0F);

		colorIn.set(ELiveryColor.WHITE.color());
		colorOut.set(ELiveryColor.WHITE.color());
		addProperty(colorRoof = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_roof").setConfigurable());
		addProperty(colorStripes = (PropColor)new PropColor(this, ELiveryColor.NAVY.color(), "color_stripes").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisBR101(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", "SSS", "PPP", 'P', "plateSteel", 'S', "ingotSteel"));
	}
}