package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisCabDBpza;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisCabDBpza extends AVehParChassisCabBase
{
	public PropColor colorBaseplate;
	public PropColor colorRoof;
	public PropColor colorFront;
	public PropColor colorStripeBottom;
	public PropColor colorStripeTop;


	public VehParChassisCabDBpza()
	{
		super("VehParChassisCabDBpza", 0.75F, 1.38F, 3.6F);

		colorOut.set(ELiveryColor.WHITE.color());
		colorIn.set(ELiveryColor.GREY_LIGHT.color());
		
		addProperty(colorBaseplate = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_baseplate").setConfigurable());
		addProperty(colorRoof = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_roof").setConfigurable());
		addProperty(colorFront = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_front").setConfigurable());
		addProperty(colorStripeBottom = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_bot").setConfigurable());
		addProperty(colorStripeTop = (PropColor)new PropColor(this, ELiveryColor.NAVY.color(), "color_stripes_top").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisCabDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPG", "PCG", "PPP", 'P', "plateSteel", 'C', "circuitBasic", 'G', "paneGlass"));
	}
}