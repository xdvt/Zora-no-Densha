package zoranodensha.vehicleParts.common.parts.chassis;

import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public abstract class AVehParBaseplateModFreight extends AVehParBaseImpl
{
	/** Property holding the base plate's color. */
	public PropColor color;



	public AVehParBaseplateModFreight(String name, float mass)
	{
		this(name, 0.75F, 0.22F, mass);
	}

	public AVehParBaseplateModFreight(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2, heightBy2, mass);
		addProperty(color = (PropColor)new PropColor(this, new ColorRGBA(0.27F, 0.27F, 0.27F), "color").setConfigurable());
	}
}