package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisCabBR101;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisCabBR101 extends AVehParChassisCabBase
{
	/** Chassis side color. */
	public PropColor colorChassisSide;
	/** Chassis front strip color. */
	public PropColor colorChassisStrip;
	/** Chassis side stripes color. */
	public PropColor colorChassisStripes;
	/** Driver's desk color. */
	public PropColor colorTable;
	/** Door color. */
	public PropColor colorDoors;
	/** {@code true} if this part's left door is open. */
	public PropBoolean isDoorOpenL;
	/** {@code true} if this part's right door is open. */
	public PropBoolean isDoorOpenR;



	public VehParChassisCabBR101()
	{
		super("VehParChassisCabBR101", 0.75F, 0.74575F, 3.33F);

		colorIn.set(ELiveryColor.WHITE.color());
		colorOut.set(ELiveryColor.GREY_LIGHT.color());
		addProperty(colorChassisSide = (PropColor)new PropColor(this, ELiveryColor.WHITE.color(), "color_side").setConfigurable());
		addProperty(colorChassisStrip = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_strip").setConfigurable());
		addProperty(colorChassisStripes = (PropColor)new PropColor(this, ELiveryColor.NAVY.color(), "color_stripes").setConfigurable());
		addProperty(colorTable = (PropColor)new PropColor(this, new ColorRGBA(0.85F, 1.0F, 1.0F), "color_table").setConfigurable());
		addProperty(colorDoors = (PropColor)new PropColor(this, ELiveryColor.WHITE.color(), "color_doors").setConfigurable());
		addProperty(isDoorOpenL = (PropBoolean)new PropBoolean(this, "isDoorOpenL").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(isDoorOpenR = (PropBoolean)new PropBoolean(this, "isDoorOpenR").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisCabBR101(this) : renderer);
	}

	@Override
	public boolean onActivated(EntityPlayer player)
	{
		if (super.onActivated(player))
		{
			/* Close doors if the player was seated. */
			if (!getTrain().worldObj.isRemote)
			{
				setDoorState(false, false);
				setDoorState(true, false);
			}
			return true;
		}
		return false;
	}

	/**
	 * Called to play the door close sound.
	 */
	private void playSoundDoorClose(World world)
	{
		Vec3 vecPos = PartHelper.getPosition(this);
		world.playSoundEffect(vecPos.xCoord, vecPos.yCoord, vecPos.zCoord, ModData.ID + ":vehPar_chassisCabBR101_doorClose", 1.0F, world.rand.nextFloat() * 0.1F + 0.9F);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", "PCG", "PPP", 'P', "plateSteel", 'C', "circuitBasic", 'G', "paneGlass"));
	}

	protected void setDoorState(boolean isRightSide, boolean value)
	{
		PropBoolean prop = isRightSide ? isDoorOpenR : isDoorOpenL;
		if (prop.set(value) && !getTrain().worldObj.isRemote && !value)
		{
			playSoundDoorClose(getTrain().worldObj);
		}
	}
}