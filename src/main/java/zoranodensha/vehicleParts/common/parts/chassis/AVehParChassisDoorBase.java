package zoranodensha.vehicleParts.common.parts.chassis;

import net.minecraft.util.ResourceLocation;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.common.core.ModData;



public abstract class AVehParChassisDoorBase extends AVehParChassisBase
{
	/** Part type of this door. */
	public final PartTypeDoorImpl type_door;

	/** Property holding door color. */
	public PropColor prop_colorDoors;
	/** Property holding door opening chime sound. */
	protected PropSound prop_soundOpeningChime;
	/** Property holding door warning sound. */
	protected PropSound prop_soundWarningChime;
	/** Property holding door shutting sound. */
	protected PropSound prop_soundDoorShut;



	public AVehParChassisDoorBase(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2, heightBy2, mass);

		type_door = new PartTypeDoorImpl(this);

		addProperty(prop_colorDoors = (PropColor)new PropColor(this, "color_doors").setConfigurable());
		addPropertySounds();
	}


	/**
	 * Adds all door sounds to the property list.<br>
	 * May be overridden to configure sounds.
	 */
	protected void addPropertySounds()
	{
		addProperty(prop_soundOpeningChime = new PropSound(this, ModData.ID + ":vehPar_chassisDoors_DBpza_openingchime", "soundOpeningChime"));
		addProperty(prop_soundWarningChime = new PropSound(this, ModData.ID + ":vehPar_chassisDoors_DBpza_warningchime", "soundWarningChime"));
		addProperty(prop_soundDoorShut = new PropSound(this, ModData.ID + ":vehPar_chassisDoors_DBpza_doorshut", "soundDoorShut"));
	}
}