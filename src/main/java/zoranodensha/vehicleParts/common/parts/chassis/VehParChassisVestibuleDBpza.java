package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisVestibuleDBpza;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisVestibuleDBpza extends AVehParChassisBase
{
	public PropIntegerBounded prop_type;
	public PropColor colorBaseplate;
	public PropColor colorStripeBottom;
	public PropColor colorStripeTop;



	public VehParChassisVestibuleDBpza()
	{
		this(0);
	}

	public VehParChassisVestibuleDBpza(int type)
	{
		super("VehParChassisVestibuleDBpza", 0.5F, 0.715F, 1.0F);

		addProperty(prop_type = (PropIntegerBounded)new PropIntegerBounded(this, type, 0, 5, "type").setConfigurable());
		addProperty(colorBaseplate = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_baseplate").setConfigurable());
		addProperty(colorStripeBottom = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_bot").setConfigurable());
		addProperty(colorStripeTop = (PropColor)new PropColor(this, ELiveryColor.NAVY.color(), "color_stripes_top").setConfigurable());

		colorOut.set(ELiveryColor.WHITE.color());
		colorIn.set(ELiveryColor.GREY_LIGHT.color());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisVestibuleDBpza(this) : renderer);
	}

	@Override
	public float getScaledWidth()
	{
		float width = this.width;

		if (prop_type != null)
		{
			switch (prop_type.get())
			{
				case 0:
					width = 0.25F;
					break;
				case 1:
					width = 0.7F;
					break;
				case 3:
					width = 1.2F;
					break;
				case 4:
					width = 1.56F;
					break;
			}
		}

		return width * Math.min(getScale()[0], getScale()[2]);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "   ", "SPS", "PPP", 'P', "plateSteel", 'S', Blocks.glass_pane));
	}
}