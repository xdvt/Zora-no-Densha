package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisVestibuleRoofDBpza;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisVestibuleRoofDBpza extends AVehParChassisBase
{

	/** While {@code true}, this piece will take the shape of a transition piece between a Top part and a Vestibule part. */
	public PropIntegerBounded prop_type;
	public PropColor colorRoof;



	public VehParChassisVestibuleRoofDBpza()
	{
		this(0);
	}

	public VehParChassisVestibuleRoofDBpza(int type)
	{
		super("VehParChassisVestibuleRoofDBpza", 0.75F, 0.34F, 0.6F);

		addProperty(prop_type = (PropIntegerBounded)new PropIntegerBounded(this, type, 0, 1, "type").setConfigurable());
		addProperty(colorRoof = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_roof").setConfigurable());

		colorOut.set(ELiveryColor.WHITE.color());
		colorIn.set(ELiveryColor.GREY_LIGHT.color());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisVestibuleRoofDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " P ", "P P", "   ", 'P', "plateSteel"));
	}
}