package zoranodensha.vehicleParts.common.parts.chassis;

import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public abstract class AVehParChassisBase extends AVehParBaseImpl
{
	/** Property holding the outer chassis color. */
	public PropColor colorOut;
	/** Property holding the inner chassis color. */
	public PropColor colorIn;



	public AVehParChassisBase(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2, heightBy2, mass);

		addProperty(colorOut = (PropColor)new PropColor(this, "color").setConfigurable());
		addProperty(colorIn = (PropColor)new PropColor(this, "color_inside").setConfigurable());
	}
}