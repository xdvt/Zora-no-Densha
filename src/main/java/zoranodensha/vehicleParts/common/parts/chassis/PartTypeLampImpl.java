package zoranodensha.vehicleParts.common.parts.chassis;

import net.minecraft.entity.player.EntityPlayer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Activatable;
import zoranodensha.api.vehicles.part.type.PartTypeLamp;



/**
 * Lamp part type implementation offering an activation hook.
 */
public class PartTypeLampImpl extends PartTypeLamp implements IPartType_Activatable
{
	public PartTypeLampImpl(VehParBase parent)
	{
		super(parent);

		/* Ensure the parent is a valid type. */
		if (!(parent instanceof AVehParChassisCabBase))
		{
			throw new IllegalArgumentException("Supplied parent was no " + AVehParChassisCabBase.class.getSimpleName() + "!");
		}
	}

	@Override
	public void setFromLightMode(ELightMode lightMode, boolean beamsEnabled)
	{
		/* Save current light mode to parent. */
		((AVehParChassisCabBase)getParent()).mode.set(lightMode);
		super.setFromLightMode(lightMode, beamsEnabled);
	}

	@Override
	public boolean onActivated(EntityPlayer player)
	{
		/* Custom on-click hook. */
		return ((AVehParChassisCabBase)getParent()).onActivated(player);
	}
}