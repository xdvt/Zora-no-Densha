package zoranodensha.vehicleParts.common.parts.buffer;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.buffer.VehParRenderer_BufferDBpza;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParBufferDBpza extends AVehParBuffer
{
	public VehParBufferDBpza()
	{
		super("VehParBufferDBpza", 0.12F, 0.12F, 0.08F);
		color.set(ELiveryColor.GREY_DARK.color());
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BufferDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		ItemStack result = new ItemStack(itemStack.getItem(), 2, itemStack.getItemDamage());
		result.setTagCompound(itemStack.getTagCompound());
		GameRegistry.addRecipe(new ShapedOreRecipe(result, "S S", "S S", "L L", 'S', "ingotSteel", 'L', new ItemStack(ModCenter.ItemBucketOilLubricant)));
	}
}