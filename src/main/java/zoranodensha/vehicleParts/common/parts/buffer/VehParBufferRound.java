package zoranodensha.vehicleParts.common.parts.buffer;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.buffer.VehParRenderer_BufferRound;



public class VehParBufferRound extends AVehParBuffer
{
	public VehParBufferRound()
	{
		super("VehParBufferRound", 0.07F);
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BufferRound(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		ItemStack result = new ItemStack(itemStack.getItem(), 2, itemStack.getItemDamage());
		result.setTagCompound(itemStack.getTagCompound());
		GameRegistry.addRecipe(new ShapedOreRecipe(result, "P P", "S S", "L L", 'P', "plateSteel", 'S', "ingotSteel", 'L', new ItemStack(ModCenter.ItemBucketOilLubricant)));
	}
}
