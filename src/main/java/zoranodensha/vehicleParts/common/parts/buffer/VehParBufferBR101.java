package zoranodensha.vehicleParts.common.parts.buffer;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.buffer.VehParRenderer_BufferBR101;



public class VehParBufferBR101 extends AVehParBuffer
{
	/*** Property holding whether or not to render the step plates. */
	public APartProperty<Boolean> doRenderPlate;



	public VehParBufferBR101()
	{
		super("VehParBufferBR101", 0.08F);

		color.set(0x2B2B2BFF);
		addProperty(doRenderPlate = new PropBoolean(this, "renderPlate").setConfigurable());
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BufferBR101(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		OreDictionary.registerOre("vehParBufferBR101", itemStack);

		ItemStack result = new ItemStack(itemStack.getItem(), 2, itemStack.getItemDamage());
		result.setTagCompound(itemStack.getTagCompound());
		GameRegistry.addRecipe(new ShapedOreRecipe(result, "S S", "P P", "L L", 'P', "plateSteel", 'S', "ingotSteel", 'L', new ItemStack(ModCenter.ItemBucketOilLubricant)));
	}
}