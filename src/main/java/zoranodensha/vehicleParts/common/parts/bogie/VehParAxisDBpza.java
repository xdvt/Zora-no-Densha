package zoranodensha.vehicleParts.common.parts.bogie;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.bogie.VehParRenderer_AxisDBpza;
import zoranodensha.vehicleParts.common.sounds.BogieSounds;
import zoranodensha.vehicleParts.common.sounds.BogieSounds.EBogieStyle;



public class VehParAxisDBpza extends AVehParBogieImpl
{
	public VehParAxisDBpza()
	{
		super("VehParAxisNormal", 0.75F, 0.325F, 6.0F);

		setScale(1.0F, 1.0F, 0.8F);
		type_bogie.localY.set(0.325F);
		type_bogie.brakeForce.set(40);
		initSounds();
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_AxisDBpza(this) : renderer);
	}

	@Override
	protected void initSounds()
	{
		bogieSounds = new BogieSounds(this, EBogieStyle.PASSENGER);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "LP ", "SSS", "APA", 'L', new ItemStack(ModCenter.ItemBucketOilLubricant), 'P', "plateSteel", 'S', "ingotSteel", 'A', Items.minecart));
	}
}