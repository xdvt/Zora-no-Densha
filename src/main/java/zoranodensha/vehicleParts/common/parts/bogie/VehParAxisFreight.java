package zoranodensha.vehicleParts.common.parts.bogie;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.bogie.VehParRenderer_AxisFreight;
import zoranodensha.vehicleParts.common.sounds.BogieSounds;
import zoranodensha.vehicleParts.common.sounds.BogieSounds.EBogieStyle;



public class VehParAxisFreight extends AVehParBogieImpl
{
	/** Property holding this bogie's color. */
	public PropColor prop_color;



	public VehParAxisFreight()
	{
		super("VehParAxisFreight", 0.75F, 0.4195F, 5.5F);

		addProperty(prop_color = (PropColor)new PropColor(this, new ColorRGBA(0.32F, 0.32F, 0.32F), "color").setConfigurable());
		type_bogie.maxSpeed.set(120);
		type_bogie.localY.set(0.4195F);
		type_bogie.brakeForce.set(25);
		initSounds();
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_AxisFreight(this) : renderer);
	}


	@Override
	protected void initSounds()
	{
		bogieSounds = new BogieSounds(this, EBogieStyle.FREIGHT);
	}


	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "LP ", "SDS", "APA", 'L', new ItemStack(ModCenter.ItemBucketOilLubricant), 'P', "plateSteel", 'S', "ingotSteel", 'A', Items.minecart, 'D', "dyeWhite"));
	}
}