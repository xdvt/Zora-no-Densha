package zoranodensha.vehicleParts.common.parts.bogie;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeEngineElectric;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.bogie.VehParRenderer_AxisFLIRT;
import zoranodensha.vehicleParts.client.render.bogie.VehParRenderer_AxisFLIRTTrailer;
import zoranodensha.vehicleParts.common.sounds.BogieSounds;
import zoranodensha.vehicleParts.common.sounds.BogieSounds.EBogieStyle;
import zoranodensha.vehicleParts.common.sounds.ElectricEngineSounds;


public class VehParAxisFLIRTTrailer extends AVehParBogieImpl
{
	/** Property holding whether this bogie has an intercirculation above. */
	public PropBoolean hasIntercirculation;
	/** Property holding whether this bogie is a jacob's bogie. */
	public PropBoolean isJacobs;

	/** Property holding the intercirculation's length. */
	public PropFloatBounded intercirculationSize;
	/** Property holding the intercirculation's Y-offset. */
	public PropFloat intercirculationY;

	/** Property holding the bogie truck color. */
	public PropColor color;



	public VehParAxisFLIRTTrailer()
	{
		super("VehParAxisFLIRTTrailer", 0.605F, 0.263F, 4.5F);

		setScale(1.0F, 1.0F, 0.8F);
		type_bogie.localY.set(0.48F);
		type_bogie.brakeForce.set(210 / 3);

		/*
		 * Configurable general settings
		 */
		addProperty(color = (PropColor)new PropColor(this, new ColorRGBA(0.175F, 0.175F, 0.175F), "color").setConfigurable());
		addProperty(isJacobs = (PropBoolean)new PropBoolean(this, "isJacobs").setConfigurable());

		/*
		 * Configurable intercirculation settings
		 */
		addProperty(hasIntercirculation = (PropBoolean)new PropBoolean(this, "hasIntercirculation")
		{
			@Override
			public Boolean get()
			{
				return super.get() && getIsConfigurable();
			}

			@Override
			public boolean getIsConfigurable()
			{
				return isJacobs.get();
			};
		}.setConfigurable());

		addProperty(intercirculationSize = (PropFloatBounded)new PropFloatBounded(this, 0.2F, 0.001F, 10.0F, "intercirculationSize")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return hasIntercirculation.get();
			};
		}.setConfigurable());

		addProperty(intercirculationY = (PropFloat)new PropFloat(this, 0.95F, "intercirculationY")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return hasIntercirculation.get();
			};
		}.setConfigurable());

		/* Initialise bogie sounds. */
		initSounds();
	}

	@Override
	public float getMass()
	{
		float mass = super.getMass();
		if (isJacobs.get() && hasIntercirculation.get())
		{
			mass += 0.5F * (intercirculationSize.get() / 0.2F);
		}
		return mass;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_AxisFLIRTTrailer(this) : renderer);
	}

	@Override
	protected void initSounds()
	{
		bogieSounds = new BogieSounds(this, EBogieStyle.PASSENGER);
	}

	@Override
	protected void onUpdate()
	{
		super.onUpdate();
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "LPC", "SSS", "APA", 'L', new ItemStack(ModCenter.ItemBucketOilLubricant), 'P', new ItemStack(ModCenter.ItemPart, 1, 7), 'C', "circuitBasic", 'S', "ingotSteel", 'A', Items.minecart));
	}
}