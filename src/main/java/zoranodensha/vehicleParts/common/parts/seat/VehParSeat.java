package zoranodensha.vehicleParts.common.parts.seat;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.property.PropSound.BasicSound;
import zoranodensha.api.vehicles.part.property.PropSound.PropSoundRepeated;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeSeat;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.seat.VehParRenderer_Seat;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.common.sounds.APartSounds;
import zoranodensha.vehicleParts.presets.ELiveryColor;



/**
 * Vehicle parts which offer an {@link net.minecraft.entity.Entity Entity} for players to ride.<br>
 * <br>
 * The entity is offered on an on-demand basis, i.e. it will only spawn whenever a player tries to
 * sit on a seat. Once the player leaves the seat or dies, the respective entity will be removed.<br>
 * This avoids cluttering the world's entity list with unused dummy entities and also aims to increase
 * reliability and performance.<br>
 * <br>
 * Seat parts synchronise their seat entity - if it exists - to clients with synchronisation packets.
 * This method is used e.g. whenever a client comes across a train which is read from NBT.<br>
 * However, a newly spawned seat entity needs to notify and connect to its respective client-sided
 * seat parts on its own. This happens using spawn data implementation.
 */
public class VehParSeat extends AVehParBaseImpl
{
	/** Seat part type, handling all player interaction and updates. */
	public final PartTypeSeat type_seat;

	/** Seat color. */
	public PropColor prop_color;
	/** Color of seat paddings. */
	public PropColor prop_colorPadding;
	/** Color of seat handles. */
	public PropColor prop_colorHandles;
	/** Property holding the number of chairs that will be rendered from this part. */
	public PropInteger prop_amount;

	/** Property holding the cab's ventilation sound. */
	protected final PropSound soundAirConditioning;



	public VehParSeat()
	{
		this("VehParSeat", PartTypeSeat.class, 0.08F);
	}

	protected VehParSeat(String name, Class<? extends PartTypeSeat> clazz, float mass)
	{
		super(name, 0.25F, 0.575F, mass);

		/*
		 * Instantiate part type using reflection, as we can either have a seat or cab part type.
		 */
		try
		{
			type_seat = clazz.getConstructor(VehParBase.class).newInstance(this);
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}

		addProperty(prop_color = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color").setConfigurable());
		addProperty(prop_colorPadding = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_padding").setConfigurable());
		addProperty(prop_colorHandles = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_handles").setConfigurable());
		addProperty(prop_amount = (PropInteger)new PropInteger.PropIntegerBounded(this, 1, 3, "amount").setConfigurable());

		addProperty(soundAirConditioning = new PropSoundRepeated(this, ModData.ID + ":vehPar_seat_vent", "soundAirConditioning")
		{
			@Override
			public Integer get()
			{
				VehParSeat seat = (VehParSeat)getParent();
				if (seat.getTrain() != null && seat.getTrain().worldObj.isRemote)
				{
					if (!APartSounds.isPlayerInside(seat.getTrain()))
					{
						return 0;
					}

					if (type_seat.getPassenger() != Minecraft.getMinecraft().thePlayer)
					{
						return 0;
					}
				}

				return -1;
			}

			@Override
			protected BasicSound getNewSound(ResourceLocation resLoc)
			{
				BasicSound basicSound = super.getNewSound(resLoc);

				basicSound.setVolume(1.0F);
				basicSound.setPitch(1.0F);

				return basicSound;
			}
		});
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_Seat(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		OreDictionary.registerOre("vehparSeat", itemStack);

		ItemStack result = new ItemStack(itemStack.getItem(), 8);
		result.setTagCompound((NBTTagCompound)itemStack.getTagCompound().copy());
		OreDictionary.registerOre("vehparSeat", itemStack);
		GameRegistry.addRecipe(new ShapedOreRecipe(result, " CP", " CP", " P ", 'C', new ItemStack(Item.getItemFromBlock(Blocks.carpet), 1), 'P', "plateSteel", 'S', "ingotSteel"));
	}
}