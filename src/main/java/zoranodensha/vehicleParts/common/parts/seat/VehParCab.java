package zoranodensha.vehicleParts.common.parts.seat;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.util.Random;

import org.apache.logging.log4j.Level;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.property.PropSound.PropSoundRepeated;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.type.cab.AScreenRenderer;
import zoranodensha.api.vehicles.part.type.cab.EMTMSOperationMode;
import zoranodensha.api.vehicles.part.type.cab.PropButton;
import zoranodensha.api.vehicles.part.type.cab.PropButton.PropButtonScreen;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.api.vehicles.part.util.BufferedTexture;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.vehicleParts.client.render.seat.VehParRenderer_Cab;
import zoranodensha.vehicleParts.common.sounds.APartSounds;
import zoranodensha.vehicleParts.common.sounds.CabSounds;



/**
 * Modern cab with two screens which display basic information (such as speed, speed limits, throttle and reverser states).
 */
public class VehParCab extends VehParSeat
{
	/** RNG */
	private static final Random random = new Random();

	/*
	 * Properties and part type.
	 */
	/** This cab's part type. */
	public final PartTypeCabImpl type_cab;

	/** Property holding the desk's color. */
	public final PropColor color_desk;
	/** Property holding the alerter sound. */
	protected final PropSound soundAlerter;
	private float ventilationLevel = 0.0F;

	protected final PropSound soundVentilation;

	/** A wrapper class containing many sounds relating to the cab. */
	protected CabSounds cabSounds;

	/** {@code true} while this part is reading from NBT. */
	protected boolean isReadingFromNBT;


	/*
	 * Cab Controls
	 */
	@SideOnly(Side.CLIENT) public CabControl afbControl;
	// @SideOnly(Side.CLIENT) public CabControl airBrakeControl;
	@SideOnly(Side.CLIENT) public CabControl brakeModeControl;
	@SideOnly(Side.CLIENT) public CabControl cabLightControl;
	@SideOnly(Side.CLIENT) public CabControl doorControl;
	// @SideOnly(Side.CLIENT) public CabControl dynamicBrakeControl;
	// @SideOnly(Side.CLIENT) public CabControl headlightControl;
	@SideOnly(Side.CLIENT) public CabControl hornControl;
	@SideOnly(Side.CLIENT) public CabControl lightBeamControl;
	@SideOnly(Side.CLIENT) public CabControl tcsControl;
	@SideOnly(Side.CLIENT) public CabControl throttleControl;
	@SideOnly(Side.CLIENT) public CabControl trainLightControl;
	@SideOnly(Side.CLIENT) public Gauge gaugeTop;
	@SideOnly(Side.CLIENT) public Gauge gaugeBottom;


	/*
	 * Other Junk
	 */
	/** A counter which ticks down to allow for logic that occurs on the client every 10 ticks. */
	@SideOnly(Side.CLIENT) private int slowClientTick;
	@SideOnly(Side.CLIENT) public boolean indicatorLightParkBrake;
	@SideOnly(Side.CLIENT) public boolean indicatorLightStaffResponsible;



	public VehParCab()
	{
		super("VehParCab", PartTypeCabImpl.class, 0.16F);

		type_cab = (PartTypeCabImpl)type_seat;

		addProperty(color_desk = (PropColor)new PropColor(this, new ColorRGBA(0.9F, 0.9F, 0.9F), "color_desk").setConfigurable());
		addProperty(soundAlerter = new SoundAlerter(this));

		addProperty(soundVentilation = new PropSoundRepeated(this, ModData.ID + ":vehPar_cab_vent", "soundVentilation")
		{
			@Override
			public Integer get()
			{
				VehParCab cab = (VehParCab)getParent();
				if (cab.getTrain() != null && cab.getTrain().worldObj.isRemote)
				{
					if (!APartSounds.isPlayerInside(cab.getTrain()))
					{
						return 0;
					}
				}

				return ventilationLevel > 0.0F ? -1 : 0;
			}

			@Override
			protected BasicSound getNewSound(ResourceLocation resLoc)
			{
				BasicSound basicSound = super.getNewSound(resLoc);

				basicSound.setVolume(0.1F);
				basicSound.setPitch(0.5F);

				return basicSound;
			}
		});

		/*
		 * Initialise the cab sounds wrapper.
		 */
		cabSounds = new CabSounds(this);
	}

	/**
	 * Returns {@code true} if the riding entity is the local client player.
	 */
	@SideOnly(Side.CLIENT)
	public boolean getEntityIsPlayer()
	{
		return (type_seat.getPassenger() == Minecraft.getMinecraft().thePlayer);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		if (renderer == null)
		{
			renderer = new VehParRenderer_Cab(this);
		}
		return renderer;
	}


	/**
	 * Helper method to initialise all client-only cab controls.
	 */
	@SideOnly(Side.CLIENT)
	public void initControls()
	{
		/* Button and lever controls */
		float throttle = type_cab.powerBrakeControllerSmooth.get();

		afbControl = new CabControl(-30.0F, 30.0F, 1.30F, 8.0F, 0.0F);
		// airBrakeControl = new CabControl(-30.0F, 30.0F, 1.15F, 6.0F, (airBrake > 0.0F ? 30.0F + (airBrake * -60.0F) : 30.0F));
		brakeModeControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, (type_cab.button_brakeMode.get() ? 20.0F : 0.0F));
		cabLightControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_lights_cab.isActive() ? 20.0F : 0.0F);
		doorControl = new CabControl(-25.0F, 25.0F, 2.0F, 8.0F, 0.0F);
		// dynamicBrakeControl = new CabControl(-30.0F, 30.0F, 1.15F, 6.0F, (dynamicBrake > 0.0F ? 30.0F + (dynamicBrake * -60.0F) : 30.0F));
		// headlightControl = new CabControl(-5.0F, 365.0F, 1.40F, 60.0F, ((type_cab.lightMode.get().ordinal() + 5) * 90.0F % 360.0F));
		hornControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_horn.isActive() ? 20.0F : 0.0F);
		lightBeamControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_lights_beams.isActive() ? 20.0F : 0.0F);
		tcsControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_alerter.isActive() ? 20.0F : 0.0F);
		throttleControl = new CabControl(-30.0F, 30.0F, 1.15F, 6.0F, (throttle != 0.0F ? 0.0F + (throttle * 30.0F) : 0.0F));
		trainLightControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_lights_train.isActive() ? 20.0F : 0.0F);

		gaugeTop = new Gauge("bogie 1 cyl", "bogie 2 cyl", 5.0F, Color.RED, 0.0F, Color.YELLOW, 0.0F);
		gaugeBottom = new Gauge("brake pipe", "main reservoir", 10.0F, Color.RED, 0.0F, Color.YELLOW, 0.0F);
	}


	@Override
	public void onPropertyChanged(IPartProperty<?> property)
	{
		super.onPropertyChanged(property);

		/* Notify cab screens. */
		type_cab.mtms.onPropertyChanged(property);

		/*
		 * This section handles client-sided button changes.
		 */
		if (getTrain() != null && getTrain().worldObj.isRemote)
		{
			/*
			 * Button properties.
			 */
			if (property instanceof PropButton)
			{
				/* This is the button that changed. */
				PropButton button = (PropButton)property;

				/* Screen buttons trigger a beeping sound. */
				if (property instanceof PropButtonScreen)
				{
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						if (button.isActive())
						{
							type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_beep", 1.0F, 1.0F);
						}
					}
				}

				/* Alerter */
				else if (button == type_cab.button_alerter)
				{
					if (tcsControl != null)
					{
						tcsControl.setTarget(button.isActive() ? 20.0F : 0.0F);
					}
					if (button.isActive() && !isReadingFromNBT)
					{
						playSwitchSound();
					}
				}

				/*
				 * Brake Mode Switch
				 */
				else if (button == type_cab.button_brakeMode)
				{
					if (brakeModeControl != null)
					{
						brakeModeControl.setTarget(type_cab.button_brakeMode.get() ? 20.0F : 0.0F);
					}
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						type_cab.getPassenger().playSound(ModData.ID + (type_cab.button_brakeMode.get() ? ":vehPar_cab_breaker1" : ":vehPar_cab_breaker0"), 1.0F, 1.0F);
					}
				}

				/* Doors left */
				else if (button == type_cab.button_doors_left)
				{
					if (doorControl != null)
					{
						doorControl.setTarget(button.isActive() ? 20.0F : -20.0F);
					}
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						type_cab.getPassenger().playSound(ModData.ID + (type_cab.button_doors_left.get() ? ":vehPar_cab_switch1" : ":vehPar_cab_switch0"), 1.0F, 1.0F);
					}
				}

				/* Doors right */
				else if (button == type_cab.button_doors_right)
				{
					if (doorControl != null)
					{
						doorControl.setTarget(button.isActive() ? 20.0F : -20.0F);
					}
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						type_cab.getPassenger().playSound(ModData.ID + (type_cab.button_doors_right.get() ? ":vehPar_cab_switch1" : ":vehPar_cab_switch0"), 1.0F, 1.0F);
					}
				}

				/* Horn */
				else if (button == type_cab.button_horn)
				{
					if (hornControl != null)
					{
						hornControl.setTarget(button.isActive() ? 20.0F : 0.0F);
					}
					if (button.isActive() && !isReadingFromNBT)
					{
						playSwitchSound();
					}
				}

				/* Lights (beams) */
				else if (button == type_cab.button_lights_beams)
				{
					if (lightBeamControl != null)
					{
						lightBeamControl.setTarget(type_cab.button_lights_beams.isActive() ? 20.0F : 0.0F);
					}
					if (!isReadingFromNBT)
					{
						playSwitchSound();
					}
				}

				/* Lights (cab) */
				else if (button == type_cab.button_lights_cab)
				{
					if (cabLightControl != null)
					{
						cabLightControl.setTarget(type_cab.button_lights_cab.isActive() ? 20.0F : 0.0F);
					}
					if (!isReadingFromNBT)
					{
						playSwitchSound();
					}
				}

				/* Lights (train) */
				else if (button == type_cab.button_lights_train)
				{
					if (trainLightControl != null)
					{
						trainLightControl.setTarget(type_cab.button_lights_train.isActive() ? 20.0F : 0.0F);
					}
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						type_cab.getPassenger().playSound(ModData.ID + (type_cab.button_lights_train.isActive() ? ":vehPar_cab_switch1" : ":vehPar_cab_switch0"), 1.0F, 1.0F);
					}
				}

				/* Park Brake Apply Button */
				else if (button == type_cab.button_parkBrake_apply)
				{
					if (!isReadingFromNBT)
					{
						if (getEntityIsPlayer())
						{
							type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_button", 1.0F, 1.0F);
						}
					}
				}

				/* Park Brake Release Button */
				else if (button == type_cab.button_parkBrake_release)
				{
					if (!isReadingFromNBT)
					{
						if (getEntityIsPlayer())
						{
							type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_button", 1.0F, 1.0F);
						}
					}
				}
			}

			/*
			 * Other properties (Not Buttons)
			 */
			else
			{
				/* Reverser */
				if (property == type_cab.reverser)
				{
					/* Play sound to player if applicable. */
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						int reverserIndex;
						switch (type_cab.getReverserState())
						{
							default:
								reverserIndex = 0;
								break;

							case FORWARD:
								reverserIndex = 1;
								break;

							case BACKWARD:
								reverserIndex = 2;
								break;

							case NEUTRAL_BACKWARD:
							case NEUTRAL_FORWARD:
								reverserIndex = 3;
								break;
						}

						type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_reverser" + reverserIndex, 1.0F, 1.0F);
					}
				}

				// /* Brake */
				// else if (property == type_cab.airBrakeController)
				// {
				// if (airBrakeControl != null)
				// {
				// float brake = type_cab.airBrakeController.get();
				// airBrakeControl.setTarget(brake != 0.0F ? 30.0F + (brake * -60.0F) : 30.0F);
				// }
				// }

				// /* Dynamic Brake */
				// else if (property == type_cab.dynamicBrakeController)
				// {
				// if (dynamicBrakeControl != null)
				// {
				// float brake = type_cab.dynamicBrakeController.get();
				// dynamicBrakeControl.setTarget(brake != 0.0F ? 30.0F + (brake * -60.0F) : 30.0F);
				// }
				// }

				/* Throttle */
				else if (property == type_cab.powerBrakeController)
				{
					if (throttleControl != null)
					{
						float throttle = Math.abs(type_cab.powerBrakeController.get());
						throttleControl.setTarget(throttle != 0.0F ? -30.0F + (throttle * 60.0F) : -30.0F);
					}
				}
			}
		}
	}

	/**
	 * Called during the cab type's {@link PartTypeCabImpl#onTick() tick update} to update this part's visuals.
	 */
	protected void onUpdate()
	{
		/*
		 * Update sounds.
		 */
		if (cabSounds != null)
		{
			cabSounds.onUpdate();
		}

		/*
		 * Update visual things here, thus client side only.
		 */
		Train train = getTrain();
		if (!train.worldObj.isRemote || !getEntityIsPlayer())
		{
			return;
		}

		/* Instantiate control and gauge fields if necessary. */
		if (throttleControl == null || gaugeTop == null)
		{
			initControls();
		}

		/*
		 * Update Vent Sound
		 */
		if (!type_cab.getReverserState().equals(EReverser.LOCKED))
		{
			if (ventilationLevel < 1.0F)
			{
				ventilationLevel += 0.04F;
			}

			if (ventilationLevel > 1.0F)
			{
				ventilationLevel = 1.0F;
			}
		}
		else
		{
			if (ventilationLevel > 0.0F)
			{
				ventilationLevel -= 0.015F;
			}

			if (ventilationLevel < 0.0F)
			{
				ventilationLevel = 0.0F;
			}
		}
		if (soundVentilation != null && soundVentilation.sound != null)
		{
			soundVentilation.sound.setVolume(ventilationLevel);
			soundVentilation.sound.setPitch(0.5F + (ventilationLevel * 0.5F));
		}

		/*
		 * Quieten Air conditioning volume in the cab.
		 */
		if (soundAirConditioning.sound != null)
		{
			if (soundAirConditioning.sound.getVolume() != 0.5F)
			{
				soundAirConditioning.sound.setVolume(0.5F);
			}
		}

		/*
		 * Update Controls
		 */

		afbControl.setTarget(-30.0F + (type_cab.afbControllerSmooth.get() * 60.0F));
		afbControl.tick();
		// airBrakeControl.setTarget(30.0F + (type_cab.airBrakeControllerSmooth.get() * -60.0F));
		// airBrakeControl.tick();
		brakeModeControl.tick();
		cabLightControl.tick();
		doorControl.tick();
		// dynamicBrakeControl.setTarget(30.0F + (type_cab.dynamicBrakeControllerSmooth.get() * -60.0F));
		// dynamicBrakeControl.tick();
		hornControl.tick();
		lightBeamControl.tick();
		tcsControl.tick();
		throttleControl.setTarget((type_cab.powerBrakeControllerSmooth.get() * 30.0F));
		throttleControl.tick();
		trainLightControl.tick();


		/* Update gauges. */
		float parkBrakeCylinderPressure = 0.0F;
		{
			float auxReservoirPressure = 0.0F;
			float brakeCylinderPressure = 0.0F;
			float brakeCylinderPressureOther = 0.0F;
			float brakePipePressure = 0.0F;
			float mainReservoirPipePressure = 0.0F;

			/*
			 * Get the bogie underneath this cab.
			 */
			OrderedTypesList<PartTypeBogie> bogies = type_cab.getVehicle().getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
			PartTypeBogie bogie = bogies.get(0);
			PartTypeBogie bogieNext = bogies.get(bogies.size() - 1);
			if (bogie != null)
			{
				/* Auxiliary Reservoir */
				auxReservoirPressure = bogie.pneumaticsSupplementaryReservoir.getPressure();

				/* Brake Pipe */
				brakePipePressure = bogie.pneumaticsBrakePipe.getPressure();

				/* Brake Cylinders (for each bogie) */
				brakeCylinderPressure = bogie.pneumaticsBrakeCylinder.getPressure();
				if (bogieNext != null)
				{
					brakeCylinderPressureOther = bogieNext.pneumaticsBrakeCylinder.getPressure();
				}
				else
				{
					brakeCylinderPressureOther = brakeCylinderPressure;
				}

				/* Main Reservoir */
				mainReservoirPipePressure = bogie.pneumaticsMainReservoirPipe.getPressure();

				/* Park Brake */
				parkBrakeCylinderPressure = bogie.pneumaticsParkBrakeCylinder.getPressure();
			}


			gaugeTop.setNeedleAValue(brakeCylinderPressure / 100.0F);
			gaugeTop.setNeedleBValue(brakeCylinderPressureOther / 100.0F);
			gaugeTop.tick();

			gaugeBottom.setNeedleAValue(this.type_cab.brakeValve.getPressure() / 100.0F);
			gaugeBottom.setNeedleBValue(mainReservoirPipePressure / 100.0F);
			gaugeBottom.tick();
		}

		/*
		 * Slow Client Tick
		 */
		slowClientTick--;
		if (slowClientTick <= 0)
		{
			slowClientTick = 10;

			/*
			 * Update the park brake indicator light.
			 */
			boolean indicatorLightParkBrakeOld = indicatorLightParkBrake;
			indicatorLightParkBrake = parkBrakeCylinderPressure < 275.0F;
			if (indicatorLightParkBrake != indicatorLightParkBrakeOld)
			{
				this.updateVertexState = true;
			}

			/*
			 * Update the staff-responsible indicator light.
			 */
			boolean indicatorLightStaffResponsibleOld = indicatorLightStaffResponsible;
			indicatorLightStaffResponsible = type_cab.mtms.operationMode.get() != EMTMSOperationMode.FULL_SUPERVISION;
			{
				if (indicatorLightStaffResponsible != indicatorLightStaffResponsibleOld)
				{
					this.updateVertexState = true;
				}
			}
		}
	}

	/**
	 * <p>
	 * Helper method that plays a random switch sound to the player in this cab.
	 * </p>
	 * <p>
	 * <i>Client-side only.</i>
	 * </p>
	 */
	@SideOnly(Side.CLIENT)
	private void playSwitchSound()
	{
		if (getEntityIsPlayer())
		{
			type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_switch" + random.nextInt(2), 1.0F, 1.0F);
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt, ETagCall callType)
	{
		isReadingFromNBT = true;
		super.readFromNBT(nbt, callType);
		isReadingFromNBT = false;
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "MCM", "COC", "PSP", 'P', "plateSteel", 'O', new ItemStack(ModCenter.ItemPart, 1, 0), 'S', "vehparSeat", 'C', "circuitBasic", 'M', new ItemStack(ModCenter.ItemPart, 1, 3)));
	}



	/**
	 * Client-sided helper class containing render data of physical controls in the cab.
	 */
	@SideOnly(Side.CLIENT)
	public static class CabControl
	{

		/**
		 * The current state of the control. Depending on the control, this may be an
		 * angle or displacement.
		 */
		private float state;
		private float lastState;
		private float target;
		private float velocity;

		private final float min, max;
		private final float maxChange;
		private final float floppiness;



		public CabControl(float minimum, float maximum, float floppiness, float maxChange, float state)
		{
			this.min = minimum;
			this.max = maximum;
			this.maxChange = maxChange;
			this.floppiness = floppiness;
			this.target = this.lastState = this.state = state;
		}


		public boolean getIsMoving()
		{
			return lastState != state;
		}

		public float getState()
		{
			return state;
		}

		public float getState(float partialTick)
		{
			return lastState + ((state - lastState) * partialTick);
		}

		public float getTarget()
		{
			return target;
		}

		public void setTarget(float newTarget)
		{
			this.target = newTarget;
		}

		public void tick()
		{
			lastState = state;
			velocity += (target - state) / floppiness;

			if (velocity < -maxChange)
				velocity = -maxChange;
			else if (velocity > maxChange)
				velocity = maxChange;

			state += velocity;

			if (state < min)
			{
				state = min;
				velocity *= -0.20F;
			}
			else if (state > max)
			{
				state = max;
				velocity *= -0.20F;
			}
			else
			{
				velocity *= 0.32F;
			}
		}

	}


	/**
	 * Pressure gauges displayed in the cab.
	 */
	@SideOnly(Side.CLIENT)
	public static class Gauge extends BufferedTexture
	{
		protected static final Font FONT_DEFAULT = new Font(Font.SANS_SERIF, Font.PLAIN, 14);
		protected static final Font FONT_MINI = new Font(Font.SANS_SERIF, Font.PLAIN, 11);

		private final String topText;
		private final String bottomText;

		private final float maximumValue;

		private Color needleAColor;
		private Color needleBColor;

		private float needleAValue = 0.0F;
		private float needleBValue = 0.0F;
		private float needleAVelocity = 0.0F;
		private float needleBVelocity = 0.0F;
		private float needleATargetValue = 0.0F;
		private float needleBTargetValue = 0.0F;

		private boolean hasNeedleB = false;



		/**
		 * Initialises a new instance of the {@link zoranodensha.vehicleParts.common.parts.seat.VehParCab.Gauge} class, with a single needle.
		 * 
		 * @param needleColor - The color of the needle.
		 * @param needleStartingValue - The starting value of the needle.
		 */
		@SideOnly(Side.CLIENT)
		public Gauge(String topText, String bottomText, float maximumValue, Color needleColor, float needleStartingValue)
		{
			super(160, 160);

			this.topText = topText;
			this.bottomText = bottomText;

			this.maximumValue = maximumValue;

			this.needleAValue = needleStartingValue;
			this.needleAColor = needleColor;

			hasNeedleB = false;

			repaint();
		}

		/**
		 * Initialises a new instance of the {@link zoranodensha.vehicleParts.common.parts.seat.VehParCab.Gauge} class, with two needles.
		 * 
		 * @param needleAColor - The colour of the 'A' needle.
		 * @param needleAStartingValue - The starting value of the 'A' needle.
		 * @param needleBColor - The color of the 'B' needle.
		 * @param needleBStartingValue - The starting value of the 'B' needle.
		 */
		@SideOnly(Side.CLIENT)
		public Gauge(String topText, String bottomText, float maximumValue, Color needleAColor, float needleAStartingValue, Color needleBColor, float needleBStartingValue)
		{
			super(160, 160);

			this.topText = topText;
			this.bottomText = bottomText;

			this.maximumValue = maximumValue;

			this.needleAValue = needleAStartingValue;
			this.needleAColor = needleAColor;

			this.needleBValue = needleBStartingValue;
			this.needleBColor = needleBColor;
			this.hasNeedleB = true;

			repaint();
		}


		@SideOnly(Side.CLIENT)
		public void repaint()
		{
			Graphics2D g = getGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setBackground(new Color(0.1F, 0.1F, 0.1F));

			int centreX = getWidth() / 2;
			int centreY = getHeight() / 2;
			int maximumNotch = (int)Math.ceil(maximumValue);
			float notchRotation = 270.0F / (float)maximumNotch;

			g.clearRect(0, 0, getWidth(), getHeight());

			/*
			 * Notches
			 */
			AffineTransform transform = g.getTransform();
			{
				// Rotate to the starting angle, the first notch.
				g.rotate(ZnDMathHelper.RAD_MULTIPLIER * -135.0D, centreX, centreY);

				// Go through all notches and render them.
				for (int i = 0; i <= maximumNotch; i++)
				{
					g.setColor(Color.WHITE);
					g.fillRect(centreX - 1, centreY - 73, 2, 11);
					g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (notchRotation / 4.0F), centreX, centreY);

					for (int j = 0; j < 3; j++)
					{
						if (i < maximumNotch)
						{
							g.setColor(Color.LIGHT_GRAY);
							g.fillRect(centreX, centreY - 73, 1, 8);
							g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (notchRotation / 4.0F), centreX, centreY);
						}
					}
				}
			}
			g.setTransform(transform);

			/*
			 * Line
			 */
			// g.setColor(Color.BLACK);
			// g.drawArc(centreX - 55, centreY - 55, 110, 110, -45, 270);

			/*
			 * Numbers
			 */
			{
				g.setColor(Color.WHITE);
				g.setFont(FONT_DEFAULT);

				float rotation = -135.0F;

				for (int i = 0; i <= maximumNotch; i++)
				{
					int numberX = centreX + (int)(Math.sin(ZnDMathHelper.RAD_MULTIPLIER * rotation) * 53.0F);
					int numberY = centreX - (int)(Math.cos(ZnDMathHelper.RAD_MULTIPLIER * rotation) * 53.0F);

					AScreenRenderer.drawCenteredString(g, String.valueOf(i), numberX, numberY);

					rotation += notchRotation;
				}
			}

			/*
			 * Subtitle
			 */
			{
				g.setFont(FONT_MINI);
				g.setColor(Color.LIGHT_GRAY);
				AScreenRenderer.drawCenteredString(g, "ZnD", centreX, centreY + 45);
				AScreenRenderer.drawCenteredString(g, "x100 kPa", centreX, centreY + 60);

				// g.setFont(FONT_MINI);
				// g.setColor(Color.LIGHT_GRAY);
				//
				// if (!topText.isEmpty())
				// {
				// AScreenRenderer.drawCenteredString(g, topText, centreX, centreY + 50);
				// }
				//
				// if (!bottomText.isEmpty())
				// {f
				// AScreenRenderer.drawCenteredString(g, bottomText, centreX, centreY + 60);
				// }
			}

			/*
			 * Needle B
			 */
			if (hasNeedleB)
			{
				{
					g.setColor(needleBColor);

					/*
					 * Rotate the needle to face its value.
					 */
					g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (-135.0D + (needleBValue * notchRotation)), getWidth() / 2, getHeight() / 2);

					/*
					 * Render the shape of the needle.
					 */
					g.fillPolygon(	new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 9, centreX - 5, centreX - 2 },
									new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
					g.fillOval(centreX - 6, centreY - 6, 12, 12);

					/*
					 * Draw a darker outline of the needle to increase visibility.
					 */
					g.setColor(needleBColor.brighter());
					{
						g.drawPolygon(	new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 0, centreX - 5, centreX - 2 },
										new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
					}

					/*
					 * Render a black 'pin' in the centre of the needle, for what it rotates on.
					 */
					g.setColor(Color.DARK_GRAY);
					g.fillOval(centreX - 2, centreY - 2, 4, 4);
				}
				g.setTransform(transform);
			}

			/*
			 * Needle A
			 */
			{
				g.setColor(needleAColor);

				/*
				 * Rotate the needle to face its value.
				 */
				g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (-135.0D + (needleAValue * notchRotation)), getWidth() / 2, getHeight() / 2);

				/*
				 * Render the needle's polygon shape.
				 */
				g.fillPolygon(	new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 9, centreX - 5, centreX - 2 },
								new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
				g.fillOval(centreX - 6, centreY - 6, 12, 12);

				/*
				 * Render the needle's darker outline.
				 */
				g.setColor(needleAColor.brighter());
				{
					g.drawPolygon(	new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 0, centreX - 5, centreX - 2 },
									new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
				}

				/*
				 * Render the needle's rotation pin in the centre.
				 */
				g.setColor(Color.DARK_GRAY);
				g.fillOval(centreX - 2, centreY - 2, 4, 4);

			}
			g.setTransform(transform);

			/*
			 * Reload the texture
			 */
			loadTexture();
		}


		@SideOnly(Side.CLIENT)
		public void setNeedleAValue(float newValue)
		{
			this.needleATargetValue = newValue;
		}

		public void setNeedleBValue(float newValue)
		{
			this.needleBTargetValue = newValue;
		}


		@SideOnly(Side.CLIENT)
		public void tick()
		{

			float needleAValuePrevious = needleAValue;
			float needleBValuePrevious = needleBValue;
			needleAVelocity += (needleATargetValue - needleAValue) / 3.0F;
			needleBVelocity += (needleBTargetValue - needleBValue) / 3.0F;
			needleAValue += needleAVelocity;
			needleBValue += needleBVelocity;

			if (needleAValue < -0.1F)
			{
				needleAValue = -0.1F;
				needleAVelocity = 0.0F;
			}
			else if (needleAValue > 11.0F)
			{
				needleAValue = 11.0F;
				needleAVelocity = 0.0F;
			}
			else
			{
				needleAVelocity *= 0.40F;
			}

			if (needleBValue < -0.1F)
			{
				needleBValue = -0.1F;
				needleBVelocity = 0.0F;
			}
			else if (needleBValue > 11.0F)
			{
				needleBValue = 11.0F;
				needleBVelocity = 0.0F;
			}
			else
			{
				needleBVelocity *= 0.40F;
			}

			if (needleAValuePrevious != needleAValue || needleBValuePrevious != needleBValue || getTextureID() == -1)
			{
				repaint();
			}
		}
	}


	/**
	 * Alerter sound class.
	 */
	public static class SoundAlerter extends PropSoundRepeated
	{
		public SoundAlerter(VehParCab parent)
		{
			super(parent, ModData.ID + ":vehPar_cab_alerter", "soundAlerter");
		}

		@Override
		public Integer get()
		{
			VehParCab cab = (VehParCab)getParent();
			if (cab.getTrain() != null && cab.getTrain().worldObj.isRemote)
			{
				/* On client side, only play Alerter sound if the player is the driver. */
				if (!cab.getEntityIsPlayer())
				{
					return 0;
				}
			}
			return cab.type_cab.alerter.getDoAlert() ? -1 : 0;
		}
	}
}
