package zoranodensha.vehicleParts.common.parts.seat;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.type.cab.DDU;
import zoranodensha.api.vehicles.part.type.cab.EDDUMenu;
import zoranodensha.api.vehicles.part.type.cab.PropButton;
import zoranodensha.api.vehicles.part.type.cab.PropLever;
import zoranodensha.api.vehicles.part.type.cab.PropButton.EButtonSize;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.api.vehicles.part.type.seat.EntitySeat;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



/**
 * Custom cab type implementation offering a tick hook for the parent vehicle part and specifying button hover coordinates.
 */
public class PartTypeCabImpl extends PartTypeCab
{
	protected final PropButton cabButton_reverser_backward;
	protected final PropButton cabButton_reverser_forward;
	protected final PropButton cabButton_reverser_neutral;

	/*
	 * Cab Screens
	 */
	public final DDU dduL;
	public final DDU dduR;

	/*
	 * Levers
	 */
	/** Cab throttle, ranging from {@code -1.0F} (maximum brakes) to {@code 1.0F} (maximum acceleration). */
	public final PropLever powerBrakeController;


	/**
	 * An AFB controller which is animated on the client side and is used to control the server-side AFB controller.
	 */
	public PropLever afbControllerSmooth;


	/**
	 * A power controller which is animated on the client side and controls the server-side power controller.
	 */
	public PropLever powerBrakeControllerSmooth;



	public PartTypeCabImpl(VehParBase parent)
	{
		super(parent);

		/* Ensure the parent is a valid type. */
		if (!(parent instanceof VehParCab))
		{
			throw new IllegalArgumentException("Supplied parent was no " + VehParCab.class.getSimpleName() + "!");
		}

		/*
		 * Screens
		 */
		dduL = new DDU(this.mtms, "left", EDDUMenu.MENU_OPERATION);
		dduR = new DDU(this.mtms, "right", EDDUMenu.MENU_INFORMATION);

		/*
		 * Un-animated Controllers
		 */
		addProperty(powerBrakeController = (PropLever)new PropLever(this, "throttle", 8, PropLever.DEFAULT_LEVER_SPEED, true).setSyncDir(ESyncDir.CLIENT));

		/*
		 * Animated Controllers
		 */
		addProperty(afbControllerSmooth = (PropLever)new PropLever(this, "afbSmooth", 32, 0.008F).setSyncDir(ESyncDir.SERVER));
		addProperty(powerBrakeControllerSmooth = (PropLever)new PropLever(this, "throttleSmooth", 8, PropLever.DEFAULT_LEVER_SPEED, true).setSyncDir(ESyncDir.SERVER));


		/* Add separate reverser buttons to make the reverser clickable. */
		addProperty(cabButton_reverser_backward = new PropButton(this, null, "reverser_backward")
		{
			@Override
			protected void onPressed()
			{
				reverser.set((reverser.get() == EReverser.BACKWARD) ? EReverser.LOCKED : EReverser.BACKWARD);
			}
		});

		addProperty(cabButton_reverser_forward = new PropButton(this, null, "reverser_forward")
		{
			@Override
			protected void onPressed()
			{
				reverser.set((reverser.get() == EReverser.FORWARD) ? EReverser.LOCKED : EReverser.FORWARD);
			}
		});

		addProperty(cabButton_reverser_neutral = new PropButton(this, null, "reverser_neutral")
		{
			@Override
			protected void onPressed()
			{
				/* Try to set whichever works first. */
				if (!reverser.set((reverser.get() == EReverser.NEUTRAL_BACKWARD) ? EReverser.LOCKED : EReverser.NEUTRAL_BACKWARD))
				{
					reverser.set((reverser.get() == EReverser.NEUTRAL_FORWARD) ? EReverser.LOCKED : EReverser.NEUTRAL_FORWARD);
				}
			}
		});

		/*
		 * Initialise button positions in helper method.
		 */
		initButtonPositions();
	}


	@Override
	public float getBrakeLevelPhysical()
	{
		if (powerBrakeController.get() <= 0.0F)
		{
			return Math.abs(powerBrakeController.get());
		}
		else
		{
			return 0.0F;
		}
	}


	@Override
	public float getDynamicBrakeLevelPhysical()
	{
		if (powerBrakeController.get() <= 0.0F && !button_brakeMode.get())
		{
			return Math.abs(powerBrakeController.get());
		}
		else
		{
			return 0.0F;
		}
	}


	@Override
	public float getThrottlePhysical()
	{
		if (powerBrakeController.get() >= 0.0F)
		{
			return powerBrakeController.get();
		}
		else
		{
			return 0.0F;
		}
	}


	/**
	 * Helper method used to initialise all button positions.
	 */
	private void initButtonPositions()
	{
		//@formatter:off
		
		/*
		 * posX		Forward/Backward (Blender +X), Add 0.4 to get MC value
		 * posY		Up/Down			 (Blender +Z), Minus 0.08 to get MC value
		 * posZ		Left/Right		 (Blender -Y), Same
		 */
		
		/* Mouse Control Coordinates */
		button_horn.setPositionAndSize(						posX(-0.0725), posY( 0.0340), posZ( 0.3980), EButtonSize.MEDIUM);
		button_alerter.setPositionAndSize(					posX(-0.0890), posY( 0.0340), posZ( 0.3675), EButtonSize.MEDIUM);
		button_lights_beams.setPositionAndSize(				posX(-0.1410), posY( 0.0340), posZ( 0.2710), EButtonSize.MEDIUM);
		button_lights_cab.setPositionAndSize(				posX(-0.1575), posY( 0.0340), posZ( 0.2405), EButtonSize.MEDIUM);
		button_lights_train.setPositionAndSize(				posX(-0.1740), posY( 0.0340), posZ( 0.2100), EButtonSize.MEDIUM);

		/* Reverser Buttons */
		cabButton_reverser_backward.setPositionAndSize(		posX(-0.1450), posY( 0.0340), posZ( 0.1400), EButtonSize.MEDIUM);
		cabButton_reverser_neutral.setPositionAndSize(		posX(-0.1700), posY( 0.0340), posZ( 0.1400), EButtonSize.MEDIUM);
		cabButton_reverser_forward.setPositionAndSize(		posX(-0.1950), posY( 0.0340), posZ( 0.1400), EButtonSize.MEDIUM);

		/* Train Lights and Door Buttons */
//		button_lights_headlights.setPositionAndSize(		posX(-0.1615), posY( 0.0340), posZ(-0.2333), EButtonSize.MEDIUM);
		button_doors_left.setPositionAndSize(				posX( 0.0350), posY( 0.0340), posZ(-0.4100), EButtonSize.MEDIUM);
		button_doors_right.setPositionAndSize(				posX( 0.0350), posY( 0.0340), posZ(-0.4650), EButtonSize.MEDIUM);
		
		/* Breakers and Switches */
		button_brakeMode.setPositionAndSize(				posX(-0.1932), posY( 0.0340), posZ(-0.1745), EButtonSize.MEDIUM);
		button_parkBrake_apply.setPositionAndSize(			posX(-0.0950), posY( 0.0342), posZ(-0.3659), EButtonSize.SMALL );
		button_parkBrake_release.setPositionAndSize(		posX(-0.0900), posY( 0.0342), posZ(-0.3923), EButtonSize.SMALL ); 

		/* Left DDU Buttons (Left Side) */
		dduL.screenButton_brightness.setPositionAndSize(	posX(-0.2600), posY( 0.1650), posZ( 0.1120), EButtonSize.MEDIUM);
		dduL.screenButton_contrast.setPositionAndSize(		posX(-0.2500), posY( 0.1465), posZ( 0.1120), EButtonSize.MEDIUM);
		dduL.screenButton_swap.setPositionAndSize(			posX(-0.2325), posY( 0.0905), posZ( 0.1120), EButtonSize.MEDIUM);
		dduL.screenButton_menu.setPositionAndSize(			posX(-0.2250), posY( 0.0715), posZ( 0.1120), EButtonSize.MEDIUM);
		/** Left DDU Buttons (Right Side) */
		dduL.screenButton_up.setPositionAndSize(			posX(-0.2600), posY( 0.1650), posZ(-0.1120), EButtonSize.MEDIUM);
		dduL.screenButton_left.setPositionAndSize(			posX(-0.2500), posY( 0.1465), posZ(-0.1120), EButtonSize.MEDIUM);
		dduL.screenButton_right.setPositionAndSize(			posX(-0.2465), posY( 0.1277), posZ(-0.1120), EButtonSize.MEDIUM);
		dduL.screenButton_down.setPositionAndSize(			posX(-0.2395), posY( 0.1095), posZ(-0.1120), EButtonSize.MEDIUM);
		dduL.screenButton_enter.setPositionAndSize(			posX(-0.2285), posY( 0.0800), posZ(-0.1120), EButtonSize.MEDIUM);

		/* Right DDU Buttons (Left Side) */
		dduR.screenButton_brightness.setPositionAndSize(	posX(-0.2490), posY( 0.1680), posZ(-0.2230), EButtonSize.MEDIUM);
		dduR.screenButton_contrast.setPositionAndSize(		posX(-0.2425), posY( 0.1495), posZ(-0.2200), EButtonSize.MEDIUM);
		dduR.screenButton_swap.setPositionAndSize(			posX(-0.2185), posY( 0.0905), posZ(-0.2080), EButtonSize.MEDIUM);
		dduR.screenButton_menu.setPositionAndSize(			posX(-0.2113), posY( 0.0710), posZ(-0.2050), EButtonSize.MEDIUM);
		/* Right DDU Buttons (Right Side) */
		dduR.screenButton_up.setPositionAndSize(			posX(-0.1380), posY( 0.1650), posZ(-0.4215), EButtonSize.MEDIUM);
		dduR.screenButton_left.setPositionAndSize(			posX(-0.1350), posY( 0.1475), posZ(-0.4175), EButtonSize.MEDIUM);
		dduR.screenButton_right.setPositionAndSize(			posX(-0.1280), posY( 0.1310), posZ(-0.4135), EButtonSize.MEDIUM);
		dduR.screenButton_down.setPositionAndSize(			posX(-0.1210), posY( 0.1125), posZ(-0.4095), EButtonSize.MEDIUM);
		dduR.screenButton_enter.setPositionAndSize(			posX(-0.1080), posY( 0.0805), posZ(-0.4035), EButtonSize.MEDIUM);
		
		//@formatter:on
	}

	@Override
	public void onClientKeyChange(ECabKey cabKey, boolean isKeyDown)
	{
		super.onClientKeyChange(cabKey, isKeyDown);

		/*
		 * Animated smooth levers
		 */
		switch (cabKey)
		{
			case AFB_DECREASE: {
				afbControllerSmooth.decreasing = isKeyDown;
				break;
			}

			case AFB_INCREASE: {
				afbControllerSmooth.increasing = isKeyDown;
				break;
			}

			case THROTTLE_DECREASE: {
				powerBrakeControllerSmooth.decreasing = isKeyDown;
				break;
			}

			case THROTTLE_INCREASE: {
				powerBrakeControllerSmooth.increasing = isKeyDown;
				break;
			}

			default:
				break;
		}

		/* Special case for door buttons; if the key was released, reset the lever control */
		if (!isKeyDown)
		{
			PropButton button = buttonMap.get(cabKey);
			if (button == button_doors_left || button == button_doors_right)
			{
				((VehParCab)getParent()).doorControl.setTarget(0.0F);
			}
		}
	}

	@Override
	public void onTick()
	{
		super.onTick();
		((VehParCab)getParent()).onUpdate();

		/*
		 * DDU screens
		 */
		dduL.onUpdate();
		dduR.onUpdate();

		/*
		 * Client-side Logic
		 */
		if (getTrain().worldObj.isRemote)
		{
			Integer result;

			/*
			 * Power Controller
			 */
			result = powerBrakeControllerSmooth.tick(true);
			if (result != null && getPassenger() != null)
			{
				switch (result)
				{
					case 8: {
						getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle2", 1.0F, 1.0F);
						break;
					}

					case 1: {
						if (powerBrakeControllerSmooth.increasing)
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle1", 1.0F, 1.0F);
						}
						break;
					}

					case 0: {
						getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle0", 1.0F, 1.0F);
						break;
					}

					case -1: {
						if (powerBrakeControllerSmooth.decreasing)
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle1", 1.0F, 1.0F);
						}
						break;
					}

					case -7: {
						if (powerBrakeControllerSmooth.decreasing)
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle3", 1.0F, 1.0F);
						}
						else if (powerBrakeControllerSmooth.increasing)
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle4", 1.0F, 1.0F);
						}
						break;
					}

					case -8: {
						getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle5", 1.0F, 1.0F);
						break;
					}
				}
			}
			// if ((result >= 0 && result <= 1) || result == 8)
			// {
			// if (result == 8)
			// {
			// result = 2;
			// }
			// getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle" + result, 1.0F, 1.0F);
			// }

			/*
			 * AFB Controller
			 */
			result = afbControllerSmooth.tick(true);
			// if (result != null)
			// {
			// switch (result)
			// {
			// case 0: {
			// getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle0", 1.0F, 1.0F);
			// break;
			// }
			// case 1: {
			// getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle1", 1.0F, 1.0F);
			// break;
			// }
			// case 32: {
			// getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle1", 1.0F, 1.0F);
			// break;
			// }
			//
			//
			// default: {
			// if (result % 4 == 0)
			// {
			// getPassenger().playSound(ModData.ID + ":vehPar_cab_throttle1", 0.40F, 1.0F);
			// }
			// }
			// }
			// }
		}

		/*
		 * Server-side Logic
		 */
		else
		{
			/*
			 * AFB Controller
			 */
			if (afbController.getCurrentNotch() != afbControllerSmooth.getCurrentNotch())
			{
				afbController.setNotch(afbControllerSmooth.getCurrentNotch());
			}

			/*
			 * Power Controller
			 */
			if (powerBrakeController.getCurrentNotch() != powerBrakeControllerSmooth.getCurrentNotch())
			{
				powerBrakeController.setNotch(powerBrakeControllerSmooth.getCurrentNotch());
			}
		}
	}

	private static float posX(double x)
	{
		return (float)-((x - 0.4) * 0.8 - 0.075);
	}

	private static float posY(double y)
	{
		return (float)((y + 0.075) * 0.8);
	}

	private static float posZ(double z)
	{
		return (float)-(z * 0.8);
	}
}