package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_ElectricsFLIRT;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public class VehParElectricsFLIRT extends AVehParBaseImpl
{
	/** {@link EType Type} of this part. */
	public PropEnum<EType> prop_type;



	public VehParElectricsFLIRT()
	{
		super("VehParElectricsFLIRT", 0.5F, 0.125F, 0.25F);
		addProperty(prop_type = (PropEnum<EType>)new PropEnum<EType>(this, EType.M, "type").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ElectricsFLIRT(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " C ", "CPC", " C ", 'P', new ItemStack(ModCenter.ItemPart, 1, 7), 'C', "circuitBasic"));
	}



	public static enum EType
	{
		/** Wide, flat electrics. */
		L,

		/** Sightly thinner, thicker electrics. */
		M;
	}
}
