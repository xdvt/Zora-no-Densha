package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_StairsDBpza;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParStairsDBpza extends AVehParBaseImpl
{
	/** Property holding this part's color. */
	public PropColor color;
	/** Floor color. */
	public PropColor color_baseplate;



	public VehParStairsDBpza()
	{
		super("VehParStairsDBpza", 0.350F, 0.72F, 0.15F);
		addProperty(color = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color").setConfigurable());
		addProperty(color_baseplate = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_baseplate").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_StairsDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		ItemStack result = new ItemStack(itemStack.getItem(), 2, itemStack.getItemDamage());
		result.setTagCompound(itemStack.getTagCompound());
		GameRegistry.addRecipe(new ShapedOreRecipe(result, "  P", " PS", "PSS", 'P', "plateSteel", 'S', "ingotSteel"));
	}
}