package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeLamp;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_Lamp;
import zoranodensha.vehicleParts.common.parts.basic.AVehParBasic;



/**
 * A part which is comprised of a light 'flare' and a light 'cone'. This part can be placed on top of a headlight to create a realistic-looking high beam effect.
 * 
 * @author Jaffa
 */
public class VehParLamp extends AVehParBasic
{
	/** Lamp part type, dealing with actual functionality. */
	public final PartTypeLamp type_lamp;



	public VehParLamp()
	{
		super("VehParLamp", 0.1F, 0.1F, 0.02F);

		type_lamp = new PartTypeLamp(this);
		color.set(ColorRGBA.WHITE.clone().setAlpha(0.8F).toHEX());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return renderer == null ? renderer = new VehParRenderer_Lamp(this) : renderer;
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "   ", "CL ", "   ", 'C', "circuitBasic", 'L', new ItemStack(Blocks.redstone_lamp)));
	}
}