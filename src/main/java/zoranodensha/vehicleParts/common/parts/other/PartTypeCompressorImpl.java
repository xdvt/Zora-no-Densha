package zoranodensha.vehicleParts.common.parts.other;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.property.PropSound.PropSoundRepeated;
import zoranodensha.api.vehicles.part.type.PartTypeCompressor;
import zoranodensha.common.core.ModData;



/**
 * An implementation of {@link zoranodensha.api.vehicles.part.type.PartTypeCompressor} providing properties and methods for sounds
 * 
 * @author Jaffa
 */
public class PartTypeCompressorImpl extends PartTypeCompressor
{

	/** The sound of the compressor when it's running. */
	public PropSoundRepeated soundCompressorRunning;
	/** The sound effect to play when the compressor stops running. */
	public PropSound soundCompressorStop;

	private boolean playedStopSound = false;



	/**
	 * Initialises a new instance of the {@link zoranodensha.vehicleParts.common.parts.other.PartTypeCompressorImpl} class.
	 * 
	 * @param parent - The parent vehicle part which owns this instance.
	 */
	public PartTypeCompressorImpl(VehParBase parent)
	{
		super(parent);

		/*
		 * Ensure the parent class is of a valid type.
		 */
		if (!(parent instanceof VehParCompressor))
		{
			throw new IllegalArgumentException("Supplied parent was not a " + VehParCompressor.class.getSimpleName() + "! >:(");
		}

		/*
		 * Sounds
		 */
		addProperty(soundCompressorRunning = new PropSoundRepeated(parent, ModData.ID + ":vehPar_compressor_loop", "soundCompressorRunning"));
		addProperty(soundCompressorStop = new PropSound(parent, ModData.ID + ":vehPar_compressor_stop", "soundCompressorStop"));
	}

	@Override
	public void onTick()
	{
		super.onTick();

		if (!parent.getTrain().worldObj.isRemote)
		{
			/*
			 * Update the compressor sounds.
			 */
			if (compressorEnabled.get())
			{
				soundCompressorRunning.play(6);
				playedStopSound = false;
			}
			else if (!playedStopSound)
			{
				soundCompressorStop.play();
				playedStopSound = true;
			}
		}
	}

}
