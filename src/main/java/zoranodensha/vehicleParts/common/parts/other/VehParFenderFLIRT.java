package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_FenderFLIRT;
import zoranodensha.vehicleParts.common.parts.basic.AVehParBasic;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParFenderFLIRT extends AVehParBasic
{
	public VehParFenderFLIRT()
	{
		super("VehParFenderFLIRT", 0.03375F, 0.1F, 0.04F);
		color.set(ELiveryColor.GREY_LIGHT.color());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_FenderFLIRT(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "   ", "RP ", "   ", 'P', "plateSteel", 'R', "dyeRed"));
	}
}
