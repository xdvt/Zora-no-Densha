package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_DetailSWF;
import zoranodensha.vehicleParts.common.parts.basic.AVehParBasic;
import zoranodensha.vehicleParts.presets.ELiveryColor;
import zoranodensha.vehicleParts.presets.SlidingWallFreight_S;
import zoranodensha.vehicleParts.presets.SlidingWallFreight_S.EDetailType;



public class VehParDetailSWF extends AVehParBasic
{
	/** {@link EDetailType Type} of this part. */
	public PropEnum<EDetailType> prop_type;



	/**
	 * Blank constructor - will use default detail type {@link SlidingWallFreight_S.EDetailType#VALVE}.
	 */
	public VehParDetailSWF()
	{
		this(EDetailType.VALVE);
	}

	public VehParDetailSWF(EDetailType type)
	{
		super("VehParDetailSWF", 0.2F, 0.2F, 0.2F);

		addProperty(prop_type = (PropEnum<EDetailType>)new PropEnum<EDetailType>(this, type, "type").setConfigurable());
		color.set(ELiveryColor.GOLD.color());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_DetailSWF(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " N ", "NIN", " N ", 'N', "nuggetSteel", 'I', "ingotSteel"));
	}
}