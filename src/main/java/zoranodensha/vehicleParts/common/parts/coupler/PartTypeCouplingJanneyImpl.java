package zoranodensha.vehicleParts.common.parts.coupler;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypeCouplingJanney;



/**
 * Custom Janney coupling part type hooking into the parent to play sounds where desired.
 */
public class PartTypeCouplingJanneyImpl extends PartTypeCouplingJanney
{
	public PartTypeCouplingJanneyImpl(VehParBase parent)
	{
		super(parent);

		/* Ensure the parent is a valid type. */
		if (!(parent instanceof VehParCouplerJanney))
		{
			throw new IllegalArgumentException("Supplied parent was no " + VehParCouplerJanney.class.getSimpleName() + "!");
		}
	}

	@Override
	protected boolean doTryCouple()
	{
		if (super.doTryCouple())
		{
			((VehParCouplerJanney)getParent()).playSound(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean doTryDecouple()
	{
		if (super.doTryDecouple())
		{
			((VehParCouplerJanney)getParent()).playSound(false);
			return true;
		}
		return false;
	}
}