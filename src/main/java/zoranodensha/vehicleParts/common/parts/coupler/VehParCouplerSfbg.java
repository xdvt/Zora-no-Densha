package zoranodensha.vehicleParts.common.parts.coupler;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.coupler.VehParRenderer_CouplerSfbg;



/**
 * Scharfenberg couplers are completely automatic and need a cab to control them.<br>
 * They <i>couple and decouple automatically.</i>
 */
public class VehParCouplerSfbg extends AVehParCouplerBase
{
	/** This coupling's part type. */
	public final PartTypeCouplingScharfenbergImpl type_coupling;

	/** Property holding the type of this coupler. {@code 0} = no extensions, {@code 1} = side extensions, {@code 2} top extension. */
	public PropIntegerBounded prop_type;
	/** Property holding the color of the coupler. */
	public PropColor color;
	/** Property holding the color of the coupler's extension. */
	public PropColor color_extension;



	public VehParCouplerSfbg()
	{
		super("VehParCouplerSfbg", 0.63F, 0.075F, 0.1F);

		type_coupling = new PartTypeCouplingScharfenbergImpl(this);
		type_coupling.expansion.set(1.000F);
		
		addProperty(prop_type = (PropIntegerBounded)new PropIntegerBounded(this, 0, 2, "type").setConfigurable());
		addProperty(color = (PropColor)new PropColor(this, new ColorRGBA(0.20F, 0.19F, 0.19F), "color").setConfigurable());
		addProperty(color_extension = (PropColor)new PropColor(this, new ColorRGBA(0.11F, 0.11F, 0.11F), "color_extension")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return (prop_type.get() != 0);
			};
		}.setConfigurable());
	}

	@Override
	protected String getCoupleSound()
	{
		return "vehPar_coupler_sfbg_couplesound";
	}

	@Override
	protected String getDecoupleSound()
	{
		return "vehPar_coupler_sfbg_couplesound";
	}

	@Override
	public float getLength()
	{
		return type_coupling.getLength();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_CouplerSfbg(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " S ", " S ", "PCP", 'S', "ingotSteel", 'P', "plateSteel", 'C', "circuitBasic"));
	}
}