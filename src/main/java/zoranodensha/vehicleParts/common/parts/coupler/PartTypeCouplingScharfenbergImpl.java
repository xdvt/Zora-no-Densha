package zoranodensha.vehicleParts.common.parts.coupler;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypeCouplingScharfenberg;



/**
 * Custom Scharfenberg coupling part type hooking into the parent to play sounds where desired.
 */
public class PartTypeCouplingScharfenbergImpl extends PartTypeCouplingScharfenberg
{
	public PartTypeCouplingScharfenbergImpl(VehParBase parent)
	{
		super(parent);

		/* Ensure the parent is a valid type. */
		if (!(parent instanceof VehParCouplerSfbg))
		{
			throw new IllegalArgumentException("Supplied parent was no " + VehParCouplerSfbg.class.getSimpleName() + "!");
		}
	}

	@Override
	protected boolean doTryCouple()
	{
		if (super.doTryCouple())
		{
			((VehParCouplerSfbg)getParent()).playSound(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean doTryDecouple()
	{
		if (super.doTryDecouple())
		{
			((VehParCouplerSfbg)getParent()).playSound(false);
			return true;
		}
		return false;
	}
}