package zoranodensha.vehicleParts.common.parts.coupler;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.coupler.VehParRenderer_CouplerJanney;



/**
 * Janney couplers are an intermediate between screw- and Scharfenberg couplers;<br>
 * They <i>couple automatically, but decouple manually.</i>
 */
public class VehParCouplerJanney extends AVehParCouplerBase
{
	/** This coupling's part type. */
	public final PartTypeCouplingJanneyImpl type_coupling;

	/** Property holding the color of the coupler. */
	public PropColor color;



	public VehParCouplerJanney()
	{
		super("VehParCouplerJanney", 0.474F, 0.075F, 0.1F);

		type_coupling = new PartTypeCouplingJanneyImpl(this);
		type_coupling.expansion.set(1.000F);
		addProperty(color = (PropColor)new PropColor(this, new ColorRGBA(0.44F, 0.21F, 0.06F), "color").setConfigurable());
	}

	@Override
	public float getLength()
	{
		return type_coupling.getLength();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_CouplerJanney(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " S ", " S ", "PLP", 'S', "ingotSteel", 'P', "plateSteel", 'C', "circuitBasic", 'L', Item.getItemFromBlock(Blocks.lever)));
	}
}