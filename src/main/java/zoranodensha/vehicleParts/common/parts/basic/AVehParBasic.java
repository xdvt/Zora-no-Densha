package zoranodensha.vehicleParts.common.parts.basic;

import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public abstract class AVehParBasic extends AVehParBaseImpl
{
	/** Property holding the primary color of this vehicle part. */
	public final PropColor color;



	public AVehParBasic(String name)
	{
		this(name, 0.5F, 0.5F, 1.0F);
	}

	public AVehParBasic(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2, heightBy2, mass);
		addProperty(color = (PropColor)new PropColor(this, "color").setConfigurable());
	}
}