package zoranodensha.vehicleParts.common.parts.basic;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropString;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.basic.VehParRenderer_BasicText;



public class VehParBasicText extends AVehParBasic
{
	/** Property holding the text rendered by this part. */
	public PropString text;



	public VehParBasicText()
	{
		this("VehParBasicText", 0.1F, 0.1F, 0.0F);
	}

	public VehParBasicText(String name, float widthBy2, float heightBy2, float weight)
	{
		super(name, widthBy2, heightBy2, weight);
		addProperty(text = (PropString)new PropString(this, ModData.NAME, "text").setConfigurable());
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BasicText(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " D ", " P ", "   ", 'D', "dye", 'P', "plateSteel"));
	}
}