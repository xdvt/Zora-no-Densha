package zoranodensha.vehicleParts.common.parts.transport;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.transport.VehParRenderer_TransportWood;



public class VehParTransportWood extends AVehParTransportItem
{
	private static final String[] whitelist = new String[] { "plankWood", "logWood" };



	public VehParTransportWood()
	{
		super("VehParTransportWood", 0.8F, 0.545F, 2.75F);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_TransportWood(this) : renderer);
	}

	@Override
	public boolean isItemValid(ItemStack itemStack)
	{
		if (itemStack != null && itemStack.getItem() instanceof ItemBlock && (Block.getBlockFromItem(itemStack.getItem())).isNormalCube())
		{
			int[] oreIDs = OreDictionary.getOreIDs(itemStack);
			if (oreIDs != null)
			{
				String oreDictName;
				for (int id : oreIDs)
				{
					oreDictName = OreDictionary.getOreName(id);
					for (String entry : whitelist)
					{
						if (entry.equalsIgnoreCase(oreDictName))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "   ", "S S", "S S", 'S', "ingotSteel"));
	}
}