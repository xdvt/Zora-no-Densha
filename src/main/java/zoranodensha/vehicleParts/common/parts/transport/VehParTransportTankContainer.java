package zoranodensha.vehicleParts.common.parts.transport;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropString;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeLoadable;
import zoranodensha.api.vehicles.part.type.PartTypeTransportTank;
import zoranodensha.vehicleParts.client.render.transport.VehParRenderer_TransportTankContainer;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public class VehParTransportTankContainer extends AVehParBaseImpl
{
	/** Loadable part type, providing basic access to load state data. */
	public final PartTypeLoadable type_loadable;
	/** Tank part type, handling all tank-related functionality. */
	public final PartTypeTransportTank type_transportTank;

	/** Property holding this part's color. */
	public PropColor color;
	/** Property holding background color of the logo texture. */
	public PropColor colorBackground;
	/** Property holding the tank's color. */
	public PropColor colorTank;
	/** Property holding the logo texture URL. */
	public PropString textureURL;

	/** Logo texture of this container. May be {@code null}. */
	private ResourceLocation logoTex;



	public VehParTransportTankContainer()
	{
		super("VehParTransportTank", 0.675F, 0.675F, 4.0F);

		type_loadable = new PartTypeLoadable(this);
		type_transportTank = new PartTypeTransportTank(this);

		addProperty(color = (PropColor)new PropColor(this, new ColorRGBA(0.05F, 0.05F, 0.5F), "color").setConfigurable());
		addProperty(colorTank = (PropColor)new PropColor(this, new ColorRGBA(0.7F, 0.7F, 0.7F), "color_tank").setConfigurable());
		addProperty(colorBackground = (PropColor)new PropColor(this, new ColorRGBA(0.7F, 0.7F, 0.7F), "color_background")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return !textureURL.get().isEmpty();
			};
		}.setConfigurable());

		addProperty(textureURL = (PropString)new PropString(this, "textureURL")
		{
			@Override
			public boolean set(Object property)
			{
				if (property instanceof String && super.set(VehParTransportContainer.getValidatedURL(property.toString())))
				{
					logoTex = null;
					return true;
				}
				return false;
			}
		}.setConfigurable());
	}

	
	@SideOnly(Side.CLIENT)
	public ResourceLocation getLogoTexture()
	{
		if (logoTex == null)
		{
			logoTex = VehParTransportContainer.downloadResourceLoc(textureURL.get(), getName(), colorBackground.get().toHEX());
		}
		return logoTex;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_TransportTankContainer(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "SPS", "PDP", "SPS", 'S', "ingotSteel", 'P', "plateSteel", 'D', "dye"));
	}
}