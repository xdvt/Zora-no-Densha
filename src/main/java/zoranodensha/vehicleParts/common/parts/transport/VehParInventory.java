package zoranodensha.vehicleParts.common.parts.transport;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.transport.VehParRenderer_Inventory;



public class VehParInventory extends AVehParTransportItem
{
	public VehParInventory()
	{
		super("VehParInventory", 0.5F, 0.5F, 0.0F);
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_Inventory(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " S ", " C ", "   ", 'S', new ItemStack(ModCenter.ItemPart, 1, 7), 'C', Blocks.chest));
	}
}