package zoranodensha.vehicleParts.common.parts.transport;

import java.util.ArrayList;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.transport.VehParRenderer_TransportHopper;



public class VehParTransportHopper extends AVehParTransportItem
{
	private static final ArrayList<Item> whitelistItem = new ArrayList<Item>();
	private static String[] whitelistOreDict = new String[] {
			/* Items */
			"dust", "slimeball", "crop", "ingot", "ingotbrick", "seed", "gem", "coalcoke", "fuelcoke",
			/* Blocks */
			"ore", "sand", "block", "stone" };

	static
	{
		/* Items */
		whitelistItem.add(Items.apple);
		whitelistItem.add(Items.clay_ball);
		whitelistItem.add(Items.coal);
		whitelistItem.add(Items.flint);
		whitelistItem.add(Items.gunpowder);
		whitelistItem.add(Items.wheat_seeds);
		whitelistItem.add(Items.melon);
		whitelistItem.add(Items.melon_seeds);
		whitelistItem.add(Items.pumpkin_seeds);

		/* Blocks */
		whitelistItem.add(Item.getItemFromBlock(Blocks.dirt));
		whitelistItem.add(Item.getItemFromBlock(Blocks.grass));
		whitelistItem.add(Item.getItemFromBlock(Blocks.gravel));
		whitelistItem.add(Item.getItemFromBlock(Blocks.mycelium));
		whitelistItem.add(Item.getItemFromBlock(ModCenter.BlockBallastGravel));
		whitelistItem.add(Item.getItemFromBlock(ModCenter.BlockBlastFurnace));
		whitelistItem.add(Item.getItemFromBlock(ModCenter.BlockCoalCoke));
		whitelistItem.add(Item.getItemFromBlock(ModCenter.BlockCokeOven));
	}



	public VehParTransportHopper()
	{
		super("VehParTransportHopper", 0.75F, 0.675F, 5.0F);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_TransportHopper(this) : renderer);
	}

	@Override
	public boolean isItemValid(ItemStack itemStack)
	{
		if (itemStack != null)
		{
			/* Check item white list. */
			Item item = itemStack.getItem();
			for (Item entry : whitelistItem)
			{
				if (entry == item)
				{
					return true;
				}
			}

			/* Nothing found? Check ore dictionary white list. */
			int[] oreIDs = OreDictionary.getOreIDs(itemStack);
			if (oreIDs != null)
			{
				String oreDictName;
				for (int id : oreIDs)
				{
					oreDictName = OreDictionary.getOreName(id);
					if (oreDictName == null)
					{
						continue;
					}

					for (String entry : whitelistOreDict)
					{
						if (oreDictName.toLowerCase().startsWith(entry))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "P P", "P P", " H ", 'P', "plateSteel", 'H', new ItemStack(Item.getItemFromBlock(Blocks.hopper))));
	}
}