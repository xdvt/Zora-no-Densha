package zoranodensha.vehicleParts.common.parts.transport;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;

import javax.imageio.ImageIO;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.property.PropString;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.transport.VehParRenderer_TransportContainer;



public class VehParTransportContainer extends AVehParTransportItem
{
	/** Property holding background color of the logo texture. Will mask with this colour if the texture has transparent pixels. */
	public PropColor colorBackground;
	/** Property holding how many pieces are between the container's ends. */
	public PropIntegerBounded pieces;
	/** Property holding the logo texture URL. */
	public PropString textureURL;

	/** Logo texture of this container. May be {@code null}. */
	private ResourceLocation logoTex;



	public VehParTransportContainer()
	{
		super("VehParTransportContainer", 0.675F, 0.675F, 2.3F);

		color.set(0x662914FF); // 0.4F - 0.16F - 0.08F

		addProperty(pieces = (PropIntegerBounded)new PropIntegerBounded(this, 8, 0, 20, "pieces").setConfigurable());
		addProperty(colorBackground = (PropColor)new PropColor(this, new ColorRGBA(0.47F, 0.47F, 0.5F), "color_background")
		{
			@Override
			public boolean getIsConfigurable()
			{
				return !textureURL.get().isEmpty();
			};
		}.setConfigurable());

		addProperty(textureURL = (PropString)new PropString(this, "textureURL")
		{
			@Override
			public boolean set(Object property)
			{
				if (property instanceof String && super.set(getValidatedURL(property.toString())))
				{
					logoTex = null;
					return true;
				}
				return false;
			}
		}.setConfigurable());
	}

	
	@SideOnly(Side.CLIENT)
	public ResourceLocation getLogoTexture()
	{
		if (logoTex == null && !textureURL.get().isEmpty())
		{
			logoTex = VehParTransportContainer.downloadResourceLoc(textureURL.get(), getName(), colorBackground.get().toHEX());
		}
		return logoTex;
	}

	/**
	 * {@code null}-safe getter for {@link #pieces}, used during initialisation.
	 */
	public int getPieces()
	{
		return (pieces != null) ? pieces.get() : 0;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_TransportContainer(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", "PDP", "PPP", 'P', "plateSteel", 'D', "dye"));
	}

	/**
	 * Helper method to download a texture.
	 */
	@SideOnly(Side.CLIENT)
	public static ResourceLocation downloadResourceLoc(String textureURL, String partName, int backgroundHEX)
	{
		/*
		 * If no texture URL is given, return null.
		 */
		if (textureURL.isEmpty())
		{
			return null;
		}

		/*
		 * Otherwise, load or get the logo.
		 */

		/* Get image cache directory. Create if needed. */
		File cacheDir = new File(ModCenter.DIR_ADDONS, "cache/");
		if (!cacheDir.exists())
		{
			if (!cacheDir.mkdirs())
			{
				return null;
			}
		}

		BufferedImage bufImg = null;
		File imgFile = new File(cacheDir, String.format("%s.png", Integer.toHexString(textureURL.hashCode())));

		try
		{
			/* Try reading from image file if existent. */
			if (imgFile.exists())
			{
				bufImg = ImageIO.read(imgFile);
			}

			/* Otherwise try downloading from the web. */
			if (bufImg == null)
			{
				bufImg = ImageIO.read(new URL(textureURL));
			}
		}
		catch (IOException e)
		{
			ModCenter.log.info(String.format("Failed to read container image from '%s': %s", textureURL, e.getMessage()));
		}

		/* If image exists, try loading the resource location. */
		if (bufImg != null)
		{
			/* Try writing the image to file. */
			try
			{
				ImageIO.write(bufImg, "png", imgFile);
			}
			catch (IOException e)
			{
				ModCenter.log.debug(String.format("Failed to write container image from '%s': %s", textureURL, e.getMessage()));
			}
			catch (IndexOutOfBoundsException e)
			{
				;
			}

			/* Mask all transparent pixels with background colour. */
			BufferedImage copy = new BufferedImage(bufImg.getWidth(), bufImg.getHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = copy.createGraphics();
			g2d.setColor(new Color(backgroundHEX));
			g2d.fillRect(0, 0, copy.getWidth(), copy.getHeight());
			g2d.drawImage(bufImg, 0, 0, null);
			g2d.dispose();

			/* Convert image to resource location. */
			return Minecraft.getMinecraft().getTextureManager().getDynamicTextureLocation(partName, new DynamicTexture(copy));
		}
		return null;
	}

	/**
	 * Returns a validated version of the given String URL,
	 * or an empty String if it was neither a URL, URI, nor a File.
	 * 
	 * @return A validated URL, URI, or File path, or an empty String if invalid.
	 */
	public static String getValidatedURL(String s)
	{
		/* This is by far not the best way to validate an URL, but we can't be bothered to consider another dependency. */
		try
		{
			@SuppressWarnings("unused") URL url = new URL(s);
			return s;
		}
		catch (Exception e)
		{
			/* If we can't parse a URL, then try a URI */
			try
			{
				URI uri = new URI(s);
				return uri.toURL().toString();
			}
			catch (Exception e1)
			{
				/* If neither URI nor URL worked, try File. */
				File f = new File(s);
				if (f.exists())
				{
					try
					{
						s = f.toURI().toURL().toString();
						return s;
					}
					catch (Exception e2)
					{
						;
					}
				}
			}
		}
		return "";
	}
}