package zoranodensha.vehicleParts.common.parts.transport;

import net.minecraft.item.ItemStack;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypeTransportItem;



/**
 * Custom item transport part type that offers a hook for the implementing vehicle part to check whether a given item is valid.
 */
public class PartTypeTransportItemImpl extends PartTypeTransportItem
{
	public PartTypeTransportItemImpl(VehParBase parent)
	{
		super(parent);

		/* Ensure the parent is a valid type. */
		if (!(parent instanceof AVehParTransportItem))
		{
			throw new IllegalArgumentException("Supplied parent was no " + AVehParTransportItem.class.getSimpleName() + "!");
		}
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack)
	{
		return super.isItemValidForSlot(slot, itemStack) && ((AVehParTransportItem)getParent()).isItemValid(itemStack);
	}
}