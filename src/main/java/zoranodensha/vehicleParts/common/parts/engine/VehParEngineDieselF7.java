package zoranodensha.vehicleParts.common.parts.engine;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.engine.VehParRenderer_EngineDieselF7;



public class VehParEngineDieselF7 extends AVehParEngineDiesel
{
	/** Number of center section pieces this engine consists of. */
	public PropIntegerBounded pieces;



	public VehParEngineDieselF7()
	{
		super("VehParEngineDieselF7", 0.75F, 0.77F, 3.50F, 1100, 0.71F, 0.9F, 140, 940);
		addProperty(pieces = (PropIntegerBounded)new PropIntegerBounded(this, 1, 2, "pieces").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null) ? renderer = new VehParRenderer_EngineDieselF7(this) : renderer;
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(	itemStack, " S ", "CBC", "SSS", 'S', new ItemStack(ModCenter.ItemPart, 1, 7), 'C', "circuitBasic", 'B',
													new ItemStack(ModCenter.ItemBucketOilLubricant)));
	}
}