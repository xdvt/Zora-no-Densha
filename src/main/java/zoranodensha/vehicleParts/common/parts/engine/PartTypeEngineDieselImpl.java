package zoranodensha.vehicleParts.common.parts.engine;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.type.PartTypeEngineDiesel;
import zoranodensha.common.core.ModData;



/**
 * Custom diesel part type hooking into the parent's sound update method.
 */
public class PartTypeEngineDieselImpl extends PartTypeEngineDiesel
{
	public PartTypeEngineDieselImpl(VehParBase parent)
	{
		super(parent);

		/* Ensure the parent is a valid type. */
		if (!(parent instanceof AVehParEngineDiesel))
		{
			throw new IllegalArgumentException("Supplied parent was no " + AVehParEngineDiesel.class.getSimpleName() + "!");
		}

		/*
		 * Sounds
		 */
		addProperty(soundIgnitionSet = new PropSound(parent, ModData.ID + ":vehPar_engineDiesel_start_ext", "soundIgnitionStart"));
		addProperty(soundIgnitionTrip = new PropSound(parent, ModData.ID + ":vehPar_engineDiesel_stop_ext", "soundIgnitionStop"));
	}

	@Override
	public void onTick()
	{
		super.onTick();
		((AVehParEngineDiesel)getParent()).engineSounds.onUpdate();
	}
}
