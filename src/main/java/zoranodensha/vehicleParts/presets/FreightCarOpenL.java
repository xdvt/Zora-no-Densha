package zoranodensha.vehicleParts.presets;

import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportOpen;



public class FreightCarOpenL extends FreightCar
{
	public FreightCarOpenL(boolean isFinished)
	{
		super(isFinished, "Open, Large", 39);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = super.getParts();
		float offY = -0.0955F;

		VehParTransportOpen open = new VehParTransportOpen();
		open.setOffset(0.0F, offY + 1.5F, 0.0F);
		open.setScale(1.0F, 2.5F, 1.0F);
		list.add(open);

		return list;
	}
}
