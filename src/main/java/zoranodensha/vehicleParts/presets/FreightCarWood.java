package zoranodensha.vehicleParts.presets;

import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportWood;



public class FreightCarWood extends FreightCar
{
	public FreightCarWood(boolean isFinished)
	{
		super(isFinished, "Wood", 25);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = super.getParts();
		float offY = -0.0955F;

		VehParTransportWood wood = new VehParTransportWood();
		wood.setOffset(0.0F, offY + 1.35F, 0.0F);
		list.add(wood);

		return list;
	}
}
