package zoranodensha.vehicleParts.presets;

import static zoranodensha.api.vehicles.part.type.APartTypeCoupling.COUPLING_DEFAULT_OFFSET_Y;

import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisFreight;
import zoranodensha.vehicleParts.common.parts.buffer.VehParBufferRound;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateModFreightEnd;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateModFreightFlat;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateModFreightMid;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateModFreightTrans;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerScrew;



public class FreightCar extends APreset
{
	public FreightCar(boolean isFinished)
	{
		this(isFinished, "Flat", 22);
	}

	protected FreightCar(boolean isFinished, String name, int weight)
	{
		super(isFinished, "Optimus13", "Freight Car (" + name + ")", 120, weight);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = new OrderedPartsList();
		float offY = -0.0955F;
		float f = 0.0F;

		for (int i = 1; i > -2; i -= 2)
		{
			VehParBaseplateModFreightEnd baseEnd = new VehParBaseplateModFreightEnd();
			baseEnd.setOffset(4.75F * i, offY + 0.68F, 0.0F);
			baseEnd.setRotation(0.0F, f, 0.0F);
			list.add(baseEnd);

			VehParBaseplateModFreightFlat baseFlat = new VehParBaseplateModFreightFlat();
			baseFlat.setOffset(3.5F * i, offY + 0.8525F, 0.0F);
			baseFlat.setRotation(0.0F, f, 0.0F);
			list.add(baseFlat);

			VehParBaseplateModFreightTrans baseTrans = new VehParBaseplateModFreightTrans();
			baseTrans.setOffset(2.25F * i, offY + 0.68F, 0.0F);
			baseTrans.setRotation(0.0F, f, 0.0F);
			list.add(baseTrans);

			VehParBaseplateModFreightMid baseMid = new VehParBaseplateModFreightMid();
			baseMid.setOffset(0.75F * i, offY + 0.68F, 0.0F);
			list.add(baseMid);

			VehParBufferRound buffer = new VehParBufferRound();
			buffer.setOffset(5.63F * i, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
			buffer.setRotation(0.0F, f, 0.0F);
			buffer.color.set(buffer.color.get().multiply(0.6F));
			list.add(buffer);

			VehParCouplerScrew coupler = new VehParCouplerScrew();
			coupler.setOffset(5.588F * i, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
			coupler.setRotation(0.0F, f, 0.0F);
			coupler.type_coupling.expansion.set(0.192F);
			list.add(coupler);

			VehParAxisFreight axis = new VehParAxisFreight();
			axis.setOffset(4.0F * i, 0.4195F, 0.0F);
			list.add(axis);

			f += 180.0F;
		}
		return list;
	}
}
