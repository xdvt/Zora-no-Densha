package zoranodensha.vehicleParts.presets;

import static zoranodensha.api.vehicles.part.type.APartTypeCoupling.COUPLING_DEFAULT_OFFSET_Y;

import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.EFlareBehaviour;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicCube;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisDBpza;
import zoranodensha.vehicleParts.common.parts.buffer.VehParBufferDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisBottomDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisCabDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisDoorsDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisTopDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisVestibuleDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisVestibuleRoofDBpza;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerScrew;
import zoranodensha.vehicleParts.common.parts.other.VehParBufferBaseDBpza;
import zoranodensha.vehicleParts.common.parts.other.VehParDestBoard;
import zoranodensha.vehicleParts.common.parts.other.VehParIntercirculationDBpza;
import zoranodensha.vehicleParts.common.parts.other.VehParLamp;
import zoranodensha.vehicleParts.common.parts.other.VehParStairsDBpza;
import zoranodensha.vehicleParts.common.parts.seat.VehParCab;
import zoranodensha.vehicleParts.common.parts.seat.VehParSeat;



public class DBpzaCabCar extends APreset
{
	public DBpzaCabCar(boolean isFinished)
	{
		super(isFinished, "Xenoniuss", "DBpza Cab Car", 160, 51);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = new OrderedPartsList();
		float offY = -0.174F;

		/*
		 * Symmetric parts
		 */
		for (int i = -1; i <= 1; i += 2)
		{
			/* VehParAxisDBpza */
			VehParAxisDBpza axis = new VehParAxisDBpza();
			axis.setOffset(5.55F * i, 0.325F, 0.0F);
			list.add(axis);

			/* VehParChassisBottomDBpza */
			for (int j = (i < 0 ? 1 : 0); j < 4; ++j)
			{
				VehParChassisBottomDBpza bot = new VehParChassisBottomDBpza(1);
				bot.setOffset((0.9F * j) * i, offY + 1.085F, 0.0F);

				if (j == 3)
				{
					bot.setRotation(0.0F, (i > 0) ? 180.0F : 0.0F, 0.0F);
					bot.hasUVID.set(true);
				}

				list.add(bot);
			}

			/* VehParChassisDoorsDBpza */
			VehParChassisDoorsDBpza doors = new VehParChassisDoorsDBpza(i >= 0, 0);
			doors.setOffset(3.825F * i, offY + 1.085F, 0.0F);
			doors.setRotation(0.0F, i >= 0 ? 180.0F : 0.0F, 0.0F);
			list.add(doors);

			/* VehParChassisTopDBpza */
			for (int j = 0; j < 5; ++j)
			{
				VehParChassisTopDBpza top = new VehParChassisTopDBpza(1);
				top.setOffset((0.9F * j + 0.45F) * i, offY + 2.365F, 0.0F);
				top.prop_colorStripe.set(i >= 0 ? ELiveryColor.GOLD.color() : ELiveryColor.NAVY.color());
				list.add(top);
			}

			/* VehParChassisVestibuleRoofDBpza */
			VehParChassisVestibuleRoofDBpza roof = new VehParChassisVestibuleRoofDBpza(1);
			roof.setOffset(5.1F * i, offY + 2.53F, 0.0F);
			roof.setScale(1.0F, 1.0F, 1.0F);
			roof.setRotation(0.0F, i >= 0 ? 180.0F : 0.0F, 0.0F);
			list.add(roof);

			/* VehParDestBoard */
			VehParDestBoard boardInside = new VehParDestBoard();
			boardInside.setOffset(4.495F * i, offY + 1.65F, 0.3F * i);
			boardInside.setScale(0.7F, 1.0F, 1.0F);
			boardInside.setRotation(0.0F, 90.0F * -i, 0.0F);
			boardInside.color.set(ELiveryColor.GREY_LIGHT.color());
			list.add(boardInside);

			VehParDestBoard boardOutside = new VehParDestBoard();
			boardOutside.setOffset(0.0F, offY + 1.8F, i * 0.755F);
			boardOutside.setRotation(0.0F, i >= 0 ? 0.0F : 180.0F, 0.0F);
			boardOutside.color.set(ELiveryColor.WHITE.color());
			list.add(boardOutside);

			/* VehParSeat - Upper floor */
			for (float[] val : new float[][] { { 0.2F, 180.0F }, { 1.0F, 0.0F }, { 1.375F, 180.0F }, { 2.075F, 0.0F }, { 2.675F, 0.0F }, { 3.275F, 0.0F }, { 3.875F, 0.0F } })
			{
				for (int side = -1; side <= 1; side += 2)
				{
					if (val[0] == 3.875F && side == i)
					{
						continue;
					}

					VehParSeat seat = new VehParSeat();

					if (i < 0)
					{
						seat.setRotation(0.0F, val[1] + 180.0F, 0.0F);
						seat.setOffset(val[0] * i, offY + 2.225F, 0.425F * side);
						seat.setScale(0.6F, 0.6F, 0.6F);
						seat.prop_color.set(ELiveryColor.NAVY.color());
					}
					else
					{
						seat.setRotation(0.0F, val[1], 0.0F);
						seat.setOffset(val[0] * i, offY + 2.225F, 0.4F * side);
						seat.setScale(0.6F, 0.6F, 0.75F);
						seat.prop_color.set(ELiveryColor.NAVY.color().multiply(0.5F));
						seat.prop_colorHandles.set(ELiveryColor.GOLD.color());
					}

					list.add(seat);
				}
			}

			/* VehParSeat - Lower floor */
			if (i > 0)
			{
				for (int j = 0; j < 3; ++j)
				{
					for (int side = -1; side <= 1; side += 2)
					{
						if (j == 0 && side > 0)
						{
							continue;
						}

						VehParSeat seat = new VehParSeat();
						seat.setOffset((1.225F * -j) + 0.475F, offY + 1.015F, -0.5F * side);
						seat.setRotation(0.0F, 90.0F * side, 0.0F);
						seat.setScale(0.6F, 0.6F, 0.6F);
						seat.prop_amount.set(3);
						seat.prop_color.set(ELiveryColor.NAVY.color());
						list.add(seat);
					}
				}
			}

			/* VehParStairsDBpza */
			VehParStairsDBpza stairs = new VehParStairsDBpza();
			stairs.setOffset(4.9F * i, offY + 1.53F, 0.3F * i);
			stairs.setRotation(0.0F, i >= 0 ? 180.0F : 0.0F, 0.0F);
			list.add(stairs);
		}

		/*
		 * Asymmetric parts
		 */
		/* VehParBasicCube - Toilets, rear wall and door */
		VehParBasicCube door = new VehParBasicCube();
		door.color.set(ELiveryColor.GREY_LIGHT.color());
		door.setOffset(-7.35F, offY + 1.55F, 0.0F);
		door.setScale(0.05F, 1.2F, 0.63F);
		list.add(door);

		VehParBasicCube rearWall = new VehParBasicCube();
		rearWall.setOffset(-7.325F, offY + 1.495F, 0.0F);
		rearWall.setScale(0.05F, 1.38F, 1.4F);
		rearWall.color.set(ELiveryColor.WHITE.color());
		list.add(rearWall);

		VehParBasicCube toilet = new VehParBasicCube();
		toilet.setOffset(2.1F, offY + 1.2F, 0.2F);
		toilet.setScale(2.0F, 1.2F, 1.0F);
		toilet.color.set(ELiveryColor.GREY_LIGHT.color());
		list.add(toilet);

		for (int j = -1; j <= 1; j += 2)
		{
			VehParBasicCube machineRoom = new VehParBasicCube();
			machineRoom.setOffset(-6.702F, offY + 1.524F, 0.45F * j);
			machineRoom.setScale(1.2F, 1.5F, 0.5F);
			machineRoom.color.set(ELiveryColor.GREY_LIGHT.color());
			list.add(machineRoom);
		}

		/* VehParBuffer */
		VehParBufferDBpza bufferFront = new VehParBufferDBpza();
		bufferFront.setOffset(7.6F, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
		list.add(bufferFront);

		VehParBufferDBpza bufferRear = new VehParBufferDBpza();
		bufferRear.setOffset(-7.3F, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
		bufferRear.setRotation(0.0F, 180.0F, 0.0F);
		list.add(bufferRear);

		/* VehParBufferBaseDBpza */
		VehParBufferBaseDBpza bufferBase = new VehParBufferBaseDBpza();
		bufferBase.setOffset(-7.1F, offY + 0.655F, 0.0F);
		list.add(bufferBase);

		/* VehParCab */
		VehParCab cab = new VehParCab();
		cab.setOffset(6.65F, offY + 1.35F, 0.0F);
		cab.prop_color.set(ELiveryColor.NAVY.color().multiply(0.5F));
		cab.prop_colorHandles.set(ELiveryColor.GREY_LIGHT.color());
		cab.prop_colorPadding.set(ELiveryColor.WHITE.color());
		cab.color_desk.set(ELiveryColor.GREY_LIGHT.color());
		list.add(cab);

		/* VehParChassisCabDBpza */
		VehParChassisCabDBpza chassisCab = new VehParChassisCabDBpza();
		chassisCab.setOffset(6.85F, offY + 1.755F, 0.0F);
		list.add(chassisCab);

		for (int k = -1; k <= 1; k += 2)
		{
			VehParLamp flare = new VehParLamp();
			flare.type_lamp.behaviour.set(EFlareBehaviour.FRONT_HIGH);
			flare.type_lamp.flareTilt.set(-16.0F);
			flare.setOffset(7.44F, offY + 1.455F, 0.35F * k);
			list.add(flare);

			flare = new VehParLamp();
			flare.type_lamp.behaviour.set(EFlareBehaviour.REAR);
			flare.type_lamp.flareTilt.set(-16.0F);
			flare.setOffset(7.44F, offY + 1.455F, 0.25F * k);
			flare.color.set(ColorRGBA.RED.toHEX());
			list.add(flare);
		}

		VehParLamp shuntFlare = new VehParLamp();
		shuntFlare.type_lamp.behaviour.set(EFlareBehaviour.FRONT_HIGH);
		shuntFlare.type_lamp.flareTilt.set(-45.0F);
		shuntFlare.setOffset(6.85F, offY + 2.555F, 0.0F);
		list.add(shuntFlare);

		/* VehParChassisVestibuleDBpza */
		VehParChassisVestibuleDBpza vestibule = new VehParChassisVestibuleDBpza(4);
		vestibule.setOffset(-5.3F, offY + 1.405F, 0.0F);
		list.add(vestibule);

		vestibule = new VehParChassisVestibuleDBpza(5);
		vestibule.setOffset(-6.6F, offY + 1.405F, 0.0F);
		list.add(vestibule);

		vestibule = new VehParChassisVestibuleDBpza(0);
		vestibule.setOffset(-7.225F, offY + 1.405F, 0.0F);
		list.add(vestibule);

		VehParChassisVestibuleDBpza vestibuleBehindCab = new VehParChassisVestibuleDBpza(3);
		vestibuleBehindCab.setOffset(5.1F, offY + 1.405F, 0.0F);
		list.add(vestibuleBehindCab);

		/* VehParChassisVestibuleRoofDBpza */
		VehParChassisVestibuleRoofDBpza roof = new VehParChassisVestibuleRoofDBpza(0);
		roof.setOffset(-6.525F, offY + 2.53F, 0.0F);
		roof.setScale(1.65F, 1.0F, 1.0F);
		list.add(roof);

		/* VehParCouplerScrew */
		VehParCouplerScrew couplerFront = new VehParCouplerScrew();
		couplerFront.setOffset(7.39F, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
		couplerFront.type_coupling.expansion.set(0.33F);
		list.add(couplerFront);

		VehParCouplerScrew couplerRear = new VehParCouplerScrew();
		couplerRear.setOffset(-7.3F, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
		couplerRear.setRotation(0.0F, 180.0F, 0.0F);
		list.add(couplerRear);

		/* VehParIntercirculationDBpza */
		VehParIntercirculationDBpza intercirculation = new VehParIntercirculationDBpza();
		intercirculation.setOffset(-7.375F, offY + 1.55F, 0.0F);
		intercirculation.setScale(0.5F, 1.0F, 1.2F);
		list.add(intercirculation);

		return list;
	}
}
