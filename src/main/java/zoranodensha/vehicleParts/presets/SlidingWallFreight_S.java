package zoranodensha.vehicleParts.presets;

import static zoranodensha.api.vehicles.part.type.APartTypeCoupling.COUPLING_DEFAULT_OFFSET_Y;

import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisY25;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateEndSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateFrameSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisEndSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisRoofSWF;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisWallSWF;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerScrew;
import zoranodensha.vehicleParts.common.parts.other.VehParDetailSWF;
import zoranodensha.vehicleParts.common.parts.transport.VehParInventory;



public class SlidingWallFreight_S extends APreset
{
	public SlidingWallFreight_S(boolean isFinished)
	{
		super(isFinished, "Xenoniuss", "Sliding Wall Freight (Short)", 120, 21);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = new OrderedPartsList();
		float offY = -0.0675F;

		for (int i = 1; i >= -1; i -= 2)
		{
			for (int j = 0; j < 4; ++j)
			{
				/* Don't add center parts twice. */
				if (i < 0 && j == 0)
				{
					continue;
				}

				/* VehParBaseplateSWF */
				VehParBaseplateSWF baseplate = new VehParBaseplateSWF();
				baseplate.setOffset(i * j, offY + 0.7325F, 0.0F);
				baseplate.setScale(1.0F, 1.25F, 1.0F);
				list.add(baseplate);

				/* VehParChassisRoofSWF */
				VehParChassisRoofSWF roof = new VehParChassisRoofSWF();
				roof.setOffset(i * j, offY + 2.45F, 0.0F);
				list.add(roof);
			}

			/* VehParChassisWallSWF - Double */
			for (float f : new float[] { 0.5F, 3.0F })
			{
				boolean isReversed = (f == 0.5F) != (i < 0);

				VehParChassisWallSWF wall = new VehParChassisWallSWF();
				wall.isReversed.set(!isReversed);
				wall.setOffset(i * f, offY + 1.6F, 0.47F);
				wall.hasUVID.set((f == 3.0F) && (i < 0));

				if (isReversed)
				{
					wall.colorPanelBot.set(ELiveryColor.NAVY.color());
				}
				list.add(wall);

				VehParChassisWallSWF wall2 = new VehParChassisWallSWF();
				wall2.isReversed.set(isReversed);
				wall2.setRotation(0.0F, 180.0F, 0.0F);
				wall2.setOffset(i * f, offY + 1.6F, -0.47F);
				wall2.hasUVID.set((f == 3.0F) && (i > 0));

				if (!isReversed)
				{
					wall2.colorPanelBot.set(ELiveryColor.NAVY.color());
				}
				list.add(wall2);
			}

			/* VehParChassisWallSWF - Single */
			for (float f : new float[] { 1.25F, 1.75F, 2.25F })
			{
				VehParChassisWallSWF wall = new VehParChassisWallSWF();
				wall.isDouble.set(false);
				wall.setOffset(i * f, offY + 1.6F, 0.47F);
				wall.hasUVID.set(false);

				if ((i > 0) ? (f == 1.25F) : (f == 2.25F))
				{
					wall.colorPanelBot.set(ELiveryColor.GREY_DARK.color());
				}
				list.add(wall);

				VehParChassisWallSWF wall2 = new VehParChassisWallSWF();
				wall2.isDouble.set(false);
				wall2.setOffset(i * f, offY + 1.6F, -0.47F);
				wall2.setRotation(0.0F, 180.0F, 0.0F);
				wall2.hasUVID.set(false);

				if ((i < 0) ? (f == 1.25F) : (f == 2.25F))
				{
					wall2.colorPanelBot.set(ELiveryColor.GREY_DARK.color());
				}
				list.add(wall2);
			}

			/* VehParAxisSWF */
			VehParAxisY25 axis = new VehParAxisY25();
			axis.setOffset(i * 2.4F, 0.27F, 0.0F);
			axis.setRotation(0.0F, (i < 0) ? 180.0F : 0.0F, 0.0F);
			list.add(axis);

			/* VehParBaseplateEndSWF */
			VehParBaseplateEndSWF baseplateEnd = new VehParBaseplateEndSWF();
			baseplateEnd.setOffset(i * 3.42F, offY + 0.76F, 0.0F);
			baseplateEnd.setScale(1.0F, 1.25F, 1.0F);
			baseplateEnd.setRotation(0.0F, (i > 0) ? 180.0F : 0.0F, 0.0F);
			list.add(baseplateEnd);

			/* VehParChassisEndSWF */
			VehParChassisEndSWF chassisEnd = new VehParChassisEndSWF();
			chassisEnd.setOffset(i * 3.55F, offY + 1.60F, 0.0F);
			chassisEnd.setRotation(0.0F, (i > 0) ? 180.0F : 0.0F, 0.0F);
			list.add(chassisEnd);

			/* VehParCouplerScrew */
			VehParCouplerScrew coupler = new VehParCouplerScrew();
			coupler.setOffset(i * 3.55F, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
			coupler.setRotation(0.0F, (i < 0) ? 180.0F : 0.0F, 0.0F);
			list.add(coupler);

			/* VehParDetailSWF */
			VehParDetailSWF detail = new VehParDetailSWF(EDetailType.VALVE);
			detail.setOffset(0.5F * i, offY + 0.5F, 0.65F * i);
			detail.setRotation(0.0F, (i < 0) ? 180.0F : 0.0F, 0.0F);
			list.add(detail);

			for (int j = 1; j >= -1; j -= 2)
			{
				VehParDetailSWF detail2 = new VehParDetailSWF(EDetailType.HOOK);
				detail2.setOffset(3.05F * i, offY + 0.75F, 0.73F * j);
				detail2.setRotation(0.0F, (j < 0) ? 180.0F : 0.0F, 0.0F);
				list.add(detail2);
			}
		}

		/* VehParChassisFrameSWF */
		VehParBaseplateFrameSWF frame = new VehParBaseplateFrameSWF();
		frame.setOffset(0.0F, offY + 0.55F, 0.0F);
		list.add(frame);

		/* VehParTransportCustom */
		VehParInventory inventory = new VehParInventory();
		inventory.setOffset(0.0F, offY + 1.4F, 0.0F);
		list.add(inventory);

		return list;
	}



	/**
	 * Type of wall for this freight wagon.
	 */
	public enum EWallType
	{
		SOFT,
		SOLID
	}

	/**
	 * Various details of this freight wagon.
	 */
	public enum EDetailType
	{
		HOOK,
		MOUNT,
		VALVE
	}
}
