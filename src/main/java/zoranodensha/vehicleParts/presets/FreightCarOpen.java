package zoranodensha.vehicleParts.presets;

import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportOpen;



public class FreightCarOpen extends FreightCar
{
	public FreightCarOpen(boolean isFinished)
	{
		super(isFinished, "Open", 29);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = super.getParts();
		float offY = -0.0955F;

		VehParTransportOpen open = new VehParTransportOpen();
		open.setOffset(0.0F, offY + 1.148F, 0.0F);
		list.add(open);

		return list;
	}
}
