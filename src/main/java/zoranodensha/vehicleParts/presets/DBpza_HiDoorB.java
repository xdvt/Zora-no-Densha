package zoranodensha.vehicleParts.presets;

import static zoranodensha.api.vehicles.part.type.APartTypeCoupling.COUPLING_DEFAULT_OFFSET_Y;

import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicCube;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisDBpza;
import zoranodensha.vehicleParts.common.parts.buffer.VehParBufferDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisBottomDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisDoorsDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisStairsDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisTopDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisVestibuleDBpza;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisVestibuleRoofDBpza;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerScrew;
import zoranodensha.vehicleParts.common.parts.other.VehParBufferBaseDBpza;
import zoranodensha.vehicleParts.common.parts.other.VehParDestBoard;
import zoranodensha.vehicleParts.common.parts.other.VehParIntercirculationDBpza;
import zoranodensha.vehicleParts.common.parts.seat.VehParSeat;



public class DBpza_HiDoorB extends APreset
{
	public DBpza_HiDoorB(boolean isFinished)
	{
		super(isFinished, "Xenoniuss", "DBpza (High-Door B)", 160, 50);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = new OrderedPartsList();
		float offY = -0.174F;

		for (int i = -1; i <= 1; i += 2)
		{
			/* VehParAxisDBpza */
			VehParAxisDBpza axis = new VehParAxisDBpza();
			axis.setOffset(5.55F * i, 0.325F, 0.0F);
			list.add(axis);

			/* VehParBasicCube - Toilets, rear wall and door */
			VehParBasicCube door = new VehParBasicCube();
			door.color.set(ELiveryColor.GREY_LIGHT.color());
			door.setOffset(7.35F * i, offY + 1.55F, 0.0F);
			door.setScale(0.05F, 1.2F, 0.63F);
			list.add(door);

			VehParBasicCube rearWall = new VehParBasicCube();
			rearWall.setOffset(7.325F * i, offY + 1.495F, 0.0F);
			rearWall.setScale(0.05F, 1.38F, 1.4F);
			rearWall.color.set(ELiveryColor.WHITE.color());
			list.add(rearWall);

			if (i > 0)
			{
				for (int j = -1; j <= 1; j += 2)
				{
					VehParBasicCube toilet = new VehParBasicCube();
					toilet.setOffset(6.702F * i, offY + 1.524F, 0.45F * j);
					toilet.setScale(1.2F, 1.5F, 0.5F);
					toilet.color.set(ELiveryColor.GREY_LIGHT.color());
					list.add(toilet);
				}
			}

			/* VehParBufferBaseDBpza */
			VehParBufferBaseDBpza bufferBase = new VehParBufferBaseDBpza();
			bufferBase.setOffset(7.1F * i, offY + 0.655F, 0.0F);
			list.add(bufferBase);

			/* VehParBufferDBpza */
			VehParBufferDBpza buffer = new VehParBufferDBpza();
			buffer.setOffset(7.3F * i, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
			buffer.setRotation(0.0F, (i < 0) ? 180.0F : 0.0F, 0.0F);
			list.add(buffer);

			/* VehParChassisBottomDBpza */
			for (int j = (i < 0 ? 1 : 0); j < 4; ++j)
			{
				VehParChassisBottomDBpza bot = new VehParChassisBottomDBpza(1);
				bot.setOffset((0.9F * j) * i, offY + 1.085F, 0.0F);

				if (j == 3)
				{
					bot.setRotation(0.0F, (i > 0) ? 180.0F : 0.0F, 0.0F);
					bot.hasUVID.set(true);
				}

				list.add(bot);
			}

			/* VehParChassisDoorsDBpza */
			VehParChassisDoorsDBpza doors = new VehParChassisDoorsDBpza(i >= 0, 1);
			doors.setOffset(5.3F * i, offY + 1.495F, 0.0F);
			doors.setRotation(0.0F, (i >= 0) ? 180.0F : 0.0F, 0.0F);
			list.add(doors);

			/* VehParChassisStairsDBpza */
			VehParChassisStairsDBpza chassisStairs = new VehParChassisStairsDBpza(0);
			chassisStairs.setOffset(3.825F * i, offY + 1.595F, 0.0F);
			chassisStairs.setRotation(0.0F, (i >= 0) ? 180.0F : 0.0F, 0.0F);
			list.add(chassisStairs);

			/* VehParChassisTopDBpza */
			for (int j = 0; j < 4; ++j)
			{
				if (j == 0 && i > 0)
				{
					continue;
				}

				VehParChassisTopDBpza top = new VehParChassisTopDBpza(1);
				top.setOffset((0.9F * j) * i, offY + 2.365F, 0.0F);
				list.add(top);
			}

			/* VehParChassisVestibuleDBpza */
			VehParChassisVestibuleDBpza vestibule;

			vestibule = new VehParChassisVestibuleDBpza(i > 0 ? 5 : 2);
			vestibule.setOffset(6.6F * i, offY + 1.405F, 0.0F);
			list.add(vestibule);

			vestibule = new VehParChassisVestibuleDBpza(0);
			vestibule.setOffset(7.225F * i, offY + 1.405F, 0.0F);
			list.add(vestibule);

			/* VehParChassisVestibuleRoofDBpza */
			VehParChassisVestibuleRoofDBpza roof;

			roof = new VehParChassisVestibuleRoofDBpza();
			roof.setOffset(5.175F * i, offY + 2.53F, 0.0F);
			roof.setScale(1.35F, 1.0F, 1.0F);
			roof.setRotation(0.0F, (i >= 0) ? 180.0F : 0.0F, 0.0F);
			list.add(roof);

			roof = new VehParChassisVestibuleRoofDBpza();
			roof.setOffset(6.6F * i, offY + 2.53F, 0.0F);
			roof.setScale(1.5F, 1.0F, 1.0F);
			list.add(roof);

			/* VehParCouplerScrew */
			VehParCouplerScrew coupler = new VehParCouplerScrew();
			coupler.setOffset(7.3F * i, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
			coupler.setRotation(0.0F, (i < 0) ? 180.0F : 0.0F, 0.0F);
			list.add(coupler);

			/* VehParDestBoard */
			VehParDestBoard boardInside = new VehParDestBoard();
			boardInside.setOffset(4.449F * i, offY + 2.45F, 0.33F * i);
			boardInside.setScale(0.5F, 0.5F, 0.5F);
			boardInside.setRotation(0.0F, 90.0F * -i, 0.0F);
			boardInside.color.set(ELiveryColor.GREY_LIGHT.color());
			list.add(boardInside);

			VehParDestBoard boardOutside = new VehParDestBoard();
			boardOutside.setOffset(0.0F, offY + 1.8F, i * 0.755F);
			boardOutside.setRotation(0.0F, (i >= 0) ? 0.0F : 180.0F, 0.0F);
			boardOutside.color.set(ELiveryColor.WHITE.color());
			list.add(boardOutside);

			/* VehParIntercirculationDBpza */
			VehParIntercirculationDBpza intercirculation = new VehParIntercirculationDBpza();
			intercirculation.setOffset(7.375F * i, offY + 1.55F, 0.0F);
			intercirculation.setScale(0.5F, 1.0F, 1.2F);
			list.add(intercirculation);

			/* VehParSeat - Upper floor */
			for (float[] val : new float[][] { { 0.2F, 180.0F }, { 1.0F, 0.0F }, { 1.375F, 180.0F }, { 2.075F, 0.0F }, { 2.675F, 0.0F }, { 3.275F, 0.0F }, { 3.875F, 0.0F } })
			{
				for (int side = -1; side <= 1; side += 2)
				{
					if (val[0] > 3.0F && side == i)
					{
						continue;
					}

					VehParSeat seat = new VehParSeat();
					seat.setRotation(0.0F, (i < 0) ? val[1] + 180.0F : val[1], 0.0F);
					seat.setOffset(val[0] * i, offY + 2.225F, 0.425F * side);
					seat.setScale(0.6F, 0.6F, 0.6F);
					seat.prop_color.set(ELiveryColor.NAVY.color());
					list.add(seat);
				}
			}

			/* VehParSeat - Lower floor */
			for (float[] val : new float[][] { { 0.185F, 180.0F }, { 0.985F, 0.0F }, { 1.585F, 0.0F }, { 2.185F, 0.0F }, { 2.785F, 0.0F }, { 3.385F, 0.0F } })
			{
				for (int side = -1; side <= 1; side += 2)
				{
					if (val[0] > 3.0F && side != i)
					{
						continue;
					}

					VehParSeat seat = new VehParSeat();
					seat.setRotation(0.0F, (i < 0) ? val[1] + 180.0F : val[1], 0.0F);
					seat.setOffset(val[0] * i, offY + 1.015F, 0.425F * side);
					seat.setScale(0.6F, 0.6F, 0.6F);
					seat.prop_color.set(ELiveryColor.NAVY.color());
					list.add(seat);
				}
			}
		}

		return list;
	}
}
