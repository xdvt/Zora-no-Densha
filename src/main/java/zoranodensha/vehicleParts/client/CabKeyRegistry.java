package zoranodensha.vehicleParts.client;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic.ECabKey;



/**
 * Registry for cab key bindings.<br>
 * <br>
 * Any keys for which an event shall be posted and an entry
 * in the configuration GUI shall be made need to be registered here.
 */
@SideOnly(Side.CLIENT)
public final class CabKeyRegistry
{
	//@formatter:off
	
	public static final CabKeyBinding[] keys = new CabKeyBinding[] {
			new CabKeyBinding(Keyboard.KEY_R, ECabKey.AFB_INCREASE),
			new CabKeyBinding(Keyboard.KEY_F, ECabKey.AFB_DECREASE),
			new CabKeyBinding(Keyboard.KEY_Q, ECabKey.ALERTER),
			new CabKeyBinding(Keyboard.KEY_SEMICOLON, ECabKey.BRAKE_DECREASE),
			new CabKeyBinding(Keyboard.KEY_COMMA, ECabKey.BRAKE_DYNAMIC_DECREASE),
			new CabKeyBinding(Keyboard.KEY_PERIOD, ECabKey.BRAKE_DYNAMIC_INCREASE),
			new CabKeyBinding(Keyboard.KEY_APOSTROPHE, ECabKey.BRAKE_INCREASE),
			new CabKeyBinding(Keyboard.KEY_Y, ECabKey.BRAKE_MODE),
			new CabKeyBinding(Keyboard.KEY_P, ECabKey.DECOUPLE),
			new CabKeyBinding(Keyboard.KEY_I, ECabKey.DOORS_LEFT),
			new CabKeyBinding(Keyboard.KEY_O, ECabKey.DOORS_RIGHT),
			new CabKeyBinding(Keyboard.KEY_RETURN, ECabKey.EMERGENCY_BRAKE),
			new CabKeyBinding(Keyboard.KEY_SPACE, ECabKey.HORN),
			new CabKeyBinding(Keyboard.KEY_J, ECabKey.LIGHTS_BEAMS),
			new CabKeyBinding(Keyboard.KEY_K, ECabKey.LIGHTS_CAB),
			new CabKeyBinding(Keyboard.KEY_L, ECabKey.LIGHTS_TRAIN),
			new CabKeyBinding(Keyboard.KEY_X, ECabKey.MOTOR_DISABLE),
			new CabKeyBinding(Keyboard.KEY_Z, ECabKey.MOTOR_ENABLE),
			new CabKeyBinding(Keyboard.KEY_1, ECabKey.MULTI_PURPOSE_1),
			new CabKeyBinding(Keyboard.KEY_2, ECabKey.MULTI_PURPOSE_2),
			new CabKeyBinding(Keyboard.KEY_3, ECabKey.MULTI_PURPOSE_3),
			new CabKeyBinding(Keyboard.KEY_4, ECabKey.MULTI_PURPOSE_4),
			new CabKeyBinding(Keyboard.KEY_7, ECabKey.PANTOGRAPH_BACK),
			new CabKeyBinding(Keyboard.KEY_8, ECabKey.PANTOGRAPH_FRONT),
			new CabKeyBinding(Keyboard.KEY_RBRACKET, ECabKey.PARK_BRAKE_APPLY),
			new CabKeyBinding(Keyboard.KEY_LBRACKET, ECabKey.PARK_BRAKE_RELEASE),
			new CabKeyBinding(Keyboard.KEY_S, ECabKey.REVERSER_BACKWARD),
			new CabKeyBinding(Keyboard.KEY_W, ECabKey.REVERSER_FORWARD),
			new CabKeyBinding(Keyboard.KEY_D, ECabKey.THROTTLE_DECREASE),
			new CabKeyBinding(Keyboard.KEY_A, ECabKey.THROTTLE_INCREASE)
	};
	
	//@formatter:on



	/**
	 * Called to update all key bindings with matching key code.
	 */
	public static ArrayList<CabKeyBinding> eventTick(int keyCode, boolean keyState)
	{
		ArrayList<CabKeyBinding> ret = new ArrayList<CabKeyBinding>();

		for (CabKeyBinding key : keys)
		{
			if (key.keyCode == keyCode)
			{
				key.isPressed = keyState;
				if (key.isPressed)
				{
					++key.pressTime;
				}
				ret.add(key);
			}
		}
		return ret;
	}



	/**
	 * Configurable key binding for cab keys.<br>
	 * Shows up after registration in Zora no Densha's key configuration GUI for customisation.
	 */
	@SideOnly(Side.CLIENT)
	public static class CabKeyBinding
	{
		/*
		 * Initialised data.
		 */
		private final ECabKey cabKey;
		public final int keyCodeDefault;
		public int keyCode;

		/*
		 * Dynamic key-press data.
		 */
		/** {@code true} while this key is pressed. */
		private boolean isPressed;
		private int pressTime;



		/**
		 * Initialise a new key binding with given data.
		 * 
		 * @param keyCode - Default key code of this key binding.
		 * @param cabKey - {@link zoranodensha.api.vehicles.vehiclePart.IVehiclePartCab.ECabKey Cab key} mapped to this key.
		 * @param desc - A short description of this key binding.
		 */
		public CabKeyBinding(int keyCode, ECabKey cabKey)
		{
			this.keyCode = keyCodeDefault = keyCode;
			this.cabKey = cabKey;
		}

		/**
		 * Return the {@link zoranodensha.api.vehicles.vehiclePart.IVehiclePartCab.ECabKey cab key} mapped to this key.
		 */
		public ECabKey getCabKey()
		{
			return cabKey;
		}

		/**
		 * Returns {@code true} while this key is pressed down.
		 */
		public boolean isPressed()
		{
			return isPressed;
		}

		/**
		 * Returns the number of times this key has been registered as pressed down.
		 */
		public int getPressTime()
		{
			return pressTime;
		}

		@Override
		public String toString()
		{
			return cabKey.toString().toLowerCase();
		}
	}
}