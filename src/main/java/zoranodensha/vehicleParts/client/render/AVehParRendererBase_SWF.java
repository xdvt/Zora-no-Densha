package zoranodensha.vehicleParts.client.render;

import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;

public abstract class AVehParRendererBase_SWF
{
	public static final ObjModelSpeedy model = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "SlidingWallFreight");
}
