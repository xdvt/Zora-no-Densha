package zoranodensha.vehicleParts.client.render;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import net.minecraft.util.ResourceLocation;
import zoranodensha.common.core.ModData;



public class GraphicsResources
{
	public static class MSMR
	{
		public static BufferedImage contrast;
		public static BufferedImage defaultBackground;
		public static BufferedImage defaultBackgroundDay;
		public static BufferedImage signalStrength;
		public static BufferedImage splash;
	}



	public static BufferedImage imageScreenGlare;
	public static BufferedImage imageScreenGlareSmall;
	public static BufferedImage imageScreenShade;
	public static BufferedImage imageZnD;

	public static BufferedImage[] imageMTCSIcons;
	public static HashMap<String, ResourceLocation> imageButtonIcons = new HashMap<String, ResourceLocation>();

	static
	{
		try
		{
			imageZnD = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/logo.png"));
			imageScreenGlare = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/mtcs_screen_glare.png"));
			imageScreenGlareSmall = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_screen_glare.png"));
			imageScreenShade = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/mtcs_screen_shade.png"));

			imageMTCSIcons = new BufferedImage[14];
			for (int i = 0; i < imageMTCSIcons.length; i++)
			{
				imageMTCSIcons[i] = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/mtcs_icon" + i + ".png"));
			}

			MSMR.contrast = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_contrast.png"));
			MSMR.defaultBackground = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_default.png"));
			MSMR.defaultBackgroundDay = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_default_day.png"));
			MSMR.signalStrength = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_signalStrength.png"));
			MSMR.splash = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmrSplash.png"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}



	/**
	 * Tries to retrieve a resource location for the specified button name from the {@link #imageButtonIcons button icon map}.<br>
	 * Will attempt to load the image if it isn't present in the map.
	 * 
	 * @return The matching icon.
	 */
	public static ResourceLocation getScreenButtonIcon(String buttonName)
	{
		ResourceLocation iconLoc = imageButtonIcons.get(buttonName);
		if (iconLoc == null)
		{
			iconLoc = new ResourceLocation(ModData.ID, "textures/vehicleParts/cab/" + buttonName + ".png");
			imageButtonIcons.put(buttonName, iconLoc);
		}
		return iconLoc;
	}
}