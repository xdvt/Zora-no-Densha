package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_FLIRT;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisEndFLIRT;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisEndFLIRT extends AVehParRendererBase_FLIRT implements IVehiclePartRenderer
{
	/*
	 * Common pieces, shard amongst both types.
	 */
	private static final ObjectList gen_stripes_double = model.makeGroup("End_Stripes_Double");
	private static final ObjectList gen_stripes_sides = model.makeGroup("End_Stripes_Sides");
	private static final ObjectList gen_stripes_single = model.makeGroup("End_Stripes_Single");
	private static final ObjectList gen_stripes_top = model.makeGroup("End_Stripes_Top");

	/*
	 * High-level floor type.
	 */
	private static final ObjectList hi_chassis = model.makeGroup("EndHi_Chassis");
	private static final ObjectList hi_grey = model.makeGroup("EndHi_Grey");
	private static final ObjectList hi_inside = model.makeGroup("EndHi_Inside");

	/*
	 * Low-level floor type.
	 */
	private static final ObjectList lo_chassis = model.makeGroup("EndLo_Chassis");
	private static final ObjectList lo_grey = model.makeGroup("EndLo_Grey");
	private static final ObjectList lo_inside = model.makeGroup("EndLo_Inside");

	/** The part to render. */
	final VehParChassisEndFLIRT part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_ChassisEndFLIRT(VehParChassisEndFLIRT part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisEndFLIRT)
		{
			return new VehParRenderer_ChassisEndFLIRT((VehParChassisEndFLIRT)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				/*
				 * Type-dependent pieces.
				 */
				if (part.isHigh.get())
				{
					part.colorOut.apply(tessellator);
					hi_chassis.render(tessellator);

					applyGrey(tessellator);
					hi_grey.render(tessellator);

					part.colorIn.apply(tessellator);
					hi_inside.render(tessellator);
				}
				else
				{
					part.colorOut.apply(tessellator);
					lo_chassis.render(tessellator);

					applyGrey(tessellator);
					lo_grey.render(tessellator);

					part.colorIn.apply(tessellator);
					lo_inside.render(tessellator);
				}

				/*
				 * Common pieces.
				 */
				part.colorStripesDouble.apply(tessellator);
				gen_stripes_double.render(tessellator);

				part.colorSides.apply(tessellator);
				gen_stripes_sides.render(tessellator);

				part.colorStripesSingle.apply(tessellator);
				gen_stripes_single.render(tessellator);

				part.colorStripesTop.apply(tessellator);
				gen_stripes_top.render(tessellator);

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		GL11.glScalef(0.45F, 0.45F, 0.45F);

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
