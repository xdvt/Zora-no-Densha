package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_DBpza;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisDoorsDBpza;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisDoorsDBpza extends AVehParRendererBase_DBpza implements IVehiclePartRenderer
{
	/*
	 * Bottom-level door type
	 */
	private static final ObjectList bottom_chassis = model.makeGroup("BottomDoors_Chassis");
	private static final ObjectList bottom_colorA = model.makeGroup("BottomDoors_ColorA");
	private static final ObjectList bottom_interior = model.makeGroup("BottomDoors_Interior");
	private static final ObjectList bottom_stripeBottom = model.makeGroup("BottomDoors_StripeBottom");

	private static final ObjectList bottom_doorL = model.makeGroup("BottomDoors_DoorL");
	private static final ObjectList bottom_doorLW = model.makeGroup("BottomDoors_DoorL_Window");
	private static final ObjectList bottom_doorR = model.makeGroup("BottomDoors_DoorR");
	private static final ObjectList bottom_doorRW = model.makeGroup("BottomDoors_DoorR_Window");

	/*
	 * Vestibule door type
	 */
	private static final ObjectList vestibule_chassis = model.makeGroup("VestibuleDoors_Chassis");
	private static final ObjectList vestibule_colorA = model.makeGroup("VestibuleDoors_ColorA");
	private static final ObjectList vestibule_interior = model.makeGroup("VestibuleDoors_Interior");
	private static final ObjectList vestibule_stripeBottom = model.makeGroup("VestibuleDoors_StripeBottom", "VestibuleDoors_Chassis_ColorD");
	private static final ObjectList vestibule_stripeTop = model.makeGroup("VestibuleDoors_StripeTop");

	private static final ObjectList vestibule_doorL = model.makeGroup("VestibuleDoors_DoorL");
	private static final ObjectList vestibule_doorLW = model.makeGroup("VestibuleDoors_DoorL_Window");
	private static final ObjectList vestibule_doorR = model.makeGroup("VestibuleDoors_DoorR");
	private static final ObjectList vestibule_doorRW = model.makeGroup("VestibuleDoors_DoorR_Window");

	private static final Mat3x3 rotationMatrix = new Mat3x3().rotateY(180);

	/** The part to render. */
	final VehParChassisDoorsDBpza part;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_ChassisDoorsDBpza(VehParChassisDoorsDBpza part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisDoorsDBpza)
		{
			return new VehParRenderer_ChassisDoorsDBpza((VehParChassisDoorsDBpza)copyPart);
		}
		return null;
	}

	/**
	 * Helper method to render doors on either side.
	 */
	private void renderDoors(Tessellator tessellator, float state, int sign)
	{
		/*
		 * Prepare the respective door object.
		 */
		ObjectList doorL, doorR;
		float initialOffsetX = 0.075F;
		float offsetScale = 1.0F;
		boolean renderWindow = (TrainRenderer.renderPass == 1);

		switch (part.prop_type.get())
		{
			default:
				doorL = renderWindow ? bottom_doorLW : bottom_doorL;
				doorR = renderWindow ? bottom_doorRW : bottom_doorR;
				break;

			case 1:
				initialOffsetX = 0.0F;
				offsetScale = 1.6F;
				doorL = renderWindow ? vestibule_doorLW : vestibule_doorL;
				doorR = renderWindow ? vestibule_doorRW : vestibule_doorR;
				break;
		}

		/*
		 * Prepare door state level;
		 * 0.0 - 0.4 Extend step
		 * 0.4 - 0.6 Open door (Z)
		 * 0.6 - 1.0 Open door (X)
		 */
		float offX = (state >= 1.1F) ? 0.35F : (state > 0.1F) ? ((state - 0.1F) * 0.35F) : 0.0F;
		float offZ = (state >= 0.1F) ? 0.05F : (state * 0.5F);
		offX *= offsetScale;

		/*
		 * Apply color
		 */
		if (renderWindow)
		{
			tessellator.setColorRGBA_F(0.15F, 0.15F, 0.15F, 0.75F);
		}
		else
		{
			part.prop_colorDoors.apply(tessellator);
		}

		/*
		 * Render models with offset.
		 */
		for (int i = -1; i <= 2; i += 2)
		{
			tessellator.setTranslation(-offX * (i * sign) + initialOffsetX, 0.0F, -offZ * sign);
			ObjectList obj = (i < 0) ? doorL : doorR;

			if (sign == 1)
			{
				obj.render(tessellator, rotationMatrix);
			}
			else
			{
				obj.render(tessellator);
			}
		}
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return true;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Opaque = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		switch (TrainRenderer.renderPass)
		{
			case 0: {
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Opaque == null)
					{
						switch (part.prop_type.get())
						{
							/*
							 * Bottom Doors
							 */
							case 0:
								part.colorOut.apply(tessellator);
								bottom_chassis.render(tessellator);

								part.colorBaseplate.apply(tessellator);
								bottom_colorA.render(tessellator);

								part.colorIn.apply(tessellator);
								bottom_interior.render(tessellator);

								part.colorStripeBottom.apply(tessellator);
								bottom_stripeBottom.render(tessellator);

								tessellator.setTranslation(0.0D, 0.0D, 0.0D);
								break;

							/*
							 * Vestibule Doors
							 */
							case 1:
								part.colorOut.apply(tessellator);
								vestibule_chassis.render(tessellator);

								part.colorIn.apply(tessellator);
								vestibule_interior.render(tessellator);

								part.colorBaseplate.apply(tessellator);
								vestibule_colorA.render(tessellator);

								part.colorStripeBottom.apply(tessellator);
								vestibule_stripeBottom.render(tessellator);

								part.colorStripeTop.apply(tessellator);
								vestibule_stripeTop.render(tessellator);

								tessellator.setTranslation(0.0D, 0.0D, 0.0D);
								break;
						}

						renderDoors(tessellator, part.type_door.doorStateL(), 1);
						renderDoors(tessellator, part.type_door.doorStateR(), -1);

						tessellator.setTranslation(0, 0, 0);
						vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Opaque);
					}
				}
				tessellator.draw();
				break;
			}


			/*
			 * Transparent Items
			 */
			case 1: {

				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Transparent == null)
					{
						renderDoors(tessellator, part.type_door.doorStateL(), 1);
						renderDoors(tessellator, part.type_door.doorStateR(), -1);

						tessellator.setTranslation(0, 0, 0);
						vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Transparent);
					}
				}
				tessellator.draw();
				break;
			}

		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		/* Determine which model is rendered. */
		{
			long time = Minecraft.getMinecraft().theWorld.getTotalWorldTime();
			time %= (20 * 2);

			part.prop_type.set((int)(time / 20));
			part.updateVertexState = true;
		}

		/* Render part item. */
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}
}
