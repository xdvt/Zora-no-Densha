package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_SWF;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisWallSWF;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisWallSWF extends AVehParRendererBase_SWF implements IVehiclePartRenderer
{
	/*
	 * Objects
	 */
	/* Soft wall, single */
	private static final ObjectList soft1_frameBot = model.makeGroup("WallA_Single_ColorA");
	private static final ObjectList soft1_frameTop = model.makeGroup("WallA_Single_ColorB");
	private static final ObjectList soft1_panelBot = model.makeGroup("WallA_Single_ColorC");
	private static final ObjectList soft1_panelTop = model.makeGroup("WallA_Single_ColorD");
	private static final ObjectList soft1_inside = model.makeGroup("WallA_Single_Interior");

	/* Soft wall, double */
	private static final ObjectList soft2_frameBot = model.makeGroup("WallA_Double_ColorA");
	private static final ObjectList soft2_frameTop = model.makeGroup("WallA_Double_ColorB");
	private static final ObjectList soft2_panelBot = model.makeGroup("WallA_Double_ColorC");
	private static final ObjectList soft2_panelTop = model.makeGroup("WallA_Double_ColorD");
	private static final ObjectList soft2_inside = model.makeGroup("WallA_Double_Interior");
	private static final ObjectList soft2_detail = model.makeGroup("WallA_Double_Detail");
	private static final ObjectList soft2_detailRev = model.makeGroup("WallA_Double_DetailReversed");

	/* Solid wall, single */
	private static final ObjectList solid1_frameBot = model.makeGroup("WallB_Single_ColorA");
	private static final ObjectList solid1_frameTop = model.makeGroup("WallB_Single_ColorB");
	private static final ObjectList solid1_panelBot = model.makeGroup("WallB_Single_ColorC");
	private static final ObjectList solid1_panelTop = model.makeGroup("WallB_Single_ColorD");
	private static final ObjectList solid1_inside = model.makeGroup("WallB_Single_Interior");

	/* Solid wall, double */
	private static final ObjectList solid2_frameBot = model.makeGroup("WallB_Double_ColorA");
	private static final ObjectList solid2_frameTop = model.makeGroup("WallB_Double_ColorB");
	private static final ObjectList solid2_panelBot = model.makeGroup("WallB_Double_ColorC");
	private static final ObjectList solid2_panelTop = model.makeGroup("WallB_Double_ColorD");
	private static final ObjectList solid2_inside = model.makeGroup("WallB_Double_Interior");
	private static final ObjectList solid2_detail = model.makeGroup("WallB_Double_Detail");
	private static final ObjectList solid2_detailRev = model.makeGroup("WallB_Double_DetailReversed");

	/*
	 * Non-static fields
	 */
	private final VehParChassisWallSWF part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_ChassisWallSWF(VehParChassisWallSWF part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisWallSWF)
		{
			return new VehParRenderer_ChassisWallSWF((VehParChassisWallSWF)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft mc = Minecraft.getMinecraft();
		mc.renderEngine.bindTexture(RenderUtil.texture_white);

		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				final boolean isDouble = part.isDouble.get();
				final boolean isReversed = part.isReversed.get();

				switch (part.wallType.get())
				{
					case SOFT: {
						if (isDouble)
						{
							part.colorFrameBot.apply(tessellator);
							soft2_frameBot.render(tessellator);

							part.colorFrameTop.apply(tessellator);
							soft2_frameTop.render(tessellator);

							part.colorPanelBot.apply(tessellator);
							soft2_panelBot.render(tessellator);

							part.colorPanelTop.apply(tessellator);
							soft2_panelTop.render(tessellator);

							part.colorInside.apply(tessellator);
							soft2_inside.render(tessellator);

							part.colorDetail.apply(tessellator);
							(isReversed ? soft2_detailRev : soft2_detail).render(tessellator);
						}
						else
						{
							part.colorFrameBot.apply(tessellator);
							soft1_frameBot.render(tessellator);

							part.colorFrameTop.apply(tessellator);
							soft1_frameTop.render(tessellator);

							part.colorPanelBot.apply(tessellator);
							soft1_panelBot.render(tessellator);

							part.colorPanelTop.apply(tessellator);
							soft1_panelTop.render(tessellator);

							part.colorInside.apply(tessellator);
							soft1_inside.render(tessellator);
						}
						break;
					}

					case SOLID: {
						if (isDouble)
						{
							part.colorFrameBot.apply(tessellator);
							solid2_frameBot.render(tessellator);

							part.colorFrameTop.apply(tessellator);
							solid2_frameTop.render(tessellator);

							part.colorPanelBot.apply(tessellator);
							solid2_panelBot.render(tessellator);

							part.colorPanelTop.apply(tessellator);
							solid2_panelTop.render(tessellator);

							part.colorInside.apply(tessellator);
							solid2_inside.render(tessellator);

							part.colorDetail.apply(tessellator);
							(isReversed ? solid2_detailRev : solid2_detail).render(tessellator);
						}
						else
						{
							part.colorFrameBot.apply(tessellator);
							solid1_frameBot.render(tessellator);

							part.colorFrameTop.apply(tessellator);
							solid1_frameTop.render(tessellator);

							part.colorPanelBot.apply(tessellator);
							solid1_panelBot.render(tessellator);

							part.colorPanelTop.apply(tessellator);
							solid1_panelTop.render(tessellator);

							part.colorInside.apply(tessellator);
							solid1_inside.render(tessellator);
						}
						break;
					}
				}

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();

		/*
		 * Render UVID
		 */
		if (part.hasUVID.get())
		{
			VehicleData vehicle = part.getVehicle();
			if (vehicle != null)
			{
				double partWidth = 0.21;
				if (part.isDouble.get())
				{
					partWidth *= 2;
				}

				GL11.glPushMatrix();
				GL11.glColor3f(1.0F, 1.0F, 1.0F);
				GL11.glTranslated(-partWidth, -0.35, 0.2625);
				GL11.glScalef(0.0064F, -0.0064F, 0.01F);
				{
					int yOff = 0;
					for (String s : vehicle.getUVID().toString().split(" "))
					{
						mc.fontRenderer.drawString(s, 0, yOff, 0xe2d5b5);
						yOff += 10;
					}
				}
				GL11.glPopMatrix();
			}
		}
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.75F, 0.75F, 0.75F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}

}
