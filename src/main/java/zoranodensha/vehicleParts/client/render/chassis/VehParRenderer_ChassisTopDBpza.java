package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_DBpza;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisTopDBpza;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisTopDBpza extends AVehParRendererBase_DBpza implements IVehiclePartRenderer
{
	private static final ObjectList topA_chassis = model.makeGroup("TopA_Chassis");
	private static final ObjectList topA_colorA = model.makeGroup("TopA_ColorA");
	private static final ObjectList topA_colorB = model.makeGroup("TopA_ColorB");
	private static final ObjectList topA_interior = model.makeGroup("TopA_Interior");
	private static final ObjectList topA_stripeTop = model.makeGroup("TopA_StripeTop");

	private static final ObjectList topB_chassis = model.makeGroup("TopB_Chassis");
	private static final ObjectList topB_colorA = model.makeGroup("TopB_ColorA");
	private static final ObjectList topB_colorB = model.makeGroup("TopB_ColorB");
	private static final ObjectList topB_interior = model.makeGroup("TopB_Interior");
	private static final ObjectList topB_stripeTop = model.makeGroup("TopB_StripeTop");
	private static final ObjectList topB_windows = model.makeGroup("TopB_Windows");

	private static final ObjectList topC_chassis = model.makeGroup("TopC_Chassis");
	private static final ObjectList topC_colorA = model.makeGroup("TopC_ColorA");
	private static final ObjectList topC_colorB = model.makeGroup("TopC_ColorB");
	private static final ObjectList topC_interior = model.makeGroup("TopC_Interior");
	private static final ObjectList topC_stripeTop = model.makeGroup("TopC_StripeTop");
	private static final ObjectList topC_windows = model.makeGroup("TopC_Windows");


	/** The part to render. */
	final VehParChassisTopDBpza part;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_ChassisTopDBpza(VehParChassisTopDBpza part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisTopDBpza)
		{
			return new VehParRenderer_ChassisTopDBpza((VehParChassisTopDBpza)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (part.prop_type.get() != 0) || (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Opaque = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		switch (TrainRenderer.renderPass)
		{
			case 0: {
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Opaque == null)
					{
						switch (part.prop_type.get())
						{
							case 0:
								part.colorOut.apply(tessellator);
								topA_chassis.render(tessellator);

								part.colorIn.apply(tessellator);
								topA_interior.render(tessellator);

								part.prop_colorFloor.apply(tessellator);
								topA_colorA.render(tessellator);

								part.prop_colorRoof.apply(tessellator);
								topA_colorB.render(tessellator);

								part.prop_colorStripe.apply(tessellator);
								topA_stripeTop.render(tessellator);
								break;

							case 1:
								part.colorOut.apply(tessellator);
								topB_chassis.render(tessellator);

								part.colorIn.apply(tessellator);
								topB_interior.render(tessellator);

								part.prop_colorFloor.apply(tessellator);
								topB_colorA.render(tessellator);

								part.prop_colorRoof.apply(tessellator);
								topB_colorB.render(tessellator);

								part.prop_colorStripe.apply(tessellator);
								topB_stripeTop.render(tessellator);
								break;

							case 2:
								part.colorOut.apply(tessellator);
								topC_chassis.render(tessellator);

								part.colorIn.apply(tessellator);
								topC_interior.render(tessellator);

								part.prop_colorFloor.apply(tessellator);
								topC_colorA.render(tessellator);

								part.prop_colorRoof.apply(tessellator);
								topC_colorB.render(tessellator);

								part.prop_colorStripe.apply(tessellator);
								topC_stripeTop.render(tessellator);
								break;
						}

						vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Opaque);
					}
				}
				tessellator.draw();

				break;
			}

			case 1: {
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Transparent == null)
					{
						switch (part.prop_type.get())
						{
							case 1:
								tessellator.setColorRGBA_F(0.15F, 0.15F, 0.15F, 0.75F);
								topB_windows.render(tessellator);
								break;

							case 2:
								tessellator.setColorRGBA_F(0.15F, 0.15F, 0.15F, 0.75F);
								topC_windows.render(tessellator);
								break;
						}

						vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Transparent);
					}
				}
				tessellator.draw();

				break;
			}
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		/* Determine which model is rendered. */
		{
			long time = Minecraft.getMinecraft().theWorld.getTotalWorldTime();
			time %= (20 * 3);

			part.prop_type.set((int)(time / 20));
			part.updateVertexState = true;
		}

		/* Render part item. */
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}
}