package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_DBpza;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisCabDBpza;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisCabDBpza extends AVehParRendererBase_DBpza implements IVehiclePartRenderer
{
	private static final ObjectList cab_chassis = model.makeGroup("Cab_Chassis");
	private static final ObjectList cab_colorA = model.makeGroup("Cab_ColorA");
	private static final ObjectList cab_colorB = model.makeGroup("Cab_ColorB");
	private static final ObjectList cab_colorC = model.makeGroup("Cab_ColorC");
	private static final ObjectList cab_destBoard = model.makeGroup("Cab_DestBoard");
	private static final ObjectList cab_interior = model.makeGroup("Cab_Interior", "Cab_Desk");
	private static final ObjectList cab_interiorBehind = model.makeGroup("Cab_InteriorBehind");
	private static final ObjectList cab_stripeBottom = model.makeGroup("Cab_StripeBottom");
	private static final ObjectList cab_stripeTop = model.makeGroup("Cab_StripeTop");
	private static final ObjectList cab_windows = model.makeGroup("Cab_Windows");

	private static final ObjectList lights_top = model.makeGroup("Cab_LightTop");
	private static final ObjectList lights_bottomRed = model.makeGroup("Cab_LightBottomRed");
	private static final ObjectList lights_bottomWhite = model.makeGroup("Cab_LightBottomWhite");

	/** The part to render. */
	final VehParChassisCabDBpza part;

	/* Vertex states. */
	private TesselatorVertexState vertexState_Lights_Off;
	private TesselatorVertexState vertexState_Lights_On;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_OpaqueLight;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_ChassisCabDBpza(VehParChassisCabDBpza part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisCabDBpza)
		{
			return new VehParRenderer_ChassisCabDBpza((VehParChassisCabDBpza)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return true;
	}

	public void renderLights(ELightMode mode, Tessellator tessellator)
	{
		/*
		 * Render active lights
		 */
		if (mode != ELightMode.OFF)
		{
			GL11.glPushAttrib(GL11.GL_LIGHTING);
			GL11.glDisable(GL11.GL_LIGHTING);
			{
				RenderUtil.lightmapPush();
				{
					RenderUtil.lightmapBright();

					tessellator.startDrawing(GL11.GL_TRIANGLES);
					if (vertexState_Lights_On == null)
					{
						switch (mode)
						{
							case FRONT:
								tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
								lights_top.render(tessellator);
								lights_bottomWhite.render(tessellator);
								break;

							case REAR:
								tessellator.setColorOpaque_F(1.0F, 0.0F, 0.0F);
								lights_bottomRed.render(tessellator);
								break;
						}
						vertexState_Lights_On = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Lights_On);
					}
					tessellator.draw();
				}
				RenderUtil.lightmapPop();
			}
			GL11.glPopAttrib();
		}

		/*
		 * Render inactive lights
		 */
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		if (vertexState_Lights_Off == null)
		{
			tessellator.setColorOpaque_F(0.1F, 0.1F, 0.1F);

			switch (mode)
			{
				case OFF:
					lights_top.render(tessellator);
					lights_bottomRed.render(tessellator);
					lights_bottomWhite.render(tessellator);
					break;

				case FRONT:
					lights_bottomRed.render(tessellator);
					break;

				case REAR:
					lights_top.render(tessellator);
					lights_bottomWhite.render(tessellator);
					break;
			}
			vertexState_Lights_Off = VertexStateCreator_Tris.getVertexState();
		}
		else
		{
			tessellator.setVertexState(vertexState_Lights_Off);
		}
		tessellator.draw();
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Lights_Off = null;
			vertexState_Lights_On = null;
			vertexState_Opaque = null;
			vertexState_OpaqueLight = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		switch (TrainRenderer.renderPass)
		{
			case 0: {
				/* Render chassis. */
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Opaque == null)
					{
						part.colorOut.apply(tessellator);
						cab_chassis.render(tessellator);

						part.colorBaseplate.apply(tessellator);
						cab_colorA.render(tessellator);

						part.colorRoof.apply(tessellator);
						cab_colorB.render(tessellator);

						part.colorFront.apply(tessellator);
						cab_colorC.render(tessellator);

						tessellator.setColorOpaque_F(0.1F, 0.1F, 0.1F);
						cab_destBoard.render(tessellator);

						part.colorIn.apply(tessellator);
						cab_interiorBehind.render(tessellator); // This interior piece is not affected by the cab light.

						part.colorStripeBottom.apply(tessellator);
						cab_stripeBottom.render(tessellator);

						part.colorStripeTop.apply(tessellator);
						cab_stripeTop.render(tessellator);
					}
					else
					{
						tessellator.setVertexState(vertexState_Opaque);
					}
				}
				tessellator.draw();

				RenderUtil.lightmapPush();
				{
					RenderUtil.lightmapBright(part);

					tessellator.startDrawing(GL11.GL_TRIANGLES);
					{
						if (vertexState_OpaqueLight == null)
						{
							part.colorIn.apply(tessellator);
							cab_interior.render(tessellator);
						}
						else
						{
							tessellator.setVertexState(vertexState_OpaqueLight);
						}
					}
					tessellator.draw();
				}
				RenderUtil.lightmapPop();

				/* Render lights. */
				renderLights(part.getLightMode(), tessellator);

				/* Render destination info. */
				renderDestination();
				break;
			}

			case 1: {
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Transparent == null)
					{
						tessellator.setColorRGBA_F(0.15F, 0.15F, 0.15F, 0.75F);
						cab_windows.render(tessellator);

						vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Transparent);
					}
				}
				tessellator.draw();
				break;
			}
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glPushAttrib(GL11.GL_DEPTH_TEST);
		{
			GL11.glEnable(GL11.GL_DEPTH_TEST);
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.4F, 0.4F, 0.4F);
			renderPart(1.0F);
		}
		GL11.glPopAttrib();
		GL11.glPopMatrix();
	}

	/**
	 * Helper method to render destination info.<br>
	 * Runs checks beforehand.
	 */
	private void renderDestination()
	{
		Train train = part.getTrain();
		if (train == null || ModCenter.cfg.rendering.detailVehicles <= 0)
		{
			return;
		}

		PartTypeCabBasic cab = train.getActiveCab();
		if (cab != null)
		{
			String[] arr = cab.getDestination();
			if (arr == null || arr.length == 0)
			{
				return;
			}

			String s = arr[0];
			if (s == null || s.isEmpty())
			{
				return;
			}

			GL11.glPushAttrib(GL11.GL_LIGHTING);
			GL11.glDisable(GL11.GL_LIGHTING);
			{
				FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
				final boolean isUnicode = fontRenderer.getUnicodeFlag();

				fontRenderer.setUnicodeFlag(true);
				s = fontRenderer.trimStringToWidth(s, 80);

				GL11.glColor3f(1.0F, 1.0F, 1.0F);
				GL11.glTranslatef(0.178F, 0.525F, 0.0F);
				GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(-18.0F, 1.0F, 0.0F, 0.0F);
				GL11.glScalef(0.0125F, -0.0125F, 0.0125F);

				RenderUtil.lightmapPush();
				{
					RenderUtil.lightmapBright();
					fontRenderer.drawString(s, -fontRenderer.getStringWidth(s) / 2, 0, 0xFFB90C);
					fontRenderer.setUnicodeFlag(isUnicode);
				}
				RenderUtil.lightmapPop();
			}
			GL11.glPopAttrib();
		}
	}
}
