package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_SWF;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisEndSWF;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisEndSWF extends AVehParRendererBase_SWF implements IVehiclePartRenderer
{
	/*
	 * Objects
	 */
	/* Soft walls' end piece. */
	private static final ObjectList endSoft = model.makeGroup("EndA");
	private static final ObjectList endSoft_sideBot = model.makeGroup("EndA_ColorA");
	private static final ObjectList endSoft_sideTop = model.makeGroup("EndA_ColorB");
	private static final ObjectList endSoft_wheels = model.makeGroup("EndA_ColorE");

	/* Solid walls' end piece. */
	private static final ObjectList endSolid = model.makeGroup("EndB");
	private static final ObjectList endSolid_sideBot = model.makeGroup("EndB_ColorA");
	private static final ObjectList endSolid_sideTop = model.makeGroup("EndB_ColorB");
	private static final ObjectList endSolid_wheels = model.makeGroup("EndB_ColorE");

	private final VehParChassisEndSWF part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_ChassisEndSWF(VehParChassisEndSWF part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisEndSWF)
		{
			return new VehParRenderer_ChassisEndSWF((VehParChassisEndSWF)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				switch (part.walltype.get())
				{
					case SOFT: {
						part.color.apply(tessellator);
						endSoft.render(tessellator);

						part.colorBot.apply(tessellator);
						endSoft_sideBot.render(tessellator);

						part.colorTop.apply(tessellator);
						endSoft_sideTop.render(tessellator);

						part.color_wheels.apply(tessellator);
						endSoft_wheels.render(tessellator);

						break;
					}

					case SOLID: {
						part.color.apply(tessellator);
						endSolid.render(tessellator);

						part.colorBot.apply(tessellator);
						endSolid_sideBot.render(tessellator);

						part.colorTop.apply(tessellator);
						endSolid_sideTop.render(tessellator);

						part.color_wheels.apply(tessellator);
						endSolid_wheels.render(tessellator);

						break;
					}
				}

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 1.0F, 0.25F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.50F, 0.50F, 0.50F);
		}

		GL11.glRotatef(180, 0, 1, 0);
		renderPart(1.0F);
		GL11.glPopMatrix();
	}

}
