package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeCabBasic;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_FLIRT;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisFLIRT.EChassis;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisFLIRT extends AVehParRendererBase_FLIRT implements IVehiclePartRenderer
{
	private static final ObjectList gen_stripes_double = model.makeGroup("Chassis_Stripes_Double");
	private static final ObjectList gen_stripes_single = model.makeGroup("Chassis_Stripes_Single");
	private static final ObjectList gen_stripes_top = model.makeGroup("Chassis_Stripes_Top");

	private static final ObjectList hi_chassis = model.makeGroup("ChassisHi_Chassis");
	private static final ObjectList hi_grey = model.makeGroup("ChassisHi_Grey");
	private static final ObjectList hi_inside = model.makeGroup("ChassisHi_Inside");
	private static final ObjectList hi_sides = model.makeGroup("ChassisHi_Stripes_Sides");
	private static final ObjectList hi_windows = model.makeGroup("ChassisHi_Windows");

	private static final ObjectList hiEl_chassis = model.makeGroup("ChassisHiEl_Chassis");
	private static final ObjectList hiEl_grey = model.makeGroup("ChassisHiEl_Grey");
	private static final ObjectList hiEl_inside = model.makeGroup("ChassisHiEl_Inside");
	private static final ObjectList hiEl_sides = model.makeGroup("ChassisHiEl_Stripes_Sides");
	private static final ObjectList hiEl_windows = model.makeGroup("ChassisHiEl_Windows");

	private static final ObjectList lo_chassis = model.makeGroup("ChassisLo_Chassis");
	private static final ObjectList lo_grey = model.makeGroup("ChassisLo_Grey");
	private static final ObjectList lo_inside = model.makeGroup("ChassisLo_Inside");
	private static final ObjectList lo_sides = model.makeGroup("ChassisLo_Stripes_Sides");
	private static final ObjectList lo_windows = model.makeGroup("ChassisLo_Windows");

	private static final ObjectList loBd_black = model.makeGroup("ChassisLoBd_Black");
	private static final ObjectList loBd_inside = model.makeGroup("ChassisLoBd_Inside");
	private static final ObjectList loBd_sides = model.makeGroup("ChassisLoBd_Stripes_Sides");
	private static final ObjectList loBd_windows = model.makeGroup("ChassisLoBd_Windows");

	private static final ObjectList md_chassis = model.makeGroup("ChassisMd_Chassis");
	private static final ObjectList md_grey = model.makeGroup("ChassisMd_Grey");
	private static final ObjectList md_inside = model.makeGroup("ChassisMd_Inside");
	private static final ObjectList md_sides = model.makeGroup("ChassisMd_Stripes_Sides");
	private static final ObjectList md_windows = model.makeGroup("ChassisMd_Windows");

	private static final ObjectList tr_chassis = model.makeGroup("ChassisTr_Chassis");
	private static final ObjectList tr_grey = model.makeGroup("ChassisTr_Grey");
	private static final ObjectList tr_inside = model.makeGroup("ChassisTr_Inside");
	private static final ObjectList tr_sides = model.makeGroup("ChassisTr_Stripes_Sides");
	private static final ObjectList tr_windows = model.makeGroup("ChassisTr_Windows");

	/** Maximum width per line of destination info, in pixels. */
	private static final int MAX_LINE_WIDTH = 100;

	/** The part to render. */
	final VehParChassisFLIRT part;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_ChassisFLIRT(VehParChassisFLIRT part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisFLIRT)
		{
			return new VehParRenderer_ChassisFLIRT((VehParChassisFLIRT)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return true;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Opaque = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		switch (TrainRenderer.renderPass)
		{
			case 0:
				/* Tessellator stuff */
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				if (vertexState_Opaque == null)
				{
					/*
					 * Render common pieces.
					 */
					part.colorStripesDouble.apply(tessellator);
					gen_stripes_double.render(tessellator);

					part.colorStripesSingle.apply(tessellator);
					gen_stripes_single.render(tessellator);

					part.colorStripesTop.apply(tessellator);
					gen_stripes_top.render(tessellator);


					/*
					 * Render type-dependent pieces.
					 */
					switch (part.prop_type.get())
					{
						case HI:
							part.colorOut.apply(tessellator);
							hi_chassis.render(tessellator);

							applyGrey(tessellator);
							hi_grey.render(tessellator);

							part.colorIn.apply(tessellator);
							hi_inside.render(tessellator);

							part.colorSides.apply(tessellator);
							hi_sides.render(tessellator);
							break;

						case HI_EL:
							part.colorOut.apply(tessellator);
							hiEl_chassis.render(tessellator);

							applyGrey(tessellator);
							hiEl_grey.render(tessellator);

							part.colorIn.apply(tessellator);
							hiEl_inside.render(tessellator);

							part.colorSides.apply(tessellator);
							hiEl_sides.render(tessellator);
							break;

						case LO:
							part.colorOut.apply(tessellator);
							lo_chassis.render(tessellator);

							applyGrey(tessellator);
							lo_grey.render(tessellator);

							part.colorIn.apply(tessellator);
							lo_inside.render(tessellator);

							part.colorSides.apply(tessellator);
							lo_sides.render(tessellator);
							break;

						case LO_BD:
							tessellator.setColorOpaque_F(0.05F, 0.05F, 0.05F);
							loBd_black.render(tessellator);

							part.colorOut.apply(tessellator);
							lo_chassis.render(tessellator);

							applyGrey(tessellator);
							lo_grey.render(tessellator);

							part.colorIn.apply(tessellator);
							loBd_inside.render(tessellator);

							part.colorSides.apply(tessellator);
							loBd_sides.render(tessellator);
							break;

						case MD:
							part.colorOut.apply(tessellator);
							md_chassis.render(tessellator);

							applyGrey(tessellator);
							md_grey.render(tessellator);

							part.colorIn.apply(tessellator);
							md_inside.render(tessellator);

							part.colorSides.apply(tessellator);
							md_sides.render(tessellator);
							break;

						case TR:
							part.colorOut.apply(tessellator);
							tr_chassis.render(tessellator);

							applyGrey(tessellator);
							tr_grey.render(tessellator);

							part.colorIn.apply(tessellator);
							tr_inside.render(tessellator);

							part.colorSides.apply(tessellator);
							tr_sides.render(tessellator);
							break;
					}

					/* Create vertex state. */
					vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState_Opaque);
				}
				tessellator.draw();

				/*
				 * Render UVID and destination info, if applicable.
				 */
				if (part.hasUVID.get())
				{
					renderUVID();
				}

				if (part.prop_type.get() == EChassis.LO_BD)
				{
					renderDestination();
				}
				break;

			case 1:
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				if (vertexState_Transparent == null)
				{
					part.colorWindows.apply(tessellator);

					switch (part.prop_type.get())
					{
						case HI:
							hi_windows.render(tessellator);
							break;

						case HI_EL:
							hiEl_windows.render(tessellator);
							break;

						case LO:
							lo_windows.render(tessellator);
							break;

						case LO_BD:
							loBd_windows.render(tessellator);
							break;

						case MD:
							md_windows.render(tessellator);
							break;

						case TR:
							tr_windows.render(tessellator);
							break;
					}
					vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState_Transparent);
				}
				tessellator.draw();
				break;
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		/* Determine which model is rendered. */
		{
			final EChassis[] types = EChassis.values();
			long time = Minecraft.getMinecraft().theWorld.getTotalWorldTime();
			time %= (20 * types.length);

			part.prop_type.set(types[(int)(time / 20)]);
			part.updateVertexState = true;
		}

		/* Actually render item. */
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}

			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}

	/**
	 * Helper method to render the vehicle's UVID.<br>
	 * Runs checks beforehand.
	 */
	private void renderUVID()
	{
		VehicleData vehicle = part.getVehicle();
		if (vehicle == null)
		{
			return;
		}

		GL11.glPushAttrib(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_LIGHTING);
		{
			FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
			String uvid = vehicle.getUVID().toString();
			int stringX = -fontRenderer.getStringWidth(uvid) / 2;
			int colour = part.colorOut.get().offset(0.25F).toAHEX();

			for (int i = 0; i < 2; ++i)
			{
				GL11.glPushMatrix();
				{
					if (i == 1)
					{
						GL11.glRotatef(180, 0, 1, 0);
					}

					GL11.glTranslatef(0.0F, -0.8F, 0.751F);
					GL11.glScalef(0.006F, -0.00875F, 0.00875F);
					fontRenderer.drawSplitString(uvid, stringX, 0, uvid.length() * 10, colour);
				}
				GL11.glPopMatrix();
			}
		}
		GL11.glPopAttrib();
	}

	/**
	 * Helper method to render destination info on the boards.<br>
	 * Runs checks beforehand.
	 */
	private void renderDestination()
	{
		Train train = part.getTrain();
		if (train == null || ModCenter.cfg.rendering.detailVehicles <= 0)
		{
			return;
		}

		GL11.glPushAttrib(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_LIGHTING);
		{
			PartTypeCabBasic cab = train.getActiveCab();
			if (cab != null)
			{
				String[] arr = cab.getDestination();
				if (arr == null || arr.length == 0)
				{
					return;
				}

				FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;

				final boolean isUnicode = fontRenderer.getUnicodeFlag();
				fontRenderer.setUnicodeFlag(true);
				{
					String[] s = new String[Math.min(arr.length, 2)];
					for (int i = 0; i < s.length; ++i)
					{
						s[i] = (arr[i] == null) ? "" : (i == 0) ? fontRenderer.trimStringToWidth(arr[i], MAX_LINE_WIDTH) : arr[i];
					}

					if (s.length > 1 && !s[1].isEmpty())
					{
						if (fontRenderer.getStringWidth(s[1]) >= MAX_LINE_WIDTH)
						{
							int offset = train.ticksExisted / 4;

							for (int i = 0; i < 24; ++i)
							{
								s[1] += " ";
							}

							for (int i = offset % s[1].length(); i > 0; --i)
							{
								s[1] = s[1].substring(1) + s[1].substring(0, 1);
							}

							s[1] = fontRenderer.trimStringToWidth(s[1], MAX_LINE_WIDTH);
						}
					}

					for (int i = 0; i < 2; ++i)
					{
						GL11.glColor3f(1.0F, 1.0F, 1.0F);
						GL11.glPushMatrix();
						{
							if (i > 0)
							{
								GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
							}

							GL11.glTranslatef(0.0F, 0.2F, 0.75F);
							GL11.glScalef(0.0075F, -0.0075F, 0.0075F);
							GL11.glPushAttrib(GL11.GL_LIGHTING);
							GL11.glDisable(GL11.GL_LIGHTING);
							{
								RenderUtil.lightmapPush();
								RenderUtil.lightmapBright();
								{
									GL11.glTranslatef(0.0F, -10.0F, 0.0F);
									if (s.length > 1 && !s[1].isEmpty())
									{
										fontRenderer.drawString(s[1], -fontRenderer.getStringWidth(s[1]) / 2, 0, 0xFFB90C);
									}

									GL11.glTranslatef(0.0F, -9.0F, 0.0F);
									if (s.length > 0 && !s[0].isEmpty())
									{
										fontRenderer.drawString(s[0], -fontRenderer.getStringWidth(s[0]) / 2, 0, 0xFFB90C);
									}
								}
								RenderUtil.lightmapPop();
							}
							GL11.glPopAttrib();
						}
						GL11.glPopMatrix();
					}
				}
				fontRenderer.setUnicodeFlag(isUnicode);
			}
		}
		GL11.glPopAttrib();
	}
}
