package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_SWF;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateEndSWF;



@SideOnly(Side.CLIENT)
public class VehParRenderer_BaseplateEndSWF extends AVehParRendererBase_SWF implements IVehiclePartRenderer
{
	private static final ObjectList buffer = model.makeGroup("DetailA");

	private final VehParBaseplateEndSWF part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_BaseplateEndSWF(VehParBaseplateEndSWF part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParBaseplateEndSWF)
		{
			return new VehParRenderer_BaseplateEndSWF((VehParBaseplateEndSWF)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Tessellator tessellator = Tessellator.instance;

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.colorBuffer.apply(tessellator);
				tessellator.setTranslation(-0.07, 0.0, 0.43);
				buffer.render(tessellator);
				tessellator.setTranslation(-0.07, 0.0, -0.43);
				buffer.render(tessellator);
				tessellator.setTranslation(0, 0, 0);

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(0.25F, 0.0F, 0.25F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			else
			{
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				GL11.glScalef(0.75F, 0.75F, 0.75F);
			}
			GL11.glRotatef(180, 0, 1, 0);
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}

}
