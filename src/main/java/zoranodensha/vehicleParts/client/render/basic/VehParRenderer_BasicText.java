package zoranodensha.vehicleParts.client.render.basic;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicText;



@SideOnly(Side.CLIENT)
public class VehParRenderer_BasicText implements IVehiclePartRenderer
{
	/** The part to render. */
	final VehParBasicText part;



	public VehParRenderer_BasicText(VehParBasicText part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParBasicText)
		{
			return new VehParRenderer_BasicText((VehParBasicText)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 1);
	}

	@Override
	public void renderPart(float partialTick)
	{
		String s = part.text.get();
		if (!s.isEmpty())
		{
			renderText(s, part.color.get().toAHEX());
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		String s = part.text.get();
		if (s.isEmpty())
		{
			return;
		}

		GL11.glPushMatrix();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glScalef(2.5F, 2.5F, 2.5F);

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		}

		GL11.glPushAttrib(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_LIGHTING);
		renderText(s, 0xFFFFFF);
		GL11.glPopAttrib();
		GL11.glPopMatrix();
	}

	/**
	 * Called to render the given String with the given HEX color. Renders the String's back side as well.
	 */
	private static void renderText(String text, int color)
	{
		FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
		int width = fontRenderer.getStringWidth(text) / 2;
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glScalef(0.01F, -0.01F, 0.01F);
		fontRenderer.drawString(text, -width, 0, color);
		GL11.glEnable(GL11.GL_CULL_FACE);
	}
}
