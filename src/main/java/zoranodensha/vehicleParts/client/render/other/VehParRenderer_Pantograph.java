package zoranodensha.vehicleParts.client.render.other;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.other.VehParPantograph;



@SideOnly(Side.CLIENT)
public class VehParRenderer_Pantograph implements IVehiclePartRenderer
{
	private static final ObjModelSpeedy model = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_pantograph");

	private static final ObjectList armBottom = model.makeGroup("Pantograph_ArmBot");
	private static final ObjectList armTop_Double = model.makeGroup("Pantograph_ArmTop_Double");
	private static final ObjectList armTop_Single = model.makeGroup("Pantograph_ArmTop_Single");
	private static final ObjectList bot_Frame = model.makeGroup("Pantograph_Bot_Frame");
	private static final ObjectList bot_Insulators = model.makeGroup("Pantograph_Bot_Insulators");
	private static final ObjectList bot_Springs = model.makeGroup("Pantograph_Bot_Springs");
	private static final ObjectList top_Contacts = model.makeGroup("Pantograph_Top_Contacts");
	private static final ObjectList top_Frame = model.makeGroup("Pantograph_Top_Frame");
	private static final ObjectList top_Guides = model.makeGroup("Pantograph_Top_Guides");

	/** The part to render. */
	final VehParPantograph part;

	private final Mat3x3 rotationMatrix_armBot = new Mat3x3().rotateZ(-40);
	private final Mat3x3 rotationMatrix_armTop = new Mat3x3().rotateZ(40);
	private TesselatorVertexState vertexState;



	public VehParRenderer_Pantograph(VehParPantograph part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParPantograph)
		{
			return new VehParRenderer_Pantograph((VehParPantograph)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		float extension = part.animationExtension;
		float lastExtension = part.animationExtensionLast;

		if (extension == 0.0F)
		{
			renderPartsStatic(false);
		}
		else if (extension == 1.0F)
		{
			renderPartsStatic(true);
		}
		else
		{
			renderPartsAnimated(lastExtension + ((extension - lastExtension) * partialTick));
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glTranslatef(0.5F, -0.1F, 0.5F);
			GL11.glScalef(0.75F, 0.75F, 0.75F);
			renderPartsAnimated(1.0F);
		}
		GL11.glPopMatrix();
	}

	/**
	 * Dynamic rendering while the arm is extracting or retracting.
	 * 
	 * @param extension - How far the pantograph has extended.
	 */
	private void renderPartsAnimated(float extension)
	{
		vertexState = null;
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

		/* Oh.. I accidentally inverted animations. This is a fix. */
		extension = 1.0F - extension;

		/* And this fixes a slight error with offsets during the animation.. */
		float f = extension;
		if (f > 0.5F)
		{
			f = 1.0F - f;
		}

		/* Render in order, from base to head. */
		part.colorBaseFrame.apply();
		bot_Frame.render();
		part.colorBaseInsulators.apply();
		bot_Insulators.render();
		part.colorBaseSprings.apply();
		bot_Springs.render();

		GL11.glPushMatrix();
		GL11.glTranslated(0.07, 0.125, 0.0);
		GL11.glRotatef(40.0F * extension, 0, 0, 1);
		part.colorArmBottom.apply();
		armBottom.render();
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glTranslated(-0.5764 - (0.2664 * extension) - (0.15 * f), 0.7747 - (0.5675 * extension), 0.0);
		GL11.glRotatef(-40.0F * extension, 0, 0, 1);
		part.colorArmTop.apply();
		switch (part.prop_type.get())
		{
			case HEAD1_ARM1:
				armTop_Single.render();
				break;

			case HEAD1_ARM2:
				armTop_Double.render();
				break;
		}
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glTranslated(0.1836 + (0.0633 * extension), 1.5642 - (1.2408 * extension), 0.0);
		GL11.glColor4f(0.2F, 0.2F, 0.2F, 1.0F);
		top_Contacts.render();
		part.colorHeadFrame.apply();
		top_Frame.render();
		part.colorHeadGuides.apply();
		top_Guides.render();

		GL11.glPopMatrix();
	}

	/**
	 * Static rendering while the arm is fully extended or retracted.
	 */
	private void renderPartsStatic(boolean isExtended)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.colorBaseFrame.apply(tessellator);
				bot_Frame.render(tessellator);
				part.colorBaseInsulators.apply(tessellator);
				bot_Insulators.render(tessellator);
				part.colorBaseSprings.apply(tessellator);
				bot_Springs.render(tessellator);

				if (isExtended)
				{
					tessellator.setTranslation(0.07, 0.125, 0.0);
					part.colorArmBottom.apply(tessellator);
					armBottom.render(tessellator);

					tessellator.setTranslation(-0.5764, 0.7747, 0.0);
					part.colorArmTop.apply(tessellator);
					switch (part.prop_type.get())
					{
						case HEAD1_ARM1:
							armTop_Single.render(tessellator);
							break;

						case HEAD1_ARM2:
							armTop_Double.render(tessellator);
							break;
					}

					tessellator.setTranslation(0.1836, 1.5642, 0.0);
					tessellator.setColorOpaque_F(0.2F, 0.2F, 0.2F);
					top_Contacts.render(tessellator);
					part.colorHeadFrame.apply(tessellator);
					top_Frame.render(tessellator);
					part.colorHeadGuides.apply(tessellator);
					top_Guides.render(tessellator);
				}
				else
				{
					tessellator.setTranslation(0.07, 0.125, 0.0);
					part.colorArmBottom.apply(tessellator);
					armBottom.render(tessellator, rotationMatrix_armBot);

					tessellator.setTranslation(-0.8428, 0.2072, 0.0);
					part.colorArmTop.apply(tessellator);
					switch (part.prop_type.get())
					{
						case HEAD1_ARM1:
							armTop_Single.render(tessellator, rotationMatrix_armTop);
							break;

						case HEAD1_ARM2:
							armTop_Double.render(tessellator, rotationMatrix_armTop);
							break;
					}

					tessellator.setTranslation(0.2469, 0.3234, 0.0);
					tessellator.setColorOpaque_F(0.2F, 0.2F, 0.2F);
					top_Contacts.render(tessellator);
					part.colorHeadFrame.apply(tessellator);
					top_Frame.render(tessellator);
					part.colorHeadGuides.apply(tessellator);
					top_Guides.render(tessellator);
				}

				tessellator.setTranslation(0, 0, 0);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}
}