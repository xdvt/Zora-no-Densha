package zoranodensha.vehicleParts.client.render.other;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.other.VehParLamp;



@SideOnly(Side.CLIENT)
public class VehParRenderer_Lamp implements IVehiclePartRenderer
{
	public static final ResourceLocation texture_antivignette = new ResourceLocation(ModData.ID, "textures/vehicleParts/lamp/antivignette.png");
	public static final ResourceLocation texture_gradient = new ResourceLocation(ModData.ID, "textures/vehicleParts/lamp/gradient.png");
	public static final ObjModelSpeedy model_flare = new ObjModelSpeedy(ModData.ID, "models/vehicleParts/lampFlare");

	/*
	 * Objects
	 */
	private static final ObjectList lights_flare;
	private static final ObjectList torch_body;
	private static final ObjectList torch_lens;
	static
	{
		ObjModelSpeedy model = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_torch");
		lights_flare = model_flare.makeGroup("Plane");
		torch_body = model.makeGroup("Body");
		torch_lens = model.makeGroup("Lens");
	}

	/** The parent part to render. */
	private final VehParLamp part;

	/*
	 * Vertex States and Rotation Matrices
	 */
	private TesselatorVertexState vertexState;
	private Mat3x3 rotationMatrixFlare = new Mat3x3();



	public VehParRenderer_Lamp(VehParLamp part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParLamp)
		{
			return new VehParRenderer_Lamp((VehParLamp)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 1);
	}

	/**
	 * Renders a cone with the point at the current translation.<br>
	 * <i>This method does not disable lighting or modify lightmap values.
	 * These operations must be done before this method is called.</i>
	 * 
	 * @param x - The X displacement of the light origin.
	 * @param y - The Y displacement of the light origin.
	 * @param z - The Z displacement of the light origin.
	 * @param length - The length between the point of the cone and the base.
	 * @param radius - The radius of the base of the cone.
	 */
	private void renderLightCone()
	{
		// FIXME @Leshuwa - When going very far away from a light cone in fancy mode, it may (partially) turn blue/ black.
		final Tessellator tessellator = Tessellator.instance;
		final ColorRGBA beamColor = part.color.get().clone().setAlpha(0.06F);
		final boolean fancyLights = ModCenter.cfg.rendering.enableFancyLights;
		final float length = part.type_lamp.beamLength.get();
		final float radius = part.type_lamp.beamRadius.get();

		float multiplier = 1.0F;
		int beams = 3;

		GL11.glPushAttrib(GL11.GL_COLOR_BUFFER_BIT);
		{
			if (fancyLights)
			{
				GL11.glBlendFunc(GL11.GL_DST_COLOR, GL11.GL_SRC_ALPHA);
				beams = 5;
			}

			GL11.glPushMatrix();
			GL11.glRotatef(5.0F, 0, 0, -1);
			{
				while (beams > 0)
				{
					tessellator.startDrawing(GL11.GL_TRIANGLE_FAN);
					{
						tessellator.setColorRGBA_F(beamColor.getR(), beamColor.getG(), beamColor.getB(), beamColor.getA());
						tessellator.setNormal(0, 1, 0);
						tessellator.addVertexWithUV(0.0, 0.0, 0.0, 0.0, 0.0);

						for (int angle = 0; angle <= 360; angle += 20)
						{
							//@formatter:off
							tessellator.addVertexWithUV(length,
							                            multiplier * Math.sin(ZnDMathHelper.RAD_MULTIPLIER * angle) * radius,
														multiplier * Math.cos(ZnDMathHelper.RAD_MULTIPLIER * angle) * radius * 1.25D,
														0.0F, 1.0F);
							//@formatter:on
						}
					}
					tessellator.draw();

					multiplier *= 1.05F;
					beams--;
				}
			}
			GL11.glPopMatrix();
		}
		GL11.glPopAttrib();
	}

	@Override
	public void renderPart(float partialTick)
	{
		/*
		 * Reset vertex state if necessary and prepare data.
		 */
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		boolean enableBeam = part.type_lamp.beamEnabled.get();
		boolean enableFlare = part.type_lamp.flareEnabled.get();
		boolean isTrainInWorld = part.getTrain() != null && part.getTrain().addedToChunk;

		/*
		 * Prepare rendering if either beams/ flares are enabled or if the parent train isn't existant/ spawned.
		 */
		if (!isTrainInWorld || enableBeam || enableFlare)
		{
			GL11.glPushAttrib(GL11.GL_ALPHA_TEST_FUNC);
			GL11.glPushAttrib(GL11.GL_ALPHA_TEST_REF);
			GL11.glPushAttrib(GL11.GL_LIGHTING);
			{
				GL11.glAlphaFunc(GL11.GL_GREATER, 0.01F);
				GL11.glDisable(GL11.GL_LIGHTING);

				RenderUtil.lightmapPush();
				RenderUtil.lightmapBright();
				{
					/* Render flare where necessary. */
					if (enableFlare || !isTrainInWorld)
					{
						Minecraft.getMinecraft().renderEngine.bindTexture(texture_antivignette);

						Tessellator tessellator = Tessellator.instance;
						tessellator.startDrawing(GL11.GL_TRIANGLES);
						{
							if (vertexState == null)
							{
								float flareTilt = part.type_lamp.flareTilt.get();
								if (flareTilt != 0.0F)
								{
									rotationMatrixFlare.reset();
									rotationMatrixFlare.rotateZ(flareTilt);

									part.color.apply(tessellator);
									lights_flare.render(tessellator, rotationMatrixFlare);
								}
								else
								{
									part.color.apply(tessellator);
									lights_flare.render(tessellator);
								}

								vertexState = VertexStateCreator_Tris.getVertexState();
							}
							else
							{
								tessellator.setVertexState(vertexState);
							}
						}
						tessellator.draw();
					}

					/* Render beam if desired. */
					if (enableBeam)
					{
						Minecraft.getMinecraft().renderEngine.bindTexture(texture_gradient);
						renderLightCone();
					}
				}
				RenderUtil.lightmapPop();
			}
			GL11.glPopAttrib();
			GL11.glPopAttrib();
			GL11.glPopAttrib();
		}
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		GL11.glPushMatrix();
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

			GL11.glColor4f(0.2F, 0.2F, 0.2F, 1.0F);
			torch_body.render();

			RenderUtil.lightmapPush();
			RenderUtil.lightmapBright();
			{
				GL11.glPushAttrib(GL11.GL_LIGHTING);
				{
					GL11.glDisable(GL11.GL_LIGHTING);
					GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
					torch_lens.render();
				}
				GL11.glPopAttrib();
			}
			RenderUtil.lightmapPop();
		}
		GL11.glPopMatrix();
	}
}