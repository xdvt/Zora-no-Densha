package zoranodensha.vehicleParts.client.render.coupler;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerScrew;



@SideOnly(Side.CLIENT)
public class VehParRenderer_CouplerScrew extends AVehParRendererBase_Coupler
{
	private static final ObjectList chain, chain_hanging, hook;
	static
	{
		ObjModelSpeedy obj = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_couplerScrew");
		chain = obj.makeGroup("chain_connected");
		chain_hanging = obj.makeGroup("hook", "chain_hanging");
		hook = obj.makeGroup("hook");
	}

	/** The part to render. */
	final VehParCouplerScrew part;
	private TesselatorVertexState vertexState;
	private boolean renderChain;



	public VehParRenderer_CouplerScrew(VehParCouplerScrew part)
	{
		super(part);
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParCouplerScrew)
		{
			return new VehParRenderer_CouplerScrew((VehParCouplerScrew)copyPart);
		}
		return null;
	}

	@Override
	protected boolean applyAlignedRotation()
	{
		if (matrixData != null)
		{
			/*
			 * Calculate rotation heading from this coupler to the other one.
			 * Also calculate offset along local X (post-rotation).
			 */
			float yaw = 0.0F;
			float pitch = 0.0F;
			double offset = 0.0D;
			{
				Vec3 pos0 = PartHelper.getPosition(coupler);
				Vec3 pos1 = PartHelper.getPosition(matrixData.couplerOther.getParent());
				Vec3 view = PartHelper.getViewVec(matrixData.couplerOther.getParent(), false);
				pos1 = pos1.addVector(view.xCoord * 0.105, view.yCoord * 0.105, view.zCoord * 0.105);

				double dX = pos1.xCoord - pos0.xCoord;
				double dY = pos1.yCoord - pos0.yCoord;
				double dZ = pos1.zCoord - pos0.zCoord;
				double d0 = (dX * dX) + (dZ * dZ);

				/* Calculate rotation values. */
				if (d0 > 0.001D)
				{
					yaw = (float)(-Math.atan2(dZ, dX) * VehicleHelper.DEG_FACTOR);

					if (Math.abs(dY) > 0.001D)
					{
						pitch = (float)(Math.atan(dY / Math.sqrt(d0)) * VehicleHelper.DEG_FACTOR);
					}
				}

				/* Calculate offset along local X. */
				offset = Math.sqrt(d0 + dY * dY);
				offset /= 0.375;
			}

			GL11.glRotatef(yaw, 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(pitch, 0.0F, 0.0F, 1.0F);
			GL11.glScaled(offset, 1, 1);

			return true;
		}
		return false;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				tessellator.setColorOpaque_F(0.15F, 0.13F, 0.13F);
				if (renderChain)
				{
					hook.render(tessellator);
				}
				else
				{
					chain_hanging.render(tessellator);
				}
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef(0.0F, 0.25F, 0.0F);
		GL11.glScalef(2.7F, 2.7F, 2.7F);
		renderPart(1.0F);
		GL11.glPopMatrix();
	}

	@Override
	public boolean renderPart_Pre(float partialTick)
	{
		if (super.renderPart_Pre(partialTick))
		{
			/* Render dynamic chain first. */
			renderChain = (matrixData != null) && (part.getOffset()[0] >= matrixData.couplerOther.getParent().getOffset()[0]);
			if (renderChain)
			{
				Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
				GL11.glColor3f(0.15F, 0.13F, 0.13F);
				chain.render();
			}

			renderPart_Post();
			GL11.glPopMatrix();
			GL11.glPushMatrix();

			/* Then proceed with standard rendering routines for the base. */
			return false;
		}

		renderChain = false;
		return false;
	}
}
