package zoranodensha.vehicleParts.client.render.coupler;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerSfbg;



@SideOnly(Side.CLIENT)
public class VehParRenderer_CouplerSfbg extends AVehParRendererBase_Coupler
{
	// @formatter:off
	private static final ObjectList
	base,
	base_grey,

	ext_side_coverL,
	ext_side_coverL_tris,
	ext_side_coverR,
	ext_side_coverR_tris,
	ext_side_electrics,
	ext_side_wires,

	ext_top_cover,
	ext_top_cover_tris,
	ext_top_electrics,
	ext_top_wires;
	// @formatter:on

	static
	{
		ObjModelSpeedy obj = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_couplerSfbg");

		base = obj.makeGroup("base");
		base_grey = obj.makeGroup("base_grey");

		ext_side_coverL = obj.makeGroup("ext_side_coverL");
		ext_side_coverL_tris = obj.makeGroup("ext_side_coverL_tris");
		ext_side_coverR = obj.makeGroup("ext_side_coverR");
		ext_side_coverR_tris = obj.makeGroup("ext_side_coverR_tris");
		ext_side_electrics = obj.makeGroup("ext_side_electrics");
		ext_side_wires = obj.makeGroup("ext_side_wires");

		ext_top_cover = obj.makeGroup("ext_top_cover");
		ext_top_cover_tris = obj.makeGroup("ext_top_cover_tris");
		ext_top_electrics = obj.makeGroup("ext_top_electrics");
		ext_top_wires = obj.makeGroup("ext_top_wires");
	}

	/** The part to render. */
	final VehParCouplerSfbg part;

	private TesselatorVertexState vertexState;
	private final Mat3x3 rotationMatrixPosY = new Mat3x3().rotateY(90);
	private final Mat3x3 rotationMatrixNegY = new Mat3x3().rotateY(-90);
	private final Mat3x3 rotationMatrixNegZ = new Mat3x3().rotateZ(-90);



	public VehParRenderer_CouplerSfbg(VehParCouplerSfbg part)
	{
		super(part);
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParCouplerSfbg)
		{
			return new VehParRenderer_CouplerSfbg((VehParCouplerSfbg)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		/* Render base with applicable extension parts */
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.color.apply(tessellator);
				base.render(tessellator);

				tessellator.setColorOpaque_F(0.24F, 0.24F, 0.24F);
				base_grey.render(tessellator);

				switch (part.prop_type.get())
				{
					default:
						break;

					case 1:
						part.color_extension.apply(tessellator);
						ext_side_wires.render(tessellator);

						if (part.type_coupling.getIsCoupled())
						{
							tessellator.setTranslation(0.01D, 0.0D, 0.0D);
							ext_side_electrics.render(tessellator);

							tessellator.setTranslation(0.63D, 0.01D, -0.22D);
							ext_side_coverL.render(tessellator, rotationMatrixPosY);

							tessellator.setColorOpaque_F(0.45F, 0.45F, 0.1F);
							ext_side_coverL_tris.render(tessellator, rotationMatrixPosY);

							part.color_extension.apply(tessellator);

							tessellator.setTranslation(0.63D, 0.01D, 0.22D);
							ext_side_coverR.render(tessellator, rotationMatrixNegY);

							tessellator.setColorOpaque_F(0.45F, 0.45F, 0.1F);
							ext_side_coverR_tris.render(tessellator, rotationMatrixNegY);
						}
						else
						{
							ext_side_electrics.render(tessellator);

							tessellator.setTranslation(0.63D, 0.01D, -0.15D);
							ext_side_coverL.render(tessellator);

							tessellator.setColorOpaque_F(0.45F, 0.45F, 0.1F);
							ext_side_coverL_tris.render(tessellator);

							part.color_extension.apply(tessellator);

							tessellator.setTranslation(0.63D, 0.01D, 0.15D);
							ext_side_coverR.render(tessellator);

							tessellator.setColorOpaque_F(0.45F, 0.45F, 0.1F);
							ext_side_coverR_tris.render(tessellator);
						}
						break;

					case 2:
						part.color_extension.apply(tessellator);
						ext_top_wires.render(tessellator);

						if (part.type_coupling.getIsCoupled())
						{
							tessellator.setTranslation(0.01D, 0.0D, 0.0D);
							ext_top_electrics.render(tessellator);

							tessellator.setTranslation(0.57D, 0.1D, 0.0D);
							ext_top_cover.render(tessellator, rotationMatrixNegZ);

							tessellator.setColorOpaque_F(0.45F, 0.45F, 0.1F);
							ext_top_cover_tris.render(tessellator, rotationMatrixNegZ);
						}
						else
						{
							ext_top_electrics.render(tessellator);

							tessellator.setTranslation(0.57D, 0.1D, 0.0D);
							ext_top_cover.render(tessellator);

							tessellator.setColorOpaque_F(0.45F, 0.45F, 0.1F);
							ext_top_cover_tris.render(tessellator);
						}
						break;
				}

				tessellator.setTranslation(0, 0, 0);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef(-0.92F, 0.0F, 0.0F);
		GL11.glScalef(2.5F, 2.5F, 2.5F);
		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
