package zoranodensha.vehicleParts.client.render.bogie;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_SWF;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisY25;



@SideOnly(Side.CLIENT)
public class VehParRenderer_AxisY25 extends AVehParRendererBase_Axis implements IVehiclePartRenderer
{
	/*
	 * Objects
	 */
	private static final ObjectList bogie = AVehParRendererBase_SWF.model.makeGroup("Bogie_Base");
	private static final ObjectList wheel = AVehParRendererBase_SWF.model.makeGroup("Bogie_Wheel");

	/*
	 * Part
	 */
	private final VehParAxisY25 part;

	/*
	 * Vertex States
	 */
	private TesselatorVertexState vertexStateBogie;
	private TesselatorVertexState vertexStateWheel;



	public VehParRenderer_AxisY25(VehParAxisY25 part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParAxisY25)
		{
			return new VehParRenderer_AxisY25((VehParAxisY25)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexStateBogie = null;
			vertexStateWheel = null;
			part.updateVertexState = false;
		}

		if (ModCenter.cfg.rendering.detailVehicles > 0)
		{
			updateWheelRotation(part, part.lastDistMoved, part.distMoved, partialTick);
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		/*
		 * Bogie
		 */
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexStateBogie == null)
			{
				part.color.apply(tessellator);
				bogie.render(tessellator);

				vertexStateBogie = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexStateBogie);
			}
		}
		tessellator.draw();

		/*
		 * Wheels
		 */
		for (int i = -1; i <= 1; i += 2)
		{
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(i * 0.54F, 0.0F, 0.0F);
				GL11.glRotatef(wheelRotation, 0.0F, 0.0F, 1.0F);

				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexStateWheel == null)
					{
						part.colorWheels.apply(tessellator);
						wheel.render(tessellator);

						vertexStateWheel = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexStateWheel);
					}
				}
				tessellator.draw();

			}
			GL11.glPopMatrix();
		}
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.65F, 0.65F, 0.65F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}

}
