package zoranodensha.vehicleParts.client.render.bogie;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.common.parts.bogie.AVehParBogieImpl;



@SideOnly(Side.CLIENT)
public abstract class AVehParRendererBase_Axis
{
	protected float wheelRotation = 0.0F;



	protected void updateWheelRotation(AVehParBogieImpl part, double lastDistMoved, double distMoved, float partialTick)
	{
		/* Don't update if game is paused. */
		if (Minecraft.getMinecraft().isGamePaused())
		{
			return;
		}

		final float velocityScaling = ModCenter.cfg.trains.velocityScale;
		distMoved *= velocityScaling;
		lastDistMoved *= velocityScaling;

		float dist = (float)(lastDistMoved + (distMoved - lastDistMoved) * partialTick);
		if (Math.abs(dist) > 0.0D)
		{
			Vec3 vec = Vec3.createVectorHelper(1, 0, 0);
			{
				vec.rotateAroundZ(part.type_bogie.getRotationPitch() * VehicleHelper.RAD_FACTOR);
				vec.rotateAroundY(part.type_bogie.getRotationYaw() * VehicleHelper.RAD_FACTOR);
				vec.rotateAroundX(part.type_bogie.getRotationRoll() * VehicleHelper.RAD_FACTOR);
			}

			double angle = VehicleHelper.getAngle(vec, part.type_bogie.getDirection());
			if (Math.abs(angle) < 90.0D)
			{
				dist = -dist;
			}

			wheelRotation += dist * VehicleHelper.DEG_FACTOR;
			wheelRotation %= 360.0F;
		}
	}
}
