package zoranodensha.vehicleParts.client.render.bogie;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_BR101;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisBR101;



@SideOnly(Side.CLIENT)
public class VehParRenderer_AxisBR101 extends AVehParRendererBase_Axis implements IVehiclePartRenderer
{
	public static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/vehicleParts/axisBR101.png");
	public static final ObjectList bogie = AVehParRendererBase_BR101.model.makeGroup("Bogie_Base");
	public static final ObjectList wheel = AVehParRendererBase_BR101.model.makeGroup("Bogie_Wheel");

	/** The part to render. */
	final VehParAxisBR101 part;
	private TesselatorVertexState vertexStateBogie;
	private TesselatorVertexState vertexStateWheel;



	public VehParRenderer_AxisBR101(VehParAxisBR101 part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParAxisBR101)
		{
			return new VehParRenderer_AxisBR101((VehParAxisBR101)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		/* Reset vertex states if required, and update wheel rotation if allowed. */
		if (part.updateVertexState)
		{
			vertexStateBogie = null;
			vertexStateWheel = null;
			part.updateVertexState = false;
		}

		if (ModCenter.cfg.rendering.detailVehicles > 0)
		{
			updateWheelRotation(part, part.lastDistMoved, part.distMoved, partialTick);
		}

		/* Render bogie and wheels. */
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		Tessellator tessellator = Tessellator.instance;

		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexStateBogie == null)
			{
				tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
				bogie.render(tessellator);
				vertexStateBogie = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexStateBogie);
			}
		}
		tessellator.draw();

		for (int i = -1; i < 2; i += 2)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(i * 0.753F, -0.094F, 0.0F);
			GL11.glRotatef(wheelRotation, 0.0F, 0.0F, 1.0F);
			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexStateWheel == null)
				{
					tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
					wheel.render(tessellator);
					vertexStateWheel = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexStateWheel);
				}
			}
			tessellator.draw();
			GL11.glPopMatrix();
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.65F, 0.65F, 0.65F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
