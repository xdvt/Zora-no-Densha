package zoranodensha.vehicleParts.client.render.bogie;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer.IVehiclePartSpecialRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_FLIRT;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisFLIRT;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisFLIRTTrailer;


@SideOnly(Side.CLIENT)
public class VehParRenderer_AxisFLIRTTrailer extends AVehParRendererBase_Axis implements IVehiclePartSpecialRenderer
{
	private static final ObjectList bogie = AVehParRendererBase_FLIRT.model.makeGroup("Bogie_Base", "Bogie_Bogie");
	private static final ObjectList jacob = AVehParRendererBase_FLIRT.model.makeGroup("Bogie_Base", "Bogie_Jacob");
	private static final ObjectList wheel = AVehParRendererBase_FLIRT.model.makeGroup("Bogie_Wheel");

	// @formatter:off
	private static final float[][] vertices = new float[][] {
		{ 0.39F, 0.615F }, { 0.57F, 0.975F }, { 0.65F, 0.895F }, { 0.46F, 0.545F },
		{ 0.46F, -.395F }, { 0.65F, -.385F }, { 0.26F, -.815F }, { 0.26F, -.545F },
		{ -.26F, -.545F }, { -.26F, -.815F }, { -.65F, -.385F }, { -.46F, -.395F },
		{ -.46F, 0.545F }, { -.65F, 0.895F }, { -.57F, 0.975F }, { -.39F, 0.615F }
	};
	// @formatter:on

	/** The part to render. */
	final VehParAxisFLIRTTrailer part;
	private TesselatorVertexState vertexStateBogie;
	private TesselatorVertexState vertexStateWheel;



	public VehParRenderer_AxisFLIRTTrailer(VehParAxisFLIRTTrailer part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParAxisFLIRT)
		{
			return new VehParRenderer_AxisFLIRTTrailer((VehParAxisFLIRTTrailer) copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	/**
	 * Helper method to render an elastic intercirculation.
	 *
	 * @param rot - The bogie's world rotation.
	 */
	private void renderIntercirculation()
	{
		/* Prepare data. */
		float yawA, yawB;
		float pitchA, pitchB;
		float size = part.intercirculationSize.get() * 0.5F;

		if (part.sectionA == null || part.sectionB == null)
		{
			yawA = yawB = 0.0F;
			pitchA = pitchB = 0.0F;
		}
		else
		{
			double dX_a = part.type_bogie.getPosX() - part.sectionA.getPosX();
			double dX_b = part.type_bogie.getPosX() - part.sectionB.getPosX();
			double dY_a = part.type_bogie.getPosY() - part.sectionA.getPosY();
			double dY_b = part.type_bogie.getPosY() - part.sectionB.getPosY();
			double dZ_a = part.type_bogie.getPosZ() - part.sectionA.getPosZ();
			double dZ_b = part.type_bogie.getPosZ() - part.sectionB.getPosZ();

			yawA = (float)Math.atan2(dX_a, dZ_a) * VehicleHelper.DEG_FACTOR;
			yawB = (float)Math.atan2(dX_b, dZ_b) * VehicleHelper.DEG_FACTOR;
			yawA += 90.0F;
			yawB += 90.0F;

			pitchA = (float)Math.atan(dY_a / Math.sqrt(dX_a * dX_a + dZ_a * dZ_a)) * VehicleHelper.DEG_FACTOR;
			pitchB = (float)Math.atan(dY_b / Math.sqrt(dX_b * dX_b + dZ_b * dZ_b)) * VehicleHelper.DEG_FACTOR;
		}

		if (part.getTrain() == null || !part.getTrain().addedToChunk)
		{
			yawA = 0;
			yawB = 180;
		}

		/* Render in- and outside. */
		float offY = part.intercirculationY.get();
		renderIntercirculation_Inside(size, offY, yawA, yawB, pitchA, pitchB);
		renderIntercirculation_Outside(size, offY, yawA, yawB, pitchA, pitchB);

		/* Render ends. */
		GL11.glPushMatrix();
		GL11.glRotatef(yawA, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-pitchA, 0.0F, 0.0F, 1.0F);
		GL11.glTranslatef(size, offY, 0.0F);
		renderIntercirculation_Ends();
		GL11.glPopMatrix();

		GL11.glPushMatrix();
		GL11.glRotatef(yawB, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-pitchB, 0.0F, 0.0F, 1.0F);
		GL11.glTranslatef(size, offY, 0.0F);
		renderIntercirculation_Ends();
		GL11.glPopMatrix();
	}

	@Override
	public void renderPart(float partialTick)
	{
		/* Reset vertex states if required, and update wheel rotation if allowed. */
		if (part.updateVertexState)
		{
			vertexStateBogie = null;
			vertexStateWheel = null;
			part.updateVertexState = false;
		}

		if (ModCenter.cfg.rendering.detailVehicles > 0)
		{
			updateWheelRotation(part, part.lastDistMoved, part.distMoved, partialTick);
		}

		/*
		 * Render part depending on whether its parent train exists or not.
		 */
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

		Train train = part.getTrain();
		if (train != null && train.addedToChunk)
		{
			/*
			 * Render bogie with dynamic intercirculation.
			 */
			
			GL11.glPushMatrix();
			{
				/* Rotation */
				GL11.glRotatef(TrainRenderer.interpolate(part.type_bogie.getRotationYaw(), part.type_bogie.getPrevRotationYaw()), 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(TrainRenderer.interpolate(part.type_bogie.getRotationPitch(), part.type_bogie.getPrevRotationPitch()), 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(TrainRenderer.interpolate(part.type_bogie.getRotationRoll(), part.type_bogie.getPrevRotationRoll()), 1.0F, 0.0F, 0.0F);
				
				/* Translation */
				float[] scale = part.getScale();
				GL11.glScalef(scale[0], scale[1], scale[2]);

				if (TrainRenderer.getInvertCullMode(scale[0], scale[1], scale[2]))
				{
					GL11.glCullFace(GL11.GL_FRONT);
				}
				
				/* Render */
				renderPart_Bogie();
			}
			GL11.glPopMatrix();

			if (part.hasIntercirculation.get())
			{
				AVehParRendererBase_FLIRT.applyGrey();
				renderIntercirculation();
			}
		}
		else
		{
			/*
			 * Render default part with default intercirculation, if applicable.
			 */
			
			/* Rotation */
			float[] rot = part.getRotation();
			GL11.glRotatef(rot[1], 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(rot[2], 0.0F, 0.0F, 1.0F);
			GL11.glRotatef(rot[0], 1.0F, 0.0F, 0.0F);

			/* Translation */
			float[] scale = part.getScale();
			GL11.glScalef(scale[0], scale[1], scale[2]);

			if (TrainRenderer.getInvertCullMode(scale[0], scale[1], scale[2]))
			{
				GL11.glCullFace(GL11.GL_FRONT);
			}
			
			/* Render */
			renderPart_Bogie();

			if (part.hasIntercirculation.get())
			{
				AVehParRendererBase_FLIRT.applyGrey();
				renderIntercirculation();
			}
		}
	}

	/**
	 * Render's the bogie base as well as the wheels from vertex states.
	 */
	private void renderPart_Bogie()
	{
		/* Render bogie and wheels. */
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexStateBogie == null)
			{
				part.color.apply(tessellator);
				(part.isJacobs.get() ? jacob : bogie).render(tessellator);
				vertexStateBogie = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexStateBogie);
			}
		}
		tessellator.draw();

		for (int i = -1; i < 2; i += 2)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(i * 0.851F, -0.156F, 0.0F);
			GL11.glRotatef(wheelRotation, 0.0F, 0.0F, 1.0F);
			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexStateWheel == null)
				{
					ColorRGBA.multiplierPush(1.2F, 1.2F, 1.2F, 1.0F);
					part.color.apply(tessellator);
					ColorRGBA.multiplierPop();

					wheel.render(tessellator);
					vertexStateWheel = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexStateWheel);
				}
			}
			tessellator.draw();
			GL11.glPopMatrix();
		}
	}

	@Override
	public void renderPart_Post()
	{
		;
	}

	@Override
	public boolean renderPart_Pre(float partialTick)
	{
		if (part.getTrain() == null || !part.getTrain().addedToChunk)
		{
			float[] offset = part.getOffset();
			GL11.glTranslatef(offset[0], offset[1], offset[2]);
		}
		return true;
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.65F, 0.65F, 0.65F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}

	private static void renderIntercirculation_Ends()
	{
		final Tessellator t = Tessellator.instance;
		for (int face = 0, vertex; face < 8; ++face)
		{
			t.startDrawingQuads();
			if (face % 2 == 0)
			{
				for (int i = 0; i < 4; ++i)
				{
					vertex = face * 2 + i;
					if (vertex >= vertices.length)
					{
						vertex -= vertices.length;
					}

					t.addVertexWithUV(0.0F, vertices[vertex][1], vertices[vertex][0], vertices[vertex][0] * 0.5F, vertices[vertex][1] * 0.5F);
				}
			}
			else
			{
				for (int i = 3; i >= 0; --i)
				{
					vertex = face * 2 + i;
					if (vertex >= vertices.length)
					{
						vertex -= vertices.length;
					}

					t.addVertexWithUV(0.0F, vertices[vertex][1], vertices[vertex][0], vertices[vertex][0] * 0.5F, vertices[vertex][1] * 0.5F);
				}
			}
			t.draw();
		}
	}

	private void renderIntercirculation_Inside(float size, float offY, float yawA, float yawB, float pitchA, float pitchB)
	{
		Tessellator t = Tessellator.instance;

		for (int face = 0; face < 8; ++face)
		{
			/* Determine vertex indices. */
			int v1;
			int v2;

			if (face % 2 == 0)
			{
				v1 = face * 2;
				v2 = v1 + 3;
			}
			else
			{
				v1 = face * 2 + 1;
				v2 = v1 + 1;
			}

			if (v2 >= vertices.length)
			{
				v2 -= vertices.length;
			}

			/* Determine rotation. */
			float yaw1 = yawA * VehicleHelper.RAD_FACTOR;
			float yaw0 = VehicleHelper.normalise(part.type_bogie.getRotationYaw(), yawA) * VehicleHelper.RAD_FACTOR;
			float pitch1 = pitchA * VehicleHelper.RAD_FACTOR;
			float pitch0 = VehicleHelper.normalise(part.type_bogie.getRotationPitch(), pitchA) * VehicleHelper.RAD_FACTOR;

			for (int i = 0; i < 2; ++i)
			{
				/* Calculate vertex positions. */
				Vec3 vec3 = Vec3.createVectorHelper(0.0D, vertices[v1][1] + offY, vertices[v1][0]);
				Vec3 vec2 = Vec3.createVectorHelper(size, vertices[v1][1] + offY, vertices[v1][0]);
				Vec3 vec1 = Vec3.createVectorHelper(size, vertices[v2][1] + offY, vertices[v2][0]);
				Vec3 vec0 = Vec3.createVectorHelper(0.0D, vertices[v2][1] + offY, vertices[v2][0]);

				vec0.rotateAroundZ(-pitch0);
				vec1.rotateAroundZ(pitch1);
				vec2.rotateAroundZ(pitch1);
				vec3.rotateAroundZ(-pitch0);

				vec3.rotateAroundY(yaw0);
				vec2.rotateAroundY(yaw1);
				vec1.rotateAroundY(yaw1);
				vec0.rotateAroundY(yaw0);

				/* Render faces. */
				t.startDrawingQuads();
				t.addVertexWithUV(vec3.xCoord, vec3.yCoord, vec3.zCoord, vertices[v1][0] * 0.5D, vertices[v1][1] * 0.5D);
				t.addVertexWithUV(vec2.xCoord, vec2.yCoord, vec2.zCoord, vertices[v1][0] * 0.5D, vertices[v1][1] * 0.5D);
				t.addVertexWithUV(vec1.xCoord, vec1.yCoord, vec1.zCoord, vertices[v2][0] * 0.5D, vertices[v2][1] * 0.5D);
				t.addVertexWithUV(vec0.xCoord, vec0.yCoord, vec0.zCoord, vertices[v2][0] * 0.5D, vertices[v2][1] * 0.5D);
				t.draw();

				/* Determine rotation. */
				yaw1 = yawB * VehicleHelper.RAD_FACTOR;
				yaw0 = VehicleHelper.normalise(part.type_bogie.getRotationYaw(), yawB) * VehicleHelper.RAD_FACTOR;
				pitch1 = pitchB * VehicleHelper.RAD_FACTOR;
				pitch0 = VehicleHelper.normalise(-part.type_bogie.getRotationPitch(), pitchB) * VehicleHelper.RAD_FACTOR;
			}
		}
	}

	private void renderIntercirculation_Outside(float size, float offY, float yawA, float yawB, float pitchA, float pitchB)
	{
		Tessellator t = Tessellator.instance;

		for (int face = 0; face < 8; ++face)
		{
			/* Determine vertex indices. */
			int v1;
			int v2;

			if (face % 2 == 0)
			{
				v1 = face * 2 + 1;
				v2 = v1 + 1;
			}
			else
			{
				v1 = face * 2;
				v2 = v1 + 3;
			}

			if (v2 >= vertices.length)
			{
				v2 -= vertices.length;
			}

			/* Determine rotation. */
			float yaw1 = yawA * VehicleHelper.RAD_FACTOR;
			float yaw0 = VehicleHelper.normalise(part.type_bogie.getRotationYaw(), yawA) * VehicleHelper.RAD_FACTOR;
			float pitch1 = pitchA * VehicleHelper.RAD_FACTOR;
			float pitch0 = VehicleHelper.normalise(part.type_bogie.getRotationPitch(), pitchA) * VehicleHelper.RAD_FACTOR;

			for (int i = 0; i < 2; ++i)
			{
				/* Calculate vertex positions. */
				Vec3 vec0 = Vec3.createVectorHelper(0.0D, vertices[v2][1] + offY, vertices[v2][0]);
				Vec3 vec1 = Vec3.createVectorHelper(size, vertices[v2][1] + offY, vertices[v2][0]);
				Vec3 vec2 = Vec3.createVectorHelper(size, vertices[v1][1] + offY, vertices[v1][0]);
				Vec3 vec3 = Vec3.createVectorHelper(0.0D, vertices[v1][1] + offY, vertices[v1][0]);

				vec0.rotateAroundZ(-pitch0);
				vec1.rotateAroundZ(pitch1);
				vec2.rotateAroundZ(pitch1);
				vec3.rotateAroundZ(-pitch0);

				vec0.rotateAroundY(yaw0);
				vec1.rotateAroundY(yaw1);
				vec2.rotateAroundY(yaw1);
				vec3.rotateAroundY(yaw0);

				/* Render faces. */
				t.startDrawingQuads();
				t.addVertexWithUV(vec0.xCoord, vec0.yCoord, vec0.zCoord, vertices[v2][0] * 0.5D, vertices[v2][1] * 0.5D);
				t.addVertexWithUV(vec1.xCoord, vec1.yCoord, vec1.zCoord, vertices[v2][0] * 0.5D, vertices[v2][1] * 0.5D);
				t.addVertexWithUV(vec2.xCoord, vec2.yCoord, vec2.zCoord, vertices[v1][0] * 0.5D, vertices[v1][1] * 0.5D);
				t.addVertexWithUV(vec3.xCoord, vec3.yCoord, vec3.zCoord, vertices[v1][0] * 0.5D, vertices[v1][1] * 0.5D);
				t.draw();

				/* Determine rotation. */
				yaw1 = yawB * VehicleHelper.RAD_FACTOR;
				yaw0 = VehicleHelper.normalise(part.type_bogie.getRotationYaw(), yawB) * VehicleHelper.RAD_FACTOR;
				pitch1 = pitchB * VehicleHelper.RAD_FACTOR;
				pitch0 = VehicleHelper.normalise(-part.type_bogie.getRotationPitch(), pitchB) * VehicleHelper.RAD_FACTOR;
			}
		}
	}
}
