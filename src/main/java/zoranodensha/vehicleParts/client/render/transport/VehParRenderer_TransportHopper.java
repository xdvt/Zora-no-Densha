package zoranodensha.vehicleParts.client.render.transport;

import java.util.Random;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_ModFreight;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportHopper;



@SideOnly(Side.CLIENT)
public class VehParRenderer_TransportHopper extends AVehParRendererBase_ModFreight implements IVehiclePartRenderer
{
	private static final ObjectList extension = model.makeGroup("Extension_Hopper");
	private static final Random ran = new Random();

	/** The part to render. */
	final VehParTransportHopper part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_TransportHopper(VehParTransportHopper part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParTransportHopper)
		{
			return new VehParRenderer_TransportHopper((VehParTransportHopper)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		TextureManager renderEngine = Minecraft.getMinecraft().renderEngine;
		renderEngine.bindTexture(RenderUtil.texture_white);

		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.color.apply(tessellator);
				extension.render(tessellator);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();

		/* If gag part or render setting is turned off, return. */
		if (!part.getIsFinished() || ModCenter.cfg.rendering.detailVehicles <= 0)
		{
			return;
		}

		/* Also don't render if the part either has no vehicle, or the vehicle wasn't added to any chunk yet. */
		{
			Train parent = part.getTrain();
			if (parent == null || !parent.addedToChunk)
			{
				return;
			}
		}

		renderEngine.bindTexture(TextureMap.locationBlocksTexture);

		float fill = part.type_transportItem.getFillPercentage();
		float[] arr = part.getScale();

		GL11.glPushMatrix();
		GL11.glScalef(1.0F / Math.abs(arr[0]), 1.0F / Math.abs(arr[1]), 1.0F / Math.abs(arr[2]));
		GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
		GL11.glTranslatef(0.0F, 0.0F, (fill - 0.575F) * Math.abs(arr[1]));
		{
			int invSize = part.type_transportItem.getSizeInventory();
			int length = MathHelper.floor_float(Math.abs(arr[0]) * (49.0F + (fill * 6.0F)));
			int width = MathHelper.floor_float(Math.abs(arr[2]) * 5.0F);
			int max = length * width;
			int cnt = 0;
			int stackSize;
			ItemStack itemStack;

			GL11.glPushAttrib(GL12.GL_RESCALE_NORMAL);
			{
				ran.setSeed(max);
				inner: for (int slotID = 0; slotID < invSize; ++slotID)
				{
					itemStack = part.type_transportItem.getStackInSlot(slotID);
					if (itemStack != null)
					{
						for (stackSize = itemStack.stackSize - 1; stackSize >= 0; --stackSize)
						{
							++cnt;
							GL11.glPushMatrix();
							applyOffset(cnt, length, width, arr[1]);
							RenderUtil.renderItemStack(itemStack);
							GL11.glPopMatrix();

							if (cnt > max)
							{
								break inner;
							}
						}
					}
				}
			}
			GL11.glPopAttrib();
		}
		GL11.glPopMatrix();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glScalef(0.4F, 0.4F, 0.4F);

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}

	private static void applyOffset(int cnt, int length, int width, float scaleY)
	{
		if (cnt <= 1)
		{
			return;
		}

		float offX = 0.0F;
		float offY = 0.0F;
		float offZ = 0.0F;
		--cnt;

		/* Y-offset */
		offY = (10.0F / (1 + ran.nextInt(10))) * 0.00625F;
		if (ran.nextBoolean())
		{
			offY = -offY;
		}
		offY -= cnt * 0.001F;

		/* Z-offset */
		int z = cnt % width;
		final float f = 0.175F;
		if (z > 0)
		{
			if (z % 2 == 0)
			{
				offZ = (z / 2) * f;
				offZ = -offZ;
			}
			else
			{
				offZ = ((z + 1) / 2) * f;
			}
		}

		/* X-offset */
		int x = (cnt / width) % length;
		if (x > 0)
		{
			if (x % 2 == 0)
			{
				offX = (x / 2) * f;
				offX = -offX;
			}
			else
			{
				offX = ((x + 1) / 2) * f;
			}
		}

		/* Order of axes is messed up due to rotation. */
		GL11.glTranslatef(offZ, offX - 0.003125F, offY * scaleY);
		GL11.glRotatef(ran.nextFloat() * 360.0F, 0.0F, 0.0F, 1.0F);

		if (ModCenter.cfg.rendering.detailVehicles > 1)
		{
			GL11.glRotatef(ran.nextFloat() * 10.0F, 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(ran.nextFloat() * 10.0F, 1.0F, 0.0F, 0.0F);
		}
	}
}