package zoranodensha.vehicleParts.client.render.transport;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_ModFreight;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportWood;



@SideOnly(Side.CLIENT)
public class VehParRenderer_TransportWood extends AVehParRendererBase_ModFreight implements IVehiclePartRenderer
{
	private static final ObjectList extension = model.makeGroup("Extension_Wood");

	/** An array used to calculate the offset of each level's wood log. */
	private static final int[][] offset = new int[][] { { 0, 0 }, { 0, 1 }, { 0, -1 }, { 1, 0 }, { 1, 1 }, { 1, -1 }, { -1, 0 }, { -1, 1 }, { -1, -1 }, };

	/** The part to render. */
	final VehParTransportWood part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_TransportWood(VehParTransportWood part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParTransportWood)
		{
			return new VehParRenderer_TransportWood((VehParTransportWood)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		TextureManager renderEngine = Minecraft.getMinecraft().renderEngine;
		renderEngine.bindTexture(RenderUtil.texture_white);

		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.color.apply(tessellator);
				extension.render(tessellator);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();

		/* If gag part or render setting is turned off, return. */
		if (!part.getIsFinished() || ModCenter.cfg.rendering.detailVehicles <= 0)
		{
			return;
		}

		/* Also don't render if the part either has no vehicle, or the vehicle wasn't added to any chunk yet. */
		{
			Train parent = part.getTrain();
			if (parent == null || !parent.addedToChunk)
			{
				return;
			}
		}

		/*
		 * Max. Values (per log):
		 * 3.6 x
		 * y = z
		 * 0.5 z
		 */
		renderEngine.bindTexture(TextureMap.locationBlocksTexture);

		int cnt = 0;
		int invSize = part.type_transportItem.getSizeInventory();
		float[] arr = part.getScale();
		float xOff = 3.6F * Math.abs(arr[0]);
		float yOff = (-0.54F * Math.abs(arr[1])) + 0.34F;
		float zOff = 0.5F * Math.abs(arr[2]);
		ItemStack itemStack;
		Block block;

		GL11.glPushMatrix();
		GL11.glScalef(1.0F / Math.abs(arr[0]), 1.0F / Math.abs(arr[1]), 1.0F / Math.abs(arr[2]));
		{
			for (int slotID = 0; slotID < invSize; ++slotID)
			{
				itemStack = part.type_transportItem.getStackInSlot(slotID);
				if (itemStack != null && itemStack.getItem() instanceof ItemBlock)
				{
					block = Block.getBlockFromItem(itemStack.getItem());
					if (block.getRenderType() != -1)
					{
						GL11.glPushMatrix();
						{
							GL11.glTranslatef(xOff * offset[cnt][0], yOff, zOff * offset[cnt][1]);
							GL11.glScalef(zOff, zOff, zOff);
							GL11.glRotatef(90, 0, 0, 1);
							renderLog(block, itemStack.getItemDamage(), zOff * 0.5F, xOff, slotID);
						}
						GL11.glPopMatrix();

						++cnt;
						if (cnt >= 9)
						{
							/* Expand y-offset by z-offset. Y and Z are interlocked, so that all logs have cubic front and rear. */
							yOff += zOff;
							cnt -= 9;
						}
					}
				}
			}
		}
		GL11.glPopMatrix();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glScalef(0.4F, 0.4F, 0.4F);

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}

	/**
	 * Render a wood log using the given Block with the given length and half width, and apply some "random" rotation depending on its seed (i.e. its slotID in the inventory).
	 */
	private static void renderLog(Block block, int metadata, float widthHalf, float length, int seed)
	{
		int max = (int)length;
		if (max <= 0.0F)
		{
			/* Why bother, when the length is equal to or less than zero? */
			return;
		}

		float f;
		if (seed % 2 == 0)
		{
			f = (seed * 2) % 10;
		}
		else
		{
			f = -((seed * 2) & 10);
		}

		for (int i = -max; i <= max; ++i)
		{
			GL11.glPushMatrix();
			GL11.glTranslatef(0.0F, i + (f / 100.0F), 0.0F);
			GL11.glRotatef(f, 0.0F, 1.0F, 0.0F);
			RenderUtil.renderBlocks.renderBlockAsItem(block, metadata, 1.0F);
			GL11.glPopMatrix();
		}
	}
}