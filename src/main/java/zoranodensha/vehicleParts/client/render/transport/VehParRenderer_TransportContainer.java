package zoranodensha.vehicleParts.client.render.transport;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_ModFreight;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportContainer;



@SideOnly(Side.CLIENT)
public class VehParRenderer_TransportContainer extends AVehParRendererBase_ModFreight implements IVehiclePartRenderer
{
	private static final ObjectList end = model.makeGroup("Container_End");
	private static final ObjectList end_bars = model.makeGroup("Container_End_Bars");
	private static final ObjectList end_board = model.makeGroup("Container_End_Board", "Container_End_Bars");

	private static final ObjectList center = model.makeGroup("Container_Center");
	private static final ObjectList center_board = model.makeGroup("Container_Center_Board");

	/** The part to render. */
	final VehParTransportContainer part;
	private TesselatorVertexState vertexState;
	private final Mat3x3 rotationMatrix = new Mat3x3().rotateY(180);



	public VehParRenderer_TransportContainer(VehParTransportContainer part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParTransportContainer)
		{
			return new VehParRenderer_TransportContainer((VehParTransportContainer)copyPart);
		}
		return null;
	}

	private void renderContainer(Tessellator tessellator, ObjectList center, ObjectList end)
	{
		final int size = part.getPieces() + 1;
		for (int i = 0; i <= size; ++i)
		{
			tessellator.setTranslation((-0.25D * size) + (i * 0.5D), 0.0D, 0.0D);

			if (i == 0)
			{
				end.render(tessellator, rotationMatrix);
			}
			else if (i == size)
			{
				end.render(tessellator);
			}
			else if (center != null)
			{
				center.render(tessellator);
			}
		}
	}

	@Override
	public boolean renderInPass(int pass)
	{
		/* Render both passes if there is a texture to render. Otherwise, just render default pass. */
		return (part.getLogoTexture() != null) || (pass == 0);
	}

	private void renderLogo()
	{
		double size = (part.getPieces() + 1) * 0.25D;
		double xOff = size - 0.19527D;

		Tessellator t = Tessellator.instance;
		t.startDrawingQuads();
		t.addVertexWithUV(-xOff, 0.4D, -0.75D, 1, 0);
		t.addVertexWithUV(xOff, 0.4D, -0.75D, 0, 0);
		t.addVertexWithUV(xOff, -0.35D, -0.75D, 0, 1);
		t.addVertexWithUV(-xOff, -0.35D, -0.75D, 1, 1);
		t.draw();
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		switch (TrainRenderer.renderPass)
		{
			case 0:
				Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
				Tessellator tessellator = Tessellator.instance;
				tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexState == null)
				{
					part.color.apply(tessellator);
					renderContainer(tessellator, center, end);

					tessellator.setColorOpaque_F(0.47F, 0.47F, 0.5F);
					if (part.getLogoTexture() != null)
					{
						renderContainer(tessellator, center_board, end_board);
					}
					else
					{
						renderContainer(tessellator, null, end_bars);
					}

					tessellator.setTranslation(0, 0, 0);
					vertexState = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState);
				}
			}
				tessellator.draw();
				break;

			case 1:
				if (part.getLogoTexture() != null)
				{
					GL11.glColor3f(1.0F, 1.0F, 1.0F);
					Minecraft.getMinecraft().renderEngine.bindTexture(part.getLogoTexture());

					renderLogo();
					GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
					renderLogo();
				}
				break;
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glScalef(0.7F, 0.7F, 0.7F);

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}