package zoranodensha.vehicleParts.client.render.transport;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_ModFreight;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportTankContainer;



@SideOnly(Side.CLIENT)
public class VehParRenderer_TransportTankContainer extends AVehParRendererBase_ModFreight implements IVehiclePartRenderer
{
	private static final ObjectList frame = model.makeGroup("Tank_Frame");
	private static final ObjectList ladder = model.makeGroup("Tank_Ladder");
	private static final ObjectList logo = model.makeGroup("Tank_Logo_Image");
	private static final ObjectList tank = model.makeGroup("Tank_Tank");
	private static final ObjectList tank2 = model.makeGroup("Tank_Tank", "Tank_Logo_Frame");
	private static final ObjectList vent = model.makeGroup("Tank_Vent");

	/** The part to render. */
	final VehParTransportTankContainer part;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_TransportTankContainer(VehParTransportTankContainer part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParTransportTankContainer)
		{
			return new VehParRenderer_TransportTankContainer((VehParTransportTankContainer)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return true;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Opaque = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Tessellator tessellator = Tessellator.instance;
		switch (TrainRenderer.renderPass)
		{
			case 0:
				Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
				tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexState_Opaque == null)
				{
					tessellator.setColorOpaque_F(0.37F, 0.37F, 0.37F);
					ladder.render(tessellator);

					part.color.apply(tessellator);
					frame.render(tessellator);

					part.colorTank.apply(tessellator);
					(part.getLogoTexture() != null ? tank2 : tank).render(tessellator);

					tessellator.setColorOpaque_F(0.08F, 0.08F, 0.08F);
					vent.render(tessellator);

					vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState_Opaque);
				}
			}
				tessellator.draw();
				break;

			case 1:
				if (part.getLogoTexture() != null)
				{
					Minecraft.getMinecraft().renderEngine.bindTexture(part.getLogoTexture());
					tessellator.startDrawing(GL11.GL_TRIANGLES);
					{
						if (vertexState_Transparent == null)
						{
							tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
							logo.render(tessellator);
							vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
						}
						else
						{
							tessellator.setVertexState(vertexState_Transparent);
						}
					}
					tessellator.draw();
				}
				break;
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.5F, 0.25F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}