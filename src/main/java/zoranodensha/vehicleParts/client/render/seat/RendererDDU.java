package zoranodensha.vehicleParts.client.render.seat;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.Random;

import org.apache.logging.log4j.Level;

import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;
import net.minecraftforge.common.ForgeChunkManager;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.type.PartTypeDoor;
import zoranodensha.api.vehicles.part.type.cab.AScreenRenderer;
import zoranodensha.api.vehicles.part.type.cab.DDU;
import zoranodensha.api.vehicles.part.type.cab.EDDUMenu;
import zoranodensha.api.vehicles.part.type.cab.MTMS;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.vehicleParts.client.render.GraphicsResources;


public class RendererDDU extends AScreenRenderer
{
	private static final Dimension SIZE = new Dimension(512, 320);
	
	/**
	 * The DDU screen that this renderer is for. There may be multiple screens in a
	 * single cab so this DDU instance only represents one of potentially many.
	 */
	private final DDU ddu;
	
	/**
	 * A random number generator.
	 */
	private Random random;
	
	/**
	 * Metric to Imperial Conversion Coefficients
	 */
	private static final float KMH_TO_MPH = 0.621371F;
	private static final float KPA_TO_PSI = 0.145038F;
	private static final float M_TO_FOOT = 3.28084F;
	private static final float M_TO_YARD = 1.09361F;
	private static final float TONNE_TO_TON = 0.984207F;
	
	/**
	 * Interface area.
	 */
	private final Rectangle area = new Rectangle(5, 5, texture.getWidth() - 10, texture.getHeight() - 10);
	
	
	/**
	 * Initialises a new instance of the
	 * {@link zoranodensha.vehicleParts.client.render.seat.RendererDDU} class, a
	 * renderer for a DDU instance.
	 *
	 * @param ddu - The particular DDU that this renderer is for.
	 */
	public RendererDDU(DDU ddu)
	{
		/*
		 * Superclass call to set the screen width and height.
		 */
		super(SIZE);
		
		/*
		 * RNG.
		 */
		random = new Random();
		
		this.ddu = ddu;
	}
	
	@Override
	public void onRepaint()
	{
		MTMS mtms = ddu.mtms;
		Train train = mtms.parentCab.getTrain();
		
		/*
		 * Get the graphics object from the texture.
		 */
		Graphics2D g = texture.getGraphics();
		AffineTransform transform;
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
		
		/*
		 * Get the width and height of the texture.
		 */
		int width = texture.getWidth();
		int height = texture.getHeight();
		
		/* Get whether imperial units display has been enabled. */
		final boolean imperial = mtms.imperial.get();
		
		/*
		 * Clear the screen.
		 */
		g.setBackground(Color.BLACK);
		g.clearRect(0, 0, width, height);
		
		/*
		 * Draw the current menu.
		 */
		switch (ddu.menu.get())
		{
			/*
			 * Operations Screen
			 */
			case MENU_OPERATION_DETAILED:
			case MENU_OPERATION:
			{
				/* Background */
				if (!ddu.contrast.get())
				{
					Stroke oldStroke = g.getStroke();
					{
						g.setStroke(new BasicStroke(20.0F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
						g.setColor(COLOR_BACKGROUND);
						g.drawRect(area.x + 10, area.y + 10, area.width - 20, area.height - 20);
						g.fillRect(area.x + 10, area.y + 10, area.width - 20, area.height - 20);
					}
					g.setStroke(oldStroke);
					
				}
				
				/*
				 * Speed Area
				 */
				{
					final float speedOverLimit = mtms.getOverspeedAmount();
					final float speedOverTarget = mtms.getOvertargetAmount();
					// Value that the needle should point to. This could either be km/h or MPH.
					final float speedometerNumber = imperial ? mtms.trainSpeed.get() * KMH_TO_MPH : mtms.trainSpeed.get();
					
					/*
					 * Speedometer
					 */
					{
						/* Determine the maximum value of the speedometer. */
						int vMax = train.getMaxSpeed();
						int maxSpeed = vMax;
						
						if (imperial)
						{
							maxSpeed *= KMH_TO_MPH;
						}
						
						if (maxSpeed >= 300)
						{
							maxSpeed = 400;
						}
						else if (maxSpeed >= 200)
						{
							maxSpeed = 300;
						}
						else if (maxSpeed >= 100)
						{
							maxSpeed = 200;
						}
						else
						{
							maxSpeed = 140;
						}
						
						/* Calculate step size depending on maximum speed. */
						int centreX = area.x + 136;
						int centreY = area.y + 130;
						float step = 270.0F / maxSpeed;
						
						if (maxSpeed >= 400)
						{
							step = 270.0F / (maxSpeed - 100);
						}
						else if (maxSpeed >= 300)
						{
							step = 270.0F / (maxSpeed - 50);
						}
						
						/* Render notches */
						transform = g.getTransform();
						{
							g.rotate(ZnDMathHelper.RAD_MULTIPLIER * -135.0F, centreX, centreY);
							
							float notchStep = 10.0F;
							for (int i = 0; i <= maxSpeed; i += 10)
							{
								if (i >= 200 && notchStep >= 10.0F)
								{
									notchStep = 5.0F;
								}
								
								if ((!imperial && i == vMax) || (imperial && i == (int) (vMax * KMH_TO_MPH)))
								{
									g.setColor(Color.ORANGE);
								}
								else
								{
									g.setColor(Color.WHITE);
								}
								
								if (i % (maxSpeed >= 200 ? 50 : 20) == 0 || i == maxSpeed)
								{
									g.fillRect(centreX - 2, centreY - 110, 4, 20);
								}
								else
								{
									g.fillRect(centreX - 2, centreY - 110, 3, 10);
								}
								
								g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (step * notchStep), centreX, centreY);
							}
						}
						g.setTransform(transform);
						
						/* Numbers */
						{
							g.setColor(Color.WHITE);
							g.setFont(FONT_DEFAULT);
							
							float rotation = -135.0F;
							float notchStep = 10.0F;
							
							for (int i = 0; i <= maxSpeed; i += 10)
							{
								if (i >= 200 && notchStep >= 10.0F)
								{
									notchStep = 5.0F;
								}
								
								if (i % (i >= 200 ? 100 : (maxSpeed >= 200 ? 50 : 20)) == 0)
								{
									
									int numberX = centreX + (int) (Math.sin(ZnDMathHelper.RAD_MULTIPLIER * rotation) * 70.0F);
									int numberY = centreY - (int) (Math.cos(ZnDMathHelper.RAD_MULTIPLIER * rotation) * 70.0F);
									
									drawCenteredString(g, String.valueOf(i), numberX, numberY);
								}
								
								rotation += (step * notchStep);
							}
						}
						
						Color needleBackgroundColor = Color.LIGHT_GRAY;
						Color needleTextColor = ddu.contrast.get() ? Color.BLACK : COLOR_BACKGROUND;
						if (mtms.speedLimit.get() >= 0.0F)
						{
							if (speedOverLimit > mtms.speedLimit.get() * 0.12F)
							{
								needleBackgroundColor = Color.RED;
								needleTextColor = Color.WHITE;
							}
							else if (speedOverLimit > (mtms.speedLimit.get() * 0.02F) + 1.0F)
							{
								needleBackgroundColor = Color.ORANGE;
								needleTextColor = Color.BLACK;
							}
							else if (speedOverTarget > 0.0F && mtms.targetSpeed.get() >= 0 && mtms.speedLimit.get() < mtms.trackSpeed.get())
							{
								needleBackgroundColor = Color.YELLOW;
								needleTextColor = Color.BLACK;
							}
						}
						
						/* Needle */
						transform = g.getTransform();
						{
							g.setColor(needleBackgroundColor);
							g.rotate(ZnDMathHelper.RAD_MULTIPLIER * -135.0F, centreX, centreY);
							
							if (Math.round(speedometerNumber) > 200.0F && maxSpeed > 200)
							{
								g.rotate(ZnDMathHelper.RAD_MULTIPLIER * ((200.0F + ((Math.round(speedometerNumber) - 200.0F) / 2.0F)) * step), centreX, centreY);
							}
							else
							{
								g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (Math.round(speedometerNumber) * step), centreX, centreY);
							}
							
							g.fillRect(centreX - 2, centreY - 100, 4, 100);
							g.fillRect(centreX - 4, centreY - 60, 8, 60);
						}
						g.setTransform(transform);
						
						/* Circle */
						{
							g.setColor(needleBackgroundColor);
							g.fillOval(centreX - 32, centreY - 32, 64, 64);
							
							g.setColor(needleTextColor);
							g.setFont(FONT_SPEEDOMETER);
							drawCenteredString(g, String.format("%.0f", speedometerNumber), centreX, centreY + 2);
							
							if (imperial)
							{
								g.setColor(COLOR_WARNING);
								g.setFont(FONT_MINI);
								drawCenteredString(g, "mph", centreX, centreY + 50);
							}
							else
							{
								g.setColor(ddu.contrast.get() ? Color.DARK_GRAY : Color.GRAY);
								g.setFont(FONT_MINI);
								drawCenteredString(g, "km/h", centreX, centreY + 50);
							}
							
							/*
							 * Extra Border
							 */
							g.setStroke(new BasicStroke(5.0F));
							if (speedOverLimit >= 5.0F)
							{
								g.setColor(System.currentTimeMillis() / 250 % 2 == 0 ? Color.RED : Color.GRAY);
							}
							else
							{
								g.setColor(Color.GRAY);
							}
							g.drawOval(centreX - 30, centreY - 30, 60, 60);
						}
						
						/*
						 * Speed Limit Indicator
						 */
						if (mtms.speedLimit.get() >= 0.0F)
						{
							/*
							 * Speed Limit Indicator Values
							 */
							float speedLimit = mtms.speedLimit.get();
							float targetSpeed = mtms.targetSpeed.get();
							boolean braking = mtms.targetSpeed.get() >= 0;
							float minimumSpeed = mtms.MIN_SPEED_LIMIT;
							
							if (imperial)
							{
								speedLimit *= KMH_TO_MPH;
								targetSpeed *= KMH_TO_MPH;
							}
							
							transform = g.getTransform();
							{
								g.rotate(ZnDMathHelper.RAD_MULTIPLIER * -135.0F, centreX, centreY);
								
								if (speedLimit > maxSpeed)
								{
									speedLimit = maxSpeed;
								}
								
								if (speedLimit > 200 && maxSpeed > 200)
								{
									if (braking)
									{
										final float speedLimitOver200 = speedLimit - 200.0F;
										final float targetSpeedOver200 = targetSpeed - 200.0F;
										
										if (targetSpeedOver200 > 0.0F)
										{
											final float beginOffset = (200.0F * step) + (targetSpeedOver200 * step * 0.5F);
											
											g.setColor(Color.GRAY);
											g.setStroke(new BasicStroke(8.0F));
											g.drawArc(centreX - 120, centreY - 120, 240, 240, 95, (int) -beginOffset - 5);
											
											g.setColor(needleBackgroundColor);
											g.drawArc(centreX - 120, centreY - 120, 240, 240, 95 - (int) (beginOffset + 8), (int) ((speedLimitOver200 - targetSpeedOver200) * (-step * 0.5F)) + 5);
										}
										else
										{
											final float beginOffset = (targetSpeed * step);
											
											g.setColor(Color.GRAY);
											g.setStroke(new BasicStroke(8.0F));
											g.drawArc(centreX - 120, centreY - 120, 240, 240, 95, (int) -beginOffset - 5);
											
											g.setColor(needleBackgroundColor);
											g.drawArc(centreX - 120, centreY - 120, 240, 240, 95 - (int) (beginOffset + 8), (int) (((200 - targetSpeed) * -step) + (int) (speedLimitOver200 * (-step * 0.5F)) + 5));
										}
										
										g.setStroke(new BasicStroke(6.0F));
										
										AffineTransform t = g.getTransform();
										{
											g.rotate(ZnDMathHelper.RAD_MULTIPLIER * ((200.0F * step) + (speedLimitOver200 * (step * 0.5F))), centreX, centreY);
											g.drawLine(centreX, centreY - 121, centreX, centreY - 102);
										}
										g.setTransform(t);
									}
									else
									{
										final float speedLimitOver200 = speedLimit - 200.0F;
										
										g.setColor(Color.GRAY);
										g.setStroke(new BasicStroke(8.0F));
										g.drawArc(centreX - 120, centreY - 120, 240, 240, 95, (int) ((200.0F * -step) + (speedLimitOver200 * -step * 0.5F)) - 3);
										
										g.setStroke(new BasicStroke(6.0F));
										AffineTransform t = g.getTransform();
										{
											g.rotate(ZnDMathHelper.RAD_MULTIPLIER * ((200.0F * step) + (speedLimitOver200 * (step * 0.5F))), centreX, centreY);
											g.drawLine(centreX, centreY - 121, centreX, centreY - 102);
										}
										g.setTransform(t);
									}
								}
								else
								{
									if (braking)
									{
										/*
										 * Minimum Speed Indicator
										 */
										if (targetSpeed < minimumSpeed)
										{
											g.setColor(Color.GRAY);
											g.setStroke(new BasicStroke(8.0F));
											g.drawArc(centreX - 120, centreY - 120, 240, 240, 95, (int) (minimumSpeed * -step) - 5);
										}
										
										g.setColor(Color.GRAY);
										g.setStroke(new BasicStroke(8.0F));
										g.drawArc(centreX - 120, centreY - 120, 240, 240, 95, (int) (targetSpeed * -step) - 5);
										
										g.setColor(needleBackgroundColor);
										g.drawArc(centreX - 120, centreY - 120, 240, 240, 95 - ((int) (targetSpeed * step) + 5), (int) ((speedLimit - targetSpeed) * -step) + 2);
										
										g.setStroke(new BasicStroke(6.0F));
										AffineTransform t = g.getTransform();
										{
											g.rotate(ZnDMathHelper.RAD_MULTIPLIER * ((speedLimit - 0.75F) * step), centreX, centreY);
											g.drawLine(centreX, centreY - 121, centreX, centreY - 102);
										}
										g.setTransform(t);
										
										/*
										 * Minimum Speed Indicator
										 */
										if (targetSpeed < minimumSpeed)
										{
											g.setColor(Color.GRAY);
											g.setStroke(new BasicStroke(5.0F));
											g.drawArc(centreX - 122, centreY - 122, 244, 244, 95, (int) (minimumSpeed * -step) - 5);
										}
									}
									else
									{
										g.setColor(Color.GRAY);
										g.setStroke(new BasicStroke(8.0F));
										g.drawArc(centreX - 120, centreY - 120, 240, 240, 95, (int) (speedLimit * -step) - 3);
										
										g.setStroke(new BasicStroke(6.0F));
										AffineTransform t = g.getTransform();
										{
											g.rotate(ZnDMathHelper.RAD_MULTIPLIER * ((speedLimit - 0.75F) * step), centreX, centreY);
											g.drawLine(centreX, centreY - 121, centreX, centreY - 102);
										}
										g.setTransform(t);
									}
									
									/*
									 * Overspeed Indicator
									 */
									if (speedOverLimit >= 1.0F)
									{
										g.setColor(Color.RED);
										g.setStroke(new BasicStroke(24.0F, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
										
										AffineTransform t = g.getTransform();
										{
											if (braking)
											{
												if (speedLimit < minimumSpeed)
												{
													g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (((minimumSpeed - targetSpeed) - 0.75F) * step), centreX, centreY);
												}
												else
												{
													g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (((speedLimit - targetSpeed) - 0.75F) * step), centreX, centreY);
												}
											}
											else
											{
												g.rotate(ZnDMathHelper.RAD_MULTIPLIER * ((speedLimit - 0.75F) * step), centreX, centreY);
											}
											g.drawArc(centreX - 111, centreY - 111, 222, 222, 89 - ((int) (targetSpeed * step)), (int) (speedOverLimit * -step) - 2);
										}
										g.setTransform(t);
									}
								}
							}
							g.setTransform(transform);
						}
						
						/*
						 * AFB Indicator
						 */
						g.setColor(ddu.contrast.get() ? Color.DARK_GRAY : COLOR_BACKGROUND.darker().darker());
						g.fillRect(centreX - 42, centreY + 68, 84, 34);
						g.setColor(ddu.contrast.get() ? Color.BLACK : COLOR_BACKGROUND.darker());
						g.fillRect(centreX - 40, centreY + 70, 80, 30);
						
						if (mtms.autodriveEnable.get())
						{
							g.setColor(Color.WHITE);
							g.setFont(FONT_DEFAULT);
							drawCenteredString(g, "AUTO", centreX, centreY + 87);
						}
						else if (mtms.cruiseControlEnabled.get())
						{
							final float afbDisplay = mtms.cruiseControlTargetSpeed.get() * (imperial ? KMH_TO_MPH : 1.0F);
							
							transform = g.getTransform();
							{
								g.rotate(ZnDMathHelper.RAD_MULTIPLIER * -135.0F, centreX, centreY);
								
								if (afbDisplay > 200.0F && maxSpeed > 200)
								{
									g.rotate(ZnDMathHelper.RAD_MULTIPLIER * ((200.0F + ((afbDisplay - 200.0F) / 2.0F)) * step), centreX, centreY);
								}
								else
								{
									g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (afbDisplay * step), centreX, centreY);
								}
								
								g.setColor(COLOR_WARNING);
								g.fillPolygon(new int[]{centreX - 8, centreX, centreX + 8, centreX}, new int[]{centreY - 110, centreY - 102, centreY - 110, centreY - 118}, 4);
								
								/*
								 * If in imperial mode, the AFB indicator won't line up with a line, so add
								 * another for better visibility.
								 */
								if (imperial)
								{
									g.fillRect(centreX - 2, centreY - 110, 4, 20);
								}
							}
							g.setTransform(transform);
							
							g.setColor(Color.WHITE);
							g.setFont(FONT_DEFAULT);
							drawCenteredString(g, String.valueOf(Math.round(mtms.cruiseControlTargetSpeed.get())), centreX, centreY + 87);
						}
						
					}
					
				}
				
				int currentNotch;
				
				final int powerNotch = Math.round(mtms.parentCab.getThrottlePhysical() * 8.0F);
				final int brakeNotch = Math.max(Math.round(mtms.parentCab.getBrakeLevelPhysical() * 8.0F), Math.round(mtms.parentCab.getDynamicBrakeLevelPhysical() * 8.0F));
				
				if (brakeNotch > 0)
				{
					currentNotch = brakeNotch * -1;
				}
				else
				{
					currentNotch = powerNotch;
				}
				
				/*
				 *
				 * Traction Area
				 *
				 */
				if (ddu.menu.get().equals(EDDUMenu.MENU_OPERATION) && mtms.autodriveEnable.get())
				{
					/*
					 * In this code block, details about the currently enabled autodrive system are
					 * displayed, including information about chunk loading.
					 */
					
					/*
					 * Outline/Border
					 */
					g.setStroke(new BasicStroke(2.0F, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
					g.setColor(ddu.contrast.get() ? Color.DARK_GRAY.darker() : COLOR_BACKGROUND.darker());
					g.drawRect(area.x + (area.width / 2) + 24, area.y + 10, 210, area.height - 85);
					
					/*
					 * Titles
					 */
					g.setFont(FONT_MINI);
					g.setColor(Color.GRAY);
					g.drawString(StatCollector.translateToLocal("zoranodensha.text.mtms.autodriveState"), area.x + (area.width / 2) + 28, area.y + 30);
					g.drawString(StatCollector.translateToLocal("zoranodensha.text.mtms.autodriveChunks"), area.x + (area.width / 2) + 28, area.y + 115);
					
					g.setColor(Color.LIGHT_GRAY);
					g.setStroke(new BasicStroke(1.0F));
					g.drawLine(area.x + (area.width / 2) + 28, area.y + 35, area.x + (area.width / 2) + 228, area.y + 35);
					g.drawLine(area.x + (area.width / 2) + 28, area.y + 120, area.x + (area.width / 2) + 228, area.y + 120);
					
					g.setFont(FONT_DEFAULT);
					g.setColor(Color.WHITE);
					
					String autodriveState = mtms.autodriveState.get().toString();
					g.drawString(autodriveState.substring(0, 1).toUpperCase() + autodriveState.substring(1).toLowerCase(), area.x + (area.width / 2) + 28, area.y + 60);
					g.drawString(String.format("%.1f → %.1f km/h", mtms.trainSpeed.get(), mtms.speedLimit.get()), area.x + (area.width / 2) + 28, area.y + 85);
					g.drawString(String.format("%d / %d", mtms.autodriveChunksLoaded.get(), mtms.autodriveChunksFree.get()), area.x + (area.width / 2) + 28, area.y + 145);
				}
				else if (ddu.menu.get().equals(EDDUMenu.MENU_OPERATION) && !mtms.autodriveEnable.get())
				{
					int centreX = area.x + area.width - 125;
					int centreY = area.y + 130;
					
					/*
					 * Thick Bar
					 */
					g.setColor(Color.LIGHT_GRAY);
					g.setStroke(new BasicStroke(25.0F, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
					g.drawArc(centreX - 95, centreY - 95, 190, 190, -45, 270);
					
					float acceleration = mtms.trainEffortAcceleration.get();
					float braking = mtms.trainEffortRetardation.get();
					// Perform a bounds check so the bar doesn't go further than the gauge.
					g.setColor(Color.GREEN);
					g.drawArc(centreX - 95, centreY - 95, 190, 190, 90, (int) (acceleration * -135.0F));
					g.setColor(braking > 0.99F ? Color.RED : Color.YELLOW);
					g.drawArc(centreX - 95, centreY - 95, 190, 190, 90, (int) (braking * 135.0F));
					
					/*
					 * Divisions
					 */
					g.setColor(ddu.contrast.get() ? Color.BLACK : COLOR_BACKGROUND);
					g.setStroke(new BasicStroke(3.0F, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
					transform = g.getTransform();
					{
						g.rotate(ZnDMathHelper.RAD_MULTIPLIER * -135.0F, centreX, centreY);
						
						for (int count = 0; count <= 24; count++)
						{
							g.drawLine(centreX, centreY - 112, centreX, centreY - 78);
							g.rotate(ZnDMathHelper.RAD_MULTIPLIER * 11.25F, centreX, centreY);
						}
					}
					g.setTransform(transform);
					
					/*
					 * Circle
					 */
					{
						g.setColor(Color.LIGHT_GRAY);
						g.fillOval(centreX - 32, centreY - 32, 64, 64);
						
						g.setStroke(new BasicStroke(5.0F));
						if (currentNotch > 0)
						{
							g.setColor(Color.GREEN);
							g.drawOval(centreX - 30, centreY - 30, 60, 60);
						}
						else if (currentNotch < 0)
						{
							g.setColor(Color.ORANGE);
							g.drawOval(centreX - 30, centreY - 30, 60, 60);
						}
						else
						{
							g.setColor(Color.GRAY);
							g.drawOval(centreX - 30, centreY - 30, 60, 60);
						}
						
						g.setFont(FONT_SPEEDOMETER);
						g.setColor(ddu.contrast.get() ? Color.BLACK : COLOR_BACKGROUND);
						drawCenteredString(g, currentNotch != 0 ? ((currentNotch > 0 ? "+" : "") + String.valueOf(currentNotch)) : "0", centreX, centreY + 2);
					}
					
					/*
					 * Unit
					 */
					g.setColor(ddu.contrast.get() ? Color.DARK_GRAY : Color.GRAY);
					g.setFont(FONT_MINI);
					drawCenteredString(g, "%", centreX, centreY + 50);
					g.setFont(FONT_DEFAULT);
					drawCenteredString(g, "-", centreX - 30, centreY + 70);
					drawCenteredString(g, "+", centreX + 30, centreY + 70);
					
					/*
					 * Indicator
					 */
					transform = g.getTransform();
					{
						float angle = mtms.trainEffortAcceleration.get() - mtms.trainEffortRetardation.get();
						
						if (angle < -1.0F)
						{
							angle = -1.0F;
						}
						else if (angle > 1.0F)
						{
							angle = 1.0F;
						}
						
						g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (angle * 135.0F), centreX, centreY);
						g.setColor(Color.WHITE);
						g.fillPolygon(new int[]{centreX, centreX - 8, centreX + 8}, new int[]{centreY - 110, centreY - 120, centreY - 120}, 3);
					}
					g.setTransform(transform);
					
					/*
					 * Numbers
					 */
					g.setFont(FONT_MINI);
					{
						float posX, posY;
						
						for (double number = -100.0D; number <= 100.0D; number += 25.0D)
						{
							posX = (float) (centreX + (Math.sin(ZnDMathHelper.RAD_MULTIPLIER * (number * 1.35D)) * 68.0D));
							posY = (float) (centreY - (Math.cos(ZnDMathHelper.RAD_MULTIPLIER * (number * 1.35D)) * 68.0D));
							
							drawCenteredString(g, String.format("%.0f", Math.abs(number)), (int) posX, (int) posY);
						}
					}
				}
				else
				{
					/*
					 * Outlines
					 */
					g.setStroke(new BasicStroke(2.0F, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
					g.setColor(ddu.contrast.get() ? Color.DARK_GRAY.darker() : COLOR_BACKGROUND.darker());
					for (int i = 0; i < 5; i++)
					{
						g.drawRect(area.x + (area.width / 2) + 24 + (i * 42), area.y + 10, 42 + (i == 4 ? 10 : 0), area.height - 85);
					}
					
					/*
					 * Titles for all the graphs
					 */
					g.setFont(FONT_MINI);
					g.setColor(Color.LIGHT_GRAY);
					drawCenteredString(g, "MR", area.x + (area.width / 2) + 24 + (int) (42 * 0.5F), area.y + 28);
					drawCenteredString(g, "BP", area.x + (area.width / 2) + 24 + (int) (42 * 1.5F), area.y + 28);
					drawCenteredString(g, "BC1", area.x + (area.width / 2) + 24 + (int) (42 * 2.5F), area.y + 28);
					drawCenteredString(g, "BC2", area.x + (area.width / 2) + 24 + (int) (42 * 3.5F), area.y + 28);
					drawCenteredString(g, "%", area.x + (area.width / 2) + 29 + (int) (42 * 4.5F), area.y + 28);
					
					/*
					 * Bar Gauges
					 */
					{
						/*
						 * Backgrounds
						 */
						for (int x = 0; x < 5; x++)
						{
							g.setColor(ddu.contrast.get() ? Color.DARK_GRAY.darker() : COLOR_BACKGROUND.darker());
							g.fillRect(area.x + (area.width / 2) + 24 + (x * 42), area.y + 55, 21, area.height - 140);
						}
						
						g.setColor(Color.LIGHT_GRAY);
						g.setFont(FONT_MICRO.deriveFont(Font.BOLD));
						{
							
							if (imperial)
							{
								/*
								 * Main Reservoir Numbers
								 */
								for (float i = 0.0F; i <= 180.0F; i += 30.0F)
								{
									g.drawString(String.valueOf(Math.round(i)), area.x + (area.width / 2) + 46, (int) (area.y + area.height - 84 - (i * 0.917F)));
								}
								
								/*
								 * BP and BC1/2 Numbers
								 */
								for (int x = 1; x <= 3; x++)
								{
									for (float i = 0.0F; i <= 90.0F; i += 15.0F)
									{
										g.drawString(String.valueOf(Math.round(i)), area.x + (area.width / 2) + 46 + (x * 42), (int) (area.y + area.height - 84 - (i * 1.834F)));
									}
								}
							}
							else
							{
								/*
								 * Main Reservoir Numbers
								 */
								for (float i = 0.0F; i <= 12.0F; i += 2.0F)
								{
									g.drawString(String.valueOf(Math.round(i)), area.x + (area.width / 2) + 49, (int) (area.y + area.height - 84 - (i * 13.75F)));
								}
								
								/*
								 * BP and BC1/2 Numbers
								 */
								for (int x = 1; x <= 3; x++)
								{
									for (float i = 0.0F; i <= 6.0F; i += 1.0F)
									{
										g.drawString(String.valueOf(Math.round(i)), area.x + (area.width / 2) + 49 + (x * 42), (int) (area.y + area.height - 84 - (i * 27.5F)));
									}
								}
							}
							
							/*
							 * Acceleration Numbers
							 */
							for (float i = -100.0F; i <= 100.0F; i += 25.0F)
							{
								g.drawString(String.valueOf(Math.round(i)), area.x + (area.width / 2) + 49 + (4 * 42), (int) (area.y + area.height - 84 - 82 - (i * 0.825F)));
							}
							
						}
						
						/*
						 * Main Reservoir Value
						 */
						{
							float value = mtms.realtimeMainReservoirPressure.get();
							
							/*
							 * Clamp value within the graph bounds.
							 */
							if (value < 0.0F)
							{
								value = 0.0F;
							}
							else if (value > 1200.0F)
							{
								value = 1200.0F;
							}
							
							/*
							 * Assign the bar's colour based on correct pressure parameters.
							 */
							if (value < 500.0F)
							{
								g.setColor(Color.RED);
							}
							else if (value < 600.0F)
							{
								g.setColor(Color.YELLOW);
							}
							else
							{
								g.setColor(Color.GREEN);
							}
							
							/*
							 * Render the bar.
							 */
							g.fillRect(area.x + (area.width / 2) + 27, area.y + 45 + (area.height - 130) - (int) (value * 0.14F), 16, (int) (value * 0.14F));
						}
						
						/*
						 * Brake Pipe Value
						 */
						{
							float value = mtms.realtimeBrakePipePressure.get();
							
							/*
							 * Clamp the value within the graph limits.
							 */
							if (value < 0.0F)
							{
								value = 0.0F;
							}
							else if (value > 600.0F)
							{
								value = 600.0F;
							}
							
							/*
							 * Assign the graph colour.
							 */
							if (value < 340.0F)
							{
								g.setColor(Color.RED);
							}
							else if (value < 480.0F)
							{
								g.setColor(Color.YELLOW);
							}
							else if (value > 530.0F)
							{
								g.setColor(Color.RED);
							}
							else
							{
								g.setColor(Color.GREEN);
							}
							
							/*
							 * Render the bar.
							 */
							g.fillRect(area.x + (area.width / 2) + 69, area.y + 45 + (area.height - 130) - (int) (value * 0.28F), 16, (int) (value * 0.28F));
						}
						
						/*
						 * Brake Cylinder Values
						 */
						{
							float value1 = mtms.realtimeBrakeCylinderPressure1.get();
							float value2 = mtms.realtimeBrakeCylinderPressure2.get();
							
							/*
							 * Clamp the value within the graph limits.
							 */
							if (value1 < 0.0F)
							{
								value1 = 0.0F;
							}
							else if (value1 > 600.0F)
							{
								value1 = 600.0F;
							}
							if (value2 < 0.0F)
							{
								value2 = 0.0F;
							}
							else if (value2 > 600.0F)
							{
								value2 = 600.0F;
							}
							
							/*
							 * Assign the graph colour.
							 */
							g.setColor(Color.YELLOW);
							
							/*
							 * Render the bars.
							 */
							g.fillRect(area.x + (area.width / 2) + 111, area.y + 45 + (area.height - 130) - (int) (value1 * 0.28F), 16, (int) (value1 * 0.28F));
							g.fillRect(area.x + (area.width / 2) + 153, area.y + 45 + (area.height - 130) - (int) (value2 * 0.28F), 16, (int) (value2 * 0.28F));
						}
						
						/*
						 * Traction Graph
						 */
						{
							float acceleration = mtms.trainEffortAcceleration.get();
							float braking = mtms.trainEffortRetardation.get();
							float notch = (float) currentNotch / 8.0F;
							
							if (acceleration > 1.0F)
							{
								acceleration = 1.0F;
							}
							else if (acceleration < -0.0F)
							{
								acceleration = -0.0F;
							}
							if (braking > 1.0F)
							{
								braking = 1.0F;
							}
							else if (braking < 0.0F)
							{
								braking = 0.0F;
							}
							
							if (notch > 1.0F)
							{
								notch = 1.0F;
							}
							else if (notch < -1.0F)
							{
								notch = -1.0F;
							}
							
							if (acceleration > braking)
							{
								g.setColor(Color.GREEN);
							}
							else
							{
								g.setColor(Color.RED);
							}
							
							if (acceleration > 0.0F)
							{
								g.fillRect(area.x + (area.width / 2) + 195, area.y + 45 + (area.height - 130) - 84 - (int) (acceleration * 84.0F), 16, (int) (acceleration * 84.0F));
							}
							if (braking > 0.0F)
							{
								g.fillRect(area.x + (area.width / 2) + 195, area.y + 45 + (area.height - 130) - 84, 16, (int) (braking * 84.0F));
							}
							
							if (notch > 0.0F)
							{
								g.setColor(Color.WHITE);
								
								g.fillRect(area.x + (area.width / 2) + 200, area.y + 45 + (area.height - 130) - 84 - (int) (notch * 84.0F), 6, (int) (notch * 84.0F));
							}
							else if (notch < 0.0F)
							{
								g.setColor(Color.WHITE);
								
								g.fillRect(area.x + (area.width / 2) + 200, area.y + 45 + (area.height - 130) - 84, 6, (int) (notch * -84.0F));
							}
						}
						
					}
				}
				
				/*
				 * Bottom Bar (Icons & Indicators)
				 */
				{
					/*
					 * Indicator Icons
					 */
					for (int i = 0; i <= 9; i++)
					{
						int left = area.x + 22 + (i * 46);
						int top = area.y + area.height - 60;
						
						g.setColor(ddu.contrast.get() ? Color.DARK_GRAY : COLOR_BACKGROUND.darker().darker());
						g.fillRect(left - 2, top - 2, 48, 48);
						g.setColor(ddu.contrast.get() ? Color.BLACK.brighter() : COLOR_BACKGROUND.darker());
						g.fillRect(left, top, 44, 44);
						
						switch (i)
						{
							case 0:
							{
								if (mtms.isolateMTCS.get())
								{
									g.drawImage(GraphicsResources.imageMTCSIcons[13], left, top, 44, 44, null);
								}
								else if (mtms.parentCab.alerter.getDoDisplay())
								{
									g.drawImage(GraphicsResources.imageMTCSIcons[2], left, top, 44, 44, null);
								}
								break;
							}
							
							case 1:
							{
								if (mtms.cruiseControlEnabled.get())
								{
									g.drawImage(GraphicsResources.imageMTCSIcons[6], left, top, 44, 44, null);
								}
								break;
							}
							
							case 2:
							{
								if (mtms.getOverspeedAmount() > 8.0F || mtms.parentCab.getDoForceStop())
								{
									if (System.currentTimeMillis() / 250 % 2 == 0)
									{
										g.setColor(Color.RED);
									}
									else
									{
										g.setColor(Color.RED.darker());
									}
									g.fillRect(left, top, 44, 44);
								}
								break;
							}
							
							case 3:
							{
								if (mtms.listParkBrakes.get().contains("1"))
								{
									g.drawImage(GraphicsResources.imageMTCSIcons[12], left, top, 44, 44, null);
								}
								break;
							}
							
							case 8:
							{
								if (PartTypeDoor.getDoorsUnlocked(train) || PartTypeDoor.getDoorsOpen(train))
								{
									g.drawImage(GraphicsResources.imageMTCSIcons[3], left, top, 44, 44, null);
								}
								else
								{
									g.drawImage(GraphicsResources.imageMTCSIcons[4], left, top, 44, 44, null);
								}
								
								break;
							}
							
							case 9:
							{
								switch (mtms.operationMode.get())
								{
									case FULL_SUPERVISION:
									{
										g.drawImage(GraphicsResources.imageMTCSIcons[1], left, top, 44, 44, null);
										break;
									}
									
									case ISOLATION:
									default:
									{
										g.drawImage(GraphicsResources.imageMTCSIcons[10], left, top, 44, 44, null);
										break;
									}
									
									case ON_SIGHT:
									{
										g.drawImage(GraphicsResources.imageMTCSIcons[0], left, top, 44, 44, null);
										break;
									}
									
									case REVERSING:
									{
										g.drawImage(GraphicsResources.imageMTCSIcons[11], left, top, 44, 44, null);
										break;
									}
								}
								break;
							}
						}
					}
					
				}
				
				break;
			}
			
			case MENU_SETTINGS:
			{
				/*
				 * Title
				 */
				{
					if (!ddu.contrast.get())
					{
						Stroke currentStroke = g.getStroke();
						{
							g.setStroke(new BasicStroke(20.0F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
							
							g.setColor(COLOR_BACKGROUND.darker());
							g.drawRect(area.x + 10, area.y + 10, area.width - 20, area.height - 20);
							g.fillRect(area.x + 10, area.y + 10, area.width - 20, area.height - 20);
							
							g.setColor(COLOR_BACKGROUND);
							g.drawRect(area.x + 10, area.y + 10, area.width - 20, 25);
							g.fillRect(area.x + 10, area.y + 10, area.width - 20, 25);
						}
						g.setStroke(currentStroke);
						g.fillRect(area.x, area.y + 10, area.width, 35);
					}
					
					g.setColor(Color.WHITE);
					g.setFont(FONT_DEFAULT);
					g.drawString("Settings", area.x + 20, area.y + 27);
				}
				
				/*
				 * Options
				 */
				{
					String optionName;
					boolean optionStatus;
					int currentOptionIndex = ddu.menuSettingsSelection.get();
					
					for (int i = 0; i < 5; i++)
					{
						APartProperty currentOption = null;
						
						switch (i)
						{
							case 0:
							{
								currentOption = mtms.automaticDoorBrake;
								break;
							}
							
							case 1:
							{
								currentOption = mtms.automaticDoorOpen;
								break;
							}
							
							case 2:
							{
								currentOption = mtms.imperial;
								break;
							}
							
							case 3:
							{
								currentOption = mtms.isolateMTCS;
								break;
							}
							
							case 4:
							{
								currentOption = mtms.autodriveEnable;
								break;
							}
						}
						
						if (currentOption != null)
						{
							optionName = currentOption.getName();
							optionStatus = (Boolean) currentOption.get();
						}
						else
						{
							continue;
						}
						
						/*
						 * Render the tile
						 */
						{
							if (currentOptionIndex == i)
							{
								if (ddu.contrast.get())
								{
									g.setColor(Color.DARK_GRAY.darker());
								}
								else
								{
									g.setColor(COLOR_BACKGROUND.brighter());
								}
							}
							else
							{
								if (ddu.contrast.get())
								{
									g.setColor(Color.BLACK);
								}
								else
								{
									g.setColor(COLOR_BACKGROUND);
								}
							}
							
							g.fillRect(area.x, area.y + 50 + (i * 50), area.width, 45);
						}
						
						/*
						 * Text
						 */
						{
							g.setColor(Color.WHITE);
							
							String translated = StatCollector.translateToLocal("zoranodensha.text." + optionName);
							g.drawString(translated, area.x + 20, area.y + 80 + (i * 50));
						}
						
						/*
						 * Status
						 */
						if (optionStatus)
						{
							int left = area.x + 442;
							int top = area.y + 50 + (i * 50);
							
							g.setColor(Color.GREEN);
							g.setStroke(new BasicStroke(8.0F, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
							g.drawPolyline(new int[]{left + 15, left + 25, left + 50}, new int[]{top + 25, top + 35, top + 10}, 3);
						}
					}
				}
				
				break;
			}
			
			/*
			 * Information Screen
			 */
			case MENU_INFORMATION:
			case MENU_INFORMATION_DETAILED:
			{
				/*
				 * Calculate clock values for other components of this screen.
				 */
				int worldTime = (int) (train.worldObj.getWorldTime() % 24000L);
				int worldTimeHour = ((worldTime / 1000) + 6) % 24;
				int worldTimeMinute = MathHelper.floor_double((worldTime % 1000) * 0.06);
				
				/* Background */
				if (!ddu.contrast.get())
				{
					Stroke oldStroke = g.getStroke();
					{
						g.setStroke(new BasicStroke(20.0F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
						g.setColor(COLOR_BACKGROUND);
						g.drawRect(area.x + 10, area.y + 10, area.width - 20, area.height - 20);
						g.fillRect(area.x + 10, area.y + 10, area.width - 20, area.height - 20);
					}
					g.setStroke(oldStroke);
				}
				
				// Number of carriages in the train.
				int carriageCount = train.getVehicleCount();
				// For each carriage, how many slots wide it is.
				int[] carriageCellWidth = new int[carriageCount];
				
				/*
				 * Each carriage's width will be represented by how many bogies it has.
				 */
				for (int carriageIndex = 0; carriageIndex < carriageCount; carriageIndex++)
				{
					OrderedTypesList<PartTypeBogie> carriageBogies = train.getVehicleAt(carriageIndex).getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
					carriageCellWidth[carriageIndex] = carriageBogies.size();
				}
				
				/*
				 * Render Left-hand Titles
				 */
				{
					g.setColor(ddu.contrast.get() ? Color.BLACK : COLOR_BACKGROUND.brighter());
					if (ddu.menu.get().equals(EDDUMenu.MENU_INFORMATION))
					{
						for (int y = 0; y < 2; y++)
						{
							g.fillRect(area.x + 10, area.y + 100 + (y * 40), 100, 30);
						}
					}
					else
					{
						for (int y = 0; y < 5; y++)
						{
							g.fillRect(area.x + 10, area.y + 100 + (y * 40), 100, 30);
						}
					}
					
					/*
					 * Title Box
					 */
					{
						// Background
						g.setColor(ddu.contrast.get() ? Color.BLACK.brighter() : COLOR_BACKGROUND.darker());
						g.fillRect(area.x + 10, area.y + 10, 100, 80);
						
						// ZnD Icon
						g.drawImage(GraphicsResources.imageZnD, area.x + 36, area.y + 18, 48, 48, null);
						
						// ZnD Version Text
						g.setFont(FONT_MINI);
						g.setColor(Color.GRAY);
						drawCenteredString(g, ModData.MODVERSION, area.x + 60, area.y + 80);
					}
					
					g.setFont(FONT_MINI);
					g.setColor(Color.LIGHT_GRAY);
					
					if (ddu.menu.get().equals(EDDUMenu.MENU_INFORMATION))
					{
						// TODO @Jaffa - Localise me, asshole. Also implement a helper method to render
						// a string inside a rectangle bounds
						drawCenteredString(g, "Doors", area.x + 60, area.y + 117);
						drawCenteredString(g, "Park Brake", area.x + 60, area.y + 157);
					}
					else
					{
						drawCenteredString(g, "Doors", area.x + 60, area.y + 117);
						drawCenteredString(g, "Park Brake", area.x + 60, area.y + 157);
						drawCenteredString(g, "Main Res", area.x + 60, area.y + 197);
						drawCenteredString(g, "Brake Pipe", area.x + 60, area.y + 237);
						drawCenteredString(g, "Brake Cyl", area.x + 60, area.y + 277);
					}
				}
				
				/*
				 * Render Carriages
				 */
				{
					int left = area.x + 120;
					int top = area.y + 40;
					
					for (int index = ddu.menuInformationCarriageScroll.get(); index < carriageCount; index++)
					{
						int carWidth = 20 + (carriageCellWidth[index] * 40);
						
						/*
						 * Don't render out of bounds
						 */
						if (left + carWidth > area.x + area.width - 30)
						{
							g.setColor(ddu.contrast.get() ? Color.DARK_GRAY : COLOR_BACKGROUND.darker());
							g.fillPolygon(new int[]{left + 8, left + 40, left + 8}, new int[]{top + 4, top + 20, top + 32}, 3);
							
							break;
						}
						
						// Car Background
						g.setColor(ddu.contrast.get() ? new Color(0.1F, 0.1F, 0.1F) : COLOR_BACKGROUND.darker());
						g.setStroke(new BasicStroke(2.0F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
						g.fillRect(left + 4, top + 1, carWidth - 8, 40 - 2);
						
						// Car Index Number
						g.setColor(COLOR_TEXT);
						g.setFont(FONT_DEFAULT);
						drawCenteredString(g, String.valueOf(index + 1), left + (carWidth / 2), top + 22);
						
						// Bogies
						int bogieLeft = left + 20;
						for (int bogie = 0; bogie < carriageCellWidth[index]; bogie++)
						{
							g.setColor(Color.LIGHT_GRAY);
							g.fillOval(bogieLeft, top + 40, 8, 8);
							g.fillOval(bogieLeft + 10, top + 40, 8, 8);
							
							bogieLeft += 40;
						}
						
						/*
						 * Dividing Line
						 */
						if (index < carriageCount - 1)
						{
							g.setColor(ddu.contrast.get() ? Color.DARK_GRAY : COLOR_BACKGROUND.darker());
							g.setStroke(new BasicStroke(2.0F));
							g.drawLine(left + carWidth, top, left + carWidth, ddu.menu.get().equals(EDDUMenu.MENU_INFORMATION) ? 176 : 296);
						}
						
						left += carWidth;
					}
				}
				
				/*
				 * Render Pantographs
				 */
				{
					int left = area.x + 120;
					int top = area.y + 10;
					final int padding = 4;
					ArrayList<float[]> pantographs = MTMS.unserialise(mtms.listPantographs.get());
					
					for (int index = ddu.menuInformationCarriageScroll.get(); index < carriageCount; index++)
					{
						if (index >= pantographs.size())
						{
							continue;
						}
						if (pantographs.get(index) == null)
						{
							continue;
						}
						if (pantographs.get(index).length == 0)
						{
							continue;
						}
						
						int carWidth = 20 + (carriageCellWidth[index] * 40);
						int cellWidth = carWidth / pantographs.get(index).length;
						
						if (pantographs.get(index)[0] == 0.0F)
						{
							left += cellWidth;
							continue;
						}
						
						/*
						 * Don't render out of bounds
						 */
						if (left + carWidth > area.x + area.width - 30)
						{
							break;
						}
						
						for (int pantograph = 0; pantograph < pantographs.get(index).length; pantograph++)
						{
							float pantographValue = Math.abs(pantographs.get(index)[pantograph]);
							boolean inverse = pantographs.get(index)[pantograph] > 0.0F;
							
							if (cellWidth < 30)
							{
								int offsetY = 0;
								
								if (pantographValue >= 0.99F)
								{
									g.setColor(Color.LIGHT_GRAY);
								}
								else if (pantographValue >= 0.15F)
								{
									g.setColor(Color.ORANGE);
								}
								else
								{
									g.setColor(Color.DARK_GRAY);
									offsetY = 15;
								}
								
								g.fillRect(left + padding, top + padding + offsetY, cellWidth - (padding * 2), 30 - offsetY - (padding * 2));
							}
							else
							{
								if (pantographValue >= 0.99F)
								{
									g.setColor(Color.LIGHT_GRAY);
								}
								else if (pantographValue >= 0.15F)
								{
									g.setColor(Color.ORANGE);
								}
								else
								{
									g.setColor(Color.LIGHT_GRAY);
								}
								
								int cellCentreX = left + (cellWidth / 2);
								
								g.setStroke(new BasicStroke(3.0F));
								
								if (pantographValue >= 0.15F)
								{
									g.drawLine(cellCentreX, top + 25, cellCentreX + (inverse ? 10 : -10), top + 15);
									g.drawLine(cellCentreX, top + 5, cellCentreX + (inverse ? 10 : -10), top + 15);
									
									g.drawLine(cellCentreX - 12, top + 4, cellCentreX + 12, top + 4);
								}
								else
								{
									g.drawLine(cellCentreX, top + 25, cellCentreX + (inverse ? 13 : -13), top + 22);
									g.drawLine(cellCentreX, top + 19, cellCentreX + (inverse ? 13 : -13), top + 22);
									
									g.drawLine(cellCentreX - 12, top + 18, cellCentreX + 12, top + 18);
								}
							}
							
							left += cellWidth;
						}
					}
				}
				
				/*
				 * Render park brake indicators
				 */
				{
					int left = area.x + 120;
					int top = area.y + 140;
					final int padding = 4;
					ArrayList<float[]> parkBrakes = MTMS.unserialise(mtms.listParkBrakes.get());
					
					for (int index = ddu.menuInformationCarriageScroll.get(); index < carriageCount; index++)
					{
						int carWidth = 20 + (carriageCellWidth[index] * 40);
						int cellWidth = carWidth / carriageCellWidth[index];
						
						/*
						 * Don't render out of bounds
						 */
						if (left + carWidth > area.x + area.width - 30)
						{
							break;
						}
						
						/*
						 * If there aren't enough values (e.g. when the values haven't been loaded yet)
						 * break out of the loop.
						 */
						if (index >= parkBrakes.size())
						{
							break;
						}
						
						for (int bogie = 0; bogie < carriageCellWidth[index]; bogie++)
						{
							boolean applied = parkBrakes.get(index)[bogie] > 0.0F;
							
							if (applied)
							{
								g.setColor(Color.RED);
							}
							else
							{
								g.setColor(Color.GREEN);
							}
							
							g.fillRect(left + (cellWidth * bogie) + padding, top + padding, cellWidth - (padding * 2), 30 - (padding * 2));
							
							if (applied)
							{
								g.setColor(Color.WHITE);
							}
							else
							{
								g.setColor(Color.BLACK);
							}
							
							g.setFont(FONT_MINI);
							// TODO @Jaffa - Localisation
							drawCenteredString(g, applied ? "ON" : "OFF", left + (cellWidth * bogie) + (cellWidth / 2), top + 17);
							
							g.setColor(ddu.contrast.get() ? Color.BLACK.brighter() : COLOR_BACKGROUND.darker());
							g.setStroke(new BasicStroke(2.0F));
							g.drawRect(left + (cellWidth * bogie) + padding, top + padding, cellWidth - (padding * 2), 30 - (padding * 2));
						}
						
						left += carWidth;
					}
				}
				
				/*
				 * Render Door Indicators
				 */
				{
					int left = area.x + 120;
					int top = area.y + 100;
					ArrayList<float[]> doors = MTMS.unserialise(mtms.listDoors.get());
					
					for (int index = ddu.menuInformationCarriageScroll.get(); index < carriageCount; index++)
					{
						if (index >= doors.size())
						{
							continue;
						}
						if (doors.get(index) == null)
						{
							continue;
						}
						
						int carWidth = 20 + (carriageCellWidth[index] * 40);
						int cellWidth = carWidth / doors.get(index).length;
						
						if (doors.get(index)[0] == 0.0F)
						{
							left += cellWidth;
							continue;
						}
						
						/*
						 * Don't render out of bounds
						 */
						if (left + carWidth > area.x + area.width - 30)
						{
							break;
						}
						
						/*
						 * Render the background (if this point is reached, there are definitely doors
						 * on this carriage).
						 */
						g.setColor(ddu.contrast.get() ? new Color(0.1F, 0.1F, 0.1F) : COLOR_BACKGROUND.darker());
						g.fillRect(left + 4, top + 2, carWidth - 8, 30 - 5);
						
						for (int door = 0; door < doors.get(index).length; door++)
						{
							boolean leftDoor = false;
							boolean rightDoor = false;
							float doorValue = doors.get(index)[door];
							
							if (doorValue >= 3.0F)
							{
								leftDoor = true;
								rightDoor = true;
							}
							else if (doorValue >= 2.0F)
							{
								rightDoor = true;
							}
							else if (doorValue >= 1.0F)
							{
								leftDoor = true;
							}
							
							/*
							 * Render right door
							 */
							if (rightDoor)
							{
								g.setColor(Color.RED);
							}
							else
							{
								g.setColor(Color.GREEN);
							}
							g.fillRect(left + (cellWidth * door) + 7, top + 3 - (rightDoor ? 7 : 0), cellWidth - 13, 6);
							
							/*
							 * Render left door
							 */
							if (leftDoor)
							{
								g.setColor(Color.RED);
							}
							else
							{
								g.setColor(Color.GREEN);
							}
							g.fillRect(left + (cellWidth * door) + 7, top + 21 + (leftDoor ? 6 : 0), cellWidth - 13, 6);
						}
						
						left += carWidth;
					}
				}
				
				if (ddu.menu.get().equals(EDDUMenu.MENU_INFORMATION))
				{
					/*
					 * Dividing Line for Bottom Section
					 */
					g.setColor(ddu.contrast.get() ? Color.DARK_GRAY : COLOR_BACKGROUND.darker());
					g.setStroke(new BasicStroke(2.0F));
					g.drawLine(area.x + 10, area.y + 185, area.x + area.width - 10, area.y + 185);
					
					/*
					 * Text Status Indicators
					 */
					{
						/*
						 * Titles
						 */
						g.setFont(FONT_MINI);
						g.setColor(Color.GRAY);
						g.drawString(StatCollector.translateToLocal("zoranodensha.text.mtms.trainLength"), area.x + 15, area.y + 205);
						g.drawString(StatCollector.translateToLocal("zoranodensha.text.mtms.trainMass"), area.x + 15, area.y + 260);
						
						g.setColor(Color.LIGHT_GRAY);
						g.setStroke(new BasicStroke(1.0F));
						g.drawLine(area.x + 15, area.y + 210, area.x + 215, area.y + 210);
						g.drawLine(area.x + 15, area.y + 265, area.x + 215, area.y + 265);
						
						g.setFont(FONT_DEFAULT);
						g.setColor(Color.WHITE);
						g.drawString(String.format("%d wagon%s, %.0f " + (imperial ? "yd" : "m"), mtms.trainWagonCount.get(), mtms.trainWagonCount.get() != 1 ? "s" : "", imperial ? mtms.trainLength.get() * M_TO_YARD : mtms.trainLength.get()),
								area.x + 15, area.y + 235);
						g.drawString(String.format("%.0f t", imperial ? mtms.trainMass.get() * TONNE_TO_TON : mtms.trainMass.get()), area.x + 15, area.y + 290);
					}
					
					/*
					 * Auxiliary Information
					 */
					{
						/*
						 * Speed + Light Mode
						 */
						if (ddu.contrast.get())
						{
							g.setStroke(new BasicStroke(2.0F));
							g.setColor(Color.DARK_GRAY);
							g.drawRect(area.x + area.width - 250, area.y + area.height - 55, 110, 40);
							g.drawRect(area.x + area.width - 250, area.y + area.height - 105, 110, 40);
						}
						else
						{
							g.setStroke(new BasicStroke(2.0F));
							g.setColor(COLOR_BACKGROUND.darker());
							g.fillRect(area.x + area.width - 250, area.y + area.height - 55, 110, 40);
							g.fillRect(area.x + area.width - 250, area.y + area.height - 105, 110, 40);
							g.setColor(COLOR_BACKGROUND.darker().darker());
							g.drawRect(area.x + area.width - 250, area.y + area.height - 55, 110, 40);
							g.drawRect(area.x + area.width - 250, area.y + area.height - 105, 110, 40);
						}
						
						/*
						 * Current Train Speed
						 */
						g.setColor(Color.WHITE);
						g.setFont(FONT_DEFAULT);
						if (imperial)
						{
							drawCenteredString(g, String.format("%.0f mph", mtms.trainSpeed.get() * KMH_TO_MPH), area.x + area.width - 195, area.y + area.height - 82);
						}
						else
						{
							drawCenteredString(g, String.format("%.0f km/h", mtms.trainSpeed.get()), area.x + area.width - 195, area.y + area.height - 82);
						}
						
						/*
						 * Track Speed
						 */
						if (mtms.speedLimit.get() < 0.0F)
						{
							g.setColor(Color.RED);
							g.setFont(FONT_MINI);
							
							drawCenteredString(g, String.format("ISOLATED"), area.x + area.width - 195, area.y + area.height - 32);
						}
						else
						{
							if (mtms.speedLimit.get() < mtms.trackSpeed.get() || mtms.speedLimit.get() <= 40.0F)
							{
								g.setColor(Color.YELLOW);
							}
							else
							{
								g.setColor(Color.WHITE);
							}
							g.setFont(FONT_DEFAULT.deriveFont(Font.BOLD));
							
							final float number = (mtms.speedLimit.get() < mtms.trackSpeed.get()) ? mtms.targetSpeed.get() : mtms.trackSpeed.get();
							if (imperial)
							{
								drawCenteredString(g, String.format("%.0f mph", number * KMH_TO_MPH), area.x + area.width - 195, area.y + area.height - 32);
							}
							else
							{
								drawCenteredString(g, String.format("%.0f km/h", number), area.x + area.width - 195, area.y + area.height - 32);
							}
						}
					}
					
					/*
					 * Analogue Clock
					 */
					{
						final int clockCentreX = area.x + area.width - 64;
						final int clockCentreY = area.y + area.height - 64;
						final int clockRadius = 50;
						
						/*
						 * Clock Face
						 */
						transform = g.getTransform();
						Stroke oldStroke = g.getStroke();
						{
							g.setColor(Color.LIGHT_GRAY);
							g.setStroke(new BasicStroke(3.0F, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER));
							
							for (int hour = 0; hour < 12; hour++)
							{
								g.drawLine(clockCentreX, clockCentreY - clockRadius, clockCentreX, clockCentreY - (int) (clockRadius * 0.8F));
								g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (360.0F / 12.0F), clockCentreX, clockCentreY);
							}
						}
						g.setStroke(oldStroke);
						g.setTransform(transform);
						
						/*
						 * Minute Hand
						 */
						transform = g.getTransform();
						{
							g.setColor(Color.WHITE);
							g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (6.0F * worldTimeMinute), clockCentreX, clockCentreY);
							g.fillPolygon(new int[]{clockCentreX - 3, clockCentreX, clockCentreX + 3, clockCentreX}, new int[]{clockCentreY, clockCentreY - (int) (clockRadius * 0.90F), clockCentreY, clockCentreY + 8}, 4);
						}
						g.setTransform(transform);
						
						/*
						 * Hour Hand
						 */
						transform = g.getTransform();
						{
							g.setColor(Color.WHITE);
							g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (30.0F * worldTimeHour), clockCentreX, clockCentreY);
							g.fillPolygon(new int[]{clockCentreX - 3, clockCentreX, clockCentreX + 3, clockCentreX}, new int[]{clockCentreY, clockCentreY - (int) (clockRadius * 0.50F), clockCentreY, clockCentreY + 8}, 4);
						}
						g.setTransform(transform);
					}
				}
				else
				{
					/*
					 * Render Main Reservoir Pressures
					 */
					{
						int left = area.x + 120;
						int top = area.y + 180;
						final int padding = 4;
						
						ArrayList<float[]> mainResPipe = MTMS.unserialise(mtms.listMainReservoirPipe.get());
						
						for (int index = ddu.menuInformationCarriageScroll.get(); index < carriageCount; index++)
						{
							int carWidth = 20 + (carriageCellWidth[index] * 40);
							int cellWidth = carWidth / carriageCellWidth[index];
							
							/*
							 * Don't render out of bounds
							 */
							if (left + carWidth > area.x + area.width - 30)
							{
								break;
							}
							
							/*
							 * If there aren't enough values (e.g. when the values haven't been loaded yet)
							 * break out of the loop.
							 */
							if (index >= mainResPipe.size())
							{
								break;
							}
							
							for (int bogie = 0; bogie < carriageCellWidth[index]; bogie++)
							{
								float value = mainResPipe.get(index)[bogie];
								if (imperial)
								{
									value *= KPA_TO_PSI;
								}
								value = Math.round(value / 5.0F) * 5.0F;
								
								g.setColor(Color.WHITE);
								g.setFont(FONT_MINI_PLAIN);
								drawCenteredString(g, String.format("%.0f", value), left + (cellWidth * bogie) + (cellWidth / 2), top + 17);
								
								if ((imperial && value < 87.0F) || (!imperial && value < 600.0F))
								{
									g.setColor(Color.RED);
								}
								else
								{
									g.setColor(ddu.contrast.get() ? Color.DARK_GRAY.darker() : COLOR_BACKGROUND.darker());
								}
								g.setStroke(new BasicStroke(2.0F));
								g.drawRect(left + (cellWidth * bogie) + padding, top + padding, cellWidth - (padding * 2), 30 - (padding * 2));
							}
							
							left += carWidth;
						}
					}
					
					/*
					 * Render Brake Pipe
					 */
					{
						int left = area.x + 120;
						int top = area.y + 220;
						final int padding = 4;
						
						ArrayList<float[]> brakePipe = MTMS.unserialise(mtms.listBrakePipe.get());
						
						for (int index = ddu.menuInformationCarriageScroll.get(); index < carriageCount; index++)
						{
							int carWidth = 20 + (carriageCellWidth[index] * 40);
							int cellWidth = carWidth / carriageCellWidth[index];
							
							/*
							 * Don't render out of bounds
							 */
							if (left + carWidth > area.x + area.width - 30)
							{
								break;
							}
							
							/*
							 * If there aren't enough values (e.g. when the values haven't been loaded yet)
							 * break out of the loop.
							 */
							if (index >= brakePipe.size())
							{
								break;
							}
							
							for (int bogie = 0; bogie < carriageCellWidth[index]; bogie++)
							{
								float value = brakePipe.get(index)[bogie];
								
								if (imperial)
								{
									value *= KPA_TO_PSI;
								}
								
								value = Math.round(value / 5.0F) * 5.0F;
								
								g.setColor(Color.WHITE);
								g.setFont(FONT_MINI_PLAIN);
								drawCenteredString(g, String.format("%.0f", value), left + (cellWidth * bogie) + (cellWidth / 2), top + 17);
								
								if ((imperial && value < 44.0F) || (!imperial && value < 300.0F))
								{
									g.setStroke(new BasicStroke(3.0F));
									g.setColor(Color.RED);
								}
								else
								{
									g.setStroke(new BasicStroke(2.0F));
									g.setColor(ddu.contrast.get() ? Color.DARK_GRAY.darker() : COLOR_BACKGROUND.darker());
								}
								g.drawRect(left + (cellWidth * bogie) + padding, top + padding, cellWidth - (padding * 2), 30 - (padding * 2));
							}
							
							left += carWidth;
						}
					}
					
					/*
					 * Render Brake Cylinders
					 */
					{
						int left = area.x + 120;
						int top = area.y + 260;
						final int padding = 4;
						
						ArrayList<float[]> brakeCylinders = MTMS.unserialise(mtms.listBrakeCylinders.get());
						
						for (int index = ddu.menuInformationCarriageScroll.get(); index < carriageCount; index++)
						{
							int carWidth = 20 + (carriageCellWidth[index] * 40);
							int cellWidth = carWidth / carriageCellWidth[index];
							
							/*
							 * Don't render out of bounds
							 */
							if (left + carWidth > area.x + area.width - 30)
							{
								break;
							}
							
							/*
							 * If there aren't enough values (e.g. when the values haven't been loaded yet)
							 * break out of the loop.
							 */
							if (index >= brakeCylinders.size())
							{
								break;
							}
							
							for (int bogie = 0; bogie < carriageCellWidth[index]; bogie++)
							{
								float value = brakeCylinders.get(index)[bogie];
								
								if (imperial)
								{
									value *= KPA_TO_PSI;
								}
								
								value = Math.round(value / 5.0F) * 5.0F;
								
								g.setColor(Color.WHITE);
								g.setFont(FONT_MINI_PLAIN);
								drawCenteredString(g, String.format("%.0f", value), left + (cellWidth * bogie) + (cellWidth / 2), top + 17);
								
								g.setColor(ddu.contrast.get() ? Color.DARK_GRAY.darker() : COLOR_BACKGROUND.darker());
								g.setStroke(new BasicStroke(2.0F));
								g.drawRect(left + (cellWidth * bogie) + padding, top + padding, cellWidth - (padding * 2), 30 - (padding * 2));
							}
							
							left += carWidth;
						}
					}
				}
				
				break;
			}
			
			/*
			 * Unknown/unimplemented menu.
			 */
			default:
			{
				/* Background Logo */
				g.drawImage(GraphicsResources.imageZnD, (width / 2) - 192, (height / 2) - 88, 384, 144, null);
				
				g.setColor(Color.ORANGE);
				g.fillRect((width / 2) - 150, (height / 2) + 70, 300, 40);
				
				g.setColor(Color.BLACK);
				g.setFont(FONT_DEFAULT);
				drawCenteredString(g, ModData.NAME + " " + ModData.MODVERSION, width / 2, (height / 2) + 90);
				
				break;
			}
		}
		
		/*
		 * Draw the screen glare.
		 */
		{
			g.drawImage(GraphicsResources.imageScreenGlare, 0, 0, width, height, null);
			
			/*
			 * Brightness Control
			 */
			for (int i = 0; i < ddu.brightness.get(); i++)
			{
				g.drawImage(GraphicsResources.imageScreenShade, 0, 0, width, height, null);
			}
		}
		
		texture.loadTexture();
	}
	
}
