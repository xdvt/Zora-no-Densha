package zoranodensha.vehicleParts.client.render.seat;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.seat.VehParSeat;



@SideOnly(Side.CLIENT)
public class VehParRenderer_Seat implements IVehiclePartRenderer
{
	/** Cab model, containing the basic seat. */
	protected static final ObjModelSpeedy model_interior = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_interior");

	/** Seat object groups. Initialised for faster rendering (saves String comparison). */
	protected static final ObjectList seatSecond_back = model_interior.makeGroup("SeatSecond_Back");
	protected static final ObjectList seatSecond_base = model_interior.makeGroup("SeatSecond_Base");
	protected static final ObjectList seatSecond_handles = model_interior.makeGroup("SeatSecond_Handles");
	protected static final ObjectList seatSecond_legs = model_interior.makeGroup("SeatSecond_Legs");

	/** Flag set and referenced by {@link #renderPassenger(float) renderPassenger()} to skip the RenderLivingEvent cancellation during passenger rendering. */
	public static boolean bypassEventCancellation;

	/** The part to render. */
	final VehParSeat part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_Seat(VehParSeat part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParSeat)
		{
			return new VehParRenderer_Seat((VehParSeat)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		/* Reset vertex state if required */
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		/* Render seat base */
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				int amount = part.prop_amount.get();
				int start = amount >= 3 ? -1 : 0;
				amount += start;

				for (int i = start; i < amount; i++)
				{
					tessellator.setTranslation(0.0, -0.075, i * 0.68);

					part.prop_color.apply(tessellator);
					seatSecond_base.render(tessellator);

					part.prop_colorPadding.apply(tessellator);
					seatSecond_back.render(tessellator);

					part.prop_colorHandles.apply(tessellator);
					seatSecond_handles.render(tessellator);

					tessellator.setColorRGBA_F(0.2F, 0.2F, 0.2F, 1.0F);
					seatSecond_legs.render(tessellator);
				}

				tessellator.setTranslation(0, 0, 0);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();

		/* Render passenger */
		renderPassenger(partialTick);
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.9F, 0.9F, 0.9F);
			GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}

	/**
	 * Helper method to render the passenger sitting in this seat, if existent.
	 */
	private void renderPassenger(float partialTick)
	{
		Entity passenger = part.type_seat.getPassenger();
		if (passenger != null)
		{
			boolean isNotPlayer = passenger.getEntityId() != Minecraft.getMinecraft().thePlayer.getEntityId();
			if (Minecraft.getMinecraft().gameSettings.thirdPersonView != 0 || isNotPlayer)
			{
				float y = (float)((passenger.posY - passenger.ridingEntity.posY) / part.getScale()[1]);
				if (isNotPlayer)
				{
					y += part.type_seat.getMountedYOffset();
				}

				GL11.glTranslatef(0.0F, y, 0.0F);
				GL11.glScalef(0.6F, 0.6F, 0.6F);

				bypassEventCancellation = true;
				{
					if (passenger instanceof EntityLivingBase)
					{
						GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
						EntityLivingBase passengerLiving = ((EntityLivingBase)passenger);

						float yaw = passengerLiving.rotationYaw;
						float prevYaw = passengerLiving.prevRotationYaw;
						float renderYaw = passengerLiving.renderYawOffset;
						float prevYawHead = passengerLiving.prevRotationYawHead;
						float yawHead = passengerLiving.rotationYawHead;
						float limbSwingAmount = passengerLiving.limbSwingAmount;
						float prevLimbSwingAmount = passengerLiving.prevLimbSwingAmount;

						passengerLiving.prevRotationYaw = passengerLiving.rotationYaw = 0.0F;
						passengerLiving.renderYawOffset = 0.0F;
						passengerLiving.prevRotationYawHead = passengerLiving.rotationYawHead = 0.0F;
						passengerLiving.prevLimbSwingAmount = passengerLiving.limbSwingAmount = 0.0F;
						RenderManager.instance.renderEntityWithPosYaw(passenger, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
						passengerLiving.rotationYaw = yaw;
						passengerLiving.prevRotationYaw = prevYaw;
						passengerLiving.renderYawOffset = renderYaw;
						passengerLiving.prevRotationYawHead = prevYawHead;
						passengerLiving.rotationYawHead = yawHead;
						passengerLiving.limbSwingAmount = limbSwingAmount;
						passengerLiving.prevLimbSwingAmount = prevLimbSwingAmount;
					}
					else
					{
						RenderManager.instance.renderEntityWithPosYaw(passenger, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
					}
				}
				bypassEventCancellation = false;
			}
		}
	}
}
