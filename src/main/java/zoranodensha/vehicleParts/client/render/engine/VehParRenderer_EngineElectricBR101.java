package zoranodensha.vehicleParts.client.render.engine;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_BR101;
import zoranodensha.vehicleParts.common.parts.engine.VehParEngineElectricBR101;



@SideOnly(Side.CLIENT)
public class VehParRenderer_EngineElectricBR101 extends AVehParRendererBase_BR101 implements IVehiclePartRenderer
{
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/vehicleParts/engineElectricBR101.png");
	private static final ObjectList battery = model.makeGroup("Battery");

	/** The part to render. */
	final VehParEngineElectricBR101 part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_EngineElectricBR101(VehParEngineElectricBR101 part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParEngineElectricBR101)
		{
			return new VehParRenderer_EngineElectricBR101((VehParEngineElectricBR101)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
				battery.render(tessellator);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
