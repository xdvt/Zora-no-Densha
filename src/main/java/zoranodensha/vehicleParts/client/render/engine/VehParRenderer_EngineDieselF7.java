package zoranodensha.vehicleParts.client.render.engine;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.engine.VehParEngineDieselF7;



public class VehParRenderer_EngineDieselF7 implements IVehiclePartRenderer
{
	/*
	 * Models
	 */
	private static final ObjModelSpeedy model = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_engineDiesel");

	/*
	 * Objects
	 */
	private static final ObjectList begin_base = model.makeGroup("Begin_Base");
	private static final ObjectList begin_cylinders = model.makeGroup("Begin_Cylinders");
	private static final ObjectList begin_detail = model.makeGroup("Begin_Detail");
	private static final ObjectList begin_tank = model.makeGroup("Begin_Tank");
	private static final ObjectList middle_base = model.makeGroup("Middle_Base");
	private static final ObjectList middle_cylinders = model.makeGroup("Middle_Cylinders");
	private static final ObjectList middle_detail = model.makeGroup("Middle_Detail");
	private static final ObjectList middle_tank = model.makeGroup("Middle_Tank");
	private static final ObjectList end_base = model.makeGroup("End_Base");
	private static final ObjectList end_cylinders = model.makeGroup("End_Cylinders");
	private static final ObjectList end_detail = model.makeGroup("End_Detail");
	private static final ObjectList end_tank = model.makeGroup("End_Tank");

	private final VehParEngineDieselF7 part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_EngineDieselF7(VehParEngineDieselF7 part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParEngineDieselF7)
		{
			return new VehParRenderer_EngineDieselF7((VehParEngineDieselF7)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		int length = part.pieces.get();

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				switch (length)
				{
					case 1: {
						tessellator.setTranslation(-0.5F, 0.0F, 0.0F);

						tessellator.setColorRGBA_F(0.25F, 0.25F, 0.25F, 1.00F);
						begin_base.render(tessellator);

						tessellator.setColorRGBA_F(0.45F, 0.45F, 0.45F, 1.00F);
						begin_cylinders.render(tessellator);

						tessellator.setColorRGBA_F(0.55F, 0.55F, 0.55F, 1.00F);
						begin_detail.render(tessellator);

						tessellator.setColorRGBA_F(0.20F, 0.20F, 0.20F, 1.00F);
						begin_tank.render(tessellator);

						tessellator.setTranslation(0.5F, 0.0F, 0.0F);

						tessellator.setColorRGBA_F(0.25F, 0.25F, 0.25F, 1.00F);
						end_base.render(tessellator);

						tessellator.setColorRGBA_F(0.45F, 0.45F, 0.45F, 1.00F);
						end_cylinders.render(tessellator);

						tessellator.setColorRGBA_F(0.55F, 0.55F, 0.55F, 1.00F);
						end_detail.render(tessellator);

						tessellator.setColorRGBA_F(0.20F, 0.20F, 0.20F, 1.00F);
						end_tank.render(tessellator);

						break;
					}

					case 2: {
						tessellator.setTranslation(0.0F, 0.0F, 0.0F);

						tessellator.setColorRGBA_F(0.25F, 0.25F, 0.25F, 1.00F);
						middle_base.render(tessellator);

						tessellator.setColorRGBA_F(0.45F, 0.45F, 0.45F, 1.00F);
						middle_cylinders.render(tessellator);

						tessellator.setColorRGBA_F(0.55F, 0.55F, 0.55F, 1.00F);
						middle_detail.render(tessellator);

						tessellator.setColorRGBA_F(0.20F, 0.20F, 0.20F, 1.00F);
						middle_tank.render(tessellator);

						tessellator.setTranslation(-1.0F, 0.0F, 0.0F);

						tessellator.setColorRGBA_F(0.25F, 0.25F, 0.25F, 1.00F);
						begin_base.render(tessellator);

						tessellator.setColorRGBA_F(0.45F, 0.45F, 0.45F, 1.00F);
						begin_cylinders.render(tessellator);

						tessellator.setColorRGBA_F(0.55F, 0.55F, 0.55F, 1.00F);
						begin_detail.render(tessellator);

						tessellator.setColorRGBA_F(0.20F, 0.20F, 0.20F, 1.00F);
						begin_tank.render(tessellator);

						tessellator.setTranslation(1.0F, 0.0F, 0.0F);

						tessellator.setColorRGBA_F(0.25F, 0.25F, 0.25F, 1.00F);
						end_base.render(tessellator);

						tessellator.setColorRGBA_F(0.45F, 0.45F, 0.45F, 1.00F);
						end_cylinders.render(tessellator);

						tessellator.setColorRGBA_F(0.55F, 0.55F, 0.55F, 1.00F);
						end_detail.render(tessellator);

						tessellator.setColorRGBA_F(0.20F, 0.20F, 0.20F, 1.00F);
						end_tank.render(tessellator);

						break;
					}
				}

				tessellator.setTranslation(0, 0, 0);

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}


	@Override
	public void renderPartItem(ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
