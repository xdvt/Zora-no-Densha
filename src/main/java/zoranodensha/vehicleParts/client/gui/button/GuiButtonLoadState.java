package zoranodensha.vehicleParts.client.gui.button;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import zoranodensha.vehicleParts.client.gui.GUIPartTypeTransportTank;



@SideOnly(Side.CLIENT)
public class GuiButtonLoadState extends GuiButton
{
	private final int loadState;



	public GuiButtonLoadState(int id, int posX, int posY, int loadState)
	{
		super(id, posX, posY, 8, 8, "");
		this.loadState = loadState;
	}

	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY)
	{
		if (!visible)
		{
			return;
		}

		mc.getTextureManager().bindTexture(GUIPartTypeTransportTank.texture);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

		this.getHoverState(mouseX, mouseY);
		mouseDragged(mc, mouseX, mouseY);
		drawTexturedModalRect(xPosition, yPosition, 176 + (loadState * 8), 50, width, height);
	}

	public int getHoverState(int mouseX, int mouseY)
	{
		field_146123_n = mouseX >= xPosition && mouseY >= yPosition && mouseX < xPosition + width && mouseY < yPosition + height;
		return super.getHoverState(field_146123_n);
	}
}
