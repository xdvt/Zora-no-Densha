Copyright © December 2018 Leshuwa Kaiheiwa
All rights reserved.


§0 COPYRIGHT OWNER PSEUDONYM AND AREA OF JURISDICTION

1. The area of jurisdiction is the Federal Republic of Germany.

2. The copyright owner reserves the right to hide his identity behind
 the given pseudonym as stated in §1.3 of the Council Directive 93/98/EWG
 of the council of the European Communities from 24th November 1993.


§1 DISCLAIMER OF LIABILITY

1. This software is provided by the copyright owner and contributors
 "as is" and any express or implied warranties are expressly disclaimed.

2. In no event shall the copyright owner or contributors or their relatives
 be liable for any damages, including, but not limited to, direct, indirect,
 physical, mental, or consequential damages, however caused, arising in any way
 out of the use of this software, even if advised of the possibility of such
 damage.


§2 CODEX OF FAIR USAGE

Any action executed by third parties that involve the project in any way is
 allowed, provided the following conditions are met:

1. The idea, the process of creation, and the final result must not, under
 any circumstances, follow the intention of, or create a result that is, harming,
 harassing or discriminating against any entity, including, but not limited to,
 a person, a group, or a nationality.

2. If the suspicion or clear fact arises that the end-product or its use breaks the
 condition described in §2.1, the author of the product remains responsible to
 remove all sources of harm originating from the product.
 2a If the author fails to do so within a period of two weeks, all rights to use
  this software will be lost and any use of the software protected by this license
  will be prohibited for a period of 365 days.
 2b The usage of this software within above period of prohibition shall be treated
  as copyright infringement as it breaks the terms described in this license.

3. The usage of this software, in parts or as a whole, is free to the end-user.


§3 TERMS OF USAGE

Any action by third parties involving the files protected by this license
is allowed, provided the following conditions are met:

1. All actions and intentions must not break the terms described in §2.

2. Actions that are permitted without the author's knowledge are:
 2a Decompiling and viewing the source code for educational purposes;
 2b Usage of API packages for any purpose under previously mentioned conditions;
 2c Copying non-API code and files, modified or unmodified, provided that the used code
  is clearly marked as the copyright owner's property and that the usage of the code does
  not break previously mentioned conditions.


§4 APPLICATION OF THIS LICENSE

1. The packages, subpackages, and all classes and their subclasses covered by this license
 unless stated otherwise remain property of the copyright owner to the point of his death.
 1a Afterwards, the project is considered public good and shall be accessible and free
  to use for anyone who desires access, provided their works do not break with the terms
  described in §2.
 1b The copyright owner waives all copyrights on files and methods where credit is given to
  third party copyright owners.